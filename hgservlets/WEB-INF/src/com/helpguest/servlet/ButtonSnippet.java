/**
 * Generate a snippet for integration into html pages
 *
 * Copyright 2004-2006 - HelpGuest Technologies, Inc.
 *
 * @author Mark Abrams
 */
package com.helpguest.servlet;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.ExpertAccount;
import com.helpguest.htmlgen.Generator;

public class ButtonSnippet extends HttpServlet {
   
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.ButtonSnippet");

    private static final String FULLY_QUALIFIED_DOMAIN_NAME = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private static final String JAWS_APPS_DIR = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.JAWS_APPS_DIR.getKey());
    private static final String WEB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey());
    
    private enum Size {
        BIG ("big"),
        LITTLE ("little");
        
        private String val;
        Size(String val) {
            this.val = val;
        }
        
        public String toString() {
            return val;
        }
        
        public boolean equals(Size s) {
            if (this.toString().equals(s.toString())){
                return true;
            }
            return false;
        }
    }
    
    private String getConnectData(String uid, String buttonName) {
        StringBuffer buf = new StringBuffer();
            buf.append("<script type=\"text/javascript\" src=\"" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/js/Jaws.js\">\r\n");
            buf.append("   <!--\r\n");
            buf.append("   var isIE = isExplorer();\r\n");
            buf.append("   //-->\r\n");
            buf.append("</script>\r\n");
            buf.append("<script LANGUAGE=\"VBScript\">\r\n");
            buf.append("   <!--\r\n");
            buf.append("   on error resume next\r\n");
            buf.append("   dim javaws150Installed\r\n");
            buf.append("   If isIE = \"true\" then\r\n");
            buf.append("   	If (Not(IsObject(CreateObject(\"JavaWebStart.isInstalled.1.5.0.0\")))) Then\r\n");
            buf.append("	    javaws150Installed = 0\r\n");
            buf.append("        Else \r\n");
            buf.append("            javaws150Installed = 1\r\n");
            buf.append("	End If\r\n");
            buf.append("   End If\r\n");
            buf.append("   //-->\r\n");
            buf.append("</script>\r\n");
            buf.append("<script LANGUAGE=\"JavaScript\" type=\"text/javascript\">\r\n");
            buf.append("  <!--\r\n");
            buf.append("  if (javaws150Installed || (navigator.userAgent.indexOf(\"Gecko\") !=-1)) {\r\n");
            buf.append("    document.write(\"<a href=\\\"" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + JAWS_APPS_DIR + "/client/" + uid + "click_wrap.jnlp\\\">\");\r\n");
            buf.append("    document.write(\"<img src=\\\"" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/images/" + buttonName + "\\\" alt=\\\"HelpGuest\\\" border=0 >\");\r\n");
            buf.append("    document.write(\"</a>\");\r\n");
            buf.append("  } else {\r\n");
            buf.append("    document.write(\"<a href=http://java.sun.com/PluginBrowserCheck?pass=https://" + FULLY_QUALIFIED_DOMAIN_NAME + JAWS_APPS_DIR + "/client/" + uid + ".html&fail=http://java.sun.com/j2se/1.5.0/download.html>\");\r\n");
            buf.append("    document.write(\"<img src=\\\"" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/images/" + buttonName + "\\\" alt=\\\"HelpGuest\\\" border=0 >\");\r\n");
            buf.append("    document.write(\"</a>\");\r\n");
            buf.append("  }\r\n");
            buf.append("  -->\r\n");
            buf.append("</script>\r\n");
        return buf.toString();
    }
    public void doGet(HttpServletRequest request,
            HttpServletResponse response) 
            throws IOException, ServletException {
	doPost(request, response);
    }


    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String email = request.getParameter("user");
        String pass = request.getParameter("password");
        String bigButtonName = "buttonUp-64.png";
        String littleButtonName = "buttonDown-32.png";
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        //Require the password to get the expert account info
        ExpertAccount ea = ExpertAccount.getExpertAccount(email,pass);

        String bigData = getConnectData(ea.getGuestUid(), bigButtonName);
        String littleData = getConnectData(ea.getGuestUid(), littleButtonName);
        String[] content = {bigData,littleData};
        try {
            out.print(Generator.generate(this,"/WEB-INF/button_snippet/",content));
        } catch (FileNotFoundException fnx) {
            log.error("could not access button_snippet parts.", fnx);
        }catch (Exception ex) {
            log.error("failed to generate links.", ex);
        }

    }
}
