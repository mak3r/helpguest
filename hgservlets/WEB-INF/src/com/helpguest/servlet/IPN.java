/*
 * IPN.java
 *
 * Instant Payment Notification handler
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import com.helpguest.data.DataBase;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.FileSystem;
import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;
import com.helpguest.servlet.util.JnlpGen;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author mabrams
 */
public class IPN extends HttpServlet implements ResultHandler {

    private int queryId;
    private int id;
    private static final int RATES_VALUES = 0;   
    private static final int PROMO_PLAN = 1;
    private static final int STANDARD_PLAN = 2;
    private static final int DUP_TXN = 3;
    private boolean isValid = false;
    private boolean isStandard = false;
    private boolean isDupTxn = false;
    private float ratesRate = 0.0f;
    private int ratesHours = 0;
    private int ratesDays = 0;
    private boolean userExists = false;
    private final String INVALID = "INVALID";
    private final String VERIFIED = "VERIFIED";
    private final String COMPLETED = "Completed";
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.IPN");
                    
    
    /** Creates a new instance of Authenticator */
    public IPN() {
    }
    
    public void getRatesValues(String code) {
        queryId = RATES_VALUES;
        String promoQuery = "select daily_rate, usage_in_hours, shelf_life_in_days " +
                            "from rates where id in ( " +
                            "select rates_id from promo where name = '" +
                            code + "' and expired = 0)";
        DataBase.getInstance().submit(promoQuery,this);
        
    }
    
    protected boolean isValidPlan(String code) {
        if (!isStandardPlan(code)) {
            //it's not standard see if its a promo'
            queryId = PROMO_PLAN;        
            String validateQuery = "select daily_rate, usage_in_hours, shelf_life_in_days " +
                            "from rates where id in ( " +
                            "select rates_id from promo where name = '" +
                            code + "' and expired = 0)";
            DataBase.getInstance().submit(validateQuery,this);            
            return isValid;
        }
        return isStandard;
    }
    
    protected boolean isStandardPlan(String code) {
        queryId = STANDARD_PLAN;
        String standardQuery = "select daily_rate, usage_in_hours, shelf_life_in_days" +
                " from rates where rate_code = '" + code + "' " +
                " and is_valid = 1 and is_standard = 1";
        DataBase.getInstance().submit(standardQuery, this);
        return isStandard;
    }
    
    public void expirePromo(String code) {
        String expireQuery = "update promo set expired = 1 where name = '" + code + "'";
        DataBase.getInstance().submit(expireQuery);
    }
    
    protected boolean isDupTxnId(String txnId) {
        queryId = DUP_TXN;
        String txnIdQuery = "select * from ipn where txn_id = '" + txnId + "'";
        DataBase.getInstance().submit(txnIdQuery, this);
        return isDupTxn;
    }

    public void capture(ResultSet rs, int queryId) {
        if (rs == null) {
            log.debug("result set is null");
        } else {
            switch (queryId) {
                case RATES_VALUES:
                    try {
                        if (rs.next()) {
                            ratesRate = rs.getFloat("daily_rate");
                            ratesHours = rs.getInt("usage_in_hours");
                            ratesDays = rs.getInt("shelf_life_in_days");
                        }
                    } catch (SQLException sqlx) {
                        //This error is thrown if there is a problem accessing the database
                        log.error("Failed to get rates values.", sqlx);
                    }
                    break;
                case STANDARD_PLAN:
                    try {
                        if (rs.next()) {
                            ratesRate = rs.getFloat("daily_rate");
                            ratesHours = rs.getInt("usage_in_hours");
                            ratesDays = rs.getInt("shelf_life_in_days");
                            isStandard = true;
                        } else {
                            isStandard = false;
                        }
                    } catch (SQLException sqlx) {
                        //This error is thrown if there is a problem accessing the database
                        log.error("Failed to verify standard plan.", sqlx);
                    }
                    break;
                case PROMO_PLAN:
                    try {
                        if (rs.next()) {
                            ratesRate = rs.getFloat("daily_rate");
                            ratesHours = rs.getInt("usage_in_hours");
                            ratesDays = rs.getInt("shelf_life_in_days");
                            isValid = true;
                        } else {
                            isValid = false;
                        }
                    } catch (SQLException sqlx) {
                        //This error is thrown if there is a problem accessing the database
                        log.error("Failed to verify promo plan.", sqlx);
                    }
                    break;
                case DUP_TXN:
                    try {
                        if (rs.next()) {
                            isDupTxn = true;
                        } else {
                            isDupTxn = false;
                            log.warn("duplicat txn_id");
                        }
                    } catch (SQLException sqlx) {
                        //This error is thrown if there is a problem accessing the database
                        log.error("Failed to check duplicate transaction.", sqlx);
                    }
                    break;
                default:
                    break;
            }
        }//end of else
    }
    
    private boolean isValidPaymentAccount(String recipient) {
        if ("sales@helpguest.com".equals(recipient) || 
            "sandbox@helpguest.com".equals(recipient)) {
            return true;
        }
        log.warn("invalid account for payment: " + recipient);
        return false;
    }
    
    private boolean isMCDataOK(String gross, String currency, int hours, float rate) {
        //convert gross to a float
        float grossFloat = 0.0f;
        try {
            grossFloat = Float.parseFloat(gross);
        } catch (NumberFormatException nfx) {
            log.error("couldn't convert gross value to a float.", nfx);
        }
        if (grossFloat == calculateCost(rate,hours) && "USD".equalsIgnoreCase(currency)) {
            return true;
        }
        log.warn("MC data set is not valid: " + gross + "/" + currency);
        return false;
    }
    
    private float calculateCost(float rate, int hours) {
        return rate*(hours/8);
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }
    
    private int getUsage(String itemNumber) {
        return Integer.parseInt(itemNumber.substring(0,itemNumber.indexOf('-')));
    }
    
    private int getShelfLife(String itemNumber) {
        return Integer.parseInt(itemNumber.substring(itemNumber.indexOf('-')+1));
    }    

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String notifyVersion = request.getParameter("notify_version");
        String verifySign = request.getParameter("verify_sign");
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String payerBusinessName = request.getParameter("payer_business_name");
        String payerEmail = request.getParameter("payer_email");
        String payerId = request.getParameter("payer_id");
        String itemName = request.getParameter("item_name");
        String itemNumber = request.getParameter("item_number");
        String custom = request.getParameter("custom");
        String tax = request.getParameter("tax");
        String testIpn = request.getParameter("test_ipn");
        String paymentStatus = request.getParameter("payment_status");
        String paymentDate = request.getParameter("payment_date");
        String mcGross = request.getParameter("mc_gross");
        String mcCurrency = request.getParameter("mc_currency");
        String receiverEmail = request.getParameter("receiver_email");
        String txnId = request.getParameter("txn_id");
        String paymentFee = request.getParameter("payment_fee");
        String cmd = request.getParameter("cmd");
        int expertId = -1;
        
        //set true when we know that the identity has been email verified
        boolean isHGVerified = false;
        isValid = false;
        isStandard = false;
        isDupTxn = false;
        Enumeration paramNames = request.getParameterNames();
        int hours = getUsage(itemNumber);
        int days = getShelfLife(itemNumber);
        //initialize locals back to 0;
        ratesRate = 0.0f;
        ratesHours = 0;
        ratesDays = 0;

        PrintWriter out;
        try {
            //Make sure we know this payer and they are helpguest email verified
            //custom is the email per helpguest
            ExpertAccount ea = ExpertAccount.getExpertAccount(custom);
            if (ea != null) {
                expertId = ea.getId();
                isHGVerified = ea.isVerified();
            } else { //end if (ea != null)
                log.warn("Unable to get data on expert " + custom);
            }
            
            //Respond to the ipn regardless of whether we know this account identity
            //open a connection with the validation site
            URL url = new URL("https://www.sandbox.paypal.com/cgi-bin/webscr");
            URLConnection uc = url.openConnection();
            uc.setDoOutput(true);
            uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            //log transaction to database
            //Be sure there is a verify sign
            if (verifySign == null) {
                verifySign = String.valueOf(System.currentTimeMillis());
            }

            if ("_hg_promo".equals(cmd)) {
                //Verify the details are not spoofed
                getRatesValues(itemName);
                response.setContentType("text/html");
                out = response.getWriter();
                if (ratesHours == hours && ratesDays == days && isHGVerified) {
                    //enable the acount
                    ea.addServices(hours,days);                    
                    //mark the promo code as expired
                    expirePromo(itemName);
                    //Send em to the login page
                    String success = "You have successfully checked out.<br>Please login.";
                    String[] loginContents = {success, "", ""};
                    try {
                        out.print(Generator.generate(this, "WEB-INF/login/", loginContents));
                    } catch (FileNotFoundException fnx) {
                        log.error("Could not generate login page.", fnx);
                    }
                } else {
                    //The promo code was invalid, contact a HG rep
                    out.println("The promotional code you entered was invald.");
                    out.println("or Your account has not been verified.");
                    out.println("Please contact a HelpGuest rep for assistance.");
                }

            } else {
                //Generate the content to post back
                StringBuffer postBack = new StringBuffer();
                postBack.append("cmd=_notify-validate");
                while (paramNames.hasMoreElements()) {
                    String name = (String)paramNames.nextElement();
                    if (name != null) {
                        postBack.append("&" + name + "=" + URLEncoder.encode(request.getParameter(name),"UTF-8"));
                        log.debug(name + "=" + request.getParameter(name));
                    }
                }
                if (log.isDebugEnabled()) {log.debug("Created postBack = '" + postBack + "'");}
                //send the data off
                out = new PrintWriter(uc.getOutputStream());
                out.println(postBack.toString());
                out.close();

                if (log.isDebugEnabled()) {log.debug("posted back, awaiting result");}
                //look for return info
                BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                String result = in.readLine();
                in.close();

                //check that payment is verified and plan is valid
                if (result.equals(VERIFIED) && isHGVerified && isValidPlan(itemName)) {
                    //Check that payment status is completed
                    if (COMPLETED.equalsIgnoreCase(paymentStatus) &&
                        !isDupTxnId(txnId) &&
                        isValidPaymentAccount(receiverEmail) &&
                        isMCDataOK(mcGross,mcCurrency,ratesHours,ratesRate)) {                        
                            //allot usage and shelf life accordingly
                            ea.addServices(hours,days);                        
                    } 
                    if (log.isDebugEnabled()) {log.debug(VERIFIED);}

                } else if (result.equals(INVALID)) {
                    log.error(INVALID + " attempt to get usage.  Consider spoofing interogation");                
                } else {
                    log.error("Could not complete validation");
                    log.error("IPN was " + result + " and payer is " + (isHGVerified?"":"not ") + "HelpGuest Verified");
                }
            }//end of else not cmd = _hg_promo
            
            //Finally, record this transaction
            String insertIpnQuery = "insert into ipn (notify_version, " +
                    "verify_sign, first_name, last_name, payer_business_name," + 
                    "payer_email, payer_id, item_name, item_number, " + 
                    "payment_status, payment_date, custom, " +
                    "tax, test_ipn, expert_id, mc_gross, mc_currency, " +
                    "receiver_email, txn_id) values ('" +
                    notifyVersion + "', '" + verifySign + "', '" + firstName + "', '" +
                    lastName + "', '" + payerBusinessName + "', '" + payerEmail + "', '" + 
                    payerId + "', '" + itemName + "', '" + itemNumber + "', '" + 
                    paymentStatus + "', '" + paymentDate + "', '" + custom + "', " +
                    tax + ", " + testIpn + ", " + expertId + ", " + 
                    mcGross + ", '" + mcCurrency + "', '" + receiverEmail + "', '" +
                    txnId + "')";
            DataBase.getInstance().submit(insertIpnQuery);            
        } catch (IOException iox) {
            log.error("Failed to validate post to instant payment notification module (IPN).", iox);
        } catch (Exception ex) {
            log.error("Error processing IPN.", ex);
        }

      
    }
    
    private void debugServlet() {
        if (log.isDebugEnabled()) {
            javax.servlet.ServletContext servletContext = this.getServletContext();
            java.util.Date time = new java.util.Date(System.currentTimeMillis());
            log.debug("ServletContext " + (servletContext==null?"is ":"is not ") + 
                      "null at " + time);
        }
    }
}
