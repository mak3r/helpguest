/*
 * PaymentComplete.java
 *
 * Enable Guest's to connect after payment is completed
 *
 * Created on November 6, 2006, 2:55 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import com.helpguest.data.ExpertAccount;
import com.helpguest.data.ExpertDetails;
import com.helpguest.data.HGSession;
import com.helpguest.data.IPNData;
import com.helpguest.data.Session;
import com.helpguest.htmlgen.Generator;
import com.helpguest.servlet.util.Emailer;
import com.helpguest.servlet.util.JnlpGen;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author mabrams
 */
public class PaymentComplete extends HttpServlet {

    private static final String IS_PROCESSING = "is processing";
    private static final String COMPLETED_SUCCESSFULLY = "completed sucessfully";
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.PaymentComplete");
                    
    
    /** Creates a new instance of Authenticator */
    public PaymentComplete() {
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String sessionId = request.getParameter("session_id");

        PrintWriter out;
                
        try {
            response.setContentType("text/html");
            out = response.getWriter();
            if (sessionId != null) {
                Session session = HGSession.getInstance(sessionId);
                ExpertAccount ea = ExpertAccount.getExpertAccount(session);
                
                //Now do something to verify that we've been paid'
                IPNData ipn = IPNData.getInstance(session, IPNData.COMPLETED);
        
                String paymentStatus = this.IS_PROCESSING;
                if (ipn != null && ipn.isCompleted()) {
                    paymentStatus = this.COMPLETED_SUCCESSFULLY;
                }
                String onOffLine = "<font style=\"color: #FF1111;\">offline</font>";
                if (ea.isLive()) {
                    onOffLine = "<font style=\"color: #11FF11;\">ONLINE</font>";
                }
                
                String[] content = {
                    paymentStatus,
                    sessionId,
                    ea.getHandle(),
                    ea.getHandle(),
                    onOffLine,
                    sessionId,
                    sessionId,
                    ea.getHandle(),
                    ea.getHandle(),
                    ea.getHandle()
                };
                out.print(Generator.generate(this,"/WEB-INF/payment_complete/",content));
            } else {
                out.print("<html><head></head><body><h1>HelpGuest SESSION ID is missing." +
                        "</h1>Please contact support.</body></html>");
            }
        } catch (IOException iox) {
            log.error("Could not generate completed payment page", iox);
        } catch (Exception ex) {
            log.error("Error processing completed payment page.", ex);
        }
      
    }
    
}
