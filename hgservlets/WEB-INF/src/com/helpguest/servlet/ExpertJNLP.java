/*
 * GuestJNLP.java
 *
 * Created on September 14, 2005, 4:57 PM
 */

package com.helpguest.servlet;

import com.helpguest.htmlgen.Generator;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class ExpertJNLP extends HttpServlet {
    
    /** Creates a new instance of GuestJNLP */
    public ExpertJNLP() {
    }
    
public void doGet(HttpServletRequest request,
            HttpServletResponse response) 
            throws IOException, ServletException {
	doPost(request, response);
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) 
                throws IOException, ServletException {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String uid = request.getParameter("uid");
        String hrefVal = "ExpertJNLP?uid=" + uid;
                
        response.setContentType("application/x-java-jnlp-file");
        PrintWriter out = response.getWriter();
        out.print(Generator.generate(this, "WEB-INF/expert_jnlp/part.1", 
                                     hrefVal, 
                                     "WEB-INF/expert_jnlp/part.2",
                                     uid,
                                     "WEB-INF/expert_jnlp/part.3"));
    }
}
