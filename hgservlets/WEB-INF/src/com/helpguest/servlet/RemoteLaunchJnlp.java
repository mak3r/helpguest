package com.helpguest.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.data.LaunchIcon;
import com.helpguest.data.RemoteLaunchOptions;
import com.helpguest.data.ResultHandler;

/**
 * 
 * Copyright 2009 - HelpGuest Technologies, Inc.
 * 
 * @author mabrams
 * 
 * The DeployLaunchIcon is backed by the following data store
 * <code>
CREATE TABLE `remote_launch_options` (
	`id` INTEGER NOT NULL auto_increment,
	`expert_id` INTEGER NOT NULL,
	`post_url` varchar(255) NOT NULL,
	`subtext` varchar(32),
	`launch_icon_id` INTEGER NOT NULL,
	`ref_name` varchar(16) NOT NULL,
	`is_pay_link` int(1) NOT NULL default 0,
	`image_content` BLOB,
	`web_content` TEXT,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`launch_icon_id`) REFERENCES LAUNCH_ICON (`id`)
);
 * </code>
 */

public class RemoteLaunchJnlp extends HttpServlet implements ResultHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(RemoteLaunchJnlp.class);
	
	public void capture(ResultSet rs, int queryId) throws SQLException {
		// TODO Auto-generated method stub

	}

	public int getQueryId() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setQueryId(int id) {
		// TODO Auto-generated method stub

	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    	/*
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        */
        //This has to boil down to the post_url
        String rURI = request.getRequestURI();
        
        //currently we don't pass any parameters
        String someParameter = request.getParameter("some_parameter");

        try {
        	RemoteLaunchOptions rlo = new RemoteLaunchOptions();
	    	String remoteURLContent = rlo.getRemoteURLContent();
	    	BufferedOutputStream bos = null;
	    	OutputStream responseStream = null;
	    	
	    	try {
	        	responseStream = response.getOutputStream();
	        	bos = new BufferedOutputStream(responseStream);
	        	
	        	if (remoteURLContent != null) {
	        		int length = remoteURLContent.length();
		        	response.setContentType("application/x-java-jnlp-file");
		        	response.setContentLength(length);
		        	
		        	bos.write(remoteURLContent.getBytes());
	        	} else {
	        		LOG.warn("webContent not found.");
	        	}
	        	
	            
	        } catch (IOException iox) {
	        	LOG.error("Error handling remote launch jnlp.", iox);
	        } finally {
	            if (bos != null) {
					bos.close();
				}
	            if (responseStream != null) {
					responseStream.flush();
					responseStream.close();
				}
	        } 
        } catch (IOException outerIox) {
        	LOG.error("could not close stream.", outerIox);
        }
      
    }
    

}
