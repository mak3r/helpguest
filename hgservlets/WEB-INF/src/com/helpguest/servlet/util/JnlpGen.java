/*
 * NewClass.java
 *
 * Created on September 15, 2005, 9:45 AM
 * @deprecated use the JnlpGen from hgservice instead com.helpguest.service.JnlpGen
 */

package com.helpguest.servlet.util;

import java.io.File;
import java.io.FileNotFoundException;

import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

import com.helpguest.data.FileSystem;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.htmlgen.Generator;

/**
 *
 * @author mabrams
 */
public class JnlpGen {
    
    private static final String clickWrapVersion = "beta-1.0.2";
    private static final Logger LOG = Logger.getLogger("com.helpguest.servlet.util.jnlpGen");    
    
    /** Creates a new instance of NewClass */
    public JnlpGen() {
    }

    //Write a jnlp files and html files based on an idividual user id and login type
    public static void generate(String uid, String loginType, HttpServlet servlet) {
        //href used for jnlp file
        String jnlpHref = FileSystem.nameJnlpFile(new LoginType(loginType),uid);
        if (LOG.isDebugEnabled()) {
        	LOG.debug("loginType: " + loginType);
        	LOG.debug("jnlpHref: " + jnlpHref);
        }
        //For jnlp app descriptor
        String jnlpPartsDir = "WEB-INF/" + loginType + "_jnlp/";                
        //First create the jnlp application descriptor
        String[] contents = {jnlpHref, uid};
        String jnlpData = "";
        try {
            jnlpData = Generator.generate(servlet,jnlpPartsDir,contents);
        } catch (FileNotFoundException fnx) {
            LOG.error("Could not generate users jnlp data.", fnx);
        }        
        File jnlpFile = new File(FileSystem.fullPath(jnlpHref));
        FileSystem.writeFile(jnlpFile, jnlpData);        

        //href used for clickWrap 
        String clickWrapHref = FileSystem.nameJnlpFile(new LoginType(loginType), uid + "click_wrap");
        //Create the click wrap jnlp
        String clickWrapPartsDir = "WEB-INF/click_wrap/"; 
        String argTags = "</argument><argument>";
        String arguments = loginType + argTags + uid + argTags + clickWrapVersion;
        String[] clickWrapContents = {clickWrapHref, " " + capitalize(loginType) + " ", arguments};
        String clickWrapData = "";
        try {
            clickWrapData = Generator.generate(servlet, clickWrapPartsDir, clickWrapContents);
        } catch (FileNotFoundException fnx) {
            LOG.error("Could not generate clickWrap data.", fnx);
        }
        File clickWrapFile = new File(FileSystem.fullPath(clickWrapHref));
        FileSystem.writeFile(clickWrapFile, clickWrapData);
        
        //For user specific html app
        String htmlPartsDir = "WEB-INF/" + loginType + "_object/";
        //Now create the user specific click wrap html
        String htmlData = "";
        try {
            //If this is an expert, don't use the clickWrapHref
            if (loginType.equalsIgnoreCase("expert")) {
                htmlData = Generator.generate(servlet, htmlPartsDir, jnlpHref);
            } else {
                htmlData = Generator.generate(servlet, htmlPartsDir, clickWrapHref);
            }
        } catch (FileNotFoundException fnx) {
            LOG.error("Could not generate html data.", fnx);
        }
        File htmlFile = new File(FileSystem.fullPath(FileSystem.nameHtmlFile(loginType, uid)));
        FileSystem.writeFile(htmlFile, htmlData);
        
        
    }
    
    private static String capitalize(String s) {
        if (s == null) {
            return s;
        } else if  (s.length() == 1) {
            return s.toUpperCase();
        } else {
            String pre = s.substring(0,1);
            String post = s.substring(1);
            return pre.toUpperCase() + post.toLowerCase();
        }   
    }
    

}
