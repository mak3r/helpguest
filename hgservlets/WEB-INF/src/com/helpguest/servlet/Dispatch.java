/*
 * Dispatch.java
 *
 * Created on June 29, 2006, 5:08 AM
 */

package com.helpguest.servlet;

import com.helpguest.data.ExpertAccount;
import com.helpguest.htmlgen.Generator;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class Dispatch extends HttpServlet {

    public static final String UPDATE_EXPERTISE = "update_expertise";
    public static final String GENERATE_LINKS = "generate_links";
    public static final String AUTHENTICATE = "authenticate";
    public static final String ACCOUNT_DETAILS = "account_details";
    public static final String GUEST_PREVIEW = "guest_preview";
    public static final String COMPLETED_SESSIONS = "completed_sessions";
    public static final String PAYMENT_REQUEST = "payment_request";
    
    public static final Logger log = Logger.getLogger("com.helpguest.servlet.Dispatch");
    
    /**
     * Creates a new instance of Dispatch
     */
    public Dispatch() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String redirect = request.getParameter("_redirect");
        String email = request.getParameter("user");
        String pass = request.getParameter("password");
        
        if (log.isDebugEnabled()) {
            java.util.Enumeration en = request.getParameterNames();
            while (en.hasMoreElements()) {
                String next = (String)en.nextElement();
                log.debug("parameter: " + next + ", value: " + request.getParameter(next));
            }
        }
        
        //Check that the authentication parameters are valid
        response.setContentType("text/html");
        PrintWriter out;
        ExpertAccount ea = ExpertAccount.getExpertAccount(email,pass);
        if (email == null || pass == null || ea == null) {
            String[] loginContents = {"Expert Login", "Your credentials expired. Please login", ""};
            try {
                out = response.getWriter();
                out.print(Generator.generate(this, "WEB-INF/login/", loginContents));
            } catch (FileNotFoundException fnx) {
                log.error("Could not generate login page.", fnx);
            } catch (IOException iox) {
                log.error("could not write page.", iox);
            }
        } else {
            RequestDispatcher dispatcher = null;
            if (log.isDebugEnabled()) {log.debug("redirect is: " + redirect);}
            if (UPDATE_EXPERTISE.equalsIgnoreCase(redirect)) {
                dispatcher = this.getServletContext().getRequestDispatcher("/UpdateExpertise");
            } else if (GENERATE_LINKS.equalsIgnoreCase(redirect)) {
                dispatcher = this.getServletContext().getRequestDispatcher("/ButtonSnippet");
            } else if (AUTHENTICATE.equalsIgnoreCase(redirect)) {
                dispatcher = this.getServletContext().getRequestDispatcher("/Authenticator");
            } else if (ACCOUNT_DETAILS.equalsIgnoreCase(redirect)) {
                dispatcher = this.getServletContext().getRequestDispatcher("/VersionTwoDetails");
            } else if (GUEST_PREVIEW.equalsIgnoreCase(redirect)) {
                dispatcher = this.getServletContext().getRequestDispatcher("/GuestPreview");
            } else if (COMPLETED_SESSIONS.equalsIgnoreCase(redirect)) {
                dispatcher = this.getServletContext().getRequestDispatcher("/CompletedSessions");
            } else if (PAYMENT_REQUEST.equalsIgnoreCase(redirect)) {
                dispatcher = this.getServletContext().getRequestDispatcher("/CompletedSessions");
            }
            try {
                if (dispatcher != null) {
                    dispatcher.forward(request,response);
                }
            } catch (ServletException sx) {
                log.error("could not forward request", sx);
            } catch (IOException iox) {
                log.error("Error trying to i/o data.", iox);
            }
        }
    }
}
