/*
 * IPNDeposit.java
 *
 * Instant Payment Notification for Deposits handler
 *
 * Created on November 6, 2006, 2:55 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.data.ExpertAccount;
import com.helpguest.data.HGSession;
import com.helpguest.data.IPNData;
import com.helpguest.data.Rates;
import com.helpguest.data.Session;
import com.helpguest.gwt.protocol.LoginType.Client;
import com.helpguest.service.JnlpGen;
import com.helpguest.servlet.util.Emailer;

/**
 *
 * @author mabrams
 */
public class IPNDeposit extends HttpServlet {

    private int id;
    private boolean userExists = false;
    private final String INVALID = "INVALID";
    private final String VERIFIED = "VERIFIED";
    
    private static final Logger LOG = Logger.getLogger("com.helpguest.servlet.IPNDeposit");
                    
    
    /** Creates a new instance of Authenticator */
    public IPNDeposit() {
    }
    
    private boolean isValidPaymentAccount(String recipient) {
        if ("sales@helpguest.com".equals(recipient) || 
            "sandbox@helpguest.com".equals(recipient)) {
            return true;
        }
        LOG.warn("invalid account for payment: " + recipient);
        return false;
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String verifySign = request.getParameter("verify_sign");
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String payerBusinessName = request.getParameter("payer_business_name");
        String payerEmail = request.getParameter("payer_email");
        String payerId = request.getParameter("payer_id");
        String itemName = request.getParameter("item_name");
        String itemNumber = request.getParameter("item_number");
        //Custom is the experts uid
        String custom = request.getParameter("custom"); 
        //invoice is the session id
        String invoice = request.getParameter("invoice");
        String tax = request.getParameter("tax");
        String testIpn = request.getParameter("test_ipn");
        String paymentStatus = request.getParameter("payment_status");
        String paymentDate = request.getParameter("payment_date");
        String mcGross = request.getParameter("mc_gross");
        String mcCurrency = request.getParameter("mc_currency");
        String receiverEmail = request.getParameter("receiver_email");
        String txnId = request.getParameter("txn_id");
        String paymentFee = request.getParameter("payment_fee");
        String cmd = request.getParameter("cmd");
        int expertId = -1;
        
        //set true when we know that the identity has been email verified
        boolean isHGVerified = false;
        DecimalFormat df = new DecimalFormat("#,###.00");
        
        Enumeration paramNames = request.getParameterNames();

        PrintWriter out;
        try {
            //Respond to the ipn regardless of whether we know this account identity
            //open a connection with the validation site
            URL url;
            if ("1".equals(testIpn)) {
                url = new URL("https://www.sandbox.paypal.com/cgi-bin/webscr");
            } else {
                url = new URL("https://www.paypal.com/cgi-bin/webscr");
            }
            URLConnection uc = url.openConnection();
            uc.setDoOutput(true);
            uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            //LOG transaction to database
            //Be sure there is a verify sign
            if (verifySign == null) {
                verifySign = String.valueOf(System.currentTimeMillis());
            }

            //Get the session object
            Session session = HGSession.getInstance(invoice);
            ExpertAccount ea = ExpertAccount.getExpertAccount(custom);
            expertId = ea.getId();
            
            Map<String,String> ipnData = new HashMap();

            ipnData.put(IPNData.VERIFY_SIGN, verifySign);
            ipnData.put(IPNData.FIRST_NAME, firstName);
            ipnData.put(IPNData.LAST_NAME, lastName);
            ipnData.put(IPNData.PAYER_BUSINESS_NAME, payerBusinessName);
            ipnData.put(IPNData.PAYER_EMAIL, payerEmail);
            ipnData.put(IPNData.PAYER_ID, payerId);
            ipnData.put(IPNData.ITEM_NAME, itemName);
            ipnData.put(IPNData.ITEM_NUMBER, itemNumber);
            ipnData.put(IPNData.PAYMENT_STATUS, paymentStatus);
            ipnData.put(IPNData.PAYMENT_DATE, paymentDate);
            ipnData.put(IPNData.CUSTOM, custom);
            ipnData.put(IPNData.TAX, tax);
            ipnData.put(IPNData.TEST_IPN, testIpn);
            ipnData.put(IPNData.EXPERT_ID, String.valueOf(expertId));
            ipnData.put(IPNData.MC_GROSS, mcGross);
            ipnData.put(IPNData.MC_CURRENCY, mcCurrency);      
            ipnData.put(IPNData.RECEIVER_EMAIL, receiverEmail);
            ipnData.put(IPNData.TXN_ID, txnId);
            ipnData.put(IPNData.INVOICE, invoice);

            IPNData.createInstance(ipnData);

            //Check if there is a closed session for this transaction
            // if so, process it.  Otherwise do not respond
            // Always LOG the request
            if (session != null && !session.isClosed()) {
                Rates rates = session.getRates();
                if (LOG.isDebugEnabled()) {
                    LOG.debug("rate.deposit: '" + df.format(rates.getDepositAmount()) + "', " +
                            "mcGross: '" + mcGross + "'");
                }
                if (df.format(rates.getDepositAmount()).equals(mcGross)) {
                    //Generate the content to post back
                    StringBuffer postBack = new StringBuffer();
                    postBack.append("cmd=_notify-validate");
                    while (paramNames.hasMoreElements()) {
                        String name = (String)paramNames.nextElement();
                        if (name != null) {
                            postBack.append("&" + name + "=" + URLEncoder.encode(request.getParameter(name),"UTF-8"));
                            //LOG.debug(name + "=" + request.getParameter(name));
                        }
                    }
                    if (LOG.isDebugEnabled()) {LOG.debug("Created postBack = '" + postBack + "'");}
                    //send the data off
                    out = new PrintWriter(uc.getOutputStream());
                    out.println(postBack.toString());
                    out.close();

                    if (LOG.isDebugEnabled()) {LOG.debug("posted back, awaiting result");}
                    //look for return info
                    BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                    String result = in.readLine();
                    in.close();

                    if (LOG.isDebugEnabled()) {LOG.debug("result received: " + result);}

                    //check that payment is verified and plan is valid
                    if (result.equals(VERIFIED)) {
                        //Check that payment status is completed
                        if (IPNData.COMPLETED.equalsIgnoreCase(paymentStatus)) {
                            if (LOG.isDebugEnabled()) {LOG.debug("payment status is completed, generating jnlp related files ...");}
                            //Now generate the jnlp and html files
                            JnlpGen.generateHtml(session.getSessionId(), new Client());
                            JnlpGen.generateJnlp(session.getSessionId(), new Client());
                            
                            //email the payer with the session id and some
                            //info on what to do
                            String subject = "Thanks for using HelpGuest.";
                            String message = "Your paypal payment has completed sucessfully.\r\n" +
                                    "Your TICKET# is " + session.getSessionId() + ".\r\n" + 
                                    "Keep this ticket# in case you need to contact us.\r\n" +
                                    "When " + ea.getHandle() + " has finished helping you, your ticket will expire.";
                            Emailer.sentMessage(subject, message, Emailer.FROM_SUPPORT, payerEmail);
                        } 
                        if (LOG.isDebugEnabled()) {LOG.debug(VERIFIED);}

                    } else if (result.equals(INVALID)) {
                        LOG.error(INVALID + " attempt to get usage.  Consider spoofing interogation");                
                    } else {
                        LOG.error("Could not complete validation");
                        LOG.error("IPN was " + result + " and payer is " + (isHGVerified?"":"not ") + "HelpGuest Verified");
                    }
                }
            } //end if session                  
        } catch (IOException iox) {
            LOG.error("Failed to validate post to instant payment notification module (IPN).", iox);
        } catch (Exception ex) {
            LOG.error("Error processing IPN.", ex);
        }             
    }
    
}
