/*
 * GuestJNLP.java
 *
 * Created on September 14, 2005, 4:57 PM
 */

package com.helpguest.servlet;

import com.helpguest.data.DataBase;
import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class ExpertsGuestList extends HttpServlet implements ResultHandler {

    private Map<String, String> guests;
    private static final int queryId = 0;
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.ExpertsGuestList");
    
    
    /** Creates a new instance of GuestJNLP */
    public ExpertsGuestList() {
    }

    private void getGuests(String uid) {
        String query = "select substring(g.uid,1,6) as pass, g.email " + 
                       "from guest g, expert e where g.expert_id = e.id " + 
                       "and e.uid = '" + uid + "'";
        DataBase.getInstance().submit(query,this);
    }
    
    private String tableizeGuestList() {
        StringBuffer sb = new StringBuffer();
        sb.append("<TABLE  border=1 cellpadding=10><tr><th>User Name</th><th>Password</th></tr>");
        if (guests.size() > 0) {
            for (Map.Entry<String,String> gMap : guests.entrySet()) {
                sb.append("<tr><td>");
                sb.append(gMap.getKey());
                sb.append("<td align=\"center\">");
                sb.append(gMap.getValue());
            }
        }
        sb.append("</Table>");
        return sb.toString();
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request,response);
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
                throws IOException, ServletException {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String uid = request.getParameter("uid");
        getGuests(uid);
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String comingSoon = "<H1>Expert options coming soon</H1>";
        out.print(Generator.generate(this, "WEB-INF/experts_guest_list/", comingSoon));//tableizeGuestList()));
    }
    

    public void capture(java.sql.ResultSet resultSet, int param) throws java.sql.SQLException {
        guests = new HashMap();
        while (resultSet.next()) {
            guests.put(resultSet.getString("email"), resultSet.getString("pass"));
        }
    }

    public void setQueryId(int param) {
    }

    public int getQueryId() {
        return queryId;
    }
    
    
}
