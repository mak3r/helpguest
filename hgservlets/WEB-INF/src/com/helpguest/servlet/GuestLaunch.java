/*
 * GuestLaunch.java
 *
 * Instant Payment Notification for Deposits handler
 *
 * Created on November 9, 2006, 2:55 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Enumeration;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;

/**
 *
 * @author mabrams
 */
public class GuestLaunch extends HttpServlet implements ResultHandler {

    private int queryId;
    private int id;
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.GuestLaunch");
                    
    
    /** Creates a new instance of Authenticator */
    public GuestLaunch() {
    }
    
     public void capture(ResultSet rs, int queryId) {
    }
    
    public int getQueryId() {
        return 0;
    }

    public void setQueryId(int id) {
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        Enumeration paramNames = request.getParameterNames();

        response.setContentType("text/html");
        
        PrintWriter out;        
        StringBuffer buf = new StringBuffer("<html><head></head><body>");
        while (paramNames.hasMoreElements()) {
            String next = (String)paramNames.nextElement();
            buf.append(next + " : " + request.getParameter(next) + "\n");
        }
        buf.append("</body></html>");
        try {
            out = response.getWriter();
            //out.print(buf.toString());
            String[] contents = {};
            out.print(Generator.generate(this,"/WEB-INF/client_launch/",contents));
        } catch (FileNotFoundException fnx) {
            log.error("could not access client_launch parts.", fnx);
        }catch (Exception ex) {
            log.error("failed to generate links.", ex);
        }

      
    }
    
}
