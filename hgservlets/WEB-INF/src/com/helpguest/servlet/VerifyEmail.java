/*
 * VerifyEmail.java
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import com.helpguest.data.DataBase;
import com.helpguest.data.FileSystem;
import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;
import com.helpguest.servlet.util.JnlpGen;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author mabrams
 */
public class VerifyEmail extends HttpServlet implements ResultHandler {

    public enum VerifyEmailOutcome {
        USER_EXISTS ("User exists."),
        UNKNOWN_USER ("Unknown user."),
        INTERNAL_ERROR ("Internal Error.  Failed trying to use result set."),        
        UNKNOWN_ERROR ("Unknown error getting rates. \nPlease contact your HelpGuest representative.");
        
        private String outcome;
        VerifyEmailOutcome(String outcome) {
            this.outcome = outcome;
        }
        
        public String toString() {
            return outcome;
        }
    }
    
    private VerifyEmailOutcome outcome = VerifyEmailOutcome.UNKNOWN_ERROR;
    private int queryId;
    private int id;
    private boolean userExists = false;
    private Collection<String> rates = new ArrayList();
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.VerifyEmail");
                    
    
    /** Creates a new instance of Authenticator */
    public VerifyEmail() {
    }
    
    public void verify(String uid, String email) {
        String userQuery = "select id from expert where uid = '" + uid + 
                           "' and email = '" + email + "'";
        DataBase.getInstance().submit(userQuery, this);
        
        if (userExists) {
            String updateQuery = "update email_verification set is_verified = 1 where expert_id = " + id;
            DataBase.getInstance().submit(updateQuery);
        }
    }

    public void capture(ResultSet rs, int queryId) {
        if (rs == null) {
            log.debug("result set is null");
            outcome = VerifyEmailOutcome.INTERNAL_ERROR;
        } else {
            try {
                if (rs.next()) {
                    userExists = true;
                    id = rs.getInt("id");
                }
            } catch (SQLException sqlx) {
                log.error("Could not verify email.", sqlx);
            }
        }//end of else
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String email = request.getParameter("email");
        String uid = request.getParameter("uid");
        //reset the hasPromoCode value
        userExists = false;
        
        response.setContentType("text/html");
        PrintWriter out;
        try {
            out = response.getWriter();
            verify(uid, email);
            String msg = "Your account has been verified.";
            String[] rateContents = {msg, "", ""};
            out.print(Generator.generate(this, "WEB-INF/login/", rateContents));
        } catch (IOException iox) {
            log.error("could not parse request.", iox);
        }

      
    }
    
    private void debugServlet() {
        if (log.isDebugEnabled()) {
            javax.servlet.ServletContext servletContext = this.getServletContext();
            java.util.Date time = new java.util.Date(System.currentTimeMillis());
            log.debug("ServletContext " + (servletContext==null?"is ":"is not ") + 
                      "null at " + time);
        }
    }
}
