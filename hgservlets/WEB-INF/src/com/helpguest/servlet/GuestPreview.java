/**
 * GuestPreview.java
 *
 * Copyright 2004-2006 - HelpGuest Technologies, Inc.
 * 
 * 
 * @Created on November 8, 2006, 7:28 PM
 * @author Mark Abrams
 */
package com.helpguest.servlet;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.Evaluation;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.HGSession;
import com.helpguest.data.HGRates;
import com.helpguest.data.Rates;
import com.helpguest.data.Session;
import com.helpguest.data.SessionGroup;
import com.helpguest.htmlgen.Generator;
import com.helpguest.servlet.util.JnlpGen;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;

public class GuestPreview extends HttpServlet {
   
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.VersionTwoDetails");
    
    public void doGet(HttpServletRequest request,
            HttpServletResponse response) 
            throws IOException, ServletException {
	doPost(request, response);
    }


    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String email = request.getParameter("user");
        String pass = request.getParameter("password");
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        //Require the password to get the expert account info
        ExpertAccount ea = ExpertAccount.getExpertAccount(email);        
        Rates rates = HGRates.getCurrentInstance(ea);
        Session session = HGSession.newInstance(ea,rates);
        SessionGroup evalGroup = new SessionGroup(ea, true);
        Evaluation eval = new Evaluation(evalGroup);
        
        String[] content = {
            ea.getHandle(),
            String.valueOf(evalGroup.getSessions().size()),
            eval.getFormattedPointPercent(),
            evalGroup.getFormattedAverageSessionTime() + " minutes",
            eval.getFormattedProblemsSolved(),
            eval.getFormattedRepeatCustomers(),
            rates.getFormattedDepositAmount(),
            rates.getFormattedMinFee(),
            String.valueOf(rates.getMinTime()),
            ea.getHandle(),
            rates.getFormattedMinRate(),
            ea.getAvatarLink(),
            ea.getSelfDescription(),            
            ea.getHandle(),
            ea.getUid(),
            session.getSessionId(),
            session.getItemName(),
            session.getItemNumber(),
            rates.getFormattedDepositAmount(),
            HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey()) +
            	"://" + HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_ROOT.getKey()) + 
            	"hgservlets/PaymentComplete?session_id="+session.getSessionId(),
            ea.getAcceptButtonValue(),
            ea.getDoNotAcceptButtonValue(),
            ea.getDoNotAcceptUrlLocation()
        };
        try {
            out.print(Generator.generate(this,"/WEB-INF/client_preview/",content));
        } catch (FileNotFoundException fnx) {
            log.error("could not access client_preview parts.", fnx);
        }catch (Exception ex) {
            log.error("failed to generate links.", ex);
        }

    }
}
