package com.helpguest.servlet;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.data.RemoteLaunchOptions;
import com.helpguest.data.ResultHandler;

/**
 * 
 * Copyright 2009 - HelpGuest Technologies, Inc.
 * 
 * @author mabrams
 * 
 */

public class HelpGuestClient extends HttpServlet implements ResultHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(HelpGuestClient.class);
	
	public void capture(ResultSet rs, int queryId) throws SQLException {
		// TODO Auto-generated method stub

	}

	public int getQueryId() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setQueryId(int id) {
		// TODO Auto-generated method stub

	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    	/*
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        */
        //This has to boil down to the post_url
        String rURI = request.getRequestURI();
        
        //currently we don't pass any parameters
        String someParameter = request.getParameter("some_parameter");
        String postURL = request.getParameter("post_url");
        String refName = request.getParameter("ref_name");
        String id = request.getParameter("id");
        debug(request);
        
        /**
         * use USER_AGENT header value to determine if we should sent the html instead of the jnlp
         */
        //TODO ...


        try {
        	RemoteLaunchOptions rlo = new RemoteLaunchOptions();
	    	String remoteURLContent = rlo.getRemoteURLContent();
	    	BufferedOutputStream bos = null;
	    	OutputStream responseStream = null;
	    	
	    	try {
	        	responseStream = response.getOutputStream();
	        	bos = new BufferedOutputStream(responseStream);
	        	
	        	if (remoteURLContent != null) {
	        		int length = remoteURLContent.length();
		        	response.setContentType("application/x-java-jnlp-file");
		        	response.setContentLength(length);
		        	
		        	bos.write(remoteURLContent.getBytes());
	        	} else {
	        		LOG.warn("webContent not found.");
	        	}
	        	
	            
	        } catch (IOException iox) {
	        	LOG.error("Error handling remote launch jnlp.", iox);
	        } finally {
	            if (bos != null) {
					bos.close();
				}
	            if (responseStream != null) {
					responseStream.flush();
					responseStream.close();
				}
	        } 
        } catch (IOException outerIox) {
        	LOG.error("could not close stream.", outerIox);
        }
      
    }
    
    private void debug(HttpServletRequest request) {
        if (LOG.isDebugEnabled()) {
        	String headerName;
        	Enumeration<String> e = request.getHeaderNames();
        	while(e.hasMoreElements()) {
        		headerName = e.nextElement();
        		LOG.debug(headerName.toUpperCase() + ": " + request.getHeader(headerName));
        	}
	
	        LOG.debug("LOCAL_ADDR: " + request.getLocalAddr());
	        LOG.debug("LOCAL_NAME: " + request.getLocalName());
	        LOG.debug("CONTEXT_PATH: " + request.getContextPath());
	        LOG.debug("REMOTE_ADDR: " + request.getRemoteAddr());
	        LOG.debug("REMOTE_HOST: " + request.getRemoteHost());
	        LOG.debug("SERVER_NAME: " + request.getServerName());
	        LOG.debug("PATH_INFO: " + request.getPathInfo());
	        LOG.debug("SERVLET_PATH: " + request.getServletPath());
	        LOG.debug("QUERY_STRING: " + request.getQueryString());
        }
    }
}
