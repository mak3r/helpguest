/*
 * Top10.java
 *
 * Created on July 17, 2006, 2:55 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import com.helpguest.data.DataBase;
import com.helpguest.data.Expert;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.FileSystem;
import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;
import com.helpguest.protocol.Lexicon;
import com.helpguest.servlet.util.CheckinCheckout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author mabrams
 */
public class Top10 extends HttpServlet implements ResultHandler {

    private int queryId;
    private static final int COMMON = 0;
    private static final int RECENT = 1;
    private static final int RANDOM = 2;
    
    private Collection<String> commonList = null;
    private Collection<String> recentList = null;
    private Collection<String> randomList = null;
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.Top10");

    
    /** Creates a new instance of Authenticator */
    public Top10() {
    }
    

    public void capture(ResultSet rs, int queryId) {
        
        if (rs == null) {
            log.error("No records found.");
        } else {
            try {
                switch (queryId) {
                    case COMMON:
                        commonList = new ArrayList();
                        while (rs.next()) {
                            commonList.add(rs.getString("keyword"));
                        }
                        break;
                    case RECENT:
                        recentList = new ArrayList();
                        while (rs.next()) {
                            recentList.add(rs.getString("keyword"));
                        }
                        break;
                    case RANDOM:
                        randomList = new ArrayList();
                        while (rs.next()) {
                            randomList.add(rs.getString("keyword"));
                        }
                        break;
                    default:
                        break;
                }
            } catch (SQLException sqlx) {
                log.error("could not get data from result set.", sqlx);
            }
        }//end of else
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }
    
    private String getWordBreaks(Collection<String> list) {
        StringBuffer buf = new StringBuffer();
        for (String word : list) {
            buf.append(word + "<br>");
        }
        //remove the last <br>
        buf.delete(buf.length()-4,buf.length());
        return buf.toString();
    }
    
    /**
     * returns the top 10 most
     * recent, most common, and random
     * in that order.
     */
    private String[] getTopTen() {
        setQueryId(COMMON);
        String commonQuery = "select keyword, count(*) as cnt  from searched_words" +
                " where keyword not in (select keyword from ignore_list) " +
                " group by keyword order by cnt desc limit 10";
        DataBase.getInstance().submit(commonQuery,this);
        
        setQueryId(RECENT);
        String recentQuery = "select keyword from searched_words " +
                " where keyword not in (select keyword from ignore_list) " +
                "order by insert_date desc limit 10";
        DataBase.getInstance().submit(recentQuery,this);
        
        setQueryId(RANDOM);
        String randomQuery = "select distinct keyword from searched_words " +
                " where keyword not in (select keyword from ignore_list) " +
                "order by rand() limit 10";
        DataBase.getInstance().submit(randomQuery,this);
        
        String[] content = {
            getWordBreaks(recentList),
            getWordBreaks(commonList),
            getWordBreaks(randomList)
        };
        return content;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request,response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String issue = request.getParameter("issue");
        
        if (issue != null && !issue.trim().equals("")) {
            try {
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/Search2");
                if (dispatcher != null) {
                    dispatcher.forward(request,response);
                }
            } catch (ServletException sx) {
                log.error("could not forward request", sx);
            } catch (IOException iox) {
                log.error("unable to complete forward request.", iox);
            }
        } else {

            response.setContentType("text/html");
            PrintWriter out;
            try {
                out = response.getWriter();
                String[] topTen =  getTopTen();
                out.print(Generator.generate(this, "WEB-INF/main/", topTen));                
            } catch (IOException iox) {
                log.error("could not parse request.", iox);
            }
        }
    }    
}
