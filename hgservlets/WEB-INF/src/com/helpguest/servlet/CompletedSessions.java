/*
 * CompletedSessions.java
 *
 * Created on January 3, 2007, 10:09 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.servlet;

import com.helpguest.SessionAssistant;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.Session;
import com.helpguest.data.SessionGroup;
import com.helpguest.htmlgen.Generator;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

public class CompletedSessions extends HttpServlet {
    
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.CompletedSessions");

    private String getRowInsert(int oe) {
        return  (oe%2==0?"</td><td class=\"even\">":"</td><td class=\"odd\">");
    }
    
    private String getHtmlRows(Collection<Session> sessions) {
        StringBuffer rows = new StringBuffer("");
        int i = 1;
        for (Session next : sessions) {
            SessionAssistant sa = new SessionAssistant(next);
            rows.append(i%2==0?"<tr><td class=\"even\">":"<tr><td class=\"odd\">");
            rows.append(next.getSessionId());
            rows.append(getRowInsert(i));
            rows.append(sdf.format(next.getInsertDate()));
            rows.append(getRowInsert(i));
            rows.append(sa.getDurationInMinutes());
            rows.append(getRowInsert(i));
            rows.append("$"+sa.getFormattedExpertAmount());
            rows.append(getRowInsert(i));
            rows.append("$"+sa.getFormattedHelpGuestAmount());
            rows.append(getRowInsert(i));
            rows.append(next.getPaymentRequestDate()==null?"--":sdf.format(next.getPaymentRequestDate()));
            rows.append(getRowInsert(i));
            rows.append(next.getPaymentSentDate()==null?"--":sdf.format(next.getPaymentSentDate()));
            rows.append("</td></tr>");
            i++;
        }
        return rows.toString();
    }
    
    public void doGet(HttpServletRequest request,
            HttpServletResponse response) 
            throws IOException, ServletException {
	doPost(request, response);
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String email = request.getParameter("user");
        String pass = request.getParameter("password");
        String paypalEmail = request.getParameter("paypal_text_field");
        String paymentRequest = request.getParameter("payment_request");
        
        if (log.isDebugEnabled()) {
            java.util.Enumeration en = request.getParameterNames();
            while (en.hasMoreElements()) {
                String next = (String)en.nextElement();
                log.debug("parameter: " + next + ", value: " + request.getParameter(next));
            }
        }
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        //Require the password to get the expert account info
        ExpertAccount ea = ExpertAccount.getExpertAccount(email,pass);
        if (paypalEmail != null) {
            ea.updatePaypalEmail(paypalEmail);
        }
        
        SessionGroup groupToPay = null;
        if (paymentRequest != null && !paymentRequest.equals("")) {
            //payment has been requested
            groupToPay = new SessionGroup(ea, false);
            groupToPay.requestPaymentOnGroup();
        }
        
        SessionGroup showGroup = new SessionGroup(ea, true);
                
        String[] content = {
            getHtmlRows(showGroup.getSessions()),
            ea.getPaypalEmail()
        };
        try {
            out.print(Generator.generate(this,"/WEB-INF/completed_sessions/",content));
        } catch (FileNotFoundException fnx) {
            log.error("could not access expert_account_details parts.", fnx);
        } catch (Exception ex) {
            log.error("failed to generate links.", ex);
        }

    }
}
