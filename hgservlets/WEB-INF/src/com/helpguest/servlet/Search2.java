/*
 * Search2.java
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.DataBase;
import com.helpguest.data.Evaluation;
import com.helpguest.data.Expert;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.FileSystem;
import com.helpguest.data.Guest;
import com.helpguest.data.ResultHandler;
import com.helpguest.data.SessionGroup;
import com.helpguest.htmlgen.Generator;
import com.helpguest.protocol.Lexicon;
import com.helpguest.servlet.util.CheckinCheckout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author mabrams
 */
public class Search2 extends HttpServlet implements ResultHandler {

    private int queryId;
    private int id;
    private static final int PARADE = 0;
    private static final int ALL_EXPERTS = 1;
    private Map<String,Integer> keywordMap = null;
    private Collection<String> sortedKeys = null;
    private List<Expert> expertList = null;
    private static final int PRIMARY = 1;
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.Search2");
    private static final String FULLY_QUALIFIED_DOMAIN_NAME = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private static final String WEB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey());
    
    /** Creates a new instance of Authenticator */
    public Search2() {
    }
    

    public void capture(ResultSet rs, int queryId) {
        switch (queryId) {
            case PARADE:
                if (rs == null) {
                    log.error("No expertise found to parade.");
                } else {
                    try {
                        keywordMap = new HashMap();
                        sortedKeys = new ArrayList();
                        while (rs.next()) {
                            sortedKeys.add(rs.getString("keyword"));
                            keywordMap.put(rs.getString("keyword"), new Integer(rs.getInt("cnt")));
                        }
                    } catch (SQLException sqlx) {
                        log.error("failed retrieving keywords for parade.", sqlx);
                    }
                }
                break;
                
            case ALL_EXPERTS:
                if (rs == null) {
                    log.error("No records found.");
                } else {
                    try {
                        expertList = new ArrayList();
                        Expert lastExpert = null;
                        while (rs.next()) {                    
                            Expert ex = new Expert(rs.getString("uid"), rs.getString("handle"));
                            if (!ex.equals(lastExpert)) {
                                lastExpert = ex;
                                expertList.add(lastExpert);
                            }

                            lastExpert.addKeyword(rs.getString("keyword"));
                            if (rs.getInt("is_primary") == PRIMARY) {
                                ex.setPrimaryKeyword(rs.getString("keyword"));
                            }
                            //TODO: these values are not yet captured
                            lastExpert.setRating(0);
                            lastExpert.setLive(rs.getInt("is_live"));
                            if (log.isDebugEnabled()) { log.debug("Expert: " + lastExpert); }
                        }
                    } catch (SQLException sqlx) {
                        log.error("could not get data from result set.", sqlx);
                    }
                }//end of else
                break;

            default:
                break;
        }//end switch
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }
    
    private String getConnectData(String uid, String handle) {
        StringBuffer buf = new StringBuffer("\r\n<SCRIPT LANGUAGE=\"JavaScript\" type=\"text/javascript\">\r\n");
        buf.append("<!--\r\n\r\n");
        buf.append("guestConnect(\"" + uid + "\", \"" + handle + "\");\r\n\r\n");
        buf.append("-->");
        buf.append("\r\n");
        buf.append("</SCRIPT>");
        buf.append("\r\n");
        return buf.toString();
    }
    
    private String getExpertParade() {
        queryId = PARADE;
        String paradeQuery = "select keyword, count(keyword) cnt from service_offerings " +
                "group by keyword order by cnt desc";
        DataBase.getInstance().submit(paradeQuery,this);
                
        StringBuffer buf = new StringBuffer();
        buf.append("<div style=\"text-align:left;\">");
        synchronized (sortedKeys) {
            for (String key : sortedKeys) {
                buf.append("<a class=\"keyword\" href=\"javascript:searchOnIssue('&quot;" + key + "&quot;')\">");
                synchronized (keywordMap) {
                    buf.append(key + "&nbsp;(" + keywordMap.get(key) + ")");
                }
                buf.append("</a>");
                buf.append("<br />");
            }
        }
        return buf.toString();
    }
    
    private String getConnectData(ExpertAccount ea) {
        String connect = null;
        if ("2".equals(ea.getWebVersion())) {
            connect = "<a href=\"" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/hgservlets/GuestPreview?user=" +
                    ea.getUid() + "\" class=\"handle\" style=\"text-decoration:none;\" " +
                    ">" + ea.getHandle() + "</a>";
        } else {
            connect = getConnectData(ea.getGuestUid(),ea.getHandle());
        }
        return connect;
    }
    

    /**
     * @Return a div block for an odd listing with status 'online'
     */
    private String getDiv(Expert expert, int index) {
        String state = CheckinCheckout.getState(expert.getLive());
        String handle = expert.getHandle();
        String expertise = expert.getExpertise();
        String rate = expert.getRate();
        ExpertAccount ea = expert.getExpertAccount();
        SessionGroup evalGroup = new SessionGroup(ea,true);
        Evaluation eval = new Evaluation(evalGroup);

        boolean online = true;
        if (Lexicon.ExpertState.OFFLINE.toString().equals(state)) {
            online = false;
        }
        StringBuffer buf = new StringBuffer();
        if (index % 2 == 0) {
            buf.append("<div id=\"even\">");
        } else {
            buf.append("<div id=\"odd\">");
        }
        buf.append("<div id=\"rt\">");
        if (online) {
            buf.append("<label class=\"online\">&nbsp;online now&nbsp;</label>");
        } else {
            buf.append("<label class=\"offline\">&nbsp;&nbsp;&nbsp;offline&nbsp;&nbsp;&nbsp;</label>");
        }
        buf.append("</div>");
        buf.append("<label class=\"expert\">Handle:&nbsp;</label>");
        if (online) {
            buf.append(getConnectData(ea));
        } else {
            buf.append("<label class=\"unlinked_handle\">" + handle + "</label>");
        }
        buf.append("<br>");
        buf.append("Expertise:&nbsp;<label class=\"expertise\">" + expertise + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>");
        buf.append("<label class=\"stock\">Rate:&nbsp;</label>");
        if (Expert.NO_FEE.equals(rate)) {
            buf.append(Expert.NO_FEE + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        } else {
            buf.append(rate + "/minute&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        }
        buf.append("<label class=\"stock\">Completed Sessions:&nbsp;</label><small>");
        buf.append(evalGroup.getSessions().size() + "</small>&nbsp;&nbsp;&nbsp;&nbsp;");
        buf.append("<label class=\"stock\">Satisfaction Rating:&nbsp;</label><small>");
        buf.append(eval.getFormattedPointPercent() + "</small>");
        buf.append("</div>");
        
        return buf.toString();
    }

    private String resultTable(String issue) {
        queryId = ALL_EXPERTS;
        String allExpertQuery = "select so.keyword, le.is_live, g.uid, ex.handle, so.is_primary from " +
                "live_expert le, service_offerings so, guest g, expert ex " +
                "where le.expert_id = ex.id and so.expert_id = ex.id and g.expert_id = ex.id " +
                "and so.keyword in (" + issue + ") order by g.uid";
        DataBase.getInstance().submit(allExpertQuery, this);
        
        StringBuffer buf = new StringBuffer();
        synchronized(expertList) {
            Collections.sort(expertList,Expert.StandardRankingComparator);
            int index = 0;
            for(Expert expert : expertList) {
                buf.append(getDiv(expert, ++index));
            }
        }
        return buf.toString();
    }
    
    /**
     * Return a div with all experts listed
     */
    private String resultTable() {
        String allExpertQuery = "select so.keyword, le.is_live, g.uid, ex.handle, so.is_primary from " +
                "live_expert le, service_offerings so, guest g, expert ex " +
                "where le.expert_id = ex.id and so.expert_id = ex.id and g.expert_id = ex.id ";
        DataBase.getInstance().submit(allExpertQuery, this);
        
        StringBuffer buf = new StringBuffer();
        synchronized(expertList) {
            Collections.sort(expertList,Expert.StandardRankingComparator);
            int index = 1;
            for(Expert expert : expertList) {
                buf.append(getDiv(expert, ++index));
            }
        }
        return buf.toString();
    }
    
    private void addSearchPhrase(String phrase) {
        String phraseQuery = "insert into executed_searches (phrase) value ('" +
                phrase + "') ";
        DataBase.getInstance().submit(phraseQuery);
    }
    
    private String removeIgnoreWords(String phrase) {
        //TODO: remove ignored words
        // rethink this.  Maybe it's unnecessary'
        return phrase;
    }
    
    private List<String> split(String phrase) {
        //Copy out the phrase to a usable form
        StringBuffer phraseBuf = new StringBuffer(phrase);
        //pull quoted text first        
        List<String> keywords = new ArrayList();
        boolean insideQuote = false;        
        int start = 0, end = 0;
        char[] cPhrase = phrase.toCharArray();
        char c;
        for (int i = cPhrase.length-1; i > -1; i--) {
            c = cPhrase[i];
            if (c == '"') {
                if (!insideQuote) {
                    end = i;
                    insideQuote = true;
                } else {
                    start = i;
                    String q = phrase.substring(start+1,end);
                    keywords.add(q);
                    phraseBuf.replace(start,end+1,"");
                    insideQuote = false;
                }                
            }
        }

        //Split the remaining phrase on whitespace
        String[] split = phraseBuf.toString().split("[\\s]+");  
        
        for (String word : split) {
            if (word.trim().length() > 0) {
                keywords.add(word.trim());
            }
        }
        
        return keywords;
    }
    
    private String commatize(final String phrase) {
        List<String> keywords = split(phrase);
        //Now concatenate quoted and single word text
        StringBuffer withCommas = new StringBuffer(256);
        for (String word : keywords) {
            withCommas.append("'" + word.trim() + "',");
        }
        
        //remove the final comma
        withCommas.deleteCharAt(withCommas.lastIndexOf(","));
        return withCommas.toString();
    }
    
    
    private void logKeywords(final String delimited) {
        //We have to add parens around these words with a comma after each
        String[] split = delimited.split(",");
        StringBuffer insertReady = new StringBuffer();
        for (String word : split) {
            insertReady.append("(" + word + "),");
        }
        //remove the final comma
        insertReady.deleteCharAt(insertReady.lastIndexOf(","));
        String keywordQuery = "insert into searched_words (keyword) values " + insertReady;
        DataBase.getInstance().submit(keywordQuery);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request,response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String issue = request.getParameter("issue");
        
        response.setContentType("text/html");
        PrintWriter out;
            
        try {
            out = response.getWriter();
            if (issue != null && !issue.trim().equals("")) {
                addSearchPhrase(issue);
                String modified = removeIgnoreWords(issue);
                modified = commatize(modified);
                logKeywords(modified);
                String result = resultTable(modified);
                if ("".equals(result)) {
                    //no experts found get the expert parade          
                    String[] parade = {"",""};
                    out.print(Generator.generate(this, "WEB-INF/expert_parade/", parade));
                } else {
                    String[] foundResults = {issue, result};
                    out.print(Generator.generate(this, "WEB-INF/search/", foundResults));
                }
            } else {
                String[] parade = {"",""};
                out.print(Generator.generate(this, "WEB-INF/expert_parade/", parade));
            }
        } catch (IOException iox) {
            log.error("could not parse request.", iox);
        }
    }    
}
