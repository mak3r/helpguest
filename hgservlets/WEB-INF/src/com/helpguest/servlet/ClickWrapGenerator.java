/*
 * ClickWrapGenerator.java
 *
 * Created on November 29, 2005, 11:20 AM
 *
 * Copyright 2005 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.data.ExpertAccount;
import com.helpguest.htmlgen.Generator;

/**
 *
 * @author mabrams
 */
public class ClickWrapGenerator extends HttpServlet {

    public enum Type {
        EXPERT {String value() {return "expert";}},
        CLIENT {String value() {return "client";}};
        abstract String value();
    }
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.ClickWrapGenerator");
                    
    
    /** Creates a new instance of Authenticator */
    public ClickWrapGenerator() {
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String type = request.getParameter("type");
        
        response.setContentType("text/rtf");
        PrintWriter out;
        try {
            out = response.getWriter();

            if (Type.EXPERT.value().equalsIgnoreCase(type)) {
                String uid = request.getParameter("uid");
                ExpertAccount ea = ExpertAccount.getExpertAccount(uid);
                String usageAvailable = ea.getPercentUsageAvailable();
                String endDate = ea.getEndDate();
                if (log.isDebugEnabled()) { log.debug("Generating expert_eula."); }
                String[] contents = {usageAvailable, endDate};
                try {
                    out.print(Generator.generate(this, "WEB-INF/expert_eula/", contents));
                } catch (FileNotFoundException fnx) {
                    log.error("Could not generate expert eula.", fnx);
                }
            } else {
                //type must be guest
                if (log.isDebugEnabled()) { log.debug("Generating client_eula."); }
                String[] files = {"WEB-INF/client_eula/part.1"};
                String[] inserts = {};
                out.print(Generator.generate(this, files, inserts));
            }            
        } catch (IOException iox) {
            log.error("could not parse request.", iox);
        } finally {
            //reset uid
        }

      
    }
    
}
