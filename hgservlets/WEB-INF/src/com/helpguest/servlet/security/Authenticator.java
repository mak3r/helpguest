/*
 * Authenticator.java
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2005 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet.security;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.data.DataBase;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.FileSystem;
import com.helpguest.data.HGRates;
import com.helpguest.data.Rates;
import com.helpguest.data.ResultHandler;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.gwt.protocol.LoginType.Client;
import com.helpguest.htmlgen.Generator;
import com.helpguest.servlet.util.JnlpGen;

/**
 *
 * @author mabrams
 */
public class Authenticator extends HttpServlet implements ResultHandler {

    public enum AuthOutcome {
        SUCCESS ("Succes"),
        FAIL_USER_PASS ("Authentication failed.  Unknown email address or password."),
        FAIL_CURRENTLY_CONNECTED ("You or another party are already logged into this account.  \nOnly one login at a time is allowed."),
        FAIL_DUPLICATE ("Authentication failed.  Duplicate identification."),
        INTERNAL_ERROR ("Internal Error.  Failed trying to use result set."),        
        UNKNOWN_ERROR ("Unknown error performing authentication. \nPlease contact your HelpGuest representative.");
        
        private String outcome;
        AuthOutcome(String outcome) {
            this.outcome = outcome;
        }
        
        public String toString() {
            return outcome;
        }
    }  
    
    
    public enum QueryId {
        AUTHENTICATE,
        RETRY_LIMIT;
    }
    
    private AuthOutcome outcome = AuthOutcome.UNKNOWN_ERROR;
    private LoginType loginType;
    private int queryId;
    private String uid; //Can be either the guest's or expert's uid.
    private int expertUid; //used when the login type is guest to id their expert
    private static final int serviceThreshold = 45;
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.security.Authenticator");
                    
    
    /** Creates a new instance of Authenticator */
    public Authenticator() {
    }
    
    public String authenticate(String email, String pass) {
        String query = "select * from " + loginType + " where email = '" + email +
                       "' and pass = PASSWORD('" + pass + "')";
        DataBase.getInstance().submit(query, this);
        
        //TODO: also authenticate by number of login attempts
        // fail if login attempts is > [some arbitrary number]
        // over some period of time (5mins. for example).
        String file = FileSystem.fullPath(FileSystem.nameJnlpFile(loginType,uid));
        if (log.isDebugEnabled()) {log.debug("Checking for existence of file " + file);}
        /*
         * @TODO: 1 possibility is to allow login but provide detailed message
         *  indicating that usage may fail.  2 possibility is to give the 
         *  expert a way of clearing outdated logins. 
         * Do not perform this check at this time.  Failure to use the application
         * after authenticating will lock out the email
         *
        if (fileExists(file)) {
            //This outcome supercedes all other outcomes.
            outcome = AuthOutcome.FAIL_CURRENTLY_CONNECTED;
        }
         */
        
        // Record the outcome
        String resultQuery = "insert into " + loginType + "_authenticator_log" +
               " (outcome, email) values ('" + outcome.toString() + "', '" + email + "')";
        DataBase.getInstance().submit(resultQuery);
        
        return outcome.toString();
    }

    public void capture(ResultSet rs, int queryId) {
        if (rs == null) {
            outcome = AuthOutcome.FAIL_USER_PASS;
        } else {
            if (log.isDebugEnabled()) {log.debug("Counting the rows returned.");}
            //Count the rows returned
            int cnt = 0;
            try {
                while (rs.next()) {
                    cnt++;
                }
            if (log.isDebugEnabled()) {log.debug( cnt + " rows returned.");}
                
            } catch (java.sql.SQLException sqlx) {
                log.error("Could not iterate result set.", sqlx);
                outcome = AuthOutcome.INTERNAL_ERROR;
            }
            if (cnt == 1) {
                try {
                    //reset the result set
                    rs.beforeFirst();
                    rs.next();
                } catch (SQLException sqlx) {
                    log.error("Error attempting to reset result set cursor to first row.",sqlx);
                }
                //count is one, it's an exact match
                //The db should never allow more than one
                outcome = AuthOutcome.SUCCESS;
                try {
                    uid = rs.getString("uid");
                    if ("null".equals(uid)) {
                        outcome = AuthOutcome.INTERNAL_ERROR;
                        log.error("uid of null is not allowed!");
                        return;
                    }
                    if (loginType.equals(new Client())) {
                        expertUid = rs.getInt("expert_id");
                    }
                } catch (SQLException sqlx) {
                    log.error("Could not get data from result set.",sqlx);
                    outcome = AuthOutcome.INTERNAL_ERROR;
                }
            } else if (cnt > 1) {
                log.error("database error. duplicate email addresses");
                outcome = AuthOutcome.FAIL_DUPLICATE;
            } else {
                outcome = AuthOutcome.FAIL_USER_PASS;
            }
        }
    }
    
    /**
     * @param fileName is the full path to the file 
     * @return true if the file is found on the file system
     * or false if it does not exist.
     */
    protected boolean fileExists(String fileName) {
        File file = new File(fileName);
        return file.exists();
    }

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }
    
    private String getExpertise(ExpertAccount ea) {
        List<String> skills = ea.getExpertise();
        StringBuffer expertise = new StringBuffer();
        for (String skill : skills) {
            expertise.append(skill + ", ");
        }
        //emphasize the first word
        expertise.insert(0,"<em>");
        expertise.insert(expertise.indexOf(","),"</em>");
        // remove the last comma
        expertise.deleteCharAt(expertise.lastIndexOf(","));
        return expertise.toString();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String type = request.getParameter("login_type");
        String email = request.getParameter("user");
        String pass = request.getParameter("password");
        
        response.setContentType("text/html");
        PrintWriter out;
        try {
            out = response.getWriter();

            //Identify the type before calling authenticate
            if (type == null) {
                String message = "Login type unspecified.  Cannot continue";
                log.error(message);
                out.print(message);
                loginType = new LoginType.Unknown();
            } else {
            	loginType = new LoginType(type);
            }

            if (log.isDebugEnabled()) {log.debug("using login type: " + loginType);}
            authenticate(email, pass);
            debugServlet();
            if (AuthOutcome.SUCCESS == outcome) {
                //Go here
                if (loginType.equals(new LoginType.Expert())) {
                    if (log.isDebugEnabled()) { log.debug("Writing expert specific jnlp file."); }
                    JnlpGen.generate(uid, loginType.toString(), this);
                    ExpertAccount ea = ExpertAccount.getExpertAccount(uid);
                    String handle = ea.getHandle();
                    String expertise = getExpertise(ea);
                    String usageAvailable = ea.getUsageAvailable();
                    String shelfLife = ea.getShelfLife();
                    String guestsWaiting = ea.getGuestsWaiting();
                    String numGuests = String.valueOf(ea.getNumberGuestsWaiting());
                    String buyMore = "";
                    if (ea.canGetMoreService(serviceThreshold)) {
                        buyMore = "<span class=\"link\"><a href=\"https://secure.helpguest.com/hgservlets/RatePlans?uid=" + ea.getUid() + "\">Add service</a></span>";
                    }
                    
                    //Get the current rate instance or create a new one
                    Rates rates = HGRates.getCurrentInstance(ea);
                    if (rates==null) {
                        rates = HGRates.newInstance(ea, HGRates.MIN_RATE, HGRates.BASE_TIME);
                    }
                    
                    if (log.isDebugEnabled()) { log.debug("Generating expert_connect."); }
                    
                    String[] connectContents = {email, uid, handle, expertise.toString(), numGuests, usageAvailable, shelfLife, buyMore};
                    String[] noConnectContents = {email, "", handle, expertise.toString(), numGuests, usageAvailable, shelfLife, buyMore};
                    String[] notVerifiedContents = {handle};
                    String[] accountDetailsContents = {
                        email,
                        uid, 
                        handle, 
                        expertise, 
                        numGuests, 
                        rates.getFormattedMinRate(),
                        String.valueOf(rates.getMinTime()),
                        String.valueOf(rates.getPreferredTime()),
                        rates.getFormattedDepositAmount(),
                        ea.getSelfDescription(),
                        ea.getAvatarLink()
                    };
                    
                    try {
                        if (!ea.isVerified()) {
                            out.print(Generator.generate(this, "WEB-INF/expert_not_verified/", notVerifiedContents));
                        } else {
                            if (log.isDebugEnabled()){log.debug("Expert is verified check version, then connect contents.");}
                            if ("2".equals(ea.getWebVersion().trim())) {
                                out.print(Generator.generate(this, "WEB-INF/expert_account_details/", accountDetailsContents));
                            } else if (ExpertAccount.EXPIRED.equals(shelfLife) || 
                                ExpertAccount.NONE.equals(usageAvailable)) {
                                out.print(Generator.generate(this, "WEB-INF/expert_no_connect/", noConnectContents));
                            } else {
                                out.print(Generator.generate(this, "WEB-INF/expert_connect/", connectContents));
                            }
                            if (log.isDebugEnabled()){log.debug("Expert is verified check version, then connect contents.");}
                            //Now generate a the jnlp, html, and click-wrap jnlp files
                            String guestUid = ea.getGuestUid();
                            if (log.isDebugEnabled()){log.debug("Generating jnlp for guest: " + guestUid);}                                
                            JnlpGen.generate(guestUid, new Client().toString(), this); 
                        }
                    } catch (FileNotFoundException fnx) {
                        log.error("Could not generate expert_connect file", fnx);
                    }
                } else if (loginType.equals(new Client())) {
                    if (log.isDebugEnabled()) { log.debug("Writing guest specific jnlp file."); }
                    JnlpGen.generate(uid, loginType.toString(), this);
                    if (log.isDebugEnabled()) { log.debug("Generating client_connect."); }
                    String[] guestContents = {uid, uid};
                    try {
                        out.print(Generator.generate(this, "WEB-INF/client_connect/", guestContents));
                    } catch (FileNotFoundException fnx) {
                        log.error("Could not generate client_connect file", fnx);
                    }
                };
            } else {
                //retry login
                String message = outcome.toString();
                String[] loginContents = {"Expert Login - ERROR", message, ""};
                try {
                    out.print(Generator.generate(this, "WEB-INF/login/", loginContents));
                } catch (FileNotFoundException fnx) {
                    log.error("Could not generate login file", fnx);
                }
            }
        } catch (IOException iox) {
            log.error("could not parse request.", iox);
        } finally {
            //reset uid
            uid = null;
        }

      
    }
    
    private String capitalize(String s) {
        if (s == null) {
            return s;
        } else if  (s.length() == 1) {
            return s.toUpperCase();
        } else {
            String pre = s.substring(0,1);
            String post = s.substring(1);
            return pre.toUpperCase() + post.toLowerCase();
        }
        
        
    }
    
    private void debugServlet() {
        if (log.isDebugEnabled()) {
            javax.servlet.ServletContext servletContext = this.getServletContext();
            java.util.Date time = new java.util.Date(System.currentTimeMillis());
            log.debug("ServletContext " + (servletContext==null?"is ":"is not ") + 
                      "null at " + time);
        }
    }
}
