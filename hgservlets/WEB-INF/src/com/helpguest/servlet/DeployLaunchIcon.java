package com.helpguest.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.data.LaunchIcon;

/**
 * 
 * Copyright 2009 - HelpGuest Technologies, Inc.
 * 
 * @author mabrams
 * 
 */

public class DeployLaunchIcon extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(DeployLaunchIcon.class);

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    	/*
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        */
        //This has to boil down to the post_url
        String rURI = request.getRequestURI();
        
        //default to id 1
        int baseImageId = 1;
        try {
        	baseImageId = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException nfx) {
        	//default is 1
        }

        LOG.debug("HOST: " + request.getHeader("host"));
        LOG.debug("REFERRER: " + request.getHeader("referrer"));
        try {
	    	LaunchIcon launchIcon = new LaunchIcon();
	    	Blob imageBlob = launchIcon.getImageBlob(baseImageId);
	    	BufferedInputStream bis = null;
	    	BufferedOutputStream bos = null;
	    	OutputStream responseStream = null;
	        try {
	        	
	        	if (imageBlob != null) {
	        		responseStream = response.getOutputStream();
		        	bis = new BufferedInputStream(imageBlob.getBinaryStream());
		        	bos = new BufferedOutputStream(responseStream);
		
		        	int length = (int)imageBlob.length();
		        	response.setContentType("image/x-png");
		        	response.setContentLength(length);
		        	
		        	byte[] buff = new byte[length];
		            int bytesRead;
		            // Simple read/write loop.
		            while(-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
		              bos.write(buff, 0, bytesRead);
		            }
	        	} else {
	        		LOG.warn("Image not found.");
	        	}
	        	
	            
	        } catch (IOException iox) {
	        	LOG.error("Error handling deploy launch icon.", iox);
	        } catch (SQLException sqlx) {
	        	LOG.error("Error retrieving deploy launch icon.", sqlx);
	        } finally {
	            if( bis != null ) {
	                bis.close();
	              }
	              if( bos != null ) {
	                bos.close();
	              }
	              if( responseStream != null ) {
	            	  responseStream.flush();
	            	  responseStream.close();
	              }
	        } 
        } catch (IOException outerIox) {
        	LOG.error("could not close stream.", outerIox);
        }
      
    }

}
