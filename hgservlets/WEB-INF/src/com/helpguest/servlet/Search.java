/*
 * Search.java
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.DataBase;
import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;
import com.helpguest.protocol.Lexicon;
import com.helpguest.servlet.util.CheckinCheckout;

/**
 *
 * @author mabrams
 */
public class Search extends HttpServlet implements ResultHandler {

    private int queryId;
    private int id;
    private static Collection<String> rowContent = null;
    private static Collection<Collection> rows = null;
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.Search");
    private static final String FULLY_QUALIFIED_DOMAIN_NAME = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private static final String JAWS_APPS_DIR = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.JAWS_APPS_DIR.getKey());
    private static final String WEB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey());
    
    /** Creates a new instance of Authenticator */
    public Search() {
    }
    

    public void capture(ResultSet rs, int queryId) {
        if (rs == null) {
            log.error("No records found.");
        } else {
            try {
                rows = new ArrayList();
                while (rs.next()) {
                    rowContent = new ArrayList();
                    rowContent.add(CheckinCheckout.getState(rs.getInt("is_live")));
                    rowContent.add(rs.getString("uid"));
                    rowContent.add(rs.getString("handle"));
                    rowContent.add(rs.getString("keyword"));
                    rows.add(rowContent);
                }
            } catch (SQLException sqlx) {
                log.error("could not get data from result set.", sqlx);
            }
        }//end of else
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }
    
    private String getConnectData(String uid, String handle) {
        StringBuffer buf = new StringBuffer("<SCRIPT LANGUAGE=\"JavaScript\" type=\"text/javascript\">");
                buf.append("\r\n");
                buf.append("<!--");
                buf.append("\r\n");
                buf.append("/* Note that the logic below always launches the JNLP application");
                buf.append("\r\n");
                buf.append("*if the browser is Gecko based. This is because it is not possible");
                buf.append("\r\n");
                buf.append("*to detect MIME type application/x-java-jnlp-file on Gecko-based browsers.");
                buf.append("\r\n");
                buf.append("*/");
                buf.append("\r\n");
                buf.append("if (javaws12Installed || (navigator.userAgent.indexOf(\"Gecko\") !=-1)) {");
                buf.append("\r\n");
                buf.append("document.write(\"<a href=\\\"" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + JAWS_APPS_DIR + "/client/");

                buf.append(uid);
                
                buf.append("click_wrap.jnlp\\\">\");");
        buf.append("\r\n");
                        buf.append("document.write(\"" + handle + "\");");
                buf.append("\r\n");
                buf.append("document.write(\"</a>\");");
                buf.append("\r\n");
                buf.append("} else {");
                buf.append("\r\n");
                buf.append("document.write(\"<a href=http://java.sun.com/PluginBrowserCheck?pass=" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + JAWS_APPS_DIR + "/client/");
                
                buf.append(uid);
                
                buf.append(".html&fail=http://java.sun.com/j2se/1.5.0/download.html>\");");
                buf.append("\r\n");
                buf.append("document.write(\"" + handle + "\");");
                buf.append("\r\n");
                buf.append("document.write(\"</a>\");");
                buf.append("\r\n");
                buf.append("}");
                buf.append("\r\n");
                buf.append("-->");
                buf.append("\r\n");
                buf.append("</SCRIPT>");
        buf.append("\r\n");
                return buf.toString();
    }

    private String asRows(String state, String uid, String handle, String expertise) {
        StringBuffer row = new StringBuffer();
        if (Lexicon.ExpertState.OFFLINE.toString().equals(state)) {
            //make it red
            row.append("<tr><td align=center><font style=\"color: #FF0000\">" + state + "</font>");
        } else {
            //make it green
            row.append("<tr><td align=center><font style=\"color: #009900\">" + state + "</font>");            
        }
        row.append("<td align=center>" + getConnectData(uid,handle));
        row.append("<td align=center>" + expertise);
        row.append("<td align=center><small>coming soon</small>");
        row.append("<td align=center><small>coming soon</small>");
        return row.toString();
    }
    
    private String resultTable(String issue) {
        String allExpertQuery = "select keyword, is_live, g.uid, handle from " +
                "live_expert le, service_offerings se, guest g, expert ex " +
                "where le.expert_id = ex.id and se.expert_id = ex.id and g.expert_id = ex.id " + 
                "order by is_live desc";
        DataBase.getInstance().submit(allExpertQuery, this);
        
        StringBuffer buf = new StringBuffer();
        for(Collection<String> row : rows) {
            String[] contents = (String[])row.toArray(new String[0]);
            buf.append(asRows(contents[0],contents[1],contents[2],contents[3]));
        }
        return buf.toString();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request,response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String issue = request.getParameter("issue");

        response.setContentType("text/html");
        PrintWriter out;
        try {
            out = response.getWriter();
            out.print(Generator.generate(this, "WEB-INF/search/", resultTable(issue)));
        } catch (IOException iox) {
            log.error("could not parse request.", iox);
        }
    }
    
}
