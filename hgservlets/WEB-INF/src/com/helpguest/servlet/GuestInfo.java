/*
 * GuestInfo.java
 *
 * Created on January 9, 2005, 11:37 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.servlet;

import com.helpguest.HelpGuestException;
import com.helpguest.data.DataBase;
import java.io.*;
import java.net.*;
import java.util.Enumeration;

import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 *
 * @author  mabrams
 * @version
 */
public class GuestInfo extends HttpServlet {
    
    Logger log = Logger.getLogger(GuestInfo.class);
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        BasicConfigurator.configure();
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Inquiry Received</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<H1> Thanks for inquiring about HelpGuest. </H1>");
        out.println("<p><H2> We'll get back to you as soon as possible</H2></p>");
        out.println("</body>");
        out.println("</html>");
        out.close();
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String[] columns = {"first", "last", "phone", "phone2", "email", "company", "title",
                            "address1", "address2", "city", "state", "zip"};
        String[] values = new String[12];
        
        values[0] = request.getParameter("first");
        values[1] = request.getParameter("last");
        values[2] = request.getParameter("phone");
        values[3] = request.getParameter("phone2");
        values[4] = request.getParameter("email");
        values[5] = request.getParameter("company");
        values[6] = request.getParameter("title");
        values[7] = request.getParameter("address1");
        values[8] = request.getParameter("address2");
        values[9] = request.getParameter("city");
        values[10] = request.getParameter("state");
        values[11] = request.getParameter("zip");

        try {
            String query = DataBase.buildInsert("inquiry", columns, values);
            DataBase.getInstance().submit(query);
        } catch (HelpGuestException hgx) {
            log.error("unable to insert data into database. " +
                      "Could not form sql statement", hgx );
        }
        processRequest(request, response);
    }

    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
