/*
 * UpdateExpertise.java
 *
 * Created on June 29, 2006, 5:08 AM
 */

package com.helpguest.servlet;

import com.helpguest.data.ExpertAccount;
import com.helpguest.htmlgen.Generator;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class UpdateExpertise extends HttpServlet {

    public static final String REMOVE = "remove";
    public static final String ADD = "add";
    public static final String PRIMARY = "primary";
    
    public static final Logger log = Logger.getLogger("com.helpguest.servlet.UpdateExpertise");
    
    /** Creates a new instance of UpdateExpertise */
    public UpdateExpertise() {
    }

    //Turn a comma separated list into an array list
    private ArrayList<String> getExpertiseList(String commaList) {
        ArrayList<String> expertiseList = new ArrayList();
        String[] stringList = commaList.split(",");        
        for (String next : stringList) {
            if (next.trim().length()>0) {
                expertiseList.add(next.trim());
            }
        }        
        return expertiseList;
    }
    
    //Generate the html to represent the expertise list
    private String getDisplayList(ArrayList<String> expertise) {
        StringBuffer toDisplay = new StringBuffer();
        //The first item is different than the rest
        toDisplay.append("<font class=\"primary\" id=\"" + 
                expertise.get(0) + "\" onclick=\"addIn(this.id)\">" + 
                expertise.get(0) + ",&nbsp;</font>");
        //now pop the primary and all the rest are the same
        expertise.remove(expertise.get(0));
        for(String next : expertise) {
            toDisplay.append("<font class=\"expertise\" id=\"" + 
                    next + "\" onclick=\"addIn(this.id)\">" + 
                    next + ",&nbsp;</font>");            
        }
        //remove the last comma
        toDisplay.deleteCharAt(toDisplay.lastIndexOf(","));

        return toDisplay.toString();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String email = request.getParameter("user");
        String pass = request.getParameter("password");
        String mode = request.getParameter("mode");
        String updateList = request.getParameter("update_list");

        if (log.isDebugEnabled()) {
            log.debug("email: " + email);
            log.debug("pass: " + pass);
            log.debug("mode: " + mode);
            log.debug("updateList: " + updateList);
        }
        
        response.setContentType("text/html");
        PrintWriter out;
        try {
            //First off get the expert account object
            ExpertAccount ea = ExpertAccount.getExpertAccount(email,pass);
                
            //Don't try anything if updateList is null or empty
            if (updateList != null && updateList.length() > 0) {
                if (PRIMARY.equalsIgnoreCase(mode)) {
                    String primary = getExpertiseList(updateList).get(0);
                    ea.updatePrimaryExpertise(primary);
                } else if (REMOVE.equalsIgnoreCase(mode)) {
                    ea.removeExpertise(getExpertiseList(updateList));
                } else if (ADD.equalsIgnoreCase(mode)) {
                    ea.addExpertise(getExpertiseList(updateList));
                } 
            }
            
            //Now, regardless of the mode, get the latest expertise list and 
            // display it
            out = response.getWriter();
            out.print(Generator.generate(this, "WEB-INF/update_expertise/", getDisplayList(ea.getExpertise())));

        } catch (FileNotFoundException fnx) {
            log.error("Could not display expertise page.", fnx);
        } catch (IOException iox) {
            log.error("Page generation problem.", iox);
        }
    }
}
