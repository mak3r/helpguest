/*
 * RatePlans.java
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2005 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import com.helpguest.data.DataBase;
import com.helpguest.data.FileSystem;
import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;
import com.helpguest.servlet.util.JnlpGen;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author mabrams
 */
public class RatePlans extends HttpServlet implements ResultHandler {

    public enum RatePlansOutcome {
        INTERNAL_ERROR ("Internal Error.  Failed trying to use result set."),        
        UNKNOWN_ERROR ("Unknown error getting rates. \nPlease contact your HelpGuest representative.");
        
        private String outcome;
        RatePlansOutcome(String outcome) {
            this.outcome = outcome;
        }
        
        public String toString() {
            return outcome;
        }
    }
    
    private final int RATES = 0;
    private final int VALIDATE = 1;
    private final int PROMO = 2;
    
    private RatePlansOutcome outcome = RatePlansOutcome.UNKNOWN_ERROR;
    private int queryId;
    private boolean hasPromoCode = false;
    private Collection<String> rates = new ArrayList();
    private String ratePlanName = null;
    private String promoCode = null;
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.RatePlans");
                    
    
    /** Creates a new instance of Authenticator */
    public RatePlans() {
    }
    
    public void getRatePlan(String code) {
        promoCode = code;
        queryId = PROMO;
        String promoQuery = "select rate_code from rates where id in (" +
                            "select rates_id from promo where name = '" +
                            code + "' and expired = 0)";
        DataBase.getInstance().submit(promoQuery,this);
        
        queryId = RATES;
        String ratesQuery = "select rate_code, daily_rate, usage_in_hours, shelf_life_in_days, is_standard " + 
                            "from rates where (is_valid = 1 && is_standard = 1) or (is_valid = 1 and rate_code = '" +
                            ratePlanName + "') order by usage_in_hours, daily_rate, shelf_life_in_days";                                
        DataBase.getInstance().submit(ratesQuery, this);
    }

    public void capture(ResultSet rs, int queryId) {
        if (rs == null) {
            log.debug("result set is null");
            outcome = RatePlansOutcome.INTERNAL_ERROR;
        } else {
            switch (queryId) {
                case RATES:
                    try {
                        //clear out the collection of rates
                        rates.clear();
                        while (rs.next()) {
                            String cost = calculateCost(rs.getFloat("daily_rate"), rs.getInt("usage_in_hours"));
                            StringBuffer row = new StringBuffer("<tr><td><input type=radio name=\"rates\" tabindex=");
                            row.append(rs.getRow() + " ");
                            //mark it checked if it is not standard
                            if (hasPromoCode && rs.getInt("is_standard") != 1) {
                                row.append("checked ");                    
                            } else if (!hasPromoCode && rs.getRow() == 3) {
                                //if there is no promo code, mark the third one
                                row.append("checked ");
                            }
                            String itemName = rs.getString("rate_code");
                            String itemNumber = rs.getInt("usage_in_hours") + "-" + rs.getInt("shelf_life_in_days");
                            row.append("value=\"" + itemName + ":" +
                                                    cost.substring(1) + ":" + 
                                                    itemNumber);
                            if (itemName.equals(ratePlanName)) {
                                row.append(":" + promoCode + "\">");
                            } else {
                                row.append(":" + itemName + "\">");
                            }
                            row.append("<td align=center>" + rs.getInt("usage_in_hours") + " hours");
                            row.append("<td align=center>" + rs.getInt("shelf_life_in_days") + " days");
                            row.append("<td align=right>" + cost);                
                            rates.add(row.toString());
                        }
                    } catch (SQLException sqlx) {
                        //This error is thrown if there is a problem accessing the database
                        log.error("Data base access error.", sqlx);
                        outcome = RatePlansOutcome.INTERNAL_ERROR;
                    }
                    break;
                case PROMO:
                    try {
                        if (rs.next()) {
                            ratePlanName = rs.getString("rate_code");
                            hasPromoCode = true;
                        } else {
                            hasPromoCode = false;
                        }
                    } catch (SQLException sqlx) {
                        log.error("No promo exists under that name.", sqlx);
                    }
                    break;
                default:
                    break;
            }
        }//end of else
    }
    
    private String calculateCost(float rate, int hours) {
        return NumberFormat.getCurrencyInstance().format(rate*(hours/8));
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request,response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String code = request.getParameter("promo");
        String uid = request.getParameter("uid");
        //reset the hasPromoCode value
        hasPromoCode = false;
        ratePlanName = null;
        
        response.setContentType("text/html");
        PrintWriter out;
        try {
            out = response.getWriter();
            getRatePlan(code);
            StringBuffer rateTableData = new StringBuffer();
            for (String rate : rates) {
                rateTableData.append(rate);
            }            
            String[] rateContents = {uid, (code==null?"":code), uid, rateTableData.toString()};
            out.print(Generator.generate(this, "WEB-INF/rate_plans/", rateContents));
        } catch (IOException iox) {
            log.error("could not parse request.", iox);
        }

      
    }
    
    private void debugServlet() {
        if (log.isDebugEnabled()) {
            javax.servlet.ServletContext servletContext = this.getServletContext();
            java.util.Date time = new java.util.Date(System.currentTimeMillis());
            log.debug("ServletContext " + (servletContext==null?"is ":"is not ") + 
                      "null at " + time);
        }
    }
}
