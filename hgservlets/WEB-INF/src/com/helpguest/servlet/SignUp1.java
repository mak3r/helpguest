/*
 * SignUp1.java
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2005 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import com.helpguest.data.DataBase;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.FileSystem;
import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;
import com.helpguest.servlet.util.JnlpGen;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;
import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author mabrams
 */
public class SignUp1 extends HttpServlet implements ResultHandler {

    public enum SignUpOutcome {
        SUCCESS ("Account Created."),
        FAIL_ACCOUNT_EXISTS ("Sign up failed.  Account e-mail address already in use."),
        FAIL_HANDLE_EXISTS ("Sign up failed.  Handle is already in use."),
        INTERNAL_ERROR ("Internal Error.  Failed trying to use result set."),        
        UNKNOWN_ERROR ("Unknown error performing sign up. \nPlease contact your HelpGuest representative.");
        
        private String outcome;
        SignUpOutcome(String outcome) {
            this.outcome = outcome;
        }
        
        public String toString() {
            return outcome;
        }
    }  
    
    private final int EMAIL = 0;
    private final int HANDLE = 1;
    
    private SignUpOutcome outcome = SignUpOutcome.UNKNOWN_ERROR;
    private int queryId;
    private String uid; //experts uid
    private int ERROR_COUNT = 0;
    private final int formHeight = 22; //measured in em
    private Properties props = System.getProperties();
    private static Random rand = new Random(System.currentTimeMillis());

    private static final Logger log = Logger.getLogger("com.helpguest.servlet.SignUp1");
                    
    
    /** Creates a new instance of Authenticator */
    public SignUp1() {
        props.put("mail.smtp.host", "smtp.helpguest.com");
        //required for smtp auth
        props.put("mail.smtp.auth", "true");
    }
    
    public void signUp(String email, String pass, String handle, String offering, String zip, String userIP) {
        if (SignUpOutcome.SUCCESS == outcome) {
            //insert the main expert record
            ExpertAccount ea = ExpertAccount.newInstance(email,pass,handle,offering,zip,userIP);

            //TODO: some of the following should be added the the creation of a new expert account
            //Insert a record for email validation
            String insertValidationQuery = "insert into email_verification (expert_id, is_verified) " + 
                                           "select id, 0 from expert where email = '" + email + "'";
            DataBase.getInstance().submit(insertValidationQuery);
            
            //Make sure inserted date is GMT
            GregorianCalendar now = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date cur = new Date(now.getTimeInMillis());
            String dbDate = sdf.format(cur);
            String insertAccountStatsQuery = "insert into expert_account_status (expert_id, end_date) " +
                                             "select id, '" + dbDate + "' from expert where email = '" + email + "'";
            DataBase.getInstance().submit(insertAccountStatsQuery);
            
            //Insert an entry for handling when the expert goes live
            String liveExpertQuery = "insert into live_expert (expert_id, is_live) " +
                    "select id, 0 from expert where email = '" + email + "'";
            DataBase.getInstance().submit(liveExpertQuery);
            
            //Create guest components that will be able to connect with this expert
            //Create a guest uid 
            String guestUid = Long.toString(System.currentTimeMillis());
            //insert the guest record
            String guestInsertQuery = "insert into guest (expert_id, uid) " +
                    "select id, '" + guestUid + "' from expert where email = '" + email + "'";
            DataBase.getInstance().submit(guestInsertQuery);

            //insert the demographics record
            String demographicsQuery = "insert into demographics(email, handle, " +
                    "expertise, zip_code, user_ip) values ('" + email + "', '" +
                    handle + "', '" + offering + "', '" + zip + "', '" + 
                    userIP + "')";
            DataBase.getInstance().submit(demographicsQuery);
        }
    }

    public void capture(ResultSet rs, int queryId) {
        if (rs == null) {
            log.debug("result set is null");
            outcome = SignUpOutcome.INTERNAL_ERROR;
        } else {
            switch (queryId) {
                case EMAIL:
                    try {
                        if (!rs.next()) {
                            //if there are no rows, the account does not exist.
                            // that's good.  We can create it.
                            outcome = SignUpOutcome.SUCCESS;
                        } else {
                            outcome = SignUpOutcome.FAIL_ACCOUNT_EXISTS;
                        }
                    } catch (SQLException sqlx) {
                        //This error is thrown if there is a problem accessing the database
                        log.error("Data base access error.", sqlx);
                        outcome = SignUpOutcome.INTERNAL_ERROR;
                    }
                    break;
                case HANDLE:
                    try {
                        if (!rs.next()) {
                            outcome = SignUpOutcome.SUCCESS;
                        } else {
                            outcome = SignUpOutcome.FAIL_HANDLE_EXISTS;
                        }
                    } catch (SQLException sqlx) {
                        outcome = SignUpOutcome.INTERNAL_ERROR;
                    }
                    break;
                default:
                    break;
            }
        }//end of else
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        String userIP = request.getRemoteAddr();
        
        String email = request.getParameter("email");
        String pass = request.getParameter("password");
        String conf_pass = request.getParameter("conf_password");
        String handle = request.getParameter("handle");
        String offering = request.getParameter("offering");
        String zip = request.getParameter("zip");
        
        response.setContentType("text/html");
        PrintWriter out;
        try {
            out = response.getWriter();
            String errors = validate(email, pass, conf_pass, handle, offering, zip);
            if (null != errors) {
                String[] retryContent = {String.valueOf(formHeight+(ERROR_COUNT*1.2)), 
                                         String.valueOf(ERROR_COUNT*1.2),
                                         errors, 
                                         email, 
                                         handle, 
                                         offering, 
                                         zip};
                out.print(Generator.generate(this, "WEB-INF/signup_failed/", retryContent));
            } else {
                signUp(email, pass, handle, offering, zip, userIP);
                //debugServlet();
                if (SignUpOutcome.SUCCESS == outcome || 
                    SignUpOutcome.FAIL_ACCOUNT_EXISTS == outcome) {
                    StringBuffer message = new StringBuffer();
                    if (SignUpOutcome.SUCCESS == outcome) {
                        //Include the following messages if the outcome was successful
                        message.append("You may login but will not be able to use<br>");
                        message.append("the system until your email is verified.<br>");
                        message.append("We sent an email message to " + email + " with the details.<br>");
                        //fire off an email if the outcome was successful
                        sendEmail(email);
                    }
                    String[] loginContents = {outcome.toString(), message.toString(), ""};
                    try {
                        out.print(Generator.generate(this, "WEB-INF/login/", loginContents));
                    } catch (FileNotFoundException fnx) {
                        log.error("Could not generate login file", fnx);
                    }
                } else {
                    out.print(outcome.toString());
                }
            }
        } catch (IOException iox) {
            log.error("could not parse request.", iox);
        } finally {
            //reset uid
            uid = null;
        }

      
    }
    
    /**
     * Validate the contents of the signup data
     * @return null if everything validates.  Otherwise return 
     * html content stating the problem
     */
    private String validate(String email, String pass, String conf_pass, String handle, String offering, String zip) {
        //Reset error count
        ERROR_COUNT = 0; 
                
        queryId = EMAIL;
        String query = "select * from expert where email = '" + email + "'";
        DataBase.getInstance().submit(query, this);
        String accountExists = null;
        if (SignUpOutcome.FAIL_ACCOUNT_EXISTS.equals(outcome)) {
            //just return and the remainder will be handled by existing code
            return null;
        }
        
        queryId = HANDLE;
        String handleQuery = "select * from expert where handle = '" + handle + "'";
        DataBase.getInstance().submit(handleQuery, this);
        String duplicateHandle = null;
        if (SignUpOutcome.FAIL_HANDLE_EXISTS.equals(outcome)) {
            duplicateHandle = SignUpOutcome.FAIL_HANDLE_EXISTS.toString();
        }
        
        String badEmail = null;
        //does the email contain an @ symbol
        if (email.indexOf("@") == -1) {
            badEmail = "Email Address is not formatted correctly.";
            ERROR_COUNT++;
        }
        
        String badPass = null;
        if (pass.length() < 8) {
            badPass = "Password must be at least 8 characters long.";
            ERROR_COUNT++;
        }
        
        String passMismatch = null;
        if (!pass.equals(conf_pass)) {
            passMismatch = "Password and confirmation password must match.";
            ERROR_COUNT++;
        }
        
        String badHandle = null;
        if (handle.length() == 0) {
            badHandle = "Expert Handle cannot be empty.";
            ERROR_COUNT++;
        }
        
        String badOffering = null;
        if (offering.length() == 0) {
            badOffering = "Primary Expertise cannot be empty.";
            ERROR_COUNT++;
        }
        
        String badZip = null;
        if (!zip.matches("[0-9]{5}") && !zip.matches("[0-9]{5}-[0-9]{4}")) {
            badZip = "Zip Code format is xxxxx or xxxxx-xxxx";
            ERROR_COUNT++;
        }

        String[] errors = {duplicateHandle, badEmail, badPass, passMismatch, badHandle, badOffering, badZip};
        return generateContent(errors);
    }
    
    /**
     * Generate valid html content as a table of error messages.
     * @return null if there are no errors otherwise, a table of error messages
     */
    private String generateContent(String[] errors) {
        Collection<String> errs = new ArrayList();
        //First see if there are any errors to be reported
        for (String error : errors) {
            if (error != null) {
                errs.add(error);
            }
        } 
        StringBuffer tableBuf = new StringBuffer("<ul>");
        if (!errs.isEmpty()) {
            for(String err : errs) {
                tableBuf.append("<li type=circle>" + err);
            }
            tableBuf.append("</ul>");
        } else {
            return null;
        }
        return tableBuf.toString();
    }
    
    /**
     * Send an email requesting verification of the email address provided
     */
    private void sendEmail(String email) {
        try {
            Session session = Session.getInstance(props, new SMTPAuthenticator());
            /**
             * use this to debug the smtp session
            session.setDebug(true);
            session.setDebugOut(new java.io.PrintStream(new java.io.File("/usr/share/tomcat/logs/MailOutDebug.txt")));
             */
            Message msg = new VerifyMessage(session);
            Address fromAddress = new InternetAddress("verify@helpguest.com");
            Address toAddress = new InternetAddress(email);
            try {
                msg.setFrom(fromAddress);
            } catch (AddressException aex) {
                log.error("from email address " + fromAddress + " is invalid.", aex);
            }
            try {
                msg.addRecipient(Message.RecipientType.TO, toAddress);
            } catch (AddressException aex) {
                log.warn("email address provided " + email + " is invalid", aex);
            }

            msg.setSubject("Verify your address to use HelpGuest.");
             
            StringBuffer content = new StringBuffer();
            ExpertAccount ea = ExpertAccount.getExpertAccount(email);
            content.append("\r\n" + ea.getHandle() + ",\r\n\r\n");
            content.append("Welcome to HelpGuest!\r\n");
            content.append("Thank you for bringing your expertise to this Marketplace.\r\n\r\n");
            content.append("Verify your account by using the weblink below.\r\n\r\n");
            content.append("https://secure.helpguest.com/hgservlets/VerifyEmail?");
            content.append("email=" + email);
            content.append("&uid=" + ea.getUid());
            content.append("\r\n\r\n");
            content.append("After verifying your email address, \r\n");
            content.append("login and click the 'Update Expertise' button, \r\n");
            content.append("to increase the number of ways that Guest's \r\n");
            content.append("can find you in the HelpGuest search.\r\n");
            content.append("\r\n");
            content.append("Check out:");
            content.append("\r\n");
            content.append("Payment details at https://secure.helpguest.com/RateCalculation.html");
            content.append("\r\n");
            content.append("How-to details at https://secure.helpguest.com/UsageScenarios.html");
            content.append("\r\n");
            content.append("Feedback at http://blog.helpguest.com");
            content.append("\r\n");
            content.append("\r\n");
            content.append("Thanks again,");
            content.append("\r\n");
            content.append("\r\n");
            content.append("-- ");
            content.append("If you could work from home,");
            content.append("would you work in your underwear?");
            content.append("or take your computer out on the town?");
            try {
                msg.setDataHandler(new DataHandler(new ByteArrayDataSource(content.toString(), "text/plain")));
            } catch (IOException iox) {
                log.error("Could not add message content.", iox);
            }
            msg.setSentDate(new Date());
            Transport.send(msg);
            /**
             * this is to send mulitple messages
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.helpguest.com","verify@helpguest.com","V2r3fy");
            msg.saveChanges();
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
             */
            
        } catch (MessagingException mex) {
            log.error("Could not send email to " + email + ".", mex);
        } catch (Exception ex) {
            log.error("Message did not go out successfully.", ex);
            outcome = SignUpOutcome.UNKNOWN_ERROR;
        }
    }
    
    private String getPromo(String email, String uid) {
        String promoCode = Integer.toHexString(rand.nextInt()).toUpperCase() + "-TRIAL";
        String promoQuery = "insert into promo (rates_id, name, authorizing_agent) " +
                "select id, '" + promoCode + "', '" + this.getClass().getName() + 
                "' from rates where rate_code = 'free-trial'";
        DataBase.getInstance().submit(promoQuery);
                
        return promoCode;
    }
    
    private void debugServlet() {
        if (log.isDebugEnabled()) {
            javax.servlet.ServletContext servletContext = this.getServletContext();
            java.util.Date time = new java.util.Date(System.currentTimeMillis());
            log.debug("ServletContext " + (servletContext==null?"is ":"is not ") + 
                      "null at " + time);
        }
    }
    
    class VerifyMessage extends MimeMessage {
        
        public VerifyMessage(Session session) {
            super(session);
        }
        
        protected void updateHeaders() throws MessagingException {
            super.updateHeaders();
            setHeader("Message-ID", "email-verification@secure.helpguest.com");
        }
    }

    class SMTPAuthenticator extends Authenticator {
        protected PasswordAuthentication getPasswordAuthentication() {
            PasswordAuthentication smtpAuth = new PasswordAuthentication("verify@helpguest.com", "V2r3fy");
            return smtpAuth;
        }
        
    }
}
