/**
 * Login generates the login pages
 *
 * Copyright 2005 - HelpGuest Technologies, Inc.
 *
 * @author Mark Abrams
 */
package com.helpguest.servlet;

import com.helpguest.htmlgen.Generator;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;

public class Login extends HttpServlet {
   
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.Login");

    public void doGet(HttpServletRequest request,
            HttpServletResponse response) 
            throws IOException, ServletException {
	doPost(request, response);
    }


    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        LineNumberReader lnr = new LineNumberReader(request.getReader());
        String loginType = request.getParameter("login_type");
        if (loginType == null) {
            loginType="Expert";
        }
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String[] loginContents = {"Expert Login", "", ""};
        try {
            out.print(Generator.generate(this, "WEB-INF/login/", loginContents));
        } catch (FileNotFoundException fnx) {
            log.error("Could not generate login page.", fnx);
        }
    }
}
