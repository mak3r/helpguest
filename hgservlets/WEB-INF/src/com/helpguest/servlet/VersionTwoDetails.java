/**
 * VersionTwoDetails.java
 *
 * Copyright 2004-2006 - HelpGuest Technologies, Inc.
 * 
 * 
 * @Created on November 8, 2006, 7:28 PM
 * @author Mark Abrams
 */
package com.helpguest.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;
import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.HGRates;
import com.helpguest.data.Rates;

public class VersionTwoDetails extends HttpServlet {
   
    private static final String emailKey = "user";
    private static final String passKey = "password";
    private static final String amountKey = "amount";
    private static final String prefTimeKey = "pref_time";
    private static final String selfDescriptionKey = "self_description";
    private static final String avatarFileKey = "avatar_file";
    private static final String WEB_ROOT = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_ROOT.getKey());
    private static final String JAWS_APPS_DIR = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.JAWS_APPS_DIR.getKey());
    private static final String AVATAR_DIR = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.AVATAR_DIR.getKey());
    private static final String fileLocation = WEB_ROOT + JAWS_APPS_DIR + AVATAR_DIR;
    private static final int maxFileSize = 102400;
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.VersionTwoDetails");

    private String getExpertise(ExpertAccount ea) {
        List<String> skills = ea.getExpertise();
        StringBuffer expertise = new StringBuffer();
        for (String skill : skills) {
            expertise.append(skill + ", ");
        }
        //emphasize the first word
        expertise.insert(0,"<em>");
        //close the em after the first comma
        expertise.insert(expertise.indexOf(","),"</em>");
        // remove the last comma
        expertise.deleteCharAt(expertise.lastIndexOf(","));
        return expertise.toString();
    }
    
    private void processFormField(FileItem item) {
  
    }
    
    private void processUploadedFile(FileItem item, ExpertAccount ea) {
        if (item!=null) {
            long fileSize = item.getSize();
            if (fileSize <= maxFileSize) {
                String uploadedName = item.getName();
                if(log.isDebugEnabled()) {log.debug("uploadedName: " + uploadedName);}
                String fileType = null;
                int index = uploadedName.lastIndexOf(".");
                if (index > -1) {
                    index++;
                    fileType = uploadedName.substring(index);               
                    ea.setAvatarFileType(fileType);
                    File avatarFile = new File(ea.getFullPathAvatarFileName());
                    try {
                        item.write(avatarFile);
                    } catch (Exception ex) {
                        log.error("unable to write avatar file to disk.", ex);
                    }
                }//otherwise there is no suffix - ignore
            }//otherwise don't try to handle the file
        }
    }
    
    public void doGet(HttpServletRequest request,
            HttpServletResponse response) 
            throws IOException, ServletException {
	doPost(request, response);
    }


    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        String email = null;
        String pass = null;
        String amount = null;
        String prefTime = null;
        String selfDescription = null;
        FileItem avatarFile = null;
        boolean isMultipartSuccess = true;
        
        if (log.isDebugEnabled()) {log.debug("In Do Post of VersionTwoDetails");}
        
        if (ServletFileUpload.isMultipartContent(new ServletRequestContext(request))) {
            FileItemFactory factory = new DiskFileItemFactory(maxFileSize,new File(fileLocation));
            ServletFileUpload upload = new ServletFileUpload(factory);
            List<FileItem> items = null;
            try {
                items = upload.parseRequest(request);
                for (FileItem next : items) {
                    if(next.isFormField()) {
                        String name = next.getFieldName();
                        String value = next.getString();
                        if (name!=null) {
                            if (name.equalsIgnoreCase(amountKey)) {
                                amount = value;
                            } else if (name.equalsIgnoreCase(prefTimeKey)) {
                                prefTime = value;
                            } else if (name.equalsIgnoreCase(selfDescriptionKey)) {
                                selfDescription = value;
                            } else if (name.equalsIgnoreCase(emailKey)) {
                                email = value;
                            } else if (name.equalsIgnoreCase(passKey)) {
                                pass = value;
                            }
                        }
                    } else {
                        //process this after we get the expert account
                        avatarFile = next;
                    }
                }
            } catch (FileUploadException fuex) {
                log.error("Failed to parse request.", fuex);
                //TODO: we should let the user know it was a problem
                
                isMultipartSuccess = false;
            }
        } else {        
            email = request.getParameter(emailKey);
            pass = request.getParameter(passKey);
            amount = request.getParameter(amountKey);
            prefTime = request.getParameter(prefTimeKey);
            selfDescription = request.getParameter(selfDescriptionKey);
        }        
        
        if (log.isDebugEnabled()) {
            java.util.Enumeration en = request.getParameterNames();
            while (en.hasMoreElements()) {
                String next = (String)en.nextElement();
                log.debug("parameter: " + next + ", value: " + request.getParameter(next));
            }
        }
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (isMultipartSuccess) {
            float minRate = HGRates.MIN_RATE;
            try {
                if (amount!=null) {
                    minRate = Float.parseFloat(amount);
                }
            } catch (NumberFormatException nfx) {
                log.error("could not convert minimum rate of '" + amount + "' to a float.", nfx);
            }

            int time = HGRates.BASE_TIME;
            try {
                if (prefTime != null) {
                    time = Integer.parseInt(prefTime);
                }
            } catch (NumberFormatException nfx) {
                log.error("could not convert preferred time of '" + prefTime + "' to an int.", nfx);
            }

            //Require the password to get the expert account info
            //prahalad changing 08/14/08
            //ExpertAccount ea = ExpertAccount.getExpertAccount(email,pass);
            ExpertAccount ea = ExpertAccount.getExpertAccount(email);
            if (selfDescription != null) {
                //ea.setSelfDescription(selfDescription);
            }

            processUploadedFile(avatarFile, ea);

            //Get the current rate instance or create a new one
            Rates rates = null;
            if (amount != null && prefTime != null) {
                //if the parameters were passed, use them
                //rates = HGRates.newInstance(ea, minRate, time, false);
            } else {
                //get the most current instance
                //rates = HGRates.getCurrentInstance(ea);
            }
            
            try {
	            String[] content = {
	                ea.getEmail(),
	                ea.getUid(), 
	                ea.getHandle(), 
	                getExpertise(ea), 
	                String.valueOf(ea.getNumberGuestsWaiting()), 
	                rates.getFormattedMinRate(),
	                String.valueOf(rates.getMinTime()),
	                String.valueOf(rates.getPreferredTime()),
	                rates.getFormattedDepositAmount(),
	                ea.getSelfDescription(),
	                ea.getAvatarURL().toString()
	            };
            } catch (Exception ex) {
            	log.error("Could not create content.", ex);
            }
           //prahalad commenting 08/14/08
            /* try {
                out.print(Generator.generate(this,"/WEB-INF/expert_account_details/",content));
            } catch (FileNotFoundException fnx) {
                log.error("could not access expert_account_details parts.", fnx);
            }catch (Exception ex) {
                log.error("failed to generate links.", ex);
            }*/
        } else {
            //isMultipartSuccess is false
            try {
                //TODO: make this more robust and useful
                out.print("<html><head></head><body>");
                out.print("There was an error processing your request.<br />");
                out.print("Please click the back button and retry or contact a HelpGuest representative.");
                out.print("</body></html>");
            } catch (Exception ex) {
                log.error("multipart failure and now this.", ex);
            }
        }
    }
}
