/*
 * ResetPass.java
 *
 * Created on April 4, 2007, 10:56 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.servlet;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.ResetCode;
import com.helpguest.htmlgen.Generator;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

public class ResetPass extends HttpServlet {
    
    private static final Logger LOG = Logger.getLogger("com.helpguest.servlet.ResetPass");
        
    /** Creates a new instance of ResetPass */
    public ResetPass() {
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String user = req.getParameter("user");
        String password = req.getParameter("password");
        String confirmPassword = req.getParameter("confirm_password");
        String code = req.getParameter("reset_code");
        
        String rootUrl = 
        	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey()) +
        	"://" + HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey()) + "/";
        resp.setContentType("text/html");
        PrintWriter out;
        try {
            out = resp.getWriter();
            ExpertAccount ea = ExpertAccount.getExpertAccount(user);
                    
            if (ea != null) {
                /* the user - email provided is valid - continue*/
                if (password.length() < 8) {
                    /*password too short*/
                    out.write("The password must be at least 8 characters. Please&nbsp;");
                    out.write("<a href=\"" + rootUrl + "password_reset.html\">try again</a>");
                } else if (password.equals(confirmPassword)) {
                    /*passwords match - continue*/
                    ResetCode resetCode = ResetCode.getInstance(code, user);
                    if (resetCode != null) {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("email: " + resetCode.getEmail());
                            LOG.debug("code :" + resetCode.getCode());
                            LOG.debug("timestamp: " + resetCode.getValidThru());
                        }
                        /*this is a valid reset code and it has not expired*/
                        //update the users password
                        ea.updatePassword(password);
                        //Expire the code
                        ResetCode.expire(resetCode);
                        //direct the user to the login page
                        String [] loginContents = {"Expert Login", 
                                                   "Your password was reset.", ""};
                        out.print(Generator.generate(this, "WEB-INF/login/", loginContents));
                    } else {
                        /* reset code is not valid or expired */
                        out.write("The reset code was invalid for this user id or it has expired.");
                        out.write("<br />");
                        out.write("<a href=\"" + rootUrl + "password_reset.html\">Try again</a>");
                        out.write("<br />");
                        out.write("<a href=\"" + rootUrl + "main.html\">Never mind</a>");
                    }
                } else {
                    /*passwords don't match*/
                    out.write("Passwords do not match please&nbsp;");
                    out.write("<a href=\"" + rootUrl + "password_reset.html\">try again</a>");
                }
            } else {
                /* user name - email address is not valid */
                out.write("The email address '" + user + "' is not valid.");
                out.write("<br />");
                out.write("<a href=\"" + rootUrl + "password_reset.html\">Try again</a>");
                out.write("<br />");
                out.write("<a href=\"" + rootUrl + "signup1.html\">Create an account</a>");
            }
        } catch (IOException iox) {
            LOG.error("Could not write content.", iox);
        } catch (Exception ex) {
            LOG.error("Unexpected exception.", ex);
        }
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }
    
    
    
}
