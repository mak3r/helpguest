/*
 * ExpertBrand.java
 *
 * Created on September 3, 2006, 10:00 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.DataBase;
import com.helpguest.data.ResultHandler;

public class ExpertBrand extends HttpServlet implements ResultHandler {
   
    private int queryId;
    private String page;
    private int eid;
    private String guid;
    private String icon;

    private static final Logger log = Logger.getLogger("com.helpguest.servlet.ExpertBrand");

    private static final String FULLY_QUALIFIED_DOMAIN_NAME = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private static final String JAWS_APPS_DIR = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.JAWS_APPS_DIR.getKey());
    private static final String WEB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey());
    
    /** Creates a new instance of ExpertBrand */
    public ExpertBrand() {
    }
    
    private void validate(String location) {
        String brandQuery = "select page, icon, guest_uid, expert_id from brand_locator" +
                " where page = '" + location + "'";
        DataBase.getInstance().submit(brandQuery,this);
        
    }
    
    public void capture(ResultSet rs, int queryId) {
        if (rs == null) {
            log.error("No records found.");
        } else {
            try {
                while (rs.next()) {
                    page = rs.getString("page");
                    eid = rs.getInt("expert_id");
                    guid = rs.getString("guest_uid");
                    icon = rs.getString("icon");
                }
            } catch (SQLException sqlx) {
                log.error("could not get data from result set.", sqlx);
            }
        }//end of else
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }
    
    public String getExpertBrandConnect(String gid, String size, String hasWebStart) {
        StringBuffer connectionString = new StringBuffer();
        if ("1".equals(hasWebStart)) {
            connectionString.append("<a href=\"" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + JAWS_APPS_DIR + "/client/" + guid + ".jnlp\" ");
            connectionString.append("target=\"HelpGuestBrandManager\">");
            connectionString.append("<img border=0 src=\"" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/images/buttonUp-32.png\" alt=\"HelpGuest\">");
            connectionString.append("</a>");
        } else {
            connectionString.append("<a href=\"http://java.sun.com/PluginBrowserCheck?pass=" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + JAWS_APPS_DIR + "/client/");
            connectionString.append(guid + ".html&fail=http://java.sun.com/j2se/1.5.0/download.html\">");
            connectionString.append("<img border=0 src=\"" + WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/images/buttonUp-32.png\" alt=\"HelpGuest\">");
            connectionString.append("</a>");
        }
        return connectionString.toString();
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)  throws IOException, ServletException {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        StringBuffer requestURL = request.getRequestURL();
        String referrer = request.getParameter("referring_page");
        String hasWebStart = "1";//request.getParameter("web_start");
        
        if (log.isDebugEnabled()) {
            log.debug("referrer: " + referrer);
            java.util.Enumeration params = request.getParameterNames();
            while (params.hasMoreElements()) {
                String elem = (String)params.nextElement();
                log.debug("ExpertBrand parameter: " + elem);
                log.debug("value: " + request.getParameter(elem));
            }
            log.debug(request.getParameter("referring_page"));
        }

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (referrer != null) {
            referrer = URLEncoder.encode(referrer,"UTF-8");
            validate(referrer);
            //if (referrer.equals(page)) {
                //out.println(referrer);
                out.println(getExpertBrandConnect(guid,"small",hasWebStart));
                //out.println("<img src=\"https://secure.helpguest.com/images/" + icon + "\" alt=\"HelpGuest\"></img>");
            //}
        }
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }
}

