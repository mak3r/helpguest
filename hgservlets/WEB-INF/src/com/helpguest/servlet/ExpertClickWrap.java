/*
 * ExpertClickWrap.java
 *
 * Created on May 21, 2006 2:55 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.servlet;

import com.helpguest.data.DataBase;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.FileSystem;
import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;
import com.helpguest.protocol.Lexicon;
import com.helpguest.servlet.util.CheckinCheckout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author mabrams
 */
public class ExpertClickWrap extends HttpServlet implements ResultHandler {

    private int queryId;
    private int id;
    private boolean validCode = false;
    private static Collection<String> rowContent = null;
    private static Collection<Collection> rows = null;
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.ExpertClickWrap");

    
    /** Creates a new instance of Authenticator */
    public ExpertClickWrap() {
    }
    

    public boolean codeIsValid(String code) {
        validCode = false; //may be changed in capture()
        String validateQuery = "select rate_code, daily_rate from rates where id in (" +
                                "select rates_id from promo where name = '" +
                                code + "' and expired = 0)";
        DataBase.getInstance().submit(validateQuery,this);
        return validCode;
    }

    public void capture(ResultSet rs, int queryId) {
        try {
            if (rs.next()) {
                if (rs.getInt("daily_rate") == 0) {
                    validCode = true;
                } else {
                    validCode = false;
                }
            } else {
                validCode = false;
            }
        } catch (SQLException sqlx) {
            log.error("No promo exists under that name.", sqlx);
        }
    }
    
    private String calculateCost(float rate, int hours) {
        return NumberFormat.getCurrencyInstance().format(rate*(hours/8));
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }

    private String getHidden(Map<String,String> params) {
        StringBuffer buf = new StringBuffer("");
        for (String name : params.keySet()) {
            buf.append("<input type=\"hidden\" name=\"" + name + "\" value=\"" + params.get(name) + "\">\r\n");
        }
        return buf.toString();
    }
    
    private String getUsage(String itemNumber) {
        return itemNumber.substring(0,itemNumber.indexOf('-'));
    }
    
    private String getShelfLife(String itemNumber) {
        return itemNumber.substring(itemNumber.indexOf('-')+1);        
    }
    
    protected void writeStandardClickWrap(PrintWriter out, String[] details) {
        try {
            out.print(Generator.generate(this, "WEB-INF/expert_click_wrap/", details));
        } catch (IOException iox) {
            log.error("could not parse request.", iox);
        }        
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request,response);
    }
        
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();

        Map params = new HashMap();
        String cmd = request.getParameter("cmd");
        params.put("cmd",cmd);
        String test_ipn = request.getParameter("test_ipn");
        params.put("test_ipn",test_ipn);
        String business = request.getParameter("business");
        params.put("business",business);
        String item_name = request.getParameter("item_name");
        params.put("item_name",item_name);
        String item_number = request.getParameter("item_number");
        params.put("item_number",item_number);
        String currency_code = request.getParameter("currency_code");
        params.put("currency_code",currency_code);
        String amount = request.getParameter("amount");
        params.put("amount",amount);
        String image_url = request.getParameter("image_url");
        params.put("image_url",image_url);
        String no_shipping = request.getParameter("no_shipping");
        params.put("no_shipping",no_shipping);
        String no_note = request.getParameter("no_note");
        params.put("no_note",no_note);
        String returnVal = request.getParameter("return");
        params.put("return",returnVal);
        String cancel_return = request.getParameter("cancel_return");
        params.put("cancel_return",cancel_return);
        String custom = request.getParameter("custom");
        params.put("custom",custom);
                        
        String hidden = getHidden(params);
        String info = "You've selected the '" + item_name + "' plan. Cost: $" + amount;
        String hours = getUsage(item_number);
        String days = getShelfLife(item_number);
        
        boolean isFreeTrial = false;
        if (Float.parseFloat(amount) == 0.0 && codeIsValid(item_name)) {
            cmd = "_hg_promo";
            params.put("cmd",cmd);
            hidden = getHidden(params);
            isFreeTrial = true;
        }
        String[] details = {hidden,hours,days,days,info};

        response.setContentType("text/html");
        PrintWriter out;
        try {
            out = response.getWriter();
            if (isFreeTrial) {
                out.print(Generator.generate(this, "WEB-INF/free_trial_click_wrap/", details));                
            } else {
                writeStandardClickWrap(out,details);
            }
        } catch (NumberFormatException nfx) {
            try {
                out = response.getWriter();
                log.error("amount: " + amount + " could not be parsed.  Generating standard click wrap.", nfx);
                writeStandardClickWrap(out,details);
            } catch (IOException x) {
                log.error("could not get output stream writer.", x);
            }
        } catch (IOException iox) {
            log.error("could not parse request.", iox);
        }
    }
    
}
