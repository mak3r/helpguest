import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.helpguest.data.AuthorizedKeys;

/**
 * StorePubKey stores the public keys for the helper.
 * 
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * 
 * @author Mark Abrams
 */

public class StorePubKey extends HttpServlet {

	private final static Logger LOG = Logger.getLogger(StorePubKey.class);
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String qString = request.getQueryString();
		String cPath = request.getContextPath();
		String pInfo = request.getPathInfo();
		String rURI = request.getRequestURI();

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<html>");
		out.println("<head>");
		out.println("<title>query string</title>");
		out.println("</head>");
		out.println("<body bgcolor=\"white\">");
		out.println("<h1> query string:" + qString + "</h1><br>");
		out.println("context path: " + cPath + "<br>");
		out.println("path info: " + pInfo + "<br>");
		out.println("request URI: " + rURI + "<br>");
		out.println("</body>");
		out.println("</html>");

		Enumeration e = request.getParameterNames();
		while (e.hasMoreElements()) {
			String name = (String) e.nextElement();
			String value = request.getParameter(name);
			out.println("<br>" + name + " = " + value);
		}

		/*
		 * ServletInputStream in = request.getInputStream();
		 * out.println("<br>Input stream size: " + request.getContentLength());
		 * int len = 0; byte[] buf = new byte[1024]; int rd =
		 * in.readLine(buf,0,1024); out.println("<br>'" + new String(buf) +
		 * "' read (" + rd + ") bytes."); while(
		 * (len=in.readLine(buf,len,buf.length)) != -1 ) { out.println("<br>" +
		 * new String(buf)); }
		 */
		LineNumberReader lnr = new LineNumberReader(request.getReader());
		out.println(lnr.readLine());

		String pubKey = request.getParameter("pubkey");
		try {
			// open the authorized_keys2 file and write to it.
			out.println("<br> making string for auth file.");
			String authFile = "/home/hlpguest/.ssh/authorized_keys2";
			// File authFile = new File("/home/hlpguest/.ssh/authorized_keys2");
			FileWriter fw = new FileWriter(authFile, true);
			PrintWriter pw = new PrintWriter(fw);
			pw.println(pubKey);
			pw.close();
		} catch (Exception ex) {
			ex.printStackTrace(out);
			LOG.error("failed writing key to file.", ex);
		}
		AuthorizedKeys.insert(pubKey);

	}
}
