/*
 * Generator.java
 *
 * Created on September 7, 2005, 5:13 PM
 */

package com.helpguest.htmlgen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServlet;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class Generator {
    
    private static final Logger log = Logger.getLogger("com.helpguest.htmlgen");

    /** Creates a new instance of Generator */
    public Generator() {
    }
    
    /**
     * Generate a String of content composed of file contents and Strings.
     * This method is intended to generate dynamic html although it may have other 
     * purposes as well, i.e. jnlp descriptor.
     * This method does not validate html data!!
     *
     * <p/>
     * It is expected that inserts contents are inserted between file
     * contents in the order of the array. eg f0|i0|f1|i1|...|flast.
     * 
     * @param files is the list of file names to be concatenated.
     * The files are expected to be resources located on the classpath.
     * @param inserts is the list of Strings to concatenate
     * @throws IllegalArgumentException if files.length is not exactly one greater
     * than inserts.length
     */
    public static String generate(String[] files, String[] inserts) throws IllegalArgumentException {
        if (files.length != inserts.length + 1) {
            throw new IllegalArgumentException("length of files must be exactly one greater than inserts.");
        }
        
        StringBuffer generated = new StringBuffer();
        for (int i = 0; i < files.length-1; i++) {
            generated.append(readFile(files[i]));
            generated.append(inserts[i]);
        }
        //Append the last file contents
        generated.append(readFile(files[files.length-1]));
        
        return generated.toString();
    }

    public static String generate(HttpServlet servlet, String[] files, String[] inserts) throws IllegalArgumentException {
        if (files.length != inserts.length + 1) {
            throw new IllegalArgumentException("length of files must be exactly one greater than inserts.");
        }
        
        StringBuffer generated = new StringBuffer();
        for (int i = 0; i < files.length-1; i++) {
            generated.append(readFile(servlet, files[i]));
            generated.append(inserts[i]);
        }
        //Append the last file contents
        generated.append(readFile(servlet, files[files.length-1]));
        
        return generated.toString();
    }

    /**
     * Generates a concatenation of the files and Strings listed in data array.
     * <p/>
     * This method uses the generate(File[], String[]) method and is
     * constrained by the same input requirments that there are one more
     * file resources than string data.
     * 
     * @throws IllegalArgumentException if files.length is not exactly one greater
     * than inserts.length
     * @param data is a list of files and strings to be concatenated together.
     */
    public static String generate(String... data) throws IllegalArgumentException {
        List <String> fileNames = new ArrayList<String>();
        List <String> stringData = new ArrayList<String>();
        
        for (String s : data) {
            Object o = s.getClass().getResourceAsStream(s);
            if (o == null) {
		if (log.isDebugEnabled()) {log.debug("'" + s + "' is string data.");}
                stringData.add(s);
            } else {
		if (log.isDebugEnabled()) {log.debug("'" + s + "' is a file name.");}
                fileNames.add(s);
            }
        }
        return generate((String[])fileNames.toArray(new String[0]), (String[])stringData.toArray(new String[0]));
    }

    
    /**
     * Generate a String composed of the contents of each file name part.X in 
     * the partsDirectory with content between each part. For example, if 
     * the content is an array of 3, parts 1-4 will be concatenated with the
     * content sequenced in between in the order it is given.
     * @return the generated string
     */
    public static String generate(HttpServlet servlet, String partsDirectory, String... content) throws FileNotFoundException {
        StringBuffer generated = new StringBuffer();
        int i = 1;
        String fileName = partsDirectory + "part." + i;
        String file = servlet.getServletContext().getRealPath(fileName);
        if (file == null) {
            log.error("null file value.. cannot generate page with fileName: " + fileName);
        } else {
            for (String insert : content) {            
                if (!(new File(file).exists())) {
                    throw new FileNotFoundException("missing file part " + file);
                }
                generated.append(readFile(servlet, fileName));
                generated.append(insert);
                i++;
                fileName = partsDirectory + "part." + i;
            }
            //Finally, append the last file part
            if (!(new File(file).exists())) {
                throw new FileNotFoundException("missing last file part " + file);
            }
            generated.append(readFile(servlet, fileName));
        }
        return generated.toString();
    }
    
    private static String readFile(HttpServlet servlet, String fileName) {
        StringBuffer sb = new StringBuffer();
        InputStreamReader isr = new InputStreamReader(servlet.getServletContext().getResourceAsStream(fileName));
        
        char[] buf = new char[1024];
        int cnt;
        try {
            while ((cnt = isr.read(buf)) != -1) {
                sb.append(buf, 0, cnt);
            }
        } catch (IOException iox) {
            log.error("Could not read file", iox);
        }
        
        return sb.toString();
    }
    
    private static String readFile(String fileName) {
        StringBuffer sb = new StringBuffer();
        InputStreamReader isr = new InputStreamReader(fileName.getClass().getResourceAsStream(fileName));
        
        char[] buf = new char[1024];
        int cnt;
        try {
            while ((cnt = isr.read(buf)) != -1) {
                sb.append(buf, 0, cnt);
            }
        } catch (IOException iox) {
            log.error("Could not read file", iox);
        }
        
        return sb.toString();
    }
    
    public static void main(String[] args) {    
        BasicConfigurator.configure();

        if (args.length >= 3) {
            System.out.println(Generator.generate(args));
        } else {
            System.out.println("usage: java <classpath> com.helpguest.htmlgen.Generator" +
                    " [<Resource File Name1> <String>...] <Resource File Name Last>");
        }
    }
}
