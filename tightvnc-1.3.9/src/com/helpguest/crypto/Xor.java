package com.helpguest.crypto;

/**
 * This class was designed to encode and decode
 * vnc passwords for use with the apples ARD vnc server.
 * it could be modified to work with other 
 * xor type encodings.
 * 
 * The apple contraints are:
 * <ol>
 * <li>the 16 char scrambler code</li>
 * <li>the maximum of 8 char truncation of the password given (unconfirmed)</li>
 * </ol>
 * @author mabrams
 *
 */

public class Xor {

	/**
	 * The code apple used to scramble the password
	 */
	static final char[] appleVNCScrambler = new char[] {
		0x17,
		0x34,
		0x51,
		0x6E,
		0x8B,
		0xA8,
		0xC5,
		0xE2,
		0xFF,
		0x1C,
		0x39,
		0x56,
		0x73,
		0x90,
		0xAD,
		0xCA
	};
	
	/**
	 * the one argument is the password to xor encrypt.
	 * It should be in double quotes
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			throw new IllegalArgumentException(
					"Usage: java -classpath <classpath> com.helpguest.crypto.Xor \"<password>\"");
		}
		System.out.println("NOTE: passwords ending in 0 cannot be decoded by this utility.");

		char[] scrambled = xor(paddedPassword(args[0]), appleVNCScrambler);
		
		publicize(scrambled);
		

	}

	private static char[] paddedPassword(final String pass) {
		char[] padded = new char[appleVNCScrambler.length];
		char[] passArray = pass.toCharArray();
		
		for (int i=0; i<padded.length; i++) {
			if (i<passArray.length) {
				padded[i] = passArray[i];				
			} else {
				padded[i] = (char) 0x0;
			}
		}
		
		return padded;
	}
	
	/**
	 * 
	 * @param realPassword
	 * @return
	 */
	private static void publicize(final char[] scrambledPass) {

		System.out.print("for the vnc commandline: ");
		for (int i = 0; i < scrambledPass.length; i++) {
			System.out.print( Integer.toHexString(((int)scrambledPass[i])).toUpperCase());
		}

		System.out.println();
		System.out.println("char[] encodedPass = new char[] {");

		for (int i=0; i<scrambledPass.length; i++) {
			System.out.println( "0x" + Integer.toHexString(((int)scrambledPass[i])).toUpperCase() + ", ");
		}
		System.out.println("};");
		System.out.println();
	}
	
	public static String decodeForAppleVNC(final char[] encodedPass) {
		char[] decoded = xor(encodedPass, appleVNCScrambler);
		StringBuffer decodedBuf = new StringBuffer();
		
		for (int i=0; i<decoded.length; i++) {
			decodedBuf.append(decoded[i]);
		}

		//remove trailing 0's - these were 
		//added as part of the padding
		//when this item was created.
		while(decodedBuf.length() > 0) {
			if (decodedBuf.charAt(decodedBuf.length()-1) == '0') {
				decodedBuf.deleteCharAt(decodedBuf.length()-1);
			} else {
				break;
			}
		}
		
		return decodedBuf.toString();
	}

	/**
	 * @return xor arrays a and b
	 */
	public static char[] xor(char[] a, char[] b) {
		int length = Math.min(a.length, b.length);
		char[] result = new char[length];
		for (int i = 0; i < length; i++) {
			result[i] = (char) (a[i] ^ b[i]);
		}
		return result;
	}

}
