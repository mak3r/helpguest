/**
 * 
 */
package com.helpguest.chatterbox.server;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.log4j.Logger;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.chatterbox.client.GuestChatter;
import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.ChatterboxParty;
import com.helpguest.gwt.protocol.chatterbox.ServerMessage;
import com.helpguest.gwt.protocol.chatterbox.WelcomeMessage;
import com.helpguest.service.chat.ChatterboxMessageClient;

/**
 * This class GuestChatterImpl manages chatterbox sessions.
 * 
 * 
 * @author mabrams
 *
 */
public class GuestChatterImpl extends RemoteServiceServlet implements
		GuestChatter, IsSerializable {

	private static final long serialVersionUID = -6458195634373158965L;
	
	private static final String host = "localhost";
	
	private final ChatterboxMessageClient chatClient = new ChatterboxMessageClient();
	
	private final Queue messageQueue = new LinkedList();
	
	private static final List chatServerClosedMessages = new ArrayList();
	
	private static final Logger LOG = Logger.getLogger("com.helpguest.chatterbox.server.GuestChatterImpl");
	
	static {
		ChatterboxMessage unavailableMessage = new ServerMessage();
		unavailableMessage.setMessage("Expert is unavailable");
		chatServerClosedMessages.add(unavailableMessage);
		
		ChatterboxMessage howToExitMessage = new ServerMessage();
		howToExitMessage.setMessage(
				"Send another message or press enter to cancel chat.");
		chatServerClosedMessages.add(howToExitMessage);
	}

	
	public List<com.helpguest.gwt.protocol.ChatterboxMessage> checkMessages() {
		final List<ChatterboxMessage> messageList =  new ArrayList<ChatterboxMessage>();
		
		ChatterboxMessage next = null;
		while ((next = (ChatterboxMessage) messageQueue.poll()) != null) {
			messageList.add(next);
		}

		return messageList;
	}

	/**
	 * {@inheritDoc}
	 */
	public void initilizeChatterBox(final int port, final ChatterboxMessage issue, final ChatterboxParty party) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("entering initializeChatterBox(ChatterboxMessage)");
			LOG.debug("message is: " + issue);
		}
		
		try {
			Socket chatSock = new Socket(host, port);
			chatClient.setSocket(chatSock);
			//FIXME this methods second param should be a ChatterboxParty
			ChatterboxMessage welcomeMsg = new WelcomeMessage(party);
			sendMessage(welcomeMsg);
			sendMessage(issue);			
		} catch (UnknownHostException uhx) {
			LOG.error("could not get socket on host: " + host, uhx);
		} catch (IOException iox) {
			LOG.error("could not write to socket.", iox);
		}
		
		Thread t = new Thread( new Runnable() {
			public void run() {
				ChatterboxMessage hostMessage;
				try {
					while ((hostMessage = (ChatterboxMessage) chatClient.read()) != null) {
						//Add the message to the message queue
						if (!"".equals(hostMessage.getMessage())) {
							messageQueue.add(hostMessage);
						}
					}
				} catch (Exception ex) {
					LOG.error("failed reading from chat client.", ex);
				}
			}
		});
		t.start();
	}

	/**
	 * {@inheritDoc}
	 */
	public void notifyActive() {
		// TODO Auto-generated method stub

	}

	
	public List<com.helpguest.gwt.protocol.ChatterboxMessage> sendMessage(final ChatterboxMessage someMessage) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("entering sendMessage(ChatterboxMessage)");
		}
		//Send the message via the chat client to the chat server.
		try {
			chatClient.write(someMessage);
		} catch (SocketException sox) {
			//TODO chat server is unavailable, notify ui (system msg)
			messageQueue.addAll(chatServerClosedMessages);
		}
		return checkMessages();
	}

	public void notifyComplete() {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isAvailable() {
		//TODO eventually, we'll evaluate if the expert is available.
		return true;
	}

}
