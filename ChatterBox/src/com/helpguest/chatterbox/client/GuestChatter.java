/**
 * 
 */
package com.helpguest.chatterbox.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public interface GuestChatter extends RemoteService {

	/**
	 * This method is called when sending a message to the chat server.
	 * This makes an implicit call to checkMessages.
	 * 
	 * @param someMessage String
	 * @return any messages waiting on the server
	 * as a List of ChatterboxMessages
     *
	 */
	public List<com.helpguest.gwt.protocol.ChatterboxMessage> sendMessage(final ChatterboxMessage someMessage);
	
	/**
	 * Initialize the chatter box.
	 * This method enables the chat by providing the service
	 * with user details and any information that might be needed
	 * to start up the chat server.
	 * 
	 * @param issue ChatterboxMessage
	 */
	public void initilizeChatterBox(final int port, final ChatterboxMessage issue, final ChatterboxParty party);

	/**
	 * This method requests to have any avialable messages
	 * waiting on the chat server returned to the client.
	 * 
	 * @return an List of messages as ChatterboxMessages
	 */
	public List<com.helpguest.gwt.protocol.ChatterboxMessage> checkMessages();

	/**
	 * Notify the server that this client is entering some
	 * information - ie preparing to send a message.
	 *
	 */
	public void notifyActive();
	
	/**
	 * Notify the server that this client is done communicating.
	 */
	public void notifyComplete();
	
	/**
	 * @return true if the expert is available to chat.
	 */
	public boolean isAvailable();
	
}
