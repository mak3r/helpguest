/**
 * 
 */
package com.helpguest.chatterbox.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public interface GuestChatterAsync {

	public void checkMessages(final AsyncCallback callback);

	public void initilizeChatterBox(
			final int port,
			final ChatterboxMessage issue,
			final ChatterboxParty party, 
			final AsyncCallback callback);
	
	public void notifyActive(final AsyncCallback callback);

	public void sendMessage(final ChatterboxMessage someMessage, final AsyncCallback callback);
	
	public void notifyComplete(final AsyncCallback callback);
	
	public void isAvailable(final AsyncCallback callback);

}
