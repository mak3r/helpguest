/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 4, 2007
 */
package com.helpguest.chatterbox.client;


import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.helpguest.gwt.protocol.ChatterboxMessage;

/**
 * @author mabrams
 *
 */
public class ChatterboxSession implements IsSerializable {

	private ChatterboxMessage initialQuestion;
	private List messages;
	private String guestName;
	//FIXME These next three should be replaced with ExpertDTO
	private String expertUid;
	private String guestUid;
	private String expertHandle;
	/**
	 * @return the expertHandle
	 */
	public String getExpertHandle() {
		return expertHandle;
	}
	/**
	 * @param expertHandle the expertHandle to set
	 */
	public void setExpertHandle(final String expertHandle) {
		this.expertHandle = expertHandle;
	}
	/**
	 * @return the expertUid
	 */
	public String getExpertUid() {
		return expertUid;
	}
	/**
	 * @param expertUid the expertUid to set
	 */
	public void setExpertUid(final String expertUid) {
		this.expertUid = expertUid;
	}
	/**
	 * @return the guestName
	 */
	public String getGuestName() {
		return guestName;
	}
	/**
	 * @param guestName the guestName to set
	 */
	public void setGuestName(final String guestName) {
		this.guestName = guestName;
	}
	/**
	 * @return the guestUid
	 */
	public String getGuestUid() {
		return guestUid;
	}
	/**
	 * @param guestUid the guestUid to set
	 */
	public void setGuestUid(final String guestUid) {
		this.guestUid = guestUid;
	}
	/**
	 * @return the initialQuestion
	 */
	public ChatterboxMessage getInitialQuestion() {
		return initialQuestion;
	}
	/**
	 * @param initialQuestion the initialQuestion to set
	 */
	public void setInitialQuestion(final ChatterboxMessage initialQuestion) {
		this.initialQuestion = initialQuestion;
	}
	/**
	 * @return the messages
	 */
	public List getMessages() {
		return messages;
	}
	/**
	 * @param messages the messages to set
	 */
	public void setMessages(final List messages) {
		this.messages = messages;
	}
}
