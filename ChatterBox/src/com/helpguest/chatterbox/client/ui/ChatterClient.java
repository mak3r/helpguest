/**
 * 
 */
package com.helpguest.chatterbox.client.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.chatterbox.client.GuestChatter;
import com.helpguest.chatterbox.client.GuestChatterAsync;
import com.helpguest.chatterbox.client.GuestParty;
import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.ChatterboxMessageType;
import com.helpguest.gwt.protocol.ChatterboxParty;
import com.helpguest.gwt.protocol.chatterbox.GuestMessage;

/**
 * @author mabrams
 * 
 */
public class ChatterClient extends VerticalPanel {

	final TextBox input = new TextBox();

	final VerticalPanel messageOutPanel = new VerticalPanel();

	final ScrollPanel scrollPanel = new ScrollPanel(messageOutPanel);

	//Create the chatterbox proxy
	final GuestChatterAsync guestChatter =
		(GuestChatterAsync) GWT.create(GuestChatter.class);

	//Default timer for 5 minutes
	private Timer timer;
	
	//Check every second
	private static final int CHECK_MESSAGE_FREQUENCY = 1000;
	
	//Default lifeline
	private LifeLine lifeLine = new LifeLine();
	
	/**
	 * This is a client ui to the GuestChatter implementation.
	 * It is important that the GuestChatter init method 
	 * is called with a userName and issue value.
	 * In order to do that, BE SURE TO CALL THE initChat method
	 * on this object after creation.
	 * 
	 * This client implements the ChatClientInterface for that
	 * purpose.
	 *
	 */
	public ChatterClient() {
		initUI();		
	}

	/**
	 * This ctor initialzes the GuestChatter impl on 
	 * instantiation.  There is no need to call initChat
	 * if this ctor is used.  Unless, name and issue
	 * are not sufficient values to initialize the GuestChatter.
	 * 
	 * @param name String
	 * @param issue String
	 */
	public ChatterClient(final ClickableChatRequestor requestor) {
		initUI();
		requestor.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				initChat(requestor);
			}
			
		});
	}

	public ChatterClient(
			final ClickableChatRequestor requestor, 
			final String brickWidth) {
		this(requestor);
		setWidth(brickWidth);
	}
		
	protected void initChat(final ChatRequestor requestor) {
		final ChatterboxParty party = GuestParty.getParty(requestor);
		// Specify the url where chatterbox is running
		ServiceDefTarget endpoint = (ServiceDefTarget) guestChatter;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "GuestChatter";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		ChatterboxMessage initialMessage = new GuestMessage(party);
		initialMessage.setMessage(requestor.getIssue()+"\n");

		guestChatter.initilizeChatterBox(
				requestor.getPort(), initialMessage, party, initializeCallback);
		
		//Initialize the polling mechanism to check for messages
		lifeLine.reset();
		timer = new Timer() {
			public void run() {
				if (!lifeLine.expired()) {
					//Only check for a certain period of time.
					guestChatter.checkMessages(checkMessagesCallback);
				} else {
					//We should probably shutdown the client
					//and do a nextStep
					//TODO shutdown and next step
				}
				
			}
		};
		//ten second check
		timer.scheduleRepeating(CHECK_MESSAGE_FREQUENCY);
		
		input.addKeyboardListener(new KeyboardListener() {
			
			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
			}
	
			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (keyCode == KEY_ENTER) {
					// don't output empty lines
					if (!input.getText().trim().equals("")) {
						ChatterboxMessage outboundMessage = new GuestMessage(party);
						//We have to add the newline for the expert
						outboundMessage.setMessage(input.getText()+"\n");
						//TODO how about the uids for whom this is bound.
						guestChatter.sendMessage(outboundMessage, sendMessageCallback);
						//Allows the timer to continue to check for messages
						lifeLine.reset();
					}
				}
			}
	
			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				// TODO Auto-generated method stub
	
			}
		});

	}

	private void initUI() {
		input.setWidth("400px");
		scrollPanel.setHeight("150px");
		scrollPanel.setWidth("400px");
	
	
		this.add(scrollPanel);
		this.add(input);
	}

	/**
	 * Parse the message and return it formatted as a Label.
	 * 
	 * @param message
	 *            String
	 * @return a Label
	 */
	private Label parseMessage(ChatterboxMessage message) {
		// Split the string into two pieces separated by a colon
		ChatterboxMessageType whom = message.getMessageType();
		String content = message.getMessage();
		Label label = new Label();

		if (whom.equals(ChatterboxMessage.GUEST)) {
			// Handle a guest message
			label.setStyleName("guest-says");
		} else if (whom.equals(ChatterboxMessage.EXPERT)) {
			// Handle an expert message
			label.setStyleName("expert-says");
		} else if (whom.equals(ChatterboxMessage.SERVER)) {
			// Handle a system message
			label.setStyleName("server-says");
		} else if (whom.equals(ChatterboxMessage.HELP_GUEST_INFO)) {
			// Handle a system message
			label.setStyleName("helpguest-says");
		} else {
			label.setStyleName("gwt-Label");
		}
		label.setText(message.getSender() + ": " + content);
		return label;
	}

	private void updateOutput(List messages) {
		// Set the sent text into the output area
		Iterator messageIterator = messages.iterator();
		while (messageIterator.hasNext()) {
			// parse the message and stylize according to which
			ChatterboxMessage next =
				(ChatterboxMessage) messageIterator.next();
			Label labeledMessage = parseMessage(next);

			messageOutPanel.add(labeledMessage);
			scrollPanel.setScrollPosition(scrollPanel
					.getScrollPosition() + 100);

		}
	}
	
	AsyncCallback initializeCallback = new AsyncCallback() {

		/**
		 * {@inheritDoc}
		 */
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				Window.alert("ChatterClient initializeCallback" + sBuf.toString());
			} catch (Throwable th) {
				Window.alert("ChatterClient initializeCallback [throwable] " + th.getMessage());
			}
		}

		/**
		 * {@inheritDoc}
		 */
		public void onSuccess(Object result) {
			// TODO make a call to get messages
			
		}
		
	};

	// Create a callback to handle the result.
	AsyncCallback sendMessageCallback = new AsyncCallback() {
		// Create the list to return
		private List messageList = new ArrayList();
		
		public void onSuccess(Object result) {
			messageList = (ArrayList) result;
			updateOutput(messageList);
			//Now reset the buffer
			input.setText("");
			input.setFocus(true);
		}

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				Window.alert("ChatterClient sendMessageCallback " + sBuf.toString());
			} catch (Throwable th) {
				Window.alert("ChatterClient sendMessageCallback [throwable] " + th.getMessage());
			}

		}

	};
	
	// Create a callback to handle the result.
	AsyncCallback checkMessagesCallback = new AsyncCallback() {
		// Create the list to return
		private List messageList = new ArrayList();
		
		public void onSuccess(Object result) {
			messageList = (ArrayList) result;
			updateOutput(messageList);
		}

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				Window.alert("ChatterClient checkMessagesCallback " + sBuf.toString());
			} catch (Throwable th) {
				Window.alert("ChatterClient checkMessagesCallback [throwable] " + th.getMessage());
			}

		}

	};
	
	class LifeLine {
		private long millisToEnd;
	    /**
	     * Default life is 1 minutes
	     */
	    private long lifeTime = 1*60*1000;


	    /**
	     * Create a lifeline with 60000 millis
	     * default (one minute)
	     */
	    public LifeLine() {}
	    
	    /**
	     * Create a lifeline with a default
	     * number of millliseconds
	     * @param millis long
	     */
	    public LifeLine(long millis) {
	    	lifeTime = millis;
	    }

	    /**
	     * Reset the lifeline to the number of millis
	     * specified.
	     * Does not reset the original default millis
	     * set, therefore subsequent calls to 
	     * reset() defaults to the original life line.
	     * 
	     * @param millis long
	     */
	    protected void reset(long millis) {
	        long currentTime = System.currentTimeMillis();
	        millisToEnd = currentTime + millis;
	    }

	    /**
	     * Reset the life line to the default
	     * number of millis
	     *
	     */
	    protected void reset() {
	    	reset(lifeTime);
	    }
	    
	    /**
	     * 
	     * @return true if the lifeline has expired.
	     * false if there is time left.
	     */
	    protected boolean expired() {
	        if (millisToEnd > System.currentTimeMillis()) {
	            return false;
	        }
	        return true;
	    }
	}
}
