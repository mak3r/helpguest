package com.helpguest.chatterbox.client.ui;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface ChatRequestor extends IsSerializable {

	public abstract String getUserName();

	public abstract String getIssue();

	public abstract String getSessionId();
	
	public abstract int getPort();
	
}