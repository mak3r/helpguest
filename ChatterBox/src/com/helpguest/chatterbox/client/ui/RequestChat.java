/**
 * 
 */
package com.helpguest.chatterbox.client.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SourcesClickEvents;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author mabrams
 *
 */
public class RequestChat extends VerticalPanel implements ClickableChatRequestor, ChatRequestor, SourcesClickEvents {

	transient final Label error = new Label();
	transient final TextBox userName = new TextBox();	
	transient final Label userNameLabel = new Label("Your Name:");
	transient final TextArea issue = new TextArea();
	transient final Button chatNowButton = new Button("Begin Chat");
	transient final VerticalPanel requestChatPanel = new VerticalPanel();
	transient public final static int MAX_MESSAGE_LENGTH = 128;
	
	transient private final Label charsToGo = new Label("0");
	transient private String stopText = "";
	transient private List clickListenerList = new ArrayList();
	private String sessionId;
	private int port;
	private String userNameValue = "";
	private String issueValue = "";
	
	/**
	 * This ctor is for serialization
	 */
	public RequestChat(){}
	
	/**
	 * Initialize the chat requesting component without 
	 * next stepper functionality.
	 *
	 */
	public RequestChat(final String chatSessionId, final int port) {
		this.sessionId = chatSessionId;
		this.port = port;
		init();
		error.setStyleName("hg-error");
	}
	
	private void init() {
		userName.setWidth("300px");
		issue.setWidth("400px");
		issue.setHeight("100");
		enableChatNowButton();

		chatNowButton.addClickListener(new ClickListener() {
			/**
			 * {@inheritDoc}
			 */
			public void onClick(Widget sender) {
				//Capture the issue and user name
				issueValue = issue.getText();
				userNameValue = userName.getText();
				
				//if the port is 0 set it to the username value
				if (port == 0) {
					try {
						port = Integer.parseInt(userNameValue);
					} catch (NumberFormatException nfx) {
						//LOG.error("port has no value");
					}
				}

				Iterator it = clickListenerList.iterator();
				while( it.hasNext() ) {
					((ClickListener) it.next()).onClick(sender);
				}
			}
	
		});
		chatNowButton.setVisible(true);
		
		issue.addKeyboardListener(new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				if (issue.getText().length() == MAX_MESSAGE_LENGTH) {
					stopText = issue.getText();
				}
				if (issue.getText().length() < MAX_MESSAGE_LENGTH &&
						!stopText.equals("")) {
					//reset the stop text
					stopText = "";
					//reset the error
					correctedMistake();
				}
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (issue.getText().length() > 0) {
					enableChatNowButton();
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				if (issue.getText().length() > MAX_MESSAGE_LENGTH) {
					issue.setText(stopText);
					onError("The maximum message length is " + MAX_MESSAGE_LENGTH);					
				}
				charsToGo.setText(String.valueOf(issue.getText().length()));
			}
			
		});

		userName.addKeyboardListener(new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				//NOOP
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (userName.getText().length() > 0) {
					enableChatNowButton();
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				//NOOP
			}
			
		});
		
		requestChatPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		requestChatPanel.add(error);
		HorizontalPanel userNamePanel = new HorizontalPanel();
		userNamePanel.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
		userNamePanel.add(userNameLabel);
		userNamePanel.add(new Label(" "));
		userNamePanel.add(userName);
		requestChatPanel.add(userNamePanel);
		VerticalPanel issuePanel = new VerticalPanel();
		HorizontalPanel charsPanel = new HorizontalPanel();
		charsPanel.add(new Label("Description of the problem: ("));
		charsPanel.add(charsToGo);
		charsPanel.add(new Label("/" + MAX_MESSAGE_LENGTH + ")"));
		issuePanel.add(charsPanel);
		issuePanel.add(issue);
		requestChatPanel.add(issuePanel);
		requestChatPanel.add(chatNowButton);
		requestChatPanel.setSpacing(10);
		this.add(requestChatPanel);
		this.setVisible(true);
		this.setWidth("440px");
		
	}
	
	private void enableChatNowButton() {
		if (issue.getText().length() > 0 &&
				userName.getText().length() > 0) {
			chatNowButton.setEnabled(true);
		} else {
			chatNowButton.setEnabled(false);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public void addClickListener(ClickListener listener) {
		clickListenerList.add(listener);
	}

	/**
	 * {@inheritDoc}
	 */
	public void removeClickListener(ClickListener listener) {
		clickListenerList.remove(listener);
	}

	/**
	 * {@inheritDoc}
	 */
	public String getUserName() {
		return userNameValue;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String getIssue() {
		return issueValue;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String getSessionId() {
		return sessionId;
	}
	
	/**
	 * Show some error text.
	 * @param text String
	 */
	public void onError(String text) {
		error.setText(text);
		error.setVisible(true);
		errorTimer.schedule(30000);		
	}
	
	public void correctedMistake() {
		error.setText("");
		error.setVisible(false);
	}

	transient Timer errorTimer = new Timer() {
		public void run() {
			correctedMistake();
		}
	};

	public int getPort() {
		return port;
	}


}
