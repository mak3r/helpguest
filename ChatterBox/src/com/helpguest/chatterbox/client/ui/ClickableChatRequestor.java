package com.helpguest.chatterbox.client.ui;

import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.SourcesClickEvents;

public interface ClickableChatRequestor extends SourcesClickEvents, ChatRequestor {

	/**
	 * {@inheritDoc}
	 */
	public abstract void addClickListener(ClickListener listener);

	/**
	 * {@inheritDoc}
	 */
	public abstract void removeClickListener(ClickListener listener);

	/**
	 * {@inheritDoc}
	 */
	public abstract String getUserName();

	/**
	 * {@inheritDoc}
	 */
	public abstract String getIssue();

	/**
	 * {@inheritDoc}
	 */
	public abstract String getSessionId();

}