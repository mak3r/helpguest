package com.helpguest.chatterbox.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.chatterbox.client.ui.ChatterClient;
import com.helpguest.chatterbox.client.ui.RequestChat;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ChatterBox implements EntryPoint {

	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		String baseComponent = "chat_component";

		VerticalPanel chatPanel = new VerticalPanel();
		
		final RequestChat requestor = new RequestChat("chatterboxSession", 0);
		requestor.setVisible(true);
		final ChatterClient chatter = new ChatterClient(requestor);
		chatter.setVisible(false);

		requestor.addClickListener(new ClickListener() {
			public void onClick(Widget widget) {
				//There is only one widget, the button
				//Validate the expert can chat otherwise error
				requestor.setVisible(false);
				chatter.setVisible(true);
			}
		});
		chatPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		chatPanel.add(chatter);
		chatPanel.add(requestor);
		chatPanel.setVisible(true);

		RootPanel.get(baseComponent).add(chatPanel);		
	}
}
