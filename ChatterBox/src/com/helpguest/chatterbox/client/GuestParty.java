/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 14, 2007
 */
package com.helpguest.chatterbox.client;

import com.helpguest.chatterbox.client.ui.ChatRequestor;
import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public class GuestParty {

	public static ChatterboxParty getParty(final ChatRequestor requestor) {
		ChatterboxParty party = new ChatterboxParty();
		
		party.setGuestName(requestor.getUserName());
		//FIXME these need to come off the RequestChat
		party.setGuestUid("someGuestUid");
		party.setSessionId(requestor.getSessionId());
		
		return party;
	}

}
