/**
 * 
 */
package com.helpguest.server;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.client.GuestChatter;

/**
 * @author mabrams
 *
 */
public class GuestChatterImpl extends RemoteServiceServlet implements
		GuestChatter {

	private static final long serialVersionUID = -6458195634373158965L;
	
	private transient SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");
	
	/* (non-Javadoc)
	 * @see com.helpguest.client.GuestChatter#checkMessages()
	 */
	public List checkMessages() {
		long ts = System.currentTimeMillis();
		String time = null;
		time = sdf.format(new Date(ts));

		List messageList = new ArrayList();		
		messageList.add("server: (" + time + ")");

		return messageList;
	}

	/* (non-Javadoc)
	 * @see com.helpguest.client.GuestChatter#initilizeChatterBox(java.lang.String, java.lang.String)
	 */
	public void initilizeChatterBox(final String user, final String pass) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.helpguest.client.GuestChatter#notifyActive()
	 */
	public void notifyActive() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.helpguest.client.GuestChatter#sendMessage(java.lang.String)
	 */
	public List sendMessage(final String someMessage) {
		
		//This implementation will actually send the message received
		// to the server initialized by initializeChatterbox.
		List messageList = checkMessages();		
		messageList.add(someMessage);

		return messageList;
	}

	public void notifyComplete() {
		// TODO Auto-generated method stub
		
	}

}
