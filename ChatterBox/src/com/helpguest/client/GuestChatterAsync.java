/**
 * 
 */
package com.helpguest.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author mabrams
 *
 */
interface GuestChatterAsync {

	/* (non-Javadoc)
	 * @see com.helpguest.client.GuestChatter#checkMessages()
	 */
	public void checkMessages(final AsyncCallback callback);

	/* (non-Javadoc)
	 * @see com.helpguest.client.GuestChatter#initilizeChatterBox(java.lang.String, java.lang.String)
	 */
	public void initilizeChatterBox(String user, String pass, final AsyncCallback callback);
	
	/* (non-Javadoc)
	 * @see com.helpguest.client.GuestChatter#notifyActive()
	 */
	public void notifyActive(final AsyncCallback callback);

	/* (non-Javadoc)
	 * @see com.helpguest.client.GuestChatter#sendMessage(java.lang.String)
	 */
	public void sendMessage(String someMessage, final AsyncCallback callback);
	
	public void notifyComplete(final AsyncCallback callback);

}
