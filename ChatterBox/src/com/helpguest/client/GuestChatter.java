/**
 * 
 */
package com.helpguest.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;

/**
 * @author mabrams
 *
 */
public interface GuestChatter extends RemoteService {

	/**
	 * This method is called when sending a message to the chat server.
	 * This makes an implicit call to checkMessages.
	 * 
	 * @param someMessage String
	 * @return any messages waiting on the server
	 * as a List of Strings.
     * 
	 */
	public List<java.lang.String> sendMessage(String someMessage);
	
	/**
	 * Initialize the chatter box.
	 * This method enables the chat by providing the service
	 * with user details and any information that might be needed
	 * to start up the chat server.
	 * 
	 * @param user String
	 * @param pass String
	 */
	public void initilizeChatterBox(String user, String pass);

	/**
	 * This method requests to have any avialable messages
	 * waiting on the chat server returned to the client.
	 * 
	 * @return an List of messages as Strings
	 */
	public List<java.lang.String> checkMessages();

	/**
	 * Notify the server that this client is entering some
	 * information - ie preparing to send a message.
	 *
	 */
	public void notifyActive();
	
	/**
	 * Notify the server that this client is done communicating.
	 */
	public void notifyComplete();
	
}
