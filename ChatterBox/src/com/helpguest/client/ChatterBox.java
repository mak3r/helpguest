package com.helpguest.client;

import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ChatterBox implements EntryPoint {

	//final Button button = new Button("Click me");

	//final Label label = new Label();

	final TextArea output = new TextArea();

	final TextBox input = new TextBox();

	final String divId = HTMLPanel.createUniqueId();

	final HTMLPanel htmlPanel = new HTMLPanel("<div id=\"" + divId
			+ "\"></div>");

	final String scrollPanelId = "chatterbox-output";

	final ScrollPanel scrollPanel = new ScrollPanel(htmlPanel);
	
	static final String CLIENT = "client";
	
	static final String SERVER = "server";
	
	static final String EXPERT = "expert";
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		/*
		button.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				if (label.getText().equals(""))
					label.setText("Hello World!");
				else
					label.setText("");
			}
		});
		*/
		
		output.setStyleName("guest_text_output");
		output.setVisibleLines(7);
		output.setWidth("500");
		input.setWidth("500");
		input.addKeyboardListener(new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (keyCode == KEY_ENTER) {
					// TODO Send the input text to the chat server

					// don't output empty lines
					if (!input.getText().trim().equals("")) {
						sendMessage(CLIENT + ":" + input.getText());
					}
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				// TODO Auto-generated method stub

			}
		});

		scrollPanel.setHeight("120");
		scrollPanel.setWidth("500");
		scrollPanel.setStyleName(scrollPanelId);
		scrollPanel.setVisible(true);
		htmlPanel.setVisible(true);
		// Assume that the host HTML has elements defined whose
		// IDs are "slot1", "slot2". In a real app, you probably would not want
		// to hard-code IDs. Instead, you could, for example, search for all
		// elements with a particular CSS class and replace them with widgets.
		//
		//RootPanel.get("slot1").add(button);
		//RootPanel.get("slot2").add(label);
		RootPanel.get("chatterbox_output").add(scrollPanel);
		RootPanel.get("chatterbox_input").add(input);
	}

	/**
	 * Send the message to the server and update the output with any new
	 * messages (which should include the one just sent).
	 * 
	 * @param message
	 *            String
	 * @return a list of messages to output
	 */
	private void sendMessage(final String message) {
		// Create the chatterbox proxy
		GuestChatterAsync guestChatter = (GuestChatterAsync) GWT
				.create(GuestChatter.class);

		// Specify the url where chatterbox is running
		ServiceDefTarget endpoint = (ServiceDefTarget) guestChatter;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "GuestChatterImpl";
		endpoint.setServiceEntryPoint(moduleRelativeURL);

		// Create a callback to handle the result.
		AsyncCallback callback = new AsyncCallback() {
			// Create the list to return
			List messageList = null;

			public void onSuccess(Object result) {
				messageList = (List) result;
				updateOutput();
			}

			public void onFailure(Throwable caught) {
				messageList.add(caught.getMessage());
				updateOutput();
			}

			private void updateOutput() {
				// Set the sent text into the output area
				Iterator messageIterator = messageList.iterator();
				while (messageIterator.hasNext()) {
					//parse the message and stylize according to which
					Label labeledMessage = parseMessage((String) messageIterator.next());

					htmlPanel.add(labeledMessage, divId);
					scrollPanel.setScrollPosition(scrollPanel
							.getScrollPosition() + 100);
					input.setText("");
					input.setFocus(true);
				}
			}
			
		};

		// send the message
		guestChatter.sendMessage(message, callback);
	}

	/**
	 * Parse the message and return it formatted as a Label.
	 * 
	 * @param message String
	 * @return a Label
	 */
	private static Label parseMessage(String message) {
		//Split the string into two pieces separated by a colon
		String[] splitMessage = message.split(":",2);
		String whom = splitMessage[0];
		String content = splitMessage[1];
		Label label = new Label();
		
		if (whom.equals(CLIENT)) {
			//Handle a guest message
			label.setStyleName("guest-says");
		} else if (whom.equals(EXPERT)) {	
			//Handle an expert message
			label.setStyleName("expert-says");
		} else if (whom.equals(SERVER)) {
			//Handle a system message
			label.setStyleName("server-says");
		}
		label.setText(content + " : "
				+ label.getStyleName());
		return label;
	}

}
