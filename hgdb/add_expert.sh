#!/bin/sh

cur_date=$(date)
echo -n "Enter your mysql password: "
read -s mysqlpass
echo 
echo -n "Enter the expert account user name (no spaces): "
read expert_name 
echo -n "Enter the experts initial password: "
read expert_pass
echo -n "Enter initial usage amount in hours: "
read usage
echo -n "Enter shelf life in days: " 
read shelf_life

expert_uid=$(mysql -D helpguest -p$mysqlpass --skip-column-names -s \
             -e "select PASSWORD(concat('$expert_name', '$cur_date'));")
#echo "Expert uid will be " $expert_uid

mysql -D helpguest -p$mysqlpass -e \
	"insert into expert (uid, user, pass, contact_id, offering, max_guests, \
	 pass_question, pass_answer, create_date) values ( '$expert_uid', \
	 '$expert_name', PASSWORD('$expert_pass'), 0, 'online', 5, \
	 'is the sun warm', 'yes', NOW())"

expert_id=$(mysql -D helpguest -p$mysqlpass --skip-column-names -s \
            -e "select id from expert where uid = '$expert_uid'")
#echo "Using expert_id " $expert_id

sfx="_guest"
for x in 1 2 3 4 5 
do
	guest=$expert_name$sfx"_"$x
#	echo "Creating guest " $guest
	guest_uid=$(mysql -D helpguest -p$mysqlpass --skip-column-names -s \
	            -e "select PASSWORD(concat('$guest', '$cur_date'));")
#	echo "Guest $x uid will be " $guest_uid
	guest_pass=$(mysql -D helpguest -p$mysqlpass --skip-column-names -s \
	             -e "select substring('$guest_uid',1,6);")
#	echo "Guest $x pass is " $guest_pass
	password=$(mysql -D helpguest -p$mysqlpass --skip-column-names -s \
	             -e "select PASSWORD('$guest_pass')") 
	mysql -D helpguest -p$mysqlpass -e "insert into guest (uid, user, pass, expert_id) \
	                                   values ('$guest_uid', '$guest', '$password', '$expert_id')"
	echo "Guest$x --> username: $guest, password: $guest_pass"
done

## insert expert account status
let millis=$usage*3600*1000
mysql -D helpguest -p$mysqlpass -e "insert into expert_account_status \
                                    (time_remaining, end_date, expert_id) \
				    values ($millis, adddate(now(),$shelf_life), \
				            '$expert_id')"

