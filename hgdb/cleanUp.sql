--Clean unverified experts 

start transaction;
--expert
delete from expert where expert.id in ( 
select expert_id from email_verification where is_verified=0);

--admin_experts
delete from admin_experts where admin_experts.expert_id in (
select expert_id from email_verification where is_verified=0);

--expert_account_status
delete from expert_account_status where expert_account_status.expert_id in (
select expert_id from email_verification where is_verified=0);

--expert_account_update
delete from expert_account_update where expert_account_update.expert_id in (
select expert_id from email_verification where is_verified=0);

--expert_details
delete from expert_details where expert_details.expert_id in (
select expert_id from email_verification where is_verified=0);

--expert_payments
delete from expert_payments where expert_payments.expert_id in (
select expert_id from email_verification where is_verified=0);

--expert_rates
delete from expert_rates where expert_rates.expert_id in (
select expert_id from email_verification where is_verified=0);

--guest
delete from guest where guest.expert_id in (
select expert_id from email_verification where is_verified=0);

--ipn
delete from ipn where ipn.expert_id in (
select expert_id from email_verification where is_verified=0);

--live_expert
delete from live_expert where live_expert.expert_id in (
select expert_id from email_verification where is_verified=0);

--service_offerings
delete from service_offerings where service_offerings.expert_id in (
select expert_id from email_verification where is_verified=0);

--session
delete from session where session.expert_id in (
select expert_id from email_verification where is_verified=0);

--survey_1
delete from survey_1 where survey_1.expert_id in (
select expert_id from email_verification where is_verified=0);

--trial_complete
delete from trial_complete where trial_complete.expert_id in (
select expert_id from email_verification where is_verified=0);

--expert_tags
delete from expert_tags where expert_tags.expert_id in (
select expert_id from email_verification where is_verified=0);

--email_verification
delete from email_verification where is_verified=0;

commit;
