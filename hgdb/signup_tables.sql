
-- rates
-- maintains information on pricing
CREATE TABLE rates (
	id integer PRIMARY KEY AUTO_INCREMENT, 
	rate_code varchar(50) UNIQUE NOT NULL,
	is_valid int DEFAULT 0,
	is_standard integer DEFAULT 0,
	daily_rate float NOT NULL,
	usage_in_hours integer NOT NULL,
	shelf_life_in_days int NOT NULL
);

-- service_offerings
-- Keywords describing the services offered by the expert
-- is_primary is 0 or 1 indicating that this is the primary offering
-- expert_id is a link back to the expert.id
-- keyword is a word used to describe a non primary expertise
-- primary key forces db level duplicate checking and case insensitivity
CREATE TABLE service_offerings (
	expert_id integer NOT NULL,
        is_primary integer NOT NULL DEFAULT 0,
	keyword varchar(32) NOT NULL,
	PRIMARY KEY (expert_id, keyword)
);

-- email_verification
-- used to verify that e-mail addresses are valid
CREATE TABLE email_verification (
	id integer PRIMARY KEY AUTO_INCREMENT,
	expert_id integer NOT NULL,
	is_verified integer NOT NULL DEFAULT 0
);

-- trial_complete
-- to indicate that a free trial has been used
CREATE TABLE trial_complete (
	id integer PRIMARY KEY AUTO_INCREMENT, 
	expert_id integer NOT NULL,
	used_trial integer DEFAULT 0
);

-- user demographics
CREATE TABLE demographics (
	id integer PRIMARY KEY AUTO_INCREMENT,
	email varchar(255),
	handle varchar(255),
	expertise varchar(255),
	zip_code varchar(10),
	user_ip varchar(17),
	referral_source varchar(50)
);
