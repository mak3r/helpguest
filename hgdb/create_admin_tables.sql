/*
 * Copyright 2005, HelpGuest Technologies, Inc.
 * 
 * HelpGuest persistent user data tables
 */

--DROP TABLE IF EXISTS admin;
CREATE TABLE admin(
	id int PRIMARY KEY AUTO_INCREMENT,
	uid varchar(50) UNIQUE NOT NULL,
	user varchar(20) UNIQUE NOT NULL,
	pass varchar(50) NOT NULL,
	name varchar(255) NOT NULL
);


--DROP TABLE IF EXISTS admin_experts;
CREATE TABLE admin_experts(
	uid int PRIMARY KEY AUTO_INCREMENT,
	expert_id int NOT NULL,
	admin_id int NOT NULL
);

--DROP TABLE IF EXISTS admin_authenticator_log;
CREATE TABLE admin_authenticator_log (
	id int PRIMARY KEY AUTO_INCREMENT,
	user varchar(20) NOT NULL,
	outcome varchar(255) NOT NULL,
	timestamp timestamp NOT NULL
);

--DROP TABLE IF EXISTS expert_authenticator_log;
CREATE TABLE expert_authenticator_log (
	id int PRIMARY KEY AUTO_INCREMENT,
	email varchar(255) NOT NULL,
	outcome varchar(255) NOT NULL,
	timestamp timestamp NOT NULL
);

--DROP TABLE IF EXISTS guest_authenticator_log;
CREATE TABLE guest_authenticator_log (
	id int PRIMARY KEY AUTO_INCREMENT,
	email varchar(255) NOT NULL,
	outcome varchar(255) NOT NULL,
	timestamp timestamp NOT NULL
);

CREATE TABLE `session` (
	`id` int(11) NOT NULL auto_increment,
	`expert_id` int(11) NOT NULL default '0',
	`item_id` varchar(255) NOT NULL default '',
	`item_name` varchar(255) NOT NULL default '',
	`item_number` varchar(255) NOT NULL default '',
	`expert_rates_id` int(11) NOT NULL default '0',
	`insert_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
	`stopped` int(11) NOT NULL default '0',
	`started` int(11) NOT NULL default '0',
	`guest_start` int(11) NOT NULL default '0',
	`start_time` timestamp NOT NULL default '0000-00-00 00:00:00',
	`stop_time` timestamp NOT NULL default '0000-00-00 00:00:00',
	`amount_due` float(8,2) default '0.0',
	`payment_requested` timestamp NOT NULL default '0000-00-00 00:00:00',
	`payment_sent` timestamp NOT NULL default '0000-00-00 00:00:00',
	`session_group_id` bigint NOT NULL default '0',
	`paypal_email` varchar(255),
	`is_demo` int(11) NOT NULL default '0',
	PRIMARY KEY  (`id`),
	UNIQUE KEY `item_id` (`item_id`),
	FOREIGN KEY (`expert_rates_id`) REFERENCES expert_rates (`id`)
);


CREATE TABLE `expert_rates` (
  `id` int(11) NOT NULL auto_increment,
  `expert_id` int(11) NOT NULL default '0',
  `min_rate` float(5,2) NOT NULL default '0.99',
  `min_time` int(3) NOT NULL default '10',
  `preferred_time` int(3) unsigned NOT NULL default '20',
  `rate_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
);

CREATE TABLE `expert_payments` (
  `id` int(11) NOT NULL auto_increment,
  `expert_id` int(11) NOT NULL default '0',
  `session_id` int(11) NOT NULL default '0',
  `amount_due` float(8,2) default '0.00',
  `paypal_email` varchar(255) default '',
  `payment_sent_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `payment_record` (`expert_id`, `session_id`),
  FOREIGN KEY (`session_id`) REFERENCES session (`id`)
);

CREATE TABLE `authorized_keys` (
	`id` int(11) NOT NULL auto_increment,
	`ssh_key` text NOT NULL,
	`expired` int(1) NOT NULL default 0,
	`insert_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
	`user_name` varchar(26) default 'hlpguest',
	PRIMARY KEY (`id`)
);

