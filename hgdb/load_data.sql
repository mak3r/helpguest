-- insert data into protocol tables

truncate table protocol_response_qualifier;
start transaction;
insert into protocol_response_qualifier (qualifier) values ('accept');
insert into protocol_response_qualifier (qualifier) values ('reject');
insert into protocol_response_qualifier (qualifier) values ('vnc');
insert into protocol_response_qualifier (qualifier) values ('chat');
insert into protocol_response_qualifier (qualifier) values ('voip');
insert into protocol_response_qualifier (qualifier) values ('web_chat');
commit;

/*
truncate table protocol_request_type;
start transaction;
insert into protocol_request_type (type) values ('assistance');
insert into protocol_request_type (type) values ('escalate');
insert into protocol_request_type (type) values ('join');
commit;
*/

truncate table service_ports;
start transaction;
insert into service_ports (service, description, port_start, max_ports) 
	values ('vnc', 'port to use for vnc connections', 5900, 1000);
insert into service_ports (service, description, port_start, max_ports) 
	values ('chat', 'port to use for helpguest chat connections', 7000, 1000);
insert into service_ports (service, description, port_start, max_ports) 
	values ('voip', 'port to use for helpguest voip connections', 22220, 1000);
/* agent has 2x the ports because they are unique for guest and expert */
insert into service_ports (service, description, port_start, max_ports)
	values ('agent', 'port to use for helpguest agents', 8000, 2000);
insert into service_ports (service, description, port_start, max_ports)
	values ('web_chat', 'port to use for chatterbox', 11000, 1000);
commit;

truncate table category_offerings;
start transaction;
INSERT INTO category_offerings(category,offering) VALUES('DESIGN','Photoshop');
INSERT INTO category_offerings(category,offering) VALUES('DESIGN','Dreamweaver');
INSERT INTO category_offerings(category,offering) VALUES('DESIGN','Flash');
INSERT INTO category_offerings(category,offering) VALUES('DESIGN','Illustrator');
INSERT INTO category_offerings(category,offering) VALUES('DESIGN','InDesign');
INSERT INTO category_offerings(category,offering) VALUES('DESIGN','Fireworks');
INSERT INTO category_offerings(category,offering) VALUES('DESIGN','Premier');
INSERT INTO category_offerings(category,offering) VALUES('DESIGN','Final Cut Pro');
INSERT INTO category_offerings(category,offering) VALUES('DESIGN','General');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','C++');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Visual Basic');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Shell Scripting');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','HTML');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Web Development');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Flash Action Script');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Java');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','PHP');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Perl');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','iWeb');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','ASP');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','APACHE');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','XML');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Google web Widgets');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Coldfusion');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Dreamweaver');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Frontpage');
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Linux'); 
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','IPhone and Gphone'); 
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','IDE'); 
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','Database'); 
INSERT INTO category_offerings(category,offering) VALUES('DEVELOPMENT','General'); 
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','Microsoft Office'); 
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','Quickbooks');
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','Open office'); 
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','Blackberry');
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','Treo');
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','Other major PDA');
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','FTP');
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','VPN');
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','Shopping Carts');
INSERT INTO category_offerings(category,offering) VALUES('BUSINESS','General');
INSERT INTO category_offerings(category,offering) VALUES('SYSTEMS','Apple Control Panel');
INSERT INTO category_offerings(category,offering) VALUES('SYSTEMS','Unix/Linux Command Line');
INSERT INTO category_offerings(category,offering) VALUES('SYSTEMS','Windows System Preferences');
INSERT INTO category_offerings(category,offering) VALUES('SYSTEMS','Web Server Configuration');
INSERT INTO category_offerings(category,offering) VALUES('SYSTEMS','Servlet Container Configuration');
INSERT INTO category_offerings(category,offering) VALUES('SYSTEMS','Networking');
INSERT INTO category_offerings(category,offering) VALUES('SYSTEMS','Operating Systems');
INSERT INTO category_offerings(category,offering) VALUES('SYSTEMS','Applications');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Spyware/Adware/Simple Viruses removal');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Wireless setup');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Router Configuration/Portforwarding');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Myspace');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Facebook'); 
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Ebay');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','photoshop'); 
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','dreamweaver');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Craigslist'); 
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Google Services'); 
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Blogging');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','iPOD');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','iPhone');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','iPhoto');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','iLife'); 
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','iTunes');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Windows Media Player');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Adobe Premier');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Final Cut Pro');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Pro Tools');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Other Accessory setup');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Software Drivers Search');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Printer setup');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Networking setup'); 
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Webpage publishing'); 
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Gaming');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','World of Warcraft'); 
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','avatars');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Second Life');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','Zune (just kidding)');
INSERT INTO category_offerings(category,offering) VALUES('PERSONAL','General');
commit;
