-- ipn
-- table to hold data communicated by instant
-- payment notification (via paypal)
-- item_name ==> rates.rate_code
-- item_number ==> rates.usage_in_hours+'/'+rates.shelf_life_in_days
-- custom ==> expert.email or expert.uid
-- expert_id ==> expert.id
CREATE TABLE ipn (
    id integer PRIMARY KEY AUTO_INCREMENT,
    verify_sign varchar(255) NOT NULL UNIQUE,
    first_name varchar(64),
    last_name varchar(64),
    payer_business_name varchar(127),
    payer_email varchar(127),
    payer_id varchar(13),
    item_name varchar(50),
    item_number varchar(50),
    custom varchar(255),
    tax float,
    payment_status varchar(9),
    payment_date varchar(28),
    mc_gross float,
    mc_currency varchar(3) DEFAULT "USD",
    txn_id varchar(17),
    receiver_email varchar(127),
    test_ipn integer,
    expert_id integer NOT NULL,
    invoice varchar(255), 
    insert_date timestamp 
);

CREATE TABLE promo (
    id integer PRIMARY KEY AUTO_INCREMENT,
    rates_id integer NOT NULL,
    name varchar(16) UNIQUE NOT NULL,
    expired integer default 0,
    insert_date timestamp NOT NULL,
    authorizing_agent varchar(255) default "ADMIN"
);
