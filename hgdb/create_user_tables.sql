/*
 * Copyright 2005, HelpGuest Technologies, Inc.
 * 
 * HelpGuest persistent user data tables
 */

-- DROP TABLE IF EXISTS expert;
CREATE TABLE `expert` (
	`id` int PRIMARY KEY AUTO_INCREMENT,
	`uid` varchar(50) UNIQUE NOT NULL,
	`email` varchar(255) UNIQUE NOT NULL,
	`pass` varchar(50) NOT NULL,
	`handle` varchar(36) UNIQUE NOT NULL,
	`paypal_email` varchar(255) default 'paypal email address for payment to you',
	`web_version` varchar(16) default '2',
	`max_guests` int NOT NULL,
	`can_demo` int default 1,
	`create_date` timestamp
);

CREATE TABLE `expert_details` (
	`id` int PRIMARY KEY AUTO_INCREMENT,
	`expert_id` int NOT NULL,
	`about_me` text,
	`about_me_approved` int NOT NULL DEFAULT '1',
	`image` blob,
	`image_url` varchar(255),
	`image_approved` int NOT NULL DEFAULT '1',
	`extension` varchar(4)
);


-- DROP TABLE IF EXISTS guest;
CREATE TABLE `guest` (
	`id` integer PRIMARY KEY AUTO_INCREMENT,
	`uid` varchar(50) UNIQUE NOT NULL,
	`email` varchar(255), 
	`pass` varchar(50),
	`expert_id` int NOT NULL 
);

-- DROP TABLE IF EXISTS expert_account_status;
CREATE TABLE `expert_account_status` (
	`id` int PRIMARY KEY AUTO_INCREMENT,
	`time_remaining` bigint DEFAULT 0,
	`end_date` datetime NOT NULL, 
	`expert_id` int NOT NULL
);

/**
 * This history table for bookeeping purposes.
 * It should help us insure that
 * we are accounting usage properly
 */
-- DROP TABLE IF EXISTS expert_account_update;
CREATE TABLE `expert_account_update` (
	`id` int PRIMARY KEY AUTO_INCREMENT,
	`expert_id` int NOT NULL,
	`update_type` varchar(50) NOT NULL,
	`add_service_date` timestamp
);

-- live_expert
--  Used to indicate if an expert is active and can
--  be listed as available
CREATE TABLE `live_expert` (
	`id` integer PRIMARY KEY AUTO_INCREMENT,
	`expert_id` integer NOT NULL,
	`is_live` integer NOT NULL DEFAULT 0
);

--  brand locator
--  information about where our branded icons are
--  and who operates them
CREATE TABLE `brand_locator` (
  `id` int(11) NOT NULL auto_increment,
  `expert_id` int(11) NOT NULL default '0',
  `guest_uid` varchar(50) NOT NULL default '',
  `page` text NOT NULL,
  `icon` varchar(64) NOT NULL default '',
  `frame_target` varchar(7) default '_top',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
);

-- survey_1
--  Our first survey
CREATE TABLE `survey_1` (
    `id` integer AUTO_INCREMENT,
	`expert_id` integer NOT NULL,
	`session_id` varchar(255) NOT NULL,
	`overall_satisfaction` integer,
	`problem_solved` integer,
	`recommend` integer,
	`repeat_customer` integer,
	`professional_service` integer,
	`insert_date` timestamp NOT NULL,
    FOREIGN KEY (`session_id`) REFERENCES session (`item_id`),
    PRIMARY KEY (`id`)
);

-- password_reset_code
--  manage password resets
CREATE TABLE `password_reset_code` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(255) NOT NULL default '',
  `code` varchar(50) NOT NULL default '',
  `valid_thru` timestamp NOT NULL,
  PRIMARY KEY  (`id`)
);

-- remote_launch_options
--  maintain the launch options that allow users to run helpguest from anywhere
CREATE TABLE `remote_launch_options` (
	`id` INTEGER NOT NULL auto_increment,
	`expert_id` INTEGER NOT NULL,
	`post_url` varchar(255) NOT NULL,
	`subtext` varchar(32),
	`subtext_group_id` INTEGER,
	`launch_icon_id` INTEGER NOT NULL,
	`ref_name` varchar(16) NOT NULL,
	`is_pay_link` int(1) NOT NULL default 0,
	`rate` float(5,2),
	`html_launch_content` TEXT,
	`jnlp_launch_content` TEXT,
	`remote_image_content` BLOB,
	`remote_url_content` TEXT,
	`last_update_date` timestamp NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`launch_icon_id`) REFERENCES LAUNCH_ICON (`id`)
);

-- launch_icon
--  The standard icons used for remote launch
CREATE TABLE `launch_icon` (
	`id` INTEGER NOT NULL auto_increment,
	`icon` BLOB NOT NULL,
	PRIMARY KEY (`id`)
);
