-- Tables related to searching for an expert match

-- ignore_list
CREATE TABLE ignore_list (
	id integer PRIMARY KEY AUTO_INCREMENT,
	keyword varchar(32) NOT NULL
);

-- executed_searches
CREATE TABLE executed_searches (
	id bigint PRIMARY KEY AUTO_INCREMENT,
	phrase varchar(255) NOT NULL,
	insert_date timestamp
);

-- searched_words
CREATE TABLE searched_words (
	id bigint PRIMARY KEY AUTO_INCREMENT,
	keyword varchar(32) NOT NULL,
	insert_date timestamp
);

-- category and offering list to search for experts
-- prahalad 6/16/08 
CREATE TABLE category_offerings (
	cat_off_id int PRIMARY KEY AUTO_INCREMENT,
	category varchar(64) NOT NULL,
	offering varchar(64) NOT NULL
);

--list of 'tags' or keywords to better describe a category offering
-- prahalad 6/16/08 
CREATE TABLE expert_tags (
	tag_id int PRIMARY KEY AUTO_INCREMENT,
	cat_off_id int NOT NULL,
    tag_name text NOT NULL,
	expert_id int NOT NULL
);


