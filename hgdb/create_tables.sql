-- from within the mysql database use the backlsash-dot
-- command to 'source' this file


-- column: uid is the grouping mechanisim for multiple rows and across tables
DROP TABLE IF EXISTS protocol_request;
CREATE TABLE protocol_request (
	id integer PRIMARY KEY AUTO_INCREMENT,
	type varchar(16) NOT NULL,
	status varchar(20) DEFAULT 'WAITING',
	uname character varying(255) NOT NULL,
	message character varying(255) DEFAULT 'NO MESSAGE GIVEN',
	guest_uid varchar(50) NOT NULL,
	expert_uid varchar(50) NOT NULL,
	request_ts timestamp, 
	key_id bigint DEFAULT 0
);

DROP TABLE IF EXISTS protocol_request_type;

-- This is the table that will provide technicians with
-- a selection of users in need.  It is populated
-- by the app when a client confirms a connection
-- via the protocol
DROP TABLE IF EXISTS protocol_response;
CREATE TABLE protocol_response (
	id integer PRIMARY KEY AUTO_INCREMENT,
	type varchar(16) NOT NULL,
	port integer,
	uid varchar(50) NOT NULL,
        inet_address varchar(16) DEFAULT '0.0.0.0',
	response_ts timestamp,
	key_id bigint DEFAULT 0
);

DROP TABLE IF EXISTS protocol_response_qualifier;
CREATE TABLE protocol_response_qualifier (
	id integer PRIMARY KEY AUTO_INCREMENT,
	qualifier character varying(255)
);

-- The following two tables are needed for doleing out ports
-- to clients.  They may also be used in an administrative
-- manner when questioning what ports are in use.
DROP TABLE IF EXISTS service_ports;
CREATE TABLE service_ports (
	id integer PRIMARY KEY AUTO_INCREMENT,
	service character varying(16) NOT NULL,
	description varchar(255) NOT NULL,
	port_start integer UNIQUE NOT NULL,
	max_ports integer DEFAULT 0
);

-- This is distinct from the protocol response table as it
-- is needed by the mechanism that decides which ports are available 
-- to be issued.
-- column: available is false when 0 true when any other value
-- column: service_ports_id is the foreign key for service_ports.id
-- column: port is the port number avail/unavail for this service.
--         if no entry exists then a port is deemed unavailable.
DROP TABLE IF EXISTS current_services;
CREATE TABLE current_services (
	port integer UNIQUE,
	service_ports_id integer,
	available integer DEFAULT 0
);

CREATE TABLE IF NOT EXISTS inquiry (
        id int PRIMARY KEY AUTO_INCREMENT,
        first varchar(255),
        last varchar(255),
        phone varchar(255),
        phone2 varchar(255),
        company varchar(255),
        address1 varchar(255),
	address2 varchar(255),
        title varchar(255),
	city varchar(255),
	state varchar(255),
	zip varchar(255),
	email varchar(255)
);

CREATE TABLE `page_hits` (
  `id` int(11) NOT NULL auto_increment,
  `ip` varchar(15) default NULL,
  `page` varchar(255) default NULL,
  `hits` int(11) default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `pip` (`page`,`ip`)
);

