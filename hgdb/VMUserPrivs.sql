-- LOAD THIS FILE INTO THE DEVELOPER VIRTUAL MACHINE
-- TO ALLOW ACCESS TO HELPGUEST TABLES LOCALLY

LOCK TABLES `columns_priv` WRITE;
/*!40000 ALTER TABLE `columns_priv` DISABLE KEYS */;
INSERT INTO `columns_priv` VALUES ('%','helpguest','hlpguest','protocol_request','status','2005-11-19 06:06:48','Update'),('localhost','helpguest','notifier','expert_authenticator_log','user','2005-12-20 17:24:38','Select'),('localhost','helpguest','notifier','expert_authenticator_log','outcome','2005-12-20 17:24:38','Select'),('localhost','helpguest','notifier','expert_authenticator_log','timestamp','2005-12-20 17:24:38','Select'),('localhost','helpguest','notifier','guest_authenticator_log','user','2005-12-20 18:01:54','Select'),('localhost','helpguest','notifier','guest_authenticator_log','outcome','2005-12-20 18:01:54','Select'),('localhost','helpguest','notifier','guest_authenticator_log','timestamp','2005-12-20 18:01:54','Select'),('%','helpguest','notifier','guest_authenticator_log','email','2006-04-22 18:08:21','Select'),('localhost','helpguest','notifier','guest_authenticator_log','email','2006-04-22 18:17:29','Select'),('localhost','helpguest','notifier','expert_authenticator_log','email','2006-04-22 18:46:42','Select');
/*!40000 ALTER TABLE `columns_priv` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `db` WRITE;
/*!40000 ALTER TABLE `db` DISABLE KEYS */;
INSERT INTO `db` VALUES ('localhost','helpguest','hgservice','Y','Y','Y','Y','N','N','N','N','N','N','N','N','N','N','N','N','Y'),('','helpguest','mabrams','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','N','N','Y','Y','Y'),('192.168.1.222','helpguest','hgservice','Y','Y','Y','Y','N','N','N','N','N','N','N','N','N','N','N','N','Y'),('www.local','helpguest','hgservice','Y','Y','Y','Y','N','N','N','N','N','N','N','N','N','N','N','N','Y'),('developer','helpguest','hgservice','Y','Y','Y','Y','N','N','N','N','N','N','N','N','N','N','N','N','Y'),('developer.local','helpguest','hgservice','Y','Y','Y','Y','N','N','N','N','N','N','N','N','N','N','N','N','Y');
/*!40000 ALTER TABLE `db` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `host` WRITE;
/*!40000 ALTER TABLE `host` DISABLE KEYS */;
INSERT INTO `host` VALUES ('192.168.1.222','helpguest','Y','Y','Y','Y','N','N','N','N','N','N','N','N','N','N','N','N','Y'),('localhost','helpguest','Y','Y','Y','Y','N','N','N','N','N','N','N','N','N','N','N','N','Y');
/*!40000 ALTER TABLE `host` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `tables_priv` WRITE;
/*!40000 ALTER TABLE `tables_priv` DISABLE KEYS */;
INSERT INTO `tables_priv` VALUES ('%','helpguest','hlpguest','protocol_request','root@localhost','2005-11-19 06:06:48','Select','Select,Update'),('localhost','helpguest','notifier','expert_authenticator_log','root@localhost','2006-04-22 18:46:42','','Select'),('localhost','helpguest','notifier','guest_authenticator_log','root@localhost','2006-04-22 18:17:29','','Select'),('%','helpguest','notifier','guest_authenticator_log','root@localhost','2006-04-22 18:08:21','','Select'),('%','helpguest','brandManager','brand_locator','root@localhost','2006-09-05 00:39:16','Select',''),('localhost','helpguest','pageCounter','page_hits','root@localhost','2006-09-14 23:17:40','Select,Insert,Update',''),('localhost','helpguest','mktg_team','executed_searches','root@localhost','2006-09-21 15:18:11','Select','');
/*!40000 ALTER TABLE `tables_priv` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('%','hlpguest','03de67ea6422c0ea','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','','','','',0,0,0,0),('%','notifier','','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','','','','',0,0,0,0),('localhost','notifier','5b2d584e592f30a3','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','','','','',0,0,0,0),('192.168.1.222','hgservice','6ce59dae1b666aa1','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','','','','',0,0,0,0),('%','brandManager','72899a4d58a6fa43','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','','','','',0,0,0,0),('localhost','mktg_team','43275a935c8afded','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','','','','',0,0,0,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


