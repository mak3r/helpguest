/*
 * VerifyEmail.java
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.remote.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.helpguest.data.DataBase;
import com.helpguest.data.ResultHandler;
import com.helpguest.gwt.protocol.VerifyEmailOutcome;

/**
 *
 * @author mabrams
 */
public class VerifyEmail implements ResultHandler {

    private VerifyEmailOutcome SUCCESS = new VerifyEmailOutcome.Success();
    private VerifyEmailOutcome EMAIL_UNKNOWN = new VerifyEmailOutcome.FailEmailUnknown();
    private VerifyEmailOutcome KEY_DOES_NOT_MATCH = new VerifyEmailOutcome.FailKeyDoesNotMatch();
    private VerifyEmailOutcome UNVERIFIABLE_DATA = new VerifyEmailOutcome.FailUnverifiableData();
    private VerifyEmailOutcome INTERNAL_ERROR = new VerifyEmailOutcome.FailInternalError();      
    
    private VerifyEmailOutcome outcome = UNVERIFIABLE_DATA;
    private int queryId;
    private int id;
    private String submittedUid;
    
    private static final int VERIFYEMAIL = 1;
    
    private static final Logger LOG = Logger.getLogger(VerifyEmail.class);
                    
    
    /** Creates a new instance of Authenticator */
    public VerifyEmail() {
    }
    
    public VerifyEmailOutcome verify(String uid, String email) {
    	if (uid == null || email == null) {
    		return UNVERIFIABLE_DATA;
    	}
    	submittedUid = uid;
        String userQuery = "select id, uid from expert where email = ?";
        Object[] data = {email};
        DataBase.getInstance().submit(userQuery, this, data);
        
        if (outcome.equals(SUCCESS)) {
            String updateQuery = "update email_verification set is_verified = 1 where expert_id = " + id;
            DataBase.getInstance().submit(updateQuery);            
        }
        
        return outcome;
    }
    
    //prahalad adding 07/28/08 for password reset app 
    public VerifyEmailOutcome verify(String email) {
    	this.queryId = VERIFYEMAIL;
        String userQuery = "select id, uid from expert where email = '" + email + "'";
        //Object[] data = {email};
        //DataBase.getInstance().submit(userQuery, this, data);
        DataBase.getInstance().submit(userQuery, this);
        
        return outcome;
    }

    public void capture(ResultSet rs, int queryId) {
    	switch (queryId) {
		default:
    	if (rs == null) {
            LOG.debug("result set is null");
            outcome = EMAIL_UNKNOWN;
        } else {
            try {
                if (rs.next()) {
                	//check if the uid matches
                	if (submittedUid.equals(rs.getString("uid"))) {
                        id = rs.getInt("id");
                        outcome = SUCCESS;
                	} else {
                		outcome = KEY_DOES_NOT_MATCH;
                	}
                }
            } catch (SQLException sqlx) {
                LOG.error("Could not verify email.", sqlx);
                outcome = INTERNAL_ERROR;
            }
        }//end of else
		break;
		case VERIFYEMAIL:
			if (rs == null) {
	            LOG.debug("result set is null");
	            outcome = EMAIL_UNKNOWN;
	        } else {
	            try {
	                if (rs.next()) {
	                	
	                        outcome = SUCCESS;
	                	} else {
	                		outcome = EMAIL_UNKNOWN;
	                	}
	                
	            } catch (SQLException sqlx) {
	                LOG.error("Could not verify email.", sqlx);
	                outcome = INTERNAL_ERROR;
	            }
	        }//end of else
			break;
    }//end switch	
  }//end capture
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }


}
