/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Oct 20, 2007
 */
package com.helpguest.remote.service;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.HGCipher;
import com.helpguest.data.DataBase;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.ResultHandler;
import com.helpguest.gwt.protocol.SignUpOutcome;

/**
 * @author mabrams
 *
 */
public class SignUp implements ResultHandler {

	private static final SignUpOutcome SUCCESS = new SignUpOutcome.Success();
	private static final SignUpOutcome FAIL_ACCOUNT_EXISTS = new SignUpOutcome.FailAccountExists();
	private static final SignUpOutcome FAIL_HANDLE_EXISTS = new SignUpOutcome.FailHandleExists();
	private static final SignUpOutcome INTERNAL_ERROR = new SignUpOutcome.FailInternalError();
	private static final SignUpOutcome UNKNOWN_ERROR = new SignUpOutcome.FailUnknownError();
    
    private final int EMAIL = 0;
    private final int HANDLE = 1;
    private static String SIGNUP_REPLY_TO_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.SIGNUP_REPLY_TO_PREFIX.getKey());
    private static String WEBAPP_NAME = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEBAPP_NAME.getKey());
    private static String GWT_MODULE_ENTRY = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.GWT_MODULE_ENTRY.getKey());
    private static final String FULLY_QUALIFIED_DOMAIN_NAME = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private static String smtpEmail = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.SMTP_AUTH_EMAIL.getKey());
    private static String smtpPassword = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.SMTP_AUTH_PASSWORD.getKey());
    
    String realSmtpEmail = "";
    String realSmtpPassword = "";
    
    private SignUpOutcome outcome = UNKNOWN_ERROR;
    private int queryId;
    private Properties props = System.getProperties();

    private static final Logger log = Logger.getLogger("com.helpguest.servlet.SignUp1");
                    
    
    /** Creates a new instance of SignUp */
    public SignUp() {
        props.put("mail.smtp.host", "smtp.helpguest.com");
        //required for smtp auth
        props.put("mail.smtp.auth", "true");
    }
    
    public SignUpOutcome createAccount(
    		final String email, 
    		final String pass,
    		final String handle, 
    		final String offering, 
    		final String zip, 
    		final String userIP) {
        checkExisting(email, handle);
    	if (SUCCESS.equals(outcome)) {
            //insert the main expert record
            ExpertAccount.newInstance(email,pass,handle,offering,zip,userIP);

            //TODO: some of the following should be added the the creation of a new expert account
            //Insert a record for email validation
            String insertValidationQuery = "insert into email_verification (expert_id, is_verified) " + 
                                           "select id, 0 from expert where email = '" + email + "'";
            DataBase.getInstance().submit(insertValidationQuery);
            
            //Make sure inserted date is GMT
            GregorianCalendar now = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date cur = new Date(now.getTimeInMillis());
            String dbDate = sdf.format(cur);
            String insertAccountStatsQuery = "insert into expert_account_status (expert_id, end_date) " +
                                             "select id, '" + dbDate + "' from expert where email = '" + email + "'";
            DataBase.getInstance().submit(insertAccountStatsQuery);
            
            //Insert an entry for handling when the expert goes live
            String liveExpertQuery = "insert into live_expert (expert_id, is_live) " +
                    "select id, 0 from expert where email = '" + email + "'";
            DataBase.getInstance().submit(liveExpertQuery);
            
            //Create guest components that will be able to connect with this expert
            //Create a guest uid 
            String guestUid = Long.toString(System.currentTimeMillis());
            //insert the guest record
            String guestInsertQuery = "insert into guest (expert_id, uid) " +
                    "select id, '" + guestUid + "' from expert where email = '" + email + "'";
            DataBase.getInstance().submit(guestInsertQuery);

            //insert the demographics record
            String demographicsQuery = "insert into demographics(email, handle, " +
                    "expertise, zip_code, user_ip) values ('" + email + "', '" +
                    handle + "', '" + offering + "', '" + zip + "', '" + 
                    userIP + "')";
            DataBase.getInstance().submit(demographicsQuery);
            
            //Send a welcome/validation email
            sendEmail(email);
        }
        return outcome;
    }
    
  //prahalad adding 7/23/08 alternate createAccount method to handle ArrayList of catOffgTags
    public SignUpOutcome createAccount(
    		final String email, 
    		final String pass,
    		final String handle, 
    		final List catOffgTagList, 
    		final String zip, 
    		final String userIP,
    		final String referralSource) {
        checkExisting(email, handle);
    	if (SUCCESS.equals(outcome)) {
            //insert the main expert record
            //ExpertAccount.newInstance(email,pass,handle,offering,zip,userIP);
    		ExpertAccount.newInstance(email,pass,handle,catOffgTagList,zip,userIP);

            //TODO: some of the following should be added the the creation of a new expert account
            //Insert a record for email validation
            String insertValidationQuery = "insert into email_verification (expert_id, is_verified) " + 
                                           "select id, 0 from expert where email = '" + email + "'";
            DataBase.getInstance().submit(insertValidationQuery);
            
            //Make sure inserted date is GMT
            GregorianCalendar now = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date cur = new Date(now.getTimeInMillis());
            String dbDate = sdf.format(cur);
            String insertAccountStatsQuery = "insert into expert_account_status (expert_id, end_date) " +
                                             "select id, '" + dbDate + "' from expert where email = '" + email + "'";
            DataBase.getInstance().submit(insertAccountStatsQuery);
            
            //Insert an entry for handling when the expert goes live
            String liveExpertQuery = "insert into live_expert (expert_id, is_live) " +
                    "select id, 0 from expert where email = '" + email + "'";
            DataBase.getInstance().submit(liveExpertQuery);
            
            //Create guest components that will be able to connect with this expert
            //Create a guest uid 
            String guestUid = Long.toString(System.currentTimeMillis());
            //insert the guest record
            String guestInsertQuery = "insert into guest (expert_id, uid) " +
                    "select id, '" + guestUid + "' from expert where email = '" + email + "'";
            DataBase.getInstance().submit(guestInsertQuery);

            //insert the demographics record
            //FIXME how should we insert the cat offg list if at all?
            Object[] data = {
        			email,
        			handle,
        			"no primary exprtise is set at this time",
        			zip,
        			userIP,
        			referralSource
        	};
            String demographicsQuery = "insert into demographics(" +
            		"email, handle, expertise, zip_code, user_ip, referral_source) " +
            		"values (?, ?, ?, ?, ?, ?)";	            
            DataBase.getInstance().submit(demographicsQuery, data);
        
            //Send a welcome/validation email
            sendEmail(email);
        }
        return outcome;
    }
    
    private boolean checkExisting(String email, String handle) {
                
        queryId = EMAIL;
        String query = "select * from expert where email = '" + email + "'";
        DataBase.getInstance().submit(query, this);
        if (!SUCCESS.equals(outcome)) {
            return false;
        }
        
        queryId = HANDLE;
        String handleQuery = "select * from expert where handle = '" + handle + "'";
        DataBase.getInstance().submit(handleQuery, this);
        if (!SUCCESS.equals(outcome)) {
            return false;
        }
        
        return true;
    }

    public void capture(ResultSet rs, int queryId) {
        if (rs == null) {
            log.debug("result set is null");
            outcome = INTERNAL_ERROR;
        } else {
            switch (queryId) {
                case EMAIL:
                    try {
                        if (!rs.next()) {
                            //if there are no rows, the account does not exist.
                            // that's good.  We can create it.
                            outcome = SUCCESS;
                        } else {
                            outcome = FAIL_ACCOUNT_EXISTS;
                        }
                    } catch (SQLException sqlx) {
                        //This error is thrown if there is a problem accessing the database
                        log.error("Data base access error.", sqlx);
                        outcome = INTERNAL_ERROR;
                    }
                    break;
                case HANDLE:
                    try {
                        if (!rs.next()) {
                            outcome = SUCCESS;
                        } else {
                            outcome = FAIL_HANDLE_EXISTS;
                        }
                    } catch (SQLException sqlx) {
                        outcome = INTERNAL_ERROR;
                    }
                    break;
                default:
                    break;
            }
        }//end of else
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }
    
    /**
     * Send an email requesting verification of the email address provided
     */
    private void sendEmail(String email) {
        try {
            
        	HGCipher authCipher;
       	 
            byte[] random_number = { (byte) 0xc7, (byte) 0x73, (byte) 0x21, (byte) 0x8c,
               (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99 };

	       //encryption see enrcypted pass in log
            String pass1 = "password";
            authCipher = new HGCipher(pass1, random_number);
            String strEncryptedUser = smtpEmail;
            String[] arrEncryptedUser = strEncryptedUser.split(",");
      	    byte[] ourEncryptedData = new byte[arrEncryptedUser.length];
      
      	    for (int i=0;i<arrEncryptedUser.length;i++) { 
      	      //ourEncryptedData[i] = Byte.parseByte(arrEncryptedUser[i].trim());
      		  ourEncryptedData[i] = (byte)Integer.parseInt(arrEncryptedUser[i].trim());
      		
      	    }
            
      	    realSmtpEmail = authCipher.decrypt(ourEncryptedData);
            
             
             
      	    String strEncryptedPaswd = smtpPassword;
      	    String[] arrEncryptedPaswd = strEncryptedPaswd.split(",");
      	    byte[] ourEncryptedPaswd = new byte[arrEncryptedPaswd.length];
      
      	    for (int i=0;i<arrEncryptedPaswd.length;i++) { 
      		  ourEncryptedPaswd[i] = Byte.parseByte(arrEncryptedPaswd[i].trim());
      		
      	    }
            
      	    realSmtpPassword = authCipher.decrypt(ourEncryptedPaswd);
        	Session session = Session.getInstance(props, new SMTPAuthenticator());
            /**
             * use this to debug the smtp session
            session.setDebug(true);
            session.setDebugOut(new java.io.PrintStream(new java.io.File("/usr/share/tomcat/logs/MailOutDebug.txt")));
             */
            Message msg = new VerifyMessage(session);
            Address fromAddress = new InternetAddress(realSmtpEmail);
            Address toAddress = new InternetAddress(email);
            try {
                msg.setFrom(fromAddress);
            } catch (AddressException aex) {
                log.error("from email address " + fromAddress + " is invalid.", aex);
            }
            try {
                msg.addRecipient(Message.RecipientType.TO, toAddress);
            } catch (AddressException aex) {
                log.warn("email address provided " + email + " is invalid", aex);
            }

            msg.setSubject("Verify your address to use HelpGuest.");
             
            StringBuffer content = new StringBuffer();
            ExpertAccount ea = ExpertAccount.getExpertAccount(email);
            
            content.append("Thank you for bringing your skills to the HelpGuest Marketplace.\r\n\r\n");
            content.append("Verify your account by using the weblink below.\r\n\r\n");
            content.append(SIGNUP_REPLY_TO_PREFIX + "/" + WEBAPP_NAME + "/" + GWT_MODULE_ENTRY + "#public_tabs+");
            content.append("tab=become_an_expert&email=" + email + "&code=" + ea.getUid());
            content.append("\r\n\r\n");
            content.append("Details on how experts are paid at:\r\n\r\n");
            content.append(SIGNUP_REPLY_TO_PREFIX + "/" + WEBAPP_NAME + "/RateCalculation.html");
            content.append("\r\n\r\n");
            content.append("\"In the beginner's mind there are many possibilities,\r\n");
            content.append("but in the expert's mind there are few\"\r\n");
            content.append("-Shunryu Suzuki\r\n");

            try {
                msg.setDataHandler(new DataHandler(new ByteArrayDataSource(content.toString(), "text/plain")));
            } catch (IOException iox) {
                log.error("Could not add message content.", iox);
            }
            msg.setSentDate(new Date());
            Transport.send(msg);
            
        } catch (MessagingException mex) {
            log.error("Could not send email to " + email + ".", mex);
        } catch (Exception ex) {
            log.error("Message did not go out successfully.", ex);
            outcome = UNKNOWN_ERROR;
        }
    }
    
    class VerifyMessage extends MimeMessage {
        
        public VerifyMessage(Session session) {
            super(session);
        }
        
        protected void updateHeaders() throws MessagingException {
            super.updateHeaders();
            setHeader("Message-ID", "email-verification@" + FULLY_QUALIFIED_DOMAIN_NAME);
        }
    }

    class SMTPAuthenticator extends Authenticator {
        protected PasswordAuthentication getPasswordAuthentication() {
            PasswordAuthentication smtpAuth = new PasswordAuthentication(realSmtpEmail, realSmtpPassword);
            return smtpAuth;
        }
        
    }

}
