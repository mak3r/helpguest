package com.helpguest.util;

public class UIDGenerator {

	private UIDGenerator() {
		
	}
	
	public static String generate(final String seed) {
		StringBuffer buf = new StringBuffer();
		buf.append(Long.toHexString(System.currentTimeMillis()));
		if (seed != null) {
			if (seed.length() > 1) {
				//Insert the first character of seed as the first character in the UID
				buf.insert(0, seed.substring(0,1));
				//Insert the second character of seed as the last character in the UID
				buf.append(seed.substring(1, 2));
			}
		}
		return buf.toString().toUpperCase();
	}
}
