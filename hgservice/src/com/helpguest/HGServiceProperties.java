package com.helpguest;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;


public class HGServiceProperties extends Properties {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static HGServiceProperties instance = null;
	private static String HGSERVICE_PROPERTIES = "hgservice.properties";
	private static final Logger LOG = Logger.getLogger(HGServiceProperties.class);
	
	private HGServiceProperties() {
		super();
	}
	
	public static HGServiceProperties getInstance() {
		if (instance == null) {
			instance = new HGServiceProperties();
			try {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Loading hgservice properties");
				}
				instance.load(
						instance.getClass().getClassLoader().getResourceAsStream(HGSERVICE_PROPERTIES));
				if (LOG.isDebugEnabled()) {
					LOG.debug("properties loaded include");
					for (Map.Entry<Object, Object> next : instance.entrySet()) {
						LOG.debug("key: " + next.getKey() + ", value: " + next.getValue());
					}
				}
			} catch (IOException iox) {
				LOG.error("Could not load properties file: '" + 
						HGSERVICE_PROPERTIES + "'");
			}
		}
		return instance;
	}
	
	public static void override(final Properties props) {
		for (Map.Entry<Object, Object> next : props.entrySet()) {
			instance.put(next.getKey(), next.getValue());
		}
	}
	
	public enum HGServiceProperty {
		WEB_ROOT("web.root"), 
		WEB_URI_PREFIX("web.uri.prefix"),
		JAWS_APPS_DIR("jaws.app.dir"),
		FQDN("fully.qualified.domain.name"),
		SCPORT("servlet.container.port"),
		SIGNUP_REPLY_TO_PREFIX("signup.reply.to.prefix"),
		DRIVER("jdbc.driver"),
		DB_URI_PREFIX("db.uri.prefix"),
		DATABASE("db.name"),
		USER_NAME("db.user.name"),
		PASSWORD("db.password"),
		SERVER("db.server"),
		WEBAPP_NAME("webapp.name"),
		GWT_MODULE_ENTRY("gwt.module.entry"),
		IGNORE_HK_VERIFICATN("ignore.host.key.verification"),
		AVATAR_DIR("avatar.dir"),
		DESKTOP_DB_USER_NAME("desktop.db.user.name"),
		DESKTOP_DB_PASSWORD("desktop.db.password"),
		SMTP_AUTH_EMAIL("smtp.auth.email"),
		SMTP_AUTH_PASSWORD("smtp.auth.password");
		
		private String key;
		
		HGServiceProperty(String key) {
			this.key = key;
		}
		
		public String getKey() {
			return key;
		}
	}

}
