/*
 * Vocabulary.java
 *
 * Created on January 5, 2005, 5:52 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 * 
 */

package com.helpguest.protocol;

/**
 * Server vocabulary begins with SERV.
 * Client vocabulary begins with CLI.
 * Common vocabulary begins with COM.
 * Technician vocabulary begins with TECH.
 * 
 * @author  mabrams
 */
public class Vocabulary {
    
    public static final String COM_HUH = "huh";
    public static final String COM_BYE = "bye";
    public static final String COM_REQUEST = "request";
    public static final String COM_VNC = "vnc";
    public static final String COM_CHAT = "chat";
    public static final String COM_VOIP = "voip";
    public static final String COM_AGENT = "agent";
    public static final String COM_THANKS = "thanks";
    public static final String COM_INET_ADDRESS = "address";
    public static final String COM_WEB_CHAT = "web_chat";
    public static final String COM_CHATTERBOX = "chatterbox";

    public static final String CLI_WHAT_TEACHER = "what teacher";
    public static final String CLI_ANY = "any";
    public static final String CLI_ASSISTANCE = "assistance";
    public static final String CLI_CLASSROOM = "classroom";

    public static final String TECH_REQUEST_PERMISSION = "permission";
    public static final String TECH_ESCALATE = "escalate";
    public static final String TECH_CLOSE = "close";
    public static final String TECH_TECHNICIAN = "technician";
    public static final String TECH_TEACHER = "teacher";
    public static final String TECH_GURU = "guru";
    public static final String TECH_RESOLVED = "resolved";
    public static final String TECH_INCOMPLETE = "incomplete";
    public static final String TECH_YES = "yes";
    public static final String TECH_NO = "no";
    public static final String TECH_IM_ALIVE = "alive";
    
    public static final String SERV_WHAT_SERVICE = "service?";
    public static final String SERV_REQUEST_TYPE = "rtype?";
    public static final String SERV_WHAT_CLASS = "class?";
    public static final String SERV_WHAT_IS_THE_PROBLEM = "problem?";
    public static final String SERV_PERMISSION_TYPE = "ptype?";
    public static final String SERV_WHAT_ISSUE_ID = "issue?";
    public static final String SERV_NAME = "name?";
    public static final String SERV_WHAT_CLASS_DESCRIPTION = "description?";
    public static final String SERV_HOW_MANY_STUDENTS_MAX = "students?";
    public static final String SERV_WHAT_REASON = "reason?";
    public static final String SERV_STATUS = "status?";
    public static final String SERV_DESCRIBE_RESOLUTION = "description?";
    public static final String SERV_WELCOME = "welcome";
    public static final String SERV_SERVICE_UNAVAILABLE = "0";
    public static final String SERV_WHAT_UID = "uid?";
    
    public static final String LOGOUT = "logout";
    public static final char DELIMITER = 0x07;
    
    /** Creates a new instance of Vocabulary */
    public Vocabulary() {
    }
    
}
