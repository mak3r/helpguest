/*
 * RequestProtocol.java
 *
 * Created on January 5, 2005, 5:25 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 */

package com.helpguest.protocol;

import com.helpguest.data.ClientDataSet;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author  mabrams
 */
public class RequestProtocol implements HelpGuestProtocol {
    
    static Collection<String> requestTypes = new ArrayList<String>();
    private final int DEFAULT = 0;
    private final int MESSAGE = 1;
    private final int NAME = 2;
    private final int SERVICE = 3;
    private final int PERMISSION = 4;
    private final int UID = 5;
    private int state = DEFAULT;
    
    HelpGuestProtocol servicesProtocol = null;
    HelpGuestProtocol permissionProtocol = null;
    
    static {
        requestTypes.add(Vocabulary.CLI_ASSISTANCE);
        requestTypes.add(Vocabulary.CLI_CLASSROOM);
        requestTypes.add(Vocabulary.TECH_REQUEST_PERMISSION);
        requestTypes.add(Vocabulary.TECH_ESCALATE);
        requestTypes.add(Vocabulary.TECH_CLOSE);        
    }
    /** Creates a new instance of RequestProtocol */
    public RequestProtocol() {
    }
    
    public String processInput(String theInput, ClientDataSet cds) {
        String theOutput = Vocabulary.SERV_REQUEST_TYPE;

        switch (state) {
            case MESSAGE: {
                //Capture the message
                cds.setMessage(theInput);
                
                //reply
                theOutput = Vocabulary.SERV_NAME;
                this.state = NAME;
                break;
            } 
            case NAME: {
                //Capture the name
                cds.setUname(theInput);

                //reply
                theOutput = Vocabulary.SERV_WHAT_UID;
                this.state = UID;
                break;
            }
            case UID: {
                //capture the unique id
                cds.setUid(theInput);
                
                //reply
                theOutput = Vocabulary.SERV_WHAT_SERVICE;
                this.state = SERVICE;
                //initialize the services protocol
                servicesProtocol = new ServicesProtocol();
                break;
            }
            case SERVICE: {
                theOutput = servicesProtocol.processInput(theInput, cds);
                this.state = SERVICE;
                break;
            } 
            case PERMISSION: {
                theOutput = permissionProtocol.processInput(theInput, cds);
                this.state = PERMISSION;
                break;
            }
            default: {
                if (theInput == null) {
                    //This is an error prepare to exit
                    theOutput = Vocabulary.COM_BYE;            
                } else if (theInput.equals(Vocabulary.CLI_ASSISTANCE)) {
                    theOutput = Vocabulary.SERV_WHAT_IS_THE_PROBLEM;            
                    this.state = MESSAGE;
                } else if (theInput.equals(Vocabulary.TECH_REQUEST_PERMISSION)) {
                    theOutput = Vocabulary.SERV_PERMISSION_TYPE;
                    permissionProtocol = new PermissionProtocol();
                    this.state = PERMISSION;
                } else if (theInput.equals(Vocabulary.TECH_CLOSE)) {
                    //handled by ExpertAgent
                } else if (theInput.equals(Vocabulary.CLI_CLASSROOM)) {

                } else if (theInput.equals(Vocabulary.TECH_ESCALATE)) {
                }
            }//end of default
        }//end of switch 
        return theOutput;
    }
    
    public void setStateTo(int state) {
        //state change is not allowed from other classes for this protocol
    }
    
}
