/*
 * lexicon.java
 *
 * Created on January 5, 2005, 5:52 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 *
 */

package com.helpguest.protocol;

/**
 * Server lexicon begins with SERV.
 * Client lexicon begins with CLI.
 * Common lexicon begins with COM.
 * Technician lexicon begins with TECH.
 *
 * @author  mabrams
 */
public class Lexicon {
    
    public static enum ExpertState {
        OFFLINE("offline"),
        ACTIVE("active");
        
        private String state;
        ExpertState(String state) {
            this.state = state;
        }
        
        @Override
	public String toString() {
            return state;
        }
        
    }
    
    public static enum Server {
        SERV_WHAT_SERVICE("service?"),
        SERV_REQUEST_TYPE("rtype?"),
        SERV_WHAT_CLASS("class?"),
        SERV_WHAT_IS_THE_PROBLEM("problem?"),
        SERV_PERMISSION_TYPE("ptype?"),
        SERV_WHAT_ISSUE_ID("issue?"),
        SERV_NAME("name?"),
        SERV_WHAT_CLASS_DESCRIPTION("description?"),
        SERV_HOW_MANY_STUDENTS_MAX("students?"),
        SERV_WHAT_REASON("reason?"),
        SERV_STATUS("status?"),
        SERV_DESCRIBE_RESOLUTION("description?"),
        SERV_WELCOME("welcome"),
        SERV_SERVICE_UNAVAILABLE("0"),
        SERV_WHAT_UID("uid?");
        
        private String message;
        Server(String message) {
            this.message = message;
        }
        
        @Override
	public String toString() {
            return message;
        }
    }// end of server enum
    
    public static enum Common {
        
        COM_HUH("huh"),
        COM_BYE("bye"),
        COM_REQUEST("request"),
        COM_VNC("vnc"),
        COM_CHAT("chat"),
        COM_VOIP("voip"),
        COM_AGENT("agent"),
        COM_THANKS("thanks"),
        COM_INET_ADDRESS("address"),
        COM_WEB_CHAT("web_chat");
        
        private String message;
        Common(String message) {
            this.message = message;
        }
        
        @Override
        public String toString() {
            return message;
        }
    }
    
    public static enum Client {
        WHAT_TEACHER("what teacher"),
        ANY("any"),
        ASSISTANCE("assistance"),
        CLASSROOM("classroom"),
        SURVEY ("survey");
        
        private String message;
        Client(String message) {
            this.message = message;
        }
        
        @Override
	public String toString() {
            return message;
        }
    }
    public static enum Technician {
        TECH_REQUEST_PERMISSION("permission"),
        TECH_ESCALATE("escalate"),
        TECH_CLOSE("close"),
        TECH_TECHNICIAN("technician"),
        TECH_TEACHER("teacher"),
        TECH_GURU("guru"),
        TECH_RESOLVED("resolved"),
        TECH_INCOMPLETE("incomplete"),
        TECH_YES("yes"),
        TECH_NO("no"),
        TECH_IM_ALIVE("alive"),
        START_SESSION("start_session");
        
        private String message;
        Technician(String message) {
            this.message = message;
        }
        
        @Override
	public String toString() {
            return message;
        }
        
        public int length() {
            return message.length();
        }
    }
    
    public static enum Guest {
        STOP_SESSION("stop_session"),
        START_SESSION("guest_start");
        
        private String message;
        Guest(String message) {
            this.message = message;
        }

        @Override
		public String toString() {
            return message;
        }

        public int length() {
            return message.length();
        }
    }
    
    public static final String LOGOUT = "logout";
    public static final char DELIMITER = 0x07;
    
    /** Creates a new instance of lexicon */
    private Lexicon() {
    }
    
}
