/*
 * HelpGuestProtocol.java
 *
 * Created on January 5, 2005, 5:22 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 */

package com.helpguest.protocol;

import com.helpguest.data.ClientDataSet;

/**
 *
 * @author  mabrams
 */
public interface HelpGuestProtocol {
    
    /**
     * Implementations of this method should have 3 basic steps.
     * 1. evaluate the input and possibly take some action(s)
     * 2. set the state of the protocol
     * 3. return output based on the input
     */
    public String processInput(String theInput, ClientDataSet cds);
    
    
}
