/*
 * RequestProtocol.java
 *
 * Created on January 5, 2005, 5:25 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 */

package com.helpguest.protocol;

import com.helpguest.service.AgentServer;
import com.helpguest.service.AgentServer.AgentType;
import com.helpguest.service.HGPortFactory;
import com.helpguest.service.Service;
import com.helpguest.data.ClientDataSet;
import com.helpguest.service.HGPort;

import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author  mabrams
 */
public class PermissionProtocol implements HelpGuestProtocol {

    private static final int ISSUE = 0;
    private static final int REQUEST = 1;
    private static final int SERVICES = 2;
    private static final int BYE = 3;
    private static final int CHATTERBOX = 4;
    private int state = REQUEST;
        
    HelpGuestProtocol chatterboxProtocol = new ChatterboxProtocol();
    
    private Map portList = null;
    private static final Logger log = Logger.getLogger("com.helpguest.protocol.PermissionProtocol");
    
    /** Creates a new instance of RequestProtocol */
    public PermissionProtocol() {
    }
    
    public String processInput(String theInput, ClientDataSet cds) {
        String theOutput = Vocabulary.SERV_PERMISSION_TYPE;

        //it doesn't matter what state we're in if the client says thanks or bye.
        if (Vocabulary.COM_BYE.equals(theInput) || Vocabulary.COM_THANKS.equals(theInput)) {
	    if (log.isDebugEnabled()) {log.debug("theInput: " + theInput + " state is now BYE");}
            state = BYE;
        }

        switch (state) {
            case ISSUE: 
                if (log.isDebugEnabled()) {log.debug("entering state ISSUE.");}
                String uid = theInput;
                cds.setUid(uid);
                portList = cds.getDataForId(uid);
                if (portList.keySet().size() == 0) {
                    // no services found
                    theOutput = Vocabulary.SERV_SERVICE_UNAVAILABLE;
                    state = BYE;
                } else {
                    theOutput = Vocabulary.SERV_WHAT_SERVICE;
                }
                state = SERVICES;
                if (log.isDebugEnabled()) {log.debug("exiting state ISSUE.");}
                break;
            case SERVICES:
                if (log.isDebugEnabled()) {log.debug("entering state SERVICES.");}
                if (Vocabulary.COM_INET_ADDRESS.equals(theInput)) {
                    theOutput = cds.getInetAddress().getHostAddress();
                } else {
                    HGPort hgPort = (HGPort)portList.get(theInput);
                    //If the service is agent, dont use the guest port, 
                    // use an expert specific port
                    if (Vocabulary.COM_AGENT.equals(theInput)) {
                        theOutput = HGPortFactory.getPortAsString(Service.getService(theInput));
                        //Start the agent server
                        if (log.isDebugEnabled()) {
                            log.debug("initiating expert agent on port " + theOutput + ".");
                        }
                        //find out the expert that is working with this guest.
                        String eUid = cds.getExpertUid(cds.getUid());
                        try {
                            new AgentServer(AgentType.EXPERT, Integer.parseInt(theOutput), eUid);
                        } catch (Exception ex) {
                            log.error("Error initiating the AgentServer with expert id: " + eUid, ex);
                        }
                    } else {
                        theOutput = Integer.toString(hgPort.getPortNumber());
                    }
                }
            if (log.isDebugEnabled()) {log.debug("exiting state SERVICES.");}
                break;
            case REQUEST: 
                if (log.isDebugEnabled()) {log.debug("entering state REQUEST.");}
                if (Vocabulary.TECH_TECHNICIAN.equals(theInput)) {
                    theOutput = Vocabulary.SERV_WHAT_ISSUE_ID;
                    state = ISSUE;
                } else if (Vocabulary.TECH_TEACHER.equals(theInput)) {
                    //handle teacher permission type
                } else if (Vocabulary.TECH_GURU.equals(theInput)) {
                    //handle guru permission type
                } else if (Vocabulary.COM_CHATTERBOX.equals(theInput)){
                	//Find out the name we are to use
                	theOutput = Vocabulary.SERV_NAME;
                	state = CHATTERBOX;
                }
                if (log.isDebugEnabled()) {log.debug("exiting state REQUEST.");}
                
                break;
            case CHATTERBOX:
                if (log.isDebugEnabled()) {log.debug("entering state CHATTERBOX.");}
                theOutput = chatterboxProtocol.processInput(theInput, cds);
                if (log.isDebugEnabled()) {log.debug("exiting state CHATTERBOX.");}
                break;
            case BYE:
            default:
                if (log.isDebugEnabled()) {log.debug("entering state BYE/default.");}
                theOutput = Vocabulary.COM_BYE;
                //reset the state.
                state = REQUEST;
                if (log.isDebugEnabled()) {log.debug("exiting state BYE/default.");}
                break;
           
        }
        return theOutput;
    }
        
}
