/**
 * 
 */
package com.helpguest.protocol;

import com.helpguest.data.ClientDataSet;
import com.helpguest.data.ExpertAccount;
import com.helpguest.service.HGPortFactory;
import com.helpguest.service.Service;

/**
 * @author mabrams
 *
 */
public class ChatterboxProtocol implements HelpGuestProtocol {

	private String theOutput = null;
	
	/**
	 * @parram theInput is expected to be the name (email address of an expert).
	 * insert valid values into the protocol_request and protocol_response
	 * tables and return the port number to use for web_chat - the
	 * only service used in the chatterbox protocol.
	 * @param cds the client data set to use.
	 * @return the port number to use for the chat session.
	 */
	public String processInput(String theInput, ClientDataSet cds) {
		
		//Insert the name and new port numbers into the 
		//protocol request/response
		theOutput = HGPortFactory.getPortAsString(Service.WEB_CHAT);
        
		ExpertAccount ea = ExpertAccount.getExpertAccount(theInput);
		
		//Set the user name
		cds.setUname(theInput);
		cds.setMessage("chatterbox initial message");
		cds.setUid(ea.getGuestUid());

		//record the service and port used
        cds.getProtocolRequestForType(Service.WEB_CHAT, theOutput);

		// TODO Auto-generated method stub
		return theOutput;
	}

}
