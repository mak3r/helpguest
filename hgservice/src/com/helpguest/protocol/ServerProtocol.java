/**
 * HelpGuest Technologies, Inc. Copyright 2005
 */
package com.helpguest.protocol;

import com.helpguest.data.ClientDataSet;

public class ServerProtocol implements HelpGuestProtocol {
    private final int DEFAULT = 0;
    private final int REQUEST_TYPE = 1;
    private int state = DEFAULT;
    
    HelpGuestProtocol requestProtocol = new RequestProtocol();
    
    public String processInput(String theInput, ClientDataSet cds) {
        String theOutput = Vocabulary.COM_BYE;

        switch (state) {
            
            case REQUEST_TYPE: {
                theOutput = requestProtocol.processInput(theInput, cds);
                this.state = REQUEST_TYPE;
                break;
            }                
            default: {
                if (theInput == null) {
                    theOutput = Vocabulary.SERV_WELCOME;
                } else if (theInput.equals(Vocabulary.COM_REQUEST)) {
                    theOutput = Vocabulary.SERV_REQUEST_TYPE;
                    state = REQUEST_TYPE;
                } 
                break;
            }
        }
        return theOutput;
    }

    /**
     * Allows caller to set the state of this protocol.
     * @param state is the state to set this protocol to.
     */
    public void setStateTo(int state) {
        this.state = state;
    }
    
}
