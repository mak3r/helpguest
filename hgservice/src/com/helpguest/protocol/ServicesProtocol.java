/*
 * RequestProtocol.java
 *
 * Created on January 5, 2005, 5:25 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 */

package com.helpguest.protocol;

import com.helpguest.service.AgentServer;
import com.helpguest.service.AgentServer.AgentType;
import com.helpguest.service.HGPortFactory;
import com.helpguest.service.Service;
import com.helpguest.data.ClientDataSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author  mabrams
 */
public class ServicesProtocol implements HelpGuestProtocol {
    
    static Collection<String> serviceTypes = new ArrayList<String>();
    private Map previouslyRequested = new HashMap();
    private final int DEFAULT = 0;
    private final int NEXT_SERVICE = 1;
    private int state = NEXT_SERVICE;
    
    private static final int MAX_CONFUSION = 10;
    private int repeatFailures = 0;
    private String theOutput = Vocabulary.COM_HUH;
    
    private static final Logger log = Logger.getLogger("com.helpguest.protocol.ServicesProtocol");
    
    static {
        serviceTypes.add(Vocabulary.COM_VNC);
        serviceTypes.add(Vocabulary.COM_CHAT);
        serviceTypes.add(Vocabulary.COM_VOIP);
        serviceTypes.add(Vocabulary.COM_AGENT);
    }
    /** Creates a new instance of RequestProtocol */
    public ServicesProtocol() {
        repeatFailures = 0;
    }
    
    public String processInput(String theInput, ClientDataSet cds) {
        String theOutput = Vocabulary.COM_HUH;
        
        switch (state) {
            case NEXT_SERVICE: {
                if (log.isDebugEnabled()) {log.debug("service input value: " + theInput);}
                if (serviceTypes.contains(theInput)) {
                    //if we have already returned 'theInput' then be sure to return
                    // the same value we returned before.
                    if (previouslyRequested.keySet().contains(theInput)) {
                        theOutput = (String)previouslyRequested.get(theInput);
                    } else if (!Vocabulary.COM_VOIP.equals(theInput)) {
                        //all tunneled services need a port number
                        // currently voip is the only non-tunneled service
                        theOutput = HGPortFactory.getPortAsString(Service.getService(theInput));
                        //add it to the map
                        previouslyRequested.put(theInput,theOutput);
                        this.state = NEXT_SERVICE;
                        
                        //record the service and port used
                        cds.getProtocolRequestForType(Service.getService(theInput), theOutput);
                        
                        //if the service is Vocabulary.COM_AGENT start the GuestAgent
                        if (Vocabulary.COM_AGENT.equals(theInput)) {
                            if (log.isDebugEnabled()) {
                                log.debug("initiating guest agent on port " + theOutput + ".");
                            }
                            new AgentServer(AgentType.GUEST, Integer.parseInt(theOutput), cds.getUid());
                        }
                    } else {
                        //this is the voip service request
                        //currently fixed to 22222
                        //@TODO: have the client discover available ports > 22220
                        theOutput = "22222";
                        previouslyRequested.put(theInput, theOutput);
                        this.state = NEXT_SERVICE;
                    }
                } else if (theInput.equals(Vocabulary.COM_THANKS)) {
                    theOutput = Vocabulary.COM_BYE;
                    this.state = DEFAULT;
                } else {
                    if (log.isDebugEnabled()) {log.debug("next service case (final else): " + theInput);}
                    //count how many times this repeated.
                    // send bye if repeated more than MAX_CONFUSION
                    if (MAX_CONFUSION > ++repeatFailures) {
                        theOutput = Vocabulary.SERV_WHAT_SERVICE;
                    } else {
                        theOutput = Vocabulary.COM_BYE;
                    }
                    this.state = NEXT_SERVICE;
                }
                break;
            }
            default: {
                if (log.isDebugEnabled()) {log.debug("default case: " + theInput);}
                //initialize the map
                previouslyRequested = new HashMap();
                theOutput = Vocabulary.SERV_WHAT_SERVICE;
                this.state = DEFAULT;
                break;
            }
        }
        return theOutput;
    }
    
}
