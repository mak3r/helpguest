/*
 * SessionAssistant.java
 *
 * Created on January 2, 2007, 10:51 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest;

import com.helpguest.data.Rates;
import com.helpguest.data.Session;
import java.text.DecimalFormat;
import org.apache.log4j.Logger;

public class SessionAssistant {
    
    public static final float HELPGUEST_PERCENT = .179f;
    private Session session = null;
    private float totalValue = 0.0f;
    
    private static final DecimalFormat df = new DecimalFormat("#,###,###.00");
    private static final Logger log = Logger.getLogger("com.helpguest.SessionAssistant");
    
    /** Creates a new instance of SessionAssistant */
    public SessionAssistant(Session session) {
        this.session = session;
        this.totalValue = calculateTotalValue();
    }

    private float calculateTotalValue() {
        float totalValue = 0.0f;
        if (session.isClosed()) {
            Rates rates = session.getRates();

            int minutes = getDurationInMinutes();
            if (minutes <= rates.getMinTime()) {
                //minutes is less than or equal to the minimum time period
                totalValue = rates.getMinFee();
            } else if (minutes > rates.getPreferredTime()) {
                //minutes is greater than the max amount of time
                totalValue = rates.getPreferredTime() * rates.getMinRate();
            } else {
                //minutes is less then the max time but more than the min time
                totalValue = rates.getMinFee() + ((minutes-rates.getMinTime())*rates.getMinRate());
            }
                        
        }
        return totalValue;        
    }
    
    private long calculateDuration() {
        return session.getEnd().getTime() - session.getStart().getTime();
    }
    
    public float getTotalValue() {
        return totalValue;
    }
    
    public float getExpertAmount() {
        return totalValue - getHelpGuestAmount();
    }
    
    public float getHelpGuestAmount() {
        return totalValue * HELPGUEST_PERCENT;
    }
    
    public String calculateFormattedTotalValue() {
        return df.format(totalValue);
    }
    
    public String getFormattedExpertAmount() {
        return df.format(getExpertAmount());
    }
    
    public String getFormattedHelpGuestAmount() {
        return df.format(getHelpGuestAmount());
    }
    
    public int getDurationInMinutes() {
        return (int)(Math.ceil(calculateDuration()/60000.0));
    }
}
