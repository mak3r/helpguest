package com.helpguest.gwt.search;

//this class will be used to search for experts within a category
//prahalad 6/16/08

public class CategoryConstraints extends QueryConstraints{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7012858826220027291L;
	
	private static final String DESIGN = "design";
	private static final String DEVELOPMENT = "development";
	private static final String BUSINESS = "business";
	private static final String SYSTEMS = "systems";
	private static final String PERSONAL = "personal";
	
	private String categoryName;
	
	public void setCategoryName(String anyCategoryName)
	{ categoryName = anyCategoryName;}
	
	public String getCategoryName()
	{return categoryName;}

}
                                                                                                                 