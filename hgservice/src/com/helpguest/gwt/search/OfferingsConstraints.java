package com.helpguest.gwt.search;

//this class will be used to search for experts within a offering
//prahalad 6/16/08

public class OfferingsConstraints extends QueryConstraints{
	
/**
	 * 
	 */
	private static final long serialVersionUID = -5838631790406756874L;
	

	private String offeringName;

	public void setOfferingName(String anyOfferingName) {
		offeringName = anyOfferingName;
	}

	public String getOfferingName() {
		return offeringName;
	}

}
