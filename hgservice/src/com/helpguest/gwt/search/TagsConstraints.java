package com.helpguest.gwt.search;

//this class will be used to search for experts within a tag
//prahalad 6/16/08

public class TagsConstraints extends QueryConstraints{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7623302645365853471L;
	
	private String tagName;
	private CategoryConstraints categoryConstraints;
	private OfferingsConstraints offeringConstraints;

	public void setTagName(String someTagName) {
		tagName = someTagName;
	}

	public String getTagName() {
		return tagName;
	}

	public CategoryConstraints getCategoryConstraints() {
		return categoryConstraints;
	}

	public void setCategoryConstraints(CategoryConstraints categoryConstraints) {
		this.categoryConstraints = categoryConstraints;
	}

	public OfferingsConstraints getOfferingConstraints() {
		return offeringConstraints;
	}

	public void setOfferingConstraints(OfferingsConstraints offeringConstraints) {
		this.offeringConstraints = offeringConstraints;
	}
	
	

}
