/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 8, 2007
 */
package com.helpguest.gwt.protocol;

import java.io.Serializable;

/**
 * @author mabrams
 *
 */
public class LoginType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	
	public LoginType() {
		
	}
		
	public LoginType(final String name) {
		this.name = name;
	}

	
	public static class Admin extends LoginType {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Admin() {
			super("admin");
		}
	}
    public static class Expert extends LoginType {

    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Expert() {
    		super("expert");
    	}
    }
    public static class Client extends LoginType {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Client() {
    		super("client");
    	}
    }
    public static class Unknown extends LoginType {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Unknown() {
    		super("unknown");
    	}
    }
    
	/**
	 * @param prefix the prefix to set
	 */
	public String toString() {
		return name;
	}
	
	/**
	 * @param cmt ChatterboxMessageType
	 * @return true if they are equal.
	 */
	public boolean equals(final LoginType loginType) {
		if (this.toString().equals(loginType.toString())){
			return true;
		}
		return false;
	}
    
}
