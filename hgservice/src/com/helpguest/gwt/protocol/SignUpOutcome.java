/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 8, 2007
 */
package com.helpguest.gwt.protocol;

import java.io.Serializable;

/**
 * @author mabrams
 *
 */
public class SignUpOutcome implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String outcome;
	
	public SignUpOutcome() {
		
	}
	
	public SignUpOutcome(final String outcome) {
		this.outcome = outcome;
	}
	
	public static class Success extends SignUpOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public Success() {
			super("Account Created.");
		}
	}
	
	public static class FailAccountExists extends SignUpOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailAccountExists() {
			super("Sign up failed.  Account e-mail address already in use.");
		}
	}
	
	public static class FailHandleExists extends SignUpOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailHandleExists() {
			super("Sign up failed.  Handle is already in use.");
		}
	}
	
	public static class FailUnknownError extends SignUpOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailUnknownError() {
			super("Unknown error performing sign up. \nPlease contact your HelpGuest representative.");
		}
	}
	
	public static class FailInternalError extends SignUpOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailInternalError() {
			super("Internal Error.  Failed trying to use result set.");
		}
	}
	
    

	public String getMessage() {
		return outcome;
	}

	/**
	 * @param prefix the prefix to set
	 */
	public String toString() {
		return outcome;
	}
	
	/**
	 * @param authOutcome SignUpOutcome
	 * @return true if they are equal.
	 */
	public boolean equals(final SignUpOutcome authOutcome) {
		if (this.toString().equals(authOutcome.toString())){
			return true;
		}
		return false;
	}

	
}
