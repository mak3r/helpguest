/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 11, 2007
 */
package com.helpguest.gwt.protocol.chatterbox;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.ChatterboxParty;
import com.helpguest.gwt.protocol.ChatterboxMessageType.HelpGuestInfo;

/**
 * @author mabrams
 *
 */
public class HelpGuestInfoMessage extends ChatterboxMessage implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6087552047354346652L;

	public HelpGuestInfoMessage() {
		this(new ChatterboxParty());
	}
	
	public HelpGuestInfoMessage(final ChatterboxParty host) {
		super(host, new HelpGuestInfo());
	}
}
