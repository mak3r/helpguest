/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 11, 2007
 */
package com.helpguest.gwt.protocol.chatterbox;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public class KeepAliveMessage extends ExpertMessage implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1L;

	public KeepAliveMessage() {
		this(new ChatterboxParty());
	}
	
	public KeepAliveMessage(final ChatterboxParty host) {
		super(host);
		this.setMessage("alive\n");
	}
}
