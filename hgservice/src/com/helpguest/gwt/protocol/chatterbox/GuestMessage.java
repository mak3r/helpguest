/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 11, 2007
 */
package com.helpguest.gwt.protocol.chatterbox;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.ChatterboxParty;
import com.helpguest.gwt.protocol.ChatterboxMessageType.Guest;

/**
 * @author mabrams
 *
 */
public class GuestMessage extends ChatterboxMessage implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1976669668496051200L;

	public GuestMessage() {
		this(new ChatterboxParty());
	}
	
	public GuestMessage(final ChatterboxParty host) {
		super(host, new Guest());
	}
}
