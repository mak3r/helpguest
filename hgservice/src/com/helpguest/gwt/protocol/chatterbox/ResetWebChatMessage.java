/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 14, 2007
 */
package com.helpguest.gwt.protocol.chatterbox;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public class ResetWebChatMessage extends HelpGuestInfoMessage implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 8124171297645562783L;

	public ResetWebChatMessage() {
		this(new ChatterboxParty());
	}
	
	public ResetWebChatMessage(final ChatterboxParty host) {
		super(host);
		this.setMessage("Web chat reset.\n");
	}
}
