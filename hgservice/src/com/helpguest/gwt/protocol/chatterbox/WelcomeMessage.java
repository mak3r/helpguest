/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 14, 2007
 */
package com.helpguest.gwt.protocol.chatterbox;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public class WelcomeMessage extends HelpGuestInfoMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8796011988394696100L;

	public WelcomeMessage() {
		this(new ChatterboxParty());
	}
	
	public WelcomeMessage(final ChatterboxParty host) {
		super(host);
		this.setMessage("Welcome to HelpGuest web chat.\n");
	}
}
