/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 11, 2007
 */
package com.helpguest.gwt.protocol.chatterbox;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.ChatterboxParty;
import com.helpguest.gwt.protocol.ChatterboxMessageType.Expert;

/**
 * @author mabrams
 *
 */
public class ExpertMessage extends ChatterboxMessage implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4629047978893390014L;

	public ExpertMessage() {
		this(new ChatterboxParty());
	}
	
	public ExpertMessage(final ChatterboxParty host) {
		super(host, new Expert());
	}
}
