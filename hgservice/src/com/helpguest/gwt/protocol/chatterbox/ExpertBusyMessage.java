/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 14, 2007
 */
package com.helpguest.gwt.protocol.chatterbox;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public class ExpertBusyMessage extends HelpGuestInfoMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3289691905973464306L;

	public ExpertBusyMessage() {
		this(new ChatterboxParty());	
	}
	
	public ExpertBusyMessage(final ChatterboxParty host) {		
		super(host);
		this.setMessage("This Expert is unavailable to chat at this time.\n");		
	}
}
