/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 14, 2007
 */
package com.helpguest.gwt.protocol.chatterbox;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public class GuestExitMessage extends HelpGuestInfoMessage implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -127530919075988622L;

	public GuestExitMessage() {
		this(new ChatterboxParty());
	}
	
	public GuestExitMessage(final ChatterboxParty host) {
		super(host);
		this.setMessage("Guest exited web chat.\n");
	}
}
