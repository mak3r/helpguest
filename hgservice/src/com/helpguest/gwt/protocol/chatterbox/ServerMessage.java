/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 11, 2007
 */
package com.helpguest.gwt.protocol.chatterbox;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public class ServerMessage extends ChatterboxMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5726265444167886908L;

	public ServerMessage() {
		this(new ChatterboxParty());
	}
	
	public ServerMessage(final ChatterboxParty host) {
		super(host, ChatterboxMessage.SERVER);
	}
}
