/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 3, 2007
 */
package com.helpguest.gwt.protocol;

import java.io.Serializable;


/**
 * @author mabrams
 *
 */
public class ChatterboxMessageType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6987382507622118876L;
	private String prefix = "";
	
	public ChatterboxMessageType() {
		
	}
	
	protected ChatterboxMessageType(final String prefix) {
		this.prefix = prefix;
	}
	
	public static class Server extends ChatterboxMessageType {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Server() {
			super("Server");
		}
	}

	public static class Guest extends ChatterboxMessageType {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Guest() {
			super("Guest");
		}
	}
	
	public static class Expert extends ChatterboxMessageType {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Expert() {
			super("Expert");
		}
	}
	
	public static class HelpGuestInfo extends ChatterboxMessageType {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public HelpGuestInfo() {
			super("HelpGuestInfo");
		}
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix the prefix to set
	 */
	public String toString() {
		return prefix;
	}
	
	/**
	 * @param cmt ChatterboxMessageType
	 * @return true if they are equal.
	 */
	public boolean equals(final ChatterboxMessageType cmt) {
		if (this.toString().equals(cmt.toString())){
			return true;
		}
		return false;
	}
}
