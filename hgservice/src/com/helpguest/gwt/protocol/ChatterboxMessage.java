/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 3, 2007
 */
package com.helpguest.gwt.protocol;

import java.io.Serializable;

import com.helpguest.gwt.protocol.ChatterboxMessageType.Expert;
import com.helpguest.gwt.protocol.ChatterboxMessageType.Guest;
import com.helpguest.gwt.protocol.ChatterboxMessageType.HelpGuestInfo;
import com.helpguest.gwt.protocol.ChatterboxMessageType.Server;


/**
 * @author mabrams
 *
 */
public class ChatterboxMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2975967265505492291L;
	
	public static final ChatterboxMessageType SERVER = new Server();
	public static final ChatterboxMessageType EXPERT = new Expert();
	public static final ChatterboxMessageType GUEST = new Guest();
	public static final ChatterboxMessageType HELP_GUEST_INFO = new HelpGuestInfo();
	
	private ChatterboxMessageType messageType;
	private String message;	
	private ChatterboxParty party;
	private String sender;
	private long timestamp;
	

	/**
	 * This ctor is to be used by the serialization
	 * mechanism.  Use of this ctor to create an instance
	 *  by clients will result in the inability to set a party
	 */
	public ChatterboxMessage() {
	}
	
	/**
	 * @param party The party this message is working with
	 * @param messageType the messageType of this message
	 */
	public ChatterboxMessage(final ChatterboxParty party,
			final ChatterboxMessageType messageType) {
		this.party = party;
		setMessageType(messageType);
	}
	
	/**
	 * @return the expertUid
	 */
	public String getExpertUid() {
		return party.getExpertUid();
	}
	/**
	 * @return the guestUid
	 */
	public String getGuestUid() {
		return party.getGuestUid();
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

        /**
         * Acts as if the setMessage(String, boolean)
         * was called with the second argument set to true.
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
        /**
	 * @param message the message to set
         * @param markTime if true has the effect of setting
         * the timestamp to now
	 */
	public void setMessage(String message, boolean markTime) {
            if(markTime) {
                timestamp = System.currentTimeMillis();
            }
            this.message = message;
	}
	
        /**
	 * @return the messageType
	 */
	public ChatterboxMessageType getMessageType() {
		return messageType;
	}
	/**
	 * @param messageType the messageType to set
	 */
	private void setMessageType(ChatterboxMessageType messageType) {
		this.messageType = messageType;
		if (messageType.equals(EXPERT)) {
			this.sender = party.getHandle();
		} else if (messageType.equals(GUEST)) {
			this.sender = party.getGuestName();
		} else {
			//This would be the case for SERVER
			//or HELP_GUEST_INFO
			this.sender = messageType.getPrefix();
		}
	}
	
	public void switchMessageType(ChatterboxMessageType messageType) {
		setMessageType(messageType);
	}
	
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return party.getSessionId();
	}
	/**
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}
	

	/**
	 * @return the expertHandle
	 */
	public String getExpertHandle() {
		return party.getHandle();
	}

	/**
	 * @return the guestName
	 */
	public String getGuestName() {
		return party.getGuestName();
	}
	/**
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}

	public Object clone() {
		ChatterboxMessage clonedMessage =
			new ChatterboxMessage(this.party, this.messageType);
		return clonedMessage;
	}
	
	public boolean equals(final Object chatterboxMessage) {
		if (!(chatterboxMessage instanceof ChatterboxMessage)) {
			return false;
		}
		ChatterboxMessage msg = (ChatterboxMessage) chatterboxMessage;
		return ChatterboxMessage.getEqualStatement(msg).equals(getEqualStatement(this));
	}
	
	private static String getEqualStatement(final ChatterboxMessage chatterboxMessage) {
		return chatterboxMessage.message + chatterboxMessage.messageType;
	}
	
	public String toString() {
		return "[" +
				"messageType:" + messageType + 
				", message:" + message + 
				", party:" + party + 
				", sender:" + sender + 
				", timestamp:" + timestamp + 
				"]";
	}

}
