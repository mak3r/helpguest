/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 14, 2007
 */
package com.helpguest.gwt.protocol;

import java.io.Serializable;


/**
 * Overriding classes should set the member variables
 * explicitly - preferable in the ctor of the subclass.
 * There are no setters for this reason.
 * To work with non-customized serialized streams
 * do not overwrite getters or the functionality may be lost.
 * 
 * @author mabrams
 *
 */
public class ChatterboxParty implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4756295049218034916L;

	protected String expertUid;
	protected String guestName;
	protected String guestUid;
	protected String handle;
	protected String sessionId;

	public ChatterboxParty() {
		
	}
	
	/**
	 * @return the expertUid
	 */
	public String getExpertUid() {
		return expertUid;
	}
	
	/**
	 * @return the guestName
	 */
	public String getGuestName() {
		return guestName;
	}
	
	/**
	 * @return the guestUid
	 */
	public String getGuestUid() {
		return guestUid;
	}
	
	/**
	 * @return the handle
	 */
	public String getHandle() {
		return handle;
	}
	
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param expertUid the expertUid to set
	 */
	public void setExpertUid(String expertUid) {
		this.expertUid = expertUid;
	}

	/**
	 * @param guestName the guestName to set
	 */
	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	/**
	 * @param guestUid the guestUid to set
	 */
	public void setGuestUid(String guestUid) {
		this.guestUid = guestUid;
	}

	/**
	 * @param handle the handle to set
	 */
	public void setHandle(String handle) {
		this.handle = handle;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
}
