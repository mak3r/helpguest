/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 8, 2007
 */
package com.helpguest.gwt.protocol;

import java.io.Serializable;

/**
 * @author mabrams
 *
 */
public class VerifyEmailOutcome implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String outcome;
	
	public VerifyEmailOutcome() {
		
	}
	
	public VerifyEmailOutcome(final String outcome) {
		this.outcome = outcome;
	}
	
	public static class Success extends VerifyEmailOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public Success() {
			super("Your email has been verified. Please sign in.");
		}
	}
	
	public static class FailEmailUnknown extends VerifyEmailOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailEmailUnknown() {
			super("The email address provided is not known to HelpGuest.");
		}
	}
	
	public static class FailKeyDoesNotMatch extends VerifyEmailOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailKeyDoesNotMatch() {
			super("Key does not match email provided.");
		}
	}
	
	public static class FailUnverifiableData extends VerifyEmailOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailUnverifiableData() {
			super("The information provided could not be verified.");
		}
	}
	
	public static class FailInternalError extends VerifyEmailOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailInternalError() {
			super("Internal Error.  Failed trying to use result set.");
		}
	}
	
    

	public String getMessage() {
		return outcome;
	}

	/**
	 * @param prefix the prefix to set
	 */
	public String toString() {
		return outcome;
	}
	
	/**
	 * @param authOutcome SignUpOutcome
	 * @return true if they are equal.
	 */
	public boolean equals(final VerifyEmailOutcome authOutcome) {
		if (this.toString().equals(authOutcome.toString())){
			return true;
		}
		return false;
	}

	
}
