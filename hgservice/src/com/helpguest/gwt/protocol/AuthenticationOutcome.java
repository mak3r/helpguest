/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 8, 2007
 */
package com.helpguest.gwt.protocol;

import java.io.Serializable;

/**
 * @author mabrams
 *
 */
public class AuthenticationOutcome implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String outcome;
	
	public AuthenticationOutcome() {
		
	}
	
	public AuthenticationOutcome(final String outcome) {
		this.outcome = outcome;
	}
	
	public static class Success extends AuthenticationOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public Success() {
			super("success");
		}
	}
	
	public static class FailUserPass extends AuthenticationOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailUserPass() {
			super("Authentication failed.  Unknown email address or password.");
		}
	}
	
	public static class FailCurrentlyConnected extends AuthenticationOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailCurrentlyConnected() {
			super("You or another party are already logged into this account.  \nOnly one login at a time is allowed.");
		}
	}
	
	public static class FailDuplicate extends AuthenticationOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public FailDuplicate() {
			super("Authentication failed.  Duplicate identification.");
		}
	}
	
	public static class InternalError extends AuthenticationOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public InternalError() {
			super("Internal Error.  Failed trying to use result set.");
		}
	}
	
	public static class UnknownError extends AuthenticationOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public UnknownError() {
			super("Unknown error performing authentication. \nPlease contact your HelpGuest representative.");
		}
	}
	
	public static class UnknownLoginType extends AuthenticationOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public UnknownLoginType() {
			super("The login type was unknown.  Please contact your helpguest representative.");
		}
	}
	
	public static class UserNotVerified extends AuthenticationOutcome {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public UserNotVerified() {
			super("This email address is not verified. Please check your email for the HelpGuest verification link.");
		}		
	}
	
    

	public String getMessage() {
		return outcome;
	}

	/**
	 * @param prefix the prefix to set
	 */
	public String toString() {
		return outcome;
	}
	
	/**
	 * @param cmt ChatterboxMessageType
	 * @return true if they are equal.
	 */
	public boolean equals(final AuthenticationOutcome authOutcome) {
		if (this.toString().equals(authOutcome.toString())){
			return true;
		}
		return false;
	}

	
}
