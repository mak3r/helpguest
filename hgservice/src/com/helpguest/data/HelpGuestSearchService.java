/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 25, 2007
 */
package com.helpguest.data;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.helpguest.gwt.search.*;

import org.apache.log4j.Logger;

/**
 * @author mabrams
 *
 */
public class HelpGuestSearchService implements ResultHandler {

	private int queryId;

	private static final int PARADE = 0;

	private static final int ALL_EXPERTS = 1;
	
	private static final int ALL_EXPERT_IDS = 2;
	
	private static final int ALL_OFFERINGS = 3;
	
	private static final int EXPERT_ID = 4;

	private Map<String, Integer> keywordMap = null;

	private Collection<String> sortedKeys = null;

	private List<Expert> expertList = null;
	
	private List<String> expertIds = null;
	
	private List<String> allOfferings = null;

	private static final int PRIMARY = 1;
	
	private String expertUid = null;

	private static final Logger LOG = Logger
			.getLogger(HelpGuestSearchService.class);
	
	/**
	 * {@inheritDoc}
	 */
	public List<Expert> findExperts(final String issue) {
		if (issue != null && !issue.trim().equals("")) {
			addSearchPhrase(issue);
			String modified = removeIgnoreWords(issue);
			modified = commatize(modified);
			logKeywords(modified);
			List<Expert> result = resultTable(modified);
			if (result == null || result.size() == 0) {
				//no experts found get the expert parade          
				//FIXME special case what do we do?
				if (LOG.isDebugEnabled()) {
					LOG.debug("issue is: " + issue + ", no results found.");
				}
				return null;
			} else {
				//There is some result, lets send it back over the wire.
				//TODO turn the result into a List of ExpertDTO's
				if (LOG.isDebugEnabled()) {
					LOG.debug("returning result list.");
				}
				return result;
			}
		} else {
			//no experts found get the expert parade          
			//FIXME special case what do we do?
			if (LOG.isDebugEnabled()) {
				LOG.debug("no issue, no results found.");
			}
			//This may be the local search use dummy data
			return null;
		}
	}

	public void capture(final ResultSet rs, final int queryId) {
		switch (queryId) {
		case PARADE:
			if (rs == null) {
				LOG.error("No expertise found to parade.");
			} else {
				try {
					this.keywordMap = new HashMap<String, Integer>();
					this.sortedKeys = new ArrayList<String>();
					while (rs.next()) {
						this.sortedKeys.add(rs.getString("keyword"));
						this.keywordMap.put(rs.getString("keyword"),
								new Integer(rs.getInt("cnt")));
					}
				} catch (SQLException sqlx) {
					LOG.error("failed retrieving keywords for parade.", sqlx);
				}
			}
			break;

		case ALL_EXPERTS:
			if (rs == null) {
				LOG.error("No records found.");
			} else {
				try {
					this.expertList = new ArrayList<Expert>();
					Expert lastExpert = null;
					while (rs.next()) {
						Expert ex = new Expert(rs.getString("uid"), rs
								.getString("handle"));
						if (!ex.equals(lastExpert)) {
							lastExpert = ex;
							this.expertList.add(lastExpert);
						}

						lastExpert.addKeyword(rs.getString("keyword"));
						if (rs.getInt("is_primary") == PRIMARY) {
							ex.setPrimaryKeyword(rs.getString("keyword"));
						}
						//TODO: these values are not yet captured
						lastExpert.setRating(0);
						lastExpert.setLive(rs.getInt("is_live"));
						if (LOG.isDebugEnabled()) {
							LOG.debug("Expert: " + lastExpert);
						}
					}
				} catch (SQLException sqlx) {
					LOG.error("could not get data from result set.", sqlx);
				}
			}//end of else
			break;
		
		case ALL_EXPERT_IDS:
			if (rs == null) {
				LOG.error("No records found.");
			} else {
				try {
					
					this.expertIds = new ArrayList<String>();
					String lastExpertId = null;
					while (rs.next()) {
						String exId = new String(rs.getString("uid"));
						if (!exId.equals(lastExpertId)) {
							lastExpertId = exId;
							this.expertIds.add(lastExpertId);
							
						}

						/*lastExpert.addKeyword(rs.getString("keyword"));
						if (rs.getInt("is_primary") == PRIMARY) {
							ex.setPrimaryKeyword(rs.getString("keyword"));
						}*/
						//TODO: these values are not yet captured
						//lastExpert.setRating(0);
						//lastExpert.setLive(rs.getInt("is_live"));
						if (LOG.isDebugEnabled()) {
							LOG.debug("ExpertId: " + lastExpertId);
						}
					}
				} catch (SQLException sqlx) {
					LOG.error("could not get data from result set.", sqlx);
				}
			}//end of else
			break;
		
		case ALL_OFFERINGS:
			if (rs == null) {
				LOG.error("No records found.");
			} else {
				try {
					
					this.allOfferings = new ArrayList<String>();
					//String lastExpertId = null;
					while (rs.next()) {
						String catOffering = new String(rs.getString("offering"));
						
							this.allOfferings.add(catOffering);
						
						if (LOG.isDebugEnabled()) {
							LOG.debug("Offering: " + catOffering);
						}
					}
				} catch (SQLException sqlx) {
					LOG.error("could not get data from result set.", sqlx);
				}
			}//end of else
			break;
		case EXPERT_ID:
			if (rs == null) {
				LOG.error("No records found.");
			} else {
				try {
					
					//this.allOfferings = new ArrayList<String>();
					//String lastExpertId = null;
					while (rs.next()) {
						//String catOffering = new String(rs.getString("offering"));
						
							this.expertUid = rs.getString("uid");
						
						if (LOG.isDebugEnabled()) {
							LOG.debug("ExpertUid: " + expertUid);
						}
					}
				} catch (SQLException sqlx) {
					LOG.error("could not get data from result set.", sqlx);
				}
			}//end of else
			break;
		default:
			break;
		}//end switch
	}

	public int getQueryId() {
		return this.queryId;
	}

	public void setQueryId(final int id) {
		this.queryId = id;
	}

	/**
	 * @param issue String
	 * @return a List of ExpertDTO's
	 */
	private List<Expert> resultTable(final String issue) {
		this.queryId = ALL_EXPERTS;
		String allExpertQuery = "select so.keyword, le.is_live, g.uid, ex.handle, so.is_primary from "
				+ "live_expert le, service_offerings so, guest g, expert ex "
				+ "where le.expert_id = ex.id and so.expert_id = ex.id and g.expert_id = ex.id "
				+ "and so.keyword in (" + issue + ") order by g.uid";
		DataBase.getInstance().submit(allExpertQuery, this);

		Collections.sort(this.expertList, Expert.StandardRankingComparator);

		return expertList;
	}

	private void addSearchPhrase(final String phrase) {
		String phraseQuery = "insert into executed_searches (phrase) value (?) ";
		Object[] data = {phrase};
		DataBase.getInstance().submit(phraseQuery, data);
	}

	private String removeIgnoreWords(final String phrase) {
		//TODO: remove ignored words
		// rethink this.  Maybe it's unnecessary'
		return phrase;
	}

	private List<String> split(final String phrase) {
		//Copy out the phrase to a usable form
		StringBuffer phraseBuf = new StringBuffer(phrase);
		//pull quoted text first        
		List<String> keywords = new ArrayList<String>();
		boolean insideQuote = false;
		int start = 0, end = 0;
		char[] cPhrase = phrase.toCharArray();
		char c;
		for (int i = cPhrase.length - 1; i > -1; i--) {
			c = cPhrase[i];
			if (c == '"') {
				if (!insideQuote) {
					end = i;
					insideQuote = true;
				} else {
					start = i;
					String q = phrase.substring(start + 1, end);
					keywords.add(q);
					phraseBuf.replace(start, end + 1, "");
					insideQuote = false;
				}
			}
		}

		//Split the remaining phrase on whitespace
		String[] split = phraseBuf.toString().split("[\\s]+");

		for (int i = 0; i < split.length; i++) {
			String word = split[i];
			if (word.trim().length() > 0) {
				keywords.add(word.trim());
			}
		}

		return keywords;
	}

	
	private String commatize(final String phrase) {
		List keywords = split(phrase);
		//Now concatenate quoted and single word text
		StringBuffer withCommas = new StringBuffer(256);
		Iterator it = keywords.iterator();
		while (it.hasNext()) {
			String word = (String) it.next();
			withCommas.append("'" + word.trim() + "',");
		}

		//remove the final comma
		withCommas.deleteCharAt(withCommas.lastIndexOf(","));
		return withCommas.toString();
	}
	
	//prahalad adding 08/01/08 to use regex in queryconstr search and tagconstr search
	private String addPipe(final String phrase) {
		List keywords = split(phrase);
		//Now concatenate quoted and single word text
		StringBuffer withCommas = new StringBuffer(256);
		Iterator it = keywords.iterator();
		while (it.hasNext()) {
			String word = (String) it.next();
			withCommas.append(word.trim() + "|");
		}

		//remove the final comma
		withCommas.deleteCharAt(withCommas.lastIndexOf("|"));
		return withCommas.toString();
	}

	/**
	 * TODO this can be converted to use the prepared statement call on DataBase
	 * @param delimited
	 */
	private void logKeywords(final String delimited) {
		//We have to add parens around these words with a comma after each
		String[] split = delimited.split(",");
		StringBuffer insertReady = new StringBuffer();
		for (int i = 0; i < split.length; i++) {
			String word = split[i];
			insertReady.append("(" + word + "),");
		}
		//remove the final comma
		insertReady.deleteCharAt(insertReady.lastIndexOf(","));
		String keywordQuery = "insert into searched_words (keyword) values "
				+ insertReady;
		DataBase.getInstance().submit(keywordQuery);
	}
	
	//prahalad adding 08/05/2008 return a expertuid by passing the handle
	public String getExpertUid(String expertHandle)
	{
		this.queryId = EXPERT_ID;
		String expertUidQuery = "select distinct uid from expert " +
				"where handle = '" + expertHandle + "'";
		
		DataBase.getInstance().submit(expertUidQuery, this);
		
		return expertUid;
	}
	
	//ultimately this method should hit the DB for list of expert ids
	//prahalad 6/16/08
	public List<String> getExpertIdList(QueryConstraints qc){
		
		String searchQuery = qc.getSearchQuery();
		//addSearchPhrase(searchQuery);
		//String modified = removeIgnoreWords(searchQuery);
		searchQuery = addPipe(searchQuery);
		//logKeywords(searchQuery);
		//List<Expert> result = resultTable(searchQuery);
		this.queryId = ALL_EXPERT_IDS;
		
		String allExpertQuery = "select distinct e.uid "+
		"from category_offerings co, expert_tags et, expert e, live_expert le where " +
		"co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.id = le.expert_id and "+
		"(co.category REGEXP '" + searchQuery + "' or co.offering REGEXP '" + searchQuery + "' or " +
		"et.tag_name REGEXP '" + searchQuery + "') " +
		"order by le.is_live desc, (select avg(overall_satisfaction) from survey_1 s1 " +
		"where s1.expert_id = et.expert_id and overall_satisfaction != -1) desc, " +
		"(select min_rate from expert_rates er where er.expert_id = et.expert_id order by rate_date desc limit 1) asc," +
		"(select count(distinct item_id) from session ses where start_time is  not null and stop_time is not null " +
		"and ses.expert_id=et.expert_id) desc,(select avg(stop_time - start_time) from session sess " +
		"where start_time is not null and stop_time is not null and sess.expert_id=et.expert_id) asc";
		
		DataBase.getInstance().submit(allExpertQuery, this);
		
		return expertIds;
		
	}
	
	//ultimately this method should hit the DB for list of expert ids to drill down from
	//an existing list using a text box query search prahalad 07/09/08
	public List<String> getExpertIdList(QueryConstraints qc, List<String> parentExpertIdList){
		
		String searchQuery = qc.getSearchQuery();
		//addSearchPhrase(searchQuery);
		//String modified = removeIgnoreWords(searchQuery);
		searchQuery = addPipe(searchQuery);
		//logKeywords(searchQuery);
		//List<Expert> result = resultTable(searchQuery);
		
		int noOfIds = parentExpertIdList.size();
		String listOfIds = "";
		for (int it = 0; it < noOfIds; it ++)
		{
			String comma = "";
			if (it==0){}else {comma = "','";}
			listOfIds = listOfIds + comma + parentExpertIdList.get(it);
		}
		this.queryId = ALL_EXPERT_IDS;
		
		String allExpertQuery = "select distinct e.uid "+
		"from category_offerings co, expert_tags et, expert e, live_expert le  where " +
		"co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.id = le.expert_id and "+
		"(co.category REGEXP '" + searchQuery + "' or co.offering REGEXP '" + searchQuery + "' or " +
		"et.tag_name REGEXP '" + searchQuery + "') and e.uid in ('" + listOfIds + "') " +
		"order by le.is_live desc, (select avg(overall_satisfaction) from survey_1 s1 " +
		"where s1.expert_id = et.expert_id and overall_satisfaction != -1) desc, " +
		"(select min_rate from expert_rates er where er.expert_id = et.expert_id " +
		"order by rate_date desc limit 1) asc,(select count(distinct item_id) from session ses " +
		"where start_time is not null and stop_time is not null and ses.expert_id=et.expert_id) desc," +
		"(select avg(stop_time - start_time) from session sess " +
		"where start_time is  not null and stop_time is not null and sess.expert_id=et.expert_id) asc";
		
		DataBase.getInstance().submit(allExpertQuery, this);
		
		return expertIds;
		
	}	

	public List<String> getExpertIdList(CategoryConstraints cc){
		
		String catName = cc.getCategoryName();
		
		this.queryId = ALL_EXPERT_IDS;
		
		String allExpertQuery = "select distinct e.uid "+
		"from category_offerings co, expert_tags et, expert e, live_expert le where " +
		"co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.id = le.expert_id and " +
		"co.category = '" + catName + "' order by le.is_live desc, (select avg(overall_satisfaction) from survey_1 s1 " +
		"where s1.expert_id = et.expert_id and overall_satisfaction != -1) desc, (select min_rate " +
		"from expert_rates er where er.expert_id = et.expert_id order by rate_date desc limit 1) asc, " +
		"(select count(distinct item_id) from session ses where start_time is  not null " +
		"and stop_time is not null and ses.expert_id=et.expert_id) desc,(select avg(stop_time - start_time) " +
		"from session sess where start_time is  not null and stop_time is not null " +
		"and sess.expert_id=et.expert_id) asc";
		
		DataBase.getInstance().submit(allExpertQuery, this);
		
		return expertIds;
		
	}
//ultimately this method should hit the DB for subset of parentExperts based on the category passed
//and keeping the search query intact as well
//prahalad 10/30/08
	public List<String> getExpertIdList(CategoryConstraints cc, QueryConstraints qc, List<String> parentExpertIdList){
	
	String catName = cc.getCategoryName();
	String searchQuery = qc.getSearchQuery();
	searchQuery = addPipe(searchQuery);
	int noOfIds = parentExpertIdList.size();
	String listOfIds = "";
	for (int it = 0; it < noOfIds; it ++)
	{
		String comma = "";
		if (it==0){}else {comma = "','";}
		listOfIds = listOfIds + comma + parentExpertIdList.get(it);
	}
    
	this.queryId = ALL_EXPERT_IDS;
	
	
	
	String allExpertQuery = "select distinct e.uid "+
	"from category_offerings co, expert_tags et, expert e, live_expert le where " +
	"co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.id = le.expert_id and "+
	"(co.category REGEXP '" + searchQuery + "' or co.offering REGEXP '" + searchQuery + "' or " +
	"et.tag_name REGEXP '" + searchQuery + "') and e.uid in ('" + listOfIds + "') " +
	"and co.category = '" + catName + "' order by le.is_live desc, (select avg(overall_satisfaction) " +
	"from survey_1 s1 where s1.expert_id = et.expert_id and overall_satisfaction != -1) desc, " +
	"(select min_rate from expert_rates er where er.expert_id = et.expert_id order by rate_date desc limit 1) asc," +
	"(select count(distinct item_id) from session ses where start_time is  not null and stop_time is not null " +
	"and ses.expert_id=et.expert_id) desc,(select avg(stop_time - start_time) from session sess " +
	"where start_time is not null and stop_time is not null and sess.expert_id=et.expert_id) asc";
	
	DataBase.getInstance().submit(allExpertQuery, this);
	
	return expertIds;
	
}
	
	//ultimately this method should hit the DB for subset of parentExperts based on the category passed
	//prahalad 6/16/08
		public List<String> getExpertIdList(CategoryConstraints cc, List<String> parentExpertIdList){
		
		String catName = cc.getCategoryName();
		int noOfIds = parentExpertIdList.size();
		String listOfIds = "";
		for (int it = 0; it < noOfIds; it ++)
		{
			String comma = "";
			if (it==0){}else {comma = "','";}
			listOfIds = listOfIds + comma + parentExpertIdList.get(it);
		}
	    
		this.queryId = ALL_EXPERT_IDS;
		
		String allExpertQuery = "select distinct e.uid "+
		"from category_offerings co, expert_tags et, expert e, live_expert le " +
		"where co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.id = le.expert_id and "+
		"co.category = '" + catName + "'  and e.uid in ('" + listOfIds + "') " + 
		"order by le.is_live desc, (select avg(overall_satisfaction) from survey_1 s1 " +
		"where s1.expert_id = et.expert_id and overall_satisfaction != -1) desc, (select min_rate from expert_rates er " +
		"where er.expert_id = et.expert_id order by rate_date desc limit 1) asc,(select count(distinct item_id) " +
		"from session ses where start_time is  not null and stop_time is not null and ses.expert_id=et.expert_id) desc," +
		"(select avg(stop_time - start_time) from session sess where start_time is  not null and stop_time is not null " +
		"and sess.expert_id=et.expert_id) asc";
		
		DataBase.getInstance().submit(allExpertQuery, this);
		
		return expertIds;
		
	}	
	//ultimately this method should hit the DB for list of offerings for the category passed
	//prahalad 6/16/08
public List<String> getOfferings(CategoryConstraints cc){
		
	   String catName = cc.getCategoryName();
	   this.queryId = ALL_OFFERINGS;
	   String allOfferingsQuery = "select distinct offering from category_offerings " +
	   "where category = '" + catName + "' order by offering";
		
		DataBase.getInstance().submit(allOfferingsQuery, this);
		
		return allOfferings;
		
	}
//prahalad 6/16/08 return a list of experts for a offering 
public List<String> getExpertIdList(OfferingsConstraints oc){
	
	String offName = oc.getOfferingName();
	
	this.queryId = ALL_EXPERT_IDS;
	
	String allExpertQuery = "select distinct e.uid "+
	"from category_offerings co, expert_tags et, expert e, live_expert le  " +
	"where co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.id = le.expert_id and "+
	"co.offering = '" + offName + "' order by le.is_live desc, (select avg(overall_satisfaction) from survey_1 s1 " +
	"where s1.expert_id = et.expert_id and overall_satisfaction != -1) desc, (select min_rate " +
	"from expert_rates er where er.expert_id = et.expert_id order by rate_date desc limit 1) asc," +
	"(select count(distinct item_id) from session ses where start_time is  not null and stop_time is not null " +
	"and ses.expert_id=et.expert_id) desc,(select avg(stop_time - start_time) from session sess " +
	"where start_time is  not null and stop_time is not null and sess.expert_id=et.expert_id) asc";
	
	DataBase.getInstance().submit(allExpertQuery, this);
	
	return expertIds;
	
}
//prahalad 6/16/08 drill down into a list of experts for a offering
public List<String> getExpertIdList(OfferingsConstraints oc, List<String> parentExpertIdList){
	
	String offName = oc.getOfferingName();
	int noOfIds = parentExpertIdList.size();
	String listOfIds = "";
	for (int it = 0; it < noOfIds; it ++)
	{
		String comma = "";
		if (it==0){}else {comma = "','";}
		listOfIds = listOfIds + comma + parentExpertIdList.get(it);
	}
    
	this.queryId = ALL_EXPERT_IDS;
	
	String allExpertQuery = "select distinct e.uid "+
	"from category_offerings co, expert_tags et, expert e, live_expert le " +
	"where co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.id = le.expert_id and "+
	"co.offering = '" + offName + "'  and e.uid in ('" + listOfIds + "') " + 
	"order by order by le.is_live desc, (select avg(overall_satisfaction) from survey_1 s1 " +
	"where s1.expert_id = et.expert_id and overall_satisfaction != -1) desc, (select min_rate " +
	"from expert_rates er where er.expert_id = et.expert_id order by rate_date desc limit 1) asc," +
	"(select count(distinct item_id) from session ses where start_time is  not null and stop_time is not null " +
	"and ses.expert_id=et.expert_id) desc,(select avg(stop_time - start_time) from session sess " +
	"where start_time is  not null and stop_time is not null and sess.expert_id=et.expert_id) asc";
	
	DataBase.getInstance().submit(allExpertQuery, this);
	
	return expertIds;
	
}
//prahalad 6/16/08 return a list of experts for a tag
public List<String> getExpertIdList(TagsConstraints tc){
	
	String tagName = tc.getTagName();
	tagName = addPipe(tagName);
	this.queryId = ALL_EXPERT_IDS;
	
	String allExpertQuery = "select distinct e.uid "+
	"from category_offerings co, expert_tags et, expert e, live_expert le " +
	"where co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.id = le.expert_id and "+
	"et.tag_name REGEXP '" + tagName + "' order by le.is_live desc, (select avg(overall_satisfaction) " +
	"from survey_1 s1 where s1.expert_id = et.expert_id and overall_satisfaction != -1) desc, " +
	"(select min_rate from expert_rates er where er.expert_id = et.expert_id order by rate_date desc limit 1) asc," +
	"(select count(distinct item_id) from session ses where start_time is  not null and stop_time is not null " +
	"and ses.expert_id=et.expert_id) desc,(select avg(stop_time - start_time) from session sess " +
	"where start_time is not null and stop_time is not null and sess.expert_id=et.expert_id) asc";
	
	DataBase.getInstance().submit(allExpertQuery, this);
	
	return expertIds;
	
}
//prahalad 6/16/08 drill down into a list of experts for a tag
public List<String> getExpertIdList(TagsConstraints tc, List<String> parentExpertIdList){
	
	String tagName = tc.getTagName();
	tagName = addPipe(tagName);
	int noOfIds = parentExpertIdList.size();
	String listOfIds = "";
	for (int it = 0; it < noOfIds; it ++)
	{
		String comma = "";
		if (it==0){}else {comma = "','";}
		listOfIds = listOfIds + comma + parentExpertIdList.get(it);
	}
    
	this.queryId = ALL_EXPERT_IDS;
	
	String allExpertQuery = "select distinct e.uid "+
	"from category_offerings co, expert_tags et, expert e, live_expert le " +
	"where co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.id = le.expert_id and "+
	"et.tag_name REGEXP '" + tagName + "'  and e.uid in ('" + listOfIds + "') " + 
	"order by le.is_live desc, (select avg(overall_satisfaction) from survey_1 s1 where s1.expert_id = et.expert_id " +
	"and overall_satisfaction != -1) desc, (select min_rate from expert_rates er where er.expert_id = et.expert_id " +
	"order by rate_date desc limit 1) asc,(select count(distinct item_id) from session ses where start_time is not null " +
	"and stop_time is not null and ses.expert_id=et.expert_id) desc,(select avg(stop_time - start_time) " +
	"from session sess where start_time is  not null and stop_time is not null and sess.expert_id=et.expert_id) asc";
	
	DataBase.getInstance().submit(allExpertQuery, this);
	
	return expertIds;
	
}

}
