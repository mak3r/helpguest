/*
 * HGSession.java
 *
 * Created on November 7, 2006, 2:19 PM
 *
 * Wrapper around session related tables.
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.gwt.protocol.LoginType;

public class HGSession implements ResultHandler, Session {
	
	private static String WEB_ROOT = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_ROOT.getKey());
	private static String WEB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey());
	private static String FULLY_QUALIFIED_DOMAIN_NAME = 
	     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
	private static String JAWS_APP_DIR = 
		HGServiceProperties.getInstance().getProperty(HGServiceProperty.JAWS_APPS_DIR.getKey());

    private final Timestamp now = new Timestamp(System.currentTimeMillis());
            
    private int id;
    private String itemId;
    private String itemName;
    private String itemNumber;
    private String email;
    private int expertId;
    private Rates rates;
    private Timestamp start = now;
    private Timestamp end = now;
    private Date insertDate = null;
    private Date paymentRequested = null;
    private Date paymentSent = null;
    private long sessionGroupId;
    private boolean INVALID;
    private boolean CLOSED = false;

    private static final Logger log = Logger.getLogger("com.helpguest.data.HGSession");
    
    private HGSession() {
        
    }

    private void createRecord() {
        String createQuery = "insert into session " +
                "(expert_id, item_id, item_name, item_number, expert_rates_id, " +
                "start_time, stop_time) values " +
                "('" + expertId + "','" + itemId + "','" + itemName + "','" + 
                itemNumber + "','" + rates.getId() + "', now(), now() )";
        DataBase.getInstance().submit(createQuery);
    }
    
    private void createDemoRecord() {
    	//Expire any old demo sessions
    	String expireQuery = "update session set is_demo = 0 " +
    			"where expert_id = ? and is_demo = 1";
    	Object[] data = {expertId};
    	DataBase.getInstance().submit(expireQuery, data);
    	
    	//Now create a new demo session
        String createQuery = "insert into session " +
        "(expert_id, item_id, item_name, item_number, expert_rates_id, " +
        "start_time, stop_time, is_demo) values " +
        "('" + expertId + "','" + itemId + "','" + itemName + "','" + 
        itemNumber + "','" + rates.getId() + "', now(), now(), 1 )";
        DataBase.getInstance().submit(createQuery);
    }
    

    /** Creates a new instance of HGSession */
    public static HGSession newInstance(ExpertAccount ea, Rates rates) {
        HGSession session = new HGSession();
        session.itemId = ea.getHandle().toUpperCase() + "Q" + Long.toHexString(System.currentTimeMillis()).toUpperCase();
        session.itemName = ea.getHandle();
        session.itemNumber = String.valueOf(rates.getMinTime()) + "-" + 
                             rates.getFormattedMinRate() + "-" +
                             String.valueOf(rates.getPreferredTime()) + "-" +
                             rates.getFormattedDepositAmount();
        session.expertId = ea.getId();
        session.rates = rates;
        session.createRecord();
        return session;
    }
    
    /** 
     * Creates a new instance of HGSession. 
     * 
     * This method is intended for use with the DemoSession.
     * 
     */
    public static HGSession newInstance(
    		final ExpertAccount ea, 
    		final Rates rates, 
    		final boolean isDemo) {
        HGSession session = new HGSession();
        session.itemId = ea.getHandle().substring(0,1).toUpperCase() + 
        		"X" + Long.toHexString(System.currentTimeMillis()).toUpperCase();
        session.itemName = ea.getHandle();
        session.itemNumber = String.valueOf(rates.getMinTime()) + "-" + 
                             rates.getFormattedMinRate() + "-" +
                             String.valueOf(rates.getPreferredTime()) + "-" +
                             rates.getFormattedDepositAmount();
        session.expertId = ea.getId();
        session.rates = rates;
        session.createDemoRecord();
        return session;
    }
    

    
    
    /**
     * @param itemId is the unique id for the session
     * @return the session associated with this id
     * or null if no session matches this id
     */
    public static HGSession getInstance(String itemId) {
        HGSession session = new HGSession();
        String createQuery = "select session.*, email from session, expert " +
                "where session.item_id = '" + itemId + "' and " +
                "expert.id = session.expert_id";
        DataBase.getInstance().submit(createQuery,session);
        return session;
    }

    /**
     * @param itemId is the unique id for the session
     * @return the session associated with this id
     * or null if no session matches this id
     */
    public static HGSession getDemoInstance(ExpertAccount ea) {
        HGSession session = new HGSession();
        String createQuery = "select session.*, expert.email from session, expert " +
                "where session.is_demo = 1 and " +
                "expert.email = ? and expert.id = session.expert_id";
        Object[] data = {ea.getEmail()};
        DataBase.getInstance().submit(createQuery, session, data);
        return session;
    }

    /** 
     * start the session
     * by creating a record of the session in the database
     * @see com.helpguest.protocol.Lexicon.Client.START_SESSION
     * @see com.helpguest.protocol.Lexicon.Client.STOP_SESSION
     * @see com.helpguest.protocol.Lexicon.Technician.START_SESSION
     */
    public static void start(String itemId) {
        //Only start a session if it's not started and also not already stopped'
        String startQuery = "update session set start_time = now(), started = 1 " +
                "where item_id = ? and guest_start = 1 and started != 1 ";
        Object[] data = {itemId};
        DataBase.getInstance().submit(startQuery,data);
    }
    
    /**
     * guest indication to start the session
     */
    public static void guestStart(String itemId) {
        String startQuery = "update session set guest_start = 1 where item_id = ?";
        Object[] data = {itemId};
        DataBase.getInstance().submit(startQuery,data);        
    }
    
    /**
     * stop the session
     * by stamping the stop_time on the db record
     * if the session has already been stopped, it will not 
     * set a new timestamp.  If it has not been started,
     * it will not set a timestamp or the stopped flag to true.
     */
    public static void stop(String itemId) {
        String stopQuery = "update session set stop_time = now(), stopped = 1 " +
                    "where item_id = ? and stopped != 1 and started = 1" ;
        Object[] data = {itemId};
        DataBase.getInstance().submit(stopQuery,data);
    }

    /**
     * This item_id/session id/ticket# has expired
     * mark it so and remove the related files
     */
    public static void expire(String itemId) {
        File jnlpFile = 
        	new File(WEB_ROOT + JAWS_APP_DIR + "/" + 
        			new LoginType.Client().toString() + "/"+itemId+".jnlp");
        jnlpFile.delete();
        File htmlFile = 
        	new File(WEB_ROOT + JAWS_APP_DIR + "/" + 
        			new LoginType.Client().toString() + "/"+itemId+".html");
        htmlFile.delete();
        File cwFile = 
        	new File(WEB_ROOT + JAWS_APP_DIR + "/" + 
        			new LoginType.Client().toString() + "/"+itemId+"click_wrap.jnlp");
        cwFile.delete();
        
        String expireQuery = "update session set expired = 1 where item_id = ? ";
        Object[] data = {itemId};
        DataBase.getInstance().submit(expireQuery,data);
    }
    
    public Timestamp getStart() {
        return start;
    }
    
    public Timestamp getEnd() {
        return end;
    }
    
    /**
     * @return true if the session is closed
     */
    public boolean isClosed() {
        if (end.after(start) || CLOSED) {
            return true;
        }
        return false;
    }
    
    public int getId () {
        return id;
    }
    
    /**
     * A synonym for getSessionId()
     */
    public String getItemId() {
        return getSessionId();
    }
    
    public String getSessionId() {
        return itemId;
    }
    
    public String getItemName() {
        return itemName;
    }
    
    public String getItemNumber() {
        return itemNumber;
    }

    public int getExpertId() {
        return expertId;
    }
    
    public Rates getRates() {
        return rates;
    }
    
    public Date getPaymentSentDate() {
        return paymentSent;
    }

    public Date getPaymentRequestDate() {
        return paymentRequested;
    }

    public Date getInsertDate() {
        return insertDate;
    }
    
    public long getSessionGroupId() {
        return sessionGroupId;
    }

    private void createLinkPage() {
        
    }
    
    public String getLinkPage() {
        String url = null;
        try {
            url = URLEncoder.encode(WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/about.html", "UTF-8");
        } catch (UnsupportedEncodingException uex) {
            log.error("could not encode url.", uex);
        }
        return url;
    }
    
    public void capture(ResultSet rs, int queryId) throws SQLException {
         try {
            if (rs.next()) {
		id = rs.getInt("id");
                itemId = rs.getString("item_id");
                itemName = rs.getString("item_name");
                itemNumber = rs.getString("item_number");
                expertId = rs.getInt("expert_id");
                rates = HGRates.getCurrentInstance(ExpertAccount.getExpertAccount(rs.getString("email")));
                start = rs.getTimestamp("start_time");
                end = rs.getTimestamp("stop_time");
                insertDate = rs.getDate("insert_date");
                paymentRequested = rs.getDate("payment_requested");
                paymentSent = rs.getDate("payment_sent");
                sessionGroupId = rs.getLong("session_group_id");
                email = rs.getString("email");
            } else {
                INVALID = true;
            }
        } catch (SQLException sqlx) {
            //This error is thrown if there is a problem accessing the database
            log.error("Failed to capture data set.", sqlx);
            INVALID = true;
        }
    }

    public void setQueryId(int id) {
    }

    public int getQueryId() {
        return 0;
    }
    
}
