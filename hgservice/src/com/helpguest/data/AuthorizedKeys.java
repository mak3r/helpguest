package com.helpguest.data;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;



public class AuthorizedKeys implements ResultHandler {

	private List<String> activeKeys = new ArrayList<String>();
	private static final Logger LOG = Logger.getLogger(AuthorizedKeys.class);

	public AuthorizedKeys() {
		
	}

	public static void insert(final String key) {
		String insertQuery = "insert into authorized_keys (ssh_key, expired) values (?, ?)";
		Object[] data = {key, "0"};
		DataBase.getInstance().submit(insertQuery, data);
	}

	public static void deleteExpiredKeys() {
		String deleteQuery = "delete from authorized_keys where expired > 0";
		DataBase.getInstance().submit(deleteQuery);
	}
	
	public static void expireKey(final String key) {
		String expireQuery = "update authorized_keys set expired = ? where ssh_key = ?";
		Object[] data = {"1", key};
		DataBase.getInstance().submit(expireQuery, data);
	}
	
	public List<String> getCurrentKeys() {
		String selectQuery = "select ssh_key from authorized_keys where expired = 0";
		DataBase.getInstance().submit(selectQuery, this);
		
		return activeKeys;
	}

	public void capture(ResultSet rs, int queryId) throws SQLException {
		try {
			String key;
			long length;
			activeKeys.clear();
			while (rs.next()) {
				Clob keyClob = rs.getClob("ssh_key");
				length = keyClob.length();
			    key = keyClob.getSubString(1, (int) length);
				activeKeys.add(key);
			}
		} catch(Exception ex) {
			LOG.error("failed to get keys list", ex);
		}
	}

	public int getQueryId() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setQueryId(int id) {
		// TODO Auto-generated method stub
		
	}

}
