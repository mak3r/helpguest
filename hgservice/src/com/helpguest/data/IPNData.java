/*
 * IPNData.java
 *
 * Created on January 9, 2007, 10:38 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import org.apache.log4j.Logger;

public class IPNData implements ResultHandler {
    
    public static final String VERIFY_SIGN = "verify_sign";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PAYER_BUSINESS_NAME = "payer_business_name";
    public static final String PAYER_EMAIL = "payer_email";
    public static final String PAYER_ID = "payer_id"; 
    public static final String ITEM_NAME = "item_name"; 
    public static final String ITEM_NUMBER = "item_number"; 
    public static final String PAYMENT_STATUS = "payment_status"; 
    public static final String PAYMENT_DATE = "payment_date";
    public static final String CUSTOM = "custom";
    public static final String TAX = "tax"; 
    public static final String TEST_IPN = "test_ipn"; 
    public static final String EXPERT_ID = "expert_id"; 
    public static final String MC_GROSS = "mc_gross"; 
    public static final String MC_CURRENCY = "mc_currency";                
    public static final String RECEIVER_EMAIL = "receiver_email"; 
    public static final String TXN_ID = "txn_id"; 
    public static final String INVOICE = "invoice";

    public static final String COMPLETED = "Completed";
    public static final String PENDING = "Pending";

    private int queryId;
    private static final int DUP_TXN = 0;
    private static final int VERIFY_SIGN_INSTANCE = 1;
    private static final int SESSION_ID_INSTANCE = 2;
    private boolean dupTxn = false;
    //verifySign is the unique key
    private String verifySign;
    private String paymentStatus;
    private String invoice;
    private String itemName;
    private String itemNumber;
    private String payerEmail;
    
    private static final Logger log = Logger.getLogger("com.helpguest.data.IPN");
    
    /** Creates a new instance of IPN */
    private IPNData(String verifySign) {
        this.verifySign = verifySign;
    }
    
    private IPNData() {        
    }
    
    public static IPNData getInstance(String verifySign) {
        IPNData ipn = new IPNData(verifySign);
        ipn.queryId = VERIFY_SIGN_INSTANCE;
        String query = "select * from ipn where " + VERIFY_SIGN + " = ?";
        Object[] data = {verifySign};
        DataBase.getInstance().submit(query,ipn,data);

        return ipn;
}
    
    public static IPNData getInstance(Session session, String paymentStatus) {
        IPNData ipn = new IPNData();
        ipn.queryId = SESSION_ID_INSTANCE;
        String query = "select * from ipn where " + INVOICE + " = ? " +
                "and " + PAYMENT_STATUS + " = ? ";
        Object[] data = {session.getSessionId(), paymentStatus};
        DataBase.getInstance().submit(query,ipn,data);
        if (ipn.verifySign != null) {
            return ipn;
        }
        return null;
    }
    
    public static IPNData createInstance(Map<String,String> ipnData) {
        if (log.isDebugEnabled()) {log.debug("preparing to create ipn instance");}
        if (ipnData.get(VERIFY_SIGN) == null || (ipnData.get(VERIFY_SIGN)).length() <= 0) {
            return null;
        }
        if (log.isDebugEnabled()) {log.debug("about to get call ipn ctor with txnid");}
        IPNData ipn = IPNData.getInstance(ipnData.get(TXN_ID));
        if (!ipn.isDupTxn()) {    
            //Finally, record this transaction
            String insertIpnQuery = "insert into ipn (" +
                VERIFY_SIGN + ", " +
                FIRST_NAME + ", " +
                LAST_NAME + ", " +
                PAYER_BUSINESS_NAME + ", " +
                PAYER_EMAIL + ", " +
                PAYER_ID + ", " + 
                ITEM_NAME + ", " + 
                ITEM_NUMBER + ", " + 
                PAYMENT_STATUS + ", " + 
                PAYMENT_DATE + ", " +
                CUSTOM + ", " +
                TAX + ", " + 
                TEST_IPN + ", " + 
                EXPERT_ID + ", " + 
                MC_GROSS + ", " + 
                MC_CURRENCY + ", " +                
                RECEIVER_EMAIL + ", " + 
                TXN_ID + ", " + 
                INVOICE + ") " +
                //"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            //Object[] data = {
                " values ('" + 
                ipnData.get(VERIFY_SIGN) + "', '" +
                ipnData.get(FIRST_NAME) + "', '" +
                ipnData.get(LAST_NAME) + "', '" +
                ipnData.get(PAYER_BUSINESS_NAME) + "', '" +
                ipnData.get(PAYER_EMAIL) + "', '" +
                ipnData.get(PAYER_ID) + "', '" + 
                ipnData.get(ITEM_NAME) + "', '" + 
                ipnData.get(ITEM_NUMBER) + "', '" + 
                ipnData.get(PAYMENT_STATUS) + "', '" + 
                ipnData.get(PAYMENT_DATE) + "', '" +
                ipnData.get(CUSTOM) + "', '" +
                ipnData.get(TAX) + "', '" + 
                ipnData.get(TEST_IPN) + "', '" + 
                ipnData.get(EXPERT_ID) + "', '" + 
                ipnData.get(MC_GROSS) + "', '" + 
                ipnData.get(MC_CURRENCY) + "', '" +                
                ipnData.get(RECEIVER_EMAIL) + "', '" + 
                ipnData.get(TXN_ID) + "', '" + 
                ipnData.get(INVOICE) + "')";
            //};
            DataBase.getInstance().submit(insertIpnQuery);//,data);
            
            //fill in the data pieces
            ipn.paymentStatus = ipnData.get(PAYMENT_STATUS);
            ipn.invoice = ipnData.get(INVOICE);
            ipn.itemName = ipnData.get(ITEM_NAME);
            ipn.itemNumber = ipnData.get(ITEM_NUMBER);
            ipn.payerEmail = ipnData.get(PAYER_EMAIL);
        }
        return ipn;
    }
    
    protected boolean isDupTxn() {
        return dupTxn;
    }
    
    /**
     * @return the unique key for this ipn
     */
    public String getVerifySign() {
        return verifySign;
    }
    
    public String getPaymentStatus() {
        return paymentStatus;
    }
    
    public String getInvoice() {
        return invoice;
    }
    
    public String getItemName() {
        return itemName;
    }
    
    public String getItemNumber() {
        return itemNumber;
    }
    
    public String getPayerEmail() {
        return payerEmail;
    }
    
    public boolean isCompleted() {
        if(COMPLETED.equals(paymentStatus)) {
            return true;
        }
        return false;
    }
    
    public void capture(ResultSet rs, int queryId) {
        if (rs == null) {
            log.debug("result set is null");
        } else {
            switch (queryId) {
                case SESSION_ID_INSTANCE:
                    try {
                        //It might be possible for there to be multiple
                        // records with the same session id
                        // the method checks status but we can't even be sure
                        //there aren't two of the same status.
                        if (rs.next()) {
                            this.verifySign = rs.getString(VERIFY_SIGN);
                            this.paymentStatus = rs.getString(PAYMENT_STATUS);
                            this.invoice = rs.getString(INVOICE);
                            this.itemName = rs.getString(ITEM_NAME);
                            this.itemNumber = rs.getString(ITEM_NUMBER);
                            this.payerEmail = rs.getString(PAYER_EMAIL);
                        }
                    } catch (SQLException sqlx) {
                        log.error("could not get instance data.", sqlx);
                    }
                    break;
                case VERIFY_SIGN_INSTANCE:
                    try {
                        if (rs.next()) {
                            this.dupTxn = true;
                            this.paymentStatus = rs.getString(PAYMENT_STATUS);
                            this.invoice = rs.getString(INVOICE);
                            this.itemName = rs.getString(ITEM_NAME);
                            this.itemNumber = rs.getString(ITEM_NUMBER);
                            this.payerEmail = rs.getString(PAYER_EMAIL);
                        } else {
                            dupTxn = false;
                        }
                    } catch (SQLException sqlx) {
                        log.error("could not get instance data.", sqlx);
                    }
                    break;

                default:
                    break;
            }
        }//end of else
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }
    
}
