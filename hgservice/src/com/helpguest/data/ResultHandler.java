/*
 * ResultHandler.java
 * This class is designed to be used with 
 * com.helpguest.service.DataBase.
 * It allows the implementing class to avoid
 * mucking up the class with the code common
 * to all database queries.
 *
 * Created on January 3, 2005, 3:39 PM
 * Copyright 2005 HelpGuest Technologies, Inc.
 */

package com.helpguest.data;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author  mabrams
 */
public interface ResultHandler {
    
    /** 
     * The implementing class should use this
     * call to extract the results from the queries
     * that are passed to com.helpguest.service.DataBase
     */
    public void capture(ResultSet rs, int queryId) throws SQLException;

    /**
     * Concrete implementations should return the id
     * provided using the setQueryId(int) method.
     * This allows an individual class to implement
     * this interface and uniquely identify multiple
     * queries from that class.
     */
    public int getQueryId();
    
    /**
     * Concrete implementations should use this
     * to set a unique id when a single class the implements
     * this interfaces uses more than one query.
     */
    public void setQueryId(int id);
    
    public String toString();
}
