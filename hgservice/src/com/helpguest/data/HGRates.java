/*
 * HGRates.java
 *
 * Created on November 8, 2006, 6:26 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import org.apache.log4j.Logger;

public class HGRates implements ResultHandler, Rates {
    
    public static final int BASE_TIME = 10;
    public static final float MIN_RATE = .99f;
    public static final float BASE_RATE = BASE_TIME * MIN_RATE;
    
    private int id;
    private int expertId;
    private float minRate = MIN_RATE;
    private int minTime = BASE_TIME;
    private int prefTime = 2*BASE_TIME;
    private boolean INVALID = false;
    
    private DecimalFormat df = new DecimalFormat("#,###,###.00");
    private static final Logger log = Logger.getLogger("com.helpguest.data.HGRates");
    
    /**
     * Creates a new instance of HGRates
     */
    private HGRates() {
    }

    /**
     * @param ea is the Expert account that this rates record is associated with
     * @param minRate is the minimum rate the expert wants to charge
     * @param prefTime is the prefered amount of time the expert will suggest for it's guests
     * @return a HGRates object
     */
    public static HGRates newInstance(ExpertAccount ea, float minRate, int prefTime) {
        HGRates rates = new HGRates();
        rates.expertId = ea.getId();
        //The goal is to have the minimum time and rate create a fee of at least $10 ($9.90 by default)
        rates.minRate = Math.max(MIN_RATE, minRate);
        rates.minTime = Math.min(BASE_TIME, (int)Math.ceil(BASE_TIME/rates.minRate));
        //The preferred time cannot be less than the minTime
        rates.prefTime = Math.max(prefTime, rates.minTime);
        
        rates.createRecord();
        return rates;
    }
    
    /**
     * @param ea is the Expert account that this rates record is associated with
     * @param minRate is the minimum rate the expert wants to charge
     * @param prefTime is the prefered amount of time the expert will suggest for it's guests
     * @param force if true force a new record even if the current values are 
     * equivalent to the passed in parameters minRate and prefTime.  True results
     * are identical to calling the same method without the force parameter. If false, 
     * only store a new record if the parameters minRate and minTime are different than
     * the current record.
     * @return a HGRates object
     */
    public static HGRates newInstance(ExpertAccount ea, float minRate, int prefTime, boolean force) {
        HGRates rates = null;
        
        if (force) {
            rates = newInstance(ea,minRate,prefTime);
        } else {
            rates = getCurrentInstance(ea);
            if (rates == null) {
                rates = newInstance(ea,minRate,prefTime);
            } else if (rates.getMinRate() != minRate || rates.getPreferredTime() != prefTime) {
                if (log.isDebugEnabled()) {
                    log.debug("minRate: " + minRate + ", rates.minRate: " + rates.getMinRate());
                    log.debug("time: " + prefTime + ", rates.minTime: " + rates.getPreferredTime());
                }
                rates = newInstance(ea,minRate,prefTime);
            }
        }
        
        return rates;
    }
    
    /**
     * Create a new instance of HGRates with 0 values for 
     * minRate, minTime, and prefTime.  This is intended
     * for use with the DemoSession that uses a non-valued
     * session.
     * 
     * @param ea ExpertAccount
     * @return HGRates
     */
    protected static HGRates getDemoInstance(ExpertAccount ea) {
    	
    	HGRates rates = getZeroInstance(ea);
    	
    	if (rates == null) {
    		rates = new HGRates();   	
	    	rates.expertId = ea.getId();
	    	
	    	rates.minRate = 0.0f;
	    	rates.minTime = 0;
	    	rates.prefTime = 0;
	    	rates.createDemoRecord();
    	}
    	
    	return rates;
    }
    
    /**
     * Assistant to getDemoInstance.
     * 
     * @param ea ExpertAccount
     * @return HGRates
     */
    private static HGRates getZeroInstance(ExpertAccount ea) {
        HGRates rates = new HGRates();
        rates.expertId = ea.getId();
    	String zeroQuery = "select *, rate_date as rd from expert_rates " +
					"where expert_id = ? and minRate = 0";
    	Object[] data = {rates.expertId};
    	DataBase.getInstance().submit(zeroQuery, rates, data);

    	if (rates.INVALID) {
            rates = null;
        }
        return rates;
    } 


    
    public static HGRates getCurrentInstance(ExpertAccount ea) {
        HGRates rates = new HGRates();
        rates.expertId = ea.getId();
        String currentQuery = "select *, rate_date as rd from expert_rates " +
                "where expert_id ='" + rates.expertId + "' " +
                "order by rd desc " +
                "limit 1";
        DataBase.getInstance().submit(currentQuery,rates);
        if (log.isDebugEnabled()) {
            log.debug("rates object: " + rates);
            log.debug("INVALID: " + (rates.INVALID ? "true" : "false"));
        }
        if (rates.INVALID) {
            rates = null;
        }
        return rates;
    } 

    private final void createRecord() {
        String ratesQuery = "insert into expert_rates " +
                "(expert_id, min_rate, min_time, preferred_time, rate_date) " +
                "value ('" + expertId + "','" + minRate + 
                "','" + minTime + "','" + prefTime + "', now())";
        DataBase.getInstance().submit(ratesQuery);    
    }
    
    private final void createDemoRecord() {
        String ratesQuery = "insert into expert_rates " +
			        "(expert_id, min_rate, min_time, preferred_time, rate_date) " +
			        "value ('" + expertId + "', " +
			        "0.00, 0, 0 , '0000-00-00 00:00:00')";
        DataBase.getInstance().submit(ratesQuery);    	
    }

    public int getId() {
        return id;
    }
    
    public float getMinRate() {
        return minRate;
    }
    
    public String getFormattedMinRate() {
        return df.format(getMinRate());
    }
    
    public int getMinTime() {
        return minTime;
    }
    
    public int getPreferredTime() {
        return prefTime;
    }
    
    public float getDepositAmount() {
        return minRate*prefTime;
    }
    
    public String getFormattedDepositAmount() {
        return df.format(getDepositAmount());
    }
    
    public float getMinFee() {
        return minRate*minTime;
    }
    
    public String getFormattedMinFee() {
        return df.format(getMinFee());
    }
    
    public int getExpertId() {
        return expertId;
    }
    
    public void capture(ResultSet rs, int queryId) throws SQLException {
        try {
            if (rs.next()) {
                id = rs.getInt("id");
                minRate = rs.getFloat("min_rate");
                minTime = rs.getInt("min_time");
                prefTime = rs.getInt("preferred_time");
            } else {
                INVALID = true;
            }
        } catch (SQLException sqlx) {
            //This error is thrown if there is a problem accessing the database
            log.error("Failed to capture data set.", sqlx);
            INVALID = true;
        }
    }

    public void setQueryId(int id) {
    }

    public int getQueryId() {
        return 0;
    }
    
    @Override
	public String toString() {
        StringBuffer buf = new StringBuffer("");
        buf.append("[expertId: " + expertId);
        buf.append(", minRate: " + minRate);
        buf.append(", minTime: " + minTime);
        buf.append(", prefTime: " + prefTime);
        buf.append("]");
        
        return buf.toString();
    }
    
    
}
