/*
 * ExpertAccount.java
 *
 * This is the join object for the tables expert and expert_account.
 *
 * Created on October 22, 2005, 6:23 AM
 */

package com.helpguest.data;

import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;


import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.util.UIDGenerator;



/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class ExpertAccount implements ResultHandler {

    public final static String NONE = "NONE";
    public final static String EXPIRED = "EXPIRED";
 
    private final static int EAS = 0;
    private final static int GUESTS_WAITING = 1;
    private final static int IDENTITY = 2;
    private final static int VERIFIED = 3;
    private final static int GUEST_UID = 4;
    private final static int SECRET = 5;
    private final static int SURVEY = 6;
    private int queryId;
 
    private boolean VALID_USER = false;
    private boolean VALID_PASS = false;
    
    private int HOURS_PER_DAY = 24;
    private int MINUTES_PER_HOUR = 60;
    private int SECONDS_PER_MINUTE = 60;
    private int MILLISECONDS_PER_SECOND = 1000;
    
    private String identifier;
    private String uid;
    private int id;
    private int easId;
    private String email = "";
    private String handle = "";
    private String password = "";
    private String paypalEmail = "";
    private String webVersion = "1";
    private int canDemo = 0;
    private int guestsWaiting;
    private ArrayList<String> expertiseList;
    private int expertiseLimit = 10;
    private String guestUid;
    private long timeRemaining;
    private Timestamp endDate;
    private GregorianCalendar terminationDateTime = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    private GregorianCalendar startDateTime = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    private boolean verified = false;
    private ExpertDetails expertDetails = null;
    private ServiceOffering serviceOffering = null;
    
    private String overallSatisfaction;
    private String problemSolved;
    private String recommend;
    private String repeatCustomer;
    private String professionalService;
    //private int insert_date;
    private String sessionId;
        
    private Logger log = Logger.getLogger("com.helpguest.data.ExpertAccount");
    
    private static String WEB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey());
    private static String FULLY_QUALIFIED_DOMAIN_NAME = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    
    private static String WEB_ROOT = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_ROOT.getKey());
    
    private static String JAWS_APPS_DIR = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.JAWS_APPS_DIR.getKey());
    
    public static ExpertAccount getExpertAccount(String identifier) {
        ExpertAccount ea = new ExpertAccount(identifier);
        if(ea.VALID_USER) {
            return ea;
        } else {
            return null;
        }
    }
    
    public static ExpertAccount getExpertAccount(String identifier, String secret) {
        ExpertAccount ea = new ExpertAccount(identifier, secret);
        if (ea.VALID_PASS) {
            return ea;
        } else {
            return null;
        }
    }
    
    public static ExpertAccount getExpertAccount(Session session) {
        ExpertAccount ea = new ExpertAccount(session.getExpertId());
        if (ea.VALID_USER) {
            return ea;
        } else {
            return null;
        }
    }
    
    public static ExpertAccount newInstance(String email, String pass, String handle, String offering, String zip, String userIP) {
        String insertQuery = "insert into expert (uid, email, pass, handle, max_guests) " +
                                 "values (?, ?, PASSWORD(?), ?, 5)";
        Object[] data = {UIDGenerator.generate(email), email, pass, handle};
        DataBase.getInstance().submit(insertQuery,data);
        ExpertAccount ea = new ExpertAccount(email,pass);
        ea.serviceOffering = ServiceOffering.newInstance(ea,offering);
        return ea;
    }
    
   //prahalad adding 7/23/08 alternate createAccount method to handle ArrayList of catOffgTags
    public static ExpertAccount newInstance(String email, String pass, String handle, List catOffgTagList, String zip, String userIP) {
        String insertQuery = "insert into expert (uid, email, pass, handle, max_guests) " +
                                 "values (?, ?, PASSWORD(?), ?, 5)";
        Object[] data = {UIDGenerator.generate(email), email, pass, handle};
        DataBase.getInstance().submit(insertQuery,data);
        ExpertAccount ea = new ExpertAccount(email,pass);
        ea.serviceOffering = ServiceOffering.newInstance(ea,catOffgTagList);
        return ea;
    }

    private ExpertAccount(int id) {
        setQueryId(IDENTITY);
        identifier = "PK_" + id;
        String query = "select id, uid, handle, email, paypal_email, " +
                    "web_version, can_demo from expert where id = ?";
        Object[] data = {new Integer(id)};
        DataBase.getInstance().submit(query,this,data);
        validateUser();
    }
    
    private ExpertAccount(String identifier, String secret) {
        this(identifier);
        if (this.VALID_USER) {
            //username exists verify the secret password is correct
            setQueryId(SECRET);
            String secretQuery = "select 1 from expert where email = '" + email + "' " +
                    "and pass = PASSWORD('" + secret + "')"; 
            DataBase.getInstance().submit(secretQuery,this);
        } else {
            this.VALID_PASS = false;
        }    
    }
    
    /** Creates a new instance of ExpertAccount */
    private ExpertAccount(String identifier) {       
        this.identifier = identifier;
        //if the identifier has an @ symbol, consider this an email address.
        if (identifier != null && identifier.indexOf("@") != -1) {
            this.email = identifier;
            setQueryId(IDENTITY);
            String identifierQuery = "select id, uid, handle, email, paypal_email, " +
                    "web_version, can_demo from expert where email = '" + email + "'";
            DataBase.getInstance().submit(identifierQuery,this);
        } else {
            this.uid = identifier;
            setQueryId(IDENTITY);
            String identifierQuery = "select id, uid, handle, email, paypal_email, " +
                    "web_version, can_demo from expert where uid = '" + identifier + "'";
        	DataBase.getInstance().submit(identifierQuery,this);
        }
        
        validateUser();
    }

    private void validateUser() {
        //continue if it was a valid user otherwise we can't go on
        if (VALID_USER) {
            setQueryId(EAS);
            String easQuery = "select eas.time_remaining, eas.end_date, eas.id from expert_account_status eas, expert e " +
                    "where e.uid = '" + uid + "' and e.id = eas.expert_id";
            DataBase.getInstance().submit(easQuery, this);

            setQueryId(GUESTS_WAITING);
            String guestsQuery = "select distinct guest_uid " +
                    "from protocol_request " +
                    "where expert_uid = ? and status = ?";
            Object[] data = {uid, ClientSelectionData.WAITING};
            DataBase.getInstance().submit(guestsQuery, this, data);

            setQueryId(VERIFIED);
            String validQuery = "select is_verified from email_verification ev, expert ex " + 
                                "where ev.expert_id = ex.id and ex.uid = '" + uid + "'";
            DataBase.getInstance().submit(validQuery,this);   

            //make sure we don't use someone elses uid
            guestUid = "";
            setQueryId(GUEST_UID);
            String guestUidQuery = "Select uid from guest where expert_id in (" + 
                    "select id from expert where email = '" + email + "')";
            DataBase.getInstance().submit(guestUidQuery, this);

            this.expertDetails = new ExpertDetails(this);
    
        }        

    }
    
    public void getSurvey(String sid){
    	setQueryId(SURVEY);
    	this.sessionId = sid;
    	String getSurvey = "select overall_satisfaction, problem_solved, recommend, repeat_customer, " + 
    		"professional_service, insert_date from survey_1 where expert_id = "+ getId() + " and " +
    				"session_id = '" + sid + "'";
    	DataBase.getInstance().submit(getSurvey,this);
    }
    
    /**
     * Update the database with the time remaining.
     * 
     * To get the time remaining use tick().
     */
    public void update() {
        if (log.isDebugEnabled()) {log.debug("timeRemaining before tick: " + timeRemaining);}
        timeRemaining -= tick();
        if (log.isDebugEnabled()) {log.debug("timeRemaining after tick: " + timeRemaining);}
        String query = "update expert_account_status set " +
                "time_remaining = '" + timeRemaining + 
                "' where id = '" + easId + "'";
        DataBase.getInstance().submit(query);
    }
    
    /**
     * Calculates the time remaining.
     *
     * The formula to calculate time remaining is
     * the current time - time this object was created.
     *
     * @return the time remaining in milliseconds
     */
    public long tick() {
        if (!isExpired()) {
            Date sdt = startDateTime.getTime();
            Date cdt = curDateTime().getTime();
            long smillis = sdt.getTime();
            long cmillis = cdt.getTime();
            long elapsedTime = cmillis - smillis;
            return elapsedTime;
        } else {
            return 0;
        }
    }
    
    private boolean isExpired() {
        //return true if there is time remaining on this account
        return millisToExpire() <= 0;
    }
    
    private long millisToExpire() {
        long tmillis = terminationDateTime.getTimeInMillis();
        long cmillis = curDateTime().getTimeInMillis();
        return tmillis - cmillis;
    }
    
    private GregorianCalendar curDateTime() {
        return new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    }
    
    /**
     * Get a statement regarding how much usage is available.
     * @return a usage available string
     */
    public String getUsageAvailable() {
        int togo;
        //Seconds to go
        togo = (int)(timeRemaining / MILLISECONDS_PER_SECOND);
        //minutes to go
        togo = (int)(togo / SECONDS_PER_MINUTE);

        int minutes = (int)(togo % MINUTES_PER_HOUR);
        //hours to go
        togo = (int) (togo / MINUTES_PER_HOUR);

        int hours = (int)(togo % HOURS_PER_DAY);
        int days = (int)(togo / HOURS_PER_DAY);

        //We only want to display hours and minutes remaining.
        // so make the days back into hours
        hours += (days * HOURS_PER_DAY);
        
        String hourString = (hours == 1 ? "1 hour " : hours + " hours ");
        String minuteString = (minutes == 1 ? "1 minute " : minutes + " minutes ");
        String usageAvailable = null;
        usageAvailable = ((hours == 0 && minutes == 0) ? ExpertAccount.NONE : hourString + minuteString);
        
        return usageAvailable; 
    }
    
    public String getPercentUsageAvailable() {
        int togo;
        //Seconds to go
        togo = (int)(timeRemaining / MILLISECONDS_PER_SECOND);
        //minutes to go
        togo = (int)(togo / SECONDS_PER_MINUTE);

        int minutes = (int)(togo % MINUTES_PER_HOUR);
        //hours to go
        togo = (int) (togo / MINUTES_PER_HOUR);

        int hours = (int)(togo % HOURS_PER_DAY);
        int days = (int)(togo / HOURS_PER_DAY);

        //We only want to display hours and minutes remaining.
        // so make the days back into hours
        hours += (days * HOURS_PER_DAY);
        
        String minuteString = "";
        float minutePercent = minutes/60.0f;
        if (minutePercent != 0.0) {
            DecimalFormat df = new DecimalFormat(".00");
            minuteString = df.format(minutePercent);
        }
        String usageAvailable = null;
        usageAvailable = hours + minuteString + " hours";
        
        return usageAvailable; 
    }
    
    public String getEndDate() {
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMMMMMMMM d, yyyy");
        return sdf.format(this.endDate);
    }
    
    /**
     * Get a statement regarding how much shelf life remains.
     * @return the shelf life
     */
    public String getShelfLife() {
        if (isExpired()) {
            return ExpertAccount.EXPIRED;
        }
        int togo;
        //total Seconds to go
        togo = (int)(millisToExpire() / MILLISECONDS_PER_SECOND);
        //total minutes to go
        togo = (int)(togo / SECONDS_PER_MINUTE);

        int minutes = (int)(togo % MINUTES_PER_HOUR);
        //total hours to go
        togo = (int) (togo / MINUTES_PER_HOUR);

        int hours = (int)(togo % HOURS_PER_DAY);
        int days = (int)(togo / HOURS_PER_DAY);
        
        String dayString = (days == 1 ? "1 day " : days + " days ");
        String hourString = (hours == 1 ? "1 hour " : hours + " hours ");
        String minuteString = (minutes == 1 ? "1 minute " : minutes + " minutes ");
        String shelfLife = null;
        shelfLife = (days == 0 ? (hours == 0 ? minuteString : hourString + minuteString) : dayString + hourString + minuteString);
        
        return shelfLife;
    }
    
    /**
     * @param thresholdDays is the minimum number of days that a user must have
     * remaining before they may add more service
     * @param return true if the thresholdDays > shelfLife days
     */
    public boolean canGetMoreService(int thresholdDays) {
        if (isExpired()) {
            return true;
        }
        int togo;
        //total Seconds to go
        togo = (int)(millisToExpire() / MILLISECONDS_PER_SECOND);
        //total minutes to go
        togo = (int)(togo / SECONDS_PER_MINUTE);

        int minutes = (int)(togo % MINUTES_PER_HOUR);
        //total hours to go
        togo = (int) (togo / MINUTES_PER_HOUR);

        int hours = (int)(togo % HOURS_PER_DAY);
        int days = (int)(togo / HOURS_PER_DAY);
        
        return thresholdDays > days;
    }
    
    public void addUsage(int hours) {
        long millisInOneHour = MINUTES_PER_HOUR * SECONDS_PER_MINUTE * MILLISECONDS_PER_SECOND;
        long moreUsage = timeRemaining + (hours * millisInOneHour);
        String updateUsageQuery = "update expert_account_status set time_remaining = " + 
                                  moreUsage + " where expert_id = " + id;
        DataBase.getInstance().submit(updateUsageQuery);
    }
    
    public void addShelfLife(int days) {
        long millisInOneDay = HOURS_PER_DAY * MINUTES_PER_HOUR * SECONDS_PER_MINUTE * MILLISECONDS_PER_SECOND;
        //As a rule, we will add one extra minute to shelf life because it takes effectively some time
        //for a user to get from payment to the site and they are presented with a time that begain 
        // x milliseconds ago which appears poorly.  For example 7 days is instantly represented as
        // 6 days 23 hours and 59 minutes (milliseconds have passed).
        long millisInOneMinute = SECONDS_PER_MINUTE * MILLISECONDS_PER_SECOND;
        if (isExpired()) {
            //add from today
            terminationDateTime = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        }
        //add the days to the termination date
        terminationDateTime.setTimeInMillis(terminationDateTime.getTimeInMillis() + 
                (days * millisInOneDay) +
                millisInOneMinute);

        //format the date for writing to the db
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String termDate = sdf.format(new Date(terminationDateTime.getTimeInMillis()));
        String updateShelfLifeQuery = "update expert_account_status set end_date = '" + 
                                      termDate + "' where expert_id = " + id;
        DataBase.getInstance().submit(updateShelfLifeQuery);
    }
    
    public void addServices(int hours, int days) {
        addUsage(hours);
        addShelfLife(days);
    }
    
    public String getFormattedTotalSessions() {
        return "1000";
    }

    
    public String getFormattedSatisfactionValue() {
        return "99%";
    }
    
    public String getFormattedAverageSessionTime() {
        return "8";
    }
    
    /**
     * @deprecated use getAvatarURL instead
     */
    public String getAvatarLink() {
        StringBuffer avatarLink = 
        	new StringBuffer(WEB_URI_PREFIX + "://" + 
        			FULLY_QUALIFIED_DOMAIN_NAME + 
        			JAWS_APPS_DIR + "/expert/avatars/");
        if (expertDetails.isAvatarApproved()) {
            avatarLink.append(uid + ".jpg");
        } else {
            avatarLink.append("defaultAvatar.png");
        }
        
        return avatarLink.toString();
    }
    
    public URL getAvatarURL() {
        return expertDetails.getAvatarURL();
    }
    
    public String getAvatarFileType() {
        return expertDetails.getAvatarFileType();
    }
    
    public void setAvatarFileType(String extension) {
        expertDetails.setAvatarFileType(extension);
    }
    
    public String getFullPathAvatarFileName() {
        return expertDetails.getFullPathAvatarFileName();
    }
    
    public String getSelfDescription() {
        if (expertDetails == null ) {
            expertDetails = new ExpertDetails(this);
        }
        return expertDetails.getSelfDescription();
    }

    public void setSelfDescription(String description) {
        if (expertDetails == null ) {
            expertDetails = new ExpertDetails(this);
        }
        expertDetails.setSelfDescription(description);
    }
    
    public boolean isLive() {
        return expertDetails.isLive();
    }
    
    public String getAcceptButtonValue() {
        return "Yes, I want to get it fixed!";
    }
    
    public String getDoNotAcceptButtonValue() {
        return "Peh, I'll figure it out myself.";
    }
    
    public String getDoNotAcceptUrlLocation() {
        return WEB_URI_PREFIX + "://secure.helpguest.com/about.html";
    }
    
    public String getGuestsWaiting() {
        String waiting = email + ", you have " + 
                         (guestsWaiting == 1 ? "1 Guest waiting." : guestsWaiting + " Guests waiting.");
        return waiting;
    }
    
    public int getNumberGuestsWaiting() {
        return guestsWaiting;
    }
    
    public boolean isVerified() {
        return verified;
    }
    
    /**
     * A synonym for getHandle
     * @deprecated use getHandle instead
     */
    public String getUserName() {
        return handle;
    }

    public String getEmail() {
        return email;
    }
    
    /**
     * A synonym for getUserName
     */
    public String getHandle() {
        return handle;
    }
    
    public String getPaypalEmail() {
        return paypalEmail;
    }
    
    /**
     * @param the email address to use.
     * if the current paypal email address
     * equals the input parameter, no
     * change to persistent storage is made.
     * otherwise, this email is persisted
     */
    public void updatePaypalEmail(String email) {
        if (email == null || email.equals(paypalEmail)) {
            return;
        }
        String query = "update expert set paypal_email = ? " +
                "where id = ?";
        Object[] data = {email, new Integer(id)};
        DataBase.getInstance().submit(query,data);
        this.paypalEmail = email;
    }

    public void updateBio(String bio) {
    	String query = "update expert_details set about_me = ? where expert_id = ? ";
    	Object[] data = {bio, new Integer(id)};
    	DataBase.getInstance().submit(query, data);
    }
    
    public void updatePassword(String newPassword) {
        this.password = newPassword;
        String storedProc = "update expert set pass = " +
                "PASSWORD(?) where id = ?";
        Object[] data = {password, new Integer(id)};
        DataBase.getInstance().submit(storedProc,data);
    }
    
    public void submitSurvey (String sessionId, String strSurveyAction){
    	
    	String submitSurveyQuery = "";
    	if (strSurveyAction.equals("insert")){
    		
    		/*submitSurveyQuery = "insert into survey_1 (overall_satisfaction, " +
            "problem_solved, recommend, repeat_customer, professional_service, " +
            "session_id, expert_id) select ?, ?, ?, ?, ?, ?, ?";*/
    		
    		submitSurveyQuery = "insert into survey_1 (overall_satisfaction, " +
            "problem_solved, recommend, repeat_customer, professional_service, " +
            "session_id, expert_id) values (" + getOverallSatisfaction() + ", " + 
            getProblemSolved() + ", " + getRecommend() + ", " +
            getRepeatCustomer() + ", " + getProfessionalService() + ", " +
            "'" + sessionId + "', " + getId() + ")";

    		
    	}else if (strSurveyAction.equals("update")){
    		
    		/*submitSurveyQuery = "update survey_1 " +
			"set overall_satisfaction = ?," +
            "problem_solved = ?," +
            "recommend = ?," +
            "repeat_customer = ?," +
            "professional_service = ? where " +
            "session_id = ? and " +
            "expert_id = ?";*/
    		
    		submitSurveyQuery = "update survey_1 " +
			"set overall_satisfaction = " + getOverallSatisfaction() + ", " +
            "problem_solved = " + getProblemSolved() + ", " +
            "recommend = " + getRecommend() + ", " +
            "repeat_customer = " + getRepeatCustomer() + ", " +
            "professional_service = " + getProfessionalService() + " where " +
            "session_id = '" + sessionId + "' and " +
            "expert_id = " + getId();
    	}
    	
    	Object[] data = {Integer.parseInt(getOverallSatisfaction()),Integer.parseInt(getProblemSolved()),
    			Integer.parseInt(getRecommend()), Integer.parseInt(getRepeatCustomer()),
    			Integer.parseInt(getProfessionalService()), sessionId, getId()
    			};
    	//DataBase.getInstance().submit(submitSurveyQuery,this, data);
    	DataBase.getInstance().submit(submitSurveyQuery);
    }
    
    public String getWebVersion() {
        return webVersion;
    }
    
    public int getCanDemo() {
    	return canDemo;
    }
    
    public String getUid() {
        return uid;
    }
    
    public int getId() {
        return id;
    }
    
    
    public String getOverallSatisfaction() {
        return overallSatisfaction;
    }
    
    public void setOverallSatisfaction(String strSatisfaction){
    	this.overallSatisfaction = strSatisfaction;
    }
    
    public String getProblemSolved() {
        return problemSolved;
    }
    
    public void setProblemSolved(String strProblemSolved){
    	this.problemSolved = strProblemSolved;
    }
    
    public String getRecommend() {
        return recommend;
    }
    
    public void setRecommend(String strRecommend){
    	this.recommend = strRecommend;
    }
    
    public String getRepeatCustomer() {
        return repeatCustomer;
    }
    
    public void setRepeatCustomer(String strRepeatCustomer){
    	this.repeatCustomer = strRepeatCustomer;
    }
    
    public String getProfessionalService() {
        return professionalService;
    }
    
    public void setProfessionalService(String strProfessionalService){
    	this.professionalService = strProfessionalService;
    }
    
    public String getGuestUid() {
        return guestUid;
    }
    
    public ArrayList<String> getExpertise() {
        if (serviceOffering == null) {
            serviceOffering = ServiceOffering.getInstance(this);
        }
        return serviceOffering.getExpertise();
    }
    
    public void addExpertise(ArrayList<String> expertise) {
        if (serviceOffering == null) {
            serviceOffering = ServiceOffering.getInstance(this);
        }
        serviceOffering.addExpertise(expertise);
    }
    
    /**
     * Wrap this parameter in an ArrayList and use the ArrayList parameter method
     * @param expertise is to be added 
     */
    public void addExpertise(String expertise) {
        if (serviceOffering == null) {
            serviceOffering = ServiceOffering.getInstance(this);
        }
        serviceOffering.addExpertise(expertise);
    }
    
    public void removeExpertise(ArrayList<String> expertise) {
        if (serviceOffering == null) {
            serviceOffering = ServiceOffering.getInstance(this);
        }
        serviceOffering.removeExpertise(expertise);
    }

    /**
     * Wrap this parameter in an ArrayList and use the ArrayList parameter method
     * @param expertise is to be removed
     */
    public void removeExpertise(String expertise) {
        if (serviceOffering == null) {
            serviceOffering = ServiceOffering.getInstance(this);
        }
        serviceOffering.removeExpertise(expertise);       
    }

    /**
     * Update the primary expertise.
     * There are 2 possibilities.
     * 1) The parameter is already the primary expertise
     * 
     * 2) The parameter passed in is one of the values in the expertise list
     *    but not the primary expertise.
     *    In which case we should make the current primary a regular expertise
     *    And then set this new value as the primary.
     * 
     * 3) The parameter passed in is not in the expertise list.
     *    In which case we should make it the primary and move
     *    the current primary into the regular list, size permitting
     *    
     * @param primary is the value to make the primary expertise
     */
    public void updatePrimaryExpertise(String primary) {
        if (serviceOffering == null) {
            serviceOffering = ServiceOffering.getInstance(this);
        }
        serviceOffering.updatePrimaryExpertise(primary);
    }

    public void capture(java.sql.ResultSet rs, int queryId) throws SQLException { 
        
        switch (queryId) {
            case EAS:
                try {
                    while (rs.next()) {
                        //there should only be one result.
                        this.timeRemaining = Long.valueOf(rs.getLong("time_remaining"));
                        this.easId = rs.getInt("id");
                        this.endDate = rs.getTimestamp("end_date");
                        terminationDateTime.setTime(endDate);
                    }
                } catch (SQLException sqlx) {
                    log.error("could not capture account status info", sqlx);
                    throw sqlx;
                }
                break;
                
            case GUESTS_WAITING:
                rs.last();
                try {
                    guestsWaiting = rs.getRow();
                } catch (SQLException sqx) {
                    if (log.isInfoEnabled()) {log.info("No rows in the guest list.  Using value 0", sqx);}
                    guestsWaiting = 0;
                }
                break;
            case IDENTITY: 
                try {
                    rs.next();
                    this.id = rs.getInt("id");
                    this.email = rs.getString("email");
                    this.uid = rs.getString("uid");
                    this.handle = rs.getString("handle");
                    this.paypalEmail = rs.getString("paypal_email");
                    this.webVersion = rs.getString("web_version");
                    this.canDemo = rs.getInt("can_demo");
                    VALID_USER = true;
                } catch (SQLException sqx) {
                    log.error("Could not acquire identity for " + identifier);
                    VALID_USER = false;
                    this.email = "";
                }
                break;
            case VERIFIED:
                try {
                    if (rs.next() && rs.getInt("is_verified") != 0) {
                        verified = true;
	                }
                } catch (SQLException sqlx) {
                    log.error("Could not verify account for uid " + uid + " due to sql failure.",sqlx);
                }
                break;
            case GUEST_UID:
                try {
                    if (rs.next()) {
                        guestUid = rs.getString("uid");
                    }
                } catch (SQLException sqlx) {
                    log.error("Could not get the guestUid.", sqlx);
                }
                break;
            case SECRET:
                try {
                    //any result indicates that the password is a correct match
                    if (rs.next()) {
                        VALID_PASS = true;
                    } else {
                        VALID_PASS = false;
                    }
                } catch (SQLException sqlx) {
                    log.error("Could not verify password.", sqlx);
                }
                break;
            case SURVEY: 
                try {
                    if (rs.next()){
                    this.overallSatisfaction = rs.getString("overall_satisfaction");
                    this.problemSolved = rs.getString("problem_solved");
                    this.recommend = rs.getString("recommend");
                    this.repeatCustomer = rs.getString("repeat_customer");
                    this.professionalService = rs.getString("professional_service");
                    }
                   
                } catch (SQLException sqx) {
                    log.error("Could not acquires survey for sid " + sessionId + " exception: " + sqx.getMessage(), sqx);
                    
                }
                break;    
            default:
                if (log.isDebugEnabled()) {log.debug("Unknown query called with results expected.");}
                break;
        };
        
    }

    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }

    public int getQueryId() {
        return queryId;
    }

    public void uploadAvatarFile(String avatarImageFile){
    	
    }
    
}
