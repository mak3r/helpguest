/*
 * ServiceOffering.java
 *
 * Created on January 23, 2007, 8:53 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;

public class ServiceOffering implements ResultHandler {
    
    private int queryId = 0;
    private static final int EXPERTISE = 1;
    private ExpertAccount ea;
    private ArrayList<String> expertiseList;
    private static final int BASE_KEYWORD_COUNT = 10;
    private int expertiseLimit = BASE_KEYWORD_COUNT;

    private static final Logger log = Logger.getLogger("com.helpguest.data.ServiceOffering");

    /** Creates a new instance of ServiceOffering */
    private ServiceOffering(ExpertAccount ea) {
        this.ea = ea;
        updateExpertise();
    }
    
    protected static ServiceOffering getInstance(ExpertAccount ea) {
        ServiceOffering so = new ServiceOffering(ea);
        return so;
    }

    public static ServiceOffering newInstance(ExpertAccount ea, String offering) {
        //Insert a record for the primary expertise
        //split on commas - before the first comma is primary
        String[] keywords = offering.split(",");
        //remove white space
        Collection<String> trimmed = new ArrayList<String>();
        for( String next : keywords) {
            trimmed.add(next.trim());
        }
        keywords = trimmed.toArray(new String[0]);
        StringBuffer inserts = new StringBuffer("(" + ea.getId() + ",?,1)");
        for (int i = 1; i<keywords.length; i++) {
            inserts.append(",(" + ea.getId() + ",?,0)");
        }
        String offeringQuery = "insert into service_offerings (expert_id, keyword, is_primary) " +
                "values " + inserts;            
        DataBase.getInstance().submit(offeringQuery,keywords);

        return getInstance(ea);
    }
    
  //prahalad adding 7/23/08 alternate createAccount method to handle ArrayList of catOffgTags
    public static ServiceOffering newInstance(ExpertAccount ea, List catOffgTagList) {
        //Insert a record for the primary expertise
        //split on commas - before the first comma is primary
        /*
         * String[] keywords = offering.split(",");
        //remove white space
        Collection<String> trimmed = new ArrayList<String>();
        for( String next : keywords) {
            trimmed.add(next.trim());
        }
        keywords = trimmed.toArray(new String[0]);
        StringBuffer inserts = new StringBuffer("(" + ea.getId() + ",?,1)");
        for (int i = 1; i<keywords.length; i++) {
            inserts.append(",(" + ea.getId() + ",?,0)");
        }
        String offeringQuery = "insert into service_offerings (expert_id, keyword, is_primary) " +
                "values " + inserts;            
        DataBase.getInstance().submit(offeringQuery,keywords);
		*/
    	for (int listIt = 0;listIt<catOffgTagList.size();listIt++)
		{
			String catOffTag = catOffgTagList.get(listIt).toString();
			String category = catOffTag.substring(0,catOffTag.indexOf(':'));
			String offering = catOffTag.substring(catOffTag.indexOf(':') + 1, catOffTag.indexOf(':', catOffTag.indexOf(':') + 1) );
			String tag = catOffTag.substring(catOffTag.indexOf(':', catOffTag.indexOf(':') + 1) + 1).trim();
		
			String offeringQuery = "insert into expert_tags (cat_off_id, tag_name, expert_id) " +
            "values ((select cat_off_id from category_offerings where category = '" + category + "' " +
            		"and offering = '" + offering + "' ),'" + tag + "'," + ea.getId() + ")";
			
			if (!tag.equals("")){
		    	DataBase.getInstance().submit(offeringQuery);}
			
		}
    	
        return getInstance(ea);
    }

    /**
     * @param identifier is either the uid or email of the Expert
     * @param offering is the keyword, or keyword list (comma separated)
     * It it's a list, the first keyword is considered primary all following
     * keywords are considered non-primary
     */
    public static ServiceOffering newInstance(String identifier, String offering) {
        return newInstance(ExpertAccount.getExpertAccount(identifier),offering);
    }
    
    /**
     * @param set the limit on how many keywords this expert may have
     * 
     */
    public void setLimit(int keywordMax) {
        //TODO: implement - also needs a db field (on expert?)
    }
     
    /**
     *
     */
    public int getLimit() {
        return expertiseLimit;
    }
    
    /**
     * TODO: this may hit the db too often given that some clients
     * might iterate a list and call addExpertise - updating every time
     */
    private void updateExpertise() {
        setQueryId(EXPERTISE);
        String expertiseQuery = "select keyword from service_offerings where " +
                "expert_id = '" + ea.getId() + "' order by is_primary desc";
        DataBase.getInstance().submit(expertiseQuery,this);
    }
    
    /**
     * @return the expertise as a List.
     * The first item in the list is the primary expertise
     */
    public ArrayList<String> getExpertise() {        
        return expertiseList;
    }
    
    public void addExpertise(ArrayList<String> expertise) {
        //use a set to eliminate duplicate possibility
        HashSet<String> exSet = new HashSet<String>(expertiseList);
        StringBuffer toAdd = new StringBuffer();
        int remaining = expertiseLimit - expertiseList.size();
        for (String next : expertise) {
            if (exSet.size() < expertiseLimit && !exSet.contains(next)) {
                toAdd.append("(" + ea.getId() + ",'" + next.trim() + "'),");
                //put it in the set so we don't add it twice
                exSet.add(next);
            }
        }
        //remove the last comma
        if (toAdd.lastIndexOf(",") != -1) {
            toAdd.deleteCharAt(toAdd.lastIndexOf(","));
        }
        String expertiseQuery = "insert into service_offerings (expert_id,keyword) " +
                "values " + toAdd.toString();
        DataBase.getInstance().submit(expertiseQuery);
        updateExpertise();
    }
    
    /**
     * Wrap this parameter in an ArrayList and use the ArrayList parameter method
     * @param expertise is to be added 
     */
    public void addExpertise(String expertise) {
        ArrayList<String> list = new ArrayList<String>(1);
        list.add(expertise);
        addExpertise(list);
    }
    
    /**
     * It is not allowed that one should remove the primary expertise.
     * corollary: there must always remain at least one expertise 
     * corollary: there must always be a primary expertise
     */
    public void removeExpertise(ArrayList<String> expertise) {
        StringBuffer toRemove = new StringBuffer();
        for (String item : expertise) {
            toRemove.append( "'" + item + "',");
        }
        if (toRemove.lastIndexOf(",") != -1) {
            toRemove.deleteCharAt(toRemove.lastIndexOf(","));
        }
        String expertiseQuery = "delete from service_offerings where " +
                "expert_id = " + ea.getId() + " and keyword in ( " + toRemove
                + ") and is_primary = 0";
        DataBase.getInstance().submit(expertiseQuery);
        updateExpertise();
                
    }

    /**
     * Wrap this parameter in an ArrayList and use the ArrayList parameter method
     * @param expertise is to be removed
     */
    public void removeExpertise(String expertise) {
        ArrayList<String> list = new ArrayList<String>(1);
        list.add(expertise);
        removeExpertise(list);        
    }

    /**
     *
     */
    public void setPrimaryExpertise(String primary) {
        //change the primary to the requested value
        String primaryQuery = "update service_offerings set keyword = '" + 
                primary + "' where expert_id = " + ea.getId() + 
                " and is_primary = 1";
        DataBase.getInstance().submit(primaryQuery);
        updateExpertise();
    }
    
    /**
     * Update the primary expertise.
     * There are 2 possibilities.
     * 1) The parameter is already the primary expertise
     * 
     * 2) The parameter passed in is one of the values in the expertise list
     *    but not the primary expertise.
     *    In which case we should make the current primary a regular expertise
     *    And then set this new value as the primary.
     * 
     * 3) The parameter passed in is not in the expertise list.
     *    In which case we should make it the primary and move
     *    the current primary into the regular list, size permitting
     *    
     * @param primary is the value to make the primary expertise
     */
    public void updatePrimaryExpertise(String primary) {
        //Case 1 the new value is null or empty
        if (primary == null || "".equals(primary.trim())) {
            //there is nothing to do.
            return;
        }
        String existingPrimary = expertiseList.get(0);
        
        //Case 2 the new value exists as primary already
        if (primary.equalsIgnoreCase(existingPrimary)) {
            //this is already the primary do nothing
            return;
        }

        //Case 3 the new primary should be set
        //remove this requested primary from the regular list
        //This is required becuse the db will think this is a dup if it's in the
        //list already'
        if (expertiseList.contains(primary)) {
            removeExpertise(primary);
        }
        
        //Set the primary value
        setPrimaryExpertise(primary);

        //now make sure the previous primary element is in the list
        addExpertise(existingPrimary);
    }

    public void capture(ResultSet rs, int queryId) throws SQLException {
        switch (queryId) {
            case EXPERTISE:
                try {
                    expertiseList = new ArrayList<String>();
                    while (rs.next()) {
                        expertiseList.add(rs.getString("keyword"));
                    }
                } catch (SQLException sqlx) {
                    log.error("Could not get expertise.", sqlx);
                }
                break;
            default:
                if (log.isDebugEnabled()) {log.debug("Unknown query called with results expected.");}
                break;
        };
    }

    public void setQueryId(int id) {
        this.queryId = id;
    }

    public int getQueryId() {
        return queryId;
    }
    
}
