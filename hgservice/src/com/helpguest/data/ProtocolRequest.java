/*
 * ProtocolRequest.java
 *
 * Created on January 6, 2005, 5:33 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 */

package com.helpguest.data;

/**
 *
 * @author  mabrams
 */
public class ProtocolRequest {
    String type = null;
    String uname = null;
    String message = null;
    
    /** Creates a new instance of ProtocolRequest */
    public ProtocolRequest() {
    }
    
    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public java.lang.String getType() {
        return type;
    }    
    
    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }
    
    /**
     * Getter for property uname.
     * @return Value of property uname.
     */
    public java.lang.String getUname() {
        return uname;
    }
    
    /**
     * Setter for property uname.
     * @param uname New value of property uname.
     */
    public void setUname(java.lang.String uname) {
        this.uname = uname;
    }
    
    /**
     * Getter for property message.
     * @return Value of property message.
     */
    public java.lang.String getMessage() {
        return message;
    }
    
    /**
     * Setter for property message.
     * @param message New value of property message.
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }
    
}
