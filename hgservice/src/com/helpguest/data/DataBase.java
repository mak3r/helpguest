/*
 * DataBase.java
 *
 * Created on January 3, 2005, 2:57 PM
 * Copyright 2005 HelpGuest Technologies, Inc.
 */

package com.helpguest.data;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import com.helpguest.HGServiceProperties;
import com.helpguest.HelpGuestException;
import com.helpguest.HGServiceProperties.HGServiceProperty;




/**
 *
 * @author  mabrams
 */
public class DataBase {
    
    static DataBase db = null;
    static Connection conn = null;
    final static String DRIVER = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.DRIVER.getKey());
    final static String DB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.DB_URI_PREFIX.getKey());    	
    final static String DATABASE = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.DATABASE.getKey());
    private static String user = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.USER_NAME.getKey());
    private static String password = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.PASSWORD.getKey());
    private static String server = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.SERVER.getKey());
    private static String uri = DB_URI_PREFIX + server + DATABASE;
    private static final String zeroDateTimeBehavior = "zeroDateTimeBehavior=convertToNull";
    private static final int RETRIES = 5;
    private static final String COMMUNICATIONS_ERROR = "08S01";
    private static final String DEADLOCK = "41000";
    private static final Logger LOG = Logger.getLogger("com.helpguest.data.DataBase");
    
    
    /** Creates a new instance of DataBase */
    private DataBase() {
    	//TODO handle the connection to the database in the tomcat configuration
        try {
            Class.forName(DRIVER);
        } catch (Exception ex) {
            LOG.error("Could not load database driver " + DRIVER, ex);
        }
        
        try {
            
            
            //prahalad adding 9/4 
            /**/
             HGCipher authCipher;
 
             byte[] random_number = { (byte) 0xc7, (byte) 0x73, (byte) 0x21, (byte) 0x8c,
                (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99 };
 
	       //encryption see enrcypted pass in log
              String pass1 = "password";
              authCipher = new HGCipher(pass1, random_number);
             
              String strEncryptedUser = user;
              String[] arrEncryptedUser = strEncryptedUser.split(",");
        	  byte[] ourEncryptedData = new byte[arrEncryptedUser.length];
        
        	  for (int i=0;i<arrEncryptedUser.length;i++) { 
        	      //ourEncryptedData[i] = Byte.parseByte(arrEncryptedUser[i].trim());
        		  ourEncryptedData[i] = (byte)Integer.parseInt(arrEncryptedUser[i].trim());
        		
        	  }
              
        	  String realUser = authCipher.decrypt(ourEncryptedData);
              
               
              
        	  String strEncryptedPaswd = password;
        	  String[] arrEncryptedPaswd = strEncryptedPaswd.split(",");
        	  byte[] ourEncryptedPaswd = new byte[arrEncryptedPaswd.length];
        
        	  for (int i=0;i<arrEncryptedPaswd.length;i++) { 
        		  ourEncryptedPaswd[i] = Byte.parseByte(arrEncryptedPaswd[i].trim());
        		
        	  }
              
        	  String realPassword = authCipher.decrypt(ourEncryptedPaswd);

           
              uri = DB_URI_PREFIX + server + DATABASE + "?" + zeroDateTimeBehavior;
              conn = DriverManager.getConnection(uri, realUser, realPassword);
            
	    if (LOG.isDebugEnabled()) { LOG.debug("uri: " + uri); }
        } catch (SQLException sqlex) {
            LOG.error("Could not get connection to database.  URI: " + uri, sqlex);
        }
    }

    /**
     * Use the default setting to for the database connection
     */
    public static DataBase getInstance() {
    	if (db == null) {
            db = new DataBase();
        }
        return db;
    }
    //prahalad adding 6/24/08
    //public Connection getConnection(){ return this.conn;}

    /**
     * If the database has not already been initialized, it will be initialized
     * with the server, user and password provided.
     * If it has already been initialized by a call to DataBase.getInstance() or
     * DataBase.getInstance(Sring, String, String) then no modifications to the
     * server, user or password will be made.  The existing DataBase connection
     * will be returned.
     * Use the getServer and getUser methods to find the current user and server
     * names.
     * @param server is the server to connect to
     * @param user is the username to use
     * @param password is the password to use with the given username
     */
    public static DataBase getInstance(String server, String encryptedUser, String encryptedPassword) {
        if (db == null) {
            DataBase.server = server;
            DataBase.user = encryptedUser;
            DataBase.password = encryptedPassword;
            db = new DataBase();
        }
        return db;
    }
    
    /**
     * Submit a prepared statement and the value entries for that prepared statement.
     * @param preparedStatement a prepared sql statement
     * @param handler the results handler
     * @param data a 2D array where dimension one is the type and dimension 2 is the actual data.
     * The types currently handled for dimension 1 are Int, Float, and String.
     * Objects are inserted into the preparedStatement in array order.
     */
    public void submit(String preparedStatement, ResultHandler handler, Object[] data) {
         int retryCount = 0;
        
        while (retryCount++ < RETRIES) {
            String sqlState = submit(preparedStatement, handler, data, true);
            if (sqlState != null) {
                if (COMMUNICATIONS_ERROR.equals(sqlState) || DEADLOCK.equals(sqlState)) {
                    //Try reestablishing the database connection
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("retrying preparedStatement (" + retryCount + "): '" + preparedStatement + "'");
                    }
                    conn = null;
                    db = new DataBase();
                } else {
                    LOG.error("Cannot retry prepared statement due to sql state failure: '" + sqlState + "'");
                    break;
                }
            } else {
                break;
            }
        }
    }
    
    /**
     * Submit a prepared statement and the value entries for that prepared statement.
     * @param preparedStatement a prepared sql statement
     * @param handler the results handler
     * @param data a 2D array where dimension one is the type and dimension 2 is the actual data.
     * The types currently handled for dimension 1 are Long, Date, Integer, Float, and String.
     * Objects are inserted into the preparedStatement in array order.
     * @param try again if 
     */
    private String submit(String preparedStatement, ResultHandler handler, Object[] data, boolean retry) {
        PreparedStatement pstmt = null; 
        ResultSet rs = null; 

        try {
            pstmt = conn.prepareStatement(preparedStatement);
            int i = 1;
            for (Object next : data) {
                if (next instanceof Integer) {
                    pstmt.setInt(i, ((Integer)next).intValue());
                } else if (next instanceof Float) {
                    pstmt.setFloat(i, ((Float)next).floatValue());
                } else if (next instanceof String) {
                    pstmt.setString(i, (String)next);
                } else if (next instanceof Date) {
                    pstmt.setDate(i, (Date)next);
                } else if (next instanceof Long) {
                    pstmt.setLong(i, ((Long)next).longValue());
                }
                i++;
            }
            if (LOG.isDebugEnabled()) {LOG.debug(pstmt.toString());}
            rs = pstmt.executeQuery();
            if (rs != null) { 
                handler.capture(rs, handler.getQueryId());
            } else {
                LOG.error("prepared sql statement was invalid, no results were returned: '" + preparedStatement + "'");
            }
        } catch (SQLException sqlx) {
            String sqlState = sqlx.getSQLState();
            LOG.error("failed running preparedStatement: '" + preparedStatement + "' in ResultHandler: " + handler +
                      " due to sql state: '" + sqlState + "'");
            return sqlState;
        } finally { 
            // release resources 
            if (rs != null) { 
                try {
                    rs.close();                     
                } catch (SQLException sqlEx) { 
                    if (LOG.isEnabledFor(Priority.WARN)) {LOG.warn("failed to close result set.", sqlEx);}
                } 
                rs = null; 
            }

            if (pstmt != null) { 
                try { 
                    pstmt.close(); 
                } catch (SQLException sqlEx) { 
                    if (LOG.isEnabledFor(Priority.WARN)) {LOG.warn("failed to close statement.", sqlEx);}
                }
                pstmt = null; 
            } 
        }
        return null;
    }
    
    /**
     * Submit a prepared statement and the value entries for that prepared statement.
     * @param preparedStatement a prepared sql statement.
     * It should be an insert or update statement.
     * @param handler the results handler
     * @param data a 2D array where dimension one is the type and dimension 2 is the actual data.
     * The types currently handled for dimension 1 are Int, Float, and String.
     * Objects are inserted into the preparedStatement in array order.
     */
    public void submit(String preparedStatement, Object[] data) {
         int retryCount = 0;
        
        while (retryCount++ < RETRIES) {
            String sqlState = submit(preparedStatement, data, true);
            if (sqlState != null) {
                if (COMMUNICATIONS_ERROR.equals(sqlState) || DEADLOCK.equals(sqlState)) {
                    //Try reestablishing the database connection
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("retrying preparedStatement (" + retryCount + "): '" + preparedStatement + "'");
                    }
                    conn = null;
                    db = new DataBase();
                } else {
                    LOG.error("Cannot retry prepared statement due to sql state failure: '" + sqlState + "'");
                    break;
                }
            } else {
                break;
            }
        }
    }
    
    /**
     * Submit a prepared statement and the value entries for that prepared statement.
     * @param preparedStatement a prepared sql statement.  The statement must not 
     * return a result set, it should be an insert or update statement
     * @param handler the results handler
     * @param data a 2D array where dimension one is the type and dimension 2 is the actual data.
     * The types currently handled for dimension 1 are Long, Date, Integer, Float, and String.
     * Objects are inserted into the preparedStatement in array order.
     * 
     */
    private String submit(String preparedStatement, Object[] data, boolean retry) {
        PreparedStatement pstmt = null; 
        ResultSet rs = null; 

        try {
            pstmt = conn.prepareStatement(preparedStatement);
            int i = 1;
            for (Object next : data) {
                if (next instanceof Integer) {
                    pstmt.setInt(i, ((Integer)next).intValue());
                } else if (next instanceof Float) {
                    pstmt.setFloat(i, ((Float)next).floatValue());
                } else if (next instanceof String) {
                    pstmt.setString(i, (String)next);
                } else if (next instanceof Date) {
                    pstmt.setDate(i, (Date)next);
                } else if (next instanceof Long) {
                    pstmt.setLong(i, ((Long)next).longValue());
                } else if (next instanceof Boolean) {
                	pstmt.setBoolean(i, ((Boolean) next).booleanValue());
                }
                i++;
            }
            if (LOG.isDebugEnabled()) {LOG.debug(pstmt.toString());}
            pstmt.executeUpdate();
        } catch (SQLException sqlx) {
            String sqlState = sqlx.getSQLState();
            LOG.error("failed running preparedStatement: '" + pstmt.toString() + 
                      " due to sql state: '" + sqlState + "'");
            //lets try running it through an insert
            //return sqlState;
            //just use the part after the ':'
            String[] objString = pstmt.toString().split(":");
            return submit(objString[1],true);
        } finally { 
            // release resources 
            if (rs != null) { 
                try {
                    rs.close();                     
                } catch (SQLException sqlEx) { 
                    if (LOG.isEnabledFor(Priority.WARN)) {LOG.warn("failed to close result set.", sqlEx);}
                } 
                rs = null; 
            }

            if (pstmt != null) { 
                try { 
                    pstmt.close(); 
                } catch (SQLException sqlEx) { 
                    if (LOG.isEnabledFor(Priority.WARN)) {LOG.warn("failed to close statement.", sqlEx);}
                }
                pstmt = null; 
            } 
        }
        return null;
    }
    
    /**
     * Submit a sql query and get a collection of results back
     * @param query is the sql query to execute.
     * @param handler is the handler that parses the results
     */
    public void submit(String query, ResultHandler handler) {
        int retryCount = 0;
        
        while (retryCount++ < RETRIES) {
            String sqlState = submit(query, handler,true);
            if (sqlState != null) {
                if (COMMUNICATIONS_ERROR.equals(sqlState) || DEADLOCK.equals(sqlState)) {
                    //Try reestablishing the database connection
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("retrying query (" + retryCount + "): '" + query + "'");
                    }
                    conn = null;
                    db = new DataBase();
                } else {
                    LOG.error("Cannot retry query due to sql state failure: '" + sqlState + "'");
                    break;
                }
            } else {
                break;
            }
        }
    }
    
    /**
     * Submit a sql query and get a collection of results back
     * @param query is the sql query to execute.
     * @param handler is the handler that parses the results
     * @return the sqlstate if a sql exception is handled. null if the query succeeds
     */
    private String submit(String query, ResultHandler handler, boolean retry) {
        Statement stmt = null; 
        ResultSet rs = null; 

        try {
            stmt = conn.createStatement(); 
            rs = stmt.executeQuery(query);
            if (LOG.isDebugEnabled()) { 
            	LOG.debug("Executing query with return results expected: '" + query + "'" ); 
            	//LOG.debug("Using user: " + this.user + " password:" + this.password);
            	}
            
            if (rs != null) { 
                handler.capture(rs, handler.getQueryId());
            } else {
                LOG.error("sql statement was invalid, no results were returned: '" + query + "'");
            }
        } catch (SQLException sqlex) {
            String sqlState = sqlex.getSQLState();
            LOG.error("failed running query: '" + query + "' in ResultHandler: " + handler +
                      " due to sql state: '" + sqlState + "'");
            return sqlState;
        } finally { 
            // release resources 
            if (rs != null) { 
                try {
                    rs.close();                     
                } catch (SQLException sqlEx) { 
                    if (LOG.isEnabledFor(Priority.WARN)) {LOG.warn("failed to close result set.", sqlEx);}
                } 
                rs = null; 
            }

            if (stmt != null) { 
                try { 
                    stmt.close(); 
                } catch (SQLException sqlEx) { 
                    if (LOG.isEnabledFor(Priority.WARN)) {LOG.warn("failed to close statement.", sqlEx);}
                }
                stmt = null; 
            } 
        }
        return null;
    }


    /**
     * Submit a sql query that does not expect results back
     * i.e. insert, update, delete.
     * @param query is the sql query to execute.
     */
    public void submit(String query) {
        int retryCount = 0;
        
        while (retryCount++ < RETRIES) {
            String sqlState = submit(query, true);
            if (sqlState != null) {
                if (COMMUNICATIONS_ERROR.equals(sqlState) || DEADLOCK.equals(sqlState)) {
                    //Try reestablishing the database connection
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("retrying query (" + retryCount + "): '" + query + "'");
                    }
                    conn = null;
                    db = new DataBase();
                } else {
                    LOG.error("Cannot retry query due to sql state failure: '" + sqlState + "'");
                    break;
                }
            } else {
                break;
            }
        }
    }
    
    /**
     * Submit a sql query that does not expect results back
     * i.e. insert, update, delete.
     * @param query is the sql query to execute.
     */
    private String submit(String query, boolean retry) {
	if (LOG.isDebugEnabled()) {LOG.debug("preparing to execute query: " + query);}
        Statement stmt = null; 
        ResultSet rs = null; 

        try {
            stmt = conn.createStatement();
            stmt.execute(query);
	    if ( LOG.isDebugEnabled() ) { LOG.debug("executed query: " + query); } 
        } catch (SQLException sqlex) {
            String sqlState = sqlex.getSQLState();
            LOG.error("failed running query: '" + query + "' due to sql state: ' " + 
                       sqlState + "'.", sqlex);  
            return sqlState;
        } finally { 
            // release resources 
            if (rs != null) { 
                try {
                    rs.close();                     
                } catch (SQLException sqlEx) { 
                    if (LOG.isEnabledFor(Priority.WARN)) {LOG.warn("failed to close result set.", sqlEx);}
                } 
                rs = null; 
            }

            if (stmt != null) { 
                try { 
                    stmt.close(); 
                } catch (SQLException sqlEx) { 
                    if (LOG.isEnabledFor(Priority.WARN)) {LOG.warn("failed to close statement.", sqlEx);}
                }
                stmt = null; 
            } 
        }
        return null;
    }
    
    /**
     * Build and insert string that is valid sql.
     * number of columns and values must match or a HelpGuestException is thrown.
     * ordering of values and columns must be syncronized or the data may go into 
     * incorrect columns.  This method only works when all the values
     * and columns are String data.
     *
     * @param tableName is the name of the table to insert into.
     * @param columns is the array of columns to insert data into
     * @param values are the values to insert.
     * @return a valid sql string
     */
    public static String buildInsert(String tableName, String[] columns, String[] values) throws HelpGuestException {
        if (columns.length != values.length) {
            throw new HelpGuestException("in buildInsert method, fields and values must be the same size arrays.");
        }
        String query = "insert into " + tableName + buildFields(columns) + buildValues(values);
        return query;
    }
    
    private static String buildFields(String[] fields) {
        StringBuffer f = new StringBuffer(" (");
        for (int i = 0; i < fields.length; i++) {
            if (i < fields.length -1 ) {
                f.append(fields[i] + ", ");
            } else {
                f.append(fields[i] + ") ");
            }
        }
        return f.toString();
    }

    private static String buildValues(String[] values) {
        StringBuffer v = new StringBuffer(" values ('");
        for (int i = 0; i < values.length; i++) {
            if (i < values.length -1 ) {
                v.append(values[i] + "', '");
            } else {
                v.append(values[i] + "')");
            }
        }
        return v.toString();
    }
    
    public String getServer() {
        return server;
    }
    
    public String getUser() {
        return user;
    }

    public static void main( String[] args ) {
        org.apache.log4j.BasicConfigurator.configure(new org.apache.log4j.ConsoleAppender(
				new org.apache.log4j.SimpleLayout()));
	String query = "select * from guest";
	if (args.length == 1) {
	    server = args[0];
	}
	class TestHandler implements ResultHandler {
	    public void capture(ResultSet rs, int queryId) throws java.sql.SQLException {
		while(rs.next()) {
   		    LOG.info(rs.getString("user")+","+rs.getString("uid"));
		}
	    }
          
	    public int getQueryId() { 
	        return 0;
            }

	    public void setQueryId(int id) {
	    }

	    @Override
		public String toString() {
	        return "TestHandler";
	    }

	};
        try {
	    DataBase.getInstance().submit(query, new TestHandler());
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }
}

