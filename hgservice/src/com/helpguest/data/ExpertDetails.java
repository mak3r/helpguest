/*
 * ExpertDetails.java
 *
 * Created on December 21, 2006, 12:18 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;

public class ExpertDetails implements ResultHandler {
   
	private static final Logger LOG = Logger.getLogger(ExpertDetails.class);
	
	private static String WEB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey());
	private static String myfqdn = 
	     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
	private static String webroot = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_ROOT.getKey());
	
    private ExpertAccount ea;
    private boolean liveExpert = false;
    private static final String DEFAULT_DESCRIPTION = "some words about your expertise";
    private static final String AVATAR_DEFAULT_URL = WEB_URI_PREFIX + "://" + myfqdn + "/jaws/apps/expert/avatars/";
    private static final String AVATAR_DEFAULT_LOCATION = webroot + "/jaws/apps/expert/avatars/";
    private static final String DEFAULT_AVATAR = "defaultAvatar.png";
    private String aboutMe = DEFAULT_DESCRIPTION;
    private String extension = null;
    private boolean avatarApproved = false;
    private boolean EXISTS = false;
    
    private int queryId = -1;
    private static final int LIVE_EXPERT = 0;
    private static final int ABOUT_ME= 1;
    
    private static final Logger log = Logger.getLogger("com.helpguest.data.ExpertDetails");
    
    /** Creates a new instance of ExpertDetails */
    public ExpertDetails(ExpertAccount ea) {
        this.ea = ea;
        if (ea != null) {
            queryId = ABOUT_ME;
            String preparedStatement = "select about_me, image_approved, extension " +
                    "from expert_details " +
                    "where expert_id = ? ";
            Object[] data = {new Integer(ea.getId())};
            DataBase.getInstance().submit(preparedStatement,this,data);
            //If for some reason a detail record was not created,
            // we are now going to create one by inserting a default
            // self description
            if (!EXISTS) {
            	setSelfDescription(DEFAULT_DESCRIPTION);
            }
        }
    }
        
    private void insertSelfDescription() {
        String preparedStatement = "insert into expert_details (expert_id, about_me) " +
                "values (?, ?)";
        Object[] data = {new Integer(ea.getId()), aboutMe};
        DataBase.getInstance().submit(preparedStatement,data);
    }
    
    private void updateSelfDescription() {
        String preparedStatement = "update expert_details set about_me = ? where expert_id = ? ";
        Object[] data = {aboutMe, new Integer(ea.getId())};
        DataBase.getInstance().submit(preparedStatement,data);
    }

    public void setSelfDescription(String description) {
        if (!aboutMe.equals(description)) {
            this.aboutMe = description;
        }
        if (!EXISTS) {
            insertSelfDescription();
        } else {
            updateSelfDescription();
        }
    }
    
    public String getSelfDescription() {
        return aboutMe;
    }
    
    public String getAvatarFileType() {
        return extension;
    }
    
    public void setAvatarFileType(String extension) {
        //only hit the db if there is a change
        if (extension != null && !extension.equals(this.extension)) {
            this.extension = extension;
            String query = "update expert_details set extension = ? " +
                    "where expert_id = ? ";
            Object[] data = {extension,ea.getId()};
            DataBase.getInstance().submit(query,data);
        }
    }
    
    public String getAvatarFileName() {
        return ea.getUid() + "." + getAvatarFileType();
    }
    
    public String getFullPathAvatarFileName() {
        return AVATAR_DEFAULT_LOCATION + getAvatarFileName();
    }
    
    public boolean avatarExists() {
        File avatarFile = new File(AVATAR_DEFAULT_LOCATION + getAvatarFileName());
        return avatarFile.exists();
    }
    
    public URL getAvatarURL() {
        URL url = null;
        try {
            if (avatarExists()) {
                url = new URL(AVATAR_DEFAULT_URL + getAvatarFileName());
                if (LOG.isDebugEnabled()) {
                	LOG.debug("User submitted avatar exists. Using avatar url: " + url.getPath());
                }
            } else {
                url = new URL(AVATAR_DEFAULT_URL + DEFAULT_AVATAR);
                if (LOG.isDebugEnabled()) {
                	LOG.debug("User submitted avatar _does_not_ exist. Using default avatar url: " + url.getPath());
                }
            }
        } catch (MalformedURLException mux) {
            log.error("could not create url for avatar file", mux);
        }

        return url;
    }
    
    public File getAvatarFile() {
        File avatarFile = new File(AVATAR_DEFAULT_LOCATION + getAvatarFileName());
        if (!avatarExists()) {
            avatarFile = new File(AVATAR_DEFAULT_LOCATION + DEFAULT_AVATAR);
        }
        return avatarFile;
    }
        
    public boolean isAvatarApproved() {
        return avatarApproved;
    }
    
    public boolean isLive() {
        queryId = LIVE_EXPERT;
        String query = "select is_live from live_expert " +
                "where expert_id = ? ";
        Object[] data = {ea.getId()};
        DataBase.getInstance().submit(query,this,data);
        return liveExpert;
    }

    public void capture(ResultSet rs, int queryId) throws SQLException {
        switch(queryId) {
            case ABOUT_ME:
                try {
                    if (rs.next()) {
                        aboutMe = rs.getString("about_me");
                        extension = rs.getString("extension");
                        if (rs.getInt("image_approved")!=0) {
                            avatarApproved = true;
                        }
                        EXISTS = true;
                    } else {
                        EXISTS = false;
                    }
                } catch (SQLException sqlx) {
                    //This error is thrown if there is a problem accessing the database
                    log.error("Failed to capture data set.", sqlx);
                }
                break;
            
            case LIVE_EXPERT:
                try {
                    if (rs.next()) {
                        if (rs.getInt("is_live") > 0) {
                            this.liveExpert = true;
                        }
                    }
                } catch (SQLException sqlx) {
                    log.error("could not determine live status.", sqlx);
                }
                break;
              
            default:
                break;
        }
    }

    public void setQueryId(int id) {
        this.queryId = id;
    }

    public int getQueryId() {
        return queryId;
    }
    
}
