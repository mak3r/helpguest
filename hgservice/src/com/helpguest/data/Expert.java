/*
 * Expert.java
 *
 * Created on July 12, 2006, 5:17 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public class Expert implements Comparable {
    
    private static final int PRE = -1;
    private static final int POST = 1;
    private static final int EQUAL = 0;
    public static final String NO_FEE = "NO_FEE";

    private String primaryKeyword = null;
    private Collection<String> keywordList = new ArrayList<String>();
    private String rate = NO_FEE;
    private int rating = 0;
    private int live = 0;
    private ExpertAccount ea;
    private Guest guest;
    private Rates expertRates;

    private String guestUID = null;
    private String handle = null;



    /** Creates a new instance of Expert */
    public Expert(String guestUID, String handle) {
        this.guestUID = guestUID;
        this.handle = handle;
        this.guest = Guest.getInstance(guestUID);
        this.ea = ExpertAccount.getExpertAccount(guest.getExpertUid());
        if ("2".equals(ea.getWebVersion())) {
            this.expertRates = HGRates.getCurrentInstance(ea);
            if (expertRates == null) {
                this.expertRates = HGRates.newInstance(ea, HGRates.MIN_RATE, HGRates.BASE_TIME);
            }
            this.rate = expertRates.getFormattedMinRate();
        }
    }
    
    public void setPrimaryKeyword(String primaryKeyword) {
        this.primaryKeyword = primaryKeyword;
    }

    public String getPrimaryKeyword() {
        return primaryKeyword;
    }
    
    public boolean hasPrimary() {
        return (primaryKeyword != null);
    }

    public void setKeywordList(Collection<String> keywordList) {
        this.keywordList = keywordList;
    }

    public Collection<String> getKeywordList() {
        return keywordList;
    }

    public void addKeyword(String keyword) {
        keywordList.add(keyword);
    }

    public void removeKeyword(String keyword) {
        //if it's also the primary, clear that value'
        if (keyword.equals(primaryKeyword)) {
            primaryKeyword = null;
        }
        keywordList.remove(keyword);
    }
    
    public String getExpertise() {
        StringBuffer list = new StringBuffer();
        for (String keyword : keywordList) {            
            list.append(keyword + ", ");
        }
        list.deleteCharAt(list.lastIndexOf(","));
        return list.toString();
    }
    
    public ExpertAccount getExpertAccount() {
        return ea;
    }
    
    public String getRate() {
        return rate;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
    
    public void setLive(int live) {
        this.live = live;
    }

    public int getLive() {
        return live;
    }

    public String getGuestUID() {
        return guestUID;
    }

    public String getHandle() {
        return handle;
    }

    public static Comparator<Expert> StandardRankingComparator = new Comparator<Expert>() {
        public int compare(Expert expertA, Expert expertB) throws ClassCastException {
            if (!(expertA instanceof Expert) || !(expertB instanceof Expert)) {
                throw new ClassCastException("An Expert object is expected.");
            }

            Expert expertACast = (Expert)expertA;
            Expert expertBCast = (Expert)expertB;

            if (expertBCast.getLive() < expertACast.getLive()) {
                return PRE;
            }

            if (expertBCast.getLive() > expertACast.getLive()) {
                return POST;
            }

            //They are equal in live comparison
            // Having a primary keyword match is ranked higher than not.
            if (!expertBCast.hasPrimary() & expertACast.hasPrimary()) {
                return PRE;
            }

            if (expertBCast.hasPrimary() & !expertACast.hasPrimary()) {
                return POST;
            }

            //They are equal in primary keyword comparison
            // A longer keyword list is higer ranked than less keyword matches
            if (expertBCast.keywordList.size() < expertACast.keywordList.size()) {
                return PRE;
            }

            if (expertBCast.keywordList.size() > expertACast.keywordList.size()) {
                return POST;
            }

            //They are equal in terms of number of keyword matches
            // A lower rate is higher ranked
            if (expertBCast.rate.equals(NO_FEE)) {
                return PRE;
            }
            
            if (expertACast.rate.equals(NO_FEE)) {
                return PRE;
            }
            
            if (expertBCast.rate.compareTo(expertACast.rate) < 0) {
                return PRE;
            }

            if (expertBCast.rate.compareTo(expertACast.rate) > 0) {
                return POST;
            }

            //They are equal in terms of rate
            // A higher rating is higher ranked
            if (expertBCast.rating < expertACast.rating) {
                return PRE;
            }

            if (expertBCast.rating > expertACast.rating) {
                return POST;
            }

            //They are ranked equally
            return EQUAL;
        }
    };

    /** 
     * @return 0 if objects are equal. < 0 if anotherExpert preceeds this.
     * and > 0 if this preceeds anotherExpert.
     */
    public int compareTo(Object anotherExpert) throws ClassCastException {
        if (!(anotherExpert instanceof Expert)) {
            throw new ClassCastException("An Expert object is expected.");
        }
        
        return this.handle.compareTo(((Expert)anotherExpert).getHandle());
    }

    @Override
	public boolean equals(Object that) {
        if (!(that instanceof Expert) || that == null) {
            return false;
        }

        return this.handle.toUpperCase().equals(((Expert)that).getHandle().toUpperCase());
    }
    
    @Override
	public String toString() {        
        return "{" + guestUID + ", " + handle + ", " + primaryKeyword + ", " +
               keywordList + ", " + rate + ", " + rating + ", " + live + "}";
    }
}
