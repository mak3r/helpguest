/*
 * Guest.java
 *
 * Created on November 9, 2006, 9:00 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;

public class Guest implements ResultHandler {
    
    private boolean INVALID = false;
    private String guestUid;
    private int id;
    private String expertUid;
    
    private static final Logger log = Logger.getLogger("com.helpguest.data.Guest");
    
    /** Creates a new instance of Guest */
    private Guest() {
    }
    
    public static Guest getInstance(String guestUid) {
        Guest guest = new Guest();
        String guestQuery = "select guest.id, guest.uid as guest_uid, expert.uid as expert_uid " +
                "from guest, expert " +
                "where guest.uid = '" + guestUid + "' and " +
                "expert.id = guest.expert_id";
        DataBase.getInstance().submit(guestQuery,guest);
        if (guest.INVALID) {
            guest = null;
        }
        return guest;
    }
    
    public String getExpertUid() {
        return expertUid;
    }

    public void capture(ResultSet rs, int queryId) throws SQLException {
        try {
            if (rs.next()) {
                guestUid = rs.getString("guest_uid");
                id = rs.getInt("id");
                expertUid = rs.getString("expert_uid");
            } else {
                log.error("There was no matching record found for the guest.");
                INVALID = true;
            }
        } catch (SQLException sqlx) {
            log.error("Could not get guest record data successfully.", sqlx);
            INVALID = true;
        }
    }

    public void setQueryId(int id) {
    }

    public int getQueryId() {
        return 0;
    }
    
}
