/*
 * Authenticator.java
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2005 - HelpGuest Technologies, Inc.
 */

package com.helpguest.data;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.helpguest.gwt.protocol.AuthenticationOutcome;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.service.JnlpGen;

/**
 *
 * @author mabrams
 */
public class Authenticator implements ResultHandler {

	private static final LoginType CLIENT = new LoginType.Client();
	private static final LoginType EXPERT = new LoginType.Expert();
	private static final LoginType ADMIN = new LoginType.Admin();
	private static final LoginType UNKNOWN = new LoginType.Unknown();
	
    private static final AuthenticationOutcome SUCCESS = 
    	new AuthenticationOutcome.Success();
    private static final AuthenticationOutcome FAIL_USER_PASS =
    	new AuthenticationOutcome.FailUserPass();
    
    /*
    private static final AuthenticationOutcome FAIL_CURRENTLY_CONNECTED =
    	new AuthenticationOutcome.FailCurrentlyConnected();
    */
    private static final AuthenticationOutcome FAIL_DUPLICATE =
    	new AuthenticationOutcome.FailDuplicate();
    private static final AuthenticationOutcome INTERNAL_ERROR =
    	new AuthenticationOutcome.InternalError();
    private static final AuthenticationOutcome UNKNOWN_ERROR =
    	new AuthenticationOutcome.UnknownError();
    private static final AuthenticationOutcome UNKNOWN_LOGIN_TYPE =
    	new AuthenticationOutcome.UnknownLoginType();
    private static final AuthenticationOutcome USER_NOT_VERIFIED =
    	new AuthenticationOutcome.UserNotVerified();
    
    public enum QueryId {
        AUTHENTICATE,
        RETRY_LIMIT;
    }
    
    private AuthenticationOutcome outcome = UNKNOWN_ERROR;
    private LoginType loginType;
    private int queryId;
    private String uid; //Can be either the guest's or expert's uid.
    private int expertUid; //used when the login type is guest to id their expert
    private static final Logger LOG = Logger.getLogger(Authenticator.class);
                    
    
    /** Creates a new instance of Authenticator */
    public Authenticator() {
    }
    
    private String queryAuthentication(final String email, final String pass) {
        String query = "select * from " + loginType + " where email = '" + email +
                       "' and pass = PASSWORD('" + pass + "')";
        DataBase.getInstance().submit(query, this);
        
        //TODO: also authenticate by number of login attempts
        // fail if login attempts is > [some arbitrary number]
        // over some period of time (5mins. for example).
        String file = FileSystem.fullPath(FileSystem.nameJnlpFile(loginType,uid));
        if (LOG.isDebugEnabled()) {LOG.debug("Checking for existence of file " + file);}
        /*
         * @TODO: 1 possibility is to allow login but provide detailed message
         *  indicating that usage may fail.  2 possibility is to give the 
         *  expert a way of clearing outdated logins. 
         * Do not perform this check at this time.  Failure to use the application
         * after authenticating will lock out the email
         *
        if (fileExists(file)) {
            //This outcome supercedes all other outcomes.
            outcome = FAIL_CURRENTLY_CONNECTED;
        }
         */
        
        // Record the outcome
        String resultQuery = "insert into " + loginType + "_authenticator_log" +
               " (outcome, email) values ('" + outcome.toString() + "', '" + email + "')";
        DataBase.getInstance().submit(resultQuery);
        
        return outcome.toString();
    }

    public void capture(final ResultSet rs, final int queryId) {
        if (rs == null) {
            outcome = FAIL_USER_PASS;
        } else {
            if (LOG.isDebugEnabled()) {LOG.debug("Counting the rows returned.");}
            //Count the rows returned
            int cnt = 0;
            try {
                while (rs.next()) {
                    cnt++;
                }
            if (LOG.isDebugEnabled()) {LOG.debug( cnt + " rows returned.");}
                
            } catch (java.sql.SQLException sqlx) {
                LOG.error("Could not iterate result set.", sqlx);
                outcome = INTERNAL_ERROR;
            }
            if (cnt == 1) {
                try {
                    //reset the result set
                    rs.beforeFirst();
                    rs.next();
                } catch (SQLException sqlx) {
                    LOG.error("Error attempting to reset result set cursor to first row.",sqlx);
                }
                //count is one, it's an exact match
                //The db should never allow more than one
                outcome = SUCCESS;
                try {
                    uid = rs.getString("uid");
                    if ("null".equals(uid)) {
                        outcome = INTERNAL_ERROR;
                        LOG.error("uid of null is not allowed!");
                        return;
                    }
                    if (CLIENT.equals(loginType)) {
                        expertUid = rs.getInt("expert_id");
                    }
                } catch (SQLException sqlx) {
                    LOG.error("Could not get data from result set.",sqlx);
                    outcome = INTERNAL_ERROR;
                }
            } else if (cnt > 1) {
                LOG.error("database error. duplicate email addresses");
                outcome = FAIL_DUPLICATE;
            } else {
                outcome = FAIL_USER_PASS;
            }
        }
    }
    
    /**
     * @param fileName is the full path to the file 
     * @return true if the file is found on the file system
     * or false if it does not exist.
     */
    protected boolean fileExists(final String fileName) {
        File file = new File(fileName);
        return file.exists();
    }

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(final int id) {
        queryId = id;
    }

    /**
     * The same as calling authenticate with the 
     * boolean parameter as false
     * 
     * @param email String
     * @param password String 
     * @param type LoginType
     * @return AuthenticationOutcome
     */
    public AuthenticationOutcome authenticate(
    		final String email,
    		final String password,
    		final LoginType type) {
    	
    	return authenticate(email, password, type, false);
    }
    
    public AuthenticationOutcome authenticate(final String email,
			final String password, final LoginType type,
			final boolean doGenerateJnlp) {
		//Identify the type before calling authenticate
		if (type == null) {
			String message = "Login type unspecified.  Cannot continue";
			LOG.error(message);
			loginType = UNKNOWN;
		} else if (ADMIN.equals(type)) {
			loginType = ADMIN;
		} else if (EXPERT.equals(type)) {
			loginType = EXPERT;
		} else if (CLIENT.equals(type)) {
			loginType = CLIENT;
		} else {
			loginType = UNKNOWN;
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("using login type: " + loginType);
		}
		//LoginType must be ascertained before querying
		queryAuthentication(email, password);

		if (SUCCESS == outcome) {
			//Go here
			if (loginType.equals(ADMIN)) {
				outcome = SUCCESS;
			} else if (loginType.equals(EXPERT)) {
				
				ExpertAccount ea = ExpertAccount.getExpertAccount(uid);
				if (!ea.isVerified()) {
					outcome = USER_NOT_VERIFIED;
				} else {
					outcome = SUCCESS;
					if (doGenerateJnlp) {
						if (LOG.isDebugEnabled()) {
							LOG.debug("Writing expert specific jnlp file.");
						}
						JnlpGen.generateJnlp(uid, EXPERT);
						JnlpGen.generateHtml(uid, EXPERT);
					}
				}
			} else if (loginType.equals(CLIENT)) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Writing guest specific jnlp file.");
				}
				if (doGenerateJnlp) {
					//generate a jnlp with the expert uid as a parameter
					JnlpGen.generateJnlp(uid, CLIENT);
					JnlpGen.generateHtml(uid, CLIENT);
				}
			} else {
				outcome = UNKNOWN_LOGIN_TYPE;
			}
		}

		return outcome;
	}

}
