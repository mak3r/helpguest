package com.helpguest.data;

import java.io.Serializable;

public class RemoteLaunchOption implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int rowId = 0;
	
	private int expertId = 0;
	
	private String postURL = null;
	
	private String subText = null;
	
	private String refName = null;
	
	private boolean payLink = false;
	
	private float amount = 0;
	
	private int launchIconId = 0;
	
	private String remoteURLContent = null;
	
	private int subtextGroupId = 0;

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public int getExpertId() {
		return expertId;
	}

	public void setExpertId(int expertId) {
		this.expertId = expertId;
	}

	public String getPostURL() {
		return postURL;
	}

	public void setPostURL(String postURL) {
		this.postURL = postURL;
	}

	public String getSubText() {
		return subText;
	}

	public void setSubText(String subText) {
		this.subText = subText;
	}

	public String getRefName() {
		return refName;
	}

	public void setRefName(String refName) {
		this.refName = refName;
	}

	public boolean isPayLink() {
		return payLink;
	}

	public void setPayLink(boolean payLink) {
		this.payLink = payLink;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public int getLaunchIconId() {
		return launchIconId;
	}

	public void setLaunchIconId(int launchIconId) {
		this.launchIconId = launchIconId;
	}

	public String getRemoteURLContent() {
		return remoteURLContent;
	}

	public void setRemoteURLContent(String remoteURLContent) {
		this.remoteURLContent = remoteURLContent;
	}

	public int getSubtextGroupId() {
		return subtextGroupId;
	}

	public void setSubtextGroupId(int subtextGroupId) {
		this.subtextGroupId = subtextGroupId;
	}


}
