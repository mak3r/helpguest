/*
 * ResetCode.java
 *
 * Created on April 4, 2007, 11:19 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;




public class ResetCode implements ResultHandler {
    
    private static final Logger LOG = Logger.getLogger("com.helpguest.data.ResetCode");
    private static boolean INVALID_CODE = true;
    
    private static String SIGNUP_REPLY_TO_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.SIGNUP_REPLY_TO_PREFIX.getKey());
    private static String WEBAPP_NAME = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEBAPP_NAME.getKey());
    private static String GWT_MODULE_ENTRY = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.GWT_MODULE_ENTRY.getKey());
    private static final String FULLY_QUALIFIED_DOMAIN_NAME = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private static String smtpEmail = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.SMTP_AUTH_EMAIL.getKey());
    private static String smtpPassword = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.SMTP_AUTH_PASSWORD.getKey());
    
    String realSmtpEmail = "";
    String realSmtpPassword = "";
    
    private int queryId = 0;
    private String email;    
    private String code = null;
    private Timestamp validThru;
    
    
    /** Creates a new instance of ResetCode */
    private ResetCode() {
    }

    public static ResetCode getInstance(String code, String email) {
        ResetCode resetCode = new ResetCode();
        String query = "select email, code, valid_thru " +
                "from password_reset_code where code = ? " +
                "and email = ?";
        Object[] params = {code, email};
        DataBase.getInstance().submit(query, resetCode, params);
        
        if (INVALID_CODE || resetCode.isExpired()) {
            return null;
        }
        return resetCode;
    }
    
    public static ResetCode getInstance(String email) {
        ResetCode resetCode = new ResetCode();
        String query = "select email, code, valid_thru " +
                "from password_reset_code where email = ?";
        Object[] params = {email};
        DataBase.getInstance().submit(query, resetCode, params);
        
        if (resetCode.isExpired()) {
        	String delQuery = "delete from password_reset_code where email = '" + email + "'";
            DataBase.getInstance().submit(delQuery);
            DataBase.getInstance().submit(query, resetCode, params);
        }
       
        if (INVALID_CODE){
        	//prahalad adding if() 12/12/08
        	String resetEmail = email;
        	if (email.length()>12){
        		 resetEmail = email.substring(0,12);
        	}
        	String str = resetEmail + System.currentTimeMillis();
        	Timestamp validThru = new Timestamp(System.currentTimeMillis() + 259200000);
        	char[] chars = str.toCharArray();
            StringBuffer strBuffer = new StringBuffer();
            for (int i = 0; i < chars.length; i++) {
                    strBuffer.append(Integer.toHexString((int) chars[i]));
            }
            String inQuery = "insert into password_reset_code(email, code, valid_thru) " +
        	"values('" + email + "','" + strBuffer.toString() + "','" + validThru.toString() + "')";
            DataBase.getInstance().submit(inQuery);
            DataBase.getInstance().submit(query, resetCode, params);
        }
        
        return resetCode;
    }
    
    /**
     * Create a new ResetCode in the data store
     */
    public static ResetCode newInstance(String email) {
        //TODO implement a hex representation of the code
        // insert it all in the db
        // use email + current time to get the code.
    	
        
        
        //return strBuffer.toString();
        throw new UnsupportedOperationException();
    }

    public String getCode() {
        return code;
    }

    public String getEmail() {
        return email;
    }

    public void setValidThru(Timestamp validThru) {
        this.validThru = validThru;
    }

    public Timestamp getValidThru() {
        return validThru;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     * @return true if the the validThru timestamp
     * is before the current time - now().
     */
    private boolean isExpired() {
        Long now = System.currentTimeMillis();
        if (validThru == null || validThru.getTime() < now) {
            return true;
        }
        return false;
    }
    
    /**
     * Expire the reset code.
     * This method modifies the parameter passed in.
     * It is set to null. The parameter passed in can no longer be
     * used after this call.
     *
     * @param the ResetCode to expire
     * @return null
     */
    public static ResetCode expire(ResetCode resetCode) {
        String storedProc = "update password_reset_code set valid_thru = now() " +
                "where code = ?";
        //FIXME : Use the java timestamp not the db timestamp for consistency
        Object[] data = {resetCode.getCode()};
        DataBase.getInstance().submit(storedProc, data);
        resetCode = null;
        return resetCode;
    }
    
    /**
     * 
     * The implementing class should use this
     * call to extract the results from the queries
     * that are passed to com.helpguest.service.DataBase
     */
    public void capture(ResultSet rs, int queryId) throws SQLException {
        try {
            if (rs.next()) {
                //there should only be one result.
                this.email = rs.getString("email");
                this.code = rs.getString("code");
                this.validThru = rs.getTimestamp("valid_thru");
                INVALID_CODE = false;
            } else {
                INVALID_CODE = true;
            }
        } catch (SQLException sqlx) {
            LOG.error("could not capture account status info", sqlx);
            throw sqlx;
        }
    }

    /**
     * Concrete implementations should use this
     * to set a unique id when a single class the implements
     * this interfaces uses more than one query.
     */
    public void setQueryId(int id) {
    }

    /**
     * Concrete implementations should return the id
     * provided using the setQueryId(int) method.
     * This allows an individual class to implement
     * this interface and uniquely identify multiple
     * queries from that class.
     */
    public int getQueryId() {
        return queryId;
    }
    
    public void sendEmail(final String expertEmail){
		
		final Properties props = System.getProperties();
		props.put("mail.smtp.host", "smtp.helpguest.com");
        //required for smtp auth
        props.put("mail.smtp.auth", "true");
        
        try {
            
        	//get Cipher to decrypt smtp auth email, password 
        	HGCipher authCipher;
        	 
            byte[] random_number = { (byte) 0xc7, (byte) 0x73, (byte) 0x21, (byte) 0x8c,
               (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99 };

	       //encryption see enrcypted pass in log
            String pass1 = "password";
            authCipher = new HGCipher(pass1, random_number);
            String strEncryptedUser = smtpEmail;
            String[] arrEncryptedUser = strEncryptedUser.split(",");
      	    byte[] ourEncryptedData = new byte[arrEncryptedUser.length];
      
      	    for (int i=0;i<arrEncryptedUser.length;i++) { 
      	      //ourEncryptedData[i] = Byte.parseByte(arrEncryptedUser[i].trim());
      		  ourEncryptedData[i] = (byte)Integer.parseInt(arrEncryptedUser[i].trim());
      		
      	    }
            
      	    realSmtpEmail = authCipher.decrypt(ourEncryptedData);
            
             
             
      	    String strEncryptedPaswd = smtpPassword;
      	    String[] arrEncryptedPaswd = strEncryptedPaswd.split(",");
      	    byte[] ourEncryptedPaswd = new byte[arrEncryptedPaswd.length];
      
      	    for (int i=0;i<arrEncryptedPaswd.length;i++) { 
      		  ourEncryptedPaswd[i] = Byte.parseByte(arrEncryptedPaswd[i].trim());
      		
      	    }
            
      	    realSmtpPassword = authCipher.decrypt(ourEncryptedPaswd);
        	
             
             Session session = Session.getInstance(props, new SMTPAuthenticator());
            /**
             * use this to debug the smtp session
            session.setDebug(true);
            session.setDebugOut(new java.io.PrintStream(new java.io.File("/usr/share/tomcat/logs/MailOutDebug.txt")));
             */
            Message msg = new VerifyMessage(session);
            Address fromAddress = new InternetAddress(realSmtpEmail);
            Address toAddress = new InternetAddress(expertEmail);
            try {
                msg.setFrom(fromAddress);
            } catch (AddressException aex) {
            	LOG.error("from email address " + fromAddress + " is invalid. ", aex);
            }
            try {
                msg.addRecipient(Message.RecipientType.TO, toAddress);
            } catch (AddressException aex) {
            	LOG.error("email address provided " + expertEmail + " is invalid. ", aex);
            }

            msg.setSubject("Password Reset Code from HelpGuest");
             
            //String msgContent = content;
            //ExpertAccount ea = ExpertAccount.getExpertAccount(expertEmail);
            StringBuffer content = new StringBuffer();
            content.append("Your Reset code is: " + getCode() + "\r\n\r\n");
            content.append("Someone has requested a password reset for this email address.\r\n");
            content.append(expertEmail + "\r\n\r\n");
            content.append("If you are trying to reset yor password, please use the link below to visit the HelpGuest website and reset your password\r\n\r\n");
            content.append(SIGNUP_REPLY_TO_PREFIX + "/" + WEBAPP_NAME + "/" + GWT_MODULE_ENTRY + "#public_tabs+");
            content.append("tab=become_an_expert&reset_password=yes&reset_code=" + getCode());
            content.append("\r\n\r\n");
            content.append("-- \r\n");
            content.append("HelpGuest Marketplace Password Reset Department");
            
            try {
                msg.setDataHandler(new DataHandler(new ByteArrayDataSource(content.toString(), "text/plain")));
            } catch (IOException iox) {
            	LOG.error("Could not add message content. ", iox);
            }
            msg.setSentDate(new Date());
            Transport.send(msg);
            /**
             * this is to send mulitple messages
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.helpguest.com","verify@helpguest.com","V2r3fy");
            msg.saveChanges();
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
             */
            
        } catch (MessagingException mex) {
        	LOG.error("Could not send email to " + expertEmail + ".",  mex);
        } catch (Exception ex) {
        	LOG.error("Message did not go out successfully. ", ex);
            //outcome = UNKNOWN_ERROR;
        }
	}
	
    class VerifyMessage extends MimeMessage {
        
        public VerifyMessage(Session session) {
            super(session);
        }
        
        protected void updateHeaders() throws MessagingException {
            super.updateHeaders();
            setHeader("Message-ID", "email-verification@" + FULLY_QUALIFIED_DOMAIN_NAME);
        }
    }

    class SMTPAuthenticator extends Authenticator {
        protected PasswordAuthentication getPasswordAuthentication() {
            PasswordAuthentication smtpAuth = new PasswordAuthentication(realSmtpEmail, realSmtpPassword);
            return smtpAuth;
        }
        
    }
    
}
