/**
 * HelpGuest TechnoLOGies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.helpguest.gwt.search.OfferingsConstraints;


/**
 * @author mabrams
 *
 */
public class Expertise implements ResultHandler {

    private int queryId;
    
    private static final int CATEGORIES = 1;
    private static final int OFFERINGS = 2;
    private static final int TAGS = 3;
    private static final int UP_TAGS = 4;
    private static final int IN_TAGS = 5;
    private static final int CATOFFGS = 6;
    private static final int EXPERT_CATEGORIES = 7;
    private static final int  CATOFFGTAGS = 8;
   
    private List<String> expertiseList = null;
    
    private List<String> categoryList = null;
    
    private List<String> offeringList = null;
    
    private List<String> tagsList = null;
    
    private List<String> catOffgsList = null;
    
    private List<String> catOffgTagsList = null;
    
    private List<String> expertCategoryList = null;
    
    private static final Logger LOG = Logger.getLogger("com.helpguest.data.Expertise");

    
    public List getCommonRequests(final int limit) {
        String commonQuery = "select keyword, count(*) as cnt  from searched_words" +
                " where keyword not in (select keyword from ignore_list) " +
                " group by keyword order by cnt desc limit ?";
        Object[] data = {String.valueOf(limit)};
        DataBase.getInstance().submit(commonQuery, this, data);
        
        return expertiseList;
    }
    
    public List<String> getRecentRequests(final int limit) {
        String recentQuery = "select keyword from searched_words " +
                " where keyword not in (select keyword from ignore_list) " +
                "order by insert_date desc limit ?";
        Object[] data = {String.valueOf(limit)};
        DataBase.getInstance().submit(recentQuery, this, data);
        
        return expertiseList;
    }     
    
    public List getRandomRequests(final int limit) {
        String randomQuery = "select distinct keyword from searched_words " +
                " where keyword not in (select keyword from ignore_list) " +
                "order by rand() limit ?";
        Object[] data = {String.valueOf(limit)};
        DataBase.getInstance().submit(randomQuery, this, data);
        
        return expertiseList;
    }
    

    public List getExpertOfferings(final int limit) {
        String expertiseQuery = "select keyword, count(keyword) as cnt " +
        		"from service_offerings group by keyword order by cnt desc, " +
        		"is_primary desc limit ?";
        Object[] data = {String.valueOf(limit)};
        DataBase.getInstance().submit(expertiseQuery, this, data);
        
        return expertiseList;
    }
    
    //prahalad adding 07/09/08 to get categories
    public List getExpertCategories(){
    	
    	this.queryId = CATEGORIES;
    	String categoryQuery = "select distinct category from category_offerings";
    	DataBase.getInstance().submit(categoryQuery, this);
    	return categoryList;
    	 }
    
    /**
     * @author ying
     * */
    public List getExpertCategories(final String expert_id) {
    	this.queryId = EXPERT_CATEGORIES;
    	String expertCategoryQuery = "select distinct category from category_offerings where cat_off_id in " +
    			"(select distinct cat_off_id from expert_tags where expert_id = (select id from expert where uid = '" +
    			expert_id +
    			"'))";
    	DataBase.getInstance().submit(expertCategoryQuery, this);
    	return expertCategoryList;
    }

    //prahalad adding 07/09/08 to get offerings for a category
    public List getExpertOfferings(String category){
    	
    	this.queryId = OFFERINGS;
    	String offeringsQuery = "select distinct offering from category_offerings " + 
    	"where category = '" + category + "' order by offering";
    	DataBase.getInstance().submit(offeringsQuery, this);
    	return offeringList;
    	 }
    
  //prahalad adding 07/14/08 to get tags for a category, offering, expertid
    public List getExpertTags(String category, String offering, String expertId){
    	
    	this.queryId = TAGS;
    	String tagsQuery = "select distinct et.tag_name from category_offerings co, " +
    			"expert_tags et, expert e where  " +
    			"co.category = '" + category + "' and co.offering = '" + offering + "' and  " +
    			"co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.uid = '" + expertId  +
    			"' order by tag_name";
    	/*String tagsQuery = "select distinct co.category, co.offering, et.tag_name from category_offerings co, " +
		"expert_tags et, expert e where  " +
		"co.cat_off_id = et.cat_off_id and et.expert_id = e.id and e.uid = '" + expert_id  +
		"' order by co.category, co.offering, et.tag_name";*/
    			
    	DataBase.getInstance().submit(tagsQuery, this);
    	return tagsList;
    	 }
   
  
   
    //prahalad adding 07/14/08 to update tags for a category, offering, expertid
    public void updateExpertTags(String category, String offering, String expertId, String tag_content){
    	
    	//this.queryId = UP_TAGS;
    	String upTagsQuery = "update expert_tags set tag_name = '" + tag_content + "' " +
    			"where expert_id = (select id from expert where uid = '"+ expertId +"') and " +
    					"cat_off_id = (select cat_off_id from category_offerings " +
    			"where category = '"+ category +"' and offering = '"+ offering +"')" ;
    			
    	DataBase.getInstance().submit(upTagsQuery);
    	
    	 }
   
    //prahalad adding 07/14/08 to insert tags for a category, offering, expertid   
    public void insertExpertTags(String category, String offering, String expertId, String tag_content){
    	
    	//this.queryId = IN_TAGS;
    	String inTagsQuery = "insert into expert_tags(cat_off_id, tag_name, expert_id) " +
    			"values((select cat_off_id from category_offerings " +
    			"where category = '"+ category +"' and offering = '"+ offering +"'),'"+tag_content+"'," +
    					"(select id from expert where uid = '"+ expertId +"'))";
    			
    	DataBase.getInstance().submit(inTagsQuery);
    	
    	 }
    
  //prahalad adding 07/21/08 to delete tags for a category, offering, expertid 
    public void deleteExpertTags(String category, String offering, String expertId){
    	
    	String delTagsQuery = "delete from expert_tags " +
		"where cat_off_id = (select cat_off_id from category_offerings where " +
		"category = '" + category + "' and offering = '"+ offering + "') and expert_id = " +
				"(select id from expert where uid = '"+ expertId +"')";
		
    	DataBase.getInstance().submit(delTagsQuery);
    	
    }

    public List getCatOffPairs(){
    	
    	this.queryId = CATOFFGS;
    	String catOffgsQuery = "select distinct category, offering from category_offerings " +
		"order by category, offering";
    	
    	DataBase.getInstance().submit(catOffgsQuery, this);
    	return catOffgsList;
    }
    
    public List getCatOffTagTuples(String expertId){
    	
    	this.queryId = CATOFFGTAGS;
    	String catOffgTagsQuery = "select distinct co.category, co.offering, et.tag_name " +
    			"from category_offerings co, expert_tags et, expert e " +
    			"where co.cat_off_id = et.cat_off_id and et.expert_id = e.id " +
    			"and e.uid = '" + expertId + "' " +
    			"order by co.category, co.offering, et.tag_name";
    	
    	DataBase.getInstance().submit(catOffgTagsQuery, this);
    	return catOffgTagsList;
    }
    /**
	 * {@inheritDoc}
	 */
	public void capture(ResultSet rs, int queryId) throws SQLException {
		switch (queryId) {
		default: 
		if (rs == null) {
            LOG.error("No records found.");
        } else {
	    	expertiseList = new ArrayList<String>();
	        while (rs.next()) {
	        	expertiseList.add(rs.getString("keyword"));
	        }                
        }//end of else
		break;
		case CATEGORIES:
			if (rs == null) {
	            LOG.error("No records found.");
	        } else {
	        	categoryList = new ArrayList<String>();
		        while (rs.next()) {
		        	categoryList.add(rs.getString("category"));
		        }                
	        }//end of else
			break;
		case OFFERINGS:
			if (rs == null) {
	            LOG.error("No records found.");
	        } else {
	        	offeringList = new ArrayList<String>();
		        while (rs.next()) {
		        	offeringList.add(rs.getString("offering"));
		        }                
	        }//end of else
			break;
		case TAGS:
			if (rs == null) {
	            LOG.error("No records found.");
	        } else {
	        	tagsList = new ArrayList<String>();
		        while (rs.next()) {
		        	tagsList.add( rs.getString("tag_name"));
		        }                
	        }//end of else
			break;
		case CATOFFGS:
			if (rs == null) {
	            LOG.error("No records found.");
	        } else {
	        	catOffgsList = new ArrayList<String>();
		        while (rs.next()) {
		        	catOffgsList.add( rs.getString("category") + ":" + rs.getString("offering") + ":");
		        }                
	        }//end of else
			break;
		case CATOFFGTAGS:
			if (rs == null) {
	            LOG.error("No records found.");
	        } else {
	        	catOffgTagsList = new ArrayList<String>();
		        while (rs.next()) {
		        	catOffgTagsList.add( rs.getString("category") + ":" + rs.getString("offering") + ":" 
		        			+ rs.getString("tag_name"));
		        }                
	        }//end of else
			break;
		case EXPERT_CATEGORIES:
			if(rs == null) {
				LOG.error("No records found.");
			} else {
				expertCategoryList = new ArrayList<String>();
				while(rs.next()) {
					expertCategoryList.add(rs.getString("category"));
				}
			}
			break;
		}//end switch
		
	}//end capture

	/**
	 * {@inheritDoc}
	 */
	public int getQueryId() {
		return queryId;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setQueryId(int id) {
		this.queryId = id;
	}
}
