/*
 * SessionGroup.java
 *
 * Created on December 31, 2006, 1:02 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import com.helpguest.SessionAssistant;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.log4j.Logger;

public class SessionGroup implements ResultHandler {
    
    private Collection<Session> sessions = new ArrayList<Session>();
    private ExpertAccount ea;
    
    private final DecimalFormat df = new DecimalFormat("#,###,###.00");
    private static final Logger log = Logger.getLogger("com.helpguest.data.SessionGroup");
    
    private SessionGroup() {
        
    }
    
    /**
     * Get a new SessionGroup for this expert account.
     * @param ea is the Experts Account to get the session group for.
     * @param fetchAll if true, fetch all records for this account
     * where remote access was used
     * regardless of payment status.  If false, only fetch unpaid records
     * where remote access was used
     */
    public SessionGroup(ExpertAccount ea, boolean fetchAll) {
        this.ea = ea;
        String filter = "and s.session_group_id = 0 ";
        if (fetchAll) {
            filter = "";
        }
        String query = "select s.id as sid, s.expert_id, s.insert_date, " +
                "s.item_id, s.item_name, s.item_number, s.start_time, s.stop_time, " +
                "s.payment_requested, s.payment_sent,  s.session_group_id, " +
                "r.id as rid, r.min_rate, r.min_time, r.preferred_time " +
                "from session s, expert_rates r  " +
                "where s.expert_rates_id = r.id " + filter +
                "and s.expert_id = ? " +
                "order by s.insert_date ";
        Object[] data = {new Integer(ea.getId())};
        DataBase.getInstance().submit(query,this,data);
    }
    
    public SessionGroup(ExpertAccount ea, long sessionGroupId) {
        this.ea = ea;
        String query = "select s.id as sid, s.expert_id, s.insert_date, " +
                "s.item_id, s.item_name, s.start_time, s.stop_time, " +
                "s.payment_requested, s.payment_sent, s.session_group_id, " +
                "r.id as rid, r.min_rate, r.min_time, r.preferred_time " +
                "from session s, expert_rates r  " +
                "where s.expert_rates_id = r.id and s.session_group_id = ? " +
                "and s.expert_id = ?";
        Object[] data = {new Long(sessionGroupId), new Integer(ea.getId())};
        DataBase.getInstance().submit(query,this,data);
    }
    
    /**
     * Assign a payment requested date of now() and session group id
     * for all sessions in the group
     */
    public void requestPaymentOnGroup() {
        String sessionIds = getUnpaidCsvIdList();
        String query = "update session set " +
                "session_group_id = ?, " +
                "paypal_email = ?, " + 
                "payment_requested = now() " +
                "where id in (" + sessionIds + ")";
        Object[] data = {new Long(System.currentTimeMillis()), ea.getPaypalEmail()};
        DataBase.getInstance().submit(query,data);
    }
    
    /**
     * Mark all sessions in this session group as paid
     * Set the date that payment was sent for all
     * items in this session group if the date has not already been set
     */
    public void markPaid(Date paidDate) {
        String sessionIds = getUnpaidCsvIdList();
        String query = "update session set " +
                "payment_sent = ?, " +
                "where id in (" + sessionIds + ")";
        Object[] data = {paidDate};
        DataBase.getInstance().submit(query,data);        
    }

    private String getUnpaidCsvIdList() {
        StringBuffer sessionIds = new StringBuffer("");
        for(Session next: sessions) {
            if (next.getSessionGroupId()==0) {
                sessionIds.append(next.getId() + ", ");
            }
        }
        if (sessionIds.length()>0) {
            sessionIds.deleteCharAt(sessionIds.lastIndexOf(","));
        }
        return sessionIds.toString();
    }
    
    /** 
     * @return a comma separated list of session ids
     * primarily used for sql statements
     */
    protected String getCsvSessionIdList() {
        StringBuffer sessionIds = new StringBuffer("");
        for(Session next: sessions) {
            sessionIds.append("'" + next.getSessionId() + "', ");
        }
        if (sessionIds.length()>0) {
            sessionIds.deleteCharAt(sessionIds.lastIndexOf(","));
        }
        return sessionIds.toString();
    }
    
    /**
     * @return all the sessions in this session group
     */
    public Collection<Session> getSessions() {
        return sessions;
    }
    
    public float getAverageSessionTime() {
        float avgTime = 0.0f;
        int accumulatedTime = 0;
        for (Session next : sessions) {
            SessionAssistant sa = new SessionAssistant(next);
            accumulatedTime += sa.getDurationInMinutes();
        }
        avgTime = (float)accumulatedTime/(float)sessions.size();
        return avgTime;
    }
    
    public String getFormattedAverageSessionTime() {
        DecimalFormat dfPercent = new DecimalFormat("###.0");
        return dfPercent.format(getAverageSessionTime());
    }
        
    public float getSessionTotal() {
        float total = 0.0f;
        for (Session next : sessions) {
            SessionAssistant sa = new SessionAssistant(next);
            total += sa.getTotalValue();
        }
        return total;
    }

    public String getFormattedSessionTotal() {
        return df.format(getSessionTotal());
    }
    
    
    
    public void capture(ResultSet rs, int queryId) throws SQLException {
         try {
            while (rs.next()) {
		SGSession s = new SGSession();
                SGRates r = new SGRates(); 
                s.setId(rs.getInt("sid"));
                s.setSessionId(rs.getString("item_id"));
                s.setItemName(rs.getString("item_name"));
                s.setItemNumber(rs.getString("item_number"));
                s.setExpertId(rs.getInt("expert_id"));
                s.setStart(rs.getTimestamp("start_time"));
                s.setEnd(rs.getTimestamp("stop_time"));
                s.setInsertDate(rs.getDate("insert_date"));
                s.setPaymentRequestDate(rs.getDate("payment_requested"));
                s.setPaymentSentDate(rs.getDate("payment_sent"));
                s.setSessionGroupId(rs.getLong("session_group_id"));
                r.setId(rs.getInt("rid"));
                r.setMinRate(rs.getFloat("min_rate"));
                r.setMinFee(rs.getFloat("min_rate")*rs.getInt("min_time"));
                r.setMinTime(rs.getInt("min_time"));
                r.setPreferredTime(rs.getInt("preferred_time"));
                r.setExpertId(rs.getInt("expert_id"));
                s.setRates(r);
                if (s.getStart().getTime() < s.getEnd().getTime()) {
                    s.setClosed(true);
                    //only add closed sessions
                    sessions.add(s);
                }
            }
        } catch (SQLException sqlx) {
            //This error is thrown if there is a problem accessing the database
            log.error("Failed to capture data set.", sqlx);
        }
    }

    public void setQueryId(int id) {
        
    }

    public int getQueryId() {
        return -1;
    }

    
    
    protected class SGSession implements Session {
        
        private int id;
        private String itemId;
        private String itemName;
        private String itemNumber;
        private String email;
        private int expertId;
        private Rates rates;
        private Timestamp start = null;
        private Timestamp end = null;
        private Date insertDate = null;
        private Date paymentRequested = null;
        private Date paymentSent = null;
        private long sessionGroupId;
        private boolean closed = false;
    
        public SGSession() {            
        }
        
        public boolean isClosed() {
            return closed;
        }

        public void setClosed(boolean closed) {
            this.closed = closed;
        }
        
        public long getSessionGroupId() {
            return sessionGroupId;
        }
        
        public void setSessionGroupId(long sessionGroupId) {
            this.sessionGroupId = sessionGroupId;
        }
        
        public String getSessionId() {
            return itemId;
        }
        
        public void setSessionId(String sessionId) {
            this.itemId = sessionId;
        }

        public Rates getRates() {
            return rates;
        }

        public void setRates(Rates rates) {
            this.rates = rates;
        }
        
        public String getItemNumber() {
            return itemNumber;
        }
        
        public void setItemNumber(String itemNumber) {
            this.itemNumber = itemNumber;
        }

        public String getItemName() {
            return itemName;
        }
        
        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public int getId() {
            return id;
        }
        
        public void setId(int id) {
            this.id = id;
        }

        public int getExpertId() {
            return expertId;
        }
        
        public void setExpertId(int expertId) {
            this.expertId = expertId;
        }

        public Timestamp getStart() {
            return start;
        }
        
        public void setStart(Timestamp start) {
            this.start = start;
        }

        public Timestamp getEnd() {
            return end;
        }

        public void setEnd(Timestamp end) {
            this.end = end;
        }
        
        public Date getPaymentSentDate() {
            return paymentSent;
        }
        
        public void setPaymentSentDate(Date paymentSent) {
            this.paymentSent = paymentSent;
        }

        public Date getPaymentRequestDate() {
            return paymentRequested;
        }
        
        public void setPaymentRequestDate(Date paymentRequested) {
            this.paymentRequested = paymentRequested;
        }

        public Date getInsertDate() {
            return insertDate;
        }
        
        public void setInsertDate(Date insertDate) {
            this.insertDate = insertDate;
        }
     
        @Override
		public String toString() {
            StringBuffer buf = new StringBuffer("");
            buf.append("[");
            buf.append("id: " + id);
            buf.append(", expertId: " + expertId);
            buf.append(", start: " + start);
            buf.append(", end: " + end);
            buf.append(", itemId: " + itemId);
            buf.append(", itemName: " + itemName);
            buf.append(", itemNumber: " + itemNumber);
            buf.append(", insertDate: " + insertDate);
            buf.append(", sessionGroupId: " + sessionGroupId);
            buf.append(", rates: " + rates);
            buf.append("]");

            return buf.toString();            
        }
    }
   
    protected class SGRates implements Rates {
        
        private int prefTime;
        private int minTime;
        private float minRate;
        private float minFee;
        private int expertId;
        private int id;
        
        public SGRates() {
            
        }

        public int getPreferredTime() {
            return prefTime;
        }
        
        public void setPreferredTime(int prefTime) {
            this.prefTime = prefTime;
        }

        public int getMinTime() {
            return minTime;
        }
        
        public void setMinTime(int minTime) {
            this.minTime = minTime;
        }

        public float getMinRate() {
            return minRate;
        }
        
        public void setMinRate(float minRate) {
            this.minRate = minRate;
        }

        public float getMinFee() {
            return minFee;
        }
        
        public void setMinFee(float minFee) {
            this.minFee = minFee;
        }

        public int getId() {
            return id;
        }
        
        public void setId(int id) {
            this.id = id;
        }

        public String getFormattedMinRate() {
            return df.format(getMinRate());
        }

        public String getFormattedMinFee() {
            return df.format(getMinFee());
        }

        public String getFormattedDepositAmount() {
            return df.format(getDepositAmount());
        }

        public float getDepositAmount() {
            return getMinFee() * getPreferredTime();
        }
        
        public int getExpertId() {
            return expertId;
        }
        
        public void setExpertId(int expertId) {
            this.expertId = expertId;
        }
        
        @Override
		public String toString() {
            StringBuffer buf = new StringBuffer("");
            buf.append("[");
            buf.append("id: " + id);
            buf.append(", expertId: " + expertId);
            buf.append(", minRate: " + minRate);
            buf.append(", minTime: " + minTime);
            buf.append(", prefTime: " + prefTime);
            buf.append("]");

            return buf.toString();
        }

        
    }
    
}
