package com.helpguest.data;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class LaunchIcon implements ResultHandler {

	private static final Logger LOG = Logger.getLogger(LaunchIcon.class);
	//ImageIcon icon = null;
	Blob image = null;
	
	
	public Blob getImageBlob(int rowId) {
		String select = "select icon from launch_icon where id = ?";
		Object[] data = {String.valueOf(rowId)};
		DataBase.getInstance().submit(select, this, data);
		
		return image;
	}

	public void capture(ResultSet rs, int queryId) throws SQLException {
		//currently there is only one query
		try {
			// if record found process blob
			if (rs.next()) {
				image = rs.getBlob("icon");
				/*
				// get blob
				Blob image = rs.getBlob("icon");
				// setup the streams to process blob
				InputStream input = image.getBinaryStream();
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				// set read buffer size
				byte[] rb = new byte[1024];
				int ch = 0;
				// process blob
				while ((ch = input.read(rb)) != -1) {
					output.write(rb, 0, ch);
				}
				// transfer to byte buffer
				byte[] b = output.toByteArray();
				input.close();
				output.close();
				// load final buffer to image icon
				icon = new ImageIcon(b);
				*/
			} else {
				LOG.warn("No icon found.  Check that a valid row id was requested.");
			}
			/*
		} catch (IOException iox) {
			LOG.error("could not process result set.", iox);
			*/
		} catch (Exception iox) {
			LOG.error("could not process result set.", iox);
		}
			

	}

	public int getQueryId() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setQueryId(int id) {
		// TODO Auto-generated method stub

	}

}
