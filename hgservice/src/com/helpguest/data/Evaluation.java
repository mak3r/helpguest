/*
 * Evaluation.java
 *
 * Created on January 7, 2007, 5:41 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import org.apache.log4j.Logger;

public class Evaluation implements ResultHandler {
    
    private int queryId = 0;
    private static final int SATISFACTION = 0;
    private static final int RESOLVED = 1;
    private static final int REPEAT_CUSTOMERS = 2;
    private static final int POINTS = 3;
    
    private float satisfaction = 0.0f;
    private int satisfactionCount = 0;
    private int problemsSolved = 0;
    private int problemsSolvedCount = 0;
    private int repeatCustomers = 0;
    private int repeatCustomersCount = 0;
    private int pointCount = 0;
    private int possiblePoints = 0;

    private final DecimalFormat df = new DecimalFormat("#,###,###.00");
    private final DecimalFormat dfPercent = new DecimalFormat("###");
    private static final Logger log = Logger.getLogger("com.helpguest.data.Evaluation");
    
    /** Creates a new instance of Evaluation */
    public Evaluation(SessionGroup sg) {
        String sessionIds = sg.getCsvSessionIdList();

        if (sessionIds.length() > 0) {
	        queryId = SATISFACTION;        
	        String satisfactionQuery = "select count(*) as cnt, avg(overall_satisfaction) as os from survey_1 " +
	                "where session_id in (" + sessionIds + ") and " +
	                "overall_satisfaction != -1";
	        DataBase.getInstance().submit(satisfactionQuery,this);
	        
	        queryId = RESOLVED;
	        String resolvedQuery = "select problem_solved from survey_1 " +
	                "where session_id in (" + sessionIds + ") and " +
	                "problem_solved != -1"; 
	        DataBase.getInstance().submit(resolvedQuery,this);
	        
	        queryId = REPEAT_CUSTOMERS;
	        String repeatQuery = "select repeat_customer from survey_1 " +
	                "where session_id in (" + sessionIds + ") and " +
	                "repeat_customer != -1"; 
	        DataBase.getInstance().submit(repeatQuery,this);
	
	        queryId = POINTS;
	        String pointsQuery = "select recommend, professional_service as ps " +
	                "from survey_1 where session_id in (" + sessionIds + ") " +
	                "and (recommend != -1 or professional_service != -1)";
	        DataBase.getInstance().submit(pointsQuery,this);
        }
    }    
    
    public float getOverallSatisfaction() {
        //max amount is 4 minimum is 1
        return (satisfaction/4)*100.0f;
    }

    public String getFormattedOverallSatisfaction() {
        return (satisfaction == -1 ? "not enough data" : df.format(getOverallSatisfaction()));
    }
    
    public int getSatisfactionCount() {
    	return satisfactionCount;
    }
    
    public int getProblemsSolved() {
        return problemsSolved;
    }
    
    public int getProblemsSolvedCount() {
        return problemsSolvedCount;
    }
    
    public float getProblemsSolvedPercent() {
        return ((float)getProblemsSolved()/(float)getProblemsSolvedCount())*100;
    }

    public String getFormattedProblemsSolved() {
        String solved = "not yet evaluated";
        if (getProblemsSolvedCount() > 0) {
            solved = getProblemsSolved() + " of " + getProblemsSolvedCount() +
                    " (" + dfPercent.format(getProblemsSolvedPercent()) + "%)";
        }
        return solved;
    }
    
    public int getRepeatCustomers() {
        return repeatCustomers;
    }
    
    public int getRepeatCustomersCount() {
        return repeatCustomersCount;
    }
    
    public float getRepeatCustomerPercent() {
        return ((float)getRepeatCustomers()/(float)getRepeatCustomersCount())*100;
    }
    
    public String getFormattedRepeatCustomers() {
        String repeats = "no evaluations on repeats yet";
        if (getRepeatCustomersCount() > 0) {
            repeats = dfPercent.format(getRepeatCustomerPercent()) + "%";
        }
        return repeats;
    }
    
    public int getPointCount() {
        return pointCount;
    }
    
    public int getPossiblePointCount() {
        return possiblePoints;
    }
    
    public float getPointPercent() {
        return ((float)getPointCount()/(float)getPossiblePointCount())*100;
    }
    
    public String getFormattedPointPercent() {
        String points = "no evaluations for points yet";
            
        if (getPossiblePointCount() > 0) {
            points = getPointCount() + " of " + getPossiblePointCount() + " possible points (" + 
                dfPercent.format(getPointPercent()) + "% positive)";    
        }
        return points;
    }
    
    public void capture(ResultSet rs, int queryId) throws SQLException {
        try {
            switch (queryId) {
                case SATISFACTION:
                    if (rs.next()) {
                        satisfaction = rs.getFloat("os");
                        satisfactionCount = rs.getInt("cnt");
                    }
                    break;
                
                case RESOLVED:
                    while (rs.next()) {
                        //In Survey, yes = 1, 0 = no, 2 = I don't know
                        if (rs.getInt("problem_solved") == 1) {
                            problemsSolved++;
                        }
                        //increment the count for every problem evaluated
                        problemsSolvedCount++;
                    }
                    break;
                    
                case REPEAT_CUSTOMERS:
                    while (rs.next()) {
                        if (rs.getInt("repeat_customer") == 1) {
                            repeatCustomers++;
                        }
                        //increment the count for every problem evaluated
                        repeatCustomersCount++;
                    }
                    break;
                    
                case POINTS:
                    while (rs.next()) {
                        if (rs.getInt("recommend")==1) {
                            pointCount++;
                        }
                        if (rs.getInt("ps")==1) {
                            pointCount++;
                        }
                        if (rs.getInt("recommend")!=-1) {
                            possiblePoints++;
                        }
                        if (rs.getInt("ps")!=-1) {
                            possiblePoints++;
                        }
                    }
                    break;
                    
                default:
                    break;
            }
        } catch (SQLException sqlx) {
            //This error is thrown if there is a problem accessing the database
            log.error("Failed to capture data set.", sqlx);
        }
    }

    public void setQueryId(int id) {
        this.queryId = id;
    }

    public int getQueryId() {
        return queryId;
    }

    
}
