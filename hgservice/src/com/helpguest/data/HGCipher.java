package com.helpguest.data;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;



public class HGCipher {
	String key;
    Cipher Encrypter;
    Cipher Decrypter;
 
    public HGCipher(String inKey, byte[] salt) {
    
        // Declare encryption structures
        PBEKeySpec pbeKeySpec;
        PBEParameterSpec pbeParamSpec;
        
        SecretKeyFactory keyFac;
        int iterationCount = 55;
        
        
 
        // use the hashed encryption key as our "password"
        key = inKey;
 
        // Initialize the ciphers for encryption and decryption
        try {
            
        	Encrypter = Cipher.getInstance("PBEWithMD5AndDES");
            Decrypter = Cipher.getInstance("PBEWithMD5AndDES");
        	
            // Create PBE parameter set
            // PBE = hashing + symmetric encryption.  A 64 bit random
            // number (the salt) is added to the password and hashed
            // using a Message Digest Algorithm (MD5 in this example.).
            // The number of times the password is hashed is determined
            // by the iteration count.  Adding a random number and
            // hashing multiple times enlarges the key space.           
            // Initialize PBE Cipher with key and parameters
            pbeParamSpec = new javax.crypto.spec.PBEParameterSpec(salt, iterationCount);
            
            // Use PBEKeySpec to create a key based on a password.
            // The password is passed as a character array
            // Convert key SecretKey object
            pbeKeySpec = new javax.crypto.spec.PBEKeySpec(inKey.toCharArray());
            keyFac = javax.crypto.SecretKeyFactory.getInstance("PBEWithMD5AndDES");
            javax.crypto.SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);
         
            Encrypter.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);
            Decrypter.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec);
         }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
 
    public byte[] encrypt(String text) {
        try {
        	
        	return  Encrypter.doFinal(text.getBytes("UTF-8"));
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
 
    public String decrypt(byte[] data) {
        try {
            return new String(Decrypter.doFinal(data), "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
            //return null;
            return e.getMessage();
        }
    }

    public static void main(String[] args) {
    	if (args.length != 1) {
    		System.out.println("one argument is required.");
    		System.exit(1);
    	}
    	HGCipher authCipher;
 
        byte[] random_number = { (byte) 0xc7, (byte) 0x73, (byte) 0x21, (byte) 0x8c,
                (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99 };
 
       
        String pass1 = "password";
        String encryptionString = args[0];
        // Create authentication cipher with this key
        authCipher = new HGCipher(pass1, random_number);
 
        // Encrypt Encryption String using authentication cipher
        byte[] encryptedData = authCipher.encrypt(encryptionString);
 
        // Decrypt Encryption String using authentication cipher
        String decryptedString = authCipher.decrypt(encryptedData);
        //byte[] convertBytesAndBackAgain = (new String(encryptedData)).getBytes();
        System.out.println("each byte *********");
        for (int i = 0; i<encryptedData.length; i++) {
        	System.out.print(encryptedData[i] + ",");
        }
        System.out.println("\nplaintext: ["+encryptionString+"]");
        System.out.println("decrypted: ["+decryptedString+"]");
        
    		
    }
}

