/*
 * FileSystem.java
 *
 * Created on December 2, 2005, 12:59 PM
 */

package com.helpguest.data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.gwt.protocol.LoginType;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class FileSystem {
    
    private static final String CODE_BASE = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_ROOT.getKey()) +
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.JAWS_APPS_DIR.getKey()) + 
    	System.getProperty("file.separator");
    private static final Logger log = Logger.getLogger("com.helpguest.data.FileSystem");

    /** Creates a new instance of FileSystem */
    public FileSystem() {
    }
    
    /**
     * Write the content to this file.
     * @param file is the file to write to.
     * @param content is the content of the file.
     */
    public static void writeFile(File file, String content) {
	try {
            FileWriter fw = new FileWriter(file);
            fw.write(content);
            fw.flush();
            fw.close();
        } catch (IOException iox) {
		log.error("Error trying to write file " + file + ".", iox);
	}        
    }
    
    /*
     * Provide the jnlp filename and path part that will be generated 
     * with these parameters.
     * @return the jnlpfile with directory prefix (not full path
     * only codebase down).
     */
    public static String nameJnlpFile(LoginType loginType, String uid) {
        return loginType + "/" + uid + ".jnlp";
    }

    /**
     * Provide the html filename and path part that will be generated
     * with these parameters.
     * @return the htmlfile with directory prefix (full path is not
     * appended, only codebase down).
     */
    public static String nameHtmlFile(String loginType, String uid) {
        return loginType + "/" + uid + ".html";
    }
    
    /**
     * @return the fullpath to the file named
     */
    public static String fullPath(String fileName) {
        return CODE_BASE + fileName;
    }
    
    public static final String getCodeBase() {
        return CODE_BASE;
    }
}
