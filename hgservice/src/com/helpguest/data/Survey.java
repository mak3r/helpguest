/*
 * Survey.java
 *
 * Created on May 1, 2006, 6:01 PM
 * Copyright 2006 HelpGuest Technologies, Inc.
 */

package com.helpguest.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class Survey implements Serializable {

    private Map<Question,Value> answers = new HashMap();
    private String sessionId;
    static final long serialVersionUID = -3647261500460184983L;

    public static enum Value {
        ZERO,
        ONE,
        TWO,
        THREE,
        FOUR;
    }

    public static enum Question {
        OVERALL_SATISFACTION ("How satisfied were you with the overall HelpGuest experience?"),
        PROBLEM_SOLVED ("Did this Expert solve your problem?"),
        RECOMMEND ("Would you recommend this expert to others with a similar problem?"),
        REPEAT_CUSTOMER ("Have you worked with this Expert before (either with or without HelpGuest)?"),
        PROFESSIONAL_MANNER ("Was the service provided in a professional manner?");
        
        private String q;
        Question (String q) {
            this.q = q;
        }
        
        @Override
		public String toString() {
            return q;
        }
        
        public boolean equals(Question q) {
            if (q != this) {
                return false;
            }
            return true;
        }
    }
    
    
    /** Creates a new instance of Survey */
    public Survey(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setResponse(Question q, Value val) {
        answers.put(q,val);
    }

    /**
     * @return the value of the response to this question.
     * If the question is not answered (in the collection)
     * then -1 is returned.
     */
    public int getResponse(Question q) {
        if (answers.get(q) == null) {
            return -1;
        }
        return (answers.get(q)).ordinal();
    }
    
    public void persist(String guestUid) {
        HGSession session = HGSession.getInstance(sessionId);
        String storeQuery = "insert into survey_1 (overall_satisfaction, " +
                "problem_solved, recommend, repeat_customer, professional_service, " +
                "session_id, expert_id) select ?, ?, ?, ?, ?, ?, ? ";
        Object[] data = { 
            new Integer(getResponse(Survey.Question.OVERALL_SATISFACTION)),
            new Integer(getResponse(Survey.Question.PROBLEM_SOLVED)),
            new Integer(getResponse(Survey.Question.RECOMMEND)),
            new Integer(getResponse(Survey.Question.REPEAT_CUSTOMER)),
            new Integer(getResponse(Survey.Question.PROFESSIONAL_MANNER)),
            session.getItemId(),
            session.getExpertId()
        };
        DataBase.getInstance().submit(storeQuery,data);
    }
}
