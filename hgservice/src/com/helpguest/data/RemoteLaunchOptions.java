package com.helpguest.data;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.service.JnlpGen;
import com.helpguest.util.UIDGenerator;
/**
 * Copyright 2009, HelpGuest Technologies, Inc.
 * 
 * @author mabrams
 *
 */
public class RemoteLaunchOptions implements ResultHandler {

	private String remoteURLContent;
	private static final Logger LOG = Logger.getLogger(RemoteLaunchOptions.class);
	private List<RemoteLaunchOption> remoteLaunchOptionList = new ArrayList<RemoteLaunchOption>();
	private RemoteLaunchOption remoteLaunchOption;
	
	enum QueryId {
		GET_LAUNCH_OPTIONS,
		GET_REMOTE_URL_CONTENT,
		FIND_LAUNCH_OPTION;
	} 
	private int localQueryId;
	
	/**
	 * Get all the launch options for this expert id
	 * @param expertId
	 * @return a list of RemoteLaunchOption
	 */
	public List<RemoteLaunchOption> getLaunchOptions(final int expertId) {
		localQueryId = QueryId.GET_LAUNCH_OPTIONS.ordinal();
		String launchOptionsQuery = "select id, expert_id, post_url, subtext, ref_name, is_pay_link, " +
				"launch_icon_id, remote_url_content " +
				"from remote_launch_options where expert_id = ?";
		Object[] data = {String.valueOf(expertId)};
		DataBase.getInstance().submit(launchOptionsQuery, this, data);
		
		return remoteLaunchOptionList;
	}
	
	public String getRemoteURLContent() {
		String select = "select id, expert_id, post_url, remote_url_content from remote_launch_options";
		DataBase.getInstance().submit(select, this);
		
		//there should be some validation of the post_url here
		
		return remoteURLContent;
	}
	
	/**
	 * Create a remote launch option based on the template passed in.
	 * @param rlo
	 * @return the RemoteLaunchOption backed by the data store
	 */
	public RemoteLaunchOption createLaunchOption(final RemoteLaunchOption rlo) {
		if (rlo == null) {
			return rlo;
		}
		
		String uid = UIDGenerator.generate(rlo.getRefName());
		File loadFileJnlp = JnlpGen.generateJnlp(rlo.getRefName() + uid, new LoginType.Client());
		File loadFileHtml = JnlpGen.generateHtml(rlo.getRefName() + uid, new LoginType.Client());
		String remoteUrlContent = JnlpGen.generateRemoteLaunchRef(rlo);
		//If either one is null, we cannot continue
		if (loadFileHtml == null || loadFileJnlp == null) {
			LOG.error("failed to generate one of html or jnlp files");
			return null;
		}
		
		Boolean payLink = Boolean.FALSE;		
		//setup the amount flag
		if (rlo.getAmount() > 0) {
			payLink = Boolean.TRUE;
		}
		String createQuery = "insert into remote_launch_options " +
				"(expert_id, post_url, subtext, ref_name, is_pay_link, " +
				"rate, launch_icon_id, html_launch_content, jnlp_launch_content, remote_url_content, last_update_date) " +
				"values (?, ?, ?, ?, ?, ?, ?, LOAD_FILE(?), LOAD_FILE(?), ?, 'now()')";
		Object[] data = {
				rlo.getExpertId(), 
				rlo.getPostURL(), 
				rlo.getSubText(), 
				rlo.getRefName(),
				payLink,
				rlo.getAmount(),
				rlo.getLaunchIconId(),
				loadFileHtml.getAbsolutePath(),
				loadFileJnlp.getAbsolutePath(),
				remoteUrlContent
			};
		DataBase.getInstance().submit(createQuery, data);
		
		return findLaunchOption(rlo);
	}
	
	/**
	 * Find the launch option specified by this template
	 * @param rlo
	 * @return the launch option specified by this template or 
	 * if no values are set in the template, return null
	 */
	public RemoteLaunchOption findLaunchOption(final RemoteLaunchOption rlo) {
		if (rlo == null) {
			return rlo;
		}
		
		//assign class local to this value
		remoteLaunchOption = rlo;
		localQueryId = QueryId.FIND_LAUNCH_OPTION.ordinal();
		
		List<Object> whereClauseList = getWhereClause(rlo);
		if (whereClauseList == null) {
			return null;
		}
		String clause = null;
		if (whereClauseList.size() > 0) {
			clause = (String) whereClauseList.remove(0);
		}
		
		Object[] data = null;
		//Check for 0 size again because we've removed the clause if it existed.
		if (whereClauseList.size() > 0) {
			data = whereClauseList.toArray();
		}
		
		String createQuery = "select id, expert_id, post_url, subtext, ref_name, is_pay_link, rate, remote_url_content " +
				"from remote_launch_options " + clause;
		DataBase.getInstance().submit(createQuery, this, data);
		
		return remoteLaunchOption;
	}
	
	/**
	 * 
	 * @param rlo
	 * @return null if this RemoteLaunchOption has been deleted otherwise return
	 * the RemoteLaunchOption passed in.
	 */
	public RemoteLaunchOption deleteLaunchOption(final RemoteLaunchOption rlo) {
		List<Object> whereClauseList = getWhereClause(rlo);
		if (whereClauseList == null) {
			return null;
		}
		String clause = null;
		if (whereClauseList.size() > 0) {
			clause = (String) whereClauseList.remove(0);
		}
		
		Object[] data = null;
		//Check for 0 size again because we've removed the clause if it existed.
		if (whereClauseList.size() > 0) {
			data = whereClauseList.toArray();
		}
		if (data == null) {
			return rlo;
		}
		String deleteQuery = "delete from remote_launch_options " + clause;
		DataBase.getInstance().submit(deleteQuery, data);
		
		return null;
	}
	
	/**
	 * Create a where clause and an array of stored procedure query parameters
	 * based on the non null parameters of this RemoteLaunchOption.
	 * 
	 * The String clause passed in is modified to contain the where clause.
	 * 
	 * @param rlo
	 * @return A list where the first item in the list is the 
	 * where clause as a String value and the remaining items in the
	 * list are an array of paramters for the stored procedure query 
	 */
	private List<Object> getWhereClause(final RemoteLaunchOption rlo) {
		if (rlo == null) {
			return null;
		}
		List<Object> params = new ArrayList<Object>();
		StringBuffer buf = new StringBuffer(" where ");
		if (rlo.isPayLink()) {
			buf.append(" rate = ? and");
			params.add(new Float(rlo.getAmount()));
		}
		if (rlo.getExpertId() > 0) {
			buf.append(" expert_id = ? and");
			params.add(new Integer(rlo.getExpertId()));
		}
		if (rlo.getLaunchIconId() > 0) {
			buf.append(" launch_icon_id = ? and");
			params.add(new Integer(rlo.getLaunchIconId()));
		}
		if (rlo.getPostURL() != null) {
			buf.append(" post_url = ? and ");
			params.add(rlo.getPostURL());
		}
		if (rlo.getRefName() != null) {
			buf.append(" ref_name = ? and ");
			params.add(rlo.getRefName());
		}
		if (rlo.getRowId() > 0) {
			buf.append(" id = ? and ");
			params.add(new Integer(rlo.getRowId()));
		}
		if (rlo.getSubText() != null) {
			buf.append(" subtext = ? and");
			params.add(rlo.getSubText());
		}
		
		//remove the final 'and'
		buf.delete(buf.lastIndexOf("and"),buf.length());
		
		//set the first item in the list to the where buffer
		params.add(0, buf.toString());
		return params;
		
		
	}
	
	public void capture(ResultSet rs, int queryId) throws SQLException {
		QueryId currentId = QueryId.values()[queryId];
		switch (currentId) {
		case FIND_LAUNCH_OPTION:
			try {
				if (rs.next()) {
					remoteLaunchOption = new RemoteLaunchOption();
					
					remoteLaunchOption.setRowId(rs.getInt("id"));
					remoteLaunchOption.setExpertId(rs.getInt("expert_id"));
					remoteLaunchOption.setPostURL(rs.getString("post_url"));
					remoteLaunchOption.setSubText(rs.getString("subtext"));
					remoteLaunchOption.setRefName(rs.getString("ref_name"));
					remoteLaunchOption.setPayLink((rs.getInt("is_pay_link") == 0 ? false : true));
					remoteLaunchOption.setAmount(rs.getFloat("rate"));
					remoteLaunchOption.setRemoteURLContent(rs.getString("remote_url_content"));
				} 
			} catch (Exception iox) {
				LOG.error("could not process result set.", iox);
			}
			break;
		case GET_LAUNCH_OPTIONS:
			try {
				remoteLaunchOptionList.clear();
				while (rs.next()) {
					RemoteLaunchOption rlo = new RemoteLaunchOption();
					
					rlo.setRowId(rs.getInt("id"));
					rlo.setExpertId(rs.getInt("expert_id"));
					rlo.setPostURL(rs.getString("post_url"));
					rlo.setSubText(rs.getString("subtext"));
					rlo.setRefName(rs.getString("ref_name"));
					rlo.setPayLink((rs.getInt("is_pay_link") == 0 ? false : true));
					rlo.setLaunchIconId(rs.getInt("launch_icon_id"));
					rlo.setRemoteURLContent(rs.getString("remote_url_content"));
					
					remoteLaunchOptionList.add(rlo);
				} 
			} catch (Exception iox) {
				LOG.error("could not process result set.", iox);
			}
			break;
		case GET_REMOTE_URL_CONTENT:
			try {
				// if record found process blob
				if (rs.next()) {
					remoteURLContent = rs.getString("remote_url_content");
				} else {
					LOG.warn("No content found. ");
				}
			} catch (Exception iox) {
				LOG.error("could not process result set.", iox);
			}
			break;
		
		default:
			break;
		}

	}

	public int getQueryId() {
		// TODO Auto-generated method stub
		return localQueryId;
	}

	public void setQueryId(int id) {
		// TODO Auto-generated method stub
		localQueryId = id;
	}

}
