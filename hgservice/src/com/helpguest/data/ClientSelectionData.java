/*
 * ClientSelectionData.java
 *
 * Created on July 28, 2005, 8:54 AM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;

import com.helpguest.service.Service;
import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class ClientSelectionData implements ResultHandler {
    
    private int HOURS_PER_DAY = 24;
    private int MINUTES_PER_HOUR = 60;
    private int SECONDS_PER_MINUTE = 60;
    private int MILLISECONDS_PER_SECOND = 1000;

    public static final String WAITING = "WAITING";
    public static final String IN_PROGRESS = "IN PROGRESS";
    public static final String ABANDONED = "ABANDONED";
    
    public static final int NAME_COLUMN = 0;
    public static final int MESSAGE_COLUMN = 1;
    public static final int STATUS_COLUMN = 2;
    public static final int DURATION_COLUMN = 3;
    public static final int GUEST_UID_COLUMN = 4;
    
    Object[] columnNames = { "CLIENT", "HELP MESSAGE", "STATUS", "DURATION", "TICKET#" };
    Object[][] tableData = null;
    private final static int SETUP_TABLE = 0;
    private final static int LOAD_DATA = 1;
    private int queryId = SETUP_TABLE;
    private Map recordIdMap = null;
    private static final String dbServer = HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private static final String dbUser = HGServiceProperties.getInstance().getProperty(HGServiceProperty.DESKTOP_DB_USER_NAME.getKey());
    private static final String dbPass = HGServiceProperties.getInstance().getProperty(HGServiceProperty.DESKTOP_DB_PASSWORD.getKey());
        
    private static final Logger log = Logger.getLogger("com.helpguest.data.ClientSelectionData");
    private JTable clientList;
    
    /** Creates a new instance of ClientSelectionData */
    public ClientSelectionData() {
    }
    
    public DefaultTableModel getTableModel(String constraintUid) {
    	//reset table data
    	tableData = null;
        String query = "select guest_uid from protocol_request where expert_uid  = '" + 
                constraintUid + "'";
        queryId = SETUP_TABLE;
        //this will kick off the capture method.
        
        DataBase.getInstance(dbServer, dbUser, dbPass).submit(query,this);
   	  	
   	
        return new DefaultTableModel(tableData, columnNames);
    }

    public void capture(java.sql.ResultSet rs, int param) throws java.sql.SQLException {
        switch (param) {
            case SETUP_TABLE: {
                rs.last();
                log.debug("last row is: " + rs.getRow() + " / column count: " + columnNames.length);
                tableData = new Object[rs.getRow()][columnNames.length];
                break;
            }
            case LOAD_DATA: {
                int row = 0;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                JComboBox types = null;
                String previousUid = null;
                if (log.isDebugEnabled()) {
                    java.sql.ResultSetMetaData rsmd = rs.getMetaData();
                    log.debug( "column count: " + rsmd.getColumnCount());
                }
                recordIdMap = new HashMap();
                //recreate the array each time
                rs.last();
                tableData = new Object[rs.getRow()][columnNames.length];
                rs.beforeFirst();
                while(rs.next()) {
                    if (log.isDebugEnabled()) { log.debug("rs has another row: "); }
                    String uname = rs.getString("uname");
                    String message = rs.getString("message");
                    String uid = rs.getString("guest_uid");
                    String status = rs.getString("status");
                    int id = rs.getInt("id");
                    Date dt = null;                    
                    try {
                        dt = sdf.parse(rs.getString("request_ts"));
                    } catch (ParseException px) {
                        //if we cant use the insert date, use the current date
                        dt = new Date(System.currentTimeMillis());
                    }
                    long cmillis = System.currentTimeMillis();
                    long startMillis = dt.getTime();
                    String waiting = waitTime(cmillis - startMillis);
                    tableData[row][NAME_COLUMN] = uname; 
                    tableData[row][MESSAGE_COLUMN] = message;
                    tableData[row][DURATION_COLUMN] = waiting;
                    tableData[row][STATUS_COLUMN] = status;
                    tableData[row][GUEST_UID_COLUMN] = uid;                        
                    if (log.isDebugEnabled()) { log.debug("table data for row " + row + ": " + tableData[row][0] + "," +
                                                           tableData[row][1]); }
                    recordIdMap.put(row, id);
                    if (log.isDebugEnabled()) {log.debug("added row to recordIdMap.");}
                    row++;
                }
                clientList.setModel(new DefaultTableModel(tableData,columnNames));
                break;
            }
        }
    }

    public void loadData(JTable clientList, String constraintUid) {
        this.clientList = clientList;
        /*
        String query = "select id, status, guest_uid, uname, message, request_ts from protocol_request " + 
                "where expert_uid  = '" + constraintUid + "'";
                */
        //Pick up the records to show the expert but ignore the web chat type
        // records as those one is expert private.
        String query = "select id, status, guest_uid, uname, message, request_ts from protocol_request " + 
        "where expert_uid  = ? and type not in (?)";
        Object[] data = {constraintUid, Service.WEB_CHAT.toString()};
        queryId = LOAD_DATA;
        DataBase.getInstance(dbServer, dbUser, dbPass).submit(query, this, data);
    }
    
    public void markActive(int id) {
        String query = "update protocol_request set status = '" + IN_PROGRESS +
                "' where id = '" + id + "'";
        DataBase.getInstance(dbServer, dbUser, dbPass).submit(query);
    }
    
    /**
     * Get a statement regarding how long the guest has been waiting
     * @return the time waiting as a string
     */
    public String waitTime(long millisWaiting) {
        int togo;
        //Seconds to go
        togo = (int)(millisWaiting / MILLISECONDS_PER_SECOND);
        //minutes to go
        togo = (togo / SECONDS_PER_MINUTE);

        int minutes = (togo % MINUTES_PER_HOUR);
        //hours to go
        togo = (togo / MINUTES_PER_HOUR);

        int hours = (togo % HOURS_PER_DAY);
        int days = (togo / HOURS_PER_DAY);

        //We only want to display hours and minutes remaining.
        // so make the days back into hours
        hours += (days * HOURS_PER_DAY);
        
        String hourString = (hours == 1 ? "1 hour " : (hours == 0 ? "" : hours + " hours "));
        String minuteString = (minutes == 1 ? "1 minute " : (minutes == 0 ? "" : minutes + " minutes "));
        String waiting = null;
        waiting = ((hours == 0 && minutes == 0) ? "Less than a minute" : hourString + minuteString);
        
        return waiting; 
    }

    /**
     * Get the database record id for this row item.
     * It is up to the client to manage the rows and data
     * if rows are deleted then it may be necessary to loadData()
     * again or keep a client mapping of rows to ids.
     */
    public int getId(int row) {
        int id = -1;
        try {
            id = ((Integer)recordIdMap.get(row)).intValue();
        } catch (Exception ex) {
            log.error("Could not map row '" + row + "' to an id.", ex);
        }
        return id;
    }

    public int getQueryId() {
        return queryId;
    }    

    public void setQueryId(int param) {
        this.queryId = param;
    }        
    
}
