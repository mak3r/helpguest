/*
 * ClientDataSet.java
 *
 * Created on January 5, 2005, 9:37 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 */

package com.helpguest.data;

import com.helpguest.HelpGuestException;
import com.helpguest.service.HGPort;
import com.helpguest.service.Service;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author  mabrams
 */
public class ClientDataSet implements ResultHandler {


	private Map protocolRequests = new HashMap();
	private String uname = null;
	private String message = null;
	private String guestUid = null;
	private String expertUid = null;
	private long keyId;

	private static final int DATA_FOR_ID = 1;
	private static final int EXPERT_UID = 2;
	private int queryId = 0;
	private Map portList = null;
	private InetAddress inetAddress = null;

	private static final Logger log = Logger.getLogger("com.helpguest.data.ClientDataSet");

	/** Creates a new instance of ClientDataSet */
	private ClientDataSet() {
	}

	public ClientDataSet(InetAddress inetAddress) {
		this();
		this.inetAddress = inetAddress;
	}

	public void bind() {
		//move the data to the archives and 
		//bind the port
	}

	public void release() {
		//unbind the port
	}

	public void insert() throws HelpGuestException {

		Iterator it = protocolRequests.keySet().iterator();
		if (log.isDebugEnabled()) { log.debug("Found '" + protocolRequests.keySet().size() + "' services to insert."); }
		expertUid = getExpertUid(guestUid);
		keyId = System.currentTimeMillis();
		boolean firstService = true;
		while (it.hasNext()) {
			HGPort hgPort = (HGPort) it.next();
			ProtocolRequest pRequest = (ProtocolRequest) protocolRequests.get(hgPort);
			if (pRequest == null || pRequest.getType() == null) {
				throw new HelpGuestException("ProtocolRequest.type value was null for user: " + pRequest.getUname());
			}
			if (log.isDebugEnabled()) {log.debug("found type: " + pRequest.getType());}
			//Only insert one request for all types
			if (firstService) {
				String pRequestQuery = "insert into protocol_request (" + 
				"status, type, uname, message, guest_uid, expert_uid, key_id) values ('" +
				ClientSelectionData.WAITING + "', '" +
				pRequest.getType() + "', '" + 
				pRequest.getUname() + "', '" +
				pRequest.getMessage() + "', '" + 
				guestUid + "', '" +
				expertUid + "', " +
				keyId + ")";
				//no query id is set since this is an insert, there is nothing to do.
				DataBase.getInstance().submit(pRequestQuery);
				firstService = false;
			}

			if (log.isDebugEnabled()) {log.debug("preparing to insert into protocol_response.");}
			//Now insert the response
			String pResponseQuery = "insert into protocol_response (" +
			"type, port, uid, inet_address, key_id) values ('" +
			pRequest.getType() + "', '" +
			hgPort.getPortNumber() + "', '" +
			guestUid + "', '" +
			inetAddress.getHostAddress() + "', " +
			keyId + ")";
			DataBase.getInstance().submit(pResponseQuery);
			if (log.isDebugEnabled()) {log.debug("submitted protocol_resonse query sucessfully.");}
		}
	} 

	public boolean isComplete() {
		Iterator it = protocolRequests.values().iterator();
		boolean complete = true;
		while (it.hasNext() && complete) {
			ProtocolRequest pRequest = (ProtocolRequest)it.next();
			//make sure data is validated here
			if (pRequest.getType() != null &&
					pRequest.getUname() != null) {
				complete |= complete;
			}
		}
		return complete;
	}

	private void update() {

	}

	public ProtocolRequest getProtocolRequestForType(Service service, String sPort) {
		int port = -1;
		try {
			port = Integer.parseInt(sPort);
		} catch (NumberFormatException nfx) {
			log.error("Port '" + sPort + "' is not a number.  Setting port value to -1", nfx);
		}

		ProtocolRequest pRequest = (ProtocolRequest)protocolRequests.get(service.getName());
		if (pRequest == null) {
			pRequest = new ProtocolRequest();
		}
		pRequest.setUname(uname);
		pRequest.setMessage(message);
		pRequest.setType(service.getName());
		protocolRequests.put(new HGPort(port, service),pRequest);
		return pRequest;
	}

	public Map getDataForId(String guestUid) {
		queryId = DATA_FOR_ID;
		String query = "select type, port, inet_address from protocol_response where uid = '" + guestUid + "'";
		DataBase.getInstance().submit(query,this);
		return portList;
	}

	public String getExpertUid(String guestUid) {
		Object[] data = {guestUid};
		//if the expertUid is not null, we've already hit the db for it.
		if (expertUid == null) {
			queryId = EXPERT_UID;
			String query = "select e.uid as uid from expert e, session s " +
			"where s.expert_id = e.id and s.item_id = ?";
			DataBase.getInstance().submit(query,this,data);
			if (expertUid == null) {
				//try and get the expert uid the old fashioned way
				//this is legacy for version 1 interface
				query = "select e.uid as uid from expert e, guest g " + 
				"where g.expert_id = e.id and g.uid = ?";
				DataBase.getInstance().submit(query, this,data);
			}
		}
		return expertUid;
	}

	public void capture(java.sql.ResultSet rs, int queryId) throws java.sql.SQLException {
		if (queryId == DATA_FOR_ID) {
			portList = new HashMap();
			while(rs.next()) {
				String type = rs.getString("type");
				portList.put(type, new HGPort(rs.getInt("port"), Service.getService(type)));
				try {
					this.inetAddress = InetAddress.getByName(rs.getString("inet_address"));
				} catch (UnknownHostException uhx) {
					log.error("Network address is invalid. Using loopback address", uhx);
					try {
						this.inetAddress = InetAddress.getByName(null);
					} catch (UnknownHostException uhx2) {
						log.error("Unable to set inetAddress.", uhx2);
					}
				}
			}
		} else if (queryId == EXPERT_UID) {
			if ( rs!= null) {
				if (rs.next()) {
					expertUid = rs.getString("uid");
				}
			}
			if (log.isDebugEnabled()) {log.debug("expert uid is now: " + expertUid);}
		}
	}

	public int getQueryId() {
		return queryId;
	}

	public void setQueryId(int id) {
	}

	/**
	 * Getter for property message.
	 * @return Value of property message.
	 */
	public java.lang.String getMessage() {
		return message;
	}

	/**
	 * Setter for property message.
	 * @param message New value of property message.
	 */
	public void setMessage(java.lang.String message) {
		this.message = message;
	}

	/**
	 * Getter for property uname.
	 * @return Value of property uname.
	 */
	public java.lang.String getUname() {
		return uname;
	}

	/**
	 * Setter for property uname.
	 * @param uname New value of property uname.
	 */
	public void setUname(java.lang.String uname) {
		this.uname = uname;
	}

	/**
	 * Getter for property inetAddress.
	 * @return Value of property inetAddress.
	 */
	public InetAddress getInetAddress() {
		return inetAddress;
	} 

	public void setUid(String guestUid) {
		this.guestUid = guestUid;
	}

	public String getUid() {
		return guestUid;
	}
}
