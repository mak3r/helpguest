/*
 * Session.java
 *
 * Created on December 31, 2006, 1:03 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.helpguest.data;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author mabrams
 */
public interface Session {
     
    /**
     * @return true if the session is closed
     */
    public boolean isClosed();
    
    public int getId ();
    
    public String getSessionId();
    
    public String getItemName();
    
    public String getItemNumber();

    public int getExpertId();
    
    public Rates getRates();
    
    public Timestamp getStart();
    
    public Timestamp getEnd();
    
    public Date getInsertDate();
    
    public Date getPaymentRequestDate();
    
    public Date getPaymentSentDate();
    
    public long getSessionGroupId();

}
