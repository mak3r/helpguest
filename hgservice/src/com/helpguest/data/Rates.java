/*
 * Rates.java
 *
 * Created on January 1, 2007, 12:37 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.helpguest.data;

/**
 *
 * @author mabrams
 */
public interface Rates {
    
    public int getId();
    
    public int getExpertId();
    
    public float getMinRate();
    
    public String getFormattedMinRate();
    
    public int getMinTime();
    
    public int getPreferredTime();
    
    public float getDepositAmount();
    
    public String getFormattedDepositAmount();
    
    public float getMinFee();
    
    public String getFormattedMinFee();

}
