/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Oct 8, 2007
 */
package com.helpguest.data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mabrams
 *
 */
public class DemoSession {

	private static final Map<String, String> ipnMap = new HashMap<String, String>();
	static {
		ipnMap.put(IPNData.PAYMENT_STATUS, IPNData.COMPLETED);
		ipnMap.put(IPNData.MC_CURRENCY, "USD");
		ipnMap.put(IPNData.TAX, "0");
	}
	
	public static Session createDemoSession(final ExpertAccount ea) {		
		Rates zeroRate = HGRates.getDemoInstance(ea);
		Session freeBsession = HGSession.newInstance(ea, zeroRate, true);
		updateIPNMap(ea, freeBsession);
		IPNData.createInstance(ipnMap);
				
		return freeBsession;
	}
	
	private static Map<String, String> updateIPNMap(final ExpertAccount ea, final Session session) {
		String time = String.valueOf(System.currentTimeMillis());
		ipnMap.put(IPNData.CUSTOM, ea.getUid());
		ipnMap.put(IPNData.INVOICE, session.getSessionId());
		ipnMap.put(IPNData.EXPERT_ID, String.valueOf(session.getExpertId()));
		ipnMap.put(IPNData.ITEM_NAME, session.getItemName());
		ipnMap.put(IPNData.ITEM_NUMBER, session.getItemNumber());
		ipnMap.put(IPNData.MC_GROSS, session.getRates().getFormattedDepositAmount());
		ipnMap.put(IPNData.TXN_ID, time);
		ipnMap.put(IPNData.VERIFY_SIGN, time + "." + ea.getUid() + "." + ea.getHandle());
		
		return ipnMap;
	}

}
