/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 15, 2007
 */
package com.helpguest.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.log4j.Logger;

import com.helpguest.protocol.Lexicon.Common;

/**
 * @author mabrams
 *
 */
public class ProtocolResponse implements ResultHandler {


	private int id;
	private String type;
	private int port;
	private String uid;
	private String inetAddress;
	private Timestamp responseTS;
	private long keyId;
	private boolean recordExists = false;
	
	public static final String WEB_CHAT = Common.COM_WEB_CHAT.toString();
	public static final String VOIP = Common.COM_VOIP.toString();
	public static final String CHAT = Common.COM_CHAT.toString();
	public static final String AGENT = Common.COM_AGENT.toString();
	public static final String VNC = Common.COM_VNC.toString();
	
	private static final Logger LOG = Logger.getLogger(ProtocolResponse.class);
	
	/**
	 * private ctor.
	 */
	private ProtocolResponse() {
		
	}
	
	/**
	 * @param guestUid String
	 * @return null if there is no record with this guest uid for the type specified.
	 * Otherwise, the most recent record matching this uid and type.
	 */
	public static ProtocolResponse getProtocolResponse(final String guestUid, final String type) {
		ProtocolResponse response = new ProtocolResponse();
		String query = "select * from protocol_response " +
				"where uid = ? and type = ? " +
				"order by response_ts desc " +
				"limit 1";
		Object[] data = {guestUid, type};
		DataBase.getInstance().submit(query, response, data);
		
		if (response.recordExists) {
			return response;
		}
		return null;
	}
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the inetAddress
	 */
	public String getInetAddress() {
		return inetAddress;
	}

	/**
	 * @param inetAddress the inetAddress to set
	 */
	public void setInetAddress(String inetAddress) {
		this.inetAddress = inetAddress;
	}

	/**
	 * @return the keyId
	 */
	public long getKeyId() {
		return keyId;
	}

	/**
	 * @param keyId the keyId to set
	 */
	public void setKeyId(long keyId) {
		this.keyId = keyId;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the responseTS
	 */
	public Timestamp getResponseTS() {
		return responseTS;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * @param uid the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * {@inheritDoc}
	 */
	public void capture(ResultSet rs, int queryId) throws SQLException {
		if (rs != null) {
			try {
				if (rs.next()) {
					recordExists = true;
					id = rs.getInt("id");
					type = rs.getString("type");
					port = rs.getInt("port");
					uid = rs.getString("uid");
					inetAddress = rs.getString("inet_address");
					responseTS = rs.getTimestamp("response_ts");
					keyId = rs.getLong("key_id");
				} else {
					recordExists = false;
				}
			} catch (SQLException sqlx) {
				LOG.error("Could not capture record.", sqlx);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public int getQueryId() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setQueryId(int id) {
		// TODO Auto-generated method stub

	}

}
