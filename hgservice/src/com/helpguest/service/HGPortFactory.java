/*
 * PortFactory.java
 *
 * This class is designed to randomly pull a port number
 * from the database table service_ports based on
 * what ports are not in use, what the starting port
 * is marked at and the what the maximum number of ports
 * allowed are.
 *
 * Created on January 2, 2005, 8:07 PM
 * Copyright 2005 - HelpGuest Technologies, Inc. 
 */

package com.helpguest.service;

import com.helpguest.data.ResultHandler;
import com.helpguest.data.DataBase;
import com.helpguest.protocol.Vocabulary;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author  mabrams
 */
public class HGPortFactory implements ResultHandler{
    
    private final int LOAD_SERVICES = 0;
    private final int INITIALIZE_PORTS = 1;
    private int currentQuery = -1;
    private static final String SERVICE_UNAVAILABLE = "SERVICE_UNAVAILABLE";
    
    private static HGPortFactory portFactory = null;
    private static Collection services = new ArrayList();
    private static Map available = new HashMap();
    private static Map inuse = new HashMap();
    Logger log = Logger.getLogger(HGPortFactory.class);
    
    /** Creates a new instance of PortFactory */
    private HGPortFactory() {
        loadServices();
        validateServices();
        initializePorts();
    }
    
    /**
     * Load the service names that are known.
     */
    private void loadServices() {
        setQueryId(LOAD_SERVICES);
        String query = "select distinct service from service_ports";
        DataBase.getInstance().submit(query, this);
        if (log.isDebugEnabled()) {log.debug("services:" + services);}
    }
    
    /**
     * Make sure that all services in the Service class
     * are loaded.  If the two lists do not match exactly,
     * then exit.
     */
    private void validateServices() {
        Collection javaList = Service.getServiceList();
        Iterator it = javaList.iterator();
        while (it.hasNext()) {
            String nextService = ((Service)it.next()).getName();
            if (services.size() == Service.getServiceList().size()) {
                if (!services.contains(nextService)) {
                    log.fatal("mismatched services:" + nextService);
                    System.exit(1);
                } 
            } else {
                log.fatal("different number of services in db and code." +
                          " code: " + Service.getServiceList() + " db: " + services);
                System.exit(1);
            }
        }
    }
    
    /**
     * Initialize all the ports for the known services
     * to available.  First we bind all the ports.
     * then we set the availability to available
     */
    private void initializePorts() {
        setQueryId(INITIALIZE_PORTS);                
        String query = "select service, port_start, max_ports from service_ports";
        DataBase.getInstance().submit(query, this);
    }
    
    /**
     * This method returns the next available port for the 
     * specified service
     * @param service is the name of the service that a port is needed for
     * @return the string value of the port to use for this service 
     * or SERVICE_UNAVAILABLE if no port is available
     */
    public static String getPortAsString(Service service) {
        HGPort port = getPort(service);
        if (port.getPortNumber() != 0) {
            return String.valueOf(port.getPortNumber());
        }
        
        //no ports were found.
        // Try to do some syncing of db and java before returning nothing.
        return Vocabulary.SERV_SERVICE_UNAVAILABLE;
    }
    
    /**
     * This method returns the port object for the next
     * available port for this service.  If no port object is
     * available, then a port with a port value of 0 
     * is returned.
     */
    public static HGPort getPort(Service service) {
        if (portFactory == null) {
            portFactory = new HGPortFactory();
        }
        Iterator it = available.values().iterator();
        while (it.hasNext()) {
            HGPort port = (HGPort)it.next();
            if (port.isReservedFor(service.getName())) {
                Integer portNumber = new Integer(port.getPortNumber());
                inuse.put(portNumber, port);
                available.remove(portNumber);
                //TODO:un-bind the port now

                //return the port to use
                return port;
            }
        }
        
        //no ports were found.
        // Try to do some syncing of db and java before returning nothing.
        return new HGPort(0, service);        
    }

    /**
     * return a port to the factory.
     * make the port available and bind the port
     * @return true if the port was successfully restored
     * false if it was not
     */
    public static boolean restore(int portNumber) {
        if (portFactory == null) {
            portFactory = new HGPortFactory();
        }
        Integer num = new Integer(portNumber);
        HGPort port = (HGPort)inuse.remove(num);
        if (port != null) {
            //TODO: bind the port
            // If bind succeeds only then can it be added to the available list.
            available.put(num, port);

            //return success
            return true;
        }
        return false;
    }
    
    public void capture(java.sql.ResultSet rs, int queryId) throws SQLException {
        if (queryId == LOAD_SERVICES) {
            while(rs.next()) {
                services.add(rs.getString("service"));
            }
        } else if (queryId == INITIALIZE_PORTS) {
            while(rs.next()) {
                for (int i = 0; i < rs.getInt("max_ports"); i++) {
                    //TODO: now bind the port so it doesn't get used by 
                    // and external app.
                    // If bind succeeds only then can it be added to the available list.
                    int port = rs.getInt("port_start") + i;
                    available.put(new Integer(port),
                                  new HGPort(port, Service.getService(rs.getString("service"))));
                }
            }
        } else {
            
        }
    }
    
    public int getQueryId() {
        return currentQuery;
    }
    
    public void setQueryId(int id) {
        currentQuery = id;
    }
    
    @Override
	public String toString() {
        return ("[PortFactory, " + currentQuery + "]");
    }

}
