/*
 * Port.java
 *
 * Created on January 3, 2005, 5:34 PM
 * Copyright 2005 HelpGuest Technologies, Inc.
 */

package com.helpguest.service;

/**
 *
 * @author  mabrams
 */

public class HGPort implements Comparable{

    public static final int PORT_MATCHES = 1;
    public static final int SERVICE_MATCHES = 2;
    public static final int COMPLETE_MATCH = 0;
    public static final int NO_MATCH = -1;
    
    private int number;
    private Service service;
    
    public  HGPort(int number, Service service) {
        this.number = number;
        this.service = service;
    }

    public boolean isReservedFor(String service) {
        if (this.service.toString().equals(service)) {
            return true;
        }
        return false;
    }

    public int getPortNumber() {
        return number;
    }
    
    public int updatePort(int newPort) {
        this.number = newPort;
        return this.number;
    }
    
    public Service getService() {
        return service;
    }
    
    public boolean equals(HGPort p) {
        if ((p.number == this.number) && (p.service.equals(this.service))) {
            return true;
        }
        return false;
    }

    @Override
	public String toString() {
        return "[" + number + ", " + service + "]";
    }
    
    public int compareTo(Object o) {
        if (o instanceof HGPort) {        
            HGPort port = (HGPort)o;
            if (port.number == this.number) {
                if (port.service.equals(this.service)) {
                    return COMPLETE_MATCH;
                } else {
                    return PORT_MATCHES;
                }
            } else if (port.service.equals(this.service)) {
                return SERVICE_MATCHES;
            }
           
        } 
        return NO_MATCH;
            
    }
    
}