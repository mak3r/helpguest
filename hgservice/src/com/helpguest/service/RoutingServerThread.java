/**
 * HelpGuest Technologies, Inc. Copyright 2005
 */
package com.helpguest.service;

import com.helpguest.HelpGuestException;
import com.helpguest.protocol.ServerProtocol;
import com.helpguest.data.ClientDataSet;
import com.helpguest.protocol.Vocabulary;
import java.net.*;
import java.io.*;
import org.apache.log4j.Logger;

public class RoutingServerThread extends Thread {
	private Socket socket = null;

	private InetAddress inetAddress = null;

	private static final Logger log = Logger
			.getLogger(RoutingServerThread.class);

	public RoutingServerThread(Socket socket) {
		super("RoutingServerThread");
		this.socket = socket;
		//TODO: Inet address should really be retrieved via STUN however we are not
		// using it yet so we just need anything.
		this.inetAddress = socket.getInetAddress();
	}

	@Override
	public void run() {

		PrintWriter out = null;
		BufferedReader in = null;
		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket
					.getInputStream()));
		} catch (IOException iox) {
			log.error("Error creating streams. Exiting thread", iox);
		}

		String inputLine, outputLine;
		ServerProtocol sp = new ServerProtocol();
		outputLine = sp.processInput(null, null);
		out.println(outputLine);

		try {
			ClientDataSet cds = new ClientDataSet(inetAddress);
			while ((inputLine = in.readLine()) != null) {
				outputLine = sp.processInput(inputLine, cds);
				//TODO: write the output to the datastore
				out.println(outputLine);
				if (Vocabulary.COM_BYE.equals(outputLine)) {
					//check if the data set is complete.
					// if so insert it
					if (cds.isComplete()) {
						try {
							cds.insert();
						} catch (HelpGuestException hgx) {
							log.error("attempted to insert invalid data set",
									hgx);
						} catch (Exception ex) {
							log.error(
									"Unable to complete ClientDataSet insert.",
									ex);
						}
					}
					break;
				}
			}
			if (log.isDebugEnabled()) {
				log.debug("Remote client closed connection.");
			}
		} catch (IOException e) {
			if (log.isInfoEnabled()) {
				log.info("Remote Client closed connection");
			}
		} finally {
			try {
				out.close();
				in.close();
				socket.close();
			} catch (IOException iox) {
				log.error("Error closing stream or socket.", iox);
			}
		}
	}
}
