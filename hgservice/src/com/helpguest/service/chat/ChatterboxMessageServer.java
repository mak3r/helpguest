/*
 * ChatServer.java
 *
 * Created on May 10, 2004, 7:47 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.service.chat;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.chatterbox.KeepAliveMessage;

/**
 *
 * @author  mabrams
 */
public class ChatterboxMessageServer extends Thread {

	private Socket sock;

	private ChatController controller;

	private static ArrayList<OutputStream> broadcastList = new ArrayList<OutputStream>();

	private static Logger LOG = Logger
			.getLogger(ChatterboxMessageServer.class);

	/** Creates a new instance of ChatServer */
	public ChatterboxMessageServer(Socket sock) {
		this.sock = sock;
	}

	public ChatterboxMessageServer(Socket sock, ChatController controller) {
		this(sock);
		this.controller = controller;
	}

	/** When an object implementing interface <code>Runnable</code> is used
	 * to create a thread, starting the thread causes the object's
	 * <code>run</code> method to be called in that separately executing
	 * thread.
	 * <p>
	 * The general contract of the method <code>run</code> is that it may
	 * take any action whatsoever.
	 *
	 * @see     java.lang.Thread#run()
	 *
	 */
	public void run() {

		ChatterboxMessage message;
		ObjectInputStream ois;
		ObjectOutputStream oos;

		try {
			ois = new ObjectInputStream(sock.getInputStream());
			oos = new ObjectOutputStream(sock.getOutputStream());

			//Add the ObjectOutputStream to the broadcast list
			broadcastList.add(oos);

			// As long as we receive data, echo that data back to the clients.
			while ((message = (ChatterboxMessage) ois.readObject()) != null) {
				if (LOG.isInfoEnabled()) {
					LOG.info("ChatServer read message: " + message);
				} // end of if ()

				if (!message.equals(new KeepAliveMessage())) {
					//its not a keep alive message, so lets broadcast it.
					//Iterate the broadcastList and send this data to all listening sockets
					for (Iterator it = broadcastList.iterator(); it.hasNext();) {
						ObjectOutputStream next = (ObjectOutputStream) it.next();
						if (next != null && !"".equals(next)) {						
							//Write this line to the socket 
							next.writeObject(message);
							next.flush();
							//If this is the first message we've received, notify the controller
							if (controller != null && !controller.isSpokenTo()) {
								controller.setSpokenTo(true);
							}
						} else if (next == null) {
							//This should take care of not writing to disconnected sockets
							broadcastList.remove(next);
						}
					}
				}//end not keep alive message
			}

			ois.close();
			oos.close();
			sock.close();

		} catch (IOException e) {
			if (LOG.isInfoEnabled()) {
				LOG.info(e.getMessage());
			}
		} catch (ClassNotFoundException cnfx) {
			LOG.error("Could not read messages: ", cnfx);
		}
	}

}
