/*
 * ChatServer.java
 *
 * Created on May 10, 2004, 7:47 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.service.chat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.io.IOException;
import org.apache.log4j.Logger;

import com.helpguest.service.chat.ChatController;

import java.util.ArrayList;
import java.util.Iterator;



/**
 *
 * @author  mabrams
 */
public class ChatServer extends Thread {
    
    private Socket sock;
    private ChatController controller;
    private static ArrayList broadcastList = new ArrayList();
    private static Logger logger = Logger.getLogger(ChatServer.class);
    
    /** Creates a new instance of ChatServer */
    public ChatServer(Socket sock) {
	this.sock = sock;
    }
    
    public ChatServer(Socket sock, ChatController controller) {
        this(sock);
        this.controller = controller;
    }
    
    /** When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see     java.lang.Thread#run()
     *
     */
    public void run() {

        String line;
        BufferedReader is;
        PrintStream os;

        try {
            is = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            os = new PrintStream(sock.getOutputStream());

            //Add the PrintStream to the broadcast list
            broadcastList.add(os);

            // As long as we receive data, echo that data back to the clients.
            while ((line = is.readLine()) != null) {
		if (logger.isInfoEnabled()) {
		    logger.info("ChatServer read line: " + line);
		} // end of if ()

		//Iterate the broadcastList and send this data to all listening sockets
                for (Iterator it = broadcastList.iterator(); it.hasNext();) {
                    PrintStream next = (PrintStream)it.next();
                    if (next != null && !"".equals(next)) {
			//Write this line to the socket 
                        next.println(line);
                        //If this is the first message we've received, notify the controller
                        if (controller != null && !controller.isSpokenTo()) {
                            controller.setSpokenTo(true);
                        }
                    } else if (next == null) {
                        //This should take care of not writing to disconnected sockets
                        broadcastList.remove(next);
                    }
                }
            }
	    
	    is.close();
	    os.close();
	    sock.close();

        }   
        catch (IOException e) {
	    if (logger.isInfoEnabled()) {logger.info(e.getMessage());} 
        }
    }

}
    
