/*
 * ChatClient.java
 *
 * Created on May 10, 2004, 7:17 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 */

package com.helpguest.service.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.net.SocketAppender;

import com.helpguest.gwt.protocol.ChatterboxMessage;
import com.helpguest.gwt.protocol.chatterbox.GuestMessage;
import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;


/**
 *
 * @author  mabrams
 */
public class ChatterboxMessageClient {
    
    private Socket sock;
    
    private ObjectOutputStream os;
    
    private ObjectInputStream is;
    
    private static Logger LOG = Logger.getLogger(ChatterboxMessageClient.class);
    
    //prahalad adding 6/4/08 
    private static String myfqdn = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    
    /** Creates a new instance of ChatClient */
    public ChatterboxMessageClient(Socket sock) {
        setSocket(sock);
    }
    
    /**
     * Default ctor.
     * Be sure to call setSocket()
     */
    public ChatterboxMessageClient() {}
    
    /**
     * Add the socket to this chat client.
     * It won't work without a socket.
     * @param sock Socket
     */
    public void setSocket(Socket sock) {
        this.sock = sock;
        try {
            os = new ObjectOutputStream(sock.getOutputStream());
            is = new ObjectInputStream(sock.getInputStream());
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: "
                    + sock.getInetAddress().getHostName());
        }
    }
    
    /**
     * Write data to the socket
     */
    public void write(ChatterboxMessage data) throws SocketException {
        // If everything has been initialized then we want to write some data
        // to the socket we have opened a connection to
        if (sock != null && os != null && is != null) {
            try {
                os.flush();
                os.writeObject(data);
                os.flush();
            } catch (UnknownHostException e) {
                if (LOG.isInfoEnabled()) {
                    LOG.info("Could not write data.  Host unknown", e);
                }
            } catch (SocketException sox) {
                LOG.warn("The chat server has disconnected. Write failed.");
                throw sox;
            } catch (IOException e) {
                if (LOG.isInfoEnabled()) {
                    LOG.info("Exception writing data.", e);
                }
            } catch (Exception ex) {
                
            }
        }
    }
    
    /**
     * Read from the socket
     **/
    public ChatterboxMessage read() {
        ChatterboxMessage message = null;
        try {
            message = (ChatterboxMessage) is.readObject();
        } catch (IOException iox) {
            if (LOG.isInfoEnabled()) {
                LOG.info("IOException: " + iox);
            }
        } catch (ClassNotFoundException cnfx) {
            LOG.error("Could not read message: ", cnfx);
        }
        return message;
    }
    
    /**
     * Close the streams and the socket when done
     */
    public void finalize() {
        try {
            os.close();
            is.close();
            sock.close();
        } catch (IOException iox) {
            if (LOG.isInfoEnabled()) {
                LOG.info("IOException: " + iox);
            }
        }
        
    }
    
    public static void main(String[] args) throws IOException {
        //BasicConfigurator.configure(new SocketAppender("beta.helpguest.com",
    	//prahalad changing static url 6/4/08
        BasicConfigurator.configure(new SocketAppender(myfqdn,
                8888));
        Socket rSocket = null;
        String hostname = null;
        final ChatterboxMessageClient chatClient = new ChatterboxMessageClient();
        int port = 0;
        
        if (args.length != 2) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("usage: java RoutingClient <hostname> <port>");
            }
            System.exit(1);
        } else {
            try {
                port = Integer.parseInt(args[1]);
            } catch (NumberFormatException nfx) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("second parameter (port number) must be an integer value.");
                }
                System.exit(1);
            }
            hostname = args[0];
        }
        
        try {
            //rSocket = new Socket("localhost", 20001);
            rSocket = new Socket(hostname, port);
            chatClient.setSocket(rSocket);
        } catch (UnknownHostException e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Don't know about host: " + hostname + ".");
            }
            System.exit(1);
        } catch (IOException e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Couldn't get I/O for the connection to: " + hostname
                        + ".");
            }
            System.exit(1);
        }
        
        /**
         * The client and server read/write must run in separate
         * threads.  In this example, the writes are done
         * in a thread off of main.  It could also be
         * successfully done with the reads of the main thread.
         */
        try {
            Thread t = new Thread( new Runnable() {
                public void run() {
                    ChatterboxMessage fromHost;
                    //TODO ? do we need to write an initial message?
                    while ((fromHost = (ChatterboxMessage) chatClient.read()) != null) {
                        if (!fromHost.getMessage().equals("")) {
                            if (LOG.isDebugEnabled()) {
                                LOG.debug("Message from host: " + fromHost);
                            }
                            
                            System.out.println(fromHost);
                            System.out.flush();
                        }
                    }
                    
                }
                
            });
            t.start();
            
            BufferedReader stdIn = new BufferedReader(
                    new InputStreamReader(System.in));
            String fromUser;
            
            while((fromUser = stdIn.readLine()) != null) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Client: " + fromUser);
                }
                if (fromUser.equalsIgnoreCase("quit")) {
                    break;
                }
                ChatterboxMessage message = new GuestMessage();
                message.setMessage(fromUser);
                chatClient.write(message);
            }
            t.interrupt();
        } catch (SocketException sox) {
            LOG.error("The server is no longer available.", sox);
            System.out.print("The other party disconnected. Exiting.");
        } catch (IOException iox) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Connection closed by remote host.");
            }
        } finally {
            chatClient.finalize();
        }
        
        
    }
}
