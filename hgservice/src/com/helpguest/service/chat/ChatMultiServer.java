/*
 * ChatMultiServer.java
 *
 * Created on May 10, 2004, 7:47 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.service.chat;

import java.net.ServerSocket;
import java.io.IOException;
import org.apache.log4j.Logger;

import com.helpguest.service.chat.ChatController;


/**
 *
 * @author  mabrams
 */
public class ChatMultiServer extends Thread {
    
    private int port;
    private ChatController controller;
    private static Logger logger = Logger.getLogger(ChatMultiServer.class);
    
    
    /** Creates a new instance of ChatMultiServer */
    public ChatMultiServer(int port) {
        this.port = port;
    }
    
    public ChatMultiServer(int port, ChatController controller) {
        this(port);
        this.controller = controller;
    }

    public void run() {
        ServerSocket serverSocket = null;
        boolean listening = true;

        try {
            //Create a new server socket for listening
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
	    if (logger.isInfoEnabled()) {logger.info("Could not listen on port: " + port);} 	    
        }

        try {
            while (listening) {
                if (logger.isInfoEnabled()) {logger.info("Waiting to accept a socket");}
                //When the server socket accepts a new connection,
                //(accept() blocks), create a new chat server on it.
                new ChatServer(serverSocket.accept(), controller).start();
            }
        } catch (IOException e) {
	    if (logger.isInfoEnabled()) {logger.info("Could not accept connection.");} 	    
        } catch (Exception ex) {
        	ex.printStackTrace();
        	if (logger.isInfoEnabled()) {logger.info("Failed to start chat server.");}
        }

        try {
            serverSocket.close();
        } catch (IOException e) {
	    if (logger.isInfoEnabled()) {logger.info("Unable to close socket.");} 	    
        }
    }
    

}
    
