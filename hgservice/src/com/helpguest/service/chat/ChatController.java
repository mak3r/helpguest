/*
 * ChatController.java
 *
 * Created on November 12, 2006, 12:23 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.helpguest.service.chat;

/**
 *
 * @author mabrams
 */
public interface ChatController {
    public void setSpokenTo(boolean spokenTo);
    public boolean isSpokenTo();
}
