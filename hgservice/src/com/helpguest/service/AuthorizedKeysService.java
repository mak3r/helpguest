package com.helpguest.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.helpguest.data.AuthorizedKeys;

public class AuthorizedKeysService {


	private static final Timer t = new Timer("AuthorizedKeysTimer", true);
	private static AuthorizedKeys authorizedKeys = new AuthorizedKeys();
	private static final Logger LOG = Logger.getLogger(AuthorizedKeysService.class);
	
	public AuthorizedKeysService() {
	}
	
	public static void start() {
		t.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				try {
					String authFile = "/home/hlpguest/.ssh/authorized_keys2";
					List<String> keyList = authorizedKeys.getCurrentKeys();				
					//overwrite the existing file
					FileWriter fw = new FileWriter(authFile, false);
					PrintWriter pw = new PrintWriter(fw);
					for (String next : keyList) {
						pw.println(next);
					}
					pw.close();
				
				} catch (IOException iox) {
					LOG.error("failed to write key to authorized key file.", iox);
				}
			}
		},0, 5000);
	}
	
	public static void stop() {
		t.cancel();
	}
}
