/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 17, 2007
 */
package com.helpguest.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.RemoteLaunchOption;
import com.helpguest.gwt.protocol.LoginType;

/**
 * @author mabrams
 *
 */
public class JnlpGen {

	private static final Logger LOG = Logger.getLogger(JnlpGen.class);
	private static String WEB_ROOT = 
		HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_ROOT.getKey());
	private static String JAWS_APPS_DIR = 
		HGServiceProperties.getInstance().getProperty(HGServiceProperty.JAWS_APPS_DIR.getKey());
	private static String FQDN = 
		HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
	
	private static String WEB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey()); 
	
	private JnlpGen() {
	}
	
	public static File generateJnlp(final String jnlpFileName, final LoginType loginType) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Generating jnlp for " + loginType);
		}
		
		File jnlpFile = 
			new File(WEB_ROOT + JAWS_APPS_DIR + System.getProperty("file.separator") +
					loginType + System.getProperty("file.separator") + 
					jnlpFileName + ".jnlp");

		if (LOG.isDebugEnabled()) {
			LOG.debug("writing jnlpFile: " + jnlpFile.getAbsolutePath());
		}
		
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(jnlpFile, false);
	
			fileWriter.write(getPartOne(jnlpFileName, loginType.toString()));
			fileWriter.write(getPartTwo(jnlpFileName, loginType));
			fileWriter.write(getPartThree());
			fileWriter.flush();
			
		} catch (IOException iox) {
			LOG.error("Could not write jnlp file for uid: " + jnlpFileName +
					" and type: " + loginType, iox);
			LOG.info("Current user: " + System.getProperty("user.name"));
			//oops it failed. set the return instance to null
			jnlpFile = null;
		} finally {
			try {
				fileWriter.close();
			} catch (IOException iox) {
				LOG.error("Could not close file writer to jnlp for uid: " + 
						jnlpFileName +
						" and type: " + loginType, iox);
			}
		}
		
		return jnlpFile;
	}
	
	public static File generateHtml(final String htmlFileName, final LoginType loginType) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Generating html for " + loginType);
		}
		
		File htmlFile = 
			new File(WEB_ROOT + JAWS_APPS_DIR + "/" + 
					loginType + "/" + htmlFileName + ".html");
		
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(htmlFile, false);
	
			fileWriter.write(getHtmlPartOne(htmlFileName, loginType));
			fileWriter.write(getHtmlPartTwo());
			fileWriter.flush();
			
		} catch (IOException iox) {
			LOG.error("Could not write jnlp file for uid: " + htmlFileName +
					" and type: " + loginType, iox);
			//oops file creation failed.  null the return instance
			htmlFile = null;
		} finally {
			try {
				fileWriter.close();
			} catch (IOException iox) {
				LOG.error("Could not close file writer to jnlp for uid: " + 
						htmlFileName +
						" and type: " + loginType, iox);
			}
		}
		
		return htmlFile;
	}
	
	private static String getPartOne(final String uid, final String type) {
		String part1 = 
			"<?xml version=\"1.0\" encoding=\"utf-8\"?> " +
			"<!-- JNLP File for Insight Demo Application -->" +
			"<!-- Auto generated via JnlpGen -->" + 
			"<jnlp" +
			" spec=\"1.0+\"" +
			" codebase=\"" + WEB_URI_PREFIX + "://" + FQDN + JAWS_APPS_DIR + "/" + type + "/\"" + 
			" href=\"" + uid + ".jnlp";
		
		return part1;
	}
	
	private static String getPartTwo(final String uid, final LoginType loginType) {
		String part2 = "";
		if (loginType.equals(new LoginType.Expert())) {
			part2 = 
				"\">" +
				"<information>" +
				"<title>HelpGuest Expert Beta</title>" +
				"<vendor>HelpGuest Technologies Inc.</vendor>" +
				"<homepage href=\"../index.html\"/>" +
				"<description>HelpGuest's Expert application is used to provide remote assistance locally</description>" +
				"<description kind=\"short\">Provide expert assistance via HelpGuest.</description>" +
				"<description kind=\"tooltip\">Provide help here</description>" +
				"<icon href=\"../images/helpguest.png\"/>" +
				"<icon kind=\"splash\" href=\"../images/helpguest.png\"/>" +
				"</information>" +
				"<security>" +
				"<all-permissions/>" +
				"</security>" +
				"<resources>" +
				"<j2se version=\"1.5+\"/>" +
				"<jar href=\"../lib/expert.jar\"/>" +
				"<jar href=\"../lib/j2ssh-core-0.2.9.jar\"/>" +
				"<jar href=\"../lib/commons-logging.jar\"/>" +
				"<jar href=\"../lib/log4j-1.2.8.jar\"/>" +
				"<jar href=\"../lib/mysql-connector-java-3.1.10-bin.jar\"/>" +
				"<jar href=\"../lib/vncviewer.jar\"/>                " +
				"</resources>" +
				"<application-desc main-class=\"com.helpguest.ui.ExpertPanel\">" +
				"<argument>expert</argument>" +
				"<argument>" + uid;
		} else if (loginType.equals(new LoginType.Client())) {
			part2 = 
				"\">" +
				"<information>" +
				"<title>HelpGuest Client Beta</title>" +
				"<vendor>HelpGuest Technologies Inc.</vendor>" +
				"<homepage href=\"../index.html\"/>" +
				"<description>HelpGuest's Guest application is used to receive concierge tehnical assistance</description>" +
				"<description kind=\"short\">Get expert assistance via HelpGuest.</description>" +
				"<description kind=\"tooltip\">Get assistance here</description>" +
				"<icon href=\"../images/helpguest.png\"/>" +
				"<icon kind=\"splash\" href=\"../images/helpguest.png\"/>" +
				"</information>" +
				"<security>" +
				"<all-permissions/>" +
				"</security>" +
				"<resources>" +
				"<j2se version=\"1.5+\"/>" +
				"<jar href=\"../lib/guest.jar\"/>" +
				"<jar href=\"../lib/j2ssh-core-0.2.9.jar\"/>" +
				"<jar href=\"../lib/commons-logging.jar\"/>" +
				"<jar href=\"../lib/log4j-1.2.8.jar\"/>" +
				"</resources>" +
				"<resources os=\"Windows\">" +
				"<nativelib href=\"../lib/VNCHooks.dll.jar\"/>" +
				"<nativelib href=\"../lib/WinVNC.dll.jar\"/>" +
				"</resources>" +
				"<application-desc main-class=\"com.helpguest.ui.HostUI\">" +
				"<argument>client</argument>" +
				"<argument>" + uid;
		}
		
		return part2;
	}
	
	private static String getPartThree() {
		String part3 = 
			"</argument>" +
			"</application-desc>" +
			"</jnlp>";
		return part3;
	}
	
	private static String getHtmlPartOne(final String uid, final LoginType loginType) {
		String part1 =
			"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"" +
			"\"http://www.w3.org/TR/html4/loose.dtd\">" +
			"<html>" +
			"<head>" +
			"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">" +
			"<meta name=\"GENERATOR\" content=\"Microsoft FrontPage 4.0\">" +
			"<meta name=\"ProgId\" content=\"FrontPage.Editor.Document\">" +
			"<title>HelpGuest Experts Client List</title>" +
			"<SCRIPT src=\"" + WEB_URI_PREFIX + "://" + FQDN + "/js/Bakery.js\" type=\"text/javascript\">" +
			"</SCRIPT>" +
			"<SCRIPT src=\"" + WEB_URI_PREFIX + "://" + FQDN + "/js/Brand.js\" language=\"javascript\" type=\"text/javascript\">" +
			"</SCRIPT>" +
			"<SCRIPT LANGUAGE=\"JavaScript\" type=\"text/javascript\">" +
			"brandPage();" +
			"function go_to(url) {" +
			"window.location=url;" +
			"}" +
			"</SCRIPT>" +
			"</head>" +
			"<body>" +
			"<OBJECT CODEBASE=\"https://java.sun.com/update/1.5.0/jinstall-1_5_0-windows-i586.cab\" " +
			"CLASSID=\"clsid:5852F5ED-8BF4-11D4-A245-0080C6F74284\" HEIGHT=0 WIDTH=0 " +
			"STANDBY=\"Loading HelpGuest...\">" +
			"<PARAM NAME=\"app\" VALUE=\"" + WEB_URI_PREFIX + "://" + FQDN + JAWS_APPS_DIR + "/" + loginType + "/" + uid + ".jnlp";
			
		return part1;
	}
	
	private static String getHtmlPartTwo() {
		String part2 =
			"\">" +
			"<PARAM NAME=\"back\" VALUE=\"true\">" +
			"<!-- Alternate HTML for browsers which cannot instantiate the object -->" +
			"<table border=0 width=\"80%\" align=\"center\">" +
			"<tr>" +
			"<td align=center>" +
			"<tr><td>" +
			"<h3>The HelpGuest application you tried to download requires a component that you do not have on your system.</h3>" +
			"<p>" +
			"The following is a description of how to install the missing component, the Java Runtime Environment (JRE), as" +
			"of the date that this document was last updated.  Your installation may require additional steps depending upon your firewall and other security settings." +
			"<ul>" +
			"<li>If you have Windows XP and you have a message at the top of this frame that says 'This site might require the following ActiveX control...'." +
			"Click that message and then select 'Install ActiveX Control'.  Follow the prompts to install the JRE.</li><p>" +
			"<li>If you did not get the message above, you must manually install the JRE as follows:" +
			"<ol>" +
			"<li>Browse to the <a href=\"http://java.sun.com/j2se/1.5.0/download.jsp\">Sun Java 5 Download Page</a></li>" +
			"<p>" +
			"<li>Below the second black bar labeled \"JRE 5.0 Update 6  includes the JVM technology\", select the item 'Download JRE 5.0 Update 6'.  " +
			"You will be directed to a page that requires you to agree to some license terms with a list of download choices.</li>" +
			"<p>" +
			"<li>Review the license agreement and if you agree, click the 'accept' radio button you will be redirected to another page that looks identical except it does not have the " +
			"accept/decline radio buttons</li>" +
			"<p>" +
			"<li>Select the item named 'Windows Online Installation, Multi-language'.  You will be prompted to save or open the item.  " +
			"<ul>" +
			"<li>If you choose to open the item, the JRE installer will begin.</li>" +
			"<li>If you choose to save the item, you will need to browse to the item on your system and run it manually.</li>" +
			"</ul>" +
			"An installer window appears with a licence agreement.  Follow the prompts to install the JRE</li>" +
			"</ol>" +
			"</li>" +
			"</ul>" +
			"<p>" +
			"If you find this process complex, we apologize.  We sincerely hope this is the last time you ever have to follow complex installation directions without a HelpGuest Expert.<p>" +
			"If you find this process simple, kudos!  We hope you'll join us as an Expert if you haven't already done so." +
			"<tr><td>&nbsp;" +
			"<tr><td>&nbsp;" +
			"</table>" +
			"<p align=\"center\"><font size=\"1\">HelpGuest Technolgies, Inc.&nbsp;</font> </p>" +
			"<p align=\"center\">" +
			"<font size=\"1\">Inquiries: <a href=\"mailto:sales@helpguest.com\">sales@helpguest.com</a>&nbsp;&lt;&gt;&nbsp;" +
			"Web site issues: <a href=\"mailto:webmaster@helpguest.com\">webmaster@helpguest.com</a>" +
			"&nbsp;&lt;&gt;&nbsp; Technical assistance: <a href=\"mailto:support@helpguest.com\">support@helpguest.com</a>" +
			"&nbsp;&lt;&gt;&nbsp;Last Updated: December 17, 2005</font></p>" +
			"</form>" +
			"</OBJECT>" +
			"</BODY>" +
			"</HTML>";

			
		return part2;
	}
	
	public static String generateRemoteLaunchRef(final RemoteLaunchOption rlo) {
		String clientParams = "?post_url=" + rlo.getPostURL() +
			"&ref_name=" + rlo.getRefName() + "&id=" + rlo.getRowId();
		String launchParams = "?post_url=" + rlo.getPostURL() +
			"&id=" + rlo.getLaunchIconId();
		String imgTag = "<img " +
				"onclick=\"window.open('" + WEB_URI_PREFIX + "://" + FQDN + "/hgservlets/HelpGuestClient" +
				clientParams + "')\" " +
				"alt=\"HelpGuest\" " +
				"src=\"" + WEB_URI_PREFIX + "://" + FQDN + "/hgservlets/DeployLaunchIcon" + launchParams + "\"/>";
		return imgTag;
	}
}
