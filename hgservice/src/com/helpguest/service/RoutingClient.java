/**
 * HelpGuest Technologies, Inc. - Copyright 2005
 */
package com.helpguest.service;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.protocol.Vocabulary;
import java.io.*;
import java.net.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.net.SocketAppender;

public class RoutingClient {

	private static final Logger log = Logger.getLogger(RoutingClient.class);
	
	//prahalad adding 6/4/08 
    private static String myfqdn = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());

	public static void main(String[] args) throws IOException {
		//prahalad changing beta 6/4/08
		//BasicConfigurator.configure(new SocketAppender("beta.helpguest.com",
		BasicConfigurator.configure(new SocketAppender(myfqdn,
				8888));
		Socket rSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;
		String hostname = null;
		int port = 0;

		if (args.length != 2) {
			if (log.isDebugEnabled()) {
				log.debug("usage: java RoutingClient <hostname> <port>");
			}
			System.exit(1);
		} else {
			try {
				port = Integer.parseInt(args[1]);
			} catch (NumberFormatException nfx) {
				if (log.isDebugEnabled()) {
					log
							.debug("second parameter (port number) must be an integer value.");
				}
				System.exit(1);
			}
			hostname = args[0];
		}

		try {
			//rSocket = new Socket("localhost", 20001);
			rSocket = new Socket(hostname, port);
			out = new PrintWriter(rSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(rSocket
					.getInputStream()));
		} catch (UnknownHostException e) {
			if (log.isDebugEnabled()) {
				log.debug("Don't know about host: " + hostname + ".");
			}
			System.exit(1);
		} catch (IOException e) {
			if (log.isDebugEnabled()) {
				log.debug("Couldn't get I/O for the connection to: " + hostname
						+ ".");
			}
			System.exit(1);
		}

		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));
		String fromServer;
		String fromUser;

		try {
			while ((fromServer = in.readLine()) != null) {
				if (log.isDebugEnabled()) {
					log.debug("Server: " + fromServer);
				}
				if (fromServer.equals(Vocabulary.COM_BYE)) {
					if (log.isDebugEnabled()) {
						log.debug("server sent BYE");
					}
					break;
				}

				fromUser = stdIn.readLine();
				if (fromUser != null) {
					if (log.isDebugEnabled()) {
						log.debug("Client: " + fromUser);
					}
					out.println(fromUser);
				}
			}
		} catch (IOException iox) {
			if (log.isInfoEnabled()) {
				log.info("Connection closed by remote host.");
			}
		} finally {
			try {
				out.close();
				in.close();
				stdIn.close();
				rSocket.close();
			} catch (IOException iox) {
				log.error("Error closing a stream or socket.");
			}
		}
	}
}
