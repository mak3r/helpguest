/*
 * AgentServer.java
 *
 * Created on November 6, 2005, 11:47 PM
 */

package com.helpguest.service;

import java.io.IOException;
import java.net.ServerSocket;
import org.apache.log4j.Logger;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class AgentServer {
    
    public enum AgentType {
        GUEST ("guest"), 
        EXPERT ("expert");
                
        private String type;
        AgentType(String type) {
            this.type = type;
        }
        
        @Override
		public String toString() {
            return type;
        }    
    }

    Logger log = Logger.getLogger("com.helpguest.service.AgentServer");
    
    /** Creates a new instance of AgentServer */
    public AgentServer(final AgentType agentType, final int port, final String uid) {
        if (log.isDebugEnabled()) {log.debug("AgentType: " + agentType);}
        Thread t = new Thread(uid) {
            @Override
			public void run() {
                ServerSocket serverSocket = null;
                try {
                    if (log.isDebugEnabled()) {log.debug("Connecting to socket " + port + "...");}
                    serverSocket = new ServerSocket(port);
                    if (log.isDebugEnabled()) {log.debug("Connected."); }
                } catch (IOException iox) {
                    log.error("Could not listen on port: " + port + ".", iox);
                }
                switch (agentType) {
                    case GUEST:
                        try {
                            if (log.isDebugEnabled()) {log.debug("preparing GuestAgent.");}
                            new GuestAgent(serverSocket.accept(), uid).start();
                            if (log.isDebugEnabled()) {log.debug("GuestAgent started");}
                        } catch (IOException iox) {
                            log.error("Could not start GuestAgent.", iox);
                        }
                        break;
                    case EXPERT:
                        try {
                            if (log.isDebugEnabled()) {log.debug("preparing ExpertAgent.");}
                            new ExpertAgent(serverSocket.accept(), uid).start();
                            if (log.isDebugEnabled()) {log.debug("ExpertAgent started");}                       
                        } catch (IOException iox) {
                            log.error("Could not start ExpertAgent.", iox);
                        }                        
                        break;
                };
            }
        };
        t.start();
    }
    
    public static void main(String[] args) {
        org.apache.log4j.BasicConfigurator.configure();
        String usage = "usage: AgentServer <port> <GuestAgent|ExpertAgent>\n" +
                       "2nd parameter defaults to expert if none given";
        int port = 0;
        AgentType at = AgentType.EXPERT;
        if (args.length >= 1) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException nfx) {
                System.err.println(usage);
                System.exit(0);
            }
        }
        if (args.length == 2) {
            if ("GuestAgent".equalsIgnoreCase(args[1])) {
                at = AgentType.GUEST;
            }
        }
        new AgentServer(at, port, at.toString());
    }
}
