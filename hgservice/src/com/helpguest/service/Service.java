/*
 * Service.java
 *
 * Created on January 2, 2005, 8:10 PM
 * Copyright 2005 HelpGuest Technologies, Inc.
 */

package com.helpguest.service;

import com.helpguest.protocol.Vocabulary;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author  mabrams
 */
public class Service {

    private String name;
    private static Collection<Service> services = new ArrayList<Service>();
    public static final Service VNC = new Service(Vocabulary.COM_VNC);
    public static final Service CHAT = new Service(Vocabulary.COM_CHAT);
    public static final Service VOIP = new Service(Vocabulary.COM_VOIP);
    public static final Service AGENT = new Service(Vocabulary.COM_AGENT);
    public static final Service WEB_CHAT = new Service(Vocabulary.COM_WEB_CHAT);
    private static final Service none = new Service(null);

    static {
        services.add(VNC);
        services.add(CHAT);
        services.add(VOIP); 
        services.add(AGENT);
        services.add(WEB_CHAT);
    }
    
    /** Creates a new instance of ServiceListEnum */
    private Service(String service) {
        this.name = service;
    }
    
    public static Collection getServiceList() {
        return services;
    }
    
    public String getName() {
        return name;
    }

    /**
     * @returns the Service object for this string.
     * If no matching Service is found, it returns
     * the null service - a special service with
     * the null value as its name.
     */
    public static Service getService(String service) {
        Iterator it = services.iterator();
        while (it.hasNext()) {
            Service next = (Service)it.next();
            if (service.equals(next.getName())) {
                return next;
            }
        }
        //If we get here, there was not matching service
        // return the null service
        return none;
    }
    
    @Override
	public boolean equals(Object service) {
        if (service instanceof Service) {
            Service s = (Service)service;
            if (this.name.equals(s.name)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
	public String toString() {
        return name;
    }
}
