/*
 * GuestAgent.java
 *
 * Created on October 21, 2005, 1:55 PM
 */

package com.helpguest.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.ClientSelectionData;
import com.helpguest.data.DataBase;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.HGSession;
import com.helpguest.protocol.Lexicon;
import com.helpguest.protocol.Vocabulary;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class ExpertAgent extends Thread {

	private ExpertAccount expertAccount;

	private Socket socket = null;

	private InetAddress inetAddress = null;

	private String uid;

	//private String dbServer = "beta.helpguest.com";
	//prahalad changing static url beta, dbuser,dbpaswd 6/4/08
	private String dbServer = 	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());

	//private String dbUser = "";
	private String dbUser = HGServiceProperties.getInstance().getProperty(HGServiceProperty.USER_NAME.getKey());
	

	//private String dbPass = "";
	private String dbPass = HGServiceProperties.getInstance().getProperty(HGServiceProperty.PASSWORD.getKey());

	private long millisToEnd;

	/**
	 * Default life is one minute
	 */
	private long DEFAULT_LIFE = 60000;

	private static final Logger log = Logger
			.getLogger("com.helpguest.service.ExpertAgent");

	/** Creates a new instance of ExpertAgent */
	public ExpertAgent(Socket socket, String uid) {
		super(uid);
		expertAccount = ExpertAccount.getExpertAccount(uid);
		this.socket = socket;
		this.inetAddress = socket.getInetAddress();
		this.uid = uid;
		if (log.isDebugEnabled()) {
			log.debug("ExpertAgent: [" + socket + "," + this.uid + "]");
		}
		//if we don't hear from you in DEFAULT_LIFE time,
		// agent will terminate
		reset();
		this.setDaemon(false);
	}
        
        public String getUid() {
            return uid;
        }

	private void reset() {
		long currentTime = System.currentTimeMillis();
		millisToEnd = currentTime + DEFAULT_LIFE;
		if (log.isDebugEnabled()) {
			java.util.Date cDate = new java.util.Date(currentTime);
			java.util.Date eDate = new java.util.Date(millisToEnd);
			log.debug("Current time: " + cDate.toString());
			log.debug("End time:     " + eDate.toString());
		}
	}

	private boolean keepAlive() {
		long currentTimeMillis = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("millisToEnd: " + millisToEnd + ", currentTime: "
					+ currentTimeMillis);
		}

		if (millisToEnd > currentTimeMillis) {
			return true;
		}
		return false;
	}

	/**
	 * This is when the expert closes the application
	 * At that point, we should delete all abandoned 
	 * records for this expert.
	 */
	private void cleanUp() {
		/**
		 * Only delete abandoned records.  Anything active should
		 * be handled by the complete method.
		 * Anything waiting should be set to abandoned by the Guest
		 * unless it's a WEB_CHAT protocol session
		 */

		//First delete from protocol request those that are labeled
		// as abandoned
		String query = "delete from protocol_request where expert_uid = '"
				+ uid + "' and status in ('" + ClientSelectionData.ABANDONED
				+ "', '" + ClientSelectionData.IN_PROGRESS + "')";
		DataBase.getInstance(dbServer, dbUser, dbPass).submit(query);

		//Now delete the response record
		query = "delete from protocol_response " + "where key_id not in ("
				+ "select key_id from protocol_request where expert_uid = '"
				+ uid + "' and status in ('" + ClientSelectionData.WAITING
				+ "'))";
		DataBase.getInstance(dbServer, dbUser, dbPass).submit(query);

		/**
		 * Now clean up web chat requests.
		 */
		//Delete STATUS waiting, and type web_chat
		String delWebChatQuery = "delete from protocol_request where expert_uid = ? "
				+ "and status = ? and type = ?";
		Object[] requestData = { uid, ClientSelectionData.WAITING,
				Service.WEB_CHAT.toString() };
		DataBase.getInstance(dbServer, dbUser, dbPass).submit(delWebChatQuery,
				requestData);

		delWebChatQuery = "delete from protocol_response " +
				"where type = ? and uid in " +
					"( select g.uid from guest g, expert e " +
					"where g.expert_id = e.id and e.uid = ?)";
		Object[] responseData = { Service.WEB_CHAT.toString(), uid };
		DataBase.getInstance(dbServer, dbUser, dbPass).submit(delWebChatQuery,
				responseData);
	}

	/**
	 * Complete some record based on the resolution string.
	 * If the resolutio string cannot be parsed, null is returned.
	 *
	 * @param resolution is a string of the form
	 * Vocabulary.TECH_RESOLVED<D>guest uid<D>record id<D>status
	 * where '<D>' indicates the Vocabulary.DELIMITER
	 */
	private String complete(String resolution) {
		//Parse the resolution
		//TODO: insert the resolution into the database
		//resolution = inputLine.substring(Vocabulary.TECH_RESOLVED.length());
		//TODO: create a delimited format line that indicates who the guest was (gUid)
		// for example TECH_RESOLVED\x07gUid\x07resolution string
		if (log.isDebugEnabled()) {
			log.debug("Resolution: '"
					+ resolution.replace(Vocabulary.DELIMITER, '|') + "'");
		}
		//Parse the resolution to get the gUid, record id, and status
		String status = null;
		String[] splitResolution = resolution.split(String
				.valueOf(Vocabulary.DELIMITER));
		if (splitResolution.length != 4) {
			log.error("Resolution string invalid.  Cannot resolve: '"
					+ resolution.replace(Vocabulary.DELIMITER, '|')
					+ "' No records will be removed.");
		} else {
			String guestUid = splitResolution[1];
			String id = splitResolution[2];
			status = splitResolution[3];

			HGSession.expire(guestUid);

			String pReqQuery = "delete from protocol_request where guest_uid = ? ";
			String pRespQuery = "delete from protocol_response where uid = ? ";
			Object[] data = { guestUid };
			DataBase.getInstance(dbServer, dbUser, dbPass).submit(pReqQuery,
					data);
			DataBase.getInstance(dbServer, dbUser, dbPass).submit(pRespQuery,
					data);
		}
		return status;
	}

	@Override
	public void run() {
		PrintWriter out = null;
		BufferedReader in = null;
		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket
					.getInputStream()));
		} catch (IOException iox) {
			log.error("Error creating streams. Exiting thread", iox);
		}

		String inputLine, outputLine;
		Collection<String> sessions = new ArrayList<String>();

		try {
			while (((inputLine = in.readLine()) != null) && keepAlive()) {
				if (log.isDebugEnabled()) {
					log.debug("Received message: " + inputLine);
				}
				//if the message is logout, were done.
				// all other messages are ignored for now.
				if (inputLine.equals(Vocabulary.LOGOUT)) {
					break;
				} else if (inputLine.length() > Lexicon.Technician.START_SESSION
						.length()
						&& inputLine.substring(0,
								Lexicon.Technician.START_SESSION.length())
								.equals(
										Lexicon.Technician.START_SESSION
												.toString())) {
					String sessionId = inputLine.substring(inputLine
							.indexOf(':') + 1);
					HGSession.start(sessionId);
				} else if (inputLine.length() >= Vocabulary.TECH_RESOLVED
						.length()
						&& inputLine.substring(0,
								Vocabulary.TECH_RESOLVED.length()).equals(
								Vocabulary.TECH_RESOLVED)) {
					//Issue has been resolved
					if (log.isDebugEnabled()) {
						log.debug("resolution: "
								+ inputLine.substring(Vocabulary.TECH_RESOLVED
										.length()));
					}
					String status = complete(inputLine);
					//stop the clock if this is the one in progress.
					if (ClientSelectionData.IN_PROGRESS.equals(status)) {
						break;
					}
				} else if (inputLine.length() >= Vocabulary.TECH_INCOMPLETE
						.length()
						&& inputLine.substring(0,
								Vocabulary.TECH_INCOMPLETE.length()).equals(
								Vocabulary.TECH_INCOMPLETE)) {
					//Issue was not resolved but tech is closing session
					//TODO: insert the incomplete resolution into the database
					if (log.isDebugEnabled()) {
						log.debug("incomplete: "
								+ inputLine
										.substring(Vocabulary.TECH_INCOMPLETE
												.length()));
					}
					complete(inputLine.substring(Vocabulary.TECH_INCOMPLETE
							.length()));
					break;
				}
				//reset any time info is received and not logout
				reset();
			}
		} catch (IOException iox) {
			log.error("Could not keep input stream open for: " + uid
					+ ".  Service will terminate.", iox);
		} catch (Exception ex) {
			log.error("Communications cannot continue.", ex);
		} finally {
			expertAccount.update();
			//Stop any outstanding sessions
			for (String sessionId : sessions) {
				HGSession.stop(sessionId);
			}
			cleanUp();
		}
	}
}
