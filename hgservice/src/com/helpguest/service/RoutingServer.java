/**
 * HelpGuest Technologies, Inc. Copyright 2005
 */
package com.helpguest.service;

import java.net.*;
import java.io.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.net.SocketAppender;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;

public class RoutingServer {
    
	//prahalad adding 6/4/08 
    private static String myfqdn = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    
    public static void main(String[] args) throws IOException {
        //BasicConfigurator.configure();
        //prahalad changing beta 6/4/08
    	//BasicConfigurator.configure(new SocketAppender("beta.helpguest.com", 8888));
    	BasicConfigurator.configure(new SocketAppender(myfqdn, 8888));
        Logger log = Logger.getLogger(RoutingServer.class);

        //AuthorizedKeysService.start();
        
        int port = 0;
        ServerSocket serverSocket = null;
        boolean listening = true;

        if (args.length != 1) {
            System.err.println("usage: java RoutingServer <port>");            
            System.exit(1);
        } else {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException nfx) {
                System.err.println("port must be an integer value.");
                System.exit(1);
            }
        }
        
        try {
            if (log.isDebugEnabled()) {log.debug("Connecting to socket " + port + "...");}
            serverSocket = new ServerSocket(port);
            if (log.isDebugEnabled()) {log.debug("Connected."); }
        } catch (IOException e) {
            log.error("Could not listen on port: " + port + ".");
            System.exit(-1);
        }

        while (listening) {
            new RoutingServerThread(serverSocket.accept()).start();
        }

        serverSocket.close();
    }
}
