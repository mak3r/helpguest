/*
 * GuestAgent.java
 *
 * Created on October 21, 2005, 1:55 PM
 */

package com.helpguest.service;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.data.ClientSelectionData;
import com.helpguest.data.DataBase;
import com.helpguest.data.HGSession;
import com.helpguest.data.Survey;
import com.helpguest.protocol.Lexicon;
import com.helpguest.protocol.Vocabulary;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import org.apache.log4j.Logger;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class GuestAgent extends Thread {
    
    private ServerSocket serverSocket = null;
    private Socket socket = null;
    private InetAddress inetAddress = null;
    //prahalad changing beta, dbuser,dbpaswd 6/4/08
    //private String dbServer = "beta.helpguest.com";
    //private String dbUser = "";
    //private String dbPass = "";
    private String dbServer = 	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
	private String dbUser = HGServiceProperties.getInstance().getProperty(HGServiceProperty.USER_NAME.getKey());
	private String dbPass = HGServiceProperties.getInstance().getProperty(HGServiceProperty.PASSWORD.getKey());
    
    private String uid;
    private long millisToEnd;
    /**
     * Default life is 20 minutes
     */
    private long DEFAULT_LIFE = 20*60*1000;
    
    private static final Logger log = Logger.getLogger(GuestAgent.class);
    
    /** Creates a new instance of GuestAgent */
    public GuestAgent(Socket socket, String uid) {
        super(uid);
        this.socket = socket;
        this.inetAddress = socket.getInetAddress();
        this.uid = uid;
        if (log.isDebugEnabled()) {
	    log.debug("GuestAgent: [" + socket + "," + this.uid + "]");
	}
        //if we don't hear from you in DEFAULT_LIFE time,
        // agent will terminate
        reset();
        this.setDaemon(false);
    }
 
    protected void reset() {
        long currentTime = System.currentTimeMillis();
        millisToEnd = currentTime + DEFAULT_LIFE;
	if (log.isDebugEnabled()) {
            java.util.Date cDate = new java.util.Date(currentTime);
            java.util.Date eDate = new java.util.Date(millisToEnd);  
            log.debug("Current time: " + cDate.toString());
            log.debug("End time:     " + eDate.toString());
        }
    }
    
    protected boolean keepAlive() {
        if (millisToEnd > System.currentTimeMillis()) {
            return true;
        }
        return false;
    }
    
    protected void cleanUp() {
        //Update the protocol_request table as necessary
        /** 
         * REMOVE WAITING - DON'T INSERT ABANDONED BECAUSE
         * EXPERT CANNOT CLEAN HOUSE ON ABANDONED
         *
        String query = "update protocol_request set status = '" + ClientSelectionData.ABANDONED +
                "' where guest_uid = '" + uid + "' and status = '" +
                ClientSelectionData.IN_PROGRESS + "'";
        DataBase.getInstance(dbServer, dbUser, dbPass).submit(query);
         
        String pReqQuery = "delete from protocol_request where guest_uid = ? " +
                "and status = ? ";
        */
    	 if (log.isDebugEnabled()) {log.debug("In Guest Side clean up uid = " + uid);}
        String pReqQuery = "delete from protocol_request where guest_uid = '" + uid + "' " +
        "and status in ('" + ClientSelectionData.ABANDONED + "', '" + ClientSelectionData.IN_PROGRESS + "','" + 
        ClientSelectionData.WAITING + "') ";
        //Object[] data = {uid, ClientSelectionData.WAITING};
        //DataBase.getInstance(dbServer,dbUser,dbPass).submit(pReqQuery,data);
        DataBase.getInstance(dbServer,dbUser,dbPass).submit(pReqQuery);
    }
    
    @Override
	public void run() {
        PrintWriter out = null;
        BufferedReader in = null;
	try {
	    out = new PrintWriter(socket.getOutputStream(), true);
	    in = new BufferedReader(new InputStreamReader(
				    socket.getInputStream()));
        } catch (IOException iox) {
            log.error("Error creating streams. Exiting thread", iox);
        }

        String inputLine, outputLine;
        
        try {
            if (log.isDebugEnabled()) {log.debug("listening...");}
            while (((inputLine = in.readLine()) != null) && keepAlive()) {
                if (log.isDebugEnabled()) {
                    log.debug("message received: " + inputLine);
                    if (inputLine.indexOf(":") != -1) {
                        String[] si = inputLine.split(":");                    
                        log.debug("si[0]: '" + si[0] + "', si[1]: '" + si[1] + "'");
                        log.debug((Lexicon.Guest.START_SESSION.toString().equals(si[0])?"matches START_SESSION":"doesn't match START_SESSION"));
                        log.debug((Lexicon.Guest.START_SESSION.length() == si[0].length()?"length matches START_SESSION":"length doesn't match START_SESSION"));                    
                    }//end colon check
                }
                //if the message is logout, were done.
                if (inputLine.equals(Vocabulary.LOGOUT)) {
                    break;
                } else if (inputLine.length() > Lexicon.Guest.START_SESSION.length() && 
                           inputLine.substring(0,Lexicon.Guest.START_SESSION.length()).equals(Lexicon.Guest.START_SESSION.toString())) {
                    String[] sessionInfo = inputLine.split(":");
                    HGSession.guestStart(sessionInfo[1]);
                } else if (inputLine.length() > Lexicon.Guest.STOP_SESSION.length() && 
                           inputLine.substring(0,Lexicon.Guest.STOP_SESSION.length()).equals(Lexicon.Guest.STOP_SESSION.toString())) {
                    String[] sessionInfo = inputLine.split(":");
                    HGSession.stop(sessionInfo[1]);
                } else if (inputLine.equals(Lexicon.Client.SURVEY.toString())) {
                    ObjectInputStream oin = new ObjectInputStream(socket.getInputStream());
                    try {
                        Survey survey = (Survey)oin.readObject();                    
                        survey.persist(uid);
                        //The survey is the final statement issued from the client
                        // no need to keep checking the input stream.
                        break;
                    } catch (ClassNotFoundException cnfx) {
                        log.error("Could not complete survey.", cnfx);
                    }
                }
                //reset any time info is received and not logout
                reset();
            }
            
            
        } catch (IOException iox) {
            log.error("Could not keep input stream open for: " + uid
                    + ".  Service will terminate.", iox);
        } finally {
            //Stop the session if it has not already been stopped
            HGSession.stop(uid);
            cleanUp();
        }        
    }
}
