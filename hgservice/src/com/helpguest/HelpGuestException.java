/*
 * HelpGuestException.java
 *
 * Created on January 6, 2005, 5:47 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 */

package com.helpguest;

/**
 * This class is used to indicate any type of exception from 
 * the the helpguest application.
 *
 * @author  mabrams
 */
public class HelpGuestException extends Exception {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -3996985615636543493L;

	/** Creates a new instance of HelpGuestException */
    public HelpGuestException() {
    }

    public HelpGuestException(String msg) {
        super(msg);
    }
    
    public HelpGuestException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
    
    public HelpGuestException(Throwable throwable) {
        super(throwable);
    }
    
    
}
