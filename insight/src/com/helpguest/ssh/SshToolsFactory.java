/**
 * 
 */
package com.helpguest.ssh;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;


/**
 * @author mabrams
 *
 */
public class SshToolsFactory {

	private static final String CURRENT_OS_NAME = System.getProperty("os.name");
	private static final String OS_MAC = "Mac OS X";
    //prahalad changing beta 6/4/08
	//private static String hostname = "beta.helpguest.com";
	private static String hostname = HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    // The default ssh port is 9022
    private static int port = 9022;
    

	private SshToolsFactory() {
		
	}
	
	/**
	 * Use the key manager as selected by the system.
	 * This is the preferred way to get a KeyManager.
	 * In some cases, and with a perfect working ssh (currently sshTools has 
	 * some issues) the embeddedKeyManager would always be
	 * functional regardless of the underlying os.
	 * 
	 * @return a KeyManager selected by the system.
	 */
	public static KeyManager getPreferredKeyManager() {
		//FIXME another test for command line key generation is to exec() some ssh and ssh-keygen -version commands
		if (CURRENT_OS_NAME.equals(OS_MAC)) {
			return CommandLineKeyManager.getInstance();
			//return EmbeddedKeyManager.getInstance();
		} else {
			//for now, if it's not a Mac, we assume it's windows
			return EmbeddedKeyManager.getInstance();
		}
	}
	
	
	public static KeyManager getEmbeddedKeyManager() {
		return EmbeddedKeyManager.getInstance();
	}
	
	public static KeyManager getCommandLineKeyManager() {
		return CommandLineKeyManager.getInstance();
	}
	
	/**
	 * Use the tunnel manager as selected by the system.
	 * This is the preferred way to get a TunnelManager.
	 * In some cases, and with a perfect working java ssh 
	 * (currently sshTools has some issues) 
	 * the embeddedTunnelManager would always be
	 * functional regardless of the underlying os.
	 * 
	 * @return a KeyManager selected by the system.
	 */
	public static TunnelManager getPreferredTunnelManager() {
		//FIXME another test for command line key generation is to exec() some ssh and ssh-keygen -version commands
		if (CURRENT_OS_NAME.equals(OS_MAC)) {
			return CommandLineTunnelManager.getInstance();
			//return HGTunnelHost.getInstance();
		} else {
			//for now, if it's not a Mac, we assume it's windows
			return HGTunnelHost.getInstance();
		}
	}
	
	
	public static TunnelManager getEmbeddedTunnelManager() {
		return HGTunnelHost.getInstance();
	}
	
	public static TunnelManager getCommandLineTunnelManager() {
		return CommandLineTunnelManager.getInstance();
	}
	
	public static String getHostname() {
		return hostname;
	}

	public static int getPort() {
		return port;
	}

	
}
