package com.helpguest.ssh;

import java.awt.Component;

public interface TunnelManager {

	/**
	 * 
	 * @param hostname is the hostname to connect with.
	 * @param username is the username to connect with.
	 * @param privateKeyFile is the name of the file containing the private key to use
	 * @param parent is the parent component that the host key dialog verification should use
	 * @return the ssh com.sshtools.j2ssh.authentication.AuthenticationProtocolState. If the
	 * concrete class does not implement tunnels with sshtools, it should still
	 * return a value that correlates to the sshtools AuthenticationProtocolState(s)
	 */
	public abstract int authenticate(String username, String privateKeyFile,
			Component parent);

	/**
	 * Open a tunnel to the client using the ports specified.  This is a 
	 * remote forwarding tunnel. i.e. information sent to the remote port is 
	 * forwarded to the local port.
	 * @param localPort is the port on the local system
	 * @param remotePort is the port on the remote system.
	 */
	public abstract boolean openTunnelRF(int localPort, int remotePort,
			String name);

	/**
	 * Open a tunnel to the client using the ports specified.  This is a 
	 * local forwarding tunnel. i.e. information sent to the local port is 
	 * forwarded to the remote port.
	 * @param localPort is the port on the local system
	 * @param remotePort is the port on the remote system.
	 */
	public abstract boolean openTunnelLF(int localPort, int remotePort,
			String name);

	public abstract void closeTunnelLF(final String name);

	public abstract void closeTunnelRF(final String name);

	/**
	 * Close the tunnel named.
	 */
	public abstract void closeTunnel(String name);

	/**
	 * Close all tunnels, disconnect the ssh connection and release resources
	 */
	public abstract void disconnect();

	public abstract void waitForDisconnectState();
	
	/**
	 * Get the hostname used by this tunnel manager.
	 * @return the hostname used by this tunnel manager
	 */
	public abstract String getHostname();
	
	/**
	 * Get the port used by this tunnel manager.
	 * @return the port used by this tunnel manager.
	 */
	public abstract int getPort();

}