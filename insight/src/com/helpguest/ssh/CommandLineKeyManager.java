/**
 * 
 */
package com.helpguest.ssh;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.List;

import org.apache.log4j.Logger;

import com.helpguest.util.Util;

/**
 * @author mabrams
 *
 */
public class CommandLineKeyManager extends KeyManagerAdapter implements
		KeyManager {
	
	private static final Logger LOG = Logger.getLogger(CommandLineKeyManager.class);

	private static CommandLineKeyManager commandLineKeyManager = null;
	private final static String passphrase = "";//String.valueOf(Math.random());
	private String openKeyContent;

	

	private CommandLineKeyManager() {
		super();
	}
	
	protected static CommandLineKeyManager getInstance() {
    	if (commandLineKeyManager == null) {
    		commandLineKeyManager = new CommandLineKeyManager();
	    	commandLineKeyManager.createOpenKey();
	    	commandLineKeyManager.updateUserKnownHosts(commandLineKeyManager.getKnownHostRecords());
    	}
    	return commandLineKeyManager;
    }

	private String[] getSshKeygenCommands(final String type) {
		String[] sshKeygenCommands = {
			"ssh-keygen",
			"-b",
			"1024",
			"-t",
			type,
			"-C",
			"\"HelpGuest auto generated key\"",
			"-f",
			getPrivateKeyFile().getAbsolutePath()			
		};
		
		return sshKeygenCommands;
	}
	
	/* (non-Javadoc)
	 * @see com.helpguest.ssh.KeyManager#generateDSAKeys()
	 */
	public void generateDSAKeys() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Generating dsa keys via command line.");
		}
		final String[] command = getSshKeygenCommands("dsa");
		try {					
			if (LOG.isDebugEnabled()) {
				LOG.debug("Executing command now.");
			}					
			Process proc = Runtime.getRuntime().exec(command);
			Util.logProcess("CommandLineKeyManager.generateDSAKeys", proc);
			proc.getOutputStream().write(new String("\n\n").getBytes());
			proc.getOutputStream().flush();
			int exitStatus = proc.waitFor();
			if (LOG.isInfoEnabled()) {
				LOG.info("Command '" + buildCommandLine(command) + "' exited with status: " + exitStatus);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("Command execution complete.");
			}				
			//Make this private key viable to the ssh app
			setPrivateKeyPermissions();
		} catch (IOException iox) {
			LOG.error("Could not exec command", iox);
		} catch (InterruptedException iex) {
			LOG.error("Process interrupted before completion.");
		}

	}
	
	public void setPrivateKeyPermissions() {
		String [] chmodCommand = {
				"chmod",
				"600",
				getPrivateKeyFile().getAbsolutePath()
		};
		try {
			Process proc = Runtime.getRuntime().exec(chmodCommand);
		} catch (IOException iox) {
			LOG.error("failed to modify private key permissions.", iox);
		}
	}
	
	private void createOpenKey() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Creating ssh open key content.");
		}					
		
		StringBuffer sBuf = new StringBuffer();
		LineNumberReader lineReader = null;
		try {
			lineReader = new LineNumberReader(new FileReader(
							SshToolsFactory.getPreferredKeyManager().getPublicKeyFile()));
			String line = null;
			while((line = lineReader.readLine()) != null) {
				sBuf.append(line);
			}
		} catch (IOException iox) {
			LOG.error("Could not read process input stream.", iox);
		} finally {
			try {
				lineReader.close();
			} catch (IOException iox) {
				LOG.error("Could not close input stream.", iox);
			}
		}			

		
		openKeyContent = sBuf.toString();
	}
	
	/**
	 * Currently unimplemented.
	 */
	public void generateRSAKeys() {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Generate a pseudo-random passphrase for the session
	 */
	@Override
	public String getPassphrase() {
		return passphrase;
	}

	public String getOpenKeyContent() throws IllegalStateException {
		return openKeyContent;
	}

	/**
	 * This is a debug method used to concatenate the
	 * command array into a single line separated
	 * by spaces.
	 * @param command String[]
	 * @return a String of all the elements of the array concatenated
	 * together and separated by spaces.
	 */
	private String buildCommandLine(final String[] command) {
		StringBuffer sBuf = new StringBuffer();
		for(String next : command) {
			sBuf.append(next + " ");
		}
		return sBuf.toString();
	}
	
	public static void updateUserKnownHosts(List<String> knownHostRecords) {
		try {
			FileWriter fw = new FileWriter(commandLineKeyManager.getUserKnownHostsFile());
			for (String next : knownHostRecords) {
				if (next != null) {
					fw.write(next+"\n");
					fw.flush();
				}
			}
			fw.close();
		} catch (IOException iox) {
			LOG.error("Unable to add known hosts to known hosts file", iox);
		}
	}

}
