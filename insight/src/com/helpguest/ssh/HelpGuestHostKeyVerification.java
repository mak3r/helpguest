/*
 * HelpGuestHostKeyVerification.java
 *
 * Created on July 28, 2005, 11:42 AM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.ssh;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.List;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.publickey.SshPublicKey;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class HelpGuestHostKeyVerification implements HostKeyVerification {
    
	private static final Logger LOG = Logger.getLogger(HelpGuestHostKeyVerification.class);
	
    //prahalad changing host 6/4/08
	//private final String expectedHost = "beta.helpguest.com,67.18.176.157";
    private final String expectedHost = HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey()) + ",67.18.176.157";
    //prahalad adding 08/06/2008
    private final String igHostKeyVerfctn = HGServiceProperties.getInstance().getProperty(HGServiceProperty.IGNORE_HK_VERIFICATN.getKey());
    
	private final String[] fingerprints = {
	        "1024: 78 37 ae de 9f e8 9a a5 9a 09 43 5d 0b 44 8a f2",
	        "2048: fe da 9b fd 50 bb a1 e6 66 fe 1 2e 7d 3a 91 c5",
	        "2048: fe da 9b fd 50 bb a1 e6 66 fe 01 2e 7d 3a 91 c5",
	        "1028: 78 37 ae de 9f e8 9a a5 9a 9 43 5d b 44 8a f2",
	        "1028: 78 37 ae de 9f e8 9a a5 9a 09 43 5d 0b 44 8a f2"
    };
    
    private static final Logger logger = Logger.getLogger("com.helpguest.ssh.HelpGuestHostKeyVerification");
    
    /** Creates a new instance of HelpGuestHostKeyVerification */
    public HelpGuestHostKeyVerification() {
    }
    
    public boolean verifyHost(java.lang.String host, SshPublicKey pk) {
    	if (logger.isDebugEnabled()) { 
        	logger.debug("host: " + host); 
        	logger.debug("fingerprint given:    '" + pk.getFingerprint() + "'"); 
        	logger.debug("fingerprint expected one of: ");
            for (String next : fingerprints) {
            	 logger.debug("'" + next + "'"); 
            }
        }

    	/*return true;*/
        
        if (igHostKeyVerfctn.equals("false"))
        {
	        for (String next : fingerprints) {
	        	if (expectedHost.equals(host) && next.equalsIgnoreCase(pk.getFingerprint())) {
	                return true;
	            }
	        }
	       
        }else if (igHostKeyVerfctn.equals("true")) {return true;}
       
        return false;
        
    }
    
    /**
	 * Issue known hosts record.
	 * <br/>
	 * This method is not inteded for use with the J2ssh libraries but
	 * is instead inteded for use with command line ssh usage.
	 * <br/>
	 * If the helpguest server record exists, then do nothing.
	 * if it does not exist, then append it to the tail of the file.
	 * @param the knownHostRecord
	 * @deprecated this method modifies a user's file and doesn't clean up
	 * afterward.  Use updateUserKnownHosts() instead.
	 */
	public static void updateKnownHosts(final String knownHostsRecord) {
		if (knownHostsRecord == null) {
			//woops we cant do anything with a null value
			return;
		}
		
		File dotSsh = new File(System.getProperty("user.home") + File.separator + ".ssh");
		if (!dotSsh.exists()) {
			if (!dotSsh.mkdir()) {
				LOG.error(".ssh directory does not exist and it could not be created");
			}
		}
	
		File knownHosts = new File(dotSsh.getAbsoluteFile() + File.separator + "known_hosts");
		if (!knownHosts.exists()) {
			writeKnownHostsRecord(knownHosts, knownHostsRecord);
		} else {
			//check the file for this record
			//if it exits break
			// otherwise, append it
			boolean hostRecordMatched = false;
			try {
				//checking for the record
				LineNumberReader lnr = new LineNumberReader(new FileReader(knownHosts));
				String hostRecord = null;
				while ((hostRecord = lnr.readLine()) != null) {
					if (knownHostsRecord.equals(hostRecord)) {
						//we found a match
						hostRecordMatched = true;
						break;
					}
				}
				
				if (!hostRecordMatched) {
					//no match was found.  let's create it.
					writeKnownHostsRecord(knownHosts, knownHostsRecord);
				}
			} catch (IOException iox) {
				LOG.error("cannot read known_hosts_file: " + knownHosts.getAbsolutePath(), iox);
			}
		}
	}
	
	/**
	 * Assistant method to updateKnownHosts.
	 * Assumes the file exists and the record is not null.
	 * 
	 * @param knownHosts
	 * @param knownHostsRecord
	 * @deprecated due to calling method deprecation updateKnownHosts()
	 * use updateUserKnownHosts() instead.
	 */
	private static void writeKnownHostsRecord(final File knownHosts, final String knownHostsRecord) {
		try {
			FileWriter fw = new FileWriter(knownHosts, true);
			fw.write(knownHostsRecord+"\n");
			fw.flush();
			fw.close();
		} catch (IOException iox) {
			LOG.error("cannot write to known_hosts: " + knownHosts.getAbsolutePath(), iox);
		}
	}
	

}
