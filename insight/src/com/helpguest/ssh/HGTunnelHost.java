/*
 * HGTunnel.java
 *
 * Created on November 5, 2004, 12:11 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.ssh;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;
import com.sshtools.j2ssh.authentication.PublicKeyAuthenticationClient;
import com.sshtools.j2ssh.forwarding.ForwardingClient;
import com.sshtools.j2ssh.forwarding.ForwardingConfiguration;
import com.sshtools.j2ssh.forwarding.ForwardingConfigurationException;
import com.sshtools.j2ssh.transport.InvalidHostFileException;
import com.sshtools.j2ssh.transport.TransportProtocolException;
import com.sshtools.j2ssh.transport.TransportProtocolState;
import com.sshtools.j2ssh.transport.publickey.InvalidSshKeyException;
import com.sshtools.j2ssh.transport.publickey.SshPrivateKey;
import com.sshtools.j2ssh.transport.publickey.SshPrivateKeyFile;

/**
 *
 * @author  mabrams
 */
public class HGTunnelHost implements TunnelManager {
    private SshClient ssh = new SshClient();
    private int authentication = AuthenticationProtocolState.FAILED;
    
    private static HGTunnelHost hGTunnelHost = null;
    private static Logger LOG = Logger.getLogger(HGTunnelHost.class);
    private static boolean INFO = LOG.isInfoEnabled();
    
    private HGTunnelHost() {    	
    }
    
    /**
     * Creates a new instance of HGTunnel 
     */
    public static HGTunnelHost getInstance() {
    	if (hGTunnelHost == null) {
    		hGTunnelHost = new HGTunnelHost();
    	}
    	return hGTunnelHost;
    }


    /* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#authenticate(java.lang.String, java.lang.String, java.awt.Component)
	 */
    public int authenticate(String username, String privateKeyFile, Component parent) {
        if (LOG.isDebugEnabled()) {LOG.debug("authenticate(String,String,Component) : int");}

        //Don't try to authenticate if we'return already in an acceptable
        // authentication state.
        if (authentication == AuthenticationProtocolState.COMPLETE ||
            authentication == AuthenticationProtocolState.READY ||
            authentication == AuthenticationProtocolState.PARTIAL) {
                return authentication;
        }
        
        // Connect to the host
        try {
        	if (LOG.isDebugEnabled()) {LOG.debug("preparing to connect to host: " + SshToolsFactory.getHostname());}
            ssh.connect(SshToolsFactory.getHostname(), SshToolsFactory.getPort(), new HelpGuestHostKeyVerification());//new DialogHostKeyVerification(parent));
        	if (LOG.isDebugEnabled()) {LOG.debug("connect completed. preparing public key authentication");}

        	
            PublicKeyAuthenticationClient pk = new PublicKeyAuthenticationClient();
            pk.setUsername(username);

            if (LOG.isDebugEnabled()) {LOG.debug("preparing to parse private key: " + privateKeyFile);}

            SshPrivateKeyFile file =
                SshPrivateKeyFile.parse(new File(privateKeyFile));

            if (LOG.isDebugEnabled()) {LOG.debug("finished parsing private key: " + privateKeyFile);}
            
            // pasphrase argument is always null for now.
            SshPrivateKey key = file.toPrivateKey(SshToolsFactory.getPreferredKeyManager().getPassphrase());
            pk.setKey(key);

            if (LOG.isDebugEnabled()) {LOG.debug("finished setting key on file: " + privateKeyFile);}

            if (LOG.isDebugEnabled()) { 
                java.util.List list = ssh.getAvailableAuthMethods(username);
                for (java.util.Iterator it = list.iterator(); it.hasNext();) {
                    LOG.debug("Authentication method: " + (String)it.next() + 
                    " is available.");
                }
            }
            // Try the authentication
            if (LOG.isDebugEnabled()) {LOG.debug("attempting authentication");}
            authentication = ssh.authenticate(pk);
            if (LOG.isDebugEnabled()) {LOG.debug("authentication complete - value: " + authentication);}

        } catch (InvalidHostFileException ihfe) {
           if(INFO) {
        	   LOG.info("Unable to authenticate host.", ihfe);
                ihfe.printStackTrace();
           }
        } catch (InvalidSshKeyException iske) {
            if(INFO) {
                LOG.info("ssh key was invalid.", iske);
                iske.printStackTrace();
            }
        } catch (IOException ioe) {
            if(INFO) {
                LOG.info("key verification failed or unable to use private key file: ",
                            ioe);
                ioe.printStackTrace();
            }
        }
        return authentication;
    }
 
    /* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#openTunnelRF(int, int, java.lang.String)
	 */
     public boolean openTunnelRF(int localPort, int remotePort, String name ) {
        try {
            if (LOG.isInfoEnabled()) {
                LOG.info("Opening tunnel for remote forwarding. localPort: [" + localPort + 
                            "] remotePort: [" + remotePort + "] name: [" + name + "].");
            }
            ForwardingConfiguration remoteForwardingConfig = 
            	new ForwardingConfiguration(name,"0.0.0.0", remotePort, "127.0.0.1", localPort);
            ForwardingClient forwardingClient = ssh.getForwardingClient();
            forwardingClient.addRemoteForwarding(remoteForwardingConfig);
            forwardingClient.startRemoteForwarding(remoteForwardingConfig.getName());
        } catch (ForwardingConfigurationException fcx) {
            LOG.error("Could not bind on local port: " + localPort, fcx);
            return false;
        } catch (TransportProtocolException px) {
            LOG.error("Unable to open tunnel for " + name + " with local port " + 
                         localPort + " and remote port " + remotePort, px);
            return false;
        } catch (IOException iox) {
        	LOG.error("io exception caused tunnel failure for " + name 
        			+ " with local port " + localPort + " and remotePort "
        			+ remotePort, iox);
        	return false;
        }
        return true;
    }
    

     /* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#openTunnelLF(int, int, java.lang.String)
	 */
     public boolean openTunnelLF(int localPort, int remotePort, String name) {
        try {
            if (LOG.isInfoEnabled()) {
                LOG.info("Opening tunnel for local forwarding. localPort: [" + localPort + 
                            "] remotePort: [" + remotePort + "] name: [" + name + "].");
            }
            ForwardingConfiguration localForwardingConfig = 
            	new ForwardingConfiguration(name,"0.0.0.0", localPort, "127.0.0.1", remotePort);
            ForwardingClient forwardingClient = ssh.getForwardingClient();
            forwardingClient.addLocalForwarding(localForwardingConfig);
            forwardingClient.startLocalForwarding(localForwardingConfig.getName());
        } catch (ForwardingConfigurationException fcx) {
            LOG.error("Could not bind on local port: " + localPort, fcx);
            return false;
        }
        return true;
    }
     
     /* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#closeTunnelLF(java.lang.String)
	 */
    public void closeTunnelLF(final String name) {
     	try {
            ForwardingClient forwardingClient = ssh.getForwardingClient();
     		forwardingClient.removeLocalForwarding(name);    	
     	} catch (ForwardingConfigurationException fcx) {
     		LOG.error("Failed to remove forwarding for '" + name + "'", fcx);
     	}
     }
      
     /* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#closeTunnelRF(java.lang.String)
	 */
    public void closeTunnelRF(final String name) {
     	try {
            ForwardingClient forwardingClient = ssh.getForwardingClient();
     		forwardingClient.removeRemoteForwarding(name);    	
     	} catch (ForwardingConfigurationException fcx) {
     		LOG.error("Failed to remove forwarding for '" + name + "'", fcx);
     	} catch (IOException iox) {
     		LOG.error("Could not remove remote forwarding for '" + name + "'", iox);     		
     	}
     }
      
    /* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#closeTunnel(java.lang.String)
	 */
    public void closeTunnel(String name) {
    }
    
    /* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#disconnect()
	 */
    public void disconnect() {        
        ssh.disconnect();
    }
    
    /* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#waitForDisconnectState()
	 */
    public void waitForDisconnectState() {
        Thread t = new Thread ( new Runnable() {
            public void run() {
            	try {
            		ssh.getConnectionState().waitForState(TransportProtocolState.DISCONNECTED);
            	} catch (InterruptedException iex) {
            		LOG.error("Thread interrupted while waiting for disconnect.");
            	}
            }});
        t.start();
    }


	public String getHostname() {
		return SshToolsFactory.getHostname();
	}


	public int getPort() {
		return SshToolsFactory.getPort();
	}
}
