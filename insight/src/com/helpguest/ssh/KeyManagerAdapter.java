package com.helpguest.ssh;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.helpguest.ArtifactManager;
import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;

public abstract class KeyManagerAdapter implements KeyManager {

	Properties props = new Properties();
	private static String keyHostHasKey = "host.has.key";
	private static String keyHostname = "hostname";
	private static String keyPrivateKeyFile = "private.key.file";
	private static String keyUsername = "username";
	private static String keyPubKeyFile = "pub.key.file";
	private static String keyOpenKeyFile = "open.key.file";
	private static String keySecureHostRecord = "secure.host.record";
	private static String keyBetaHostRecord = "beta.host.record";
	private static String keyDeveloperHostRecord = "developer.host.record";
	private static String keyTesterHostRecord = "tester.host.record";
	private static String keyUserKnownHostsFile = "user.known.hosts";
	private static boolean hostHasKey;
	private String hostname;
	protected String privateKeyFile;
	protected String username;
	private String pubKeyFile;
	private String openKeyFile;
	private String userKnownHostsFile;
	private List<String> knownHostRecords = new ArrayList<String>();
	protected static final Logger LOG = Logger.getLogger(KeyManagerAdapter.class);
	
	//prahalad adding 6/4/08
	private static String WEB_URI_PREFIX = 
    	HGServiceProperties.getInstance().getProperty(HGServiceProperty.WEB_URI_PREFIX.getKey());
    private static String myfqdn = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private static String myScPort = 
     	HGServiceProperties.getInstance().getProperty(HGServiceProperty.SCPORT.getKey());

	/**
	 * The KeyManagerAdapter c'tor initializes it's concrete KeyManager.
	 * It calls the following methods in this order.
	 * <br />
	 * <ol>
	 * <li><code>readProperties()</code></li>
	 * <li><code>generateDSAKeys()</code></li>
	 * <li><code>generateRSAKeys()</code></li>
	 * </ol>
	 */
	public KeyManagerAdapter() {
		super();
		readProperties();
		generateDSAKeys();
		generateRSAKeys();
	}
	
	/* (non-Javadoc)
	 * @see com.helpguest.ssh.KeyManager#readProperties()
	 */
	public void readProperties() throws IllegalStateException {
	    try {
	        LOG.debug("Loading properties from file.");
	        props.load(this.getClass().getResourceAsStream("/com/helpguest/ssh/KeyManager.properties"));
	    } catch (Exception ex) {
	        String msg = "Could not get KeyManager.properties";
	        LOG.error(msg, ex);
	        throw new IllegalStateException(msg);
	    }
	
	    Boolean hhk = new Boolean(props.getProperty(keyHostHasKey));
	    hostHasKey = hhk.booleanValue();
	    hostname = props.getProperty(keyHostname);
	    privateKeyFile = props.getProperty(keyPrivateKeyFile);
	    username = props.getProperty(keyUsername);
	    pubKeyFile = props.getProperty(keyPubKeyFile);
	    openKeyFile = props.getProperty(keyOpenKeyFile);
	    userKnownHostsFile = props.getProperty(keyUserKnownHostsFile);
	    
	    knownHostRecords.add(props.getProperty(keySecureHostRecord));
	    knownHostRecords.add(props.getProperty(keyBetaHostRecord));
	    knownHostRecords.add(props.getProperty(keyDeveloperHostRecord));
	    knownHostRecords.add(props.getProperty(keyTesterHostRecord));
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.KeyManager#writeProperties()
	 */
	public void writeProperties() throws IllegalStateException {
	    try {
	    	//FIXME application properties should be managed by the artifact manager
	        FileOutputStream out = new FileOutputStream(ArtifactManager.createArtifact("application.properties", true));
	        props.store(out, "##Help Guest application properties");
	        out.close();
	    } catch (Exception ex) {
	        String msg = "Could not write application.properties";
	        LOG.error(msg, ex);
	        throw new IllegalStateException(msg);
	    }
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.KeyManager#sendPubKey()
	 */
	public void sendPubKey() throws IllegalStateException {
	    if (LOG.isDebugEnabled()) { LOG.debug( "Preparing to send public key to host"); }

	    String key = getOpenKeyContent();

	    //send the key to the ssh host
	    boolean keySent = false;
	    URL url = null;
	    try {
	    	key = URLEncoder.encode(key, "US-ASCII");
	    	url = new URL(WEB_URI_PREFIX + "://" + myfqdn + "/hgservlets/" +
	                          "storekey?pubkey=" + key);
	        if (LOG.isDebugEnabled()) { LOG.debug( "Sending key to '" + url + "'."); }
	        HttpURLConnection con = (HttpURLConnection)url.openConnection();
	        con.setRequestMethod("POST");
	        con.connect();
	        //TODO verify that the key was received.
	        //parse buf for appropriate values.
	        InputStream is = con.getInputStream();
	        byte[] buf = new byte[1024];
	        int i = is.read(buf,0,1024);
	        keySent = true;
	    } catch (Exception ex) {
	        String msg = "Could not send public key to host at url: " + url;
	        if (LOG.isDebugEnabled()) { LOG.debug("key: " + key); }
	        LOG.error(msg, ex);
	        throw new IllegalStateException(msg);
	    }
	    
	    if (keySent) {
	        props.put(keyHostHasKey, "true");
	    }
	    
	}
	
	protected List<String> getKnownHostRecords() {
		return knownHostRecords;
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.KeyManager#getUsername()
	 */
	public String getUsername() {
	    return username;
	}

	public String getPassphrase() {
		return "";
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.KeyManager#getOpenKeyFile()
	 */
	public File getOpenKeyFile() {
		return ArtifactManager.createArtifact(openKeyFile, true);
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.KeyManager#getPublicKeyFile()
	 */
	public File getPublicKeyFile() {
		return ArtifactManager.createArtifact(pubKeyFile, true);
	}
	
	public File getPrivateKeyFile() {
		return ArtifactManager.createArtifact(privateKeyFile, true);
	}
	
	public File getUserKnownHostsFile() {
		return ArtifactManager.createArtifact(userKnownHostsFile, true);
	}

}