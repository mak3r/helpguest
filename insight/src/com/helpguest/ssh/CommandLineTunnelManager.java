/*
 * HGTunnel.java
 *
 * Created on November 5, 2004, 12:11 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.ssh;

import java.awt.Component;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.helpguest.util.ProcessMonitor;
import com.helpguest.util.Util;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;

/**
 *
 * @author  mabrams
 */
public class CommandLineTunnelManager implements TunnelManager {

	private Map<String, Process> processMap = 
		new HashMap<String, Process>();

	private static CommandLineTunnelManager commandLineTunnelManager = null;
	private static Logger LOG = Logger.getLogger(CommandLineTunnelManager.class);

	private CommandLineTunnelManager() {		
	}

	/**
	 * Creates a new instance of HGTunnel 
	 */
	public synchronized static CommandLineTunnelManager getInstance() {
		if (commandLineTunnelManager == null) {
			commandLineTunnelManager = new CommandLineTunnelManager();
		}
		return commandLineTunnelManager;
	}

	/**
	 * On the command line, authentication is done when the tunnels are built.
	 * This method always returns AuthenticationProtocolState.COMPLETE
	 * @return AuthenticationProtocolState.COMPLETE
	 */
	public int authenticate(String username, String privateKeyFile, Component parent) {
		return AuthenticationProtocolState.COMPLETE;
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#openTunnelRF(int, int, java.lang.String)
	 */
	public boolean openTunnelRF(
			final int localPort, final int remotePort, final String name ) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("creating ssh tunnel via os x cli.");
		}

		String[] sshTunnelCmd = {
				"ssh",
				"-v",
				"-g",
				"-p",
				String.valueOf(SshToolsFactory.getPort()),
				"-N",
				"-a",
				"-o",
				"UserKnownHostsFile=" + SshToolsFactory.getPreferredKeyManager().getUserKnownHostsFile().getAbsolutePath(),
				"-R",
				"*:" + remotePort + ":localhost:" + localPort,
				"hlpguest@" + SshToolsFactory.getHostname(),
				"-i",
				SshToolsFactory.getPreferredKeyManager().getPrivateKeyFile().getAbsolutePath()
		};

		final ProcessMonitor monitor = new ProcessMonitor("Entering interactive session.", 30000);

		try {
			if (LOG.isDebugEnabled()) {
				StringBuffer sBuf = new StringBuffer();
				for(String next : sshTunnelCmd) {
					sBuf.append(next + " ");
				}
				LOG.debug("tunnel commands: " + sBuf.toString());
			}
			final Process proc = Runtime.getRuntime().exec(sshTunnelCmd);
			processMap.put(name, proc);

			Util.logProcessInput("CommandLineTunnelManger.openTunnelRF[" + name + 
					"/rp" + remotePort + "/lp" + localPort + "]", proc, null);
			Util.logProcessError("CommandLineTunnelManger.openTunnelRF[" + name +
					"/rp" + remotePort + "/lp" + localPort + "]", proc, monitor);

			//Write the passphrase out
			proc.getOutputStream().write(
					SshToolsFactory.getPreferredKeyManager().getPassphrase().getBytes());
			proc.getOutputStream().flush();	

			if (LOG.isDebugEnabled()) {
				LOG.debug("we're going to wait.");
			}
			while (monitor.isWaiting()) {
				try {
					Thread.sleep(2000);
					if (LOG.isDebugEnabled()) {
						LOG.debug("waiting ...");
					}
				} catch (InterruptedException iex) {
					LOG.info("Sleep interrupted.", iex);
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("we're done waiting.");
			}


		} catch (IOException iox) {
			LOG.error("Unable to create ssh tunnel via cli.");
		}

		return monitor.wasStopDiscovered();
	}



	/* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#openTunnelLF(int, int, java.lang.String)
	 */
	public boolean openTunnelLF(
			final int localPort, final int remotePort, final String name) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("creating ssh tunnel via os x cli.");
		}
		String[] sshTunnelCmd = {
				"ssh",
				"-v",
				"-g",
				"-p",
				String.valueOf(SshToolsFactory.getPort()),
				"-N",
				"-a",
				"-o",
				"UserKnownHostsFile=" + SshToolsFactory.getPreferredKeyManager().getUserKnownHostsFile().getAbsolutePath(),
				"-L",
				"localhost:" + remotePort + ":*:" + localPort,
				"hlpguest@" + SshToolsFactory.getHostname(),
				"-i",
				SshToolsFactory.getPreferredKeyManager().getPrivateKeyFile().getAbsolutePath()
		};

		final ProcessMonitor monitor = new ProcessMonitor("Entering interactive session.", 30000);

		try {
			if (LOG.isDebugEnabled()) {
				StringBuffer sBuf = new StringBuffer();
				for(String next : sshTunnelCmd) {
					sBuf.append(next + " ");
				}
				LOG.debug("tunnel commands: " + sBuf.toString());
			}
			final Process proc = Runtime.getRuntime().exec(sshTunnelCmd);
			processMap.put(name, proc);

			Util.logProcessInput("CommandLineTunnelManger.openTunnelLF[" + name +
					"/rp" + remotePort + "/lp" + localPort + "]", proc, null);
			Util.logProcessError("CommandLineTunnelManger.openTunnelLF[" + name +
					"/rp" + remotePort + "/lp" + localPort + "]", proc, monitor);

			//Write the passphrase out
			proc.getOutputStream().write(
					SshToolsFactory.getPreferredKeyManager().getPassphrase().getBytes());
			proc.getOutputStream().flush();
			if (LOG.isDebugEnabled()) {
				LOG.debug("Preparing to poll monitor.");
			}

			while (monitor.isWaiting()) {
				try {
					Thread.sleep(2000);
					if (LOG.isDebugEnabled()) {
						LOG.debug("waiting ...");
					}
				} catch (InterruptedException iex) {
					LOG.info("Sleep interrupted.", iex);
				}
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("Finished polling monitor.");
			}

		} catch (IOException iox) {
			LOG.error("Unable to create ssh tunnel via cli.");
		}

		return monitor.wasStopDiscovered();
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#closeTunnelLF(java.lang.String)
	 */
	public void closeTunnelLF(final String name) {
		closeTunnel(name);
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#closeTunnelRF(java.lang.String)
	 */
	public void closeTunnelRF(final String name) {
		closeTunnel(name);
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#closeTunnel(java.lang.String)
	 */
	public void closeTunnel(final String name) {
		Process proc = processMap.get(name);
		if (proc != null) {
			proc.destroy();
		}
		processMap.remove(name);
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#disconnect()
	 */
	public void disconnect() {        
		//close all open tunnels
		for (String key : processMap.keySet()) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("cleaning up process: " + key);
			}
			Process proc = (Process) processMap.get(key);
			if (proc != null) {
				proc.destroy();
			}
		}
		processMap.clear();
	}

	/* (non-Javadoc)
	 * @see com.helpguest.ssh.TunnelManager#waitForDisconnectState()
	 */
	public void waitForDisconnectState() {
		//no-op
	}


	public String getHostname() {
		return SshToolsFactory.getHostname();
	}


	public int getPort() {
		return SshToolsFactory.getPort();
	}
}
