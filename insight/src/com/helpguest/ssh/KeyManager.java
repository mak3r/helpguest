package com.helpguest.ssh;

import java.io.File;

public interface KeyManager {

	public abstract void readProperties() throws IllegalStateException;

	public abstract void writeProperties() throws IllegalStateException;

	public abstract void sendPubKey() throws IllegalStateException;
	
	public abstract String getOpenKeyContent() throws IllegalStateException;
	
	public abstract void generateDSAKeys();

	public abstract void generateRSAKeys();

	public abstract File getPrivateKeyFile();
	
	public abstract File getPublicKeyFile();
	
	public abstract File getOpenKeyFile();

	public abstract String getUsername();
	
	public abstract String getPassphrase();
	
	public abstract File getUserKnownHostsFile();

}