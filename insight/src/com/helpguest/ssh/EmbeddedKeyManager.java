/**
 * KeyManager.java
 *
 *
 * Created: Fri May 23 21:10:45 2003
 *
 * @author <a href="mailto:mabrams@KeyManager.com">mabrams</a>
 * @version 1.0
 * HelpGuest Technologies, Inc. Copyright 2005 
 */
package com.helpguest.ssh;

import java.io.File;
import java.io.FileWriter;

import org.apache.log4j.Logger;

import com.sshtools.j2ssh.transport.publickey.OpenSSHPublicKeyFormat;
import com.sshtools.j2ssh.transport.publickey.SshKeyGenerator;


public class EmbeddedKeyManager extends KeyManagerAdapter implements KeyManager {
    
	private static final Logger LOG = Logger.getLogger(EmbeddedKeyManager.class);

	private static EmbeddedKeyManager embeddedKeyManager = null;
	
    private EmbeddedKeyManager() {
    	super();
    } // EmbeddedKeyManager constructor
    
    protected static EmbeddedKeyManager getInstance() {
    	if (embeddedKeyManager == null) {
    		embeddedKeyManager = new EmbeddedKeyManager();
    	}
    	return embeddedKeyManager;
    }

    public void generateDSAKeys() {
        SshKeyGenerator keyGenerator = new SshKeyGenerator();
        try {
            keyGenerator.generateKeyPair("dsa", 1024, getPrivateKeyFile().getAbsolutePath(), username, getPassphrase());
            keyGenerator = null;
        } catch (Exception ex) {
            String msg = "Could not generate key pair.";
            LOG.error(msg, ex);
            throw new IllegalStateException(msg);
        }
    }
    
    /**
     * This method is currently unimplemented
     */
    public void generateRSAKeys() {
    	//no-op
    }
    
    public String getOpenKeyContent() throws IllegalStateException {
	    String key = null;
	    try {
	        key = SshKeyGenerator.convertPublicKeyFile(
	        		getPublicKeyFile(), new OpenSSHPublicKeyFormat()); 
	        //write an OpenSSH version pub key file.
	        File openFile = getOpenKeyFile();
	        FileWriter fw = new FileWriter(openFile);
	        fw.write(key);
	        fw.close();
	    } catch (Exception ex) {
	        String msg = "Failed to convert public key.";
	        LOG.error(msg, ex);
	        throw new IllegalStateException(msg);
	    }
	
	    return key;
	}
	
	
    
} // KeyManager
