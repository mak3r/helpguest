/*
 * HelpGuestMode.java
 *
 * Created on August 7, 2005, 9:26 PM
 */

package com.helpguest;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class HelpGuestMode {
    private String hgMode;
    public static final HelpGuestMode CLIENT = new HelpGuestMode("Client"); 
    public static final HelpGuestMode EXPERT = new HelpGuestMode("Expert");
    
    public HelpGuestMode(String mode) {
        this.hgMode = mode;
    }

    public boolean equals(HelpGuestMode helpGuestMode) {
        if (helpGuestMode.hgMode.equals(this.hgMode)) {
            return true;
        }
        return false;
    }

    public String toString() {
        return hgMode;
    }
}

