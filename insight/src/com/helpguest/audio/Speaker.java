/*
 * Speaker.java
 *
 * Created on January 29, 2005, 11:46 AM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.audio;

import java.io.IOException;
import javax.media.Manager;
import javax.media.MediaLocator;
import javax.media.NoPlayerException;
import javax.media.Player;
import javax.media.protocol.DataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author  mabrams
 */
public class Speaker {
    
    Player player = null;
    
    private Logger log = Logger.getLogger(Speaker.class);
    private final boolean DEBUG = log.isDebugEnabled();
    
    /** Creates a new instance of Speaker */
    public Speaker() {
    }
   
    public void initSpeaker(MediaLocator ml) {
        // Create a Player for the capture device: 
        try{
             if (DEBUG) {
                 log.debug("protocol:" + ml.getProtocol());
                 log.debug("remainder:" + ml.getRemainder());
             }
            player = Manager.createPlayer(ml);
        } catch (IOException e) { 
            log.error("Could not create player.", e);
        } catch (NoPlayerException e) {
            log.error("Speaker has no player.", e);
        } 
    }
    
    public Player getPlayer(MediaLocator ml) {
        if (player == null) {
            initSpeaker(ml);
        }
        return player;
    }

    public static void main( String[] args) {
        org.apache.log4j.BasicConfigurator.configure();
        // Get the CaptureDeviceInfo for the live audio capture device 
        javax.media.CaptureDeviceInfo di = new javax.media.CaptureDeviceInfo();
        java.util.Vector deviceList = javax.media.CaptureDeviceManager.getDeviceList(new javax.media.format.AudioFormat("linear", 44100, 16, 2)); 
         if (deviceList.size() > 0)
             di = (javax.media.CaptureDeviceInfo)deviceList.firstElement();
             
         else
         // Exit if we can't find a device that does linear, 44100Hz, 16 bit, stereo audio. 
             System.exit(-1); 

        final Player p = new Speaker().getPlayer(di.getLocator());
        p.addControllerListener(new javax.media.ControllerAdapter() {
            public void realizeComplete(javax.media.RealizeCompleteEvent evt) {
                System.out.println("REALIZED!");
                java.awt.Frame f = new java.awt.Frame("SampleSpeaker Player");
                f.addWindowListener( new java.awt.event.WindowAdapter() {
                    public void windowClosing( java.awt.event.WindowEvent evt) {
                        System.exit(0);
                    }
                });
                java.awt.Component comp = p.getControlPanelComponent(); 
                f.add(comp);
                f.setSize((int)comp.getSize().getWidth(), (int)comp.getSize().getHeight()+50);
                f.setVisible(true);               
            }
        });
        p.start();
        
        javax.media.Control[] controls = p.getControls();
        for (int i = 0; i < controls.length; i++) {
            System.out.println("control[" + i + "]: " + controls[i].toString());
        }
    }
}
