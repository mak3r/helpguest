/*
 * Microphone.java
 *
 * Created on January 29, 2005, 11:44 AM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.audio;

import java.io.IOException;
import java.util.Vector;
import javax.media.CaptureDeviceInfo;
import javax.media.CaptureDeviceManager;
import javax.media.ConfigureCompleteEvent;
import javax.media.ControllerAdapter;
import javax.media.DataSink;
import javax.media.IncompatibleSourceException;
import javax.media.Manager;
import javax.media.MediaLocator;
import javax.media.NoDataSourceException;
import javax.media.NoProcessorException;
import javax.media.NotRealizedError;
import javax.media.Processor;
import javax.media.RealizeCompleteEvent;
import javax.media.ResourceUnavailableException;
import javax.media.control.FormatControl;
import javax.media.control.TrackControl;
import javax.media.format.AudioFormat;
import javax.media.protocol.CaptureDevice;
import javax.media.protocol.ContentDescriptor;
import javax.media.protocol.DataSource;
import javax.media.protocol.PullDataSource;
import javax.media.rtp.RTPPushDataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author  mabrams
 */
public class Microphone {
    
    String url = null;
    CaptureDeviceInfo di = null;
    String deviceName = null;
    DataSource source = null;
    Processor processor = null;
    DataSink sink = null;
    private boolean isOSX = false;

    private StateHandler stateHandler = new StateHandler();

    private final static String NO_USABLE_DEVICE = "No usable audio device located.";
    private final static String NO_PROCESSOR = "Microphone has no Processor.";
    private final static String PROCESSOR_NOT_CONFIGURED = "Processor was not configured.";
    private final static String PROCESSOR_NOT_REALIZED = "Processor was not realized.";

    private final Logger log = Logger.getLogger(Microphone.class);
    private final boolean DEBUG = log.isDebugEnabled();
    
    /** Creates a new instance of Microphone */
    public Microphone(String ipAddress, int port) {
        if ("Mac OS X".equals(System.getProperty("os.name"))) {
            if (log.isInfoEnabled()) { log.info("OS is Mac OS X"); }
            isOSX = true;
        } else {
            if (log.isInfoEnabled()) { log.info("OS is " + System.getProperty("os.name")); }
        }
        url = "rtp://" + ipAddress + ":" + port + "/audio/1";
        initMic();
    }
    
    /**
     * @return an error code if the Microphone was not sucessfully initialized.
     * otherwise, return null.
     */
    public String initMic() {
        // First find a capture device that will capture linear audio
        AudioFormat format= new AudioFormat(AudioFormat.LINEAR);

        Vector devices = null;
        if (isOSX) {
            devices = CaptureDeviceManager.getDeviceList(null);
        } else {
            devices = CaptureDeviceManager.getDeviceList(format);
        }
        if (devices.size() > 0) {
            if (log.isDebugEnabled()) {log.debug("Number of devices: " + devices.size());}

            //Create a processor for this capturedevice 
            // log an error if we cannot create it 
            if (isOSX) {
                //force the use of javasound capture device on os x for now.
                // this is supposed to be fixed for in java 1.5 although I did not
                // find that to be the case.
                try {
		    processor = Manager.createProcessor(new MediaLocator("javasound://48000"));
		} catch (IOException iox) {
		    log.error("Could not get javasound processor.", iox);
		} catch (NoProcessorException npx) {
		    log.error("OSX error: " + NO_PROCESSOR, npx);
		} 
            } else {
                for (int i = 0; i < devices.size(); i++) {
                    try {
                        if (log.isDebugEnabled()) {log.debug("i: " + i);}
                
                        di = (CaptureDeviceInfo) devices.get(i);
                        if (log.isDebugEnabled()) {log.debug("Device name: " + di.getName());}
                        processor = Manager.createProcessor(di.getLocator());
                        break;
                    } catch (IOException e) { 
                        log.error("Could not get a processor.", e);
                    } catch (NoProcessorException e) { 
                        log.error(NO_PROCESSOR, e);
                    }
                }//end of for 
            }//end else [is !OSX]
            if (processor == null) {
                return NO_PROCESSOR;
            }
            
            // configure the processor and wait for it.
            boolean result = stateHandler.waitForState(processor, Processor.Configured);
            if (result == false) {
                return PROCESSOR_NOT_CONFIGURED;
            }
            
            //now that it is configured, we can set the content descriptor
            processor.setContentDescriptor(new ContentDescriptor( ContentDescriptor.RAW_RTP));
            TrackControl track[] = processor.getTrackControls(); 
            boolean encodingOk = false;

            // Go through the tracks and try to program one of them to
            // output gsm data. 
            for (int i = 0; i < track.length; i++) { 
                 if (DEBUG) { 
                     log.debug("track[" + i + "] format BEFORE format change: " + track[i].getFormat()); 
                 }
                 if (!encodingOk && track[i] instanceof FormatControl) { 
                     FormatControl fc = (FormatControl)track[i];
                     javax.media.Format fmt = fc.setFormat( new AudioFormat(AudioFormat.GSM_RTP,
                                                            8000, 
                                                            8, 
                                                            1)); 
                     if (fmt == null) {
                        track[i].setEnabled(false);
                        if (DEBUG) { log.debug("enabled gsm track: " + track[i].toString()); }
                     }
                     else {
                         encodingOk = true; 
                     }
                 } else { 
                     // we could not set this track to gsm, so disable it 
                     track[i].setEnabled(false); 
                     if (DEBUG) { log.debug("unable to set track to gsm. Track disabled." + track[i]); }
                 } 
                 if (DEBUG) { 
                     log.debug("track[" + i + "] format AFTER format change: " + track[i].getFormat()); 
                 }
             }

             // At this point, we have determined whether we can send out 
             // gsm data or not. 
             // realize the processor 
             if (encodingOk) { 
                 // block until realized.
                 result = stateHandler.waitForState(processor, Processor.Realized);
                 if (result == false) {
                     return PROCESSOR_NOT_REALIZED;
                 }
                 if (DEBUG) {log.debug("Processor realized!");}

                 // get the output datasource of the processor 
                 // if we fail log an error
                 DataSource ds = null;

                 try { 
                     ds = processor.getDataOutput(); 
                 } catch (NotRealizedError e) { 
                    log.error("Could not get DataSource.", e);
                    return "Could not get output DataSource.";
                 }

                 // hand this datasource to manager for creating an RTP 
                 // datasink our RTP datasimnk will multicast the audio 
                 MediaLocator m;
                 try {
                     m = new MediaLocator(url);
                     sink = Manager.createDataSink(ds, m);
                     sink.open();
                     sink.start(); 
                     if (DEBUG) {
                         log.debug("DataSink started.");
                     }
                 } catch (Exception e) {
                     log.error("Could not start DataSink.", e);
                     return "Could not start DataSink";
                 }     
             }// end of if (encodingOk)
        } //END OF if(deviceList.size() > 0)
        else {
            log.error("Microphone could not be found.");
            return NO_USABLE_DEVICE;
        }
        
        //processor started ok
        return null;
     }

    /**
     * Turns the mic on.
     * @throws ResourceUnavailableException if the mic was not 
     * sucessfully started.
     */
    public void on() throws ResourceUnavailableException {
        if (processor == null) {
            initMic();
        }
        boolean result = stateHandler.waitForState(processor, Processor.Realized);        
        if (result == true) {
            processor.start();
        } else {
            throw new ResourceUnavailableException("Processor is unavailable.  Initialize the mic first");
        }
    }

    /**
     * Turns the mic off but does not deallocate the resources.
     */
    public void off() {
        if (processor != null) {
            processor.stop();
        }
    }
    
    public void unplug() {
        synchronized (this) {
            if (processor != null) {
                processor.stop();
                processor.close();
                processor = null;
            }
            if (sink != null) {
                sink.close();
                sink = null;
            }
        }
    }
    
    public MediaLocator getMediaLocator() {
        return sink.getOutputLocator();
    }
    
    public static void main(String[] args) {
        org.apache.log4j.BasicConfigurator.configure();
        final Microphone mic = new Microphone("192.168.1.103", 22222);        
        javax.media.Player p = null;
        try {
            p = Manager.createPlayer(mic.sink.getOutputLocator());                    
            System.out.println("DataSinks MediaLocator: " + mic.sink.getOutputLocator());
            mic.on();
        } catch (java.io.IOException iox) {
            iox.printStackTrace();
            System.exit(1);
        } catch (javax.media.NoPlayerException npx) {
            npx.printStackTrace();
            System.exit(1);            
        } catch (javax.media.ResourceUnavailableException rux) {
            rux.printStackTrace();
            System.exit(1);
        }

        boolean result = new StateHandler().waitForState(p, javax.media.Player.Realized);
        if (result == false) {
            System.exit(1);
        }
        java.awt.Frame f = new java.awt.Frame("SampleMic Player");
        f.addWindowListener( new java.awt.event.WindowAdapter() {
            public void windowClosing( java.awt.event.WindowEvent evt) {
                mic.unplug();
                System.exit(0);
            }
        });
        java.awt.Component comp = p.getControlPanelComponent(); 
        f.add(comp);
        f.setSize((int)comp.getSize().getWidth(), (int)comp.getSize().getHeight() + 50);
        f.setVisible(true);
    }

    /**
     * Getter for property processor.
     * @return Value of property processor.
     */
    public javax.media.Processor getProcessor() {
        return processor;
    }

}
