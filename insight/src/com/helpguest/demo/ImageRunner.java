/*
 * ImageRunner.java
 *
 * Created on April 30, 2005, 3:25 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.demo;

import com.helpguest.insight.demo.AdHocVoice;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.Dimension;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;

/**
 *
 * @author mabrams
 */
public class ImageRunner extends Canvas {
    
    static final Logger log = Logger.getLogger("com.helpguest.demo.ImageRunner");
    private BufferedImage image;
    private static Dimension dim;
    private int imageNum = 0;
    private AdHocVoice ahv;
    private String demoAddr = "";
    
    /** Creates a new instance of ImageRunner */
    public ImageRunner(String address) {
        super();
        demoAddr = address;
        initComponents();
        loadImage(++imageNum);
    }

    private void initComponents() {
        this.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                imageRunnerMouseReleased(evt);
            }
        });
    }
    
    private void imageRunnerMouseReleased(java.awt.event.MouseEvent evt) {
        if (evt.isControlDown()) {
            Thread t = new Thread (new Runnable() {
            public void run() {
                ahv = AdHocVoice.getInstance(demoAddr);
                ahv.turnVoiceOn();
                ahv.setVisible(true);
            }
        });
        t.start();
        } else if (evt.isShiftDown()) {
            this.back();
        } else {
            this.forward();
        }
        this.repaint();
    }
    
    private boolean loadImage(int num) {
        BufferedImage prev = image;
        try {
            image = ImageIO.read(this.getClass().getResource("/com/helpguest/images/" + num + ".gif"));
        } catch (java.io.IOException iox) {
            //try to go back to the previous image
            log.error("Failed to get image.", iox);
            return false;
        } catch (IllegalArgumentException iax) {
            //try to go back to the previous image
            log.error("Failed to get image " + num + ".gif", iax);
            return false;            
        }
        dim = new Dimension(image.getWidth(), image.getHeight()+15);
        this.setSize(dim);
        return true;
    }

    public void forward() {
        if (!loadImage(++imageNum)) {
            loadImage(--imageNum);
        }
    }

    public void back() {
        if (!loadImage(--imageNum)) {
            loadImage(++imageNum);
        }
    }
    
    public void paint(Graphics g) {
      super.paint(g);
      update(g);
    }

    public void update(Graphics g) {
      Graphics2D g2 = (Graphics2D)g;
      g2.drawImage(image,0,0,this); 
    }
   
    public static void main(String[] args) {
        org.apache.log4j.BasicConfigurator.configure();
        
        java.awt.Frame f = new java.awt.Frame("ImageRunner");
        f.addWindowListener(new java.awt.event.WindowAdapter() {
           public void windowClosing(java.awt.event.WindowEvent evt) {
               System.exit(0);
           } 
        });
        ImageRunner ir = new ImageRunner("127.0.0.1");
        f.add(ir);
        f.setSize(ir.getSize());
        f.setVisible(true);

        while(true);
    }
}
