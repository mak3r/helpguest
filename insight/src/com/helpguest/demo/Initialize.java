/*
 * Initialize.java
 *
 * Created on April 27, 2005, 5:59 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.demo;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
/**
 *
 * @author mabrams
 */
public class Initialize {

    private static JFrame initFrame = new JFrame("Initialize HelpGuest Demo");
    private final static Logger log = Logger.getLogger("com.helpguest.demo.Initialize");
    private static InitPanel initPanel = new InitPanel();
    
    /** Creates a new instance of Initialize */
    public Initialize() {
    }

    private static void initDemo() {
        initFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                System.exit(0);
            };
        });
        if (log.isDebugEnabled()) { 
            log.debug("width/height: " + initPanel.getBounds().width + "/" + 
                       initPanel.getBounds().height);
        }
        initFrame.getContentPane().setLayout(new FlowLayout());
        initFrame.getContentPane().add(initPanel);
        initFrame.setSize(432, 180);//initPanel.getSize());
        com.helpguest.util.Util.centerWindow(initFrame);
        initFrame.setVisible(true);
    }
    
    public static void main(String args[]) {
        PropertyConfigurator.configure("demo.properties");
        //BasicConfigurator.configure();
        Thread t = new Thread( new Runnable() {
            public void run() {
                initDemo();
            }
        });
        t.start();
        String address = null;
        while ((address = initPanel.getAddress()) == null) {
            //poll until apps been initialized
        }
        initFrame.setVisible(false);
        initFrame = null;
    }
    

}
