/*
 * Agent.java
 *
 * Created on February 9, 2005, 4:20 PM
 * Copyright 2005 - HelpGuest Technologies, Inc.
 */

package com.helpguest;

import java.io.BufferedReader;
import java.io.File;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.service.HGPort;
import com.helpguest.service.Service;
import com.helpguest.ssh.KeyManager;
import com.helpguest.ssh.SshToolsFactory;
import com.helpguest.ssh.TunnelManager;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;

/**
 *
 * @author  mabrams
 */
public abstract class Agent {

    private enum AgentState {
        ENABLED, DISABLED;
    }
    private int servicePort = 20001;
    private int sshPort = 9022;
    //prahalad changing beta 6/4/08
    //private String hostname = "beta.helpguest.com";
    private String hostname = HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private int curService = 0;
    private Service[] services = null;
    private TunnelManager tunnelManager = null;
    protected KeyManager keyManager = null;
    private int authenticationResult = 0;
    private int maxTunnelAttempts = MAX_PORT_RETRIES;
    private InetAddress inetAddress = null;
    private AgentState state = AgentState.DISABLED;
    
    protected PrintWriter out = null;
    protected BufferedReader in = null;
    protected Collection connected = new ArrayList();
    protected Collection <HGPort> hgPorts = new ArrayList<HGPort>();
        
    //Use these constants to define what type of forwarding service is needed.
    public static final int MAX_PORT_RETRIES = 10;
    public static final int NO_PORT_FOR_SERVICE = -1;
    
    private static final Logger LOG = Logger.getLogger("com.helpguest.Agent");
    
    
    /** Creates a new instance of Agent */
    public Agent() {
    }
    
    /** get the uid of this agent*/
    public abstract String getUid();
    
    /**
     * Enable the services listed.
     * Enables services as if enable(services, 10000) was called.
     * @param services is a list of valid HelpGuest Services.
     * @return true if the services were all sucessfully enabled.
     *  false if any of the services could not be enabled.
     *  Use {#getFailed() getFailed()} to find out what
     *  could not be enabled.
     * @deprecated Use enable(Service[], StatusInterface) instead.
     */
    public boolean enable(Service[] services) {
        return enable(services, new StatusAdapter());
    }    
    
    public boolean enable(Service[] services, StatusInterface status) {
        this.services = services;
        status.updateStatus("Enabling services.", StatusAdapter.FIRST);
        if (keyManager == null) {
            status.updateStatus("Preparing your key.", status.next());
            keyManager = getKeyManager();
            try {
                //send a public key to our host using the key manager.
                //if no errors, we should be able to build tunnels.
                keyManager.sendPubKey();
                status.updateStatus("Preparing your key.", status.next());
            } catch (IllegalStateException isx) {
                LOG.error("Could not enable agent. inet address: " + 
                		(inetAddress != null ? inetAddress.getHostAddress() : "null"), isx);
                status.updateStatus("Connection failure", status.next());
                status.finish();
                return false;
            }
        }
        status.updateStatus("Communicating with HelpGuest.", status.next());
        connectToHost();
        status.updateStatus("Communicating with HelpGuest.", status.next());
        if (getServicePorts(hostname, servicePort) && hgPorts.size() > 0) {
            if (LOG.isDebugEnabled()) { LOG.debug("hgPorts.length: " + hgPorts.size()); }
            int maxTunnelAttempts = MAX_PORT_RETRIES;
            for (HGPort hgPort : hgPorts) { //int i = 0; i < hgPorts.length; i++, maxTunnelAttempts = MAX_PORT_RETRIES ) {
                status.updateStatus("Building secure connections.", status.next());
                int portStart = hgPort.getPortNumber();
                
                //Initially, try to open a tunnel using the same port number
                // that the remote client is using.
		//if a tunnel doesn't open, keep trying the next higher port until
		//port + MAX_PORT_RETRIES is reached.
                while( maxTunnelAttempts > 0 ) {
                    if (openTunnel(hgPort, portStart)) {
                        //set the port actually used on this HGPort
                        //hgPort.updatePort(portStart);
                        break;
                    } else {
                        portStart++;
                        maxTunnelAttempts--;
                    }
                }
                
                if (maxTunnelAttempts == 0) {
                    LOG.error("Could not create secure connection for '" + hgPort.getService().getName() + "' service.");
                }
            }
        } else {
        	status.finish("Connection Refused.");
        }
        tunnelManager.waitForDisconnectState();
        status.updateStatus("Connected.", status.next());
        state = AgentState.ENABLED;
        return false;        
    }
    
    private void connectToHost() {
    	try {
	        if (LOG.isDebugEnabled()) { LOG.debug( "Preparing to connect to host " + hostname + " on port " + sshPort + "."); }
	        tunnelManager = getTunnelManager();
	        if (LOG.isDebugEnabled()) {LOG.debug("host is: " + tunnelManager.toString() + " - about to authenticate host.");}
	        authenticationResult = tunnelManager.authenticate(keyManager.getUsername(), keyManager.getPrivateKeyFile().getAbsolutePath(), null);
	        if (LOG.isDebugEnabled()) { LOG.debug( "Completed connection routine to host " + hostname + " on port " + sshPort + "."); }
    	} catch (Exception ex) {
    		LOG.error("unable to connect with host.", ex);
    		authenticationResult = AuthenticationProtocolState.FAILED;
    	}
    }
    
    private boolean openTunnel(HGPort hgPort, int localPort) {
        // Evaluate the authenticationResult
        if (authenticationResult==AuthenticationProtocolState.PARTIAL) {
            if (LOG.isInfoEnabled()) {LOG.info("Further authentication requried!");}
            return false;
        } else if (authenticationResult==AuthenticationProtocolState.FAILED) {
            if (LOG.isInfoEnabled()) {LOG.info("Authentication failed!");}
            return false;
        } else if (authenticationResult==AuthenticationProtocolState.COMPLETE) {
            //return openTunnel(localPort, hgPort.getPortNumber(), hgPort.getService(), tunnelManager);
        	return openTunnel(hgPort, localPort, tunnelManager);
        }
        LOG.error("Unknown authentication state: " + authenticationResult);
        return false;
    }

    /**
     * Subclasses should use this method to open tunnels.
     * Based on the application and service type, these tunnels
     * will use either local or remote forwarding.
     */
    protected abstract boolean openTunnel(int localPort, int remotePort, Service service, TunnelManager tunnelManager);
    protected abstract boolean openTunnel(HGPort hgPort, int localPort, TunnelManager tunnelManager);
   
    protected TunnelManager getTunnelManager() {
    	return SshToolsFactory.getPreferredTunnelManager();
    }
    
    protected KeyManager getKeyManager() {
    	return SshToolsFactory.getPreferredKeyManager();
    }
    
    /**
     * Enable the service specified.
     * @param service is a valid HelpGuest Service
     * @return true if it was successfully enabled.
     *  false otherwise.
     */
    protected boolean enable(Service service) {
        int maxTunnelAttempts = MAX_PORT_RETRIES;
        HGPort hgPort = getHGPortFor(service);
        int portStart = hgPort.getPortNumber();
        while( maxTunnelAttempts > 0 ) {            
            if (openTunnel(hgPort, portStart)) {
                //set the port actually used on this HGPort
                hgPort.updatePort(portStart);
                return true;
            } else {
                portStart++;
                maxTunnelAttempts--;
            }
        }

        LOG.error("Could not enable '" + hgPort.getService().getName() + "' service.");
        return false;
    }
    
    /**
     * Disable all services currently enabled in this agent.
     * This does not disable the agent use release() to also 
     * release the resources used by this agent.
     * This method allows follow on calls to enable to succeed if 
     * the agent was enabled before this call was made.
     */
    public void disableAll() {
        if (tunnelManager != null) {
            //Copy the collection to avoid concurrent modification
            Collection<HGPort> hgp = new ArrayList<HGPort>(hgPorts);
            for (HGPort hgPort : hgp) {
                disable(hgPort.getService());
            }
        }
    }
    
    /**
     * Disable the service specified.
     * @param service is a valid HelpGuest Service
     */
    public void disable(Service service) {
        if (tunnelManager != null) {
            //Copy the collection to avoid concurrent modification
            Collection<HGPort> hgp = new ArrayList<HGPort>(hgPorts);
            for (HGPort hgPort : hgp) {
                if (service == hgPort.getService()) {
                    tunnelManager.closeTunnel(service.getName());
                    break;
                }
            }
        }
    }

    
    /**
     * Release the resources used by this agent.
     * Disconnect from the host.
     */
    public void release() {
        if (tunnelManager != null) {
            disableAll();
            hgPorts = new ArrayList<HGPort>();
            tunnelManager.disconnect();
            tunnelManager = null;
            state = AgentState.DISABLED;
        }
    }

    /**
     * Get the port for the specified service.
     * @param service is a valid HelpGuest Service
     * @return the integer value of the port this service runs on.
     */
    public int getPortFor(Service service) {
        for (HGPort hgPort : hgPorts) {
            if (hgPort.getService() == service) {
            	if (LOG.isDebugEnabled()) {
            		LOG.debug("service '" + service.getName() + "' port is '" + hgPort.getPortNumber() + "'");
            	}
                return hgPort.getPortNumber();
            }
        }
        return NO_PORT_FOR_SERVICE;
    }
    
    /**
     * Get the HGPort for this service.
     */
    public HGPort getHGPortFor(Service service) {
        for (HGPort hgPort : hgPorts) {
            if (hgPort.getService() == service) {
                return hgPort;
            }
        }
        return null;
    }
    
    /**
     * Get the host for the service specified.
     * @param service is a valid HelpGuest Service
     * @return the hostname as a String.
     */
    public String getHost(Service service) {
        return hostname;
    }


    /**
     * @return the next service in the list.
     * if the last service is reached, then
     * next service returns the first service in the list.
     */
    protected Service nextService() {
        if (services == null || services.length == 0) {
            // cannot go on
            return null;
        }

        if (curService == services.length) {
            curService = 0;
        }
        return services[curService++];
    }

    /**
     * @return true if the connection to the service host was successful.
     */
    protected abstract boolean getServicePorts(String hostname, int port);
    
    protected void setServicePort(int port) {
        this.servicePort = port;
    }
    
    /**
     * Getter for property inetAddress.
     * @return Value of property inetAddress.
     */
    public InetAddress getHostInetAddress() {
        return inetAddress;
    }    
    
    public void setHostInetAddress(InetAddress inetAddress) {
        this.inetAddress = inetAddress;
    }
    
    public boolean isEnabled() {
        return state == AgentState.ENABLED;
    }
    
    protected File getIdentity() {
    	return keyManager.getPrivateKeyFile();
    }
}
