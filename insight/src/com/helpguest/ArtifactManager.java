/**
 * The ArtifactManager provides a common api for the application
 * to manage filesystem artifacts.
 * 
 * Under the hood, this class maintains a directory for file system
 * artifacts.
 * 
 * If any artifacts are created they may be registered with the ArtifactManager 
 * which will automatically clean-up the artifacts when the java
 * Runtime exits.
 * 
 * 
 */
package com.helpguest;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * @author mabrams
 * 
 */
public class ArtifactManager {

	private static ArtifactManager artifactManager = null;
	private static Map<String,File> managedFiles = new HashMap<String,File>();
	
	private static final File HOME_DIR = 
		new File(System.getProperty("user.home") + File.separator + ".helpguest");

	private static final Logger LOG = Logger.getLogger(ArtifactManager.class);

	private ArtifactManager() {	
		if (!HOME_DIR.exists()) {
			HOME_DIR.mkdir();
		}
		
		Thread t = new Thread(new Runnable() {

			public void run() {
				for (File f : managedFiles.values()) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("removing file: " + f.getAbsolutePath());
					}
					f.delete();
				}
			}
			
		});
		Runtime.getRuntime().addShutdownHook(t);
	}
	
	/**
	 * Use this method to get the absolute path of the directory
	 * managed by the ArtifactManager
	 * 
	 * @return the absolute path of the storage directory.
	 */
	public static String getStorageDirectory() {
		if (artifactManager == null) {
			artifactManager = new ArtifactManager();
		}

		return HOME_DIR.getAbsolutePath();
	}
	
	public static File createArtifact(final String fileName, final boolean cleanUp) {
		File f = new File(getStorageDirectory() + File.separator + fileName);
		if (cleanUp) {
			removeOnExit(f);
		}
		
		return f;
	}

	/**
	 * 
	 * @param file
	 * @param cleanUp
	 */
	public static void removeOnExit(final File file) {
		if (!managedFiles.containsKey(file.getAbsolutePath())) {
			managedFiles.put(file.getAbsolutePath(), file);
		}
	}
}
