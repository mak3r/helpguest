/*
 * WebChatAgent.java
 *
 * Created on June 11, 2007, 5:49 AM
 *
 * Copyright 2007 - HelpGuest TechnoLOGies, Inc.
 * @author mabrams
 */

package com.helpguest;

import com.helpguest.protocol.Vocabulary;
import com.helpguest.service.HGPort;
import com.helpguest.service.Service;
import com.helpguest.ssh.TunnelManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.log4j.Logger;

public class WebChatAgent extends Agent {
    
    private String uid = null;
    private Collection<HGPort> portList = new ArrayList<HGPort>();
    private static final Logger LOG = Logger.getLogger("com.helpguest.WebChatAgent");
    
    /** Creates a new instance of WebChatAgent */
    public WebChatAgent(String someUid) {
    	this.uid = someUid;
    }
    
    public String getUid() {
        return uid;
    }

    protected boolean getServicePorts(String hostname, int port) {
        //This agent has only one service
        Service service = Service.WEB_CHAT;
        Socket rSocket = null;
        
        try {
            rSocket = new Socket(hostname, port);
            out = new PrintWriter(rSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(rSocket.getInputStream()));
        } catch (UnknownHostException e) {
            LOG.error("Don't know about host: " + hostname + ".", e);
            return false;
        } catch (IOException e) {
            LOG.error("Couldn't get I/O for the connection to: " + hostname + ".", e);
            return false;
        }
        
        String serverMsg = null;
        String agentMsg = null;
        
        try {
            while ((serverMsg = in.readLine()) != null) {
                if (LOG.isDebugEnabled()) {LOG.debug("server says: " + serverMsg); }
                
                if (Vocabulary.COM_BYE.equals(serverMsg)) {
                    break;
                } else if (Vocabulary.SERV_WELCOME.equals(serverMsg)) {                    
                    agentMsg = Vocabulary.COM_REQUEST;
                } else if (Vocabulary.SERV_REQUEST_TYPE.equals(serverMsg)) {
                    agentMsg = Vocabulary.TECH_REQUEST_PERMISSION;
                } else if (Vocabulary.SERV_PERMISSION_TYPE.equals(serverMsg)) {
                    agentMsg = Vocabulary.COM_CHATTERBOX;
                } else if (Vocabulary.SERV_NAME.equals(serverMsg)) {
                    agentMsg = uid;
                } else {
                	//it must be a port number
                	try {
                        int servPort = Integer.parseInt(serverMsg);
                        HGPort hgPort = new HGPort(servPort, service);
                        portList.add(hgPort);
                        //finish up
                        agentMsg = Vocabulary.COM_THANKS.toString();
                    } catch (NumberFormatException nfx) {
                        LOG.error("Server message: '" + serverMsg + "' is not recognized.  Agent aborting.");
                        return false;
                    } catch (Exception ex) {
                        LOG.error("did not complete the service acquisition process.", ex);
                    }
                }
                if (LOG.isDebugEnabled()) { LOG.debug("agent reply: " + agentMsg); }
                out.println(agentMsg);
                
            }//end of while((serverMsg = in.readLine()) != null)
        } catch (IOException iox) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Connection closed by remote host.");
            }
        } finally {
            try {
                out.close();
                in.close();
                rSocket.close();
            } catch (IOException iox) {
                LOG.error("Error closing a stream or socket.");
            }
        }
        hgPorts = new ArrayList<HGPort>(portList);
        return true;
    }

    protected boolean openTunnel(HGPort hgPort, int localPort, TunnelManager host) {
        return openTunnel(localPort, hgPort.getPortNumber(), hgPort.getService(), host);
    }

    protected boolean openTunnel(int localPort, int remotePort, Service service, TunnelManager host) {
        //The only service at this time is WEB_CHAT
        return host.openTunnelRF(localPort, remotePort, service.getName());
    }
    
}
