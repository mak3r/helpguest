/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 14, 2007
 */
package com.helpguest.webchat;

import com.helpguest.gwt.protocol.ChatterboxParty;

/**
 * @author mabrams
 *
 */
public class ExpertParty {

	public static ChatterboxParty getParty(final String expertUid, final String handle) {
		ChatterboxParty party = new ChatterboxParty();
		
		party.setExpertUid(expertUid);
		party.setHandle(handle);
		
		return party;
	}

}
