/*
 * ModePanel.java
 *
 * Created on August 7, 2005, 5:08 PM
 */

package com.helpguest.ui;

import com.helpguest.HelpGuestMode;
import com.helpguest.StatusAdapter;
import com.helpguest.StatusInterface;

/**
 *
 * @author  mabrams
 */
public class ModePanel extends javax.swing.JPanel implements StatusInterface {
    
    /** Creates new form ModePanel */
    public ModePanel() {
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        modeTextLabel = new javax.swing.JLabel(mode.toString());
        statusBar = new javax.swing.JProgressBar();
        helpGuestButton = new javax.swing.JButton();
        modeLabel = new javax.swing.JLabel();
        sealButton = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(360, 97));
        setMinimumSize(new java.awt.Dimension(360, 97));
        setPreferredSize(new java.awt.Dimension(360, 97));
        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 5);
        add(modeTextLabel, gridBagConstraints);

        statusBar.setMaximum(10);
        statusBar.setString("disconnected");
        statusBar.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(statusBar, gridBagConstraints);

        helpGuestButton.setFont(new java.awt.Font("Lucida Grande", 0, 11));
        helpGuestButton.setText("Connect to Client");
        helpGuestButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                helpGuestButtonMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        add(helpGuestButton, gridBagConstraints);

        modeLabel.setText("Mode: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        add(modeLabel, gridBagConstraints);

        sealButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/helpguest/images/seal-orange-57px.png"))); // NOI18N
        sealButton.setBorderPainted(false);
        sealButton.setFocusable(false);
        sealButton.setMargin(new java.awt.Insets(7, 7, 7, 7));
        sealButton.setPreferredSize(new java.awt.Dimension(57, 57));
        sealButton.setRequestFocusEnabled(false);
        sealButton.setVerifyInputWhenFocusTarget(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        add(sealButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void helpGuestButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_helpGuestButtonMouseClicked
        statusBar.setIndeterminate(true);
        this.fireMouseListenerMouseClicked(evt);
    }//GEN-LAST:event_helpGuestButtonMouseClicked
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton helpGuestButton;
    private javax.swing.JLabel modeLabel;
    private javax.swing.JLabel modeTextLabel;
    private javax.swing.JButton sealButton;
    private javax.swing.JProgressBar statusBar;
    // End of variables declaration//GEN-END:variables

    private int currentProgress;
    public static final HelpGuestMode NONE = new HelpGuestMode("None");
        
        
    /**
     * Holds value of property mode.
     */
    private HelpGuestMode mode = NONE;

    /**
     * Getter for property mode.
     * @return Value of property mode.
     */
    public HelpGuestMode getMode() {
        
        return this.mode;
    }

    /**
     * Setter for property mode.
     * @param mode New value of property mode.
     * 
     * @throws PropertyVetoException if some vetoable listeners reject the new value
     */
    public void setMode(HelpGuestMode mode) throws java.beans.PropertyVetoException {
        this.mode = mode;
        modeTextLabel.setText(mode.toString());
    }

    /**
     * Utility field used by event firing mechanism.
     */
    private javax.swing.event.EventListenerList listenerList =  null;

    /**
     * Registers MouseListener to receive events.
     * @param listener The listener to register.
     */
    public synchronized void addMouseListener(java.awt.event.MouseListener listener) {

        if (listenerList == null ) {
            listenerList = new javax.swing.event.EventListenerList();
        }
        listenerList.add (java.awt.event.MouseListener.class, listener);
    }

    /**
     * Removes MouseListener from the list of listeners.
     * @param listener The listener to remove.
     */
    public synchronized void removeMouseListener(java.awt.event.MouseListener listener) {

        listenerList.remove (java.awt.event.MouseListener.class, listener);
    }

    /**
     * Notifies all registered listeners about the event.
     * 
     * @param event The event to be fired
     */
    private void fireMouseListenerMouseClicked(java.awt.event.MouseEvent event) {

        if (listenerList == null) return;
        Object[] listeners = listenerList.getListenerList ();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i]==java.awt.event.MouseListener.class) {
                ((java.awt.event.MouseListener)listeners[i+1]).mouseClicked (event);
            }
        }
    }

    /**
     * Notifies all registered listeners about the event.
     * 
     * @param event The event to be fired
     */
    private void fireMouseListenerMousePressed(java.awt.event.MouseEvent event) {

        if (listenerList == null) return;
        Object[] listeners = listenerList.getListenerList ();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i]==java.awt.event.MouseListener.class) {
                ((java.awt.event.MouseListener)listeners[i+1]).mousePressed (event);
            }
        }
    }

    /**
     * Notifies all registered listeners about the event.
     * 
     * @param event The event to be fired
     */
    private void fireMouseListenerMouseReleased(java.awt.event.MouseEvent event) {

        if (listenerList == null) return;
        Object[] listeners = listenerList.getListenerList ();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i]==java.awt.event.MouseListener.class) {
                ((java.awt.event.MouseListener)listeners[i+1]).mouseReleased (event);
            }
        }
    }

    /**
     * Notifies all registered listeners about the event.
     * 
     * @param event The event to be fired
     */
    private void fireMouseListenerMouseEntered(java.awt.event.MouseEvent event) {

        if (listenerList == null) return;
        Object[] listeners = listenerList.getListenerList ();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i]==java.awt.event.MouseListener.class) {
                ((java.awt.event.MouseListener)listeners[i+1]).mouseEntered (event);
            }
        }
    }

    /**
     * Notifies all registered listeners about the event.
     * 
     * @param event The event to be fired
     */
    private void fireMouseListenerMouseExited(java.awt.event.MouseEvent event) {

        if (listenerList == null) return;
        Object[] listeners = listenerList.getListenerList ();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i]==java.awt.event.MouseListener.class) {
                ((java.awt.event.MouseListener)listeners[i+1]).mouseExited (event);
            }
        }
    }

    public void updateStatus(String status, int amt) {
        statusBar.setString(status);
        if (amt == StatusAdapter.COMPLETE) {
            statusBar.setValue(0);
        } else {
            statusBar.setValue(amt);
        }
        currentProgress = amt;
    }
    
    public int next() {
        return currentProgress + 1;
    }
    
    /**
     * Finish with a message.
     */
    public void finish(String message) {
        statusBar.setString(message);
        statusBar.setValue(0);
        statusBar.setIndeterminate(false);        
    }
    
    public void finish() {
        finish("");
    }
}
