/**
 * Implements a JPanel that is used to capture keyboard events
 * when an app is started.
 * 
 * This panel does not have an actual visual interface.
 * 
 * To accept keyboard events and to allow debugging,
 * create an instance of this JPanel, setRequestFocusEnabled(true), requestFocus(),
 * wait for key events.
 */
package com.helpguest.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.net.SocketAppender;

import com.helpguest.HGServiceProperties;
import com.helpguest.HGServiceProperties.HGServiceProperty;

/**
 * 
 * @author mabrams
 *
 */
public class DebugPanel extends JPanel {

	Timer t = new Timer();
	JProgressBar progressBar = new JProgressBar();
	JPanel innerPanel = new JPanel(new GridLayout(2,1));
	
	boolean finished = false;
	public DebugPanel() {
		setLayout(new BorderLayout());
		
		innerPanel.add(new JLabel("Connecting with HelpGuest security system."));
		innerPanel.add(progressBar);
		innerPanel.setSize(100,50);
		progressBar.setIndeterminate(true);
		add(new JPanel(), BorderLayout.NORTH);
		add(new JPanel(), BorderLayout.WEST);
		add(innerPanel, BorderLayout.CENTER);
		add(new JPanel(), BorderLayout.EAST);
		add(new JPanel(), BorderLayout.SOUTH);
		
		
		setFocusable(true);
        requestFocus();
        addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e) {
				int alt = 256;
				System.out.println("modifiers ex: " + e.getModifiersEx() + " key char: " + e.getKeyChar());
				if ( (e.getModifiersEx() == alt && e.getKeyChar() == 'l') ||
					 e.getKeyChar() == 'd') {
					stopTimer();
    				int choice = JOptionPane.showConfirmDialog(
    						null, 
    						new JLabel("Do you want to enable local debugging."), 
    						"HelpGuest Debug Mode?", 
    						JOptionPane.YES_NO_OPTION);
    				if (choice == JOptionPane.YES_OPTION) {
    					enableLocalDebug();
    				} else {
    					enableRemoteDebug();
    				}
    				setTimerComplete();
				}
			}
    		
    	});
		TimerTask scroll = new TimerTask() {

			@Override
			public void run() {
				//if the timer expired, try to enable remote debugging
				enableRemoteDebug();
				finished = true;
			}
			
		};
		t.schedule(scroll, 7000);
	}
	
    public void enableLocalDebug() {
    	JFileChooser localDebugLocation = new JFileChooser();
    	int doSave = localDebugLocation.showSaveDialog(this);
    	if (doSave == JFileChooser.APPROVE_OPTION) {
    		try {
    			String logPattern = "%-5p %d{HH:mm:ss,SSS} [%-80c] - %m %n";
	    		BasicConfigurator.configure(
	    				new FileAppender(new PatternLayout(logPattern), 
	    				localDebugLocation.getSelectedFile().getAbsolutePath(), 
	    				false));
	    	} catch (IOException x) {
	    		System.out.println("failed to configure file appender");
	    	}
    	}
    }
    
    public void enableRemoteDebug() {
    	BasicConfigurator.configure(new SocketAppender(HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey()), 8888));
    }
	
    private void stopTimer() {
    	t.cancel();
    	finished = false;
    }
    
    private void setTimerComplete() {
    	finished = true;
    }
    
    /**
     * This method can be used as the while condition
     * for a synchronized wait loop.
     * @return the state of the timer.
     */
    public boolean timerComplete() {
    	return finished;
    }
}
