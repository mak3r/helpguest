/*
 * ExpertClient.java
 *
 * Created on December 27, 2006, 8:21 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.helpguest.ui;

import java.io.PrintWriter;

/**
 *
 * @author mabrams
 */
public interface ExpertClient {
    public PrintWriter getPrintWriter();
}
