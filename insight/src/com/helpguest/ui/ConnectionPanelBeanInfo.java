/*
 * ConnectionPanelBeanInfo.java
 *
 * Created on November 19, 2005, 9:55 PM
 */

package com.helpguest.ui;

import java.beans.*;

/**
 * @author mabrams
 */
public class ConnectionPanelBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( com.helpguest.ui.ConnectionPanel.class , null );//GEN-HEADEREND:BeanDescriptor
        
        // Here you can add code for customizing the BeanDescriptor.
        
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_border = 8;
    private static final int PROPERTY_bounds = 9;
    private static final int PROPERTY_colorModel = 10;
    private static final int PROPERTY_component = 11;
    private static final int PROPERTY_componentCount = 12;
    private static final int PROPERTY_componentListeners = 13;
    private static final int PROPERTY_componentOrientation = 14;
    private static final int PROPERTY_componentPopupMenu = 15;
    private static final int PROPERTY_components = 16;
    private static final int PROPERTY_containerListeners = 17;
    private static final int PROPERTY_cursor = 18;
    private static final int PROPERTY_cursorSet = 19;
    private static final int PROPERTY_debugGraphicsOptions = 20;
    private static final int PROPERTY_displayable = 21;
    private static final int PROPERTY_doubleBuffered = 22;
    private static final int PROPERTY_dropTarget = 23;
    private static final int PROPERTY_enabled = 24;
    private static final int PROPERTY_focusable = 25;
    private static final int PROPERTY_focusCycleRoot = 26;
    private static final int PROPERTY_focusCycleRootAncestor = 27;
    private static final int PROPERTY_focusListeners = 28;
    private static final int PROPERTY_focusOwner = 29;
    private static final int PROPERTY_focusTraversable = 30;
    private static final int PROPERTY_focusTraversalKeys = 31;
    private static final int PROPERTY_focusTraversalKeysEnabled = 32;
    private static final int PROPERTY_focusTraversalPolicy = 33;
    private static final int PROPERTY_focusTraversalPolicyProvider = 34;
    private static final int PROPERTY_focusTraversalPolicySet = 35;
    private static final int PROPERTY_font = 36;
    private static final int PROPERTY_fontSet = 37;
    private static final int PROPERTY_foreground = 38;
    private static final int PROPERTY_foregroundSet = 39;
    private static final int PROPERTY_graphics = 40;
    private static final int PROPERTY_graphicsConfiguration = 41;
    private static final int PROPERTY_height = 42;
    private static final int PROPERTY_hierarchyBoundsListeners = 43;
    private static final int PROPERTY_hierarchyListeners = 44;
    private static final int PROPERTY_ignoreRepaint = 45;
    private static final int PROPERTY_inheritsPopupMenu = 46;
    private static final int PROPERTY_inputContext = 47;
    private static final int PROPERTY_inputMap = 48;
    private static final int PROPERTY_inputMethodListeners = 49;
    private static final int PROPERTY_inputMethodRequests = 50;
    private static final int PROPERTY_inputVerifier = 51;
    private static final int PROPERTY_insets = 52;
    private static final int PROPERTY_keyListeners = 53;
    private static final int PROPERTY_layout = 54;
    private static final int PROPERTY_lightweight = 55;
    private static final int PROPERTY_locale = 56;
    private static final int PROPERTY_location = 57;
    private static final int PROPERTY_locationOnScreen = 58;
    private static final int PROPERTY_managingFocus = 59;
    private static final int PROPERTY_maximumSize = 60;
    private static final int PROPERTY_maximumSizeSet = 61;
    private static final int PROPERTY_minimumSize = 62;
    private static final int PROPERTY_minimumSizeSet = 63;
    private static final int PROPERTY_mouseListeners = 64;
    private static final int PROPERTY_mouseMotionListeners = 65;
    private static final int PROPERTY_mousePosition = 66;
    private static final int PROPERTY_mouseWheelListeners = 67;
    private static final int PROPERTY_name = 68;
    private static final int PROPERTY_nextFocusableComponent = 69;
    private static final int PROPERTY_opaque = 70;
    private static final int PROPERTY_optimizedDrawingEnabled = 71;
    private static final int PROPERTY_paintingTile = 72;
    private static final int PROPERTY_parent = 73;
    private static final int PROPERTY_peer = 74;
    private static final int PROPERTY_preferredSize = 75;
    private static final int PROPERTY_preferredSizeSet = 76;
    private static final int PROPERTY_propertyChangeListeners = 77;
    private static final int PROPERTY_registeredKeyStrokes = 78;
    private static final int PROPERTY_requestFocusEnabled = 79;
    private static final int PROPERTY_rootPane = 80;
    private static final int PROPERTY_showing = 81;
    private static final int PROPERTY_size = 82;
    private static final int PROPERTY_toolkit = 83;
    private static final int PROPERTY_toolTipText = 84;
    private static final int PROPERTY_topLevelAncestor = 85;
    private static final int PROPERTY_transferHandler = 86;
    private static final int PROPERTY_treeLock = 87;
    private static final int PROPERTY_UI = 88;
    private static final int PROPERTY_UIClassID = 89;
    private static final int PROPERTY_valid = 90;
    private static final int PROPERTY_validateRoot = 91;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 92;
    private static final int PROPERTY_vetoableChangeListeners = 93;
    private static final int PROPERTY_visible = 94;
    private static final int PROPERTY_visibleRect = 95;
    private static final int PROPERTY_width = 96;
    private static final int PROPERTY_x = 97;
    private static final int PROPERTY_y = 98;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[99];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", com.helpguest.ui.ConnectionPanel.class, "getAccessibleContext", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", com.helpguest.ui.ConnectionPanel.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", com.helpguest.ui.ConnectionPanel.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", com.helpguest.ui.ConnectionPanel.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", com.helpguest.ui.ConnectionPanel.class, "getAncestorListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", com.helpguest.ui.ConnectionPanel.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", com.helpguest.ui.ConnectionPanel.class, "getBackground", "setBackground" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", com.helpguest.ui.ConnectionPanel.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", com.helpguest.ui.ConnectionPanel.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", com.helpguest.ui.ConnectionPanel.class, "getBounds", "setBounds" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", com.helpguest.ui.ConnectionPanel.class, "getColorModel", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", com.helpguest.ui.ConnectionPanel.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", com.helpguest.ui.ConnectionPanel.class, "getComponentCount", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", com.helpguest.ui.ConnectionPanel.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", com.helpguest.ui.ConnectionPanel.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", com.helpguest.ui.ConnectionPanel.class, "getComponentPopupMenu", "setComponentPopupMenu" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", com.helpguest.ui.ConnectionPanel.class, "getComponents", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", com.helpguest.ui.ConnectionPanel.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", com.helpguest.ui.ConnectionPanel.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", com.helpguest.ui.ConnectionPanel.class, "isCursorSet", null );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", com.helpguest.ui.ConnectionPanel.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", com.helpguest.ui.ConnectionPanel.class, "isDisplayable", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", com.helpguest.ui.ConnectionPanel.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", com.helpguest.ui.ConnectionPanel.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", com.helpguest.ui.ConnectionPanel.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", com.helpguest.ui.ConnectionPanel.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", com.helpguest.ui.ConnectionPanel.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", com.helpguest.ui.ConnectionPanel.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", com.helpguest.ui.ConnectionPanel.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", com.helpguest.ui.ConnectionPanel.class, "isFocusOwner", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", com.helpguest.ui.ConnectionPanel.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", com.helpguest.ui.ConnectionPanel.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", com.helpguest.ui.ConnectionPanel.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", com.helpguest.ui.ConnectionPanel.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", com.helpguest.ui.ConnectionPanel.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", com.helpguest.ui.ConnectionPanel.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", com.helpguest.ui.ConnectionPanel.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", com.helpguest.ui.ConnectionPanel.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", com.helpguest.ui.ConnectionPanel.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", com.helpguest.ui.ConnectionPanel.class, "isForegroundSet", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", com.helpguest.ui.ConnectionPanel.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", com.helpguest.ui.ConnectionPanel.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", com.helpguest.ui.ConnectionPanel.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", com.helpguest.ui.ConnectionPanel.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", com.helpguest.ui.ConnectionPanel.class, "getHierarchyListeners", null );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", com.helpguest.ui.ConnectionPanel.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", com.helpguest.ui.ConnectionPanel.class, "getInheritsPopupMenu", "setInheritsPopupMenu" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", com.helpguest.ui.ConnectionPanel.class, "getInputContext", null );
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", com.helpguest.ui.ConnectionPanel.class, "getInputMap", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", com.helpguest.ui.ConnectionPanel.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", com.helpguest.ui.ConnectionPanel.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", com.helpguest.ui.ConnectionPanel.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", com.helpguest.ui.ConnectionPanel.class, "getInsets", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", com.helpguest.ui.ConnectionPanel.class, "getKeyListeners", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", com.helpguest.ui.ConnectionPanel.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", com.helpguest.ui.ConnectionPanel.class, "isLightweight", null );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", com.helpguest.ui.ConnectionPanel.class, "getLocale", "setLocale" );
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", com.helpguest.ui.ConnectionPanel.class, "getLocation", "setLocation" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", com.helpguest.ui.ConnectionPanel.class, "getLocationOnScreen", null );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", com.helpguest.ui.ConnectionPanel.class, "isManagingFocus", null );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", com.helpguest.ui.ConnectionPanel.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", com.helpguest.ui.ConnectionPanel.class, "isMaximumSizeSet", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", com.helpguest.ui.ConnectionPanel.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", com.helpguest.ui.ConnectionPanel.class, "isMinimumSizeSet", null );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", com.helpguest.ui.ConnectionPanel.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", com.helpguest.ui.ConnectionPanel.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", com.helpguest.ui.ConnectionPanel.class, "getMousePosition", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", com.helpguest.ui.ConnectionPanel.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", com.helpguest.ui.ConnectionPanel.class, "getName", "setName" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", com.helpguest.ui.ConnectionPanel.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", com.helpguest.ui.ConnectionPanel.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", com.helpguest.ui.ConnectionPanel.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", com.helpguest.ui.ConnectionPanel.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", com.helpguest.ui.ConnectionPanel.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", com.helpguest.ui.ConnectionPanel.class, "getPeer", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", com.helpguest.ui.ConnectionPanel.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", com.helpguest.ui.ConnectionPanel.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", com.helpguest.ui.ConnectionPanel.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", com.helpguest.ui.ConnectionPanel.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", com.helpguest.ui.ConnectionPanel.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", com.helpguest.ui.ConnectionPanel.class, "getRootPane", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", com.helpguest.ui.ConnectionPanel.class, "isShowing", null );
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", com.helpguest.ui.ConnectionPanel.class, "getSize", "setSize" );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", com.helpguest.ui.ConnectionPanel.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", com.helpguest.ui.ConnectionPanel.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", com.helpguest.ui.ConnectionPanel.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", com.helpguest.ui.ConnectionPanel.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", com.helpguest.ui.ConnectionPanel.class, "getTreeLock", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", com.helpguest.ui.ConnectionPanel.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", com.helpguest.ui.ConnectionPanel.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", com.helpguest.ui.ConnectionPanel.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", com.helpguest.ui.ConnectionPanel.class, "isValidateRoot", null );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", com.helpguest.ui.ConnectionPanel.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", com.helpguest.ui.ConnectionPanel.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", com.helpguest.ui.ConnectionPanel.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", com.helpguest.ui.ConnectionPanel.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", com.helpguest.ui.ConnectionPanel.class, "getWidth", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", com.helpguest.ui.ConnectionPanel.class, "getX", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", com.helpguest.ui.ConnectionPanel.class, "getY", null );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
        
        // Here you can add code for customizing the properties array.
        
        return properties;         }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( com.helpguest.ui.ConnectionPanel.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
        
        // Here you can add code for customizing the event sets array.
        
        return eventSets;         }//GEN-LAST:Events
    
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_applyComponentOrientation4 = 4;
    private static final int METHOD_areFocusTraversalKeysSet5 = 5;
    private static final int METHOD_bounds6 = 6;
    private static final int METHOD_checkImage7 = 7;
    private static final int METHOD_computeVisibleRect8 = 8;
    private static final int METHOD_contains9 = 9;
    private static final int METHOD_countComponents10 = 10;
    private static final int METHOD_createImage11 = 11;
    private static final int METHOD_createToolTip12 = 12;
    private static final int METHOD_createVolatileImage13 = 13;
    private static final int METHOD_deliverEvent14 = 14;
    private static final int METHOD_disable15 = 15;
    private static final int METHOD_dispatchEvent16 = 16;
    private static final int METHOD_doLayout17 = 17;
    private static final int METHOD_enableInputMethods18 = 18;
    private static final int METHOD_findComponentAt19 = 19;
    private static final int METHOD_firePropertyChange20 = 20;
    private static final int METHOD_getActionForKeyStroke21 = 21;
    private static final int METHOD_getBounds22 = 22;
    private static final int METHOD_getClientProperty23 = 23;
    private static final int METHOD_getComponentAt24 = 24;
    private static final int METHOD_getComponentZOrder25 = 25;
    private static final int METHOD_getConditionForKeyStroke26 = 26;
    private static final int METHOD_getDefaultLocale27 = 27;
    private static final int METHOD_getFontMetrics28 = 28;
    private static final int METHOD_getInsets29 = 29;
    private static final int METHOD_getListeners30 = 30;
    private static final int METHOD_getLocation31 = 31;
    private static final int METHOD_getMousePosition32 = 32;
    private static final int METHOD_getPopupLocation33 = 33;
    private static final int METHOD_getPropertyChangeListeners34 = 34;
    private static final int METHOD_getSize35 = 35;
    private static final int METHOD_getToolTipLocation36 = 36;
    private static final int METHOD_getToolTipText37 = 37;
    private static final int METHOD_gotFocus38 = 38;
    private static final int METHOD_grabFocus39 = 39;
    private static final int METHOD_handleEvent40 = 40;
    private static final int METHOD_hasFocus41 = 41;
    private static final int METHOD_hide42 = 42;
    private static final int METHOD_imageUpdate43 = 43;
    private static final int METHOD_insets44 = 44;
    private static final int METHOD_inside45 = 45;
    private static final int METHOD_invalidate46 = 46;
    private static final int METHOD_isAncestorOf47 = 47;
    private static final int METHOD_isFocusCycleRoot48 = 48;
    private static final int METHOD_isLightweightComponent49 = 49;
    private static final int METHOD_keyDown50 = 50;
    private static final int METHOD_keyUp51 = 51;
    private static final int METHOD_layout52 = 52;
    private static final int METHOD_list53 = 53;
    private static final int METHOD_locate54 = 54;
    private static final int METHOD_location55 = 55;
    private static final int METHOD_lostFocus56 = 56;
    private static final int METHOD_minimumSize57 = 57;
    private static final int METHOD_mouseDown58 = 58;
    private static final int METHOD_mouseDrag59 = 59;
    private static final int METHOD_mouseEnter60 = 60;
    private static final int METHOD_mouseExit61 = 61;
    private static final int METHOD_mouseMove62 = 62;
    private static final int METHOD_mouseUp63 = 63;
    private static final int METHOD_move64 = 64;
    private static final int METHOD_nextFocus65 = 65;
    private static final int METHOD_paint66 = 66;
    private static final int METHOD_paintAll67 = 67;
    private static final int METHOD_paintComponents68 = 68;
    private static final int METHOD_paintImmediately69 = 69;
    private static final int METHOD_postEvent70 = 70;
    private static final int METHOD_preferredSize71 = 71;
    private static final int METHOD_prepareImage72 = 72;
    private static final int METHOD_print73 = 73;
    private static final int METHOD_printAll74 = 74;
    private static final int METHOD_printComponents75 = 75;
    private static final int METHOD_putClientProperty76 = 76;
    private static final int METHOD_registerKeyboardAction77 = 77;
    private static final int METHOD_remove78 = 78;
    private static final int METHOD_removeAll79 = 79;
    private static final int METHOD_removeNotify80 = 80;
    private static final int METHOD_removePropertyChangeListener81 = 81;
    private static final int METHOD_repaint82 = 82;
    private static final int METHOD_requestDefaultFocus83 = 83;
    private static final int METHOD_requestFocus84 = 84;
    private static final int METHOD_requestFocusInWindow85 = 85;
    private static final int METHOD_resetKeyboardActions86 = 86;
    private static final int METHOD_reshape87 = 87;
    private static final int METHOD_resize88 = 88;
    private static final int METHOD_revalidate89 = 89;
    private static final int METHOD_scrollRectToVisible90 = 90;
    private static final int METHOD_setBounds91 = 91;
    private static final int METHOD_setComponentZOrder92 = 92;
    private static final int METHOD_setDefaultLocale93 = 93;
    private static final int METHOD_show94 = 94;
    private static final int METHOD_size95 = 95;
    private static final int METHOD_toString96 = 96;
    private static final int METHOD_transferFocus97 = 97;
    private static final int METHOD_transferFocusBackward98 = 98;
    private static final int METHOD_transferFocusDownCycle99 = 99;
    private static final int METHOD_transferFocusUpCycle100 = 100;
    private static final int METHOD_unregisterKeyboardAction101 = 101;
    private static final int METHOD_update102 = 102;
    private static final int METHOD_updateUI103 = 103;
    private static final int METHOD_validate104 = 104;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[105];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation4] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation4].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet5] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet5].setDisplayName ( "" );
            methods[METHOD_bounds6] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds6].setDisplayName ( "" );
            methods[METHOD_checkImage7] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage7].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect8] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect8].setDisplayName ( "" );
            methods[METHOD_contains9] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains9].setDisplayName ( "" );
            methods[METHOD_countComponents10] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents10].setDisplayName ( "" );
            methods[METHOD_createImage11] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage11].setDisplayName ( "" );
            methods[METHOD_createToolTip12] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip12].setDisplayName ( "" );
            methods[METHOD_createVolatileImage13] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage13].setDisplayName ( "" );
            methods[METHOD_deliverEvent14] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent14].setDisplayName ( "" );
            methods[METHOD_disable15] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable15].setDisplayName ( "" );
            methods[METHOD_dispatchEvent16] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent16].setDisplayName ( "" );
            methods[METHOD_doLayout17] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout17].setDisplayName ( "" );
            methods[METHOD_enableInputMethods18] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods18].setDisplayName ( "" );
            methods[METHOD_findComponentAt19] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt19].setDisplayName ( "" );
            methods[METHOD_firePropertyChange20] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange20].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke21] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke21].setDisplayName ( "" );
            methods[METHOD_getBounds22] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds22].setDisplayName ( "" );
            methods[METHOD_getClientProperty23] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty23].setDisplayName ( "" );
            methods[METHOD_getComponentAt24] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt24].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder25] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class}));
            methods[METHOD_getComponentZOrder25].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke26] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke26].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale27] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale27].setDisplayName ( "" );
            methods[METHOD_getFontMetrics28] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics28].setDisplayName ( "" );
            methods[METHOD_getInsets29] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets29].setDisplayName ( "" );
            methods[METHOD_getListeners30] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners30].setDisplayName ( "" );
            methods[METHOD_getLocation31] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation31].setDisplayName ( "" );
            methods[METHOD_getMousePosition32] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getMousePosition", new Class[] {Boolean.TYPE}));
            methods[METHOD_getMousePosition32].setDisplayName ( "" );
            methods[METHOD_getPopupLocation33] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getPopupLocation33].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners34] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners34].setDisplayName ( "" );
            methods[METHOD_getSize35] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize35].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation36] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation36].setDisplayName ( "" );
            methods[METHOD_getToolTipText37] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText37].setDisplayName ( "" );
            methods[METHOD_gotFocus38] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus38].setDisplayName ( "" );
            methods[METHOD_grabFocus39] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus39].setDisplayName ( "" );
            methods[METHOD_handleEvent40] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent40].setDisplayName ( "" );
            methods[METHOD_hasFocus41] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus41].setDisplayName ( "" );
            methods[METHOD_hide42] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide42].setDisplayName ( "" );
            methods[METHOD_imageUpdate43] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate43].setDisplayName ( "" );
            methods[METHOD_insets44] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets44].setDisplayName ( "" );
            methods[METHOD_inside45] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside45].setDisplayName ( "" );
            methods[METHOD_invalidate46] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate46].setDisplayName ( "" );
            methods[METHOD_isAncestorOf47] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf47].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot48] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot48].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent49] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent49].setDisplayName ( "" );
            methods[METHOD_keyDown50] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown50].setDisplayName ( "" );
            methods[METHOD_keyUp51] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp51].setDisplayName ( "" );
            methods[METHOD_layout52] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout52].setDisplayName ( "" );
            methods[METHOD_list53] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list53].setDisplayName ( "" );
            methods[METHOD_locate54] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate54].setDisplayName ( "" );
            methods[METHOD_location55] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("location", new Class[] {}));
            methods[METHOD_location55].setDisplayName ( "" );
            methods[METHOD_lostFocus56] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus56].setDisplayName ( "" );
            methods[METHOD_minimumSize57] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize57].setDisplayName ( "" );
            methods[METHOD_mouseDown58] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown58].setDisplayName ( "" );
            methods[METHOD_mouseDrag59] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag59].setDisplayName ( "" );
            methods[METHOD_mouseEnter60] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter60].setDisplayName ( "" );
            methods[METHOD_mouseExit61] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit61].setDisplayName ( "" );
            methods[METHOD_mouseMove62] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove62].setDisplayName ( "" );
            methods[METHOD_mouseUp63] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp63].setDisplayName ( "" );
            methods[METHOD_move64] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move64].setDisplayName ( "" );
            methods[METHOD_nextFocus65] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus65].setDisplayName ( "" );
            methods[METHOD_paint66] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint66].setDisplayName ( "" );
            methods[METHOD_paintAll67] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll67].setDisplayName ( "" );
            methods[METHOD_paintComponents68] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents68].setDisplayName ( "" );
            methods[METHOD_paintImmediately69] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately69].setDisplayName ( "" );
            methods[METHOD_postEvent70] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent70].setDisplayName ( "" );
            methods[METHOD_preferredSize71] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize71].setDisplayName ( "" );
            methods[METHOD_prepareImage72] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage72].setDisplayName ( "" );
            methods[METHOD_print73] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print73].setDisplayName ( "" );
            methods[METHOD_printAll74] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll74].setDisplayName ( "" );
            methods[METHOD_printComponents75] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents75].setDisplayName ( "" );
            methods[METHOD_putClientProperty76] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty76].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction77] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction77].setDisplayName ( "" );
            methods[METHOD_remove78] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove78].setDisplayName ( "" );
            methods[METHOD_removeAll79] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll79].setDisplayName ( "" );
            methods[METHOD_removeNotify80] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify80].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener81] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener81].setDisplayName ( "" );
            methods[METHOD_repaint82] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint82].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus83] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus83].setDisplayName ( "" );
            methods[METHOD_requestFocus84] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus84].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow85] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow85].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions86] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions86].setDisplayName ( "" );
            methods[METHOD_reshape87] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape87].setDisplayName ( "" );
            methods[METHOD_resize88] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize88].setDisplayName ( "" );
            methods[METHOD_revalidate89] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate89].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible90] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible90].setDisplayName ( "" );
            methods[METHOD_setBounds91] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds91].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder92] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setComponentZOrder92].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale93] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale93].setDisplayName ( "" );
            methods[METHOD_show94] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("show", new Class[] {}));
            methods[METHOD_show94].setDisplayName ( "" );
            methods[METHOD_size95] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("size", new Class[] {}));
            methods[METHOD_size95].setDisplayName ( "" );
            methods[METHOD_toString96] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString96].setDisplayName ( "" );
            methods[METHOD_transferFocus97] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus97].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward98] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward98].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle99] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle99].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle100] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle100].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction101] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction101].setDisplayName ( "" );
            methods[METHOD_update102] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update102].setDisplayName ( "" );
            methods[METHOD_updateUI103] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI103].setDisplayName ( "" );
            methods[METHOD_validate104] = new MethodDescriptor ( com.helpguest.ui.ConnectionPanel.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate104].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
        
        // Here you can add code for customizing the methods array.
        
        return methods;         }//GEN-LAST:Methods
    
    private static java.awt.Image iconColor16 = null;//GEN-BEGIN:IconsDef
    private static java.awt.Image iconColor32 = null;
    private static java.awt.Image iconMono16 = null;
    private static java.awt.Image iconMono32 = null;//GEN-END:IconsDef
    private static String iconNameC16 = "/com/helpguest/images/ConnectionPanel-16.jpg";//GEN-BEGIN:Icons
    private static String iconNameC32 = "/com/helpguest/images/ConnectionPanel-32.jpg";
    private static String iconNameM16 = null;
    private static String iconNameM32 = null;//GEN-END:Icons
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
//GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
//GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
        return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
        return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
        return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
        return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
        return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
        return defaultEventIndex;
    }
    
    /**
     * This method returns an image object that can be used to
     * represent the bean in toolboxes, toolbars, etc.   Icon images
     * will typically be GIFs, but may in future include other formats.
     * <p>
     * Beans aren't required to provide icons and may return null from
     * this method.
     * <p>
     * There are four possible flavors of icons (16x16 color,
     * 32x32 color, 16x16 mono, 32x32 mono).  If a bean choses to only
     * support a single icon we recommend supporting 16x16 color.
     * <p>
     * We recommend that icons have a "transparent" background
     * so they can be rendered onto an existing background.
     *
     * @param  iconKind  The kind of icon requested.  This should be
     *    one of the constant values ICON_COLOR_16x16, ICON_COLOR_32x32,
     *    ICON_MONO_16x16, or ICON_MONO_32x32.
     * @return  An image object representing the requested icon.  May
     *    return null if no suitable icon is available.
     */
    public java.awt.Image getIcon(int iconKind) {
        switch ( iconKind ) {
            case ICON_COLOR_16x16:
                if ( iconNameC16 == null )
                    return null;
                else {
                    if( iconColor16 == null )
                        iconColor16 = loadImage( iconNameC16 );
                    return iconColor16;
                }
            case ICON_COLOR_32x32:
                if ( iconNameC32 == null )
                    return null;
                else {
                    if( iconColor32 == null )
                        iconColor32 = loadImage( iconNameC32 );
                    return iconColor32;
                }
            case ICON_MONO_16x16:
                if ( iconNameM16 == null )
                    return null;
                else {
                    if( iconMono16 == null )
                        iconMono16 = loadImage( iconNameM16 );
                    return iconMono16;
                }
            case ICON_MONO_32x32:
                if ( iconNameM32 == null )
                    return null;
                else {
                    if( iconMono32 == null )
                        iconMono32 = loadImage( iconNameM32 );
                    return iconMono32;
                }
            default: return null;
        }
    }
    
}

