/*
 * CloseTabIcon.java
 *
 * Created on November 20, 2005, 11:33 AM
 */

package com.helpguest.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class CloseTabIcon implements Icon {

    private Icon icon;
    private JTabbedPane tabbedPane;
    private Rectangle rect;
    
    public CloseTabIcon() {
        this( new ImageIcon( CloseTabIcon.class.getResource("/com/helpguest/images/CloseTabIcon.png")) );
    }
	
    public CloseTabIcon(Icon icon) {
        this.icon = icon;
    }
    
    public void paintIcon(Component c, Graphics g, int x, int y) {
        if( null==tabbedPane ){
            tabbedPane = (JTabbedPane)c;
            //Set the tabbed pane to listen for mouse events on the icon location.
            tabbedPane.addMouseListener( new MouseAdapter() {
                public void mouseReleased( MouseEvent evt ) {
                    // if the event is not already consummed and the click was on the icon close the tab
                    if ( !evt.isConsumed()  &&   rect.contains( evt.getX(), evt.getY() ) ) {
                        final int index = tabbedPane.getSelectedIndex();
                        //ExpertPanel specific feature, don't consume or remove 1st tab
                        if (index != 0) {
                            tabbedPane.remove( index );
                            evt.consume();
                        }
                    }
                }
            });
        }

        rect = new Rectangle( x,y, getIconWidth(), getIconHeight() );
        icon.paintIcon(c, g, x, y );
    }


    /**
     * just delegate
     */
    public int getIconWidth() {
        return icon.getIconWidth();
    }

    /**
     * just delegate
     */
    public int getIconHeight() {
        return icon.getIconHeight();
    }
}
