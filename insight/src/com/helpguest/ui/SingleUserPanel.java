/*
 * HostPanel.java
 *
 * Created on July 21, 2005, 10:55 AM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.ui;

import java.awt.Cursor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import org.apache.log4j.Logger;

import com.helpguest.Agent;
import com.helpguest.ClientAgent;
import com.helpguest.HelpGuestMode;
import com.helpguest.protocol.Lexicon;
import com.helpguest.protocol.Vocabulary;
import com.helpguest.remoteaccess.HelpGuestVNCServer;
import com.helpguest.remoteaccess.VNCServerFactory;
import com.helpguest.service.Service;

/**
 *
 * @author  mabrams
 */
public class SingleUserPanel extends javax.swing.JPanel implements PropertyChangeListener  {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger("com.helpguest.ui.SingleUserPanel");
    public static final String CHAT_VISIBLE_PROPERTY = "chatVisible";
    
    /** Creates new form HostPanel */
    public SingleUserPanel() {
        initComponents();
        modePanel1MouseClicked(null);
        chatControl1.addPropertyChangeListener(this);
        
        //Make sure we clean up when the app quits
        Thread cleanUp = new Thread(new Runnable() {

			public void run() {
				if (clientAgent != null) 
					clientAgent.release();
			}
        	
        });
        Runtime.getRuntime().addShutdownHook(cleanUp);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup = new javax.swing.ButtonGroup();
        chatControl1 = new com.helpguest.ui.ChatControl();
        jPanel1 = new javax.swing.JPanel();
        hostButtonPanel1 = new com.helpguest.ui.HostButtonPanel();
        modePanel1 = new com.helpguest.ui.ModePanel();

        setLayout(new java.awt.GridBagLayout());

        setBackground(new java.awt.Color(204, 204, 204));
        setMinimumSize(new java.awt.Dimension(562, 238));
        setPreferredSize(new java.awt.Dimension(562, 238));
        chatControl1.setBackground(new java.awt.Color(204, 204, 204));
        chatControl1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 10, 10);
        add(chatControl1, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        hostButtonPanel1.setBackground(new java.awt.Color(204, 204, 204));
        hostButtonPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        hostButtonPanel1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                hostButtonPanel1PropertyChange(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel1.add(hostButtonPanel1, gridBagConstraints);

        modePanel1.setBackground(new java.awt.Color(204, 204, 204));
        modePanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        modePanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                modePanel1MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel1.add(modePanel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.9;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 6, 10);
        add(jPanel1, gridBagConstraints);

    }// </editor-fold>//GEN-END:initComponents

    private void modePanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modePanel1MouseClicked
        //Make sure nothings running then enable everything.
        Thread t = new Thread(new Runnable() {
            public void run() {
                modePanel1.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                if (clientAgent.enable(services, modePanel1)) {
                    //if the client agent is successfully enabled, we can continue
	                chatControl1.enableChatServer(clientAgent);
	                modePanel1.updateStatus("Security enabled ... Waiting on expert.",modePanel1.next());
	                
	                //Now maintain the service connection.
	                try {
	                    if (logger.isDebugEnabled()) {logger.debug("Preparing to maintain the service connection.");}
	                    guestServiceSocket = new Socket("", clientAgent.getPortFor(Service.AGENT));
	                    if (logger.isDebugEnabled()) {logger.debug("guestServiceSocket: [" + guestServiceSocket + "]");}
	                    out = new PrintWriter(guestServiceSocket.getOutputStream(), true);
	                    if (logger.isDebugEnabled()) {logger.debug("Service socket ready.");}
	
	                    //This should only happend if the socket is sucessfully created.
	                    keepAlive = new TimerTask() {
	                        public void run() {
	                                //This can be any text at this time.
	                            if (logger.isDebugEnabled()) {logger.debug("Sending keep alive signal.");}
	                            out.println("alive");
	                        }
	                    };
	                    //Run the keep alive task starting now every keepAlivePeriod.
	                    timer.scheduleAtFixedRate(keepAlive,0, keepAlivePeriod);
	                    if (logger.isDebugEnabled()) {logger.debug("Keep alive schedule set for '" + keepAlivePeriod + "' millis.");}
	                    
	                    surveyDialog = new SurveyDialog(new JFrame(), true, guestServiceSocket.getOutputStream(), HostUI.UID);
	                    
	                } catch (IOException iox) {
	                    logger.error("Unable to connect to client service.", iox);
	                }   
                } else {
                	//the client could not be enabled, stop trying.
                	modePanel1.finish();
                }
            }                
        });        
        t.start();
        
    }//GEN-LAST:event_modePanel1MouseClicked

    private void hostButtonPanel1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_hostButtonPanel1PropertyChange
        if (logger.isDebugEnabled()) { logger.debug("property change event: " + evt.getPropertyName()); }
        try {
            //The chat button has changed
            /**
             * Don't allow chat to be 'disabled'
            if (HostButtonPanel.CHAT_ENABLED_PROPERTY.equals(evt.getPropertyName()) && chatControl1.isSpokenTo()) {
                chatControl1.setEnabled(hostButtonPanel1.isChatEnabled());
                if (!hostButtonPanel1.isChatEnabled()) {
                    //clientAgent.disable(Service.CHAT);
                    this.setChatVisible(false);
                } else {
                    this.setChatVisible(true);
                    //clientAgent.enable(Service.CHAT);
                }
            } 
             *
             */
            
            if (HostButtonPanel.SEE_ENABLED_PROPERTY.equals(evt.getPropertyName())) {
                //the see button has changed
                hostButtonPanel1.setDoEnabled(hostButtonPanel1.isSeeEnabled());
                if (hostButtonPanel1.isSeeEnabled()) {
                    //clientAgent.enable(Service.VNC);
                    vncServer.start();
                    out.println(Lexicon.Guest.START_SESSION+":"+HostUI.UID);
                } else {
                    vncServer.stop();
                    out.println(Lexicon.Guest.STOP_SESSION+":"+HostUI.UID);
                    //clientAgent.disable(Service.VNC);
                }
            }
            
            if (HostButtonPanel.DO_ENABLED_PROPERTY.equals(evt.getPropertyName())) {
                //the do button has changed
                hostButtonPanel1.setSeeEnabled(hostButtonPanel1.isDoEnabled());
            }
            
        } catch (PropertyVetoException pvx) {
            //do nothing
        }        
            
    }//GEN-LAST:event_hostButtonPanel1PropertyChange
    

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup;
    private com.helpguest.ui.ChatControl chatControl1;
    private com.helpguest.ui.HostButtonPanel hostButtonPanel1;
    private javax.swing.JPanel jPanel1;
    private com.helpguest.ui.ModePanel modePanel1;
    // End of variables declaration//GEN-END:variables
    
    private final String user = System.getProperty("user.name");
    private final Service[] services = {Service.CHAT, Service.VOIP, Service.VNC, Service.AGENT};
    private final Agent clientAgent = new ClientAgent(user, "Guest Beta - No Help Message");
    private PrintWriter out;
    private Socket guestServiceSocket;
    private Timer timer = new Timer(true);
    private TimerTask keepAlive;
    private SurveyDialog surveyDialog = null;
    private boolean showSurvey = false;
    private final HelpGuestVNCServer vncServer = VNCServerFactory.getVNCServer();

    /**
     * ping the GuestAgent service every 30000 milliseconds to keep alive
     */
    private long keepAlivePeriod = 30000;
    private String mode;
    
    public SurveyDialog getSurveyDialog() {
        if (showSurvey) {
            return surveyDialog;
        }
        return null;
    }
    /**
     * Send the termination signal to the GuestAgent service.
     */
    public void terminate() {
	if (out !=null ) {
            out.println(Vocabulary.LOGOUT);
	}
        if(keepAlive != null) {
	    keepAlive.cancel();
	}
        timer.cancel();
    }
    
    public void setMode(HelpGuestMode mode) {        
        try {
            modePanel1.setMode(mode);
        } catch (PropertyVetoException pvx) {
            //do nothing
        }
    }
    
    public void propertyChange(PropertyChangeEvent evt) {
        if (logger.isDebugEnabled()) {logger.debug("Something has changed: " + evt.getPropertyName());}
        if ("spokenTo".equals(evt.getPropertyName()) && chatControl1.isSpokenTo()) {
            if (logger.isDebugEnabled()) {logger.debug("I've been spoken to!");}
            Thread t = new Thread(new Runnable() {
                public void run() {
                    chatControl1.enableChatClient(clientAgent, System.getProperty("user.name"));
                    setChatVisible(true);
                    modePanel1.finish();
                    modePanel1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    //Since there was a chat session, evaluation is to happen
                    showSurvey = true;
                }
            });
            t.start();
        }
    }    
    
    /**
     * Holds value of property chatVisible.
     */
    private boolean chatVisible;

    /**
     * Utility field used by bound properties.
     */
    private java.beans.PropertyChangeSupport propertyChangeSupport =  new java.beans.PropertyChangeSupport(this);

    /**
     * Utility field used by constrained properties.
     */
    private java.beans.VetoableChangeSupport vetoableChangeSupport_1 =  new java.beans.VetoableChangeSupport(this);

    /**
     * Adds a PropertyChangeListener to the listener list.
     * @param l The listener to add.
     */
    public void addPropertyChangeListener(java.beans.PropertyChangeListener l) {

        propertyChangeSupport.addPropertyChangeListener(l);
    }

    /**
     * Removes a PropertyChangeListener from the listener list.
     * @param l The listener to remove.
     */
    public void removePropertyChangeListener(java.beans.PropertyChangeListener l) {

        propertyChangeSupport.removePropertyChangeListener(l);
    }

    /**
     * Adds a VetoableChangeListener to the listener list.
     * @param l The listener to add.
     */
    public void addVetoableChangeListener(java.beans.VetoableChangeListener l) {

        vetoableChangeSupport_1.addVetoableChangeListener (l);
    }

    /**
     * Removes a VetoableChangeListener from the listener list.
     * @param l The listener to remove.
     */
    public void removeVetoableChangeListener(java.beans.VetoableChangeListener l) {

        vetoableChangeSupport_1.removeVetoableChangeListener (l);
    }

    /**
     * Getter for property chatVisible.
     * @return Value of property chatVisible.
     */
    public boolean isChatVisible() {

        return this.chatVisible;
    }

    /**
     * Setter for property chatVisible.
     * @param chatVisible New value of property chatVisible.
     * 
     * @throws PropertyVetoException if some vetoable listeners reject the new value
     */
    public void setChatVisible(boolean chatVisible) {
        boolean oldChatVisible = this.chatVisible;
        this.chatVisible = chatVisible;
        propertyChangeSupport.firePropertyChange (CHAT_VISIBLE_PROPERTY   , new Boolean (oldChatVisible), new Boolean (chatVisible));
        chatControl1.setVisible(chatVisible);
        hostButtonPanel1.setChatEnabled(true);
    }    
    
}
