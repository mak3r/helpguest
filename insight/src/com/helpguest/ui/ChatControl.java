/*
 * Chat.java
 *
 * Created on January 30, 2005, 1:20 AM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.ui;

import com.helpguest.Agent;
import com.helpguest.service.Service;
import com.helpguest.service.chat.ChatClient;
import com.helpguest.service.chat.ChatController;
import com.helpguest.service.chat.ChatMultiServer;
import com.helpguest.util.Util;

import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import org.apache.log4j.Logger;

/**
 *
 * @author  mabrams
 */
public class ChatControl extends javax.swing.JPanel implements ChatController {
    
    /** Creates new form Chat */
    public ChatControl(Agent agent, String name) {
        this.name = name;
        this.agent = agent;
        initComponents();
    }
    
    /**
     * TODO: code needs to be included/modified to handle
     *  the condition when this c-tor is called but the 
     *  agent is not initialized (is null) when enableChatClient()
     *  or enableChatServer() is called.
     */
    public ChatControl() {
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        dialogTextArea = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        messageTextArea = new javax.swing.JTextArea();

        setBackground(new java.awt.Color(226, 200, 251));
        setMinimumSize(new java.awt.Dimension(0, 0));
        setPreferredSize(new java.awt.Dimension(493, 191));
        setLayout(new java.awt.BorderLayout());

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setResizeWeight(0.82);
        jSplitPane1.setContinuousLayout(true);
        jSplitPane1.setLastDividerLocation(100);
        jSplitPane1.setMinimumSize(new java.awt.Dimension(32, 211));
        jSplitPane1.setPreferredSize(null);

        jScrollPane1.setAutoscrolls(true);
        jScrollPane1.setMinimumSize(new java.awt.Dimension(28, 100));

        dialogTextArea.setBackground(new java.awt.Color(236, 254, 236));
        dialogTextArea.setColumns(80);
        dialogTextArea.setEditable(false);
        dialogTextArea.setLineWrap(true);
        dialogTextArea.setWrapStyleWord(true);
        dialogTextArea.setMargin(new java.awt.Insets(2, 2, 2, 2));
        dialogTextArea.setMinimumSize(new java.awt.Dimension(40, 100));
        dialogTextArea.setRequestFocusEnabled(false);
        jScrollPane1.setViewportView(dialogTextArea);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setMinimumSize(new java.awt.Dimension(0, 66));
        jPanel1.setPreferredSize(new java.awt.Dimension(0, 66));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jScrollPane2.setMaximumSize(new java.awt.Dimension(32767, 88));
        jScrollPane2.setMinimumSize(new java.awt.Dimension(0, 0));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(0, 0));

        messageTextArea.setLineWrap(true);
        messageTextArea.setRows(3);
        messageTextArea.setText("Type messages here. Press enter to send the message.");
        messageTextArea.setToolTipText("Type messages here.");
        messageTextArea.setWrapStyleWord(true);
        messageTextArea.setEnabled(false);
        messageTextArea.setFocusCycleRoot(true);
        messageTextArea.setMargin(new java.awt.Insets(2, 2, 2, 2));
        messageTextArea.setMaximumSize(new java.awt.Dimension(2147483647, 88));
        messageTextArea.setMinimumSize(new java.awt.Dimension(50, 11));
        messageTextArea.setPreferredSize(null);
        messageTextArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                messageTextAreaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                messageTextAreaKeyTyped(evt);
            }
        });
        messageTextArea.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                messageTextAreaMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(messageTextArea);

        jPanel1.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jSplitPane1.setRightComponent(jPanel1);

        add(jSplitPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void messageTextAreaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_messageTextAreaMouseReleased
        if (firstUse) {
            messageTextArea.setText(null);
            firstUse = false;
        }
    }//GEN-LAST:event_messageTextAreaMouseReleased

    private void messageTextAreaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_messageTextAreaKeyReleased
        // Add your handling code here:
        if (java.awt.event.KeyEvent.VK_ENTER == evt.getKeyCode()) {
                        if (log.isDebugEnabled()) {log.debug("chatClient " + (chatClient==null?"is ":"is not ") + "null");}
            if (!"".equals(messageTextArea.getText())) {
                if (chatClient != null) {
                    try {
                        //The newline is necessary here for the server to recognize something to be written.
                    	//chatClient.write( "Test " + "\n");
                    	chatClient.write(name + ": " +  messageTextArea.getText() + "\n");
                    } catch (SocketException sox) {
                        messageTextArea.setText("\nChat server is unavailable.\n " +
                                "Messages are no longer being delivered.\n");
                    }
                }
                messageTextArea.setText(null);
            }
        }
    }//GEN-LAST:event_messageTextAreaKeyReleased

    /**
     * This method allows the system to send messages into the chat Q.
     * The messages will show up in the dialogTextArea as if the message
     * was sent via the messageTextArea.
     * 
     * @param message is the message to send.
     */
    public void sendMessage(final String message) {
    	 //if (log.isDebugEnabled()) {log.debug("chatClient " + (chatClient==null?"is ":"is not ") + "null");}
    	if (chatClient != null) {
            try {
                //The newline is necessary here for the server to recognize something to be written.
                chatClient.write("" + message + "\n");
            } catch (SocketException sox) {
                messageTextArea.setText("\nChat server is unavailable.\n " +
                        "Messages are no longer being delivered.\n");
            		}
    	}
    }
    
    private void messageTextAreaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_messageTextAreaKeyTyped

    }//GEN-LAST:event_messageTextAreaKeyTyped
    
    public boolean enableChatClient(Agent agent, String name, Service service) {
        if (this.agent == null) {
            this.agent = agent;
        }
        this.name = name;
        dialogTextArea.setText(null);
        return enableChatClient(service);    	
    }
    
    public boolean enableChatClient(Service service) {
        int port = agent.getPortFor(service);
        if (log.isInfoEnabled()) {log.info("Enabling chat client on port " + port);}
        try {
            sock = new Socket("", port);
            chatClient = new ChatClient(sock);
            if (log.isInfoEnabled()) {log.info("chatClient: " + chatClient);}
            Thread t = new Thread( new Runnable() {
                    public void run() {
                    	try {
                    		String message = "";
                    		//flush the buffer
                    		chatClient.write(message + "\n");
                    		while((message = chatClient.read()) != null) {
                    			//Make sure there is only one newline
                    			if (!"".equals(message) && message != null) {
                    				dialogTextArea.append(message + "\n");
                    				dialogTextArea.setCaretPosition(dialogTextArea.getText().length());
                    				//update the number of rows in the text area based on the size of the message
                    				// and the current number of rows.
                    				dialogTextArea.setRows(dialogTextArea.getRows() + (message.length() / dialogTextArea.getColumns()));
                    			}
                    		}
                    	} catch (SocketException sox) {
                    		messageTextArea.setText("\nChat server is unavailable.\n " +
                    		"Messages are no longer being delivered.\n");
                    		log.info("Resetting chatClient to null.", sox);
                    		chatClient.finalize();
                    		chatClient = null;
                    	}
                    }
                });
            t.start();
            return true;
        } catch (UnknownHostException e) {
            if (log.isInfoEnabled()) {log.error("Don't know about host: localhost", e);}
        } catch (Exception ex) {
            if (log.isInfoEnabled()) {log.info("Failed to enable chat client.", ex);}            
        }
        return false;
    }
    
    public boolean enableChatClient(Agent agent, String name) {
    	return enableChatClient(agent, name, Service.CHAT);
    }
    
    
    public boolean enableChatClient() {
    	return enableChatClient(Service.CHAT);
    }
    
    public void enableChatServer(Agent agent, Service service) {
    	this.agent = agent;
    	enableChatServer(service);
    }

    public void enableChatServer(Service service) {
        int port = agent.getPortFor(service);
        if (log.isInfoEnabled()) {log.info("Starting ChatMultiServer on port " + port);}
        new ChatMultiServer(port,this).start();
        setEnabled(true);
    }
    
    /**
     * Enable with the default service of CHAT.
     * @param agent Agent
     */
    public void enableChatServer(Agent agent) {
        this.agent = agent;
        enableChatServer(Service.CHAT);
    }
    
    /**
     * Enable with the default service of CHAT.
     * Used for legacy enable calls.
     */
    public void enableChatServer() {
    	enableChatServer(Service.CHAT);
    }
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        messageTextArea.setEnabled(enabled);
    }
    
    
    public static void main(String[] args) {
        Util.frameIt(new ChatControl());
    }
     
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea dialogTextArea;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextArea messageTextArea;
    // End of variables declaration//GEN-END:variables
    
    private static final Logger log = Logger.getLogger("com.helpguest.chat.ui.Chat");
    private Socket sock;
    private ChatClient chatClient;
    private Agent agent;
    private String name;

    private boolean firstUse = true;

    /**
     * Holds value of property spokenTo.
     */
    private boolean spokenTo;

    /**
     * Utility field used by bound properties.
     */
    private java.beans.PropertyChangeSupport propertyChangeSupport =  new java.beans.PropertyChangeSupport(this);

    /**
     * Adds a PropertyChangeListener to the listener list.
     * @param l The listener to add.
     */
    public void addPropertyChangeListener(java.beans.PropertyChangeListener l) {
        propertyChangeSupport.addPropertyChangeListener(l);
    }

    /**
     * Removes a PropertyChangeListener from the listener list.
     * @param l The listener to remove.
     */
    public void removePropertyChangeListener(java.beans.PropertyChangeListener l) {
        propertyChangeSupport.removePropertyChangeListener(l);
    }

    /**
     * Getter for property spokenTo.
     * @return Value of property spokenTo.
     */
    public boolean isSpokenTo() {
        return this.spokenTo;
    }

    /**
     * Setter for property spokenTo.
     * @param spokenTo New value of property spokenTo.
     */
    public void setSpokenTo(boolean spokenTo) {
        boolean oldSpokenTo = this.spokenTo;
        this.spokenTo = spokenTo;
        propertyChangeSupport.firePropertyChange ("spokenTo", new Boolean (oldSpokenTo), new Boolean (spokenTo));
    }
}
