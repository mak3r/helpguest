/*
 * ChatBeanInfo.java
 *
 * Created on July 18, 2005, 12:36 AM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.ui.chat;

import java.beans.*;

/**
 * @author mabrams
 */
public class ChatBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( com.helpguest.ui.chat.Chat.class , null );//GEN-HEADEREND:BeanDescriptor
        
        // Here you can add code for customizing the BeanDescriptor.
        
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_border = 8;
    private static final int PROPERTY_bounds = 9;
    private static final int PROPERTY_colorModel = 10;
    private static final int PROPERTY_component = 11;
    private static final int PROPERTY_componentCount = 12;
    private static final int PROPERTY_componentListeners = 13;
    private static final int PROPERTY_componentOrientation = 14;
    private static final int PROPERTY_componentPopupMenu = 15;
    private static final int PROPERTY_components = 16;
    private static final int PROPERTY_containerListeners = 17;
    private static final int PROPERTY_cursor = 18;
    private static final int PROPERTY_cursorSet = 19;
    private static final int PROPERTY_debugGraphicsOptions = 20;
    private static final int PROPERTY_displayable = 21;
    private static final int PROPERTY_doubleBuffered = 22;
    private static final int PROPERTY_dropTarget = 23;
    private static final int PROPERTY_enabled = 24;
    private static final int PROPERTY_focusable = 25;
    private static final int PROPERTY_focusCycleRoot = 26;
    private static final int PROPERTY_focusCycleRootAncestor = 27;
    private static final int PROPERTY_focusListeners = 28;
    private static final int PROPERTY_focusOwner = 29;
    private static final int PROPERTY_focusTraversable = 30;
    private static final int PROPERTY_focusTraversalKeys = 31;
    private static final int PROPERTY_focusTraversalKeysEnabled = 32;
    private static final int PROPERTY_focusTraversalPolicy = 33;
    private static final int PROPERTY_focusTraversalPolicyProvider = 34;
    private static final int PROPERTY_focusTraversalPolicySet = 35;
    private static final int PROPERTY_font = 36;
    private static final int PROPERTY_fontSet = 37;
    private static final int PROPERTY_foreground = 38;
    private static final int PROPERTY_foregroundSet = 39;
    private static final int PROPERTY_graphics = 40;
    private static final int PROPERTY_graphicsConfiguration = 41;
    private static final int PROPERTY_height = 42;
    private static final int PROPERTY_hierarchyBoundsListeners = 43;
    private static final int PROPERTY_hierarchyListeners = 44;
    private static final int PROPERTY_ignoreRepaint = 45;
    private static final int PROPERTY_inheritsPopupMenu = 46;
    private static final int PROPERTY_inputContext = 47;
    private static final int PROPERTY_inputMap = 48;
    private static final int PROPERTY_inputMethodListeners = 49;
    private static final int PROPERTY_inputMethodRequests = 50;
    private static final int PROPERTY_inputVerifier = 51;
    private static final int PROPERTY_insets = 52;
    private static final int PROPERTY_keyListeners = 53;
    private static final int PROPERTY_layout = 54;
    private static final int PROPERTY_lightweight = 55;
    private static final int PROPERTY_locale = 56;
    private static final int PROPERTY_location = 57;
    private static final int PROPERTY_locationOnScreen = 58;
    private static final int PROPERTY_managingFocus = 59;
    private static final int PROPERTY_maximumSize = 60;
    private static final int PROPERTY_maximumSizeSet = 61;
    private static final int PROPERTY_minimumSize = 62;
    private static final int PROPERTY_minimumSizeSet = 63;
    private static final int PROPERTY_mouseListeners = 64;
    private static final int PROPERTY_mouseMotionListeners = 65;
    private static final int PROPERTY_mousePosition = 66;
    private static final int PROPERTY_mouseWheelListeners = 67;
    private static final int PROPERTY_name = 68;
    private static final int PROPERTY_nextFocusableComponent = 69;
    private static final int PROPERTY_opaque = 70;
    private static final int PROPERTY_optimizedDrawingEnabled = 71;
    private static final int PROPERTY_paintingTile = 72;
    private static final int PROPERTY_parent = 73;
    private static final int PROPERTY_peer = 74;
    private static final int PROPERTY_preferredSize = 75;
    private static final int PROPERTY_preferredSizeSet = 76;
    private static final int PROPERTY_propertyChangeListeners = 77;
    private static final int PROPERTY_registeredKeyStrokes = 78;
    private static final int PROPERTY_requestFocusEnabled = 79;
    private static final int PROPERTY_rootPane = 80;
    private static final int PROPERTY_showing = 81;
    private static final int PROPERTY_size = 82;
    private static final int PROPERTY_toolkit = 83;
    private static final int PROPERTY_toolTipText = 84;
    private static final int PROPERTY_topLevelAncestor = 85;
    private static final int PROPERTY_transferHandler = 86;
    private static final int PROPERTY_treeLock = 87;
    private static final int PROPERTY_UI = 88;
    private static final int PROPERTY_UIClassID = 89;
    private static final int PROPERTY_valid = 90;
    private static final int PROPERTY_validateRoot = 91;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 92;
    private static final int PROPERTY_vetoableChangeListeners = 93;
    private static final int PROPERTY_visible = 94;
    private static final int PROPERTY_visibleRect = 95;
    private static final int PROPERTY_width = 96;
    private static final int PROPERTY_x = 97;
    private static final int PROPERTY_y = 98;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[99];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", com.helpguest.ui.chat.Chat.class, "getAccessibleContext", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", com.helpguest.ui.chat.Chat.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", com.helpguest.ui.chat.Chat.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", com.helpguest.ui.chat.Chat.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", com.helpguest.ui.chat.Chat.class, "getAncestorListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", com.helpguest.ui.chat.Chat.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", com.helpguest.ui.chat.Chat.class, "getBackground", "setBackground" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", com.helpguest.ui.chat.Chat.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", com.helpguest.ui.chat.Chat.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", com.helpguest.ui.chat.Chat.class, "getBounds", "setBounds" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", com.helpguest.ui.chat.Chat.class, "getColorModel", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", com.helpguest.ui.chat.Chat.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", com.helpguest.ui.chat.Chat.class, "getComponentCount", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", com.helpguest.ui.chat.Chat.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", com.helpguest.ui.chat.Chat.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", com.helpguest.ui.chat.Chat.class, "getComponentPopupMenu", "setComponentPopupMenu" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", com.helpguest.ui.chat.Chat.class, "getComponents", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", com.helpguest.ui.chat.Chat.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", com.helpguest.ui.chat.Chat.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", com.helpguest.ui.chat.Chat.class, "isCursorSet", null );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", com.helpguest.ui.chat.Chat.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", com.helpguest.ui.chat.Chat.class, "isDisplayable", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", com.helpguest.ui.chat.Chat.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", com.helpguest.ui.chat.Chat.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", com.helpguest.ui.chat.Chat.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", com.helpguest.ui.chat.Chat.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", com.helpguest.ui.chat.Chat.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", com.helpguest.ui.chat.Chat.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", com.helpguest.ui.chat.Chat.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", com.helpguest.ui.chat.Chat.class, "isFocusOwner", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", com.helpguest.ui.chat.Chat.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", com.helpguest.ui.chat.Chat.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", com.helpguest.ui.chat.Chat.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", com.helpguest.ui.chat.Chat.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", com.helpguest.ui.chat.Chat.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", com.helpguest.ui.chat.Chat.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", com.helpguest.ui.chat.Chat.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", com.helpguest.ui.chat.Chat.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", com.helpguest.ui.chat.Chat.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", com.helpguest.ui.chat.Chat.class, "isForegroundSet", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", com.helpguest.ui.chat.Chat.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", com.helpguest.ui.chat.Chat.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", com.helpguest.ui.chat.Chat.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", com.helpguest.ui.chat.Chat.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", com.helpguest.ui.chat.Chat.class, "getHierarchyListeners", null );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", com.helpguest.ui.chat.Chat.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", com.helpguest.ui.chat.Chat.class, "getInheritsPopupMenu", "setInheritsPopupMenu" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", com.helpguest.ui.chat.Chat.class, "getInputContext", null );
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", com.helpguest.ui.chat.Chat.class, "getInputMap", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", com.helpguest.ui.chat.Chat.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", com.helpguest.ui.chat.Chat.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", com.helpguest.ui.chat.Chat.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", com.helpguest.ui.chat.Chat.class, "getInsets", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", com.helpguest.ui.chat.Chat.class, "getKeyListeners", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", com.helpguest.ui.chat.Chat.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", com.helpguest.ui.chat.Chat.class, "isLightweight", null );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", com.helpguest.ui.chat.Chat.class, "getLocale", "setLocale" );
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", com.helpguest.ui.chat.Chat.class, "getLocation", "setLocation" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", com.helpguest.ui.chat.Chat.class, "getLocationOnScreen", null );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", com.helpguest.ui.chat.Chat.class, "isManagingFocus", null );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", com.helpguest.ui.chat.Chat.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", com.helpguest.ui.chat.Chat.class, "isMaximumSizeSet", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", com.helpguest.ui.chat.Chat.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", com.helpguest.ui.chat.Chat.class, "isMinimumSizeSet", null );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", com.helpguest.ui.chat.Chat.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", com.helpguest.ui.chat.Chat.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", com.helpguest.ui.chat.Chat.class, "getMousePosition", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", com.helpguest.ui.chat.Chat.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", com.helpguest.ui.chat.Chat.class, "getName", "setName" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", com.helpguest.ui.chat.Chat.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", com.helpguest.ui.chat.Chat.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", com.helpguest.ui.chat.Chat.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", com.helpguest.ui.chat.Chat.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", com.helpguest.ui.chat.Chat.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", com.helpguest.ui.chat.Chat.class, "getPeer", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", com.helpguest.ui.chat.Chat.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", com.helpguest.ui.chat.Chat.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", com.helpguest.ui.chat.Chat.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", com.helpguest.ui.chat.Chat.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", com.helpguest.ui.chat.Chat.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", com.helpguest.ui.chat.Chat.class, "getRootPane", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", com.helpguest.ui.chat.Chat.class, "isShowing", null );
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", com.helpguest.ui.chat.Chat.class, "getSize", "setSize" );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", com.helpguest.ui.chat.Chat.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", com.helpguest.ui.chat.Chat.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", com.helpguest.ui.chat.Chat.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", com.helpguest.ui.chat.Chat.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", com.helpguest.ui.chat.Chat.class, "getTreeLock", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", com.helpguest.ui.chat.Chat.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", com.helpguest.ui.chat.Chat.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", com.helpguest.ui.chat.Chat.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", com.helpguest.ui.chat.Chat.class, "isValidateRoot", null );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", com.helpguest.ui.chat.Chat.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", com.helpguest.ui.chat.Chat.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", com.helpguest.ui.chat.Chat.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", com.helpguest.ui.chat.Chat.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", com.helpguest.ui.chat.Chat.class, "getWidth", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", com.helpguest.ui.chat.Chat.class, "getX", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", com.helpguest.ui.chat.Chat.class, "getY", null );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
        
        // Here you can add code for customizing the properties array.
        
        return properties;         }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( com.helpguest.ui.chat.Chat.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
        
        // Here you can add code for customizing the event sets array.
        
        return eventSets;         }//GEN-LAST:Events
    
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_applyComponentOrientation4 = 4;
    private static final int METHOD_areFocusTraversalKeysSet5 = 5;
    private static final int METHOD_bounds6 = 6;
    private static final int METHOD_checkImage7 = 7;
    private static final int METHOD_computeVisibleRect8 = 8;
    private static final int METHOD_contains9 = 9;
    private static final int METHOD_countComponents10 = 10;
    private static final int METHOD_createImage11 = 11;
    private static final int METHOD_createToolTip12 = 12;
    private static final int METHOD_createVolatileImage13 = 13;
    private static final int METHOD_deliverEvent14 = 14;
    private static final int METHOD_disable15 = 15;
    private static final int METHOD_dispatchEvent16 = 16;
    private static final int METHOD_doLayout17 = 17;
    private static final int METHOD_enable18 = 18;
    private static final int METHOD_enableChatClient19 = 19;
    private static final int METHOD_enableChatServer20 = 20;
    private static final int METHOD_enableInputMethods21 = 21;
    private static final int METHOD_findComponentAt22 = 22;
    private static final int METHOD_firePropertyChange23 = 23;
    private static final int METHOD_getActionForKeyStroke24 = 24;
    private static final int METHOD_getBounds25 = 25;
    private static final int METHOD_getClientProperty26 = 26;
    private static final int METHOD_getComponentAt27 = 27;
    private static final int METHOD_getComponentZOrder28 = 28;
    private static final int METHOD_getConditionForKeyStroke29 = 29;
    private static final int METHOD_getDefaultLocale30 = 30;
    private static final int METHOD_getFontMetrics31 = 31;
    private static final int METHOD_getInsets32 = 32;
    private static final int METHOD_getListeners33 = 33;
    private static final int METHOD_getLocation34 = 34;
    private static final int METHOD_getMousePosition35 = 35;
    private static final int METHOD_getPopupLocation36 = 36;
    private static final int METHOD_getPropertyChangeListeners37 = 37;
    private static final int METHOD_getSize38 = 38;
    private static final int METHOD_getToolTipLocation39 = 39;
    private static final int METHOD_getToolTipText40 = 40;
    private static final int METHOD_gotFocus41 = 41;
    private static final int METHOD_grabFocus42 = 42;
    private static final int METHOD_handleEvent43 = 43;
    private static final int METHOD_hasFocus44 = 44;
    private static final int METHOD_hide45 = 45;
    private static final int METHOD_imageUpdate46 = 46;
    private static final int METHOD_insets47 = 47;
    private static final int METHOD_inside48 = 48;
    private static final int METHOD_invalidate49 = 49;
    private static final int METHOD_isAncestorOf50 = 50;
    private static final int METHOD_isFocusCycleRoot51 = 51;
    private static final int METHOD_isLightweightComponent52 = 52;
    private static final int METHOD_keyDown53 = 53;
    private static final int METHOD_keyUp54 = 54;
    private static final int METHOD_layout55 = 55;
    private static final int METHOD_list56 = 56;
    private static final int METHOD_locate57 = 57;
    private static final int METHOD_location58 = 58;
    private static final int METHOD_lostFocus59 = 59;
    private static final int METHOD_minimumSize60 = 60;
    private static final int METHOD_mouseDown61 = 61;
    private static final int METHOD_mouseDrag62 = 62;
    private static final int METHOD_mouseEnter63 = 63;
    private static final int METHOD_mouseExit64 = 64;
    private static final int METHOD_mouseMove65 = 65;
    private static final int METHOD_mouseUp66 = 66;
    private static final int METHOD_move67 = 67;
    private static final int METHOD_nextFocus68 = 68;
    private static final int METHOD_paint69 = 69;
    private static final int METHOD_paintAll70 = 70;
    private static final int METHOD_paintComponents71 = 71;
    private static final int METHOD_paintImmediately72 = 72;
    private static final int METHOD_postEvent73 = 73;
    private static final int METHOD_preferredSize74 = 74;
    private static final int METHOD_prepareImage75 = 75;
    private static final int METHOD_print76 = 76;
    private static final int METHOD_printAll77 = 77;
    private static final int METHOD_printComponents78 = 78;
    private static final int METHOD_putClientProperty79 = 79;
    private static final int METHOD_registerKeyboardAction80 = 80;
    private static final int METHOD_remove81 = 81;
    private static final int METHOD_removeAll82 = 82;
    private static final int METHOD_removeNotify83 = 83;
    private static final int METHOD_removePropertyChangeListener84 = 84;
    private static final int METHOD_repaint85 = 85;
    private static final int METHOD_requestDefaultFocus86 = 86;
    private static final int METHOD_requestFocus87 = 87;
    private static final int METHOD_requestFocusInWindow88 = 88;
    private static final int METHOD_resetKeyboardActions89 = 89;
    private static final int METHOD_reshape90 = 90;
    private static final int METHOD_resize91 = 91;
    private static final int METHOD_revalidate92 = 92;
    private static final int METHOD_scrollRectToVisible93 = 93;
    private static final int METHOD_setBounds94 = 94;
    private static final int METHOD_setComponentZOrder95 = 95;
    private static final int METHOD_setDefaultLocale96 = 96;
    private static final int METHOD_show97 = 97;
    private static final int METHOD_size98 = 98;
    private static final int METHOD_toString99 = 99;
    private static final int METHOD_transferFocus100 = 100;
    private static final int METHOD_transferFocusBackward101 = 101;
    private static final int METHOD_transferFocusDownCycle102 = 102;
    private static final int METHOD_transferFocusUpCycle103 = 103;
    private static final int METHOD_unregisterKeyboardAction104 = 104;
    private static final int METHOD_update105 = 105;
    private static final int METHOD_updateUI106 = 106;
    private static final int METHOD_validate107 = 107;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[108];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation4] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation4].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet5] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet5].setDisplayName ( "" );
            methods[METHOD_bounds6] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds6].setDisplayName ( "" );
            methods[METHOD_checkImage7] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage7].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect8] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect8].setDisplayName ( "" );
            methods[METHOD_contains9] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains9].setDisplayName ( "" );
            methods[METHOD_countComponents10] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents10].setDisplayName ( "" );
            methods[METHOD_createImage11] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage11].setDisplayName ( "" );
            methods[METHOD_createToolTip12] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip12].setDisplayName ( "" );
            methods[METHOD_createVolatileImage13] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage13].setDisplayName ( "" );
            methods[METHOD_deliverEvent14] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent14].setDisplayName ( "" );
            methods[METHOD_disable15] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable15].setDisplayName ( "" );
            methods[METHOD_dispatchEvent16] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent16].setDisplayName ( "" );
            methods[METHOD_doLayout17] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout17].setDisplayName ( "" );
            methods[METHOD_enable18] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable18].setDisplayName ( "" );
            methods[METHOD_enableChatClient19] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("enableChatClient", new Class[] {}));
            methods[METHOD_enableChatClient19].setDisplayName ( "" );
            methods[METHOD_enableChatServer20] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("enableChatServer", new Class[] {}));
            methods[METHOD_enableChatServer20].setDisplayName ( "" );
            methods[METHOD_enableInputMethods21] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods21].setDisplayName ( "" );
            methods[METHOD_findComponentAt22] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt22].setDisplayName ( "" );
            methods[METHOD_firePropertyChange23] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange23].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke24] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke24].setDisplayName ( "" );
            methods[METHOD_getBounds25] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds25].setDisplayName ( "" );
            methods[METHOD_getClientProperty26] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty26].setDisplayName ( "" );
            methods[METHOD_getComponentAt27] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt27].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder28] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class}));
            methods[METHOD_getComponentZOrder28].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke29] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke29].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale30] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale30].setDisplayName ( "" );
            methods[METHOD_getFontMetrics31] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics31].setDisplayName ( "" );
            methods[METHOD_getInsets32] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets32].setDisplayName ( "" );
            methods[METHOD_getListeners33] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners33].setDisplayName ( "" );
            methods[METHOD_getLocation34] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation34].setDisplayName ( "" );
            methods[METHOD_getMousePosition35] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getMousePosition", new Class[] {Boolean.TYPE}));
            methods[METHOD_getMousePosition35].setDisplayName ( "" );
            methods[METHOD_getPopupLocation36] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getPopupLocation36].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners37] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners37].setDisplayName ( "" );
            methods[METHOD_getSize38] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize38].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation39] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation39].setDisplayName ( "" );
            methods[METHOD_getToolTipText40] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText40].setDisplayName ( "" );
            methods[METHOD_gotFocus41] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus41].setDisplayName ( "" );
            methods[METHOD_grabFocus42] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus42].setDisplayName ( "" );
            methods[METHOD_handleEvent43] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent43].setDisplayName ( "" );
            methods[METHOD_hasFocus44] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus44].setDisplayName ( "" );
            methods[METHOD_hide45] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide45].setDisplayName ( "" );
            methods[METHOD_imageUpdate46] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate46].setDisplayName ( "" );
            methods[METHOD_insets47] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets47].setDisplayName ( "" );
            methods[METHOD_inside48] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside48].setDisplayName ( "" );
            methods[METHOD_invalidate49] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate49].setDisplayName ( "" );
            methods[METHOD_isAncestorOf50] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf50].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot51] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot51].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent52] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent52].setDisplayName ( "" );
            methods[METHOD_keyDown53] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown53].setDisplayName ( "" );
            methods[METHOD_keyUp54] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp54].setDisplayName ( "" );
            methods[METHOD_layout55] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout55].setDisplayName ( "" );
            methods[METHOD_list56] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list56].setDisplayName ( "" );
            methods[METHOD_locate57] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate57].setDisplayName ( "" );
            methods[METHOD_location58] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("location", new Class[] {}));
            methods[METHOD_location58].setDisplayName ( "" );
            methods[METHOD_lostFocus59] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus59].setDisplayName ( "" );
            methods[METHOD_minimumSize60] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize60].setDisplayName ( "" );
            methods[METHOD_mouseDown61] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown61].setDisplayName ( "" );
            methods[METHOD_mouseDrag62] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag62].setDisplayName ( "" );
            methods[METHOD_mouseEnter63] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter63].setDisplayName ( "" );
            methods[METHOD_mouseExit64] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit64].setDisplayName ( "" );
            methods[METHOD_mouseMove65] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove65].setDisplayName ( "" );
            methods[METHOD_mouseUp66] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp66].setDisplayName ( "" );
            methods[METHOD_move67] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move67].setDisplayName ( "" );
            methods[METHOD_nextFocus68] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus68].setDisplayName ( "" );
            methods[METHOD_paint69] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint69].setDisplayName ( "" );
            methods[METHOD_paintAll70] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll70].setDisplayName ( "" );
            methods[METHOD_paintComponents71] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents71].setDisplayName ( "" );
            methods[METHOD_paintImmediately72] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately72].setDisplayName ( "" );
            methods[METHOD_postEvent73] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent73].setDisplayName ( "" );
            methods[METHOD_preferredSize74] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize74].setDisplayName ( "" );
            methods[METHOD_prepareImage75] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage75].setDisplayName ( "" );
            methods[METHOD_print76] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print76].setDisplayName ( "" );
            methods[METHOD_printAll77] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll77].setDisplayName ( "" );
            methods[METHOD_printComponents78] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents78].setDisplayName ( "" );
            methods[METHOD_putClientProperty79] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty79].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction80] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction80].setDisplayName ( "" );
            methods[METHOD_remove81] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove81].setDisplayName ( "" );
            methods[METHOD_removeAll82] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll82].setDisplayName ( "" );
            methods[METHOD_removeNotify83] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify83].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener84] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener84].setDisplayName ( "" );
            methods[METHOD_repaint85] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint85].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus86] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus86].setDisplayName ( "" );
            methods[METHOD_requestFocus87] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus87].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow88] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow88].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions89] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions89].setDisplayName ( "" );
            methods[METHOD_reshape90] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape90].setDisplayName ( "" );
            methods[METHOD_resize91] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize91].setDisplayName ( "" );
            methods[METHOD_revalidate92] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate92].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible93] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible93].setDisplayName ( "" );
            methods[METHOD_setBounds94] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds94].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder95] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setComponentZOrder95].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale96] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale96].setDisplayName ( "" );
            methods[METHOD_show97] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("show", new Class[] {}));
            methods[METHOD_show97].setDisplayName ( "" );
            methods[METHOD_size98] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("size", new Class[] {}));
            methods[METHOD_size98].setDisplayName ( "" );
            methods[METHOD_toString99] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString99].setDisplayName ( "" );
            methods[METHOD_transferFocus100] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus100].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward101] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward101].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle102] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle102].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle103] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle103].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction104] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction104].setDisplayName ( "" );
            methods[METHOD_update105] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update105].setDisplayName ( "" );
            methods[METHOD_updateUI106] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI106].setDisplayName ( "" );
            methods[METHOD_validate107] = new MethodDescriptor ( com.helpguest.ui.chat.Chat.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate107].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
        
        // Here you can add code for customizing the methods array.
        
        return methods;         }//GEN-LAST:Methods
    
    private static java.awt.Image iconColor16 = null;//GEN-BEGIN:IconsDef
    private static java.awt.Image iconColor32 = null;
    private static java.awt.Image iconMono16 = null;
    private static java.awt.Image iconMono32 = null;//GEN-END:IconsDef
    private static String iconNameC16 = "/com/helpguest/images/Chat-16.jpg";//GEN-BEGIN:Icons
    private static String iconNameC32 = "/com/helpguest/images/Chat-32.jpg";
    private static String iconNameM16 = null;
    private static String iconNameM32 = null;//GEN-END:Icons
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
//GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
//GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
        return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
        return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
        return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
        return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
        return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
        return defaultEventIndex;
    }
    
    /**
     * This method returns an image object that can be used to
     * represent the bean in toolboxes, toolbars, etc.   Icon images
     * will typically be GIFs, but may in future include other formats.
     * <p>
     * Beans aren't required to provide icons and may return null from
     * this method.
     * <p>
     * There are four possible flavors of icons (16x16 color,
     * 32x32 color, 16x16 mono, 32x32 mono).  If a bean choses to only
     * support a single icon we recommend supporting 16x16 color.
     * <p>
     * We recommend that icons have a "transparent" background
     * so they can be rendered onto an existing background.
     *
     * @param  iconKind  The kind of icon requested.  This should be
     *    one of the constant values ICON_COLOR_16x16, ICON_COLOR_32x32,
     *    ICON_MONO_16x16, or ICON_MONO_32x32.
     * @return  An image object representing the requested icon.  May
     *    return null if no suitable icon is available.
     */
    public java.awt.Image getIcon(int iconKind) {
        switch ( iconKind ) {
            case ICON_COLOR_16x16:
                if ( iconNameC16 == null )
                    return null;
                else {
                    if( iconColor16 == null )
                        iconColor16 = loadImage( iconNameC16 );
                    return iconColor16;
                }
            case ICON_COLOR_32x32:
                if ( iconNameC32 == null )
                    return null;
                else {
                    if( iconColor32 == null )
                        iconColor32 = loadImage( iconNameC32 );
                    return iconColor32;
                }
            case ICON_MONO_16x16:
                if ( iconNameM16 == null )
                    return null;
                else {
                    if( iconMono16 == null )
                        iconMono16 = loadImage( iconNameM16 );
                    return iconMono16;
                }
            case ICON_MONO_32x32:
                if ( iconNameM32 == null )
                    return null;
                else {
                    if( iconMono32 == null )
                        iconMono32 = loadImage( iconNameM32 );
                    return iconMono32;
                }
            default: return null;
        }
    }
    
}

