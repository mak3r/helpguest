/*
 * InSight.java
 *
 * Created on January 29, 2005, 5:34 AM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */
package com.helpguest.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.jnlp.ServiceManager;
import javax.jnlp.SingleInstanceListener;
import javax.jnlp.SingleInstanceService;
import javax.jnlp.UnavailableServiceException;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import org.apache.log4j.Logger;

import com.helpguest.HelpGuestMode;
import com.helpguest.util.Util;

/**
 *
 * @author  mabrams
 */
public class HostUI extends JPanel implements SingleInstanceListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static GuestUserPanel gup = null;
    private static JFrame jf = null;
    private JMenuItem copyMenuItem;
    private JMenuItem cutMenuItem;
    private JMenu editMenu;
    private JMenuItem exitMenuItem;
    private JMenu fileMenu;
    private JSeparator jSeparator1;
    private static JMenuBar menuBar;
    private JMenuItem pasteMenuItem;
    private JMenuItem settingsMenuItem;
    private static int oldWidth;
    private static int newWidth;
    private static int oldHeight;
    private static int newHeight;
    private static SingleInstanceService sis;
    //This is constant across the entire application
    public static String UID;
    private static final Logger log = Logger.getLogger("com.helpguest.ui.HostUI");
    private static final boolean DEBUG = log.isDebugEnabled();

    /** Creates a new instance of InSight */
    public HostUI() {
        initComponents();
    }

    private void initComponents() {
        try {
            gup = new GuestUserPanel();
            this.setLayout(new BorderLayout());
            this.add(gup, BorderLayout.CENTER);
            int width = (int) (gup.getPreferredSize().getWidth() + 45);
            int height = (int) (gup.getPreferredSize().getHeight() + 25);
            if (log.isInfoEnabled()) {
                log.info("width/height : " + width + "/" + height);
            }
            this.setPreferredSize(new Dimension(width, height));
            this.setVisible(true);
            //createMenuBar();
            gup.setChatVisible(false);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createMenuBar() {
        menuBar = new JMenuBar();
        fileMenu = new JMenu();
        exitMenuItem = new JMenuItem();
        editMenu = new JMenu();
        cutMenuItem = new JMenuItem();
        copyMenuItem = new JMenuItem();
        pasteMenuItem = new JMenuItem();
        jSeparator1 = new JSeparator();
        settingsMenuItem = new JMenuItem();

        //File menu
        fileMenu.setMnemonic('F');
        fileMenu.setText("File");
        exitMenuItem.setText("Exit");
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        //Edit Menu
        editMenu.setText("Edit");
        cutMenuItem.setMnemonic('t');
        cutMenuItem.setText("Cut");
        editMenu.add(cutMenuItem);

        copyMenuItem.setMnemonic('c');
        copyMenuItem.setText("Copy");
        editMenu.add(copyMenuItem);

        pasteMenuItem.setMnemonic('p');
        pasteMenuItem.setText("Paste");
        editMenu.add(pasteMenuItem);

        editMenu.add(jSeparator1);

        settingsMenuItem.setMnemonic('s');
        settingsMenuItem.setText("settings");
        editMenu.add(settingsMenuItem);

        menuBar.add(editMenu);
    }

    public void newActivation(String[] args) {
        //TODO: popup a window that 
        if (log.isDebugEnabled()) {
            for (int i = 0; i < args.length; i++) {
                log.debug("parameter " + i + " is " + args[i]);
            }
        }

        this.setVisible(false);
        this.setVisible(true);
    }

    private static void updateSize() {
        oldWidth = (int) jf.getSize().getWidth();
        oldHeight = (int) jf.getSize().getHeight();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final DebugPanel dp = new DebugPanel();
        javax.swing.JFrame f = com.helpguest.util.Util.frameIt(dp);
        f.setTitle("HelpGuest Client");
        com.helpguest.util.Util.centerWindow(f);

        f.setVisible(true);
        try {
            System.out.println("Waiting");
            synchronized (dp) {
                while (!dp.timerComplete()) {
                    dp.wait(1000);
                }
            }
            System.out.println("Done waiting");
        } catch (InterruptedException iex) {
            iex.printStackTrace(System.out);
        }
        f.setVisible(false);
        f.dispose();

        if (args.length < 2) {
            log.error("Invalid number of parameters passed in.  Expected 2, got " + args.length);
            System.exit(0);
        }

        try {
            java.net.InetAddress inetAddr = java.net.Inet4Address.getLocalHost();
            log.debug("server socket inet addr: " + inetAddr);
        } catch (Exception ex) {
        }

        final HostUI inSight = new HostUI();
        if (args[0].equalsIgnoreCase(HelpGuestMode.CLIENT.toString())) {
            gup.setMode(HelpGuestMode.CLIENT);
        } else if (args[0].equalsIgnoreCase(HelpGuestMode.EXPERT.toString())) {
            gup.setMode(HelpGuestMode.EXPERT);
        } else {
            log.error("Invalid mode specified " + args[0] + ", aborting.");
            //TODO add a messagebox here that indicates that the jnlp
            // file (launch program) is out of date and please use the 
            // web to get a current 
            // launch program
            System.exit(0);
        }
        //assign the unique id
        UID = args[1];


        try {
            sis = (SingleInstanceService) ServiceManager.lookup("javax.jnlp.SingleInstanceService");
            // Register the single instance listener at the start of your application
            sis.addSingleInstanceListener(inSight);
        } catch (UnavailableServiceException usx) {
            log.error("Unable to register with single instance service.", usx);
            sis = null;
        }

        jf = new JFrame("HelpGuest");
        jf.add(inSight);
        jf.setPreferredSize(new Dimension(inSight.getPreferredSize().width + 10,
                inSight.getPreferredSize().height + 40));


        jf.addWindowListener(new java.awt.event.WindowAdapter() {

            public void windowClosing(java.awt.event.WindowEvent evt) {
            	//TODO: possibly stop all remote connections here first
            	if (sis != null ) {
            		//sis may be null if this was not launched via webstart
                    //Remove the sis listener
            		sis.removeSingleInstanceListener(inSight);
            	}
                if (log.isDebugEnabled()) {
                    log.debug("Exiting ... preparing survey first");
                }
                SurveyDialog surveyDialog = gup.getSurveyDialog();
                //prahalad ddding 2/11/09
                ShowSurveyDecisionDialog showSurveyDecisionDialog = gup.getSurveyDecisionDialog();
                
                if (surveyDialog == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("The surveyDialog is null.");
                    }
                    gup.terminate();
                    System.exit(0);
                }
                log.debug("centering survey dialog");
                Util.centerWindow(surveyDialog);
                log.debug("centering survey descision dialog");
                Util.centerWindow(showSurveyDecisionDialog);
                log.debug("hiding client desktop app window");
                jf.setVisible(false);
                //It's important to terminate the GuestUserPanel before running
                // the survey.  Terminate will close all other connections
                // including vnc and services access.
                log.debug("terminating guest user panel");
                gup.terminate();
                //log.debug("showing survey");
                //surveyDialog.setVisible(true);
                //prahalad ddding 2/11/09
                log.debug("showing survey decision");
                showSurveyDecisionDialog.setVisible(true);
            }
        });

        gup.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                if (DEBUG) {
                    log.debug("property changed: " + evt.getPropertyName());
                }
                if (MinimalHostPanel.CHAT_VISIBLE_PROPERTY == evt.getPropertyName()) {

                    if (!gup.isChatVisible()) {
                        updateSize();
                        jf.setVisible(false);
                        jf.setPreferredSize(new Dimension(oldWidth, 132));
                        jf.pack();
                        jf.setVisible(true);
                    } else {
                        jf.setVisible(false);
                        jf.setPreferredSize(new Dimension(oldWidth, oldHeight));
                        jf.pack();
                        jf.setVisible(true);
                        updateSize();
                    }
                }
            }
        });

        jf.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        jf.setJMenuBar(menuBar);
        jf.pack();
        Util.centerWindow(jf);
        jf.setVisible(true);
        updateSize();
    }
}
