/*
 * Agent.java
 *
 * Created on February 9, 2005, 4:20 PM
 * Copyright 2005 - HelpGuest Technologies, Inc.
 */

package com.helpguest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.protocol.Vocabulary;
import com.helpguest.service.HGPort;
import com.helpguest.service.Service;
import com.helpguest.ssh.TunnelManager;
import com.helpguest.ui.HostUI;

/**
 *
 * @author  mabrams
 */
public class ClientAgent extends Agent {

    private int portStart = 10000; //default starting port
    private int servicePort = 20001;
    //prahalad changing beta 6/4/08
    //private static final String hostname = "beta.helpguest.com";
    private static final String hostname = HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    private String user = null;
    private String issue = null;
    private Collection<HGPort> portList = new ArrayList<HGPort>();
    
    private static final Logger LOG = Logger.getLogger("com.helpguest.ClientAgent");
    
    
    /** Creates a new instance of Agent */
    public ClientAgent(String user, String issue) {
        super();        
        this.user = user;
        this.issue = issue;
    }
    
    public String getUid() {
        return null;
    }
    
    /**
     * @return true if the connection to the service host was successful.
     */
    protected boolean getServicePorts(String hostname, int port) {
        Service firstService = nextService();
        if (firstService == null) {
            //abort
            LOG.error("At least one service must be specified.");
            return false;
        }
        
        Socket rSocket = null;

        try {
            rSocket = new Socket(hostname, port);
            out = new PrintWriter(rSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(rSocket.getInputStream()));
        } catch (UnknownHostException e) {
            LOG.error("Don't know about host: " + hostname + ".", e);
            return false;
        } catch (IOException e) {
            LOG.error("Couldn't get I/O for the connection to: " + hostname + ".", e);
            return false;
        }

        String serverMsg = null;
        String agentMsg = null;
        Service cur = firstService;
        boolean allServicesHandled = false;
        
        try {
            while ((serverMsg = in.readLine()) != null) {
                if (LOG.isDebugEnabled()) {LOG.debug("server says: " + serverMsg); }
                
                if (Vocabulary.COM_BYE.equals(serverMsg)) {
                    break;
                } else if (Vocabulary.SERV_WELCOME.equals(serverMsg)) {                    
                    agentMsg = Vocabulary.COM_REQUEST;
                } else if (Vocabulary.SERV_REQUEST_TYPE.equals(serverMsg)) {
                    agentMsg = Vocabulary.CLI_ASSISTANCE;
                } else if (Vocabulary.SERV_WHAT_IS_THE_PROBLEM.equals(serverMsg)) {
                    agentMsg = this.issue;
                } else if (Vocabulary.SERV_NAME.equals(serverMsg)) {
                    agentMsg = this.user;
                } else if (Vocabulary.SERV_WHAT_UID.equals(serverMsg)) {
                    agentMsg = HostUI.UID;
                } else if (Vocabulary.SERV_WHAT_SERVICE.equals(serverMsg)) {
                    agentMsg = cur.getName();
                } else if (allServicesHandled) {
                    agentMsg = Vocabulary.COM_THANKS;
                } else {                
                    try {
                        int servPort = Integer.parseInt(serverMsg);
                        HGPort hgPort = new HGPort(servPort, cur);
                        portList.add(hgPort);
                        cur = nextService();
                        //send a service request
                        agentMsg = cur.getName();
                        if (cur == firstService) {
                            allServicesHandled = true;
                        }                         
                    } catch (NumberFormatException nfx) {
                        LOG.error("Server message: '" + serverMsg + "' is not recognized.  Agent aborting.");
                        return false;
                    }
                }
                if (LOG.isDebugEnabled()) { LOG.debug("agent reply: " + agentMsg); }
                out.println(agentMsg);
                
            }//end of while((serverMsg = in.readLine()) != null)
        } catch (IOException iox) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Connection closed by remote host.");
            }
        } finally {
            try {
                out.close();
                in.close();
                rSocket.close();
            } catch (IOException iox) {
                LOG.error("Error closing a stream or socket.");
            }
        }
        hgPorts = new ArrayList<HGPort>(portList);
        return true;
    }
    
    protected boolean openTunnel(
    		final int localPort, 
    		final int remotePort, 
    		final Service service, 
    		final TunnelManager host) {
        if (service == Service.CHAT || service == Service.VNC) {
        	return host.openTunnelRF(localPort, remotePort, service.getName());
        } else if (service == Service.AGENT) {
            return host.openTunnelLF(localPort, remotePort, service.getName());
        } else {
            //Speaker is local forwarded for the client
            //TEMPORARY: DONT OPEN TUNNEL FOR SPEAKER OR MIC
            //return host.openTunnelLF(localPort, remotePort, service.getName());
            return true;
        }
    }
    
    protected boolean openTunnel(
    		final HGPort hgPort, 
    		final int localPort, 
    		final TunnelManager host) {
        if (hgPort.getService() == Service.VNC) {
        	return openTunnel(5900, hgPort.getPortNumber(), hgPort.getService(), host);	
        }
        return openTunnel(localPort, hgPort.getPortNumber(), hgPort.getService(), host);
    }
    
    
}
