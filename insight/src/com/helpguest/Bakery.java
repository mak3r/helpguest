/*
 * Util.java
 *
 * Created on April 27, 2005, 6:43 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.jnlp.BasicService;
import javax.jnlp.FileContents;
import javax.jnlp.JNLPRandomAccessFile;
import javax.jnlp.PersistenceService;
import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;
import javax.swing.JFrame;
import org.apache.log4j.Logger;


/**
 *
 * @author mabrams
 */
public class Bakery {
    
    /** The key/value separator is the non-printable bell character */
    private static final char KEY_VALUE_SEPARATOR = 0x07;
    /** If the cookie is in the map, it's cached */
    private static Map<URL,Map> cookieMap = new HashMap<URL,Map>();
    private static final int MAX_SIZE = 25000;
    private static PersistenceService persistenceService;
    private static BasicService basicService;

    private static final Logger logger = Logger.getLogger("com.helpguest.Bakery");
    
    /** Creates a new instance of Util */
    public Bakery() {
    }
    
    /**
     * Update the cookie on the server
     */
    public static void persistCookie(URL url) {
        initializeCookie(url);
        try {
            persistenceService = (PersistenceService)ServiceManager.lookup("javax.jnlp.PersistenceService");
            if (persistenceService.getTag(url) == PersistenceService.DIRTY) {
                Map<String,String> cachedCookieContents = getCookie(url);
                //remove the old cookie
                deleteCookie(url);
                //generate a new one from the cached data
                persistenceService.create(url, MAX_SIZE);
                FileContents fileContents = persistenceService.get(url);
                JNLPRandomAccessFile randomAccessFile = fileContents.getRandomAccessFile("rw");
                for(Map.Entry<String,String> content: cachedCookieContents.entrySet()) {
                    randomAccessFile.writeBytes(content.getKey() + KEY_VALUE_SEPARATOR + 
                                                content.getValue() + "\n");
                }
                randomAccessFile.close();
            }
        } catch (UnavailableServiceException usx) { 
            logger.error("Unable to persist cookie.", usx);
        } catch (MalformedURLException mux) {
            logger.error("Invlid url: " + url + "could not persist cookie.", mux);
        } catch (IOException iox) {
            logger.error("Could not persist cookie.", iox);
        }    
    }

    /**
     * Add the key/value pair to the cookie.
     * If the cookie does not exist, it will be created.
     * This cookie will be marked DIRTY unless it is a new cookie
     */
    public static void addCookieContent(URL url, String key, String value) {        
        getCookie(url).put(key,value);
        markCookie(url, PersistenceService.DIRTY);
        if (logger.isDebugEnabled()) {
            logger.debug("added cookie content <key/value>" + 
                         key + "/" + value);
        }
    }
    
    /**
     * Remove the cookie content that matches this key
     * @returns the value of the key that is removed
     */
    public static String removeCookieContent(URL url, String key) {
        String value = getCookie(url).remove(key);
        markCookie(url, PersistenceService.DIRTY);
        return value;
    }
    
    /**
     * Replace the value of this key with the given value.
     * This is a wrapper around the add and remove methods.
     * @returns the old value
     */
    public static String replaceCookieContent(URL url, String key, String replacementValue) {
        String oldValue = removeCookieContent(url, key);
        addCookieContent(url, key, replacementValue);
        return oldValue;
    }
    
    /**
     * Get the value for the given key at this cookie url.
     * @return the value of this key
     */
    public static String getCookieValue(URL url, String key) {
        return getCookie(url).get(key);
    }

    /**
     * Get the keys in this cookie.
     * @return a Collection of Strings that are the cookie keys
     */
    public static Collection<String> getCookieKeys(URL url) {
        return getCookie(url).keySet();
    }
    
    /**
     * Remove this cookie.
     */
    public static void deleteCookie(URL url) {
        try { 
            persistenceService = (PersistenceService)ServiceManager.lookup("javax.jnlp.PersistenceService");
            persistenceService.delete(url);
            //Now delete it from the map
            cookieMap.remove(url);
        } catch (UnavailableServiceException usx) { 
            logger.error("Unable to delete cookie.", usx);
        } catch (MalformedURLException mux) {
            logger.error("Invlid url: " + url + "could not delete cookie.", mux);
        } catch (IOException iox) {
            logger.error("Could not delete cookie.", iox);
        }    
    }
    
    /**
     * If the cookie exists do nothing.
     * If the cookie does not exist, create it
     */
    private static void initializeCookie(URL url) {
        try {
            persistenceService = (PersistenceService)ServiceManager.lookup("javax.jnlp.PersistenceService");
            try {
                //See if the cookie exists by checking it's tag value
                persistenceService.getTag(url);
            } catch (IOException iox) {
                //cookie does not exist create it
                persistenceService.create(url, MAX_SIZE);
            }             
        } catch (UnavailableServiceException usx) { 
            logger.error("Unable to create cookie. Persistence service unavailable", usx);
        } catch (MalformedURLException mux) {
            logger.error("Invlid url: " + url + "could not persist cookie.", mux);
        } catch (IOException iox) {
            logger.error("Could not create cookie and one does not exist.", iox);
        }
    }
    
    private static Map<String,String> getCookie(URL url) {
        if (cookieMap.get(url) == null) {
            cookieMap.put(url,getCookieContents(url));
        }
        return (Map<String,String>) cookieMap.get(url);
    }
    
    
    /**
     * Mark the cookie named by this url as one of the PersistenceService types:
     * CACHED, DIRTY, or TEMPORARY
     * @return true of the operation was sucessfully performed
     */
    private static boolean markCookie(URL url, int tag) {
        initializeCookie(url);
        try {
            //Mark the cookie
            persistenceService = (PersistenceService)ServiceManager.lookup("javax.jnlp.PersistenceService");
            persistenceService.setTag(url, tag);
            return true;
        } catch (MalformedURLException mux) {
            logger.error("Url: " + url + " is not valid.", mux);
        } catch (IOException iox) {
            logger.error("Could not update cookie: " + url, iox);
        } catch (UnavailableServiceException usx) { 
            logger.error("Unable to update cookie.", usx);
        }
        return false;
    }
    
    
    /**
     * Get all the key/value pairs that make up this cookie content.
     * @returns a Map<String,String> of key/value pairs
     */
    private static Map<String,String> getCookieContents(URL url) {
        Map<String,String> keyValueContent = new HashMap<String, String>();
        initializeCookie(url);
        try {
            persistenceService = (PersistenceService)ServiceManager.lookup("javax.jnlp.PersistenceService");
            FileContents fileContents = persistenceService.get(url);
            JNLPRandomAccessFile randomAccessFile = fileContents.getRandomAccessFile("r");
            String line;
            while ((line = randomAccessFile.readLine()) != null) {
                String key = line.substring(0,line.indexOf(KEY_VALUE_SEPARATOR));
                String value = line.substring(line.indexOf(KEY_VALUE_SEPARATOR)+1);
                keyValueContent.put(key,value);
                if (logger.isDebugEnabled()) {
                    logger.debug("discovered cookie content <key/value>" + 
                                 key + "/" + value);
                }
            }
            randomAccessFile.close();
            //Mark the cookie as cached
            if (keyValueContent.keySet().size() > 0) {
                markCookie(url, PersistenceService.CACHED);
            }
        } catch (MalformedURLException mux) {
            logger.error("Url: " + url + " is not valid.", mux);
        } catch (IOException iox) {
            logger.error("Could not read or update cookie: " + url, iox);
        } catch (UnavailableServiceException usx) { 
            logger.error("Unable to get cookie contents.", usx);
        }
                
        return keyValueContent;
    }
    
}
