/**
 * 
 */
package com.helpguest.remoteaccess;

import java.net.InetAddress;

import org.apache.log4j.Logger;

/**
 * @author mabrams
 *
 */
public class VNCViewerParameters {

	private static final Logger LOG = Logger.getLogger(VNCViewerParameters.class);
	
	private String password;
	
	private InetAddress serverInetAddress;

	private int port;
	
	private String hostname;

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the serverInetAddress
	 */
	public InetAddress getServerInetAddress() {		
		return serverInetAddress;
	}

	/**
	 * @param serverInetAddress the serverInetAddress to set
	 */
	public void setServerInetAddress(InetAddress serverInetAddress) {
		this.serverInetAddress = serverInetAddress;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the hostname
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * @param hostname the hostname to set
	 * also trys to set the InetAddress using this hostname.
	 */
	public void setHostname(String hostname) {
		this.hostname = hostname;
		
		//Try and set the inet address
		if (hostname != null) {
			try {
				serverInetAddress = InetAddress.getByName(hostname);
			} catch(Exception ex) {
				LOG.error("Unable to set InetAddress by name: " + hostname, ex);
				try {
					serverInetAddress = InetAddress.getLocalHost();
				} catch (Exception ex2) {
					LOG.error("Unable to set the InetAddress", ex2);
				}
			}
		}

	}

	
	
}
