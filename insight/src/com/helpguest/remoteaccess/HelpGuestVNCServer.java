package com.helpguest.remoteaccess;

public interface HelpGuestVNCServer {

	public abstract void start();

	public abstract void startViewOnly();

	public abstract void stop();

}