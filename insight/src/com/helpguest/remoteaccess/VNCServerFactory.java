package com.helpguest.remoteaccess;

public class VNCServerFactory {

	private static HelpGuestVNCServer vncServer;
	private static final String CURRENT_OS_NAME = System.getProperty("os.name");
	private static final String OS_MAC = "Mac OS X";
	
	private VNCServerFactory() {
		
	}
	
	public static HelpGuestVNCServer getVNCServer() {
		if (vncServer == null) {
			if (CURRENT_OS_NAME.equals(OS_MAC)) {
				vncServer = new VNCMac();
			} else {
				//for now, if it's not a mac, we assume it's windows
				vncServer = new VNCWindows();
			}
		}
		
		return vncServer;
	}
	
	
	
}
