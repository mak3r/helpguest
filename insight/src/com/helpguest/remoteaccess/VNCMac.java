/**
 * 
 */
package com.helpguest.remoteaccess;

import java.awt.Label;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.helpguest.util.ProcessMonitor;
import com.helpguest.util.Util;

/**
 * @author mabrams
 *
 */
public class VNCMac implements HelpGuestVNCServer {

	private static final Logger LOG = Logger.getLogger(VNCMac.class);

	private static final String USER_NAME = System.getProperty("user.name");
	private String userSecret;
	private boolean userValidated = false;
	private static final int RETRY_COUNT = 3;
	private boolean vncExecSuccess = false;

//	static final String encodedVNCPass = "615A321EEADBB6E2FF1C39567390ADCA";
	static final String encodedVNCPass = "vncpass";

	private static final String[] START_COMMANDS = {
		"sudo",
		"/System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart",
		"-configure",
		"-clientopts",
		"-setvncpw",
		"-vncpw",
		encodedVNCPass,
		"-setvnclegacy",
		"-vnclegacy",
		"yes",
		"-restart",
		"-agent"
	};
	
	/**
	 * This is not possible to set on the ARD vnc.
	 * It can however be managed from the viewer.
	 */
	private static final String[] VIEW_ONLY_COMMANDS = {
		"sudo",
		"/System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart",
		"-configure",
		"-clientopts",
		"-setvncpw",
		"-vncpw",
		encodedVNCPass,
		"-setvnclegacy",
		"-vnclegacy",
		"yes",
		"-restart",
		"-agent"
	};

	private static final String[] STOP_COMMANDS = {
		"sudo",
		"/System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart",
		"-configure",
		"-stop"
	};	

	private void startAction(final String[] commands) {

		if (!userValidated) {
			userValidated = userProvidedCredentials((String[]) null);
			if (!userValidated) {
				//they must have cancelled the request
				return;
			}
		} 

		Thread t = new Thread(new Runnable() {

			public void run() {
				if (!execute()) {
						int retriesRemaining = RETRY_COUNT;
						boolean success = execute();
						while (retriesRemaining-- > 0) {
							if (success) {
								vncExecSuccess = true;
								break;
							}
							vncExecSuccess = false;
						}
						if (!success) {
							userValidated = userProvidedCredentials("Remote access did not start.",  "Please retry.", "");
							if (userValidated) {
								startAction(commands);
							}
						}
					} else {
						//execution was successful.
						//flag it as such
						vncExecSuccess = true;
					}
			
			} 
			
			//Execute the commands - return true if the execution succeeded.
			private boolean execute() {
				final ProcessMonitor processMonitor = new ProcessMonitor("Done", 5000);
				try {
					if (LOG.isDebugEnabled()) {
						LOG.debug("enabling vnc server.");
					}			
					if (LOG.isDebugEnabled()) {
						StringBuffer sBuf = new StringBuffer();
						for(String next : commands) {
							sBuf.append(next + " ");
						}
						LOG.debug("vnc commands: '" + sBuf.toString() + "'");
					}
					Process proc = Runtime.getRuntime().exec(commands);
					if (userValidated) {
						proc.getOutputStream().write(userSecret.getBytes());
						proc.getOutputStream().flush();
					}
					Util.logProcessInput("VNCMac.start()", proc, processMonitor);
					Util.logProcessError("VNCMac.start()", proc, processMonitor);

					while (processMonitor.isWaiting()) {
						try {
							//wait a couple seconds
							Thread.sleep(2000);
							LOG.debug("Waiting for 'Done'.");
						} catch (InterruptedException iex) {
							LOG.info("Thread sleep interrupted.");
						}
					}
					if (!processMonitor.wasStopDiscovered()) {
						proc.destroy();
					}
				} catch (IOException iox) {
					LOG.error("Trouble running vnc start commands.", iox);
				}
				
				return processMonitor.wasStopDiscovered();
			}
			
		});
		t.start();
	}
	
	/* (non-Javadoc)
	 * @see com.helpguest.remoteaccess.HelpGuestVNCServer#start()
	 */
	public void start() {
		startAction(START_COMMANDS);
	}

	/* (non-Javadoc)
	 * @see com.helpguest.remoteaccess.HelpGuestVNCServer#startViewOnly()
	 */
	public void startViewOnly() {
		startAction(VIEW_ONLY_COMMANDS);
	}

	/* (non-Javadoc)
	 * @see com.helpguest.remoteaccess.HelpGuestVNCServer#stop()
	 */
	public void stop() {
		//make sure we actually exec'd the process before
		//requesting the password again.
		if (vncExecSuccess && !userValidated) {
			userValidated = userProvidedCredentials((String[]) null);
			if (!userValidated) {
				//they must have cancelled the request
				return;
			}
		} 
		Thread t = new Thread(new Runnable() {

			public void run() {
				if (!execute()) {
						int retriesRemaining = RETRY_COUNT;
						boolean success = execute();
						//This is a user unassisted retry
						while (retriesRemaining-- > 0) {
							if (success) {
								break;
							}
						}
						//Give the user the option to retry again if 
						// it still did not start 
						if (!success) {
							userValidated = userProvidedCredentials("Remote access did not terminate.",  "Please retry.", "");
							if (userValidated) {
								stop();
							}
						}
					}
				
			}
			
			//Execute the commands - return true if the execution succeeded.
			private boolean execute() {
				final ProcessMonitor processMonitor = new ProcessMonitor("Done", 5000);
				try {
					if (LOG.isDebugEnabled()) {
						LOG.debug("enabling vnc server.");
					}			
					if (LOG.isDebugEnabled()) {
						StringBuffer sBuf = new StringBuffer();
						for(String next : STOP_COMMANDS) {
							sBuf.append(next + " ");
						}
						LOG.debug("vnc commands: '" + sBuf.toString() + "'");
					}
					Process proc = Runtime.getRuntime().exec(STOP_COMMANDS);
					if (userValidated) {
						proc.getOutputStream().write(userSecret.getBytes());
						proc.getOutputStream().flush();
					}
					Util.logProcessInput("VNCMac.stop()", proc, processMonitor);
					Util.logProcessError("VNCMac.stop()", proc, processMonitor);

					while (processMonitor.isWaiting()) {
						try {
							//wait a couple seconds
							Thread.sleep(2000);
							LOG.debug("Waiting for 'Done'.");
						} catch (InterruptedException iex) {
							LOG.info("Thread sleep interrupted.");
						}
					}
					if (!processMonitor.wasStopDiscovered()) {
						proc.destroy();
					}
				} catch (IOException iox) {
					LOG.error("Trouble running vnc stop commands.", iox);
				}
				
				return processMonitor.wasStopDiscovered();
			}
			
		});
		t.start();
		//When stopping, reset the validated flag to false
		//re-validation will be required to restart.
		userValidated = false;
	}

	private boolean userProvidedCredentials(final String... optionalLabels) {
		//TODO this password dialog should be available globally to the app
		//TODO this password dialog should not get re-created every time it is shown
		//TODO add checkbox to allow re-use of the password for the duration of the session.
		JPanel passwordDialogPanel = new JPanel();
		passwordDialogPanel.setLayout(new BoxLayout(passwordDialogPanel, BoxLayout.Y_AXIS));

		JPanel userNamePanel = new JPanel();
		userNamePanel.setLayout(new BoxLayout(userNamePanel, BoxLayout.X_AXIS));

		JPanel passwordPanel = new JPanel();
		passwordPanel.setLayout(new BoxLayout(passwordPanel, BoxLayout.X_AXIS));

		JPanel inputFields = new JPanel();
		inputFields.setLayout(new BoxLayout(inputFields, BoxLayout.Y_AXIS));
		JTextField userName = new JTextField(USER_NAME, 18);
		JPasswordField secret = new JPasswordField(18);
		inputFields.add(userName);
		inputFields.add(Box.createVerticalStrut(2));
		inputFields.add(secret);

		JPanel labelFields = new JPanel();
		labelFields.setLayout(new BoxLayout(labelFields, BoxLayout.Y_AXIS));
		labelFields.add(new JLabel("Name: ", JLabel.TRAILING));
		labelFields.add(Box.createVerticalStrut(10));
		labelFields.add(new JLabel("Password: ", JLabel.TRAILING));

		JPanel inputLabelPanel = new JPanel();
		inputLabelPanel.setLayout(new BoxLayout(inputLabelPanel, BoxLayout.X_AXIS));
		inputLabelPanel.add(labelFields);
		inputLabelPanel.add(Box.createHorizontalStrut(2));
		inputLabelPanel.add(inputFields);

		if (optionalLabels != null) {
			for (String next : optionalLabels) {
				if (next != null) {
					passwordDialogPanel.add(new Label(next));
				}
			}
		}
		passwordDialogPanel.add(new Label("HelpGuest requires that you type your"));
		passwordDialogPanel.add(new Label("password."));
		passwordDialogPanel.add(Box.createVerticalStrut(40));
		passwordDialogPanel.add(inputLabelPanel);
		passwordDialogPanel.add(Box.createVerticalStrut(10));

		//last argument is the icon to display
		int outcome = JOptionPane.showConfirmDialog(
				null, 
				passwordDialogPanel,
				null,
				JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null);

		if (LOG.isDebugEnabled()) {
			LOG.debug("OK_CANCEL_OPTION value: " + JOptionPane.OK_CANCEL_OPTION);
			LOG.debug("CANCEL_OPTION value: " + JOptionPane.CANCEL_OPTION);
			LOG.debug("OK_OPTION value: " + JOptionPane.OK_OPTION);
			LOG.debug("outcome: " + outcome);
		}

		userSecret = new String(secret.getPassword());

		return outcome == JOptionPane.OK_OPTION;
	}
	
	/**
	 * Used for debugging/testing this component
	 * @param args
	 */
	public static void main(final String[] args) {
		org.apache.log4j.BasicConfigurator.configure(
				new org.apache.log4j.ConsoleAppender(
						new org.apache.log4j.SimpleLayout()));
		final HelpGuestVNCServer vncServer = VNCServerFactory.getVNCServer();		
		class StartAdapter extends java.awt.event.MouseAdapter {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				vncServer.start();				
			}			
		}
		javax.swing.JButton start = new javax.swing.JButton("Start");		
		start.addMouseListener(new StartAdapter());
		
		class StopAdapter extends java.awt.event.MouseAdapter {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				vncServer.stop();				
			}			
		}
		javax.swing.JButton stop = new javax.swing.JButton("Stop");		
		stop.addMouseListener(new StopAdapter());		
		
		JPanel startStopByePanel = new JPanel();
		startStopByePanel.add(start);
		startStopByePanel.add(stop);

		int outcome = JOptionPane.CANCEL_OPTION;
		while(outcome != JOptionPane.OK_OPTION) {
			outcome = JOptionPane.showConfirmDialog(
					null, 
					startStopByePanel, 
					"HelpGuest VNCMac Test", 
					JOptionPane.OK_CANCEL_OPTION, 
					JOptionPane.PLAIN_MESSAGE);
		}
		System.exit(0);
	}

}
