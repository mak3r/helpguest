/**
 * VNCWindows is the touch point to start a new vnc thread
 * from the winvnc.dll
 *
 * Copyright 2005
 * HelpGuest Technologies, Inc.
 */

package com.helpguest.remoteaccess;

import com.mg.helpguest.VNCJni;
import org.apache.log4j.Logger;

public class VNCWindows implements HelpGuestVNCServer {
    

    private static boolean isRunning = false;
    private static Thread vncOnThread;
    private static final Logger log = Logger.getLogger("com.helpguest.remoteaccess.VNCWindows");
    
    /* (non-Javadoc)
	 * @see com.helpguest.remoteaccess.HelpGuestVNCServer#start()
	 */
    public void start() {
        if (!isRunning) {
        	vncOnThread = new Thread(new Runnable() {
                    public void run() {
                        if (log.isInfoEnabled()) {log.info("vnc is starting.");}
                        VNCJni.startVNC();
                    }
                });
            if (log.isDebugEnabled()) { log.debug("Starting VNC thread."); }
            vncOnThread.setDaemon(true);
            vncOnThread.start();
        }
        isRunning = true;
    }
    
    /* (non-Javadoc)
	 * @see com.helpguest.remoteaccess.HelpGuestVNCServer#startViewOnly()
	 */
    public void startViewOnly() {
    	
    }
    
    /* (non-Javadoc)
	 * @see com.helpguest.remoteaccess.HelpGuestVNCServer#stop()
	 */
    public void stop() {
    	log.debug("attempting to stop vnc");
    	if (vncOnThread != null && vncOnThread.isAlive()) {
        	log.debug("interrupting 'on' thread");
    		vncOnThread.interrupt();
    	}
    	log.debug("checking if it is running");
    	if (isRunning) {
            Thread t = new Thread(new Runnable() {
                    public void run() {
                        if (log.isInfoEnabled()) {log.info("vnc is stopping.");}
                        VNCJni.stopVNC();
                    }
                });
            if (log.isDebugEnabled()) { log.debug("Stopping VNC thread."); }
            t.setDaemon(true);
            t.start();
        }
    	log.debug("setting vnc run flag to false");
        isRunning = false;
    }

}