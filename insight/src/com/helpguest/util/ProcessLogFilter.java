/**
 * 
 */
package com.helpguest.util;

/**
 * @author mabrams
 *
 */
public enum ProcessLogFilter {

	INPUT,
	ERROR;
	
}
