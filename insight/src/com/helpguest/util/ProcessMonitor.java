/**
 * ProcessMonitor was designed to monitor a 
 * java.lang.Process across threads.
 * 
 * One thread my start the Process while another
 * is assigned to handling the error stream and input stream
 * outputs.  In the case of a Process whose output
 * signifies a notable point in time for the starting
 * thread, it can give a hint by way of a string value
 * to the output thread(s).  
 * 
 * The ProcessMonitor is designed to be thread safe with a
 * fallback timeOut.  For example, if the stop String is
 * never identified, the ProcessMonitor would still
 * respond (currently there is no way to distinguish between
 * a successful identification of the stop String and
 * a timed out ProcessMonitor).  
 * 
 * The timeOut will only be respected after the 
 * ProcessMonitor.start() method is called.
 */
package com.helpguest.util;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

/**
 * @author mabrams
 *
 */
public class ProcessMonitor {

	boolean waiting = true;
	boolean stopDiscovered = false;
	String stopString;
	Timer t;
	long delay;
	private static final Logger LOG = Logger.getLogger(ProcessMonitor.class);
	
	/**
	 * use a string value to to get feedback regarding the output of the process.
	 * 
	 * @param stop is the String which will take the process monitor out of the stop state.
	 * @param timeOut is the duration to wait before stopping 
	 * the process should the stop value never be discovered
	 */
	public ProcessMonitor(final String stop, final long timeOut) {
		stopString = stop;
		t = new Timer("ProcessMonitor: " + stop);
		this.delay = timeOut;
	}
	
	/**
	 * Start the ProcessMonitor's timer.
	 */
	public synchronized void start() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Starting process monitor timer");
		}
		t.schedule(new TimerTask() {

			@Override
			public void run() {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Process monitor timer ended");
				}
				finish(false);
			}
			
		}, delay);
	}
	
	/**
	 * 
	 * @return the value of the stop String.
	 */
	public synchronized String getStopValue() {
		return stopString;
	}
	
	/**
	 * 
	 * @return true if the stop String has not been
	 * identified and the timer has not expired
	 */
	public synchronized boolean isWaiting() { 
		return waiting;
	}
	
	/**
	 * Once this process monitor is no longer waiting,
	 * isWaiting() this method may be used to find out
	 * how the monitor was finished (either by discovery of the 
	 * stop String or by timeout)
	 * 
	 * @return true if the stop String was discovered
	 * false otherwise (if it timed out)
	 */
	public synchronized boolean wasStopDiscovered() {
		return stopDiscovered;
	}
	
	/**
	 * Force the ProcessMonitor to finish waiting.
	 * Causes subsequent calls to isWaiting() to return 
	 * true.
	 * 
	 * @param stopDiscovered pass true if the stop String was
	 * discovered and false if this timed out because the timer
	 * expired.
	 */
	public synchronized void finish(final boolean stopDiscovered) {
		//either way stop waiting
		waiting = false;
		this.stopDiscovered = stopDiscovered;
	}
}
