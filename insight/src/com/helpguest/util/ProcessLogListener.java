/**
 * 
 */
package com.helpguest.util;

import java.util.List;


/**
 * @author mabrams
 *
 */
public interface ProcessLogListener {	
	
	/**
	 * 
	 * @param discovered is the String that was logged.
	 */
	public void report(final String discovered);
	
	/**
	 * 
	 * @return a List of String values the should fire an alert.
	 * When any string in this list is discovered, the report
	 * method is called with the string that was discovered.
	 */
	public List<String> getAlerts();
	
	/**
	 * This filter only reports for this log filter type
	 * @return the ProcessLogFilter. null value indicates that there 
	 * is no filtering and to process all logs
	 */
	public ProcessLogFilter getFilter();
}
