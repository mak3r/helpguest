/**
 * 
 */
package com.helpguest.util;


/**
 * @author mabrams
 *
 */
public abstract class ProcessLogInputListener implements ProcessLogListener {

	/* (non-Javadoc)
	 * @see com.helpguest.util.ProcessLogListener#getFilter()
	 */
	public ProcessLogFilter getFilter() {
		return ProcessLogFilter.INPUT;
	}

}
