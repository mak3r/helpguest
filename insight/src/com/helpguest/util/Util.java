/*
 * Util.java
 *
 * Created on April 27, 2005, 6:43 PM
 * HelpGuest Technologies, Inc. Copyright 2005 
 */

package com.helpguest.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import javax.swing.JFrame;

import org.apache.log4j.Logger;


/**
 *
 * @author mabrams
 */
public class Util {
   

    private static final Logger LOG = Logger.getLogger(Util.class);
    
    /** Creates a new instance of Util */
    public Util() {
    }
    
    public static void centerWindow(Frame frame) {
        Rectangle screen = new Rectangle(
            Toolkit.getDefaultToolkit().getScreenSize());
        Point center = new Point(
            (int) screen.getCenterX(), (int) screen.getCenterY());
        Point newLocation = new Point(
            center.x - frame.getWidth() / 2, center.y - frame.getHeight() / 2);
        if (screen.contains(newLocation.x, newLocation.y,
                            frame.getWidth(), frame.getHeight())) {
            frame.setLocation(newLocation);
        }
    } // centerWindow()

    public static void centerWindow(Dialog dialog) {
        Rectangle screen = new Rectangle(
            Toolkit.getDefaultToolkit().getScreenSize());
        Point center = new Point(
            (int) screen.getCenterX(), (int) screen.getCenterY());
        Point newLocation = new Point(
            center.x - dialog.getWidth() / 2, center.y - dialog.getHeight() / 2);
        if (screen.contains(newLocation.x, newLocation.y,
                            dialog.getWidth(), dialog.getHeight())) {
            dialog.setLocation(newLocation);
        }
    } // centerWindow()

    public static JFrame frameIt(Component component) {
        JFrame jf = new JFrame(component.getName() + "test");
        jf.addWindowListener(new java.awt.event.WindowAdapter(){
            public void windowClosing(java.awt.event.WindowEvent evt) {
                System.exit(0);
            }
        });
        jf.getContentPane().setLayout(new java.awt.BorderLayout());
        jf.getContentPane().add(component, BorderLayout.CENTER);
        jf.setSize((int)(component.getPreferredSize().getWidth() + 5), (int)(component.getPreferredSize().getHeight() + 50));
        jf.setVisible(true);
        return jf;
    }
    
    /**
     * Set the size of the given frame to a fraction of the screen size
     * @param frame is the frame whose size will be modified
     * @parm fraction is the measure between 0 and 1 of what size to make it
     *   eg. .5 makes the frame half the screen size in width and height
     */
    public static void setSize(Frame frame, double fraction) {
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();
        frame.setPreferredSize(new Dimension((int)(dim.getWidth() * fraction), 
                                             (int)(dim.getHeight() * fraction)));
        if (LOG.isDebugEnabled()) { LOG.debug("size is: " + frame.getPreferredSize()); }
    }

    public static void openBrowser(final String url) {
        Thread t = new Thread ( new Runnable() {
            public void run() {
                try {
                    Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
                } catch (IOException iox) {
                    LOG.error("Could not start browser", iox);
                }
            }
        });
        t.start();
    }
        
    /**
     * Log a Process's error stream.
     * The stream is handled as a character stream
     * 
     * @param methodName give us a hint as to whose actually doing the logging.
     * @param proc the Process to log.
     * @param processLogListener optional parameter to allow log output to be
     * handled.
     */
	public static void logProcessError(
			final String methodName, final Process proc,
			final ProcessMonitor monitor) {
		Thread errorThread = new Thread(new Runnable() {

			public void run() {
				LineNumberReader error = 
					new LineNumberReader(new InputStreamReader(proc.getErrorStream()));
				String line = null;
				try {
					if (monitor!=null) {
						monitor.start();
					}
					while ((line = error.readLine()) != null) {
						if (LOG.isDebugEnabled()) { LOG.debug(methodName + " [error]: " + line); }
						if (monitor != null && line.indexOf(monitor.getStopValue()) > -1 ) {
							LOG.info(methodName + " discovered stop value: " + line);
							monitor.finish(true);
						}
					}
					if (LOG.isDebugEnabled()) { LOG.debug("error stream ended."); }
				} catch (IOException iox) {
					LOG.error("Could not read process error stream.", iox);
				} /*finally {
					try {
						if (error != null) {
							if (LOG.isDebugEnabled()) { LOG.debug("closing error stream."); }
							error.close();
						}
					} catch (IOException iox) {
						LOG.info("Could not close error stream.  Did the application terminate it first?", iox);
					}
				}*/
			}
			
		});
		errorThread.start();
	}

	/**
     * Log a Process's input stream.
     * The stream is handled as a character stream.
     * 
     * @param methodName give us a hint as to whose actually doing the logging.
     * @param proc the Process to log.
     * @param processLogListener optional parameter to allow log output to be
     * handled.
     */
	public static void logProcessInput(
			final String methodName, final Process proc,
			final ProcessMonitor monitor) {
		Thread inputThread = new Thread(new Runnable() {

			public void run() {
				LineNumberReader input = 
					new LineNumberReader(new InputStreamReader(proc.getInputStream()));
				String line = null;
				try {
					if (monitor != null) {
						monitor.start();
					}
					while ((line = input.readLine()) != null) {
						LOG.debug(methodName + " [stdout]: " + line);
						if (monitor != null && line.indexOf(monitor.getStopValue()) > -1) {
							LOG.info(methodName + " discovered stop value: " + line);
							monitor.finish(true);
						}
					}
				} catch (IOException iox) {
					LOG.error("Could not read process input stream.", iox);
				} /* finally {
					try {
						if (input != null) {
							input.close();
						}
					} catch (IOException iox) {
						LOG.error("Could not close input stream.", iox);
					}
				}
				*/
			}
			
		});
		inputThread.start();
	}
    
    /**
     * Log a Process's input and error streams.
     * This method creates two independent threads, one
     * for error and one for input.  Both streams are 
     * handled as character streams.
     * 
     * @param methodName give us a hint as to whose actually doing the logging.
     * @param proc the Process to log.
     */
	public static void logProcess(final String methodName, final Process proc) {
		logProcessInput(methodName, proc, null);
		logProcessError(methodName, proc, null);
	}
	
}
