/*
 * AudibleNotification.java
 *
 * This class provides the methods needed to output sound
 * alerts through and application
 *
 * Created on November 1, 2006, 8:35 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.alert;

import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import org.apache.log4j.Logger;

public class AudibleNotification {

    private static final int BUFFER_SIZE = 128000;
    private static final Logger log = Logger.getLogger("com.helpguest.alert.AudibleNotification");

    /** Creates a new instance of AudibleNotification */
    public AudibleNotification() {
    }
    
    public static void beep() {
        System.out.print("\0007");
        System.out.flush();
    }
    
    public static void dingDong() {
        play("/com/helpguest/sounds/cashregister.wav");
    }
    
    public static void glassToast() {
        play("/com/helpguest/sounds/GlassToast.aif");
    }
    
    public static void welcome() {
    	play("/com/helpguest/sounds/welcome.wav");
    }

    protected static void play(String sound) {
        AudioInputStream ais = null;
        try {
            ais = AudioSystem.getAudioInputStream(AudibleNotification.class.getResource(sound));
        }
        catch (Exception ex) {
            log.error("Could not get the sound file: " + sound, ex);
        }

        AudioFormat audioFormat = ais.getFormat();
        SourceDataLine	line = null;
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
        try{
            line = (SourceDataLine) AudioSystem.getLine(info);
            line.open(audioFormat);
        }
        catch (LineUnavailableException lue) {
            log.error("could not get a line", lue);
        }
        catch (Exception ex) {
            log.error("could not play sound: " + sound, ex );
        }

        line.start();

        int bytesRead = 0;
        byte[] soundData = new byte[BUFFER_SIZE];
        while (bytesRead != -1) {
            try {
                    bytesRead = ais.read(soundData, 0, soundData.length);
            }
            catch (IOException ex) {
                log.error("could not read sound file.", ex);
            }
            if (bytesRead >= 0) {
                int bytesWritten = line.write(soundData, 0, bytesRead);
            }
        }

        line.drain();
        line.close();

    }
}
