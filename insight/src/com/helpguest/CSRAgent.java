/*
 * Agent.java
 *
 * Created on February 9, 2005, 4:20 PM
 * Copyright 2005 - HelpGuest Technologies, Inc.
 */

package com.helpguest;

import com.helpguest.HGServiceProperties.HGServiceProperty;
import com.helpguest.protocol.Vocabulary;
import com.helpguest.service.HGPort;
import com.helpguest.service.Service;
import com.helpguest.ssh.KeyManager;
import com.helpguest.ssh.SshToolsFactory;
import com.helpguest.ssh.TunnelManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.log4j.Logger;

/**
 *
 * @author  mabrams
 */
public class CSRAgent extends Agent {

    private String id = null;
    private Collection<HGPort> portList = new ArrayList<HGPort>();
    private static final int PORTS = 0;
    private static final int ADDRESS = 1;
    private int serviceMode = PORTS;
    //prahalad changing beta 6/4/08
    //private static final String hostname = "beta.helpguest.com";
    private static final String hostname = HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
    
    private static final Logger LOG = Logger.getLogger("com.helpguest.CSRAgent");
    
    
    /** Creates a new instance of Agent */
    public CSRAgent(String id) {
        super();        
        this.id = id;
    }
    
    public String getUid() {
        return id;
    }
    
    /**
     * @return true if the connection to the service host was successful.
     */
    protected boolean getServicePorts(String hostname, int port) {
        Service firstService = nextService();
        if (firstService == null) {
            //abort
            LOG.error("At least one service must be specified.");
            return false;
        }
        
        Socket rSocket = null;

        try {
            rSocket = new Socket(hostname, port);
            out = new PrintWriter(rSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(rSocket.getInputStream()));
        } catch (UnknownHostException e) {
            LOG.error("Don't know about host: " + hostname + ".", e);
            return false;
        } catch (IOException e) {
            LOG.error("Couldn't get I/O for the connection to: " + hostname + ".", e);
            return false;
        }

        String serverMsg = null;
        String agentMsg = null;
        Service cur = firstService;
        boolean allServicesHandled = false;
        if (LOG.isDebugEnabled()) {LOG.debug("firstService: " + firstService);}
        
        try {
            while ((serverMsg = in.readLine()) != null) {
                if (LOG.isDebugEnabled()) {LOG.debug("server says: " + serverMsg); }
                
                if (Vocabulary.COM_BYE.equals(serverMsg)) {
                    break;
                } else if (Vocabulary.SERV_WELCOME.equals(serverMsg)) {                    
                    agentMsg = Vocabulary.COM_REQUEST;
                } else if (Vocabulary.SERV_REQUEST_TYPE.equals(serverMsg)) {
                    agentMsg = Vocabulary.TECH_REQUEST_PERMISSION;
                } else if (Vocabulary.SERV_PERMISSION_TYPE.equals(serverMsg)) {
                    agentMsg = Vocabulary.TECH_TECHNICIAN;
                } else if (Vocabulary.SERV_WHAT_ISSUE_ID.equals(serverMsg)) {
                    agentMsg = id;
                } else if (Vocabulary.SERV_WHAT_SERVICE.equals(serverMsg)) {
                    if (getHostInetAddress() == null) {
                        agentMsg = Vocabulary.COM_INET_ADDRESS;
                        serviceMode = ADDRESS;
                    } else {
                        agentMsg = cur.getName();
                        serviceMode = PORTS;
                    }
                } else if (allServicesHandled) {
                    agentMsg = Vocabulary.COM_THANKS;
                } else {
                    
                    switch (serviceMode) {
                        case PORTS: {
                            try {
                                int servPort = Integer.parseInt(serverMsg);
                                HGPort hgPort = new HGPort(servPort, crossConnect(cur));
                                portList.add(hgPort);
                                cur = nextService();
                                if (LOG.isDebugEnabled()) {LOG.debug("next service: " + cur);}
                                //send a service request
                                agentMsg = cur.getName();
                                if (cur == firstService) {
                                    allServicesHandled = true;
                                    if (LOG.isDebugEnabled()) {LOG.debug("All services handled");}
                                }                         
                            } catch (NumberFormatException nfx) {
                                LOG.error("Server message: '" + serverMsg + "' is not recognized.  Agent aborting.");
                                return false;
                            } catch (Exception ex) {
                                LOG.error("did not complete the service acquisition process.", ex);
                            }
                            break;
                        }
                        case ADDRESS: {
                            try {
                                setHostInetAddress(InetAddress.getByName(serverMsg));
                                agentMsg = cur.getName();
                            } catch (UnknownHostException uhx) {
                                LOG.error("Invalid address sent by server.", uhx);
                            } 
                            serviceMode = PORTS;
                            break;
                        }                            
                        default:
                            break;
                    }
                }
                if (LOG.isDebugEnabled()) { LOG.debug("agent reply: " + agentMsg); }
                out.println(agentMsg);
                
            }//end of while((serverMsg = in.readLine()) != null)
        } catch (IOException iox) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Connection closed by remote host.");
            }
        } finally {
            try {
                out.close();
                in.close();
                rSocket.close();
            } catch (IOException iox) {
                LOG.error("Error closing a stream or socket.");
            }
        }
        hgPorts = new ArrayList<HGPort>(portList);
        return true;
    }
    
    public String getId() {
        return id;
    }
    
    /**
     * The customer service rep's mic needs to be connected to the 
     * clients speaker and the csr's speaker, needs to be connected
     * to the clients mic.
     * @deprecated MIC and SPEAKER services are deprecated.  Use VOIP instead
     */
    protected Service crossConnect(Service service) {
        //MIC and SPEAKER services are deprecated
        return service;
    }
    
    protected boolean openTunnel(
    		final int localPort, 
    		final int remotePort, 
    		final Service service, 
    		final TunnelManager host) throws IllegalStateException {
        if (service == Service.CHAT || service == Service.VNC || service == Service.AGENT ) {
            //speaker, vnc, and chat are local forwarded for the csr
        	return host.openTunnelLF(localPort, remotePort, service.getName());
        } else {
            //TEMPORARY: DONT OPEN TUNNEL FOR MIC OR SPEAKER
            //return host.openTunnelRF(localPort, remotePort, service.getName());
            return true;
        }

    }
    
    protected boolean openTunnel(HGPort hgPort, int localPort, TunnelManager host) {
        return openTunnel(localPort, hgPort.getPortNumber(), hgPort.getService(), host);
    }
    
    protected TunnelManager getTunnelManager() {
    	return SshToolsFactory.getEmbeddedTunnelManager();
    }
    
    protected KeyManager getKeyManager() {
    	return SshToolsFactory.getEmbeddedKeyManager();
    }
    
}
