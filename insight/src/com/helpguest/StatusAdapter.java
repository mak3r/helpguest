/*
 * StatusAdapter.java
 *
 * Created on August 7, 2005, 8:20 PM
 */

package com.helpguest;

/**
 * Copyright 2005 - HelpGuest Technologies, Inc.
 * @author mabrams
 */
public class StatusAdapter implements StatusInterface {
    
    public static final int FIRST = 0;
    /* 
     * COMPLETE indicates that the current operation is complete but
     * finish indicates that there are no more operations
     */
    public static final int COMPLETE = -1; 
    public static final int MIDDLE = 5;
    
    /** Creates a new instance of StatusAdapter */
    public StatusAdapter() {
    }

    public void updateStatus(String status, int amt) {
    }
    
    public int next() {
        return 0;
    }
    
    public void finish() {
        
    }
    
    public void finish(String message) {
        
    }
    
}
