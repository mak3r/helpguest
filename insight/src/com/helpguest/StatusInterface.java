/*
 * StatusInterface.java
 *
 * Created on August 7, 2005, 6:16 PM
 * HelpGuest Technologies, Inc. Copyright 2005
 */

package com.helpguest;

/**
 *
 * @author mabrams
 */
public interface StatusInterface {
    /**
     * Update the status text and value
     */
    public void updateStatus(String status, int amt);
    
    /**
     * Return the last amt passed to the updateStatus method
     */
    public int next();
    
    /**
     * indication that there will be no more status updates
     */
    public void finish();
    
    /**
     * indication that there will be no more status updates
     * with a message left in the status bar.
     */
    public void finish(String message);
}
