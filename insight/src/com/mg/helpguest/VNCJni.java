package com.mg.helpguest;

/**
 * VNCJni.java
 *
 *
 * Created: Fri Jun 13 05:26:07 2003
 * HelpGuest Technologies, Inc. Copyright 2005 
 *
 * @author <a href="mailto:mabrams@CRANE"></a>
 * @version 1.0
 */
public class VNCJni {

    //Native method names
    public static native int startVNC();
    public static native int stopVNC();
    public static native int viewOnly();

    static {
        System.loadLibrary("VNCHooks");
        System.loadLibrary("WinVNC");
    }

    public VNCJni() {
        
    } // VNCJni constructor

    public static void main(String[] args) {
        //startVNC();
    }
    
} // VNCJni
