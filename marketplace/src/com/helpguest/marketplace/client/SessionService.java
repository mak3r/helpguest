/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.helpguest.marketplace.client.dto.ExpertAccountDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;

/**
 * @author mabrams
 *
 */
public interface SessionService extends RemoteService {

	public SessionDTO createSession(final String expertEmail);
	
	public SessionDTO createDemoSession(final String expertEmail);
	
	public SessionDTO getDemoSession(final ExpertDTO expert);

	public SessionDTO getSession(final String sessionId);
	
	/*
	 * @param ea an ExpertAccount
	 * @return a list of SessionDTOs for this expert account
	 */
	public List<com.helpguest.marketplace.client.dto.SessionDTO> getAllSessions(final ExpertAccountDTO eaDTO);
	
	/*
	 * @param ea an ExpertAccount
	 * @return a list of SessionDTOs for this expert account
	 */
	public List<com.helpguest.marketplace.client.dto.SessionDTO> getUnpaidSessions(final ExpertAccountDTO eaDTO);

}
