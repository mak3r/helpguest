package com.helpguest.marketplace.client.history;

public interface SubCommandToken extends CommandToken {

	void setParent(final CommandToken parentCommand);
}
