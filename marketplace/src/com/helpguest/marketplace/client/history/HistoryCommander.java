/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 30, 2007
 */
package com.helpguest.marketplace.client.history;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.HistoryListener;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.WindowCloseListener;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.util.Jaws;

/**
 * 
 * 
 * @author mabrams
 *
 */
public class HistoryCommander implements HistoryListener, WindowCloseListener {

	private final Map commandTokenMap = new HashMap();
	private static final String HISTORY_COOKIE = "HISTORY_COOKIE";
	//public final static String BASE_SEPARATOR = "+"; //?
	//public final static String PARAM_SEPARATOR = "&"; //&
	//public final static String VALUE_SEPARATOR = "="; //=
	//public final static String BASE_SEPARATOR = "?";
	public final static String BASE_SEPARATOR = "?";
	public final static String PARAM_SEPARATOR = "&"; //&
	public final static String VALUE_SEPARATOR = "="; //=
	
	private String previousToken = "";
	private static boolean DEBUG = false;
	private static boolean DEBUG_HISTORY_CHANGE = true;
	private static boolean DEBUG_BUILD_TOKENS = false;
	
	//private final static String EXTERNAL_BASE_SEPARATOR = "%2B"; //+
	//private final static String EXTERNAL_BASE_SEPARATOR = "%3F"; //?
	private final static String EXTERNAL_BASE_SEPARATOR = "%3F";
	private final static String EXTERNAL_PARAM_SEPARATOR = "%26"; //&
	private final static String EXTERNAL_VALUE_SEPARATOR = "%3D"; //=
	
	//private final static String EXTERNAL_BASE_SEPARATOR = "&";
    //private final static String EXTERNAL_PARAM_SEPARATOR = "&"; //&
	//private final static String EXTERNAL_VALUE_SEPARATOR = "="; //=
	
	private static final HistoryCommander historyCommander =
		new HistoryCommander();
	
	/**
	 * Add the panel that each history item will be
	 * inserted into.
	 * 
	 * @param simplePanel
	 */
	private HistoryCommander() {
		History.addHistoryListener(this);
		Window.addWindowCloseListener(this);
	}
	
	public static HistoryCommander getInstance() {
		return historyCommander;
	}	
		
	public void register(final CommandToken ct) {
		if (!Jaws.isSafari()) {
			commandTokenMap.put(getBaseToken(ct.getBaseToken()), ct);
		} else {
			commandTokenMap.put(getSafariBaseToken(ct.getBaseToken()), ct);
		}
	}

	/**
	 * Handle history change by going from more specific
	 * to less specific histories.
	 * 1) first check if the entire token is contained in the 
	 * command token map.
	 * 2) otherwise check if the base token is contained
	 * in the command token map.
	 */
	public void onHistoryChanged(final String historyToken) {
		//Change to true if we want to debug
		if (DEBUG_HISTORY_CHANGE) {
			debug(historyToken);
			DebugPanel.setText("HistoryCommander", "HistoryCommander.onHistoryChanged() stack");
			new Throwable().printStackTrace();
		}
		
		String baseToken;
		String paramList;
		
		if (!Jaws.isSafari()) {
			baseToken  = getBaseToken(historyToken);
			paramList = getParameters(historyToken);
		} else {
			baseToken = getSafariBaseToken(historyToken);
			paramList = getSafariParameters(historyToken);
		}
		
		if (commandTokenMap.containsKey(baseToken)) {
			if (DEBUG_HISTORY_CHANGE) {
				DebugPanel.setText("HistoryCommander", "Calling onEntry() method for command: " + 
						((CommandToken)commandTokenMap.get(baseToken)).getBaseToken());
			}
			if (!Jaws.isSafari()) {
				((CommandToken) commandTokenMap.get(baseToken)).onEntry(
					getParameterMap(paramList));
			} else {
				((CommandToken) commandTokenMap.get(baseToken)).onEntry(
						getSafariParameterMap(paramList));				
			}
		}

		//Now execute the prior token exit command
		if (commandTokenMap.containsKey(previousToken)) {
			((CommandToken) commandTokenMap.get(previousToken)).onExit();			
		}
		
		//finally set the previous token to this historyToken
		previousToken = baseToken;
	}
		
	/**
	 * 
	 * @param historyToken String
	 * @return the value preceding the BASE_SEPARATOR or
	 * the input parameter if no BASE_SEPARATOR is found.
	 */
	private static String getBaseToken(final String historyToken) {
		if (DEBUG) {
			DebugPanel.setText("HistoryCommander", "Getting baseToken from: " + historyToken);
			DebugPanel.setText("HistoryCommander", "Index of base separator: " + historyToken.indexOf(BASE_SEPARATOR));
		}
		
		String baseToken = historyToken;
		if (historyToken.indexOf(BASE_SEPARATOR) > -1) {
			baseToken = historyToken.substring(0, historyToken.indexOf(BASE_SEPARATOR));
		}

		if (DEBUG) { DebugPanel.setText("HistoryCommander", "using base token value: '" + baseToken + "'"); }
		
		return baseToken;
	}

	/**
	 * 
	 * @param historyToken String
	 * @return the value preceding the BASE_SEPARATOR or
	 * the input parameter if no BASE_SEPARATOR is found.
	 */
	private static String getSafariBaseToken(final String historyToken) {
		if (DEBUG) {
			DebugPanel.setText("HistoryCommander", "Getting safari baseToken from: " + historyToken);
			DebugPanel.setText("HistoryCommander", "Index of safari base separator: " + historyToken.indexOf(EXTERNAL_BASE_SEPARATOR));
		}
		
		String baseToken = historyToken;
		if (historyToken.indexOf(EXTERNAL_BASE_SEPARATOR) > -1) {
			baseToken = historyToken.substring(0, historyToken.indexOf(EXTERNAL_BASE_SEPARATOR));
		} else {
			baseToken = getBaseToken(historyToken);
		}

		if (DEBUG) { DebugPanel.setText("HistoryCommander", "using safari base token value: '" + baseToken + "'"); }
		
		return baseToken;
	}

	/**
	 * 
	 * @param historyToken String
	 * @return the String following the BASE_SEPARATOR or
	 * the empty string if no BASE_SEPARATOR is found.
	 */
	private static String getParameters(final String historyToken) {
		if (historyToken.indexOf(BASE_SEPARATOR) > -1) {
			return historyToken.substring(
					historyToken.indexOf(BASE_SEPARATOR)+1);
		}
		return historyToken;		
	}
	
	/**
	 * 
	 * @param historyToken String
	 * @return the String following the BASE_SEPARATOR or
	 * the empty string if no BASE_SEPARATOR is found.
	 */
	private static String getSafariParameters(final String historyToken) {
		if (historyToken.indexOf(EXTERNAL_BASE_SEPARATOR) > -1) {
			return historyToken.substring(
					historyToken.indexOf(EXTERNAL_BASE_SEPARATOR)+3);
		}
		return historyToken;		
	}
	
	public static String getParameterString(final String key, final String value) {
		return key + VALUE_SEPARATOR + value;
	} 

	
	public static String buildToken(final String baseToken, final String[] paramList) {
		StringBuffer token = new StringBuffer();
		token.append(baseToken);
		token.append(BASE_SEPARATOR);
		for (int i = 0; i < paramList.length; i++) {
			if ( i%2!= 0 ) {
				//append a parameter separator before the odd ones
				token.append(PARAM_SEPARATOR);				
			}
			token.append(paramList[i]);
		}
		return token.toString();
	}
	
	public static String buildToken(final String baseToken, final String key, final String value) {
		StringBuffer token = new StringBuffer();
		token.append(baseToken);
		token.append(BASE_SEPARATOR);
		token.append(key);
		token.append(VALUE_SEPARATOR);
		token.append(value);

		return token.toString();
	}
	
	/**
	 * This method is useful for building a new token based on a previous token.
	 * For example if a history item has a sub command that must handle parameters.
	 * @param baseToken the History base token value
	 * @param kvMap a map of parameter strings key/value pairs
	 * which have already been formatted
	 * @return the complete url history command as (i.e. everything following the #)
	 */
	public static String buildToken(final String baseToken, final Map kvMap) {
		if (DEBUG_BUILD_TOKENS) {DebugPanel.setText("HistoryCommander", "building history token from kvMap");}
		StringBuffer token = new StringBuffer();
		token.append(baseToken);
		Iterator it = kvMap.keySet().iterator();
		token.append(BASE_SEPARATOR);
		boolean clean = false;
		while (it.hasNext()) {
			String nextKey = (String)it.next();
			token.append(nextKey + VALUE_SEPARATOR + kvMap.get(nextKey));
			token.append(PARAM_SEPARATOR);	
			clean = true;
		}
		//remove the final PARAM_SEPARATOR
		if (clean) {
			token.deleteCharAt(token.lastIndexOf(PARAM_SEPARATOR));
		}
		if (DEBUG_BUILD_TOKENS) {DebugPanel.setText("HistoryCommander", "finished building history token from kvMap");}
		return token.toString();
	}
	
	/**
	 * This method is useful for building a new token based on a previous token.
	 * For example if a history item has a sub command that must handle parameters.
	 * @param baseToken the History base token value
	 * @param kvMap a map of parameter strings key/value pairs
	 * which have already been formatted
	 * @return the complete url history command as (i.e. everything following the #)
	 */
	public static String buildExternalToken(final String baseToken, final Map kvMap) {
		if (DEBUG_BUILD_TOKENS) {DebugPanel.setText("HistoryCommander.buildExternalToken()", "building history token from kvMap");}
		StringBuffer token = new StringBuffer();
		token.append(baseToken);
		Iterator it = kvMap.keySet().iterator();
		token.append(EXTERNAL_BASE_SEPARATOR);
		boolean clean = false;
		while (it.hasNext()) {
			String nextKey = (String)it.next();
			token.append(nextKey + EXTERNAL_VALUE_SEPARATOR + kvMap.get(nextKey));
			token.append(EXTERNAL_PARAM_SEPARATOR);	
			clean = true;
		}
		//remove the final PARAM_SEPARATOR
		if (clean) {
			token.deleteCharAt(token.lastIndexOf(EXTERNAL_PARAM_SEPARATOR));
		}
		if (DEBUG_BUILD_TOKENS) {DebugPanel.setText("HistoryCommander.buildExternalToken():", "finished building history token from kvMap");}
		return token.toString();
	}
	
	/**
	 * Utility method for clients to get their parameter
	 * list as a map of key/value pairs. key/value pairs
	 * are separated by the PARAM_SEPARATOR.
	 * Keys are separated from values by the
	 * VALUE_SEPARATOR.
	 * 
	 * @return a Map
	 */
	private static Map getParameterMap(final String paramList) {
		if (DEBUG) { DebugPanel.setText("HistoryCommander", "getting parameter map from " + paramList); }
		
		Map kv = new HashMap();
		String[] params = paramList.split(PARAM_SEPARATOR);
		for (int i = 0; i < params.length; i++) {
			String[] kvPair = params[i].split(VALUE_SEPARATOR);
			if (kvPair.length == 2) {
				kv.put(kvPair[0], kvPair[1]);
			}
		}
		return kv;
	}
	
	/**
	 * Utility method for clients to get their parameter
	 * list as a map of key/value pairs. key/value pairs
	 * are separated by the PARAM_SEPARATOR.
	 * Keys are separated from values by the
	 * VALUE_SEPARATOR.
	 * 
	 * @return a Map
	 */
	private static Map getSafariParameterMap(final String paramList) {
		if (DEBUG) { DebugPanel.setText("HistoryCommander", "getting safari parameter map from " + paramList); }

		Map kv = new HashMap();
		String[] params = paramList.split(EXTERNAL_PARAM_SEPARATOR);
		for (int i = 0; i < params.length; i++) {
			String[] kvPair = params[i].split(EXTERNAL_VALUE_SEPARATOR);
			if (kvPair.length == 2) {
				kv.put(kvPair[0], kvPair[1]);
			}
		}
		return kv;
	}
	
	public static String getHistoryCookie() {
		return Cookies.getCookie(HISTORY_COOKIE);
	}
	
	
	/**
	 * Wrapper method to call both History methods
	 * History.newItem(String)
	 * and
	 * History.onHistoryChanged(String)
	 * in that order.
	 * @param historyToken String
	 * FIXME: should this whole method be removed in favor of History.newItem now that 
	 * gwt has fixed that method?
	 */
	public static void newItem(final String historyToken) {
		History.newItem(historyToken);
		History.onHistoryChanged(historyToken);
	}
	
	/**
	 * Call this method only once, when the intialization of the 
	 * app executes.  Use the history token for the page desired.
	 * @param initToken
	 * FIXME: should this whole method be removed in favor of History.newItem now that 
	 * gwt has fixed that method?
	 */
	public static void init(final String initToken) {
		History.newItem(initToken);
		History.onHistoryChanged(initToken);
	}
	
	
	private void debug(final String historyToken) {
		if (commandTokenMap.containsKey(historyToken)) {
			DebugPanel.setText("HistoryCommander", "historyToken " + historyToken + " is mapped.");
			CommandToken command = (CommandToken)commandTokenMap.get(historyToken);
			DebugPanel.setText("HistoryCommander", "command's token = " + command.getBaseToken());	
		} else {
			DebugPanel.setText("HistoryCommander", "historyToken " + historyToken + " is unmapped.");
			DebugPanel.setText("HistoryCommander", "checking " + historyToken + " for mapped base token");
			String baseToken;
			if (!Jaws.isSafari()) {
				baseToken = getBaseToken(historyToken);
			} else {
				baseToken = getSafariBaseToken(historyToken);
			}
			if (commandTokenMap.containsKey(baseToken)) {
				DebugPanel.setText("HistoryCommander", "baseToken " + baseToken + " is mapped.");
				CommandToken command = (CommandToken)commandTokenMap.get(baseToken);
				DebugPanel.setText("HistoryCommander", "command's token = " + command.getBaseToken());	
			}
		}
		DebugPanel.setText("HistoryCommander", "current token: " + historyToken + ", previous token: " + previousToken);		
		//What's in the commandTokenMap on a refresh?
		DebugPanel.setText("HistoryCommander", "history changed -----------------");
		DebugPanel.setText("HistoryCommander", "Tokens registered:");
		Iterator it = commandTokenMap.keySet().iterator();
		while(it.hasNext()) {
			DebugPanel.setText("HistoryCommander", "'" + it.next() + "'");
		}
		DebugPanel.setText("HistoryCommander", "------------------- end token registry");
	}
	
	/**
	 * Return true if the HistoryCommander has a command to execute for this token.
	 * 
	 * @param token
	 * @return
	 */
	public boolean canHandle(final String token) {
		if ( !Jaws.isSafari() && commandTokenMap.containsKey(getBaseToken(token))) {
			return true;
		} else if (commandTokenMap.containsKey(getSafariBaseToken(token))) {
			return true;
		} 
		return false;
	}
	

	public void onWindowClosed() {
		//NOOP
	}

	public String onWindowClosing() {
		Cookies.setCookie(HISTORY_COOKIE, History.getToken());
		return null;
	}
}
