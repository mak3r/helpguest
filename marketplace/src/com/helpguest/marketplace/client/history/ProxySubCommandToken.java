package com.helpguest.marketplace.client.history;

import java.util.Map;

import com.google.gwt.user.client.ui.SimplePanel;

public class ProxySubCommandToken extends SimplePanel implements SubCommandToken {

	public void setParent(CommandToken parentCommand) {
		if (this.getWidget() != null) {
			if (getWidget() instanceof SubCommandToken) {
				((SubCommandToken)getWidget()).setParent(parentCommand);
			}
		}
	}

	public String getBaseToken() {
		if (this.getWidget() != null) {
			if (getWidget() instanceof SubCommandToken) {
				return ((SubCommandToken)getWidget()).getBaseToken();
			}
		}
		return null;
	}

	public void onEntry(Map<String, String> kvMap) {
		if (this.getWidget() != null) {
			if (getWidget() instanceof SubCommandToken) {
				((SubCommandToken)getWidget()).onEntry(kvMap);
			}
		}
	}

	public void onExit() {
		if (this.getWidget() != null) {
			if (getWidget() instanceof SubCommandToken) {
				((SubCommandToken)getWidget()).onExit();
			}
		}
	}


}
