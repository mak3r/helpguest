/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 30, 2007
 */
package com.helpguest.marketplace.client.history;



/**
 * @author mabrams
 *
 */
public interface CommandToken extends EntryCommand, ExitCommand {

	public String getBaseToken();
	
}
