/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 21, 2007
 */
package com.helpguest.marketplace.client.history;



/**
 * Suggested use is to clean up polling rpc calls
 * when leaving a page.
 * 
 * @see HistoryCommander
 * @see CommandToken
 * @see EntryCommand
 * 
 * @author mabrams
 *
 */
public interface ExitCommand {

	/**
	 * {@inheritDoc}
	 */
	public void onExit();

}
