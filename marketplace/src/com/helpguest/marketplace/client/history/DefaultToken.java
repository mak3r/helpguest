package com.helpguest.marketplace.client.history;

public interface DefaultToken extends CommandToken {

	public String getDefaultToken();
}
