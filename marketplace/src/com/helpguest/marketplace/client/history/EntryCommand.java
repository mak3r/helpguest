/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 21, 2007
 */
package com.helpguest.marketplace.client.history;

import java.util.Map;



/**
 * Wrapper interface for Command.  Suggested use is to
 * execute some action when a history token is found.
 * For example on a browser refresh.
 *
 * @see CommandToken
 * @see HistoryCommander
 * @see ExitCommand
 * 
 * @author mabrams
 *
 */
public interface EntryCommand {

	/**
	 * {@inheritDoc}
	 */
	public void onEntry(final Map<String, String> kvMap);

}
