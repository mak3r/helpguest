/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.helpguest.gwt.protocol.SignUpOutcome;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.OfferingsConstraints;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public interface ExpertiseServiceAsync {

	/**
	 * @param limit is the maximum number to return.
	 * @return a list of the most requested search terms.
	 */
	public void getMostRequested(final int limit, final AsyncCallback callback);
	
	/**
	 * @param limit is the maximum number to return.
	 * @return a list of the most common expertise registered.
	 */
	public void getMostCommon(final int limit, final AsyncCallback callback);
	
	/**
	 * @param dto the Expert whose expertise we are retrieving
	 * @return a list of this experts expertise
	 */
	public void getExpertise(final ExpertDTO dto, final AsyncCallback callback);

	/**
	 * @param dto the Expert whose expertise we are retrieving
	 * @return a list of the most offered expertise
	 */
	public void getMostOffered(final int limit, final AsyncCallback callback);
	
	//prahalad 07/09/08 adding method to get a list of categories from DB
	//ultimately this will enable expert to add tags under appropriate catgs. and offgs. 
	public void getCategories(final AsyncCallback callback);
	
	/**
	 * @author ying
	 * */
	public void getExpertCategories(final String expert_id, final AsyncCallback callback);
	
	//prahalad 07/22/08 adding method to get a list of categories and offerings from DB
	//ultimately this will enable expert to add tags under appropriate catgs. and offgs. 
	public void getCatOffgs(final AsyncCallback callback);
	
	//prahalad 10/31/08 adding method to get a list of categories, offerings and tags from DB
	//ultimately this will enable expert to manage tags under appropriate catgs. and offgs. 
	public void getCatOffgTags(String expert_id, final AsyncCallback callback);
	
	//prahalad 07/09/08 adding method to get a list of offerings from DB for a category
	//ultimately this will enable expert to add tags under appropriate catgs. and offgs.
	public void getOfferings(final CategoryConstraints cc, final AsyncCallback callback);
	
	//prahalad 07/14/08 adding method to get tags from DB for a category, offering and expertid
	public void getTags(final CategoryConstraints cc, final OfferingsConstraints oc, final String expert_id, final AsyncCallback callback);
	
	public void updateTags(final CategoryConstraints cc, final OfferingsConstraints oc, final String expert_id, final String tag_content, final AsyncCallback callback);
}
