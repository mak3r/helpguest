/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Oct 8, 2007
 */
package com.helpguest.marketplace.client.util;

import com.helpguest.gwt.protocol.LoginType;
import com.google.gwt.core.client.GWT;


//import com.helpguest.HGServiceProperties;
//import com.helpguest.HGServiceProperties.HGServiceProperty;

/**
 * @author mabrams
 *
 */
public class Jaws {
	
	//prahalad added begin on 6/2/08
	private static MarketPlaceConstants myConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String myFqdn = myConstants.fullyQualifiedDomainName();
	private static String jawsAppDir = myConstants.jawsAppDir();
	private static String myWebUriPrefix = myConstants.webUriPrefix();
	//prahalad added end on 6/2/08
	
	public static native String getUserAgent() /*-{
		return navigator.userAgent.toLowerCase();
	}-*/; 
	
	public static native String getMimeTypes() /*-{
		//navigator.plugins.refresh(true);
		//window.alert("returning mime types");
		var mTypes;
		
		for (var i = 0; i < navigator.mimeTypes.length; i++) {
			pluginType = navigator.mimeTypes[i].type;
			mTypes = mTypes + "###" + pluginType;
		}
		return mTypes;
	}-*/;
	
	public static native boolean isWindowsIE() /*-{
		var detect = navigator.userAgent.toLowerCase();
		var msie = detect.indexOf("msie") + 1;
		var win = detect.indexOf("win") + 1;
		var windowsIE = (msie && win);
		
		return windowsIE;
	}-*/;

	public static native boolean isWindowsNotIE() /*-{
		var detect = navigator.userAgent.toLowerCase();
		var msie = detect.indexOf("msie") + 1;
		var win = detect.indexOf("win") + 1;
		var windowsIE = (!msie && win);
		
		return windowsIE;
	}-*/;
	
	public static native boolean isMacintosh() /*-{
		var detect = navigator.userAgent.toLowerCase();
		var mac = detect.indexOf("macintosh") + 1;
		
		return mac;
	}-*/;
	
	public static native boolean isSafari() /*-{
		var detect = navigator.userAgent.toLowerCase();
		var safari = detect.indexOf("safari") + 1;
		
		return safari;
	}-*/;

	public static native void refreshPlugins() /*-{
		navigator.plugins.refresh(true);
	}-*/;
	
	public static native boolean webstartVersionCheck(final String versionString) /*-{
		// Mozilla may not recognize new plugins without this refresh
		
		//navigator.plugins.refresh(true);
		var outcome = true;
		// First, determine if Web Start is available
		//if (navigator.mimeTypes['application/x-java-jnlp-file']) {
			// Next, check for appropriate version family
			for (var i = 0; i < navigator.mimeTypes.length; i++) {
				pluginType = navigator.mimeTypes[i].type;
				if (pluginType.indexOf("application/x-java-applet;version="+versionString) != -1) {
					
					outcome = false;
					
				}
			}
		//}
		 	return outcome;
		 
		
	}-*/;
		
	public static boolean installOnWinIERequired() {
		return isWindowsIE() && installRequired();
	}
	
	public static boolean installRequired() {
		//return !webstartVersionCheck("1.5");
		return webstartVersionCheck("1.5");
	}
	
	public static native void sendToSunJavaSite() /*-{
		window.open("http://jdl.sun.com/webapps/getjava/BrowserRedirect?locale=en&host=java.com", "HelpGuest Requires Java 1.5")
	}-*/;
	
	public static String generateJnlpUrl(final String jnlpFileId, final LoginType type) {
		//prahalad 05/22/2008 adding fqdn and change return
         //String myfqdn = 
	     //	HGServiceProperties.getInstance().getProperty(HGServiceProperty.FQDN.getKey());
		
		//return "https://secure.helpguest.com/jaws/apps/" + type + "/" + jnlpFileId + ".jnlp";
		return myWebUriPrefix + "://" + myFqdn + jawsAppDir + "/" + type + "/" + jnlpFileId + ".jnlp";
	}
	
	public static String generateHtmlUrl(final String jnlpFileId, final LoginType type) {
		
		//return "https://secure.helpguest.com/jaws/apps/" + type + "/" + jnlpFileId + ".html";
		//prahalad added begin on 6/2/08
		return myWebUriPrefix + "://" + myFqdn + jawsAppDir + "/" + type + "/" + jnlpFileId + ".html";
		//prahalad added end on 6/2/08
	}
	
	public static String getLaunchContent(final String jnlpFileId, final String content, final LoginType type) {
		StringBuffer jnlpBuf = new StringBuffer();
		String jnlpUrl = generateJnlpUrl(jnlpFileId, type);

		jnlpBuf.append("<iframe ");
		jnlpBuf.append("id=\"__launch_jaws\" ");
		jnlpBuf.append("name=\"__launch_jaws\" ");
		jnlpBuf.append("src=\"\" ");
		jnlpBuf.append("style=\"width:0px; height:0px; border: 0px\">");
		jnlpBuf.append("</iframe>");
		
		jnlpBuf.append("<a href=\"JawsCheck.html?jnlp_url=" + jnlpUrl + "\" target=\"__launch_jaws\">");
		jnlpBuf.append(content);
		jnlpBuf.append("</a>");
		
		return jnlpBuf.toString();
	}
	
	public static String getSimpleLaunchContent(final String jnlpFileId, final String content, final LoginType type) {
		String jnlpUrl = generateJnlpUrl(jnlpFileId, type);
		StringBuffer jnlpBuf = new StringBuffer();
		jnlpBuf.append("<a href=\"" + jnlpUrl + "\">");
		jnlpBuf.append(content);
		jnlpBuf.append("</a>");			
		
		return jnlpBuf.toString();		
	}
	
	public static String getUpgradeLaunchContent(final String jnlpFileId, final String content, final LoginType type) {
		String upgradeUrl = "http://jdl.sun.com/webapps/getjava/BrowserRedirect?locale=en&host=java.com";
		StringBuffer jnlpBuf = new StringBuffer();
		jnlpBuf.append("<a href=\"" + upgradeUrl + "\" target=\"_blank\">");
		jnlpBuf.append(content);
		jnlpBuf.append("</a>");			
		
		return jnlpBuf.toString();		
	}
	
	public static String getWinIELaunchContent(final String jnlpFileId, final String content, final LoginType type) {
		String htmlUrl = generateHtmlUrl(jnlpFileId, type);
		StringBuffer jnlpBuf = new StringBuffer();

		jnlpBuf.append("<a href=\"" + htmlUrl + "\" target=\"_self\">");
		jnlpBuf.append(content);
		jnlpBuf.append("</a>");

		return jnlpBuf.toString();
	}
	

}
