package com.helpguest.marketplace.client.util;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

public class DebugPanel extends SimplePanel {

	private static final TextArea textArea = new TextArea();
	private static final DisclosurePanel dp = new DisclosurePanel("HelpGuest Debug Panel");
	private static final StringBuffer buf = new StringBuffer();
	private static final Button clearButton = new Button("Clear");
	
	public DebugPanel(final boolean setOpen) {
		clearButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				buf.delete(0, buf.length());
				buf.append("");
				textArea.setText(buf.toString());
			}
			
		});
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(textArea);
		vp.add(clearButton);
		dp.add(vp);
		dp.setOpen(setOpen);
		setWidget(dp);
		setStyleName("debug_panel");
		
		History.addValueChangeHandler(new ValueChangeHandler<String>() {

			public void onValueChange(ValueChangeEvent<String> arg0) {
				setText(DebugPanel.class, "GWT History change: " + arg0);
			}
			
		});

	}

	/**
	 * Set the debug text content assume that we will not clear the previous content
	 * @param content
	 * @deprecated use setText(Class, String) instead
	 */
	public static void setText(final String reportingClass, final String content) {
		if (dp.isVisible()) {
			buf.append("[" + reportingClass + "] " + content + "\n");			
			textArea.setText(buf.toString());
		}
	}
	
	/**
	 * Set the debug text content assume that we will not clear the previous content
	 * @param content
	 */
	public static void setText(final Class clazz, final String content) {
		if (dp.isVisible()) {
			buf.append("[" + clazz.getName() + "] " + content + "\n");			
			textArea.setText(buf.toString());
		}
	}
	
	/**
	 * Set the debug text content assume that we will not clear the previous content
	 * @param content
	 * @deprecated use setText(Class, Throwable) instead
	 */
	public static void setText(final String reportingClass, final Throwable throwable) {
		if (dp.isVisible()) {
			StackTraceElement[] ste = throwable.getStackTrace();
			for (int i=0; i<ste.length; i++) {
				buf.append("[" + reportingClass + "] " + ste[i] + "\n");			
			}
			textArea.setText(buf.toString());
		}
	}
	
	public static void setText(final Class clazz, final Throwable throwable) {
		if (dp.isVisible()) {
			StackTraceElement[] ste = throwable.getStackTrace();
			for (int i=0; i<ste.length; i++) {
				buf.append("[" + clazz.getName() + "] " + ste[i] + "\n");			
			}
			textArea.setText(buf.toString());
		}		
	}
	
}
