/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 *
 * Created on May 23, 2008
 * prahalad
 */
package com.helpguest.marketplace.client.util;
import com.google.gwt.i18n.client.Constants;

public interface MarketPlaceConstants extends Constants {
  String fullyQualifiedDomainName();
  String webRoot();
  String webUriPrefix();
  String jawsAppDir();
  String recentExperts();
  String payPalAction();
  String payPalBusiness();
  String gwtModule();
  boolean hideDebug();
}