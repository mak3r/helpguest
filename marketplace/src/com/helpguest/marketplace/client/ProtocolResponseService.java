/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 15, 2007
 */
package com.helpguest.marketplace.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.helpguest.marketplace.client.dto.ProtocolResponseDTO;

/**
 * @author mabrams
 *
 */
public interface ProtocolResponseService extends RemoteService {

	public ProtocolResponseDTO getProtocolResponse(final String guestUid, final String type);

}
