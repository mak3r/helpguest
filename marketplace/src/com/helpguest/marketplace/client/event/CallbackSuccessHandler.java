/**
 * 
 */
package com.helpguest.marketplace.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface CallbackSuccessHandler<I> extends EventHandler {
	void onCallbackSuccess(CallbackSuccessEvent<I> event);
}