/**
 * 
 */
package com.helpguest.marketplace.client.event;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

public interface HasCallbackSuccessHandlers<I> extends HasHandlers{
	HandlerRegistration addCallbackSuccessHandler(CallbackSuccessHandler<I> handler);
}