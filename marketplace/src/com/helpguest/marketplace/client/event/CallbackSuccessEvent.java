package com.helpguest.marketplace.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class CallbackSuccessEvent<I> extends GwtEvent<CallbackSuccessHandler<I>> {

	private static GwtEvent.Type<CallbackSuccessHandler<?>> TYPE;

	private final I callbackResult;

	/**
	 * Fires a callback success event on all registered handlers in the handler
	 * manager.If no such handlers exist, this method will do nothing.
	 * 
	 * @param <I>
	 *            the old value type
	 * @param source
	 *            the source of the handlers
	 * @param value
	 *            the value
	 */
	public static <I> void fire(HasCallbackSuccessHandlers<I> source, I callbackResult) {
		if (TYPE != null) {
			CallbackSuccessEvent<I> event = new CallbackSuccessEvent<I>(callbackResult);
			source.fireEvent(event);
		}
	}

	/**
	 * Gets the type associated with this event.
	 * 
	 * @return returns the handler type
	 */
	public static Type<CallbackSuccessHandler<?>> getType() {
		if (TYPE == null) {
			TYPE = new Type<CallbackSuccessHandler<?>>();
		}
		return TYPE;
	}

	/**
	 * Creates a callback success event.
	 * 
	 * @param callbackResult
	 *            the callbackResult
	 */
	protected CallbackSuccessEvent(I callbackResult) {
		this.callbackResult = callbackResult;
	}

	// The instance knows its CallbackReturnHandler is of type I, but the TYPE
	// field itself does not, so we have to do an unsafe cast here.
	@SuppressWarnings("unchecked")
	@Override
	public final Type<CallbackSuccessHandler<I>> getAssociatedType() {
		return (Type) TYPE;
	}

	@Override
	protected void dispatch(CallbackSuccessHandler<I> handler) {
		handler.onCallbackSuccess(this);
	}

	public I getResult() {
		return callbackResult;
	}

}
