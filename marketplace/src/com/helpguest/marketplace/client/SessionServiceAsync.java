/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.helpguest.marketplace.client.dto.ExpertAccountDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public interface SessionServiceAsync {

	public void createSession(final String expertEmail, final AsyncCallback callback);
	
	public void createDemoSession(final String expertEmail, final AsyncCallback callback);
	
	public void getDemoSession(final ExpertDTO expert, final AsyncCallback callback);
	
	public void getSession(final String sessionId, final AsyncCallback callback);
	
	public void getAllSessions(final ExpertAccountDTO eaDTO, final AsyncCallback callback);
	
	public void getUnpaidSessions(final ExpertAccountDTO eaDTO, final AsyncCallback callback);

}
