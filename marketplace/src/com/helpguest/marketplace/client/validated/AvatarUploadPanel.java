/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Sep 27, 2007
 */
package com.helpguest.marketplace.client.validated;


import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormHandler;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormSubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormSubmitEvent;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.ui.TwoTonePanel;
import com.helpguest.marketplace.client.ui.VerticalSpacer;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;


/**
 * @author mabrams
 *
 */
public class AvatarUploadPanel extends FormPanel {

	
	public static final String USER_KEY = "user";
    public static final String FILE_UPLOAD_SUCCESS = "SUCCESS";
    private String secret;
    private String user;
    private Hidden password = new Hidden();
	private PopupPanel popUp = new PopupPanel();
	
	final FileUpload fileUpload = new FileUpload();
	private String avatarFileName = null;
	private String avatarFileNameChar = null;
	
	//prahalad added begin 6/3/08
	private static MarketPlaceConstants aupConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String aupFqdn = aupConstants.fullyQualifiedDomainName();
    private static String aupWebUriPrefix = aupConstants.webUriPrefix();
	

	public AvatarUploadPanel(final ExpertDTO expert) {
		this.user = expert.getExpertAccount().getEmail();
		
		setMethod(FormPanel.METHOD_POST);
		setEncoding(FormPanel.ENCODING_MULTIPART);
		//FIXME gwt version 1.4.6 has a bug with content-type=utf8
		//The fix is to use the AvatarReceiver servlet when
		// the gwt bug is fixed.
		//setAction("/marketplace/AvatarReceiver");
		//setAction("https://secure.helpguest.com/hgservlets/VersionTwoDetails");
		//prahalad changing static url 6/3/08
		setAction(aupWebUriPrefix + "://" + aupFqdn + "/hgservlets/VersionTwoDetails");
		//setAction(aupWebUriPrefix + "://" + aupFqdn + "/marketplace/AvatarReceiver");
		
		
		
		
		fileUpload.setName("avatar_file");
		
		addFormHandler(new FormHandler() {

			public void onSubmit(FormSubmitEvent event) {
				DebugPanel.setText("AvatarUploadPanel", "inside onSubmit()");
				// TODO Auto-generated method stub
				// This event is fired just before the form is submitted. We can take
		        // this opportunity to perform validation.
		       /* avatarFileName = fileUpload.getFilename().trim();
		        
		        if (avatarFileName.indexOf('/')!=-1)
		        {avatarFileNameChar="/";
		        }
		        else if(avatarFileName.indexOf('\\')!=-1){
		        	avatarFileNameChar="\\";
		        }
		        String ext = avatarFileName.substring(avatarFileName.indexOf('.'));
		        Window.alert("Ext is: " + ext);
		        if (avatarFileName.length() == 0) {
		          Window.alert("The text box must not be empty");
		          event.setCancelled(true);}
		        
		        //if ((!avatarFileName.endsWith(".png")) || (!avatarFileName.endsWith(".jpeg")) || (!avatarFileName.endsWith(".jpg")))
		        if ((ext.equals(".png")) || (ext.equals(".jpeg")) || (ext.equals(".jpg")))
				{
		        	
		        	
		        	
		        	}else{
		        		Window.alert("File is: " + avatarFileName +
						"\nPlease specify an image file ending with '.png', '.jpeg', or '.jpg'.");
				event.setCancelled(true);
				}*/
				
			}

			public void onSubmitComplete(FormSubmitCompleteEvent event) {
				if (!FILE_UPLOAD_SUCCESS.equals(event.getResults())) {
					//FIXME gwt version 1.4.6 has a bug with content-type=utf8
					//The results should be a preformatted string
					// from the AvatarReceiver servlet
					Window.alert(event.getResults());
					
				}
			}
			
		});
		
		
		
		Hidden userName = new Hidden();
		userName.setName(USER_KEY);
		//FIXME gwt version 1.4.6 has a bug with content-type=utf8
		//The fix here is to not require password entry
		// and just use the expertUid to access the AvatarReceiver servlet
		//userName.setValue(expert.getExpertUid());
		userName.setValue(expert.getExpertAccount().getEmail());
		password.setName("password");
		password.setValue(secret);
		
		HorizontalPanel vp = new HorizontalPanel();
		vp.add(fileUpload);
		vp.add(userName);
		vp.add(password);
		
		setWidget(vp);
	}
	
	public void getCredentials() {
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		Label info = new Label("Please enter the password for " + 
				user + " to complete this operation");
		
		Button okButton = new Button("OK");
		Button cancelButton = new Button("Cancel");
		
		okButton.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				popUp.hide();
				secret = passwordTextBox.getText();
				password.setValue(secret);
				submit();
				//Now that the form has been submitted, 
				// clear the password
				passwordTextBox.setText(null);
			}
			
		});

		cancelButton.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				popUp.hide();
			}
			
		});
		
		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.add(okButton);
		buttonPanel.add(new VerticalSpacer(10));
		buttonPanel.add(cancelButton);
		
		HorizontalPanel passwordPanel = new HorizontalPanel();
		passwordPanel.add(new Label("password: "));
		passwordPanel.add(new VerticalSpacer(10));
		passwordPanel.add(passwordTextBox);
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(info);
		vp.add(passwordPanel);
		vp.add(buttonPanel);
		vp.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		vp.setSpacing(20);
		vp.setWidth("300px");
		
		TwoTonePanel ttp = new TwoTonePanel("300px");
		ttp.setHeading(new Label("Authentication Required"));
		ttp.setBody(vp);

		Grid grid = new Grid(1,1);
		grid.setWidget(0, 0, ttp);
		grid.setBorderWidth(1);
		grid.setStyleName("hg-PopUp-inner-border");
		
		RoundedPanel rp = new RoundedPanel(grid, RoundedPanel.ALL, 2);
		rp.setStyleName("hg-PopUp-inner");
		rp.setCornerStyleName("hg-PopUp-inner-corner");
		
		Grid outerGrid = new Grid(1,1);
		outerGrid.setWidget(0, 0, rp);
		outerGrid.setBorderWidth(1);
		outerGrid.setStyleName("hg-PopUp-outer-border");

		
		RoundedPanel outerRP = new RoundedPanel(outerGrid, RoundedPanel.ALL, 5);
		outerRP.setStyleName("hg-PopUp-outer");
		outerRP.setCornerStyleName("hg-PopUp-outer-corner");
		
		
		
		popUp.setWidget(outerGrid);
		popUp.show();
		popUp.center();
		
	}
	
	
}
