/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Sep 13, 2007
 */
package com.helpguest.marketplace.client.validated;

import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.SessionService;
import com.helpguest.marketplace.client.SessionServiceAsync;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.client.dto.SessionDTOAssistant;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.ui.HeaderMenu;
import com.helpguest.marketplace.client.ui.HorizontalSpacer;
import com.helpguest.marketplace.client.ui.TwoTonePanel;
import com.helpguest.marketplace.client.ui.VerticalSpacer;
import com.helpguest.marketplace.client.widgets.PaddedPanel;

/**
 * @author mabrams
 *
 */
public class CompletedSessionsPage extends SimplePanel implements CommandToken,
		AuthenticationListener {

	private ExpertDTO expertDTO;
	private List sessionList;
	private Grid allSessionsGrid;
	private Label[] allSessionsColumns = {
			new Label("Ticket #"),
			/** padded to enable a 10 char column width esp. on safari **/
			new Label("   Date   "),
			new Label("Duration"),
			new Label("Guest fee"),
			new Label("Expert amount"),
			new Label("Payment requested"),
			new Label("Payment sent")
	};
	
	//Create the sessions service proxy
	final SessionServiceAsync sessionService =
		(SessionServiceAsync) GWT.create(SessionService.class);
	
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	public CompletedSessionsPage(final ExpertDTO expertDTO) {
		super();
		this.expertDTO = expertDTO;

		//Specify the url where the session service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.sessionService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "SessionService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		//Specify the url where the expert service is running
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);
	}
	
	private void init() {
		Label head = new Label("Completed Sessions");
		
		final TextBox emailTB = new TextBox();
		emailTB.setText(expertDTO.getExpertAccount().getPaypalEmail());
		emailTB.setWidth("300px");
		final Button collectButton = new Button("Collect my payment");

		emailTB.addKeyboardListener(new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {}
			public void onKeyUp(Widget sender, char keyCode, int modifiers) {}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (isValidEmailFormat(emailTB.getText())) {
					collectButton.setEnabled(true);
				} else {
					collectButton.setEnabled(false);
				}
			}
			
		});
		
		collectButton.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				expertService.makePayments(
						expertDTO.getExpertAccount(), makePaymentsCallback);
			}
			
		});
		if (!isValidEmailFormat(emailTB.getText())) {
			collectButton.setEnabled(false);
		} else {
			collectButton.setEnabled(true);
		}

		HorizontalPanel inputPanel = new HorizontalPanel();
		inputPanel.add(new VerticalSpacer(20));
		inputPanel.add(emailTB);
		inputPanel.add(new VerticalSpacer(10));
		inputPanel.add(collectButton);

		VerticalPanel vp = new VerticalPanel();
		vp.add(allSessionsGrid);
		vp.setCellHorizontalAlignment(allSessionsGrid, VerticalPanel.ALIGN_CENTER);
		vp.add(new HorizontalSpacer(20));
		vp.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		vp.add(new PaddedPanel(new Label("Make payments to paypal account:"),1,20));
		vp.add(inputPanel);
		vp.add(new HorizontalSpacer(20));
	
		TwoTonePanel ttp = new TwoTonePanel(head, vp, "710px", "hgx-TwoToneTop");
		
		PaddedPanel paddedPanel = new PaddedPanel(10,20);
		paddedPanel.add(ttp);
		
		setWidget(paddedPanel);
	}
	
	/**
	 * TODO this check can be improved.  See the standard email specifications
	 * @param email String
	 * @return true if the email has an '@' and a '.'
	 */
	private boolean isValidEmailFormat(final String email) {
		int atIndex = email.indexOf('@');
		int dotIndex = email.lastIndexOf('.');
		int length = email.length();
		if (atIndex > -1 && dotIndex > -1 && atIndex < dotIndex && dotIndex+1 < length) {
			return true;
		}
		return false;
	}
	
	AsyncCallback allSessionsCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("CompletedSessionsPage allSessionsCallback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("CompletedSessionsPage allSessionsCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			sessionList = (List) result;
			int gridSize = sessionList.size() + 1;
			allSessionsGrid = new Grid(gridSize, allSessionsColumns.length);
			allSessionsGrid.setStyleName("hg-CompletedSessions-grid");
			allSessionsGrid.setCellSpacing(10);
			//set up the header row
			for (int i=0; i<allSessionsColumns.length; i++) {
				Label columnLabel = allSessionsColumns[i];
				columnLabel.setStyleName("hg-table-column-headers");
				allSessionsGrid.setWidget(0, i, columnLabel);
				allSessionsGrid.getCellFormatter().setHorizontalAlignment(
						0, i, HorizontalPanel.ALIGN_CENTER);
			}
			//Set up the grid row by row
			for (int i = 1; i<gridSize; i++) {
				SessionDTO dto = (SessionDTO) sessionList.get(i-1);
				SessionDTOAssistant sa = new SessionDTOAssistant(dto);
				allSessionsGrid.setText(i, 0, dto.getSessionId());
				allSessionsGrid.setText(i, 1, dto.getInsertDate());
				allSessionsGrid.setText(i, 2, String.valueOf(sa.getDurationInMinutes()));				
				allSessionsGrid.setText(i, 3, sa.calculateFormattedTotalValue());
				allSessionsGrid.setText(i, 4, sa.getFormattedExpertAmount());
				allSessionsGrid.setText(i, 5, 
						(dto.getPaymentRequested()==null?"--":dto.getPaymentRequested()));
				allSessionsGrid.setText(i, 6, 
						(dto.getPaymentSent()==null?"--":dto.getPaymentSent()));
				
				//center align the duration
				allSessionsGrid.getCellFormatter().setHorizontalAlignment(
						i, 2, HorizontalPanel.ALIGN_CENTER);
				//Center align the dates
				allSessionsGrid.getCellFormatter().setHorizontalAlignment(
						i, 1, HorizontalPanel.ALIGN_CENTER);
				allSessionsGrid.getCellFormatter().setHorizontalAlignment(
						i, 5, HorizontalPanel.ALIGN_CENTER);
				allSessionsGrid.getCellFormatter().setHorizontalAlignment(
						i, 6, HorizontalPanel.ALIGN_CENTER);
				
				//Set the style for all the cells
				for (int j=0; j<allSessionsColumns.length; j++) {
					allSessionsGrid.getCellFormatter().setStyleName(
							i, j, "hg-CompletedSessions-grid");
				}
				
			}
			init();
		}
		
	};

	AsyncCallback unpaidSessionsCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("CompletedSessionsPage unpaidSessionsCallback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("CompletedSessionsPage unpaidSessionsCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {

		}
		
	};

	AsyncCallback makePaymentsCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			Window.alert("There was an error processing your request for payment.  " +
					"Please contact support for assistance.");			
		}

		public void onSuccess(Object result) {
			//Refresh the page by calling onEntry
			onEntry(null);
		}
		
	};

	/**
	 * {@inheritDoc}
	 */
	public String getBaseToken() {
		return HeaderMenu.COMPLETED_SESSIONS;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onEntry(Map<String, String> kvMap) {
		if (expertDTO != null) {
			sessionService.getAllSessions(
					expertDTO.getExpertAccount(), allSessionsCallback);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void onExit() {
		// TODO Auto-generated method stub

	}

	public void onChange(Authenticator source) {
		if (!source.isLoggedIn()) {
			expertDTO = null;
			setWidget(source.getExpiredWidget());
		}
	}

}
