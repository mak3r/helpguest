/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Sep 13, 2007
 */
package com.helpguest.marketplace.client.validated;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.ui.HeaderMenu;

/**
 * @author mabrams
 *
 */
public class UpdateExpertisePage extends SimplePanel implements CommandToken,
		AuthenticationListener {

	private ExpertDTO expertDTO;
	private Label expertise;
	/**
	 * These strings should be objects of some type.
	 * see ExpertServiceImpl for changes
	 */
	public static final String REMOVE = "remove";
    public static final String ADD = "add";
    public static final String PRIMARY = "primary";
	
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);

	public UpdateExpertisePage(final ExpertDTO expertDTO) {
		super();
		this.expertDTO = expertDTO;
		
		//Specify the url where the expert service is running
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);
	}

	
	private void init() {
		if (expertDTO != null) {
			Label instruction = new Label("Update your expertise.");
			expertise = new Label("Expertise is: " + expertDTO.getExpertAccount().getExpertiseList());

			final TextBox inputExpertise = new TextBox();
			inputExpertise.setWidth("350px");
			
			Button addButton = new Button("Add");
			Button removeButton = new Button("Remove");
			Button primaryButton = new Button("Update Primary");

			addButton.addClickListener(new ClickListener() {

				public void onClick(Widget sender) {
					expertService.modifyExpertise(
							expertDTO,
							inputExpertise.getText(),
							ADD,
							modifyExpertiseCallback);
					inputExpertise.setText(null);
				}
				
			});
			
			removeButton.addClickListener(new ClickListener() {

				public void onClick(Widget sender) {
					expertService.modifyExpertise(
							expertDTO,
							inputExpertise.getText(),
							REMOVE,
							modifyExpertiseCallback);
					inputExpertise.setText(null);
				}
				
			});
			
			primaryButton.addClickListener(new ClickListener() {

				public void onClick(Widget sender) {
					expertService.modifyExpertise(
							expertDTO,
							inputExpertise.getText(),
							PRIMARY,
							modifyExpertiseCallback);
					inputExpertise.setText(null);
				}
				
			});
			
			HorizontalPanel buttonPanel = new HorizontalPanel();
			buttonPanel.add(addButton);
			buttonPanel.add(removeButton);
			buttonPanel.add(primaryButton);
			
			
			VerticalPanel vp = new VerticalPanel();
			vp.add(instruction);
			vp.add(expertise);
			vp.add(inputExpertise);
			vp.add(buttonPanel);

			
			setWidget(vp);
		}
	}
	
	AsyncCallback modifyExpertiseCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			Window.alert("There was an error processing your request for payment.  " +
					"Please contact support for assistance.");			
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			expertise.setText("Expertise is: " + expertDTO.getExpertAccount().getExpertiseList());
		}
		
	};


	/**
	 * {@inheritDoc}
	 */
	public String getBaseToken() {
		return HeaderMenu.UPDATE_EXPERTISE;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onEntry(Map<String, String> kvMap) {
		init();
	}

	/**
	 * {@inheritDoc}
	 */
	public void onExit() {
		// TODO Auto-generated method stub

	}

	public void onChange(Authenticator source) {
		if (!source.isLoggedIn()) {
			expertDTO = null;
			setWidget(source.getExpiredWidget());
		}
	}

}
