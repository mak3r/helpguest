package com.helpguest.marketplace.client.validated.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.brick.HorizontalBrick;
import com.helpguest.marketplace.client.brick.InputBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.PaymentPlanDTO;

public class RateCalculatorBlock extends SimplePanel implements DynamicExpertBlock {

	private ExpertDTO expertDTO;
	
	private Label rateTextBoxLabel = new Label("$");
	private InputBrick rateInput = new InputBrick("Rate / minute (min. $.99) :", "6em", InputBrick.TEXT_BOX_TYPE);
	
	private Label sessionLengthTextBoxLabel = new Label("minutes");
	private InputBrick sessionLength = new InputBrick("Preferred session length :", "4em", InputBrick.TEXT_BOX_TYPE);
	
	private Label minDepositLabel = new Label("Guest minimum deposit :");
	private Label minDepositAmount = new Label("$--");
	private HorizontalBrick minDeposit = new HorizontalBrick(minDepositLabel, minDepositAmount);
	
	
	Grid buttonGrid = new Grid(2,1);
	private Button edit = new Button("Edit");
	private Button save = new Button("Save");
	private Button cancel= new Button("Cancel");
	
	private SimplePanel header = new SimplePanel();
	
	
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	public RateCalculatorBlock() {
		//Initialize the ExpertService
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);
		
		addClickListeners();
		
		init();
	}
	
	public RateCalculatorBlock(final ExpertDTO expertDTO) {
		this.expertDTO = expertDTO;
		
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);
		
		addClickListeners();
		
		refresh();
	}
	
	private final void init() {
		//This method should not do anything that depends on
		//dynamic data.  Use refresh for the dynamic content.

		header.setStyleName("rate_calculator_block_header");
		header.setWidget(new Label("Rate Calculator"));
		
		rateInput.setInputAccessory(rateTextBoxLabel, InputBrick.LEFT);
		rateInput.setStyleName("rate_calculator_block_rate");
		
		sessionLength.setInputAccessory(sessionLengthTextBoxLabel, InputBrick.RIGHT);
		sessionLength.addStyleName("rate_calculator_block_session_length");
		
		minDeposit.setStyleName("rate_calculator_block_min_deposit");
		minDeposit.alignRight(minDepositLabel);
		minDeposit.alignLeft(minDepositAmount);
		
		//disable all the editable fields to start
		setEditableEnabled(false);
		
		buttonGrid.setStyleName("rate_calculator_block_button_grid");
		buttonGrid.setWidget(0, 0, edit);
		buttonGrid.setWidget(1, 0, cancel);

		VerticalPanel leftSide = new VerticalPanel();
		leftSide.setStyleName("rate_calculator_block_left_side");				
		leftSide.add(rateInput);		
		leftSide.add(sessionLength);
		leftSide.add(minDeposit);
		
		HorizontalPanel body = new HorizontalPanel();
		body.setStyleName("rate_calculator_block_horiz_panel");
		
		body.add(leftSide);
		body.add(buttonGrid);
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(header);
		vp.add(body);
		
		setStyleName("rate_calculator_block");
		setWidget(vp);
		this.setVisible(true);
	}
	
	private final void refresh() {
		this.setVisible(false);		
		if (expertDTO != null) {
			rateInput.setInputText(expertDTO.getPaymentPlan().getRatePerMinute());
			sessionLength.setInputText(String.valueOf(expertDTO.getPaymentPlan().getPreferredTime()));
			minDepositAmount.setText("$" + expertDTO.getPaymentPlan().getDepositAmount());		
		}	

		init();
	}
	
	private void addClickListeners() {
		edit.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				setEditableEnabled(true);
			}
		});
		
		cancel.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				setEditableEnabled(false);
			}
		});
		
		save.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				setEditableEnabled(false);
				
				//Now save the expertDTO edits
				if (expertDTO != null) {
					PaymentPlanDTO pp = expertDTO.getPaymentPlan();
					pp.setPreferredTime(Integer.parseInt(sessionLength.getInputText()));
					pp.setRatePerMinute(rateInput.getInputText());	
					expertService.updateAccount(expertDTO, updateAccountCallback);
				}
			}
		});
	}
	
	private void setEditableEnabled(final boolean enabled) {
		if (!enabled) {
			buttonGrid.setWidget(0, 0, edit);
		} else {
			buttonGrid.setWidget(0, 0, save);
		}
		edit.setEnabled(!enabled);
		save.setEnabled(enabled);
		cancel.setEnabled(enabled);
		rateInput.setEnabled(enabled);
		sessionLength.setEnabled(enabled);
	}
	
	
	AsyncCallback updateAccountCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("RateCalculatorBlock updateAccountCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("RateCalculatorBlock updateAccountCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			refresh();
		}		
	};
	
	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.widgets.DynamicExpertBlock#setExpertDTO(com.helpguest.marketplace.client.dto.ExpertDTO)
	 */
	public void setExpertDTO(final ExpertDTO expertDTO) {
		this.expertDTO = expertDTO;
		refresh();
	}
	

}
