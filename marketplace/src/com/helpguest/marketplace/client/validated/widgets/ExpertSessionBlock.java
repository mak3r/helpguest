package com.helpguest.marketplace.client.validated.widgets;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.marketplace.client.bluegray.ui.ContentEntry;
import com.helpguest.marketplace.client.bluegray.ui.QuickTicket;
import com.helpguest.marketplace.client.brick.RightLeftBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.client.ui.VisualCuesImageBundle;
import com.helpguest.marketplace.client.util.Jaws;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;


public class ExpertSessionBlock extends SimplePanel implements DynamicProfileBlock {

	private static MarketPlaceConstants marketPlaceConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String FULLY_QUALIFIED_DOMAIN_NAME = marketPlaceConstants.fullyQualifiedDomainName();
    private static String WEB_URI_PREFIX = marketPlaceConstants.webUriPrefix();
    private static String GWT_MODULE = marketPlaceConstants.gwtModule();
    private String baseUrl = WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/" + GWT_MODULE + "/pageBanner.html#";

	
	private ExpertDTO expertDTO;
	private SessionDTO sessionDTO;
	private ContentEntry enabledContent = new ContentEntry("sessionEnabled.div");
	private ContentEntry disabledContent = new ContentEntry("sessionDisabled.div");
	private Label quickTicketLabel = new Label("Quick Ticket Number:");
	private TextBox sessionLinkTextBox = new TextBox();
	private HTML sessionIdHTML = new HTML("NO QUICK TICKET AVAILABLE");
	private RightLeftBrick quickTicketBrick;
	private SimplePanel statefulContent = new SimplePanel();
	private SimplePanel quickTicket = new SimplePanel();
	private SimplePanel enableDesktopApp = new SimplePanel();
	private HTML enableHTML = new HTML();
	//Windows images are a special case.  They don't load properly with gwt
	private final String windowsDisabledImage = 
		"<img border=\"0\" src=\"images/disabledConnection.png\" alt=\"Expertise Disabled\"/>";
	private final String windowsEnabledImage = 
		"<img border=\"0\" src=\"images/enabledConnection.png\" alt=\"Expertise Enabled\"/>";
	private SimplePanel header = new SimplePanel();


	private VisualCuesImageBundle visualCuesImageBundle = 
		(VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);
	private Image connected = visualCuesImageBundle.enabledConnection().createImage();
	private Image disconnected = visualCuesImageBundle.disabledConnection().createImage();
	
	private static final LoginType EXPERT = new LoginType.Expert();
	
	
	public ExpertSessionBlock() {
		init();
	}
	
	public ExpertSessionBlock(final ExpertDTO expertDTO, final SessionDTO sessionDTO) {
		this.expertDTO = expertDTO;
		this.sessionDTO = sessionDTO;
		refresh();
	}
	
	private final void init() {
		//This method should not do anything that depends on
		//dynamic data.  Use refresh for the dynamic content.
		
		header.setStyleName("expert_session_block_header");
		header.setWidget(new Label("Connectivity Detail"));
		
		quickTicket.setWidget(sessionIdHTML);
		quickTicketBrick = new RightLeftBrick(quickTicketLabel, quickTicket);
		quickTicketBrick.setStyleName("expert_session_block_quick_ticket_brick");

		VerticalPanel leftSide = new VerticalPanel();
		leftSide.setStyleName("expert_session_block_left_side");

		sessionLinkTextBox.addStyleName("expert_session_block_session_link");
		sessionLinkTextBox.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				sessionLinkTextBox.selectAll();
			}
			
		});
		sessionLinkTextBox.addKeyPressHandler(new KeyPressHandler() {

			public void onKeyPress(KeyPressEvent arg0) {
				//ignore key press events
				refresh();
			}
			
		});
		
		statefulContent.setWidget(disabledContent);
		leftSide.add(statefulContent);		
		leftSide.add(quickTicketBrick);
		leftSide.add(sessionLinkTextBox);
		
		VerticalPanel rightSide = new VerticalPanel();
		rightSide.setStyleName("expert_session_block_right_side");
		enableDesktopApp.setWidget(enableHTML);
		rightSide.add(enableDesktopApp);
		rightSide.setCellHorizontalAlignment(enableDesktopApp, HorizontalPanel.ALIGN_RIGHT);
		
		
		HorizontalPanel body = new HorizontalPanel();
		body.setStyleName("expert_session_block_horiz_panel");
		
		body.add(leftSide);		
		body.setCellHorizontalAlignment(leftSide, HorizontalPanel.ALIGN_LEFT);
		body.add(rightSide);
		body.setCellHorizontalAlignment(rightSide, HorizontalPanel.ALIGN_RIGHT);
		body.setCellVerticalAlignment(rightSide, HorizontalPanel.ALIGN_BOTTOM);
		body.setStyleName("expert_session_block_body");
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(header);
		vp.add(body);
		
		setStyleName("expert_session_block");
		setWidget(vp);
	}
	
	private final void refresh() {
		if (expertDTO != null) {
			if (expertDTO.isLive()) {
				enableHTML = new HTML(getEnableButton(expertDTO.getExpertUid(), connected));
				statefulContent.setWidget(enabledContent);
			} else {
				enableHTML = new HTML(getEnableButton(expertDTO.getExpertUid(), disconnected));
				statefulContent.setWidget(disabledContent);
			}
			enableDesktopApp.setWidget(enableHTML);
		}	
		
		if (sessionDTO != null) {
			String encodedUrl = URL.encode(baseUrl + QuickTicket.getQuickTicketToken(sessionDTO.getSessionId()));
			encodedUrl.replaceAll("&", "%26");
			encodedUrl.replaceAll("=", "%3D");
			sessionIdHTML.setHTML("<a href=\"mailto:?subject=Let's use HelpGuest to fix your computer issue&" +
				"body=I can take a look at your computer with the helpguest.com knowledge sharing tool.%0A%0A" +
				"Follow this link and give me a call when you are ready for me to help.%0A%0A" +
				encodedUrl +
				"\">" +
				sessionDTO.getSessionId() +
				"</a>");
			sessionLinkTextBox.setText(encodedUrl);
			quickTicket.setWidget(sessionIdHTML);
		}
	}
	
	private String getEnableString(final String expertUid, final Image buttonImage) {
		String mimeTypes = Jaws.getMimeTypes();
		String buttonHtml = buttonImage.toString();
		String required =
			"<p>Prerequisite:&nbsp;" +
			Jaws.getUpgradeLaunchContent(expertUid, "Install", EXPERT) + 
			" the latest version of Java.</p>";
		StringBuffer stepOne = new StringBuffer("");
		
		if (Jaws.installOnWinIERequired()) {
			buttonHtml = windowsDisabledImage;				
			stepOne.insert(
					0, 
					Jaws.getWinIELaunchContent(expertUid, buttonHtml, EXPERT));
			stepOne.insert(0, required);
		} else if (Jaws.isWindowsNotIE() && mimeTypes.indexOf("application/x-java-applet;version=1.5") == -1) {
			stepOne.insert(
					0, 
					Jaws.getLaunchContent(expertUid, buttonHtml, EXPERT));
			stepOne.insert(0, required);
		} else if (Jaws.isMacintosh()) {
			stepOne.insert(
					0,
					Jaws.getSimpleLaunchContent(expertUid, buttonHtml, EXPERT));			
		} else {
			stepOne.insert(
					0, 
					Jaws.getLaunchContent(expertUid, buttonHtml, EXPERT));
		}
		
		return stepOne.toString();
	}
	
	
	private String getEnableButton(final String expertUid, final Image buttonImage) {
		String mimeTypes = Jaws.getMimeTypes();
		if (Jaws.isWindowsNotIE() && mimeTypes.indexOf("application/x-java-applet;version=1.5") == -1) {
			//if its windows and not ie and java is not yet installed
			return getEnableString(expertUid, buttonImage);
		}	
		
				
		String buttonContent = buttonImage.toString();
		String enable = "";
		if (Jaws.installOnWinIERequired()) {
			if (expertDTO.isLive()) {
				buttonContent = windowsEnabledImage;
			} else {
				buttonContent = windowsDisabledImage;
			}
			enable =
				Jaws.getWinIELaunchContent(expertUid, buttonContent, EXPERT);
		} else if (Jaws.isMacintosh()) {
			enable = 
				Jaws.getSimpleLaunchContent(expertUid, buttonContent, EXPERT);			
		} else {
			enable =
				Jaws.getLaunchContent(expertUid, buttonContent, EXPERT);
		}
		
		return enable;
	}
	
	
	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.widgets.DynamicExpertBlock#setExpertDTO(com.helpguest.marketplace.client.dto.ExpertDTO)
	 */
	public void setExpertDTO(final ExpertDTO expertDTO) {
		this.expertDTO = expertDTO;
		refresh();
	}
	
	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.widgets.DynamicSessionBlock#setSessionDTO(com.helpguest.marketplace.client.dto.SessionDTO)
	 */
	public void setSessionDTO(final SessionDTO sessionDTO) {
		this.sessionDTO = sessionDTO;
		refresh();
	}
	
	public void update(final ExpertDTO expertDTO, final SessionDTO sessionDTO) {
		this.expertDTO = expertDTO;
		this.sessionDTO = sessionDTO;
		refresh();
	}

}
