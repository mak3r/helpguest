package com.helpguest.marketplace.client.validated.widgets;

import com.helpguest.marketplace.client.dto.ExpertDTO;

public interface DynamicExpertBlock {

	public abstract void setExpertDTO(final ExpertDTO expertDTO);

}