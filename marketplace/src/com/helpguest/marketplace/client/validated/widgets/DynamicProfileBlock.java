package com.helpguest.marketplace.client.validated.widgets;

import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;

public interface DynamicProfileBlock {

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.widgets.DynamicExpertBlock#setExpertDTO(com.helpguest.marketplace.client.dto.ExpertDTO)
	 */
	public abstract void setExpertDTO(final ExpertDTO expertDTO);

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.widgets.DynamicSessionBlock#setSessionDTO(com.helpguest.marketplace.client.dto.SessionDTO)
	 */
	public abstract void setSessionDTO(final SessionDTO sessionDTO);
	
	/**
	 * Update the expert and session dtos at the same time.
	 * It is recommended that implementations use this method to refresh less often.
	 * @param expertDTO
	 * @param sessionDTO
	 */
	public abstract void update(final ExpertDTO expertDTO, final SessionDTO sessionDTO);

}