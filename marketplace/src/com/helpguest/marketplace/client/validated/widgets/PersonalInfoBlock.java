package com.helpguest.marketplace.client.validated.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.brick.RightLeftBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.util.DebugPanel;

public class PersonalInfoBlock extends SimplePanel implements DynamicExpertBlock {

	private ExpertDTO expertDTO;
	
	private Label bioLabel = new Label("About me:");
	private TextArea bioTextArea = new TextArea();
	private RightLeftBrick bioBrick = new RightLeftBrick(bioLabel, bioTextArea);
	
	private Image avatar = new Image();
	private AvatarUploadForm avatarUploadForm = new AvatarUploadForm();
	private Label fileSizeInfo = new Label("File size limit 100kb.");
	private Label fileTypeInfo = new Label("File types accepted; jpg, jpeg, png");
		
	Grid buttonGrid = new Grid(2,1);
	private Button edit = new Button("Edit");
	private Button save = new Button("Save");
	private Button cancel= new Button("Cancel");
	
	private SimplePanel header = new SimplePanel();
	
	
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	public PersonalInfoBlock() {
		//Initialize the ExpertService
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);
		
		addClickListeners();
		
		init();
	}
	
	public PersonalInfoBlock(final ExpertDTO expertDTO) {
		this.expertDTO = expertDTO;
		
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);
		
		addClickListeners();
		
		refresh();
	}
	
	private final void init() {
		//This method should not do anything that depends on
		//dynamic data.  Use refresh for the dynamic content.

		header.setStyleName("personal_info_block_header");
		header.setWidget(new Label("Personal Detail"));
		
		//disable all the editable fields to start
		setEditableEnabled(false);
				
		buttonGrid.setStyleName("personal_info_block_button_grid");
		buttonGrid.setWidget(0, 0, edit);
		buttonGrid.setWidget(1, 0, cancel);

		VerticalPanel avatarRight = new VerticalPanel();
		avatarRight.setStyleName("personal_info_block_avatar_right");
		avatarRight.add(avatarUploadForm);
		avatarRight.add(fileSizeInfo);
		avatarRight.add(fileTypeInfo);
		
		HorizontalPanel avatarPanel = new HorizontalPanel();
		avatarPanel.setStyleName("personal_info_block_avatar_panel");
		avatarPanel.add(avatar);
		avatarPanel.add(avatarRight);
		
		bioBrick.setStyleName("personal_info_block_bio_brick");
		
		VerticalPanel leftSide = new VerticalPanel();
		leftSide.setStyleName("personal_info_block_left_side");				
		leftSide.add(bioBrick);		
		leftSide.add(avatarPanel);
		
		HorizontalPanel body = new HorizontalPanel();
		body.setStyleName("personal_info_block_horiz_panel");
		
		body.add(leftSide);
		body.add(buttonGrid);
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(header);
		vp.add(body);
		
		setStyleName("personal_info_block");
		setWidget(vp);
		this.setVisible(true);
	}
	
	private final void refresh() {
		this.setVisible(false);		
		if (expertDTO != null) {
			bioTextArea.setText(expertDTO.getBio());
			avatar.setUrl(expertDTO.getAvatarURL());
		}	

		init();
	}
	
	private void addClickListeners() {
		edit.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				setEditableEnabled(true);
			}
		});
		
		cancel.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				setEditableEnabled(false);
			}
		});
		
		save.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				setEditableEnabled(false);
				
				//Now save the expertDTO edits
				if (expertDTO != null) {
					expertDTO.setBio(bioTextArea.getText());
					expertService.updateAccount(expertDTO, updateAccountCallback);
					if (avatarUploadForm.isUploadReady()) {
						DebugPanel.setText("PersonalInfoBlock", "Upload is ready");
						avatarUploadForm.submit();
						Timer timer = new Timer() {

							public void run() {
								// TODO Auto-generated method stub
								expertService.updateAccount(expertDTO, updateAccountCallback);							
							}
							
						};
						//Wait 30 seconds before updating
						timer.schedule(10000);
					}

				}
			}
		});
	}
	
	private void setEditableEnabled(final boolean enabled) {
		if (!enabled) {
			buttonGrid.setWidget(0, 0, edit);
		} else {
			buttonGrid.setWidget(0, 0, save);
		}
		edit.setEnabled(!enabled);
		save.setEnabled(enabled);
		cancel.setEnabled(enabled);
		bioTextArea.setEnabled(enabled);
	}
	
	AsyncCallback updateAccountCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("PersonalInfoBlock updateAccountCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("PersonalInfoBlock updateAccountCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			refresh();
		}		
	};
	
	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.widgets.DynamicExpertBlock#setExpertDTO(com.helpguest.marketplace.client.dto.ExpertDTO)
	 */
	public void setExpertDTO(final ExpertDTO expertDTO) {
		this.expertDTO = expertDTO;
		refresh();
	}

	

}
