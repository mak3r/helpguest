/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Sep 27, 2007
 */
package com.helpguest.marketplace.client.validated.widgets;


import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormHandler;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormSubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormSubmitEvent;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.helpguest.marketplace.client.bakery.CookieManager;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;


/**
 * @author mabrams
 *
 */
public class AvatarUploadForm extends FormPanel {

    public static final String FILE_UPLOAD_SUCCESS = "SUCCESS";

    private final FileUpload fileUpload = new FileUpload();
	private String avatarFileName = null;
	
	private static MarketPlaceConstants marketPlaceConstants = 
		(MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String aupFqdn = marketPlaceConstants.fullyQualifiedDomainName();
    private static String aupWebUriPrefix = marketPlaceConstants.webUriPrefix();
	
	private Hidden userName = new Hidden("user");
    private Hidden password = new Hidden("password");
	

	public AvatarUploadForm() {
		setMethod(FormPanel.METHOD_POST);
		setEncoding(FormPanel.ENCODING_MULTIPART);
		setAction(aupWebUriPrefix + "://" + aupFqdn + "/hgservlets/VersionTwoDetails");
		
		fileUpload.setName("avatar_upload_form_file_upload");
		
		addFormHandler(new FormHandler() {

			public void onSubmit(FormSubmitEvent event) {
				DebugPanel.setText("AvatarUploadForm", "FormHandler.onSubmit()");
				// This event is fired just before the form is submitted. We can
				// take
				// this opportunity to perform validation.
				userName.setValue(CookieManager.ponder(CookieManager.EMAIL_COOKIE));
				password.setValue("");
				
				avatarFileName = fileUpload.getFilename().trim();

				String ext = avatarFileName.substring(avatarFileName
						.indexOf('.'));

				if (avatarFileName.length() == 0) {
					event.setCancelled(true);
					DebugPanel.setText("AvatarUploadForm", "Cancelled form submit due to invalid file namelength");
				}

				if ((ext.equalsIgnoreCase(".png")) || (ext.equalsIgnoreCase(".jpeg"))
						|| (ext.equalsIgnoreCase(".jpg"))) {
					DebugPanel.setText("AvatarUploadForm", "file extension matches: " + ext);
				} else {
					Window.alert("File is: "
									+ avatarFileName
									+ "\nPlease specify an image file ending with '.png', '.jpeg', or '.jpg'.");
					event.setCancelled(true);
					DebugPanel.setText("AvatarUploadForm", "Cancelled form submit due to invalid file extension");
				}			
				

			}
			

			public void onSubmitComplete(FormSubmitCompleteEvent event) {
				if (!FILE_UPLOAD_SUCCESS.equals(event.getResults())) {
					//The results should be a preformatted string
					// from the AvatarReceiver servlet
					DebugPanel.setText("AvatarUploadForm", event.getResults());					
				} 
				DebugPanel.setText("AvatarUploadForm", event.getResults());	
			}
			
		});
		
		HorizontalPanel vp = new HorizontalPanel();
		vp.setStyleName("avatar_upload_form_vp");
		vp.add(fileUpload);
		vp.add(userName);
		vp.add(password);

		setStyleName("avatar_upload_form");
		setWidget(vp);
	}
	
	public boolean isUploadReady() {
		return !"".equals(fileUpload.getFilename().trim());
	}	
	
}
