package com.helpguest.marketplace.client.validated.widgets;

import com.helpguest.marketplace.client.dto.SessionDTO;

public interface DynamicSessionBlock {

	public abstract void setSessionDTO(final SessionDTO sessionDTO);

}