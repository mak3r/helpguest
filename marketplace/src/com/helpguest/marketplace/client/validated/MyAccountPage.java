/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Sep 12, 2007
 */
package com.helpguest.marketplace.client.validated;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;

import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.FormSubmitEvent;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;

import com.helpguest.marketplace.client.SessionService;
import com.helpguest.marketplace.client.SessionServiceAsync;
import com.helpguest.marketplace.client.brick.InfoBrick;
import com.helpguest.marketplace.client.brick.InputBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.PaymentPlanDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.ui.HeaderMenu;
import com.helpguest.marketplace.client.ui.HorizontalSpacer;
import com.helpguest.marketplace.client.ui.RoundedImage;
import com.helpguest.marketplace.client.ui.TwoTonePanel;
import com.helpguest.marketplace.client.util.Jaws;
import com.helpguest.marketplace.client.widgets.AvatarImage;
import com.helpguest.marketplace.client.widgets.ChangingTextButton;
import com.helpguest.marketplace.client.widgets.ExpertTagsWidget;
import com.helpguest.marketplace.client.widgets.GetNewPswdToResetPswdPanel;


/**
 * @author mabrams
 *
 */
public class MyAccountPage extends SimplePanel implements CommandToken {

	private HeaderMenu headerMenu;
	private Authenticator authSource;
	private ExpertDTO expertDTO;
	private UpdateExpertisePage updateExpertisePage;
	private CompletedSessionsPage completedSessionsPage;
	private InputBrick aboutMe;
	private InputBrick rateInput;
	private InputBrick sessionLength;
	private Button edit;
	private Button save;
	private String demoSessionId = "NO DEMO SESSION AVAILABLE";
	private AvatarUploadPanel avatarUploadPanel;
	private static final LoginType EXPERT = new LoginType.Expert();
	private static final String HISTORY_TOKEN = HeaderMenu.MY_ACCOUNT;
	
	
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	//Create the session service proxy
	final SessionServiceAsync sessionService =
		(SessionServiceAsync) GWT.create(SessionService.class);
	
	
	
	public MyAccountPage(final HeaderMenu headerMenu, final Authenticator authSource) {
		super();
		this.headerMenu = headerMenu;
		this.authSource = authSource;
		
		//Initialize the ExpertService
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);
		
		//Initialize the SessionService
		ServiceDefTarget sessionEndpoint = (ServiceDefTarget) this.sessionService;
		String sessionModuleRelativeURL = GWT.getModuleBaseURL() + "SessionService";
		sessionEndpoint.setServiceEntryPoint(sessionModuleRelativeURL);
		
		

	}
	
	private void init() {

		//Setup the expertise page
		updateExpertisePage = new UpdateExpertisePage(expertDTO);
		headerMenu.setLink(HeaderMenu.UPDATE_EXPERTISE, updateExpertisePage);
		authSource.addAuthenticationListener(updateExpertisePage);

		//Setup the completed sessions page
		completedSessionsPage = new CompletedSessionsPage(expertDTO);
		headerMenu.setLink(HeaderMenu.COMPLETED_SESSIONS, completedSessionsPage);
		authSource.addAuthenticationListener(completedSessionsPage);
		
		InfoBrick handle = new InfoBrick("Handle: ", expertDTO.getExpertAccount().getHandle());
		InfoBrick expertise = new InfoBrick("Expertise: ", "" + expertDTO.getExpertAccount().getExpertiseList());
		aboutMe = new InputBrick("About me: ", "400px", "6em");
		aboutMe.setInputText(expertDTO.getBio());
		rateInput = new InputBrick("Rate / minute (min. $.99): ", "6em");
		rateInput.setInputText(expertDTO.getPaymentPlan().getRatePerMinute());
		rateInput.setInputAccessory(new Label("$"), InputBrick.LEFT);
		sessionLength = new InputBrick("Preferred session length:", "3em");
		sessionLength.setInputText(String.valueOf(expertDTO.getPaymentPlan().getPreferredTime()));
		sessionLength.setInputAccessory(new Label("minutes"), InputBrick.RIGHT);
		InfoBrick minDeposit = new InfoBrick("Guest minimum deposit:", "$" + expertDTO.getPaymentPlan().getDepositAmount());
		avatarUploadPanel = new AvatarUploadPanel(expertDTO);
		InfoBrick demoSession = new InfoBrick("Demo Session ID: ", demoSessionId);
		
		
		Grid editButtons = new Grid(1,4);
		edit = new Button("Edit");
		save = new Button("Save changes");
		
		//disable all the editable fields to start
		setEditableEnabled(false);
		
		edit.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				setEditableEnabled(true);
			}
		});
		
		save.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				setEditableEnabled(false);
				
				//This requires the user password
				// and saves the new image.
				//prahalad adding 08/08/08 commenting getcredentials
				//Window.alert("Just before the call.");
				//avatarUploadPanel.getCredentials();
				avatarUploadPanel.submit();
				
				//Now save the expertDTO edits
				expertDTO.setBio(aboutMe.getInputText());
				PaymentPlanDTO pp = expertDTO.getPaymentPlan();
				pp.setPreferredTime(Integer.parseInt(sessionLength.getInputText()));
				pp.setRatePerMinute(rateInput.getInputText());
				expertService.updateAccount(expertDTO, updateAccountCallback);
			}
		});
		
		editButtons.setWidget(0, 1, edit);
		editButtons.setWidget(0, 3, save);
		
		AvatarImage avatarImage = new AvatarImage(expertDTO);
		RoundedImage avatar =
			new RoundedImage(avatarImage, "avatar_rounded_image", 5);
		avatar.setWidth("100px");
		
		/*
		HTML enableString = new HTML(
				getEnableString(expertDTO.getExpertUid()) 
		);
		vp.add(enableString);
		*/
		
		
		
		VerticalPanel ttBottom = new VerticalPanel();
		ttBottom.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		ttBottom.add(handle);
		ttBottom.add(expertise);
		ttBottom.add(aboutMe);
		//prahalad adding spacing and cat_offg_tag to ttbottom 7/09/08
		//ttBottom.add(hp_cat_offg_tag);
		ExpertTagsWidget etw = new ExpertTagsWidget(expertDTO);
		ttBottom.add(etw);
		
		ttBottom.add(sessionLength);
		ttBottom.add(rateInput);
		ttBottom.add(minDeposit);
		ttBottom.add(demoSession);
		ttBottom.add(avatarUploadPanel);
		ttBottom.add(avatar);
		ttBottom.add(new HorizontalSpacer(20));
		ttBottom.add(editButtons);
		ttBottom.setCellHorizontalAlignment(editButtons, HorizontalPanel.ALIGN_CENTER);
		ttBottom.setWidth("600px");
		ttBottom.setSpacing(10);
		
		Label head = new Label(expertDTO.getExpertAccount().getHandle().toUpperCase());

		TwoTonePanel ttp = new TwoTonePanel(head, ttBottom, "600px", "hgx-TwoToneTop");
		
		HTML enableHTML = new HTML(getEnableButton(expertDTO.getExpertUid()));
		enableHTML.setWidth("100px");
		enableHTML.setStyleName("hg-ChangingTextButton");

		VerticalPanel vp = new VerticalPanel();
		vp.setSpacing(20);
		vp.add(enableHTML);
		vp.add(ttp);
		setWidget(vp);
	}
	
	private void setEditableEnabled(final boolean enabled) {
		edit.setEnabled(!enabled);
		save.setEnabled(enabled);
		aboutMe.setEnabled(enabled);
		rateInput.setEnabled(enabled);
		sessionLength.setEnabled(enabled);
	}
	
	private String getEnableString(final String expertUid) {
		String required =
			"Prerequisite:&nbsp;" +
			Jaws.getUpgradeLaunchContent(expertUid, "Install", EXPERT) + 
			" the latest version of Java.<br/>Then:&nbsp;";
		StringBuffer stepOne = new StringBuffer(" my Expertise");
		if (Jaws.installOnWinIERequired()) {
			stepOne.insert(
					0,
					Jaws.getWinIELaunchContent(expertUid, "Enable", EXPERT));
		} else if (Jaws.isWindowsNotIE() && Jaws.installRequired()) {
			stepOne.insert(
					0, 
					Jaws.getLaunchContent(expertUid, "Enable", EXPERT));
			stepOne.insert(0, required);
		} else if (Jaws.isMacintosh()) {
			stepOne.insert(
					0,
					Jaws.getSimpleLaunchContent(expertUid, "Enable", EXPERT));			
		} else {
			stepOne.insert(
					0, 
					Jaws.getLaunchContent(expertUid, "Enable", EXPERT));
		}
		
		return stepOne.toString();
	}
	
	private String getEnableButton(final String expertUid) {
		if (Jaws.isWindowsNotIE() && Jaws.installRequired()) {
			return getEnableString(expertUid);
		}
				
		String disabledText = "Enable my expertise";
		String enable = "";
		if (Jaws.installOnWinIERequired()) {
			enable =
				Jaws.getWinIELaunchContent(expertUid, disabledText, EXPERT);
		} else if (Jaws.isMacintosh()) {
			enable =
				Jaws.getSimpleLaunchContent(expertUid, disabledText, EXPERT);			
		} else {
			enable =
				Jaws.getLaunchContent(expertUid, disabledText, EXPERT);
		}
		
		return new ChangingTextButton(enable, disabledText).toString();
	}
	
	AsyncCallback expertCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("MyAccountPage expertCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("MyAccountPage expertCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			sessionService.getDemoSession(expertDTO, sessionCallback);
		}		
	};
	
	
	AsyncCallback sessionCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			//if we didn't get a session back, just go ahead and init
			init();
		}

		public void onSuccess(Object result) {
			SessionDTO session = (SessionDTO) result;
			demoSessionId = session.getSessionId();
			init();
		}
		
	};
	
	AsyncCallback updateAccountCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("MyAccountPage updateAccountCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("MyAccountPage updateAccountCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			init();
		}		
	};
	
	/**
	 * {@inheritDoc}
	 */
	public String getBaseToken() {
		return HISTORY_TOKEN;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onEntry(Map<String, String> kvMap) {
		//No need to check the kvMap for arguments
		if (authSource != null) {
			if (authSource.isLoggedIn() ) {
				//If the expert has not been initialized or
				// the expert does not match the currently authenticated party
				if (expertDTO == null || 
						!authSource.getAuthenticatedParty()
						.equals(expertDTO.getExpertAccount().getEmail())) {
					expertService.getExpert(authSource.getAuthenticatedParty(), expertCallback);
				} else { 
					//We need to init again in case the login had been expired
					init();
				}
			} else {
				expertDTO = null;
				setWidget(authSource.getExpiredWidget());
			}
		} else {
			setWidget(new Label("This page has expired."));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void onExit() {
		// TODO Auto-generated method stub

	}

}
