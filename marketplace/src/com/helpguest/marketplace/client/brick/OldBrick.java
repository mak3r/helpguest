/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 26, 2007
 * 
 * @deprecated use Brick and it's subclasses instead which don't rely on preformatting but rather css.
 */
package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * A DataBrick is a 1x2 block of panels.
 * 
 * @author mabrams
 *
 */
public class OldBrick extends HorizontalPanel {
	
	SimplePanel left = new SimplePanel();
	SimplePanel right = new SimplePanel();
	
	public OldBrick(
			HorizontalAlignmentConstant leftAlignment,
			HorizontalAlignmentConstant rightAlignment) {
		add(left);
		setCellHorizontalAlignment(left, leftAlignment);
		add(right);		
		setCellHorizontalAlignment(right, rightAlignment);
	}
	
	public void setLeft(final Widget leftWidget) {
		left.setWidget(leftWidget);
	}
	
	public void setRight(final Widget rightWidget) {
		right.setWidget(rightWidget);
	}
	
	public void setLeftWidth(final String width) {
		left.setWidth(width);
	}
	
	public void setRightWidth(final String width) {
		right.setWidth(width);
	}
}
