package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class RightLeftBrick extends SimplePanel {

	public RightLeftBrick(final Widget rightJustified, final Widget leftJustified) {
		HorizontalPanel hp = new HorizontalPanel();
		hp.add(rightJustified);
		hp.setCellHorizontalAlignment(rightJustified, HorizontalPanel.ALIGN_RIGHT);
		hp.add(leftJustified);
		hp.setCellHorizontalAlignment(leftJustified, HorizontalPanel.ALIGN_RIGHT);
		setWidget(hp);
		//Add Style Name
		rightJustified.addStyleName("right_left_brick_right_justified");
		leftJustified.addStyleName("left_right_brick_left_justified");
		hp.setStyleName("left_right_brick_hp");
		setStyleName("left_right_brick");
	}
	
	public RightLeftBrick(final String rightJustified, final Widget leftJustified) {
		this(new Label(rightJustified), leftJustified);
	}
}
