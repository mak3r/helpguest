/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 26, 2007
 */
package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusListener;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.SourcesChangeEvents;
import com.google.gwt.user.client.ui.SourcesClickEvents;
import com.google.gwt.user.client.ui.SourcesFocusEvents;
import com.google.gwt.user.client.ui.SourcesKeyboardEvents;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * Right aligns the left widget and 
 * Automatically Creates an input text box for the right widget.
 * 
 * @author mabrams
 *
 */
public class InputBrick extends SimplePanel 
		implements SourcesKeyboardEvents, SourcesFocusEvents, 
		SourcesClickEvents, SourcesChangeEvents {

	private static final String defaultTextBoxWidth = "100px";
	public static final int TEXT_BOX_TYPE = 0;
	public static final int TEXT_AREA_TYPE = 1;
	public static final int PASSWORD_TYPE = 2;
	public static final int LIST_BOX_TYPE = 3;
	public static final int TOP = 0;
	public static final int RIGHT = 1;
	public static final int BOTTOM = 2;
	public static final int LEFT = 3;
	
	private Label left = new Label();
	private TextBox right = new TextBox();
	private TextArea rightTall = new TextArea();
	private PasswordTextBox rightPrivate = new PasswordTextBox();
	private ListBox listBox = new ListBox();
	private Brick foundationBrick;
	private Widget inputWidget = new Widget();
	private int textInputType = TEXT_BOX_TYPE;
	
	private static final String LEFT_STYLE = "input_brick_left";
	private static final String RIGHT_STYLE = "input_brick_widget";
	private static final String ACCESSORY_STYLE = "input_brick_accessory";
	
	public InputBrick(final String label) {	
		this(label, defaultTextBoxWidth);
	}
	
	/**
	 * By default, this is a text box type.
	 * @param label
	 * @param inputFieldWidth
	 */
	public InputBrick(final String label, final String inputFieldWidth) {
		this(label, inputFieldWidth, 0, TEXT_BOX_TYPE);
	}

	/**
	 * Specify the type of input field to use as the last parameter of this 
	 * method.
	 * @param label
	 * @param inputFieldWidth
	 */
	public InputBrick(final String label, final String inputFieldWidth, final int type) {
		this(label, inputFieldWidth, 0, type);
	}

	/**
	 * This automatically creates a text area type.  since it has 
	 * a inputFieldHeight.
	 * @param label
	 * @param inputFieldWidth
	 * @param inputFieldHeight
	 */
	public InputBrick(final String label, 
			final String inputFieldWidth,
			final String inputFieldHeight) {
		this(label, inputFieldWidth, inputFieldHeight, 0, TEXT_AREA_TYPE);
	}

	public InputBrick(final String label, 
			final String inputFieldWidth,
			final int centerSpacing,
			final int type) {
		this(label, inputFieldWidth, null, centerSpacing, type);
	}
	
	/**
	 * Create an input brick based on the given parameters.
	 * 
	 * @param label is the label that will be displayed
	 * to the left of the input field.
	 * @param inputFieldWidth is the width of the input field.
	 * @param inputFieldHeight is the height of the input field.
	 * Use null to get a single line input field or single line list box.
	 * @param centerSpacing is the space between the label and the 
	 * input field. 
	 */
	public InputBrick(final String label, 
			final String inputFieldWidth,
			final String inputFieldHeight,
			final int centerSpacing,
			final int inputType) {
		//IMPORTANT must set the type as other
		// operations depend on this being correct
		textInputType = inputType;
		if (textInputType == TEXT_AREA_TYPE) {
			rightTall.setWidth(inputFieldWidth);
			rightTall.setHeight(inputFieldHeight);
			inputWidget = rightTall;
		} else if (textInputType == TEXT_BOX_TYPE) {
			right.setWidth(inputFieldWidth);
			inputWidget = right;
		} else if (textInputType == PASSWORD_TYPE) {
			//its got to be a PASSWORD_TYPE
			rightPrivate.setWidth(inputFieldWidth);
			inputWidget = rightPrivate;
		} else if (textInputType == LIST_BOX_TYPE) {
			if (inputFieldHeight != null) {
				listBox.setVisibleItemCount(Integer.parseInt(inputFieldHeight));
			}
			inputWidget = listBox;
		}
		left = new Label(label);
		
		foundationBrick = new HorizontalBrick(left, inputWidget);
		((HorizontalBrick)foundationBrick).alignRight(foundationBrick.getLeft());
		
		left.setStyleName(LEFT_STYLE);
		inputWidget.setStyleName(RIGHT_STYLE);

		setStyleName("input_brick");		
		setWidget(foundationBrick);
	}
	
	/**
	 * @deprecated use css to set the width style name is 'input_brick'
	 */
	public void setLeftWidth(final String width) {
		left.setWidth(width);
	}
	
	/**
	 * @deprecated use css to set the width style name is 'input_brick'
	 */
	public void setWidth(final String width) {
		foundationBrick.setWidth(width);
	}
	
	public String getInputText() {
		if (textInputType == TEXT_BOX_TYPE) {
			return right.getText();
		} else if (textInputType == TEXT_AREA_TYPE) {
			return rightTall.getText();
		} else if (textInputType == PASSWORD_TYPE) {
			return rightPrivate.getText();
		} else if (textInputType == LIST_BOX_TYPE) {
			return listBox.getItemText(listBox.getSelectedIndex());
		}
		return "";
	}
	
	public void setInputText(final String text) {
		if (textInputType == TEXT_BOX_TYPE) {
			right.setText(text);
		} else if (textInputType == TEXT_AREA_TYPE) {
			rightTall.setText(text);
		} else if (textInputType == PASSWORD_TYPE) {
			//must be PASSWORD_TYPE
			rightPrivate.setText(text);
		} else if (textInputType == LIST_BOX_TYPE) {
			if (text == null && listBox.getItemCount() > 0) {
				listBox.setSelectedIndex(1);
			}
			listBox.addItem(text);
		}
	}
	
	public void setInputAccessory(final Widget accessory, final int location) {
		HorizontalBrick innerBrick = null;
		left.setStylePrimaryName(LEFT_STYLE);
		inputWidget.setStylePrimaryName(RIGHT_STYLE);

		switch (location) {
		case TOP:
			innerBrick = new HorizontalBrick(left, inputWidget);
			innerBrick.alignRight(innerBrick.getLeft());
			foundationBrick = new VerticalBrick(accessory, innerBrick);
			break;
		case RIGHT:
			innerBrick = new HorizontalBrick(left, inputWidget);
			innerBrick.alignRight(innerBrick.getLeft());
			foundationBrick = new HorizontalBrick(innerBrick, accessory);
			((HorizontalBrick)foundationBrick).alignRight(foundationBrick.getLeft());
			break;
		case BOTTOM:
			innerBrick = new HorizontalBrick(left, inputWidget);
			innerBrick.alignRight(innerBrick.getLeft());
			foundationBrick = new VerticalBrick(innerBrick, accessory);
			break;
		case LEFT:
		default:
			innerBrick = new HorizontalBrick(left, accessory);
			innerBrick.alignRight(innerBrick.getLeft());
			foundationBrick = new HorizontalBrick(innerBrick, inputWidget);
			((HorizontalBrick)foundationBrick).alignRight(foundationBrick.getLeft());
			break;
		}
		setWidget(foundationBrick);
		accessory.setStylePrimaryName(ACCESSORY_STYLE);
	}
	
	public void setEnabled(final boolean enabled) {
		if (textInputType == TEXT_BOX_TYPE) {
			right.setEnabled(enabled);
		} else if (textInputType == TEXT_AREA_TYPE) {
			rightTall.setEnabled(enabled);
		} else if (textInputType == PASSWORD_TYPE) {
			//must be PASSWORD_TYPE
			rightPrivate.setEnabled(enabled);
		} else if (textInputType == LIST_BOX_TYPE) {
			listBox.setEnabled(enabled);
		}
	}

	public void addKeyboardListener(KeyboardListener listener) {
		right.addKeyboardListener(listener);
		rightTall.addKeyboardListener(listener);
		rightPrivate.addKeyboardListener(listener);
		listBox.addKeyboardListener(listener);
	}

	public void removeKeyboardListener(KeyboardListener listener) {
		right.removeKeyboardListener(listener);
		rightTall.removeKeyboardListener(listener);
		rightPrivate.removeKeyboardListener(listener);
		listBox.removeKeyboardListener(listener);
	}

	public void addFocusListener(FocusListener listener) {
		right.addFocusListener(listener);
		rightTall.addFocusListener(listener);
		rightPrivate.addFocusListener(listener);
		listBox.addFocusListener(listener);
	}

	public void removeFocusListener(FocusListener listener) {
		right.removeFocusListener(listener);
		rightTall.removeFocusListener(listener);
		rightPrivate.removeFocusListener(listener);
		listBox.removeFocusListener(listener);
	}

	public void addClickListener(ClickListener listener) {
		right.addClickListener(listener);
		rightTall.addClickListener(listener);
		rightPrivate.addClickListener(listener);
		listBox.addClickListener(listener);
	}

	public void removeClickListener(ClickListener listener) {
		right.removeClickListener(listener);
		rightTall.removeClickListener(listener);
		rightPrivate.addClickListener(listener);
		listBox.removeClickListener(listener);
	}

	public void addChangeListener(ChangeListener listener) {
		right.addChangeListener(listener);
		rightTall.addChangeListener(listener);
		rightPrivate.addChangeListener(listener);
		listBox.addChangeListener(listener);
	}

	public void removeChangeListener(ChangeListener listener) {
		right.removeChangeListener(listener);
		rightTall.removeChangeListener(listener);
		rightPrivate.removeChangeListener(listener);
		listBox.removeChangeListener(listener);
	}
	
}
