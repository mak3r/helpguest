package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class VerticalBrick extends Brick {

	VerticalPanel vp = new VerticalPanel();

	public VerticalBrick(final Widget top, final Widget bottom) {
		
		super.top = top;
		super.bottom = bottom;
		
		vp.setStyleName("vertical_brick_vp");
		
		vp.add(top);
		vp.add(bottom);
		top.setStylePrimaryName("vertical_brick_top");
		bottom.setStylePrimaryName("vertical_brick_bottom");
		
		setWidget(vp);
		setStylePrimaryName("vertical_brick");
	}
}
