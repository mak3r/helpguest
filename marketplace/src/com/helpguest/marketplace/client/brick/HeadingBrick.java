/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 26, 2007
 */
package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This brick has a default layout of putting
 * the left widget left adjusted and the
 * right widget right adusted
 * 
 * @author mabrams
 *
 */
public class HeadingBrick extends OldBrick {

	public HeadingBrick() {
		super(HorizontalPanel.ALIGN_LEFT, HorizontalPanel.ALIGN_RIGHT);
		setStyleName("helpguest_InputBrick");
	}
	
	public HeadingBrick(final Widget left, final Widget right) {
		this();
		setLeft(left);
		setRight(right);
	}
}
