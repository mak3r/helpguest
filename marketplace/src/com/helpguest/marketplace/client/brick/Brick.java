package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Brick extends SimplePanel {

	Widget left;
	Widget right;
	Widget top;
	Widget bottom;
	
	public Widget getTop() {
		return top;
	}
	
	public Widget getBottom() {
		return bottom;
	}
	
	public Widget getLeft() {
		return left;
	}
	
	public Widget getRight() {
		return right;
	}
	
	
}