/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Sep 21, 2007
 */
package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment.HorizontalAlignmentConstant;
import com.helpguest.marketplace.client.ui.VerticalSpacer;

/**
 * @author mabrams
 *
 */
public class InfoBrick extends SimplePanel {

	private Label left = new Label();
	private Label right = new Label();
	private static final HorizontalAlignmentConstant defaultAlignment = 
		HorizontalPanel.ALIGN_RIGHT;
	private static final int defaultSpacing = 10;
	
	public InfoBrick() {
		this("", "");
	}

	public InfoBrick(final HorizontalAlignmentConstant alignLabel) {
		this("", alignLabel);
	}
	
	public InfoBrick(final String label) {
		this(label, defaultAlignment);
	}
	
	public InfoBrick(final String label, final HorizontalAlignmentConstant alignLabel) {
		this(label, "", defaultSpacing, alignLabel);
	}
	
	public InfoBrick(final String label, final String info) {
		this(label, info, defaultSpacing, defaultAlignment);
	}
	
	public InfoBrick(final String label, final String info, final HorizontalAlignmentConstant alignLabel) {
		this(label, info, defaultSpacing, alignLabel);
	}
	
	public InfoBrick(final String label, final String info, final int centerSpacing) {
		this(label, info, centerSpacing, defaultAlignment);
	}
	
	public InfoBrick(
			final String label, 
			final String info, 
			final int centerSpacing, 
			final HorizontalAlignmentConstant alignLabel) {
		left.setText(label);
		right.setText(info);

		HorizontalPanel hp = new HorizontalPanel();		
		hp.add(left);
		hp.add(new VerticalSpacer(centerSpacing));
		hp.add(right);
		hp.setCellHorizontalAlignment(left, alignLabel);
		hp.setCellHorizontalAlignment(right, HorizontalPanel.ALIGN_LEFT);
		
		left.setStyleName("hg-InfoBrick-left");
		right.setStyleName("hg-InfoBrick-right");
		
		setWidget(hp);
	}
	
	public void setLeft(final Label left) {
		this.left = left;
	}
	
	public void setRight(final Label right) {
		this.right = right; 
	}
}
