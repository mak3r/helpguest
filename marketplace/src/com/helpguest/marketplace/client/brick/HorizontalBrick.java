package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

public class HorizontalBrick extends Brick {

	HorizontalPanel hp = new HorizontalPanel();

	public HorizontalBrick(final Widget left, final Widget right) {
		
		super.left = left;
		super.right = right;
		
		hp.setStylePrimaryName("horizontal_brick_hp");
		
		hp.add(left);
		hp.add(right);
		left.setStylePrimaryName("horizontal_brick_left");
		right.setStylePrimaryName("horizontal_brick_right");
		
		setWidget(hp);
		setStylePrimaryName("horizontal_brick");
	}
	
	public void alignLeft(final Widget child) {
		if (child.equals(left)) {
			hp.setCellHorizontalAlignment(left, HorizontalPanel.ALIGN_LEFT);
		} else if (child.equals(right)){
			hp.setCellHorizontalAlignment(right, HorizontalPanel.ALIGN_LEFT);			
		}
 	}

	public void alignRight(final Widget child) {
		if (child.equals(left)) {
			hp.setCellHorizontalAlignment(left, HorizontalPanel.ALIGN_RIGHT);
		} else if (child.equals(right)){
			hp.setCellHorizontalAlignment(right, HorizontalPanel.ALIGN_RIGHT);			
		}
 	}

	public void alignCenter(final Widget child) {
		if (child.equals(left)) {
			hp.setCellHorizontalAlignment(left, HorizontalPanel.ALIGN_CENTER);
		} else if (child.equals(right)){
			hp.setCellHorizontalAlignment(right, HorizontalPanel.ALIGN_CENTER);			
		}
 	}


}
