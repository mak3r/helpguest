/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 26, 2007
 */
package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Left aligns the left side.
 * Right aligns the right side.
 * By default, both left and right side are label widgets
 * 
 * @author mabrams
 *
 */
public class DetailBrick extends OldBrick {
	
	private Label left = new Label();
	private SimplePanel right = new SimplePanel();
	
	public DetailBrick() {
		super(HorizontalPanel.ALIGN_LEFT, HorizontalPanel.ALIGN_RIGHT);
		setStyleName("helpguest_DetailBrick");
		setLeft(left);
		left.setStyleName("helpguest_DetailBrickLeft");
		setRight(right);
		setCellHorizontalAlignment(right, HorizontalPanel.ALIGN_RIGHT);
		right.setStyleName("helpguest_DetailBrickRight");
	}
	
	public DetailBrick(final String label, final String value) {
		this();
		left.setText(label);
		right.setWidget(new Label(value));
	}

	public DetailBrick(final String label, final int value) {
		this();
		left.setText(label);
		right.setWidget(new Label(String.valueOf(value)));
	}

	public DetailBrick(final String label, final float value) {
		this();
		left.setText(label);
		right.setWidget(new Label(String.valueOf(value)));
	}
	
	public DetailBrick(final String label, final Widget w) {
		this();
		left.setText(label);
		right.setWidget(w);
	}
	
	public void setLeftWidth(final String width) {
		left.setWidth(width);
	}
	
	public void setRightWidth(final String width) {
		right.setWidth(width);
	}

}
