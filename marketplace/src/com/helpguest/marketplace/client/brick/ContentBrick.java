/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 26, 2007
 */
package com.helpguest.marketplace.client.brick;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Left aligns the left and right widget.
 * 
 * @author mabrams
 *
 */
public class ContentBrick extends OldBrick {
	
	public ContentBrick() {
		super(HorizontalPanel.ALIGN_LEFT, HorizontalPanel.ALIGN_LEFT);
		setStyleName("helpguest_DetailBrick");
	}
	
	public ContentBrick(final Widget left, final Widget right) {
		this();
		setLeft(left);
		setRight(right);
	}

}
