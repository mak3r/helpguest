/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 23, 2007
 */
package com.helpguest.marketplace.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.helpguest.gwt.search.QueryConstraints;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.OfferingsConstraints;
import com.helpguest.gwt.search.TagsConstraints;
import com.helpguest.marketplace.client.dto.ExpertDTO;


/**
 * @author mabrams
 *
 */
public interface SearchService extends RemoteService {
	
	/**
	 * @param query String
	 * @return a List of ExpertDTO's
	 */
	public List<com.helpguest.marketplace.client.dto.ExpertDTO> findExperts(final String query);
	
	public ExpertDTO getExpertByUid(final String expertUid);

	public ExpertDTO getExpertByHandle(final String handle);
	
	//prahalad 6/26/08 adding this method that will return offerings for a category
	public List<String> getOfferings(final CategoryConstraints cc);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a find expert textbox search
	public List<String> getExpertIdList(final QueryConstraints qc);
	
	//prahalad 07/09/08 adding this method that will return expertid list to drill down
	//from existing expert list and textbox search
	public List<String> getExpertIdList(final QueryConstraints qc, List<String> parentExpertIdList);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a category
	public List<String> getExpertIdList(final CategoryConstraints cc);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a category and drilling down 
	//into a known list of experts
	public List<String> getExpertIdList(CategoryConstraints cc, List<String> parentExpertIdList);
	
	//prahalad 10/30/08 adding this method that will return expertid list for a category and drilling down 
	//into a known list of experts within a query search for display under category tabs
	public List<String> getExpertIdList(CategoryConstraints cc, QueryConstraints qc, List<String> parentExpertIdList);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a offering
	public List<String> getExpertIdList(final OfferingsConstraints oc);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a offering and drilling down 
	//into a known list of experts
	public List<String> getExpertIdList(OfferingsConstraints oc, List<String> parentExpertIdList);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a tag
	public List<String> getExpertIdList(TagsConstraints tc);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a tag and drilling down 
	//into a known list of experts
	public List<String> getExpertIdList(TagsConstraints tc, List<String> parentExpertIdList);

}
