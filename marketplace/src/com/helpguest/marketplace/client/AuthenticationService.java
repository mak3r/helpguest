/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.helpguest.gwt.protocol.AuthenticationOutcome;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.gwt.protocol.VerifyEmailOutcome;

/**
 * @author mabrams
 *
 */
public interface AuthenticationService extends RemoteService {

	/**
	 * @param email String
	 * @param password String
	 * @param type the LoginType
	 * @param doGenerateJnlp generate a jnlp file(s) server side
	 * @return the AuthenticationOutcome
	 */
	public AuthenticationOutcome authenticate(
			final String email, final String password,
			final LoginType type, final boolean doGenerateJnlp);

	/**
	 * @param email String
	 * @param password String
	 * @param type the LoginType
	 * @return the AuthenticationOutcome
	 */
	public AuthenticationOutcome authenticate(
			final String email, 
			final String password,
			final LoginType type);
	
	/**
	 * @param email the email address to be verified.
	 * @param uid the uid associated with this email address.
	 * @return the VerifyEmailOutcome.
	 */
	public VerifyEmailOutcome verifyEmail(
			final String email,
			final String uid);
	
	/**
	 * prahalad adding 07/28/08
	 * @param email the email address to be verified.
	 * @return the VerifyEmailOutcome.
	 */
	public VerifyEmailOutcome verifyEmail(
			final String email);

}
