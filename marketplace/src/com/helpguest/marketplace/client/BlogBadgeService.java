/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.helpguest.marketplace.client.dto.BadgeDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public interface BlogBadgeService extends RemoteService {

	public BadgeDTO createBadge(final ExpertDTO expertDTO, final BadgeDTO badgeDTO);

	public BadgeDTO updateBadge(final ExpertDTO expertDTO, final BadgeDTO badgeDTO);
	
	public BadgeDTO deleteBadge(final ExpertDTO expertDTO, final BadgeDTO badgeDTO);
	
	public List<BadgeDTO> getAllBadges(final ExpertDTO expertDTO);
	
}
