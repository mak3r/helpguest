/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import java.util.List;
import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.helpguest.gwt.protocol.SignUpOutcome;
import com.helpguest.marketplace.client.dto.EvaluationDTO;
import com.helpguest.marketplace.client.dto.ExpertAccountDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public interface ExpertService extends RemoteService {

	public ExpertDTO getExpert(final String expertUid);
	
	public void makePayments(final ExpertAccountDTO eaDTO);
	
	public ExpertDTO modifyExpertise(
			final ExpertDTO dto, 
			final String updateList,
			final String mode);
	
	public SignUpOutcome createAccount(
			final String email, 
    		final String pass,
    		final String handle, 
    		final String offering, 
    		final String zip);
	
	/**
	 * prahalad adding 7/23/08 alternate createAccount method to handle ArrayList of catOffgTags
	 * @param email email address
	 * @param pass password
	 * @param handle expert selected handle
	 * @param zip zipcode
	 * @param referralSource the String value of the referral source
	 * @return a SignUpOutcome
	 */
	public SignUpOutcome createAccount(
			final String email, 
    		final String pass,
    		final String handle, 
    		final List<java.lang.String> catOffgTagList, 
    		final String zip,
    		final String referralSource);
	
	
	public EvaluationDTO getEvaluation(final ExpertDTO expert);

	public ExpertDTO updateAccount(final ExpertDTO expert);
	
	//prahalad adding updatePaswd, expireResetCode, getResetCode, sendEmail 07/30/08 
	//for password reset app 
	public void updatePaswd(final ExpertDTO expert, final String newPaswd);
	
	public void expireResetCode(final String currResetCode, final String expertEmail);
	
	public void getResetCode(final String expertEmail);
	
	public ArrayList<java.lang.String> getSurveyFromDB(final String uid, final String sessionId);
	
	public void uploadAvatarFile(final String expert, final String avatarFile);
	
	public void submitSurveyToDB(final String uid, final String sessionId, 
			final String strSatisfaction, final String strProblemSolved, final String strRecommend, 
			final String strRepeatCustomer, final String strProfessionalManner, final String surveyAction);
	
	
}
