/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 25, 2007
 */
package com.helpguest.marketplace.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 *
 */
public class RatesDTO implements IsSerializable {

	private int id;
	private int expertId;
	private float minRate;
	private int minTime;
	private int prefTime;
	private float depositAmount;
	private float minFee;
	private String formattedMinRate;
	private String formattedDepositAmount;
	private String formattedMinFee;

	/**
	 * @return the depositAmount
	 */
	public float getDepositAmount() {
		return depositAmount;
	}

	/**
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(float depositAmount) {
		this.depositAmount = depositAmount;
	}

	/**
	 * @return the expertId
	 */
	public int getExpertId() {
		return expertId;
	}

	/**
	 * @param expertId the expertId to set
	 */
	public void setExpertId(int expertId) {
		this.expertId = expertId;
	}

	/**
	 * @return the formattedDepositAmount
	 */
	public String getFormattedDepositAmount() {
		return formattedDepositAmount;
	}

	/**
	 * @param formattedDepositAmount the formattedDepositAmount to set
	 */
	public void setFormattedDepositAmount(String formattedDepositAmount) {
		this.formattedDepositAmount = formattedDepositAmount;
	}

	/**
	 * @return the formattedMinFee
	 */
	public String getFormattedMinFee() {
		return formattedMinFee;
	}

	/**
	 * @param formattedMinFee the formattedMinFee to set
	 */
	public void setFormattedMinFee(String formattedMinFee) {
		this.formattedMinFee = formattedMinFee;
	}

	/**
	 * @return the formattedMinRate
	 */
	public String getFormattedMinRate() {
		return formattedMinRate;
	}

	/**
	 * @param formattedMinRate the formattedMinRate to set
	 */
	public void setFormattedMinRate(String formattedMinRate) {
		this.formattedMinRate = formattedMinRate;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the minFee
	 */
	public float getMinFee() {
		return minFee;
	}

	/**
	 * @param minFee the minFee to set
	 */
	public void setMinFee(float minFee) {
		this.minFee = minFee;
	}

	/**
	 * @return the minRate
	 */
	public float getMinRate() {
		return minRate;
	}

	/**
	 * @param minRate the minRate to set
	 */
	public void setMinRate(float minRate) {
		this.minRate = minRate;
	}

	/**
	 * @return the minTime
	 */
	public int getMinTime() {
		return minTime;
	}

	/**
	 * @param minTime the minTime to set
	 */
	public void setMinTime(int minTime) {
		this.minTime = minTime;
	}

	/**
	 * @return the prefTime
	 */
	public int getPrefTime() {
		return prefTime;
	}

	/**
	 * @param prefTime the prefTime to set
	 */
	public void setPrefTime(int prefTime) {
		this.prefTime = prefTime;
	}

}
