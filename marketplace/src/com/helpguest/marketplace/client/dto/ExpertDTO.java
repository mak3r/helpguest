/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 23, 2007
 */
package com.helpguest.marketplace.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 *
 */
public class ExpertDTO implements IsSerializable {

	private String handle;
	private String bio;
	private boolean isLive;
	private String expertUid;
	private PaymentPlanDTO paymentPlan;
	private ExpertStatsDTO stats;
	private ExpertAccountDTO expertAccount;
	private String avatarURL;
	private boolean canDemo;
	
	
	
	/**
	 * @return the expertAccountDTO
	 */
	public ExpertAccountDTO getExpertAccount() {
		return expertAccount;
	}
	/**
	 * @param expertAccountDTO the expertAccountDTO to set
	 */
	public void setExpertAccount(ExpertAccountDTO expertAccountDTO) {
		this.expertAccount = expertAccountDTO;
	}
	/**
	 * @return the bio
	 */
	public String getBio() {
		return this.bio;
	}
	/**
	 * @param bio the bio to set
	 */
	public void setBio(final String bio) {
		this.bio = bio;
	}
	/**
	 * @return the expertUid
	 */
	public String getExpertUid() {
		return this.expertUid;
	}
	/**
	 * @param expertUid the expertUid to set
	 */
	public void setExpertUid(final String expertUid) {
		this.expertUid = expertUid;
	}
	/**
	 * @return the handle
	 */
	public String getHandle() {
		return this.handle;
	}
	/**
	 * @param handle the handle to set
	 */
	public void setHandle(final String handle) {
		this.handle = handle;
	}
	/**
	 * @return the paymentPlan
	 */
	public PaymentPlanDTO getPaymentPlan() {
		return this.paymentPlan;
	}
	/**
	 * @param paymentPlan the paymentPlan to set
	 */
	public void setPaymentPlan(final PaymentPlanDTO paymentPlan) {
		this.paymentPlan = paymentPlan;
	}
	/**
	 * @return the stats
	 */
	public ExpertStatsDTO getStats() {
		return this.stats;
	}
	/**
	 * @param stats the stats to set
	 */
	public void setStats(final ExpertStatsDTO stats) {
		this.stats = stats;
	}
	/**
	 * @return the isLive
	 */
	public boolean isLive() {
		return this.isLive;
	}
	/**
	 * @param isLive the isLive to set
	 */
	public void setLive(final boolean isLive) {
		this.isLive = isLive;
	}
	/**
	 * @return the avatarURL
	 */
	public String getAvatarURL() {
		return avatarURL;
	}
	/**
	 * @param avatarURL the avatarURL to set
	 */
	public void setAvatarURL(final String avatarURL) {
		this.avatarURL = avatarURL;
	}
	/**
	 * @return the canDemo
	 */
	public boolean isCanDemo() {
		return canDemo;
	}
	/**
	 * @param canDemo the canDemo to set
	 */
	public void setCanDemo(final boolean canDemo) {
		this.canDemo = canDemo;
	}
	
}
