/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Oct 25, 2007
 */
package com.helpguest.marketplace.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 *
 */
public class EvaluationDTO implements IsSerializable {

    private float satisfaction = 0.0f;
    
    private int satisfactionCount = 0;
    
    private int problemsSolved = 0;
    
    private int problemsSolvedCount = 0;
    
    private int repeatCustomers = 0;
    
    private int repeatCustomersCount = 0;
    
    private int pointCount = 0;
    
    private int possiblePoints = 0;
    
    private String formattedProblemsSolved;
    
    private String formattedRepeatCustomers;
    
    private String formattedPointPercent;
    
    private String formattedOverallSatisfaction;

	/**
	 * @return the formattedPointPercent
	 */
	public String getFormattedPointPercent() {
		return formattedPointPercent;
	}

	/**
	 * @param formattedPointPercent the formattedPointPercent to set
	 */
	public void setFormattedPointPercent(final String formattedPointPercent) {
		this.formattedPointPercent = formattedPointPercent;
	}

	/**
	 * @return the formattedProblemsSolved
	 */
	public String getFormattedProblemsSolved() {
		return formattedProblemsSolved;
	}

	/**
	 * @param formattedProblemsSolved the formattedProblemsSolved to set
	 */
	public void setFormattedProblemsSolved(final String formattedProblemsSolved) {
		this.formattedProblemsSolved = formattedProblemsSolved;
	}

	/**
	 * @return the formattedRepeatCustomers
	 */
	public String getFormattedRepeatCustomers() {
		return formattedRepeatCustomers;
	}

	/**
	 * @param formattedRepeatCustomers the formattedRepeatCustomers to set
	 */
	public void setFormattedRepeatCustomers(final String formattedRepeatCustomers) {
		this.formattedRepeatCustomers = formattedRepeatCustomers;
	}

	/**
	 * @return the pointCount
	 */
	public int getPointCount() {
		return pointCount;
	}

	/**
	 * @param pointCount the pointCount to set
	 */
	public void setPointCount(final int pointCount) {
		this.pointCount = pointCount;
	}

	/**
	 * @return the possiblePoints
	 */
	public int getPossiblePoints() {
		return possiblePoints;
	}

	/**
	 * @param possiblePoints the possiblePoints to set
	 */
	public void setPossiblePoints(final int possiblePoints) {
		this.possiblePoints = possiblePoints;
	}

	/**
	 * @return the problemsSolved
	 */
	public int getProblemsSolved() {
		return problemsSolved;
	}

	/**
	 * @param problemsSolved the problemsSolved to set
	 */
	public void setProblemsSolved(final int problemsSolved) {
		this.problemsSolved = problemsSolved;
	}

	/**
	 * @return the problemsSolvedCount
	 */
	public int getProblemsSolvedCount() {
		return problemsSolvedCount;
	}

	/**
	 * @param problemsSolvedCount the problemsSolvedCount to set
	 */
	public void setProblemsSolvedCount(final int problemsSolvedCount) {
		this.problemsSolvedCount = problemsSolvedCount;
	}

	/**
	 * @return the repeatCustomers
	 */
	public int getRepeatCustomers() {
		return repeatCustomers;
	}

	/**
	 * @param repeatCustomers the repeatCustomers to set
	 */
	public void setRepeatCustomers(final int repeatCustomers) {
		this.repeatCustomers = repeatCustomers;
	}

	/**
	 * @return the repeatCustomersCount
	 */
	public int getRepeatCustomersCount() {
		return repeatCustomersCount;
	}

	/**
	 * @param repeatCustomersCount the repeatCustomersCount to set
	 */
	public void setRepeatCustomersCount(final int repeatCustomersCount) {
		this.repeatCustomersCount = repeatCustomersCount;
	}

	/**
	 * @return the satisfaction
	 */
	public float getSatisfaction() {
		return satisfaction;
	}

	/**
	 * @param satisfaction the satisfaction to set
	 */
	public void setSatisfaction(final float satisfaction) {
		this.satisfaction = satisfaction;
	}

	/**
	 * @return the satisfactionCount
	 */
	public int getSatisfactionCount() {
		return satisfactionCount;
	}

	/**
	 * @param satisfactionCount the satisfactionCount to set
	 */
	public void setSatisfactionCount(final int satisfactionCount) {
		this.satisfactionCount = satisfactionCount;
	}

	/**
	 * @return the formattedOverallSatisfaction
	 */
	public String getFormattedOverallSatisfaction() {
		return formattedOverallSatisfaction;
	}

	/**
	 * @param formattedOverallSatisfaction the formattedOverallSatisfaction to set
	 */
	public void setFormattedOverallSatisfaction(final String formattedOverallSatisfaction) {
		this.formattedOverallSatisfaction = formattedOverallSatisfaction;
	}
    
    
}
