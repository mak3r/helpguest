/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 *
 */
public class IPNDTO implements IsSerializable {
	
    public static final String COMPLETED = "Completed";
    public static final String PENDING = "Pending";

	private String verifySign;
	private String paymentStatus;
	private String invoice;
	private String itemName;
	private String itemNumber;
	private String payerEmail;
	private SessionDTO session;
	

	/**
	 * @return the session
	 */
	public SessionDTO getSession() {
		return session;
	}



	/**
	 * @param session the session to set
	 */
	public void setSession(SessionDTO session) {
		this.session = session;
	}



	/**
	 * @return the invoice
	 */
	public String getInvoice() {
		return invoice;
	}



	/**
	 * @param invoice the invoice to set
	 */
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}



	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}



	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	/**
	 * @return the itemNumber
	 */
	public String getItemNumber() {
		return itemNumber;
	}



	/**
	 * @param itemNumber the itemNumber to set
	 */
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}



	/**
	 * @return the payerEmail
	 */
	public String getPayerEmail() {
		return payerEmail;
	}



	/**
	 * @param payerEmail the payerEmail to set
	 */
	public void setPayerEmail(String payerEmail) {
		this.payerEmail = payerEmail;
	}



	/**
	 * @return the paymentStatus
	 */
	public String getPaymentStatus() {
		return paymentStatus;
	}



	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}



	/**
	 * @return the verifySign
	 */
	public String getVerifySign() {
		return verifySign;
	}



	/**
	 * @param verifySign the verifySign to set
	 */
	public void setVerifySign(String verifySign) {
		this.verifySign = verifySign;
	}



	public boolean isCompleted() {
        if(COMPLETED.equals(paymentStatus)) {
            return true;
        }
        return false;
    }
    
	public String toString() {
		return "[" + verifySign + ", " + 
			paymentStatus + ", " +
			invoice + ", " + 
			itemName + ", " + 
			itemNumber + ", " + 
			payerEmail + ", " +
			session + "]";
	}

}
