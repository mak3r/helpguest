/*
 * SessionAssistant.java
 *
 * Created on Sept 13, 2007, 8:25 PM
 *
 * Copyright 2007 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.marketplace.client.dto;

import com.google.gwt.i18n.client.NumberFormat;

public class SessionDTOAssistant {
    
    public static final float HELPGUEST_PERCENT = .30f;
    private SessionDTO sessionDTO = null;
    private float totalValue = 0.0f;
    
    private static final NumberFormat currencyFormat = 
    	NumberFormat.getFormat("\u00A4#,###.00", "USD");
    
    /** Creates a new instance of SessionAssistant */
    public SessionDTOAssistant(SessionDTO sessionDTO) {
        this.sessionDTO = sessionDTO;
        this.totalValue = calculateTotalValue();
    }

    private float calculateTotalValue() {
        float totalValue = 0.0f;
        if (sessionDTO.isClosed()) {
            RatesDTO ratesDTO = sessionDTO.getRates();

            int minutes = getDurationInMinutes();
            if (minutes <= ratesDTO.getMinTime()) {
                //minutes is less than or equal to the minimum time period
                totalValue = ratesDTO.getMinFee();
            } else if (minutes > ratesDTO.getPrefTime()) {
                //minutes is greater than the max amount of time
                totalValue = ratesDTO.getPrefTime() * ratesDTO.getMinRate();
            } else {
                //minutes is less then the max time but more than the min time
                totalValue = ratesDTO.getMinFee() + ((minutes-ratesDTO.getMinTime())*ratesDTO.getMinRate());
            }
                        
        }
        return totalValue;        
    }
    
    private long calculateDuration() {
        return sessionDTO.getDuration();
    }
    
    public float getTotalValue() {
        return totalValue;
    }
    
    public float getExpertAmount() {
        return totalValue - getHelpGuestAmount();
    }
    
    public float getHelpGuestAmount() {
        return totalValue * HELPGUEST_PERCENT;
    }
    
    public String calculateFormattedTotalValue() {
        return currencyFormat.format(totalValue);
    }
    
    public String getFormattedExpertAmount() {
        return currencyFormat.format(getExpertAmount());
    }
    
    public String getFormattedHelpGuestAmount() {
        return currencyFormat.format(getHelpGuestAmount());
    }
    
    public int getDurationInMinutes() {
        return (int)(Math.ceil(calculateDuration()/60000.0));
    }
}
