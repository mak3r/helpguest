/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 23, 2007
 */
package com.helpguest.marketplace.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 *
 */
public class ExpertStatsDTO implements IsSerializable {

	private int problemsHandled;
	private float satisfactionRating;
	private String avgTimeToResolution;
	private String numberResolved;
	private String numberRepeatGuests;
	
	/**
	 * @return the avgTimeToResolution
	 */
	public String getAvgTimeToResolution() {
		return this.avgTimeToResolution;
	}
	
	/**
	 * @param avgTimeToResolution the avgTimeToResolution to set
	 */
	public void setAvgTimeToResolution(final String avgTimeToResolution) {
		this.avgTimeToResolution = avgTimeToResolution;
	}
	
	/**
	 * @return the numberRepeatGuests
	 */
	public String getNumberRepeatGuests() {
		return this.numberRepeatGuests;
	}
	
	/**
	 * @param numberRepeatGuests the numberRepeatGuests to set
	 */
	public void setNumberRepeatGuests(final String numberRepeatGuests) {
		this.numberRepeatGuests = numberRepeatGuests;
	}
	
	/**
	 * @return the numberResolved
	 */
	public String getNumberResolved() {
		return this.numberResolved;
	}
	/**
	 * @param numberResolved the numberResolved to set
	 */
	public void setNumberResolved(final String numberResolved) {
		this.numberResolved = numberResolved;
	}
	
	/**
	 * @return the satisfactionRating
	 */
	public float getSatisfactionRating() {
		return this.satisfactionRating;
	}
	
	/**
	 * @param satisfactionRating the satisfactionRating to set
	 */
	public void setSatisfactionRating(final float satisfactionRating) {
		this.satisfactionRating = satisfactionRating;
	}
	
	/**
	 * @return the problemsHandled
	 */
	public int getProblemsHandled() {
		return problemsHandled;
	}
	
	/**
	 * @param problemsHandled the problemsHandled to set
	 */
	public void setProblemsHandled(final int problemsHandled) {
		this.problemsHandled = problemsHandled;
	}

}
