/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client.dto;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 * 
 */
public class ServiceOfferingDTO implements IsSerializable {

	
	private List<java.lang.String> expertiseList;

	/*
	 * @return
	 */
	public List<java.lang.String> getExpertise() {
		return expertiseList;
	}

	/*
	 * @param expertise a list of expertise
	 */
	public void setExpertise(final List<java.lang.String> expertise) {
		this.expertiseList = expertise;
	}


}
