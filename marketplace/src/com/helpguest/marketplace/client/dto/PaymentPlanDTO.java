/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 23, 2007
 */
package com.helpguest.marketplace.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 *
 */
public class PaymentPlanDTO implements IsSerializable {

	private String ratePerMinute;
	private String depositAmount;
	private String minimumFee;
	private int minTime;
	private int preferredTime;
	
	/**
	 * @return the depositAmount
	 */
	public String getDepositAmount() {
		return depositAmount;
	}
	/**
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(final String depositAmount) {
		this.depositAmount = depositAmount;
	}
	/**
	 * @return the minimumFee
	 */
	public String getMinimumFee() {
		return minimumFee;
	}
	/**
	 * @param minimumFee the minimumFee to set
	 */
	public void setMinimumFee(final String minimumFee) {
		this.minimumFee = minimumFee;
	}
	/**
	 * @return the ratePerMinute
	 */
	public String getRatePerMinute() {
		return ratePerMinute;
	}
	/**
	 * @param ratePerMinute the ratePerMinute to set
	 */
	public void setRatePerMinute(final String ratePerMinute) {
		this.ratePerMinute = ratePerMinute;
	}
	/**
	 * @return the minTime
	 */
	public int getMinTime() {
		return minTime;
	}
	/**
	 * @param minTime the minTime to set
	 */
	public void setMinTime(final int minTime) {
		this.minTime = minTime;
	}
	/**
	 * @return the preferredTime
	 */
	public int getPreferredTime() {
		return preferredTime;
	}
	/**
	 * @param preferredTime the preferredTime to set
	 */
	public void setPreferredTime(final int preferredTime) {
		this.preferredTime = preferredTime;
	}

	
}
