package com.helpguest.marketplace.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

public class BadgeDTO implements IsSerializable{

	private int expertId = 0;
	
	private int badgeRowId = 0;

	private int baseImageRowId = 0; 
	
	private String subText  = null;
	
	private String postURL = null;
	
	private String name = null;
	
	private float amount = 0;
	
	private String remoteURLContent = null;
	
	private int subtextGroupId = 0;
	
	
	public int getExpertId() {
		return expertId;
	}

	public void setExpertId(int expertId) {
		this.expertId = expertId;
	}

	public int getBadgeRowId() {
		return badgeRowId;
	}

	public void setBadgeRowId(int badgeRowId) {
		this.badgeRowId = badgeRowId;
	}

	public int getBaseImageRowId() {
		return baseImageRowId;
	}

	public void setBaseImageRowId(int baseImageRowId) {
		this.baseImageRowId = baseImageRowId;
	}

	public String getSubText() {
		return subText;
	}

	public void setSubText(String subText) {
		this.subText = subText;
	}

	public String getPostURL() {
		return postURL;
	}

	public void setPostURL(String postURL) {
		this.postURL = postURL;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public boolean equals(BadgeDTO anotherBadge) {
		if (anotherBadge.getBadgeRowId() == badgeRowId) {
			return true;
		}
		return false;
	}

	public String getRemoteURLContent() {
		return remoteURLContent;
	}

	public void setRemoteURLContent(String remoteURLContent) {
		this.remoteURLContent = remoteURLContent;
	}

	public int getSubtextGroupId() {
		return subtextGroupId;
	}

	public void setSubtextGroupId(int subtextGroupId) {
		this.subtextGroupId = subtextGroupId;
	}
	
}
