/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client.dto;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 *
 */
public class ExpertAccountDTO implements IsSerializable {

    private String uid;
    private int id;
    private String email = "";
    private String handle = "";
    private String paypalEmail = "";
    private String guestsWaiting;
    
    private List<java.lang.String> expertiseList;
    private String guestUid;
    private String endDate;
    private ServiceOfferingDTO serviceOffering = null;

    /**
    private ExpertDetailsDTO expertDetails = null;
    */
    
    
    
	/**
	 * @return the guestsWaiting
	 */
	public String getGuestsWaiting() {
		return guestsWaiting;
	}
	/**
	 * @param guestsWaiting the guestsWaiting to set
	 */
	public void setGuestsWaiting(String guestsWaiting) {
		this.guestsWaiting = guestsWaiting;
	}
	/**
	 * @return the serviceOffering
	 */
	public ServiceOfferingDTO getServiceOffering() {
		return serviceOffering;
	}
	/**
	 * @param serviceOffering the serviceOffering to set
	 */
	public void setServiceOffering(ServiceOfferingDTO serviceOffering) {
		this.serviceOffering = serviceOffering;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the expertiseList
	 */
	public List<java.lang.String> getExpertiseList() {
		return expertiseList;
	}
	/**
	 * @param expertiseList the expertiseList to set
	 */
	public void setExpertiseList(List<java.lang.String> expertiseList) {
		this.expertiseList = expertiseList;
	}
	
	/**
	 * @return the guestUid
	 */
	public String getGuestUid() {
		return guestUid;
	}
	/**
	 * @param guestUid the guestUid to set
	 */
	public void setGuestUid(String guestUid) {
		this.guestUid = guestUid;
	}
	/**
	 * @return the handle
	 */
	public String getHandle() {
		return handle;
	}
	/**
	 * @param handle the handle to set
	 */
	public void setHandle(String handle) {
		this.handle = handle;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the paypalEmail
	 */
	public String getPaypalEmail() {
		return paypalEmail;
	}
	/**
	 * @param paypalEmail the paypalEmail to set
	 */
	public void setPaypalEmail(String paypalEmail) {
		this.paypalEmail = paypalEmail;
	}
	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}
	/**
	 * @param uid the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}    
    
}
