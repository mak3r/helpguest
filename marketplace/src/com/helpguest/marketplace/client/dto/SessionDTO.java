/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 25, 2007
 */
package com.helpguest.marketplace.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 *
 */
public class SessionDTO implements IsSerializable {

    String sessionId;
    String itemName;
    String itemNumber;
    int expertId;
    String start = "";
    String end = "";
    String insertDate = "";
    String paymentRequested = "";
    String paymentSent = "";
    long sessionGroupId;
    ExpertAccountDTO expertAccountDTO;
    RatesDTO ratesDTO;
    boolean closed;
    long duration;

	/**
	 * @return the expertId
	 */
	public int getExpertId() {
		return expertId;
	}

	/**
	 * @param expertId the expertId to set
	 */
	public void setExpertId(final int expertId) {
		this.expertId = expertId;
	}
	
	/**
	 * @return the end
	 */
	public String getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(final String end) {
		this.end = end;
	}

	/**
	 * @return the insertDate
	 */
	public String getInsertDate() {
		return insertDate;
	}

	/**
	 * @param insertDate the insertDate to set
	 */
	public void setInsertDate(final String insertDate) {
		this.insertDate = insertDate;
	}

	/**
	 * @return the start
	 */
	public String getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(final String start) {
		this.start = start;
	}

	/**
	 * @return the paymentRequested
	 */
	public String getPaymentRequested() {
		return paymentRequested;
	}
	/**
	 * @param paymentRequested the paymentRequested to set
	 */
	public void setPaymentRequested(final String paymentRequested) {
		this.paymentRequested = paymentRequested;
	}
	/**
	 * @return the paymentSent
	 */
	public String getPaymentSent() {
		return paymentSent;
	}
	/**
	 * @param paymentSent the paymentSent to set
	 */
	public void setPaymentSent(final String paymentSent) {
		this.paymentSent = paymentSent;
	}
	/**
	 * @return the itemId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param itemId the itemId to set
	 */
	public void setSessionId(final String itemId) {
		this.sessionId = itemId;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(final String itemName) {
		this.itemName = itemName;
	}
	/**
	 * @return the itemNumber
	 */
	public String getItemNumber() {
		return itemNumber;
	}
	/**
	 * @param itemNumber the itemNumber to set
	 */
	public void setItemNumber(final String itemNumber) {
		this.itemNumber = itemNumber;
	}

	/**
	 * @return the sessionGroupId
	 */
	public long getSessionGroupId() {
		return sessionGroupId;
	}
	/**
	 * @param sessionGroupId the sessionGroupId to set
	 */
	public void setSessionGroupId(final long sessionGroupId) {
		this.sessionGroupId = sessionGroupId;
	}

	/**
	 * @return the expertAccountDTO
	 */
	public ExpertAccountDTO getExpertAccount() {
		return expertAccountDTO;
	}

	/**
	 * @param eaDTO the expertAccountDTO to set
	 */
	public void setExpertAccount(final ExpertAccountDTO eaDTO) {
		this.expertAccountDTO = eaDTO;
	}

	/**
	 * @return the ratesDTO
	 */
	public RatesDTO getRates() {
		return ratesDTO;
	}

	/**
	 * @param ratesDTO the ratesDTO to set
	 */
	public void setRates(RatesDTO ratesDTO) {
		this.ratesDTO = ratesDTO;
	}

	/**
	 * @return the closed
	 */
	public boolean isClosed() {
		return closed;
	}

	/**
	 * @param closed the closed to set
	 */
	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	/**
	 * @return the duration
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String toString() {
		return "[" + sessionId + ", " + 
			itemName + ", " + 
			itemNumber + ", " + 
			expertId + ", " +
			start + ", " +
			end + ", " + 
			insertDate + ", " +
			paymentRequested + ", " +
			paymentSent + ", " +
			sessionGroupId + "]";
	}
}
