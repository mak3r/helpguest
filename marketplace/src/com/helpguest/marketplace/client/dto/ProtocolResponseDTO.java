/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 15, 2007
 */
package com.helpguest.marketplace.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author mabrams
 *
 */
public class ProtocolResponseDTO implements IsSerializable {

	private int id;
	private String type;
	private int port;
	private String uid;
	private String inetAddress;
	private long responseTS;
	private long keyId;
	
	public static final String WEB_CHAT = "web_chat";
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(final int id) {
		this.id = id;
	}
	/**
	 * @return the inetAddress
	 */
	public String getInetAddress() {
		return inetAddress;
	}
	/**
	 * @param inetAddress the inetAddress to set
	 */
	public void setInetAddress(final String inetAddress) {
		this.inetAddress = inetAddress;
	}
	/**
	 * @return the keyId
	 */
	public long getKeyId() {
		return keyId;
	}
	/**
	 * @param keyId the keyId to set
	 */
	public void setKeyId(final long keyId) {
		this.keyId = keyId;
	}
	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(final int port) {
		this.port = port;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(final String type) {
		this.type = type;
	}
	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}
	/**
	 * @param uid the uid to set
	 */
	public void setUid(final String uid) {
		this.uid = uid;
	}
	/**
	 * @return the responseTS
	 */
	public long getResponseTS() {
		return responseTS;
	}
	
	public void setResponseTS(final long timestamp) {
		this.responseTS = timestamp;
	}
	
	
	
}
