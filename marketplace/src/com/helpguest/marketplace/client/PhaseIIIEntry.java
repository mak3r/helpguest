/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 22, 2007
 */
package com.helpguest.marketplace.client;

import java.util.Map;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.GWT.UncaughtExceptionHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.bluegray.ui.FindExpert;
import com.helpguest.marketplace.client.bluegray.ui.LoginHeader;
import com.helpguest.marketplace.client.bluegray.ui.SearchWidget;
import com.helpguest.marketplace.client.bluegray.ui.TabbedContentPanel;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.BodyPanel;
import com.helpguest.marketplace.client.ui.RateCalculation;
import com.helpguest.marketplace.client.ui.VisualCuesImageBundle;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;
import com.helpguest.marketplace.client.widgets.QuickTicketSearch;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class PhaseIIIEntry implements EntryPoint {

	final BodyPanel body = new BodyPanel();
	private static MarketPlaceConstants marketPlaceConstants = 
		(MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private final static boolean DEBUG = !marketPlaceConstants.hideDebug();
	
	static {
		if (DEBUG) {
			RootPanel.get("debug-content").add(new DebugPanel(true));
			GWT.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
				public void onUncaughtException(Throwable e) {
					// TODO Auto-generated method stub
					DebugPanel.setText(PhaseIIIEntry.class, e);
				}
			});
		}
	}
	/**
	 * Body content
	 */
//	final AboutPanel aboutPanel = new AboutPanel();
	final HorizontalPanel signInPanel = new HorizontalPanel();
	final RateCalculation rateCalc = new RateCalculation();
	/**
	 * The loginHeader holds our Authenticator
	 */
	final LoginHeader loginHeader = new LoginHeader();

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		try {
			final String search = "QUICKSEARCH"; 
	        final String searchbox = "ENTER YOUR PROBLEM HERE";
	        //Window.alert("PhaseIIIEntry 1: Before SearchWidget.newInstance()");
			final SearchWidget searchWidget = SearchWidget.newInstance(search,searchbox, FindExpert.getInstance(), SearchWidget.one);
			//Window.alert("PhaseIIIEntry 2: Before new QuickTicketSearch()");
			final QuickTicketSearch quickTicketSearch = new QuickTicketSearch();
			//final QuickLogin quickLogin = new QuickLogin(ValidatedTabbedContentPanel.MY_ACCOUNT_TOKEN);
			//Window.alert("PhaseIIIEntry 3: Before new TabbedContentPanel()");
			final TabbedContentPanel tabbedContentPanel = new TabbedContentPanel(loginHeader.getAuthenticator());
			//Window.alert("PhaseIIIEntry 4: Before HistoryCommander.getInstance().register");
			HistoryCommander.getInstance().register(tabbedContentPanel);
			
			loginHeader.getAuthenticator().addAuthenticationListener(tabbedContentPanel);
			
			VisualCuesImageBundle visualCues = 
			    (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);	  
			RootPanel.get("banner-left-side").add(visualCues.wideLogo39Height().createImage());
			RootPanel.get("quicksearch-group").add(searchWidget);
			RootPanel.get("quick-ticket-group").add(quickTicketSearch);
			RootPanel.get("quick-login-group").add(loginHeader);
			//RootPanel.get("quick-login-group").add(quickLogin);
			RootPanel.get("tabbed-content").add(tabbedContentPanel);
			RootPanel.get().setStyleName("body_style");
			
			//Initialize the app with a valid value
			String entryToken = History.getToken();
			if (HistoryCommander.getInstance().canHandle(entryToken)) {
				DebugPanel.setText(PhaseIIIEntry.class, "HistoryCommander can handle token: " + entryToken);
				HistoryCommander.init(entryToken);
			} else {
				DebugPanel.setText(PhaseIIIEntry.class, "Cannot handle token: " + entryToken + " using tab panel default.");
				HistoryCommander.init(tabbedContentPanel.getDefaultToken());
			}
	
			//This is required in order for the browser to 'back' out properly
			//Its important to setup (register) this token after our initial token has been retrieved.
			initHomeCommand();
		} catch (Exception ex) {
			DebugPanel.setText(PhaseIIIEntry.class, ex);
		}
	}

	/**
	 * We register an empty base token here with an onEntry()
	 * method handler that will pop the 'home history' when
	 * 
	 * This is necessary because we never initialize the site
	 * to an empty history value.  So the only time we will
	 * get to this page is 
	 * when leaving the application via the back button
	 * on the browser.  The final 'back' action before going to the 
	 * referring page will take us here at which point
	 * we must force a History.back() command.
	 */
	private String initHomeCommand() {
		CommandToken popHistoryCommand = new CommandToken() {

			public String getBaseToken() {
				return "";
			}

			public void onEntry(Map<String, String> kvMap) {
				History.back();
			}

			public void onExit() {
				//no-op
			}
			
		};
		HistoryCommander.getInstance().register(popHistoryCommand);

		return popHistoryCommand.getBaseToken();
	}
	
	public void setWidget(Widget result) {
		body.setWidget(result);
	}


}
