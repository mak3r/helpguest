/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.helpguest.marketplace.client.dto.IPNDTO;

/**
 * @author mabrams
 *
 */
public interface IPNService extends RemoteService {
	
	public IPNDTO getCompletionDetails(final String sessionId);
	
}
