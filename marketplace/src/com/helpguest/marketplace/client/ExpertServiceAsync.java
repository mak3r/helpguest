/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.helpguest.marketplace.client.dto.ExpertAccountDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public interface ExpertServiceAsync {

	public void getExpert(final String expertUid, final AsyncCallback callback);
	
	public void makePayments(final ExpertAccountDTO eaDTO, final AsyncCallback callback);
	
	public void modifyExpertise(
			final ExpertDTO dto,
			final String updateList,
			final String mode, 
			final AsyncCallback callback);
	
	/**
	 * 
	 * @param email The new email addres String
	 * @param pass the new password String
	 * @param handle the new handle String
	 * @param offering the new expertise String
	 * @param zip the new zipcode String
	 * @param callback the AsyncCallback
	 */
	public void createAccount(
			final String email, 
    		final String pass,
    		final String handle, 
    		final String offering, 
    		final String zip,
    		final AsyncCallback callback);
	

	/**
	 * prahalad adding 7/23/08 alternate createAccount method to handle ArrayList of catOffgTags
	 * @param email The new email addres String
	 * @param pass the new password String
	 * @param handle the new handle String
	 * @param zip the new zipcode String
	 * @param referralSource the String value of the referral source
	 * @param callback the AsyncCallback
	 */
	
	public void createAccount(
			final String email, 
    		final String pass,
    		final String handle, 
    		final List<java.lang.String> catOffgTagList, 
    		final String zip,
    		final String referralSource,
    		final AsyncCallback callback);
	
	public void getEvaluation(
			final ExpertDTO expert, 
			final AsyncCallback callback);

	public void updateAccount(
			final ExpertDTO expert,
			final AsyncCallback callback);
	
	/**
	 * prahalad adding 07/29/08 alternate createAccount method to handle ArrayList of catOffgTags
	 * @param expertdto expertdto
	 * @param newPaswd the new password String
	 * @param callback the AsyncCallback
	 */
	
	public void updatePaswd(final ExpertDTO expert, 
			final String newPaswd,
			final AsyncCallback callback);
	
	/**
	 * prahalad adding 07/29/08 alternate createAccount method to handle ArrayList of catOffgTags
	 * @param currResetCode currResetCode
	 * @param expertEmail the expertEmail String
	 * @param callback the AsyncCallback
	 */
	public void expireResetCode(final String currResetCode, final String expertEmail,
			final AsyncCallback callback);
	
	/**
	 * prahalad adding 07/30/08 alternate createAccount method to handle ArrayList of catOffgTags
	 * @param expertEmail expertEmail
	 * @param callback the AsyncCallback
	 */
	public void getResetCode(final String expertEmail, final AsyncCallback callback);
	
	/**
	 * prahalad adding 02/20/09 to retrieve survey
	 * @param uid uid
	 * @param sessionId the sessionId
	 * @param callback the AsyncCallback
	 */
	public void getSurveyFromDB(final String uid, final String sessionId, final AsyncCallback callback);
	/**
	 * prahalad adding 08/08/08 alternate createAccount method to handle ArrayList of catOffgTags
	 * @param ExpertDTO ExpertDTO
	 * @param avatarFile the avatarFile String
	 * @param callback the AsyncCallback
	 */
	public void uploadAvatarFile(final String expert, final String avatarFile, final AsyncCallback callback);
	
	/**
	 * prahalad adding 11/11/08 updatePaypalEmail
	 * @param uid ExpertDTO
	 * @param sessionId the sessionId
	 * * @param strSatisfaction the Satisfaction string
	 * @param strProblemSolved the ProblemSolved String
	 * * @param strRecommend the Recommend String
	 * @param strRepeatCustomer the RepeatCustomer String
	 * * @param strProfessionalManner the ProfessionalManner string 
	 * @param surveyAction the surveyAction String
	 * @param callback the AsyncCallback
	 */
	public void submitSurveyToDB(final String uid, final String sessionId, 
			final String strSatisfaction, final String strProblemSolved, final String strRecommend, 
			final String strRepeatCustomer, final String strProfessionalManner, 
			final String surveyAction, final AsyncCallback callback);
	
}
