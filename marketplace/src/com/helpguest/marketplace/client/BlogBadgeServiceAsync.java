/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.helpguest.marketplace.client.dto.BadgeDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public interface BlogBadgeServiceAsync {	
	
	public void createBadge(final ExpertDTO expertDTO, final BadgeDTO badgeDTO, AsyncCallback<BadgeDTO> callback);

	public void updateBadge(final ExpertDTO expertDTO, final BadgeDTO badgeDTO, AsyncCallback<BadgeDTO> callback);
	
	public void deleteBadge(final ExpertDTO expertDTO, final BadgeDTO badgeDTO, AsyncCallback<BadgeDTO> callback);
	
	public void getAllBadges(final ExpertDTO expertDTO, AsyncCallback<List<BadgeDTO>> callback);
	
}
