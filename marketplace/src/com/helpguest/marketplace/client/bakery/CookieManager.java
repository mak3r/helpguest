/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 18, 2007
 */
package com.helpguest.marketplace.client.bakery;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;

/**
 * @author mabrams
 *
 */
public class CookieManager {

	public static final String EMAIL_COOKIE = "EMAIL";
	public static final String PASSWORD_COOKIE = "PASSWORD";
	private static final MarketPlaceConstants myConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static final String myfqdn = myConstants.fullyQualifiedDomainName();
	public static final String COOKIE_DOMAIN = myfqdn;
	
	//Expires in one day
	private static final long EXPIRES = 86400000L;
	
	public static void bake(final String key, final String value, boolean secure) {
		if (secure) {
			Cookies.setCookie(
					key, value, expires(), COOKIE_DOMAIN, null, true);
		} else {
			Cookies.setCookie(key, value, expires());
		}
	}
	
	public static void eat(final String key) {
		Cookies.removeCookie(key);
	}

	public static String ponder(final String key) {
		return Cookies.getCookie(key);
	}
	
	private static Date expires() {
		return new Date(System.currentTimeMillis() + EXPIRES);
	}
}
