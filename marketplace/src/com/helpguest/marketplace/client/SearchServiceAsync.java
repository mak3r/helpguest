/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 23, 2007
 */
package com.helpguest.marketplace.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.helpguest.gwt.search.OfferingsConstraints;
import com.helpguest.gwt.search.QueryConstraints;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.TagsConstraints;

import java.util.List;

/**
 * @author mabrams
 *
 */
public interface SearchServiceAsync {
	public void findExperts(final String query, final AsyncCallback callback);

	public void getExpertByUid(final String expertUid, final AsyncCallback callback);

	public void getExpertByHandle(final String handle, final AsyncCallback callback);
	
	//prahalad 6/26/08 adding this method that will return offerings for a category
	public void getOfferings(final CategoryConstraints cc, final AsyncCallback callback);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a find expert textbox search
	public void getExpertIdList(final QueryConstraints qc, final AsyncCallback callback);
	
	//prahalad 07/09/08 adding this method that will return expertid list to drill down
	//from existing expert list and textbox search
	public void getExpertIdList(final QueryConstraints qc, List parentExpertIdList, final AsyncCallback callback);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a category
	public void getExpertIdList(final CategoryConstraints cc, final AsyncCallback callback);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a category and drilling down 
	//into a known list of experts
	public void getExpertIdList(CategoryConstraints cc, List parentExpertIdList, final AsyncCallback callback);
	
	//prahalad 10/30/08 adding this method that will return expertid list for a category and drilling down 
	//into a known list of experts within a query search for display under category tabs
	public void getExpertIdList(CategoryConstraints cc, QueryConstraints qc, List parentExpertIdList, final AsyncCallback callback);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a offering
	public void getExpertIdList(final OfferingsConstraints oc, final AsyncCallback callback);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a offering and drilling down 
	//into a known list of experts
	public void getExpertIdList(OfferingsConstraints oc, List parentExpertIdList, final AsyncCallback callback);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a tag
	public void getExpertIdList(TagsConstraints tc, final AsyncCallback callback);
	
	//prahalad 6/30/08 adding this method that will return expertid list for a tag and drilling down 
	//into a known list of experts
	public void getExpertIdList(TagsConstraints tc, List parentExpertIdList, final AsyncCallback callback);


}
