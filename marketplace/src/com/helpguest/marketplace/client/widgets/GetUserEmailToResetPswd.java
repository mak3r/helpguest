package com.helpguest.marketplace.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.protocol.VerifyEmailOutcome;
import com.helpguest.marketplace.client.AuthenticationService;
import com.helpguest.marketplace.client.AuthenticationServiceAsync;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.util.DebugPanel;

public class GetUserEmailToResetPswd extends SimplePanel {

	public static final String TOKEN = "forgot_password";
	public static final String FORGOT_PASSWORD_VALUE = "true";
	private Label topLabel = new Label("REQUEST PASSWORD RESET");
	private Label topLabelDescription = new Label("A link to reset your password will be sent to the address entered below.");
	private Label emailAddressLbl = new Label("Email address used to create your HelpGuest account:");
	private TextBox emailTextBox;
	private Button submitEmailBtn;
	private Button returnToSignUpButton = new Button("Return to Sign-up");
	private Label error = new Label();
	private VerifyEmailOutcome emailOutcome = null;

	private ResultContainer resultContainer;

	// Create the expert service proxy
	final ExpertServiceAsync expertService = (ExpertServiceAsync) GWT
			.create(ExpertService.class);

	// Create the authentication service proxy
	final AuthenticationServiceAsync authenticationService = (AuthenticationServiceAsync) GWT
			.create(AuthenticationService.class);

	public GetUserEmailToResetPswd(final ResultContainer resultContainer) {
		this.resultContainer = resultContainer;

		// Initialize the AuthenticationService
		ServiceDefTarget endpoint = (ServiceDefTarget) this.authenticationService;
		String moduleRelativeURL = GWT.getModuleBaseURL()
				+ "AuthenticationService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);

		// Specify the url where the expert service is running
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String moduleRelativeURLExpertService = GWT.getModuleBaseURL()
				+ "ExpertService";
		expertEndpoint.setServiceEntryPoint(moduleRelativeURLExpertService);

		init();
	}

	private void init() {
		emailTextBox = new TextBox();
		submitEmailBtn = new Button("Submit");

		submitEmailBtn.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				String email = emailTextBox.getText().trim();
				if (email != null && !email.equals("")) {

					authenticationService.verifyEmail(
							email,
							verifyEmailCallback);
				}

			}
		});
		
		returnToSignUpButton.addClickListener(new ClickListener() {

			public void onClick(Widget arg0) {
				resultContainer.reset();
			}
				
		});
		//Adding Request Password Label
		/*
		HorizontalPanel labelPanel = new HorizontalPanel();
		labelPanel.add(topLabel);
		labelPanel.add(topLabelDescription);
		*/
		
		HorizontalPanel emailPanel = new HorizontalPanel();
		emailPanel.add(error);
		emailPanel.add(emailAddressLbl);
		emailPanel.add(emailTextBox);

		HorizontalPanel hp = new HorizontalPanel();
		hp.add(submitEmailBtn);
		hp.add(returnToSignUpButton);
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(topLabel);
		vp.add(topLabelDescription);
		vp.add(emailPanel);
		vp.add(hp);
		
		setWidget(vp);
//set style name
		 topLabel.setStyleName("request_password");
		 topLabelDescription.setStyleName("get_user_email_to_reset_password_top_label_description");
		 vp.setStyleName("become_expert_request_password");
		 emailPanel.addStyleName("email_Address");
		 hp.setStyleName("buttons_submit");
	}
	

	AsyncCallback verifyEmailCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("GetUserEmailToResetPswdPanel verifyEmailCallback: "
//								+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("GetUserEmailToResetPswdPanel verifyEmailCallback [throwable]: "
//								+ th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			emailOutcome = (VerifyEmailOutcome) result;
			// FIXME this should be using the equals method on emailOutcome, not
			// the magic string.
			if (emailOutcome.getMessage().equals(
					"Your email has been verified. Please sign in.")) {
				expertService.getResetCode(emailTextBox.getText().trim(),
						getResetCodeCallback);
			} else {
				error.setText(emailOutcome.toString());
			}
		}

	};

	AsyncCallback getResetCodeCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				DebugPanel.setText("GetUserEmailToResetPswdPanel", "getResetCodeCallback: "
								+ sBuf.toString());
			} catch (Throwable th) {
				DebugPanel.setText("GetUserEmailToResetPswdPanel", "getResetCodeCallback [throwable]: "
								+ th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			HelpGuestInfoOK info = new HelpGuestInfoOK(
					resultContainer, 
					"Request Password Reset",
					"",
					"A password 'reset code' has been emailed to the address you entered.");
			resultContainer.setWidget(info);
		}

	};

}
