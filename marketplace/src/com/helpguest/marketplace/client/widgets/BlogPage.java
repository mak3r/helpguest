/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author mabrams
 *
 */
public class BlogPage extends SimplePanel {

	public BlogPage(final String width) {
		super();
		StringBuffer iframe =
			new StringBuffer("<iframe " +
					"src=\"http://blog.helpguest.com\" " +
					"width=\"" + width + "\" " +
					"height=\"2000\" " +
					"frameborder=\"0\" " +
					"scrolling=\"yes\" " +
					"style=\"border:0;\">");
		iframe.append("</iframe>");
		setWidget(new HTML(iframe.toString()));
	}
}
