package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author Ying
 */

public class HistoryContent extends SimplePanel {

	public HistoryContent(){
		HTML content = new HTML("<p>"+
				"HelpGuest is a starting-up Company"
				+"</p>");
		setWidget(content);
		setStyleName("history_content");
	}
}
