package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ui.ResultContainer;

public class HelpGuestInfoOK extends SimplePanel {

	private ResultContainer resultContainer;
	private SimplePanel content = new SimplePanel();
	private Label infoLabel = new Label();
	private Label infoSubTextLabel = new Label();

	public HelpGuestInfoOK(final ResultContainer resultContainer) {
		this(resultContainer, "HelpGuest info ... message missing");		
	}
	
	
	public HelpGuestInfoOK(final ResultContainer resultContainer, final String message) {
		this.resultContainer = resultContainer;
		content.setWidget(new Label(message));
		init();
	}
	
	public HelpGuestInfoOK(final ResultContainer resultContainer, final String info,
			final String infoSubText, final String detail) {
		this.resultContainer = resultContainer;
		content.setWidget(new Label(detail));
		infoLabel.setText(info);
		infoSubTextLabel.setText(infoSubText);
		init();
	}
	
	public HelpGuestInfoOK(final ResultContainer resultContainer, final Widget info) {
		this.resultContainer = resultContainer;
		content.setWidget(info);
		init();
	}
	
	public void resetInfo(final Widget info) {
		content.setWidget(info);
	}
	
	public HelpGuestInfoOK(final ResultContainer resultContainer, final String info,
			final String infoSubText, final Widget detail) {
		this.resultContainer = resultContainer;
		content.setWidget(detail);
		infoLabel.setText(info);
		infoSubTextLabel.setText(infoSubText);
		init();
	}
	
	
	
	private void init() {
		Button okButton = new Button("OK");
		okButton.addClickListener(new ClickListener() {

			public void onClick(Widget arg0) {
				resultContainer.reset();
			}
			
		});

		VerticalPanel vp = new VerticalPanel();
		vp.add(infoLabel);
		vp.add(infoSubTextLabel);
		infoLabel.addStyleName("request_password");
		infoSubTextLabel.addStyleName("get_user_email_to_reset_password_top_label_description");
		vp.add(content);
		content.setStyleName("content_info");
		vp.add(okButton);
		okButton.addStyleName("buttons_submit");
		vp.setStyleName("helpguest_info_ok_vertical_panel");
		setWidget(vp);
		setStyleName("helpguest_info_ok");
	}
}
