/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 21, 2007
 */
package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.marketplace.client.ui.TwoTonePanel;

/**
 * @author mabrams
 *
 */
public class ComingSoonPanel extends SimplePanel {

	private String title;
	
	public ComingSoonPanel(final String title) {
		super();
		this.title = title;
		init();
	}
	
	private void init() {
		Label verbage = new Label("This feature is coming soon.");
		verbage.setStyleName("hg-instructional-verbage");
		
		PaddedPanel paddedVerbage = new PaddedPanel(verbage, 20);
		paddedVerbage.setCellHorizontalAlignment(
				verbage, PaddedPanel.ALIGN_CENTER);
		paddedVerbage.setWidth("300px");
		
		TwoTonePanel ttPanel = new TwoTonePanel("300px");
		ttPanel.setHeading(new Label(title));
		ttPanel.setBody(paddedVerbage);
		
		setWidget(ttPanel);
	}
}
