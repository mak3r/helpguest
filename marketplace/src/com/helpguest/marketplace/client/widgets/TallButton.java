/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 6, 2007
 */
package com.helpguest.marketplace.client.widgets;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;

/**
 * @author mabrams
 *
 */
public class TallButton extends ChangingTextButton {

	
	public TallButton() {
		super();
	}
	
	public TallButton(final String enabledText, final String disabledText) {
		super(enabledText, disabledText);
		setStyleName("hg-TallButton");
	}

	
	protected FocusPanel getButton(
			final String text,
			final String textColor,
			final String buttonColor) {
		RoundedPanel rpButton = new RoundedPanel(RoundedPanel.ALL, 5);
		
		StringBuffer innerDiv = new StringBuffer(
				"<div style=\"" +
				"background-color:" + buttonColor + ";" +
				"border:thick solid " + buttonColor + ";" + 
				"text-align:center;" +
				"\">");
		
		String[] words = text.split("\\s");
		for (int i=0; i<words.length; i++) {
			HTML label = new HTML(getHTMLWord(words[i], textColor));
			innerDiv.append(label);
		}		
		innerDiv.append("</div>");
		HTML inner = new HTML(innerDiv.toString());
				
		rpButton.add(inner);
		rpButton.setCornerColor(buttonColor);
		
		FocusPanel tallButton = new FocusPanel(rpButton);
		return tallButton;
	}
	

}
