package com.helpguest.marketplace.client.widgets;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.protocol.VerifyEmailOutcome;
import com.helpguest.marketplace.client.AuthenticationService;
import com.helpguest.marketplace.client.AuthenticationServiceAsync;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.util.DebugPanel;

public class GetNewPswdToResetPswdPanel extends SimplePanel implements CommandToken {

	public final static String TOKEN = "reset_password";
	public final static String RESET_CODE_TOKEN = "reset_code";
	private Label topLabel = new Label("RESET PASSWORD");
	private Label topLabelDescription = new Label("All fields required.");
	private VerticalPanel errors = new VerticalPanel();
	private Label emailAddressLbl = new Label("Email address:");
	private TextBox emailTextBox;
	private Label paswdLbl = new Label("Password:");
	private PasswordTextBox paswdTextBox;
	private Label conPaswdLbl = new Label("Confirm Password:");
	private PasswordTextBox conPaswdTextBox;
	private Label resetCodeLbl = new Label("Reset Code:");
	private TextBox resetCodeTextBox;
	private Button resetBtn;
	private Button cancelButton = new Button("Cancel");
	
	private ResultContainer resultContainer;
	
	private ExpertDTO expertDTO;

	private VerifyEmailOutcome emailOutcome = null;

	private Label required = new Label("All fields are required");
	private static final Label passwordErrorLength = new Label(
			"Password must be between 6 and 50 characters.");
	private static final Label passwordErrorContent = new Label(
			"Password may contain letters, numbers, and (*$&#!_-^+).");
	private static final Label confirmPasswordError = new Label(
			"Password and confirm password must match.");
	private static final Label emailError = new Label(
			"Email address is invalid.");
	private static final Label verifyEmailError = new Label();

	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	// Create the authentication service proxy
	final AuthenticationServiceAsync authenticationService = 
		(AuthenticationServiceAsync) GWT.create(AuthenticationService.class);

	public GetNewPswdToResetPswdPanel(ResultContainer resultContainer) {

		this.resultContainer = resultContainer;
		
		// Initialize the AuthenticationService
		ServiceDefTarget endpoint = (ServiceDefTarget) this.authenticationService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "AuthenticationService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		//Specify the url where the expert service is running
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String moduleRelativeURLExpertService = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(moduleRelativeURLExpertService);

		init();
	}

	private void init() {
		emailTextBox = new TextBox();
		paswdTextBox = new PasswordTextBox();
		conPaswdTextBox = new PasswordTextBox();
		resetCodeTextBox = new TextBox();
		resetBtn = new Button("Reset");

		HorizontalPanel emailPanel = new HorizontalPanel();
		emailPanel.add(emailAddressLbl);
		emailPanel.add(emailTextBox);

		HorizontalPanel paswdPanel = new HorizontalPanel();
		paswdPanel.add(paswdLbl);
		paswdPanel.add(paswdTextBox);

		HorizontalPanel conPaswdPanel = new HorizontalPanel();
		conPaswdPanel.add(conPaswdLbl);
		conPaswdPanel.add(conPaswdTextBox);

		HorizontalPanel resetPanel = new HorizontalPanel();
		resetPanel.add(resetCodeLbl);
		resetPanel.add(resetCodeTextBox);

		resetBtn.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				String email = emailTextBox.getText().trim();
				String paswd = paswdTextBox.getText().trim();
				String conPaswd = conPaswdTextBox.getText().trim();
				String resetCode = resetCodeTextBox.getText().trim();
				if (email != null && !email.equals("")) {

					authenticationService.verifyEmail(email,
							verifyEmailCallback);
				}

			}
		});
		
		cancelButton.addClickListener(new ClickListener() {

			public void onClick(Widget arg0) {
				resultContainer.reset();
			}
			
		});
		
		HorizontalPanel buttonPanel = new HorizontalPanel();
		HorizontalPanel hp = new HorizontalPanel();
		buttonPanel.add(resetBtn);
		buttonPanel.add(cancelButton);

		VerticalPanel vp = new VerticalPanel();
		vp.add(topLabel);
		vp.add(topLabelDescription);
		vp.add(errors);
		vp.add(emailPanel);
		vp.add(paswdPanel);
		vp.add(conPaswdPanel);
		vp.add(resetPanel);
		vp.add(buttonPanel);
		setWidget(vp);

		//set style name
		 topLabel.setStyleName("reset_password");
		 topLabelDescription.setStyleName("all_fields_required_top_label_description");
		 vp.setStyleName("become_expert_request_password");
		 paswdPanel.setStyleName("reset_password_2");
		 conPaswdPanel.setStyleName("reset_password_1");		 
		 emailPanel.setStyleName("email_Address_1");
		 resetPanel.setStyleName("reset_password_3");
		 buttonPanel.addStyleName("buttons_submit");
		 
	}// end init

	private boolean contentIsValidated() {
		errors.clear();
		return generalCheck() & emailCheck() & passwordCheck();
	}

	private boolean generalCheck() {
		if ("".equals(paswdTextBox.getText().trim())
				|| "".equals(conPaswdTextBox.getText().trim())
				|| "".equals(emailTextBox.getText().trim())
				|| "".equals(resetCodeTextBox.getText().trim())) {
			errors.add(required);
			return false;
		}
		return true;
	}

	private boolean passwordCheck() {
		String passwordValue = paswdTextBox.getText().trim();
		String confirmPasswordValue = conPaswdTextBox.getText().trim();
		boolean ok = true;
		if (!passwordValue.matches("[\\w\\*\\&\\#\\!\\^\\+\\$-]*")) {
			errors.add(passwordErrorContent);
			ok = false;
		}
		if (passwordValue.length() < 6 || passwordValue.length() > 50) {
			errors.add(passwordErrorLength);
			ok = false;
		}
		if (passwordValue != null
				&& !passwordValue.equals(confirmPasswordValue)) {
			errors.add(confirmPasswordError);
			ok = false;
		}
		return ok;
	}

	private boolean emailCheck() {
		String emailValue = emailTextBox.getText().trim();
		if (!emailValue.matches("[\\w\\.\\+%-]+@[\\w\\.-]+\\.[\\w]{2,4}")) {
			errors.add(emailError);
			return false;
		}
		return true;
	}

	AsyncCallback verifyEmailCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				Window
						.alert("GetUserEmailToResetPswdPanel verifyEmailCallback: "
								+ sBuf.toString());
			} catch (Throwable th) {
				Window
						.alert("GetUserEmailToResetPswdPanel verifyEmailCallback [throwable]: "
								+ th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			emailOutcome = (VerifyEmailOutcome) result;
			if (emailOutcome.getMessage().equals(
					"Your email has been verified. Please sign in.")) {
				if (contentIsValidated()) {
					
					//ExpertAccount ea = ExpertAccount.getExpertAccount(emailTextBox.getText().trim());
					expertService.getExpert(emailTextBox.getText().trim(), expertCallback);
					
					
				}
			} else {
				verifyEmailError.setText(emailOutcome.getMessage());
				errors.add(verifyEmailError);
			}
		}

	};
	
	//prahalad adding expertCallback 07/29/08
	AsyncCallback expertCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("GetNewPswdToResetPswdPanel expertCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("GetNewPswdToResetPswdPanel expertCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			expertService.updatePaswd(expertDTO, paswdTextBox.getText().trim(), 
					updatePaswdCallback);
		}		
	};
	
	//prahalad adding expertCallback 07/29/08
	AsyncCallback updatePaswdCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("GetNewPswdToResetPswdPanel updatePaswdCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("GetNewPswdToResetPswdPanel updatePaswdCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			HelpGuestInfoOK info = new HelpGuestInfoOK(resultContainer, "Your password has been reset.");
			resultContainer.setWidget(info);
			expertService.expireResetCode(resetCodeTextBox.getText().trim(), 
					emailTextBox.getText().trim(), expireResetCodeCallback);
		}		
	};

	//FIXME this seems unnecessary.  Is it a security risk to leave the code out there?
	//prahalad adding expertCallback 07/29/08
	AsyncCallback expireResetCodeCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				DebugPanel.setText("GetNewPswdToResetPswdPanel", "expireResetCodeCallback: " +  sBuf.toString());
			} catch (Throwable th) {
				DebugPanel.setText("GetNewPswdToResetPswdPanel", "expireResetCodeCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			//no-op
		}		
	};

	public String getBaseToken() {
		return TOKEN;
	}

	public void onEntry(Map<String, String> kvMap) {
		if (kvMap != null) {
			if (kvMap.containsKey(RESET_CODE_TOKEN)) {
				resetCodeTextBox.setText((String)kvMap.get(RESET_CODE_TOKEN));
			}
		}
	}

	public void onExit() {
		// TODO Auto-generated method stub
		
	}
}//end class
