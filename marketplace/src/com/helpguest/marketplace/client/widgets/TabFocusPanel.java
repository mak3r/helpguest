package com.helpguest.marketplace.client.widgets;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.observer.SourcesTabClicks;
import com.helpguest.marketplace.client.observer.TabClickListener;
import com.helpguest.marketplace.client.observer.TabToken;

public class TabFocusPanel extends FocusPanel implements
		TabToken, SourcesTabClicks, ClickListener {

	private String baseToken;
	private List tabClickListenerList = new ArrayList();
	
	
	public TabFocusPanel(final Widget widget, final String token) {
		super(widget);
		baseToken = token;
		addClickListener(this);
	}
	
	public String getTabToken() {
		return baseToken;
	}

	public void addTabClickListener(TabClickListener tabClickListener) {
		tabClickListenerList.add(tabClickListener);
	}

	public void removeTabClickListener(TabClickListener tabClickListener) {
		tabClickListenerList.remove(tabClickListener);
	}

	/**
	 * Listen to any clicks sourced by parent FocusPanel.
	 * Divert those clicks to a more specific click listener
	 * which takes a TabFocusPanel as it's argument.
	 */
	public void onClick(Widget arg0) {
		Iterator it = tabClickListenerList.iterator();
		while (it.hasNext()) {
			((TabClickListener)it.next()).onClick(this);
		}
	}

}
