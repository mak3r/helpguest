/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 25, 2007
 */
package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ui.HorizontalSpacer;
import com.helpguest.marketplace.client.ui.VerticalSpacer;

/**
 * @author mabrams
 *
 */
public class PaddedPanel extends DockPanel {

	/**
	 * Create a padded panel with a default padding
	 * @param pad default padding
	 */
	public PaddedPanel(int pad) {
		add(new HorizontalSpacer(pad), DockPanel.NORTH);
		add(new VerticalSpacer(pad), DockPanel.WEST);
		add(new HorizontalSpacer(pad), DockPanel.SOUTH);
		add(new VerticalSpacer(pad), DockPanel.EAST);
	}

	/**
	 * 
	 * @param w Widget
	 * @param pad Equal padding on all sides
	 */
	public PaddedPanel(final Widget w, final int pad) {
		add(new HorizontalSpacer(pad), DockPanel.NORTH);
		add(new VerticalSpacer(pad), DockPanel.WEST);
		add(new HorizontalSpacer(pad), DockPanel.SOUTH);
		add(new VerticalSpacer(pad), DockPanel.EAST);
		add(w, DockPanel.CENTER);
	}

	/**
	 * 
	 * @param hPad the horizontal pads - NORTH and SOUTH
	 * @param vPad the vertical pads - EAST and WEST
	 */
	public PaddedPanel(final int hPad, final int vPad) {
		add(new HorizontalSpacer(hPad), DockPanel.NORTH);
		add(new VerticalSpacer(vPad), DockPanel.WEST);
		add(new HorizontalSpacer(hPad), DockPanel.SOUTH);
		add(new VerticalSpacer(vPad), DockPanel.EAST);
	}
	
	/**
	 * 
	 * @param w the widget to pad
	 * @param hPad the horizontal pads - NORTH and SOUTH
	 * @param vPad the vertical pads - EAST and WEST
	 */
	public PaddedPanel(final Widget w, final int hPad, final int vPad) {
		add(new HorizontalSpacer(hPad), DockPanel.NORTH);
		add(new VerticalSpacer(vPad), DockPanel.WEST);
		add(new HorizontalSpacer(hPad), DockPanel.SOUTH);
		add(new VerticalSpacer(vPad), DockPanel.EAST);
		add(w, DockPanel.CENTER);
	}
	
	public void add(final Widget w) {
		add(w, DockPanel.CENTER);
	}
	
}
