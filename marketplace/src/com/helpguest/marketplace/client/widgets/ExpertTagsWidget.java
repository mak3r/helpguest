package com.helpguest.marketplace.client.widgets;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.FocusListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.Window;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.OfferingsConstraints;
import com.helpguest.marketplace.client.ExpertiseService;
import com.helpguest.marketplace.client.ExpertiseServiceAsync;
import com.helpguest.marketplace.client.brick.HorizontalBrick;
import com.helpguest.marketplace.client.brick.VerticalBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.ui.HeaderMenu;

public class ExpertTagsWidget extends SimplePanel{
	
	private HeaderMenu headerMenu;
	private Authenticator authSource;
	//prahalad adding 7/09/08
	private ListBox categoriesListBox;
	private ListBox offeringsListBox;
	private TextArea tagTextArea;
	private TextArea viewTagsTextArea;
	private Label categoriesLabel = new Label("Category");
	private Label offeringsLabel = new Label("Offering");
	private Label tagsLabel = new Label("Tags");
	private Label tagsHelp = new Label("(enter 1 per line)");
	private String tagsTextAreaHeadStr = "Category:Offering:Tags you entered\n";
	private String tagsTextAreaConStr = "";
	private String expertId = null;
	//private TreeMap catOffgTagMap;
	private ArrayList catOffgTagList;
	private ArrayList expertCatOffgTagList;
	//private ArrayList catOffgListToPlus = new ArrayList();
	private String catStrToPlus = "";
	private String offgStrToPlus = "";
	//prahalad adding search service proxy for cat_offg_tags 07/09/08
	final ExpertiseServiceAsync expertiseService =
		(ExpertiseServiceAsync) GWT.create(ExpertiseService.class);
	
	public String getTagTextArea() {
		return tagTextArea.getText();
	}

	public void setTagTextArea(String anyText) {
		this.tagTextArea.setText(anyText);
	}
	
	public String getViewTagsTextArea() {
		return viewTagsTextArea.getText();
	}

	public void setViewTagsTextArea(String anyText) {
		this.viewTagsTextArea.setText(anyText);
	}

	public ArrayList getCatOffgTagList() {
		return catOffgTagList;
	}

	public void setCatOffgTagList(ArrayList catOffgTagList) {
		this.catOffgTagList = catOffgTagList;
	}
	
	public void resetTagsWidget() {
		//init();
		//catOffgTagList.clear();
		//tagTextArea = new TextArea();
		//tagTextArea.setText(null);
		tagTextArea.setText(null);
		catStrToPlus = "";
		offgStrToPlus = "";
		tagsTextAreaConStr = tagsTextAreaHeadStr;
		viewTagsTextArea.setText(tagsTextAreaConStr);
		catOffgTagList = new ArrayList();
		//if expert dto is null load a static Arraylist with the category offering pairs
		expertiseService.getCatOffgs(getCatOffgsCallBack);
		
		init();
		//catOffgTagList.clear();
		//tagTextArea.setText(null);
	}

	public ExpertTagsWidget(ExpertDTO expertDTO) {
		
		//prahalad adding 07/09/08
		//Initialize the SessionService
		ServiceDefTarget expertiseEndpoint = (ServiceDefTarget) this.expertiseService;
		String expertiseModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertiseService";
		expertiseEndpoint.setServiceEntryPoint(expertiseModuleRelativeURL);
		
		//if expert dto is null load a static Arraylist with the category offering pairs
		
		
		if (expertDTO != null) {
			expertId = expertDTO.getExpertUid();
			expertCatOffgTagList = new ArrayList();
			
		} else {
			catOffgTagList = new ArrayList();
			expertiseService.getCatOffgs(getCatOffgsCallBack);}
		init();
	}
	
	private void init() {
		
		//prahalad adding 7/09/08
		//catOffgTagList = new ArrayList();
		categoriesListBox = new ListBox();
		offeringsListBox = new ListBox();
		tagTextArea = new TextArea();
		viewTagsTextArea = new TextArea();
		viewTagsTextArea.setReadOnly(true) ;
		tagsTextAreaConStr = tagsTextAreaHeadStr;
		//Window.alert("expertId = " + expertId);
		if (expertId != null){
			//Window.alert("In expert Id not null ");
			expertiseService.getCatOffgTags(expertId, getCatOffgTagsCallBack);
			
		}else {viewTagsTextArea.setText(tagsTextAreaConStr);}
		//Window.alert("tagsTextAreaConStr" + tagsTextAreaConStr);
		
		//tagTextArea.setCharacterWidth(15);
		//tagTextArea.setVisibleLines(8);
		
		//call made to DB to load distinct categories from category_offerings table
		expertiseService.getCategories(getCatgsCallBack);
		
		//on change of the categories selection dynamically load the offerings
		categoriesListBox.addChangeListener(new ChangeListener(){
			public void onChange(Widget sender) {
				
				String cat = categoriesListBox.getValue(categoriesListBox.getSelectedIndex());
				String offg = offeringsListBox.getValue(offeringsListBox.getSelectedIndex());
				/*if(cat.indexOf("(+)") != -1){
					cat = cat.substring(0,cat.indexOf('('));
					}
				if(offg.indexOf("(+)") != -1){
					offg = offg.substring(0,offg.indexOf('('));
					}*/
				CategoryConstraints cc = new CategoryConstraints();
				cc.setCategoryName(cat);
				OfferingsConstraints oc = new OfferingsConstraints();
				oc.setOfferingName(offg);
				
				//call made to DB to load distinct offerings from category_offerings table for a category
				expertiseService.getOfferings(cc,getOffgsCallBack);				
			}
		});
		
		offeringsListBox.addChangeListener(new ChangeListener() {
			public void onChange(Widget sender) {
			
				String cat = categoriesListBox.getValue(categoriesListBox.getSelectedIndex());
				String offg = offeringsListBox.getValue(offeringsListBox.getSelectedIndex());
				/*if(cat.indexOf("(+)") != -1){
					cat = cat.substring(0,cat.indexOf('('));
					}
				if(offg.indexOf("(+)") != -1){
					offg = offg.substring(0,offg.indexOf('('));
					}*/
				CategoryConstraints cc = new CategoryConstraints();
				cc.setCategoryName(cat);
				OfferingsConstraints oc = new OfferingsConstraints();
				oc.setOfferingName(offg);
				
				//if expert id is null create 
				if (expertId!=null) {
					expertiseService.getTags(cc,oc,expertId,getTagsCallBack);
				} else {
					for (int listIt = 0;listIt<catOffgTagList.size();listIt++) {
						String catOffTag = catOffgTagList.get(listIt).toString();
						
						if (catOffTag.startsWith(cc.getCategoryName() + ":" + oc.getOfferingName())) {
							tagTextArea.setText(
									catOffTag.substring(
											catOffTag.indexOf(':', catOffTag.indexOf(':') + 1) + 1).replace('|', '\n'));
						}
						
					}
					
				}
				
			}
		});
		
		tagTextArea.addFocusListener(new FocusListener() {
			CategoryConstraints cc = new CategoryConstraints();
			OfferingsConstraints oc = new OfferingsConstraints();
			String cat = "";
			String offg = "";
			int catIndex = -1;
			int offgIndex = -1;
			public void onFocus(Widget sender) {
				cat = categoriesListBox.getValue(categoriesListBox.getSelectedIndex());
				offg = offeringsListBox.getValue(offeringsListBox.getSelectedIndex());
				catIndex = categoriesListBox.getSelectedIndex();
				offgIndex = offeringsListBox.getSelectedIndex();
				/*if(cat.indexOf("(+)") != -1){
					cat = cat.substring(0,cat.indexOf('('));
					}
				if(offg.indexOf("(+)") != -1){
					offg = offg.substring(0,offg.indexOf('('));
					}*/
				
				cc.setCategoryName(cat);
			    oc.setOfferingName(offg);
				
				//Window.alert("In  OnFocus catOff = " + cc.getCategoryName() + ":" + oc.getOfferingName());
			}

			public void onLostFocus(Widget sender) {
				//String cat = categoriesListBox.getItemText(categoriesListBox.getSelectedIndex());
				//String offg = offeringsListBox.getItemText(offeringsListBox.getSelectedIndex());
				
				
				String taContent = tagTextArea.getText();
				
				//Window.alert("In OnLost Focus catOff = " + cc.getCategoryName() + ":" + oc.getOfferingName());
				// if tag content is not null replace new line with pipe to
				// store in DB
				if (!taContent.trim().equals("")) {
					taContent = taContent.trim().replace('\n', '|');
					tagsHelp.setStyleName("expert_tags_widget_help_acknowledged");
					
					offeringsListBox.setItemText(offgIndex, 
							offeringsListBox.getValue(offgIndex) + "(+)");
					
					} else {//end if taContent not null
					 //the tag area is blank
					
						offeringsListBox.setItemText(offgIndex, 
								offeringsListBox.getValue(offgIndex));					 
					   }
				
				
				if (expertId != null) {
					// if expert id is not null update the DB with the new tag
					expertiseService.updateTags(cc, oc, expertId, taContent,
							updateTagsCallBack);
					
					
					//run thro expertCatOffgTagList and update the tag in the list
					
					//viewTagsTextArea.setText(tagsTextAreaConStr);
					
				} else {
					// if expert id is null append the new tag to the category
					// offering pair stored
					// as string the arraylist
					
					for (int listIt = 0; listIt < catOffgTagList.size(); listIt++) {
						String catOffTag = catOffgTagList.get(listIt)
								.toString();
						//Window.alert("In For  Loop catOff = " + cc.getCategoryName() + ":" + oc.getOfferingName());
						
						if (catOffTag.startsWith(cc.getCategoryName() + ":"
								+ oc.getOfferingName())) {
							catOffgTagList.set(listIt, cc.getCategoryName()
									+ ":" + oc.getOfferingName() + ":"
									+ taContent);
							
							
						}

					}
					//reset view tags text area to reload a change in tags
					tagsTextAreaConStr = tagsTextAreaHeadStr;
					//add plus to category
					catStrToPlus = "";
					
					for (int listIt1 = 0;listIt1<catOffgTagList.size();listIt1++) {
						String catOffgTagStr = catOffgTagList.get(listIt1).toString();
						String tag = catOffgTagStr.substring(catOffgTagStr.indexOf(':', catOffgTagStr.indexOf(':') + 1) + 1).trim();
						if (tag.length()>0){
							tagsTextAreaConStr = tagsTextAreaConStr +  catOffgTagStr  + "\n";
							catStrToPlus = catStrToPlus + catOffgTagStr.substring(0,catOffgTagStr.indexOf(':')) + "," ;
						}
					}
						
					viewTagsTextArea.setText(tagsTextAreaConStr);
					for (int it=0;it<categoriesListBox.getItemCount();it++)
					 {
					 	if (catStrToPlus.indexOf(categoriesListBox.getValue(it)) != -1)
					 			{categoriesListBox.setItemText(it,categoriesListBox.getValue(it) + "(+)");}else{
					 				categoriesListBox.setItemText(it,categoriesListBox.getValue(it));
					 			}
					 }
					//expertiseService.getCategories(getCatgsCallBack);
				}
			}
		
		});

		
		categoriesListBox.setVisibleItemCount(9);
		offeringsListBox.setVisibleItemCount(9);
		
		//The tags needs a special label/help widget
		HorizontalBrick tagsLabelBrick = new HorizontalBrick(tagsLabel, tagsHelp);
		
		//Create some labeled bricks
		VerticalBrick categoriesBrick = new VerticalBrick(categoriesLabel, categoriesListBox);
		VerticalBrick offeringsBrick = new VerticalBrick(offeringsLabel, offeringsListBox);
		VerticalBrick tagsBrick = new VerticalBrick(tagsLabelBrick, tagTextArea);
		
		//Lets identify all our labels for common formatting
		//by setting these after we create the bricks, we remove/update the default style name
		categoriesLabel.setStyleName("expert_tags_widget_label");
		offeringsLabel.setStyleName("expert_tags_widget_label");
		tagsLabel.setStyleName("expert_tags_widget_label");
		tagsHelp.setStyleName("expert_tags_widget_help");
		categoriesListBox.setStyleName("expert_tags_widget_selection");
		offeringsListBox.setStyleName("expert_tags_widget_selection");
		tagTextArea.setStyleName("expert_tags_widget_text_area");
		
		//prahalad adding horiz panel 7/09/08
		HorizontalPanel catOffgTagHorizPanel = new HorizontalPanel();
		
		catOffgTagHorizPanel.add(categoriesBrick);
		catOffgTagHorizPanel.add(offeringsBrick);
		catOffgTagHorizPanel.add(tagsBrick);
		//catOffgTagHorizPanel.add(viewTagsTextArea);
		
		setWidget(catOffgTagHorizPanel);
		setStyleName("expert_tags_widget");
		
	}
	//prahalad adding getOffgsCallBack 07/10/08
	AsyncCallback getCatgsCallBack = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("ExpertTagsWidget getCatgsCallBack: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("ExpertTagsWidget getCatgsCallBack: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			List categories = (List) result;
			//prevCategory = categoriesListBox.getItemText(categoriesListBox.getSelectedIndex());
			categoriesListBox.clear();
			//catOffgListToPlus.clear();
			catStrToPlus = "";
			
			if (expertId!=null){
					for (int listIt1 = 0;listIt1<expertCatOffgTagList.size();listIt1++) {
						String expertCatOffgTagStr = expertCatOffgTagList.get(listIt1).toString();
						String expertTag = expertCatOffgTagStr.substring(expertCatOffgTagStr.indexOf(':', expertCatOffgTagStr.indexOf(':') + 1) + 1).trim();
						if (expertTag.length()>0){
							//catOffgListToPlus.add(expertCatOffgTagStr);
							catStrToPlus = catStrToPlus + expertCatOffgTagStr.substring(0,expertCatOffgTagStr.indexOf(':')) + "," ;
							
						}
					}//end for iterate thro catOffgTagList
				
					for (int listIt=0;listIt<categories.size();listIt++) { 
						String cats = (String)categories.get(listIt);
						//if (catStrToPlus.contains(cats.subSequence(0,cats.length())))
						if (catStrToPlus.indexOf(cats) != -1 )
						{
							categoriesListBox.addItem((String)categories.get(listIt) + "(+)",(String)categories.get(listIt));
						}else {
							categoriesListBox.addItem((String)categories.get(listIt), (String)categories.get(listIt));
						}
						
					}//end for iterate thro categories
				
			}else{
				for (int listIt1 = 0;listIt1<catOffgTagList.size();listIt1++) {
					String catOffgTagStr = catOffgTagList.get(listIt1).toString();
					String tag = catOffgTagStr.substring(catOffgTagStr.indexOf(':', catOffgTagStr.indexOf(':') + 1) + 1).trim();
					if (tag.length()>0){
						//catOffgListToPlus.add(catOffgTagStr);
						catStrToPlus = catStrToPlus + catOffgTagStr.substring(0,catOffgTagStr.indexOf(':')) + "," ;
						
						
					}
				}//end for iterate thro catOffgTagList
			
				for (int listIt=0;listIt<categories.size();listIt++) { 
					String cats = (String)categories.get(listIt);
					if (catStrToPlus.indexOf(cats) != -1)
					{
						categoriesListBox.addItem((String)categories.get(listIt) + "(+)",(String)categories.get(listIt));
					}else {
						categoriesListBox.addItem((String)categories.get(listIt), (String)categories.get(listIt));
					}
					
				}//end for iterate thro categories
			
			}//end else if id is not null
			
			categoriesListBox.setItemSelected(0, true);
			categoriesListBox.setVisibleItemCount(9);
			for (int lbIt=0;lbIt<categoriesListBox.getItemCount();lbIt++) {
				if (categoriesListBox.isItemSelected(lbIt))
				{
					String cat = categoriesListBox.getValue(categoriesListBox.getSelectedIndex());
					/*if(cat.indexOf("(+)") != -1){
					cat = cat.substring(0,cat.indexOf('('));
					}*/
					CategoryConstraints cc = new CategoryConstraints();
					
					cc.setCategoryName(cat);
					
					expertiseService.getOfferings(cc,getOffgsCallBack);
				}
			}
		}		
	};
	//prahalad adding getOffgsCallBack 07/09/08
	AsyncCallback getOffgsCallBack = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
	//			Window.alert("ExpertTagsWidget getOffgsCallBack: " +  sBuf.toString());
			} catch (Throwable th) {
	//			Window.alert("ExpertTagsWidget getOffgsCallBack: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			List offerings = (List) result;
			
			offeringsListBox.clear();
			offgStrToPlus = "";
			/*for (int listIt=0;listIt<offerings.size();listIt++)
			{
				offeringsListBox.addItem((String)offerings.get(listIt));
			}*/
			if (expertId!=null){
					for (int listIt1 = 0;listIt1<expertCatOffgTagList.size();listIt1++) {
						String expertCatOffgTagStr = expertCatOffgTagList.get(listIt1).toString();
						String expertTag = expertCatOffgTagStr.substring(expertCatOffgTagStr.indexOf(':', expertCatOffgTagStr.indexOf(':') + 1) + 1).trim();
						if (expertTag.length()>0){
							//catOffgListToPlus.add(expertCatOffgTagStr);
							offgStrToPlus = offgStrToPlus + expertCatOffgTagStr.substring(0,expertCatOffgTagStr.lastIndexOf(':')) + ",";
							
						}
					}//end for iterate thro catOffgTagList
					//Window.alert("offgStrToPlus" + offgStrToPlus);
					for (int listIt=0;listIt<offerings.size();listIt++) { 
						String offgs = (String)offerings.get(listIt);
						if (offgStrToPlus.indexOf(categoriesListBox.getValue(categoriesListBox.getSelectedIndex()) + ":" + offgs) != -1)
						{
							offeringsListBox.addItem((String)offerings.get(listIt) + "(+)", (String)offerings.get(listIt));
						}else {
							offeringsListBox.addItem((String)offerings.get(listIt), (String)offerings.get(listIt));
						}
					}//end for iterate thro categories
				
			}else{
				for (int listIt1 = 0;listIt1<catOffgTagList.size();listIt1++) {
					String catOffgTagStr = catOffgTagList.get(listIt1).toString();
					String tag = catOffgTagStr.substring(catOffgTagStr.indexOf(':', catOffgTagStr.indexOf(':') + 1) + 1).trim();
					if (tag.length()>0){
						//catOffgListToPlus.add(catOffgTagStr);
						offgStrToPlus = offgStrToPlus + catOffgTagStr.substring(0,catOffgTagStr.lastIndexOf(':')) + ",";
						
					}
				}//end for iterate thro catOffgTagList
			
				for (int listIt=0;listIt<offerings.size();listIt++) { 
					String offgs = (String)offerings.get(listIt);
					if (offgStrToPlus.indexOf(categoriesListBox.getValue(categoriesListBox.getSelectedIndex()) + ":" + offgs) != -1)
					{
						offeringsListBox.addItem((String)offerings.get(listIt) + "(+)", (String)offerings.get(listIt));
					}else {
						offeringsListBox.addItem((String)offerings.get(listIt), (String)offerings.get(listIt));
					}
				}//end for iterate thro categories
			}//end else if id is not null
			
			
			offeringsListBox.setItemSelected(0, true);
			offeringsListBox.setVisibleItemCount(9);
			for (int lbIt=0;lbIt<offeringsListBox.getItemCount();lbIt++)
			{
				if (offeringsListBox.isItemSelected(lbIt))
				{
					//previous.setOffering(current.getOffering());
					//prevOffering = currOffering;
					String cat = categoriesListBox.getValue(categoriesListBox.getSelectedIndex());
					String offg = offeringsListBox.getValue(offeringsListBox.getSelectedIndex());
					/*if(cat.indexOf("(+)") != -1){
						cat = cat.substring(0,cat.indexOf('('));
						}
					if(offg.indexOf("(+)") != -1){
						offg = offg.substring(0,offg.indexOf('('));
						}*/
					CategoryConstraints cc = new CategoryConstraints();
					cc.setCategoryName(cat);
					OfferingsConstraints oc = new OfferingsConstraints();
					oc.setOfferingName(offg);
					
					//current.setOffering(oc.getOfferingName());
					//currOffering = oc.getOfferingName();
					if (expertId!=null /*&& !expertId.equals("R11A307136E5P")*/){
					expertiseService.getTags(cc,oc,expertId,getTagsCallBack);}
					else 
					{
						for (int listIt = 0;listIt<catOffgTagList.size();listIt++)
						{
							String catOffTag = catOffgTagList.get(listIt).toString();
							//StringTokenizer st = new StringTokenizer(catOffgTagList.get(listIt).toString(), ":");
							//ArrayList catOffTag = new ArrayList();
							//Window.alert("catOffTag = " + catOffTag);
							if (catOffTag.startsWith(cc.getCategoryName() + ":" + oc.getOfferingName()))
								{tagTextArea.setText(catOffTag.substring(catOffTag.indexOf(':', catOffTag.indexOf(':') + 1) + 1).replace('|', '\n'));}
							
						}
						
					}
				}
			}
			
			//init();
		}		
	};

	//prahalad adding getOffgsCallBack 07/10/08
	AsyncCallback getTagsCallBack = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("ExpertTagsWidget getTagsCallBack: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("ExpertTagsWidget getTagsCallBack: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			List tags = (List) result;
			String tagStr = "";
			for (int listIt=0;listIt<tags.size();listIt++)
			{
				tagStr = tagStr + tags.get(listIt).toString().replace('|', '\n') + "\n";
			}
			tagTextArea.setText(tagStr);

		}		
	};
	
	//prahalad adding getOffgsCallBack 07/10/08
	AsyncCallback updateTagsCallBack = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("ExpertTagsWidget updateTagsCallBack: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("ExpertTagsWidget updateTagsCallBack: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertCatOffgTagList = new ArrayList();
			//reset view tags text area to reload a change in tags
			expertiseService.getCatOffgTags(expertId, getCatOffgTagsCallBack);
			//expertiseService.getCategories(getCatgsCallBack);
		}		
	};
	
	//prahalad adding getOffgsCallBack 07/10/08
	AsyncCallback getCatOffgsCallBack = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("ExpertTagsWidget getCatOffgsCallBack: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("ExpertTagsWidget getCatOffgsCallBack: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			List catOffgs = (List) result;
			
			for (int listIt=0;listIt<catOffgs.size();listIt++)
			{
				
				catOffgTagList.add(catOffgs.get(listIt).toString());
			}
			
		}		
	};
	
	//prahalad adding getOffgsCallBack 10/31/08
	AsyncCallback getCatOffgTagsCallBack = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("ExpertTagsWidget getCatOffgTagsCallBack: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("ExpertTagsWidget getCatOffgTagsCallBack: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			List catOffgTags = (List) result;
			expertCatOffgTagList.clear();
			tagsTextAreaConStr = tagsTextAreaHeadStr;
			for (int listIt=0;listIt<catOffgTags.size();listIt++)
			{
				//Window.alert("catOffgTags ["+listIt+"]" + catOffgTags.get(listIt).toString());
				expertCatOffgTagList.add(catOffgTags.get(listIt).toString());
				tagsTextAreaConStr = tagsTextAreaConStr + catOffgTags.get(listIt).toString() + "\n";
				
			}
			viewTagsTextArea.setText(tagsTextAreaConStr);
			catStrToPlus = "";
			for (int listIt1 = 0;listIt1<expertCatOffgTagList.size();listIt1++) {
				String expertCatOffgTagStr = expertCatOffgTagList.get(listIt1).toString();
				String expertTag = expertCatOffgTagStr.substring(expertCatOffgTagStr.indexOf(':', expertCatOffgTagStr.indexOf(':') + 1) + 1).trim();
				String cat = expertCatOffgTagStr.substring(0,expertCatOffgTagStr.indexOf(':'));
				if (expertTag.length()>0){
					 
					catStrToPlus = catStrToPlus + expertCatOffgTagStr.substring(0,expertCatOffgTagStr.indexOf(':')) + "," ;
					}
				}
			for (int it=0;it<categoriesListBox.getItemCount();it++)
			 {
			 	if (catStrToPlus.indexOf(categoriesListBox.getValue(it)) != -1)
			 			{categoriesListBox.setItemText(it,categoriesListBox.getValue(it) + "(+)");}else{
			 				categoriesListBox.setItemText(it,categoriesListBox.getValue(it));
			 			}
			 }
			
		}		
	};
}
