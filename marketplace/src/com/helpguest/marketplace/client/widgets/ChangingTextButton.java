/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 6, 2007
 */
package com.helpguest.marketplace.client.widgets;

import java.util.ArrayList;
import java.util.List;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.MouseListener;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.SourcesClickEvents;
import com.google.gwt.user.client.ui.SourcesMouseEvents;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author mabrams
 *
 */
public class ChangingTextButton extends SimplePanel implements SourcesMouseEvents, MouseListener, SourcesClickEvents {

	private String enabledText;
	private String disabledText;
	private FocusPanel initialButton;
	private FocusPanel hoverButton;
	private FocusPanel disabledButton;
	private List mousers = new ArrayList();

	public final static String WHITE = "#FFFFFF";
	public final static String RED = "#FF3311";
	public final static String HOT_RED = "#FF0000";
	public final static String LIGHT_GREY = "#CCCCCC";
	public final static String GOLD = "#FFCC00";
	public final static String DARK_GREY = "#404041";
	public final static String BLACK = "#000000";
	public final static String EXPERT_GREEN = "#006600";
	public final static String EXPERT_BLUE = "#0099CC";
	public final static String EXPERT_ORANGE = "#F1592A";
	
	
	public ChangingTextButton() {
		this("","");
	}
	
	public ChangingTextButton(final String enabledText, final String disabledText) {
		this.enabledText = enabledText;
		this.disabledText = disabledText;
		setStyleName("hg-ChangingTextButton");

		hoverButton = getButton(
				enabledText, DARK_GREY, EXPERT_ORANGE);
		initialButton = getButton(
				enabledText, WHITE, EXPERT_BLUE);
		disabledButton = getButton(
				disabledText, LIGHT_GREY, EXPERT_ORANGE);
		
		initialButton.addMouseListener(this);
		hoverButton.addMouseListener(this);
		
		setEnabled(true);
	}

	
	public ChangingTextButton(
			final String enabledText, 
			final String disabledText, 
			final String color) {
		this.enabledText = enabledText;
		this.disabledText = disabledText;
		setStyleName("hg-ChangingTextButton");

		hoverButton = getButton(
				enabledText, BLACK, color);
		initialButton = getButton(
				enabledText, HOT_RED, lighter(color));
		disabledButton = getButton(
				disabledText, WHITE, color);
		
		initialButton.addMouseListener(this);
		hoverButton.addMouseListener(this);
		
		setEnabled(true);
	}

	
	protected FocusPanel getButton(
			final String text,
			final String textColor,
			final String buttonColor) {
		RoundedPanel rpButton = new RoundedPanel(RoundedPanel.ALL, 5);
		
		StringBuffer innerDiv = new StringBuffer(
				"<div style=\"" +
				"background-color:" + buttonColor + ";" +
				"border:thick solid " + buttonColor + ";" + 
				"text-align:center;" +
				"\">");
		
		innerDiv.append(getHTMLWord(text, textColor));
		innerDiv.append("</div>");
		HTML inner = new HTML(innerDiv.toString());
				
		rpButton.add(inner);
		rpButton.setCornerColor(buttonColor);
		
		FocusPanel changingButton = new FocusPanel(rpButton);
		return changingButton;
	}
	
	
	protected String getHTMLWord(final String word, final String color) {
		String htmlWord = "<label style=\"" +
				"color:" + color + ";" +
				"font-family: Arial,Helvetica,sans-serif;" +
				"font-size:1.2em;" +
				"font-weight: bold;" +
				"\">" + word + "</label>";
		return htmlWord;
	}
	
	/**
	 * @return the text
	 */
	public String getEnabledText() {
		return enabledText;
	}
	
	public String getDisabledText() {
		return disabledText;
	}
	
	/**
	 * Parse the color string in the format #FFFFFF;
	 * subtract #010101 for any given string
	 * @param color the color to make lighter
	 * @return a ligter color
	 */
	public String lighter(final String color) {
		Integer hex = Integer.decode(color);
		return "#" + Integer.toHexString(hex.intValue() - 0x111111);
	}

	/**
	 * @param text the text to set
	 */
	public void setEnabledText(String text) {
		this.enabledText = text;
	}
	
	public void setDisabledText(final String text) {
		this.disabledText = text;
	}
	
	public void setEnabled(boolean enabled) {
		if (enabled) {
			setWidget(initialButton);
		} else {
			setWidget(disabledButton);
		}
	}

	public void addMouseListener(MouseListener listener) {
		hoverButton.addMouseListener(listener);
		initialButton.addMouseListener(listener);
	}

	public void removeMouseListener(MouseListener listener) {
		hoverButton.removeMouseListener(listener);
		initialButton.removeMouseListener(listener);
	}

	public void onMouseDown(Widget sender, int x, int y) {
		for(int i=0; i<mousers.size(); i++) {
			((MouseListener) mousers.get(i)).onMouseDown(sender, x, y);
		}		
	}

	public void onMouseEnter(Widget sender) {
		if (sender.equals(initialButton)) {
			setWidget(hoverButton);
		}
	}

	public void onMouseLeave(Widget sender) {
		if (sender.equals(hoverButton)) {
			setWidget(initialButton);
		}
	}

	public void onMouseMove(Widget sender, int x, int y) {
		for(int i=0; i<mousers.size(); i++) {
			((MouseListener) mousers.get(i)).onMouseMove(sender, x, y);
		}		
	}

	public void onMouseUp(Widget sender, int x, int y) {
		for(int i=0; i<mousers.size(); i++) {
			((MouseListener) mousers.get(i)).onMouseUp(sender, x, y);
		}		
	}

	public void addClickListener(ClickListener listener) {
		hoverButton.addClickListener(listener);
		initialButton.addClickListener(listener);		
	}

	public void removeClickListener(ClickListener listener) {
		hoverButton.addClickListener(listener);
		initialButton.addClickListener(listener);
	}

}
