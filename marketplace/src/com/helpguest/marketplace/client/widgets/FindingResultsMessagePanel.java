package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.marketplace.client.ui.TwoTonePanel;


public class FindingResultsMessagePanel extends SimplePanel {
	
	private Timer t;
	
	public FindingResultsMessagePanel() {
		final String messageText = "Finding experts that match your search...";
		final Label message = new Label(messageText);
		message.setStyleName("hg-instructional-verbage");
		

		t = new Timer() {
			int maxDots = 30;
			int resetCount = 0;

			public void run() {
				if (resetCount < maxDots) {
					message.setText(message.getText() + ".");
					resetCount++;
				} else {
					message.setText(messageText);
					resetCount = 0;
				}
			}
			
		};		
		
		PaddedPanel paddedVerbage = new PaddedPanel(message, 20);
		paddedVerbage.setCellHorizontalAlignment(
				message, PaddedPanel.ALIGN_CENTER);
		paddedVerbage.setWidth("300px");
		TwoTonePanel ttPanel = new TwoTonePanel("300px");
		ttPanel.setHeading(new Label("Searching"));
		ttPanel.setBody(paddedVerbage);
		setWidget(ttPanel);
	}
	
	public void stopTimer() {
		t.cancel();
	}
	
	public FindingResultsMessagePanel startTimer() {
		t.scheduleRepeating(200);
		return this;
	}

}
