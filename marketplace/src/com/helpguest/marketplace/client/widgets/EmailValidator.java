package com.helpguest.marketplace.client.widgets;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.protocol.VerifyEmailOutcome;
import com.helpguest.marketplace.client.AuthenticationService;
import com.helpguest.marketplace.client.AuthenticationServiceAsync;
import com.helpguest.marketplace.client.bluegray.ui.ContentEntry;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.history.SubCommandToken;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.util.DebugPanel;

public class EmailValidator extends SimplePanel implements SubCommandToken {

	public static final String EMAIL_ADDRESS_KEY = "email";
	public static final String EMAIL_CODE_KEY = "code";
	private static final String TOKEN = "email_validator";
	private String emailCode = "NO CODE ENTERED";
	private String emailAddress = "NO EMAIL ADDRESS ENTERED";
	private ContentEntry content = new ContentEntry("validatedYes.div");
	private Button okButton = new Button("OK");
	private VisualTimer validatingAccountTimer = new VisualTimer("Validating Your Account", 10);
	private SimplePanel validationOutcome = new SimplePanel();

	private ResultContainer resultContainer;
	
	//Create the authentication service proxy
	final AuthenticationServiceAsync authenticationService =
		(AuthenticationServiceAsync) GWT.create(AuthenticationService.class);

	public EmailValidator(final ResultContainer resultContainer) {
		this.resultContainer = resultContainer;
		
		//Initialize the AuthenticationService
		ServiceDefTarget endpoint = (ServiceDefTarget) this.authenticationService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "AuthenticationService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
	}

	private void init() {
		okButton.addClickListener(new ClickListener() {

			public void onClick(Widget arg0) {
				resultContainer.reset();
			}
			
		});
		
		VerticalPanel vp = new VerticalPanel();
		validationOutcome.setWidget(validatingAccountTimer);
		vp.add(validationOutcome);
		vp.add(okButton);
		
		setWidget(vp);
		resultContainer.setWidget(this);
	}
	
	public static String getEntryToken(final String email, final String code) {
		String[] params = {
				HistoryCommander.getParameterString(EMAIL_ADDRESS_KEY, email),
				HistoryCommander.getParameterString(EMAIL_CODE_KEY, code)
		};
		return HistoryCommander.buildToken(TOKEN, params);
	}

	public String getBaseToken() {
		return TOKEN;
	}

	public void onEntry(Map<String, String> kvMap) {
		validatingAccountTimer.start();
		init();
		if (kvMap != null) {			
			emailAddress = (String) kvMap.get(EMAIL_ADDRESS_KEY);
			emailCode = (String) kvMap.get(EMAIL_CODE_KEY);
			if (emailAddress != null && emailCode != null) {
				authenticationService.verifyEmail(
						emailAddress, 
					emailCode, 
					verifyEmailCallback);
			} else {
				validatingAccountTimer.stop();
				validationOutcome.setWidget(getInvalidContent());
			}
		}
	}

	public void onExit() {
		// TODO Auto-generated method stub
		
	}
	
	private HTML getInvalidContent() {
		StringBuffer invalidContent = new  StringBuffer();
		invalidContent.append("<div class=\"validated\" >");
		invalidContent.append("<p>Your address&nbsp;");
		invalidContent.append(emailAddress);
		invalidContent.append(" could not be validated using code&nbsp;");
		invalidContent.append(emailCode);
		invalidContent.append(".</p>");
		invalidContent.append("<p>Please be sure you have copied the entire url correctly from the email sent to you by HelpGuest.</p>");
		invalidContent.append("</div>");
		return new HTML(invalidContent.toString());
	}

	public void setParent(CommandToken parentCommand) {
		// TODO Auto-generated method stub
		
	}
	
	AsyncCallback verifyEmailCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			validatingAccountTimer.stop();
			validationOutcome.setWidget(new OopsWidget());
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				DebugPanel.setText("EmailValidator", "LoginPanel verifyEmailCallback: " + sBuf.toString());
			} catch (Throwable th) {
				DebugPanel.setText("EmailValidator", "LoginPanel verifyEmailCallback [throwable]: " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			validatingAccountTimer.stop();
			VerifyEmailOutcome emailOutcome = (VerifyEmailOutcome) result;
			if (emailOutcome.equals(new VerifyEmailOutcome.Success())) {
				validationOutcome.setWidget(new ContentEntry("validatedYes.div"));
			} else {
				validationOutcome.setWidget(new HTML(emailOutcome.getMessage()));
			}

		}
		
	};

}
