/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author mabrams
 *
 */
public class SiteSeal extends SimplePanel {

	public SiteSeal() {
		super();
		StringBuffer iframe =
			new StringBuffer("<iframe " +
					"src=\"SiteSeal.html\" " +
					"width=\"100px\" " +
					"height=\"100px\" " +
					"frameborder=\"0\" " +
					"scrolling=\"no\" " +
					"style=\"border:0;\">");
		iframe.append("</iframe>");
		setWidget(new HTML(iframe.toString()));
	}
}
