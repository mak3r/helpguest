/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 21, 2007
 */
package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.marketplace.client.ui.TwoTonePanel;

/**
 * @author mabrams
 *
 */
public class FutureFeaturePanel extends SimplePanel {

	
	public FutureFeaturePanel(final String title, final String message, final String width) {
		super();
		
		Label verbage = new Label(message);
		
		PaddedPanel paddedVerbage = new PaddedPanel(verbage, 20);
		paddedVerbage.setCellHorizontalAlignment(
				verbage, PaddedPanel.ALIGN_CENTER);
		paddedVerbage.setWidth(width);
		
		TwoTonePanel ttPanel = new TwoTonePanel(width);
		ttPanel.setHeading(new Label(title));
		ttPanel.setBody(paddedVerbage);
		
		setWidget(ttPanel);
	}
}
