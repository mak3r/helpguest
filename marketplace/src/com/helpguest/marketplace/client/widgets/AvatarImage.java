/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Sep 28, 2007
 */
package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.Image;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public class AvatarImage extends Image {

	
	public AvatarImage(final ExpertDTO expert) {
		super(expert.getAvatarURL());
		setWidth("90px");
		setHeight("100px");
		setStyleName("avatar");
	}
}
