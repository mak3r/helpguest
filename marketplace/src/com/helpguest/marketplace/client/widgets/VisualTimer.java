package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;

public class VisualTimer extends SimplePanel {
	
	private String message;
	private Label wording;
	private int dotCount;
	
	/**
	 * Clients are responsible for starting AND stopping
	 * this widget.  It will not timeout by itself.
	 * @param message String
	 * @param maxDotCount int
	 */
	public VisualTimer(final String message, final int maxDotCount) {
		this.message = message;
		dotCount = maxDotCount;
		wording = new Label(message);
		wording.setStyleName("hg-instructional-verbage");
		
		setWidget(wording);
	}

	public void start() {
		t.scheduleRepeating(500);		
	}
	
	public void stop() {
		t.cancel();
	}

	Timer t = new Timer() {
		int resetCount = 0;

		public void run() {
			if (resetCount < dotCount) {
				wording.setText(wording.getText() + ".");
				resetCount++;
			} else {
				wording.setText(message);
				resetCount = 0;
			}
		}
		
	};
}
