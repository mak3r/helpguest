package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class SectionLabel extends SimplePanel {

	private Label titleLabel;
	private Label description;
	
	private static final String STYLE = "section_label";
	
	public SectionLabel(final String title, final String desc) {
		titleLabel = new Label(title);
		description = new Label(desc);
		
		titleLabel.addStyleName("title");
		description.addStyleName("description");
		
		VerticalPanel vp = new VerticalPanel();
		vp.addStyleName(STYLE);
		vp.add(titleLabel);
		vp.add(description);
		
		add(vp);
	}
}
