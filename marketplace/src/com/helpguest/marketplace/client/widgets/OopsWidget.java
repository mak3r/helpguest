package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.marketplace.client.bluegray.ui.ContentEntry;

public class OopsWidget extends SimplePanel {

	private static final ContentEntry oops = new ContentEntry("oops.div");
	
	public OopsWidget() {
		setWidget(oops);
	}
	
}
