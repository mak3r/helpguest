/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 3, 2007
 */
package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author mabrams
 *
 */
public class BannerPanel extends SimplePanel {

	public BannerPanel() {
		HTML frame = new HTML(
			"<iframe " +
			"src=\"flash/banner.swf\" " +
			"width=\"750px\" " +
			"height=\"170px\" " +
			"frameborder=\"0\" " +
			"scrolling=\"no\" " +
			"style=\"border:0;\">" +
			"</iframe>"
		);		
		
		setWidget(frame);
	}
}
