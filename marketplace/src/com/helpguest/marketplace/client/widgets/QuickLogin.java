package com.helpguest.marketplace.client.widgets;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusListener;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.protocol.AuthenticationOutcome;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.gwt.protocol.VerifyEmailOutcome;
import com.helpguest.gwt.protocol.AuthenticationOutcome.FailUserPass;
import com.helpguest.gwt.protocol.AuthenticationOutcome.Success;
import com.helpguest.marketplace.client.AuthenticationService;
import com.helpguest.marketplace.client.AuthenticationServiceAsync;
import com.helpguest.marketplace.client.SessionService;
import com.helpguest.marketplace.client.SessionServiceAsync;
import com.helpguest.marketplace.client.bakery.CookieManager;
import com.helpguest.marketplace.client.bluegray.ui.BecomeAnExpert;
import com.helpguest.marketplace.client.bluegray.ui.TabbedContentPanel;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.observer.SourcesAuthenticationOutcomes;
import com.helpguest.marketplace.client.ui.TwoTonePanel;

public class QuickLogin extends SimplePanel implements ClickListener
               ,SourcesAuthenticationOutcomes,Authenticator{

	private TextBox userNameBox = new TextBox();
	private PasswordTextBox passwordBox = new PasswordTextBox();
	private Label error = new Label();
	private static TwoTonePanel ttExpired;
	//Create the authentication service proxy
	final AuthenticationServiceAsync authenticationService =
		(AuthenticationServiceAsync) GWT.create(AuthenticationService.class);
	//Create the session service proxy
	final SessionServiceAsync sessionService =
		(SessionServiceAsync) GWT.create(SessionService.class);
	
	private List observerList = new ArrayList();
	private AuthenticationOutcome outcome = null;
	private VerifyEmailOutcome emailOutcome = null;
	private String verifyEmailAddress;
	private String verifyEmailUid;
	private final static String VERIFY_EMAIL_ADDRESS = "email";
	private final static String VERIFY_UID = "uid";
	public static final LoginType EXPERT_LOGIN_TYPE =
		new LoginType.Expert();
	private String onLoginGoTo;
	
	private Hyperlink forgotPasswordLink = 
		new Hyperlink(
				"Forgot Password",
				TabbedContentPanel.getHistoryCommandForTab(
						BecomeAnExpert.TOKEN, 
						GetUserEmailToResetPswd.TOKEN,
						GetUserEmailToResetPswd.FORGOT_PASSWORD_VALUE));
	
	public QuickLogin(final String gotoPageHref) {
	
		//Initialize the AuthenticationService
		ServiceDefTarget endpoint = (ServiceDefTarget) this.authenticationService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "AuthenticationService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		//Initialize the SessionService
		ServiceDefTarget sessionEndpoint = (ServiceDefTarget) this.sessionService;
		String sessionModuleRelativeURL = GWT.getModuleBaseURL() + "SessionService";
		sessionEndpoint.setServiceEntryPoint(sessionModuleRelativeURL);
		
	
		this.onLoginGoTo = gotoPageHref;
	
		init();
		
	}
	
	private void init() {
		Label quickTicketLabel = new Label("EXPERT SIGN IN");
		quickTicketLabel.setStyleName("quick_login_label");
		this.userNameBox.setText("Email Address");

		userNameBox.addFocusListener(new FocusListener() {
			public void onFocus(Widget sender) {
				userNameBox.selectAll();
				userNameBox.setText(null);
			}

			public void onLostFocus(Widget sender) {
			}
		});
		
		userNameBox.setStyleName("quick_login_user_input");
		KeyboardListener userNameKeyListener = new KeyboardListener() {
			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				//NOOP	
			}


			
			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (KeyboardListener.KEY_ENTER == keyCode) {
					//Enter key pressed handle the search
					//search();
					
					onClick(sender);
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				// TODO this would be a good place to use the SuggestBox oracle
			}	
		};
		this.userNameBox.addKeyboardListener(userNameKeyListener);
		
		this.passwordBox.setText("Password");

		passwordBox.addFocusListener(new FocusListener() {
			public void onFocus(Widget sender) {
				passwordBox.selectAll();
				passwordBox.setText(null);
			}

			public void onLostFocus(Widget sender) {
			}
		});		
		
		passwordBox.setStyleName("quick_login_password_input");
		KeyboardListener passwordBoxKeyListener = new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				//NOOP	
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (KeyboardListener.KEY_ENTER == keyCode) {
					//Enter key pressed handle the search
					onClick(sender);
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				// TODO this would be a good place to use the SuggestBox oracle
			}	
		};
		this.passwordBox.addKeyboardListener(passwordBoxKeyListener);
		
		
		final PushButton goButton = new PushButton("GO >");
		goButton.setStyleName("quick_login_button");
		goButton.addClickListener(this);

		forgotPasswordLink.setStyleName("quick_login_forgot_password_link");
		
		Grid quickTicketWidget = new Grid(4,2);
		quickTicketWidget.setWidget(0, 0, quickTicketLabel);
		quickTicketWidget.setWidget(1, 0, this.userNameBox);
		quickTicketWidget.setWidget(2, 0, this.passwordBox);
		quickTicketWidget.setWidget(1, 1, goButton);
		quickTicketWidget.setWidget(3, 0, forgotPasswordLink);
		
		setWidget(quickTicketWidget);		
		setStyleName("quick_login_widget");
	}
    
	public void onClick(Widget sender) {
		if ( (userNameBox.getText() != null && !userNameBox.getText().equals(""))
				&& (passwordBox.getText() != null && !passwordBox.getText().equals("")) ) {
			authenticationService.authenticate(
					userNameBox.getText(),
					passwordBox.getText(),
					EXPERT_LOGIN_TYPE,
					true,
					callback);
			/*
			callback.onSuccess(new AuthenticationOutcome.Success());
			*/
		} else {
			//TODO throw up an error indicating that they must have a username and password
			error.setText(new FailUserPass().getMessage());
		}
		//Now clear the password box
		passwordBox.setText(null);
	}

		
	AsyncCallback callback = new AsyncCallback() {

			public void onFailure(Throwable caught) {
				try {
					throw caught;
				} catch (InvocationException iex) {
					StackTraceElement[] ste = iex.getStackTrace();
					StringBuffer sBuf = new StringBuffer(iex.getMessage());
					for (int i = 0; i < ste.length; i++) {
						sBuf.append("\n\t" + ste.toString());
					}
//					Window.alert("LoginPanel callback: " + sBuf.toString());
				} catch (Throwable th) {
//					Window.alert("LoginPanel callback [throwable]: " + th.getMessage());
				}
			}

			public void onSuccess(Object result) {
				outcome = (AuthenticationOutcome) result;
				
				if (outcome.equals(new Success())) {
					//preserve the email and password
					CookieManager.bake(
							CookieManager.EMAIL_COOKIE, userNameBox.getText(), false);
					CookieManager.bake(
							CookieManager.PASSWORD_COOKIE, passwordBox.getText(), true);
					
					//Create a demoSession (will only be created if expert.canDemo)
					sessionService.createDemoSession(
							userNameBox.getText(), createDemoSessionCallback);
					
					//onExit();
					
					//go to the href page
					//HistoryCommander.getInstance().onHistoryChanged(onLoginGoTo);			
					HistoryCommander.newItem(onLoginGoTo);
				} else {
					Window.alert(outcome.getMessage());
					//error.setText(outcome.getMessage());
				}
				//notify observers
				notifyObservers();
			}
			
		};
		
		AsyncCallback createDemoSessionCallback = new AsyncCallback() {

			public void onFailure(Throwable caught) {
				try {
					throw caught;
				} catch (InvocationException iex) {
					StackTraceElement[] ste = iex.getStackTrace();
					StringBuffer sBuf = new StringBuffer(iex.getMessage());
					for (int i = 0; i < ste.length; i++) {
						sBuf.append("\n\t" + ste.toString());
					}
//					Window.alert("LoginPanel createDemoSessionCallback: " + sBuf.toString());
				} catch (Throwable th) {
//					Window.alert("LoginPanel createDemoSessionCallback [throwable]: " + th.getMessage());
				}
			}

			public void onSuccess(Object result) {
				//no-op
			}
			
		};
		
	public void logout() {
			outcome = null;
			CookieManager.eat(CookieManager.EMAIL_COOKIE);
			CookieManager.eat(CookieManager.PASSWORD_COOKIE);
			notifyObservers();
		}
	private final void notifyObservers() {
			for (int i = 0; i<observerList.size(); i++) {
				((AuthenticationListener) observerList.get(i)).onChange(this);
			}
		}

	
	public void addAuthenticationListener(final AuthenticationListener authListener){
		observerList.add(authListener);
		}
	public void removeAuthenticationListener(final AuthenticationListener authListener){
		observerList.remove(authListener);
	}
	
	public AuthenticationOutcome getAuthenticationOutcome() {
		return outcome;
	}
	
	public boolean isLoggedIn() {
		//FIXME WE MUST SET AT LOGIN AND TEST HERE A UNIQUE READABLE PASSWORD KEY
		if (CookieManager.ponder(CookieManager.EMAIL_COOKIE) == null) {
			return false;
		}
		return true;
	}
	
	public String getAuthenticatedParty() {
		return CookieManager.ponder(CookieManager.EMAIL_COOKIE);
	}
	
	public LoginType getLoginType() {
		return EXPERT_LOGIN_TYPE;
	}
	
	public Widget getExpiredWidget() {
		if (ttExpired == null) {
			Label expiredHeader = new Label("Page expired.");
			Label expiredInstruction = new Label(
					"You must login again to access the content on this page.");
			
			expiredInstruction.setStyleName("hg-instructional-verbage");
			
			PaddedPanel paddedInstruction = 
				new PaddedPanel(expiredInstruction, 20);
			paddedInstruction.setCellHorizontalAlignment(
					expiredInstruction, PaddedPanel.ALIGN_CENTER);
			paddedInstruction.setWidth("300px");
	
			ttExpired = new TwoTonePanel(
					expiredHeader, paddedInstruction, "300px");
		}
		return ttExpired;
	}
	
}
