package com.helpguest.marketplace.client.widgets;

import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.FocusListener;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.bluegray.ui.QuickTicket;
import com.helpguest.marketplace.client.history.HistoryCommander;

public class QuickTicketSearch extends SimplePanel {

	private TextBox ticketEntry = new TextBox();
	
	public QuickTicketSearch() {
		init();
	}

	
	private void init() {
		Label quickTicketLabel = new Label("QUICK TICKET");
		quickTicketLabel.setStyleName("quick_ticket_label");
		this.ticketEntry.setText("Enter Ticket Number");
		
		ticketEntry.addFocusListener(new FocusListener() {
			public void onFocus(Widget sender) {
				ticketEntry.selectAll();
				ticketEntry.setText(null);
			}

			public void onLostFocus(Widget sender) {
			}
		});

		ticketEntry.setStyleName("quick_ticket_input");
		KeyboardListener keyListener = new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				//NOOP	
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (KeyboardListener.KEY_ENTER == keyCode) {
					//Enter key pressed handle the search
					search();
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				// TODO this would be a good place to use the SuggestBox oracle
			}	
		};
		this.ticketEntry.addKeyboardListener(keyListener);
		
		final PushButton goButton = new PushButton("GO >");
		goButton.setStyleName("quick_ticket_button");
		ClickListener clickListener = new ClickListener() {
			public void onClick(Widget sender) {
				//Execute the search
				search();
			}			
		};
		goButton.addClickListener(clickListener);
		
		Grid quickTicketWidget = new Grid(2,2);
		quickTicketWidget.setWidget(0, 0, quickTicketLabel);
		quickTicketWidget.setWidget(1, 0, this.ticketEntry);
		quickTicketWidget.setWidget(1, 1, goButton);
		setWidget(quickTicketWidget);
		setStyleName("quick_ticket_widget");
	}

	private void search() {

		HistoryCommander.newItem(QuickTicket.getQuickTicketToken(ticketEntry.getText()));			
		ticketEntry.selectAll();
	}
}
