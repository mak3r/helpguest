/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.helpguest.gwt.protocol.LoginType;

/**
 * @author mabrams
 *
 */
public interface AuthenticationServiceAsync {

	public void authenticate(
			final String email, final String password,
			final LoginType type, final boolean doGenerateJnlp, 
			final AsyncCallback callback);

	public void authenticate(
			final String email, 
			final String password,
			final LoginType type, 
			final AsyncCallback callback);
	
	/**
	 * @param email the email address to be verified.
	 * @param uid the uid associated with this email address.
	 * @return the VerifyEmailOutcome.
	 */
	public void verifyEmail(
			final String email,
			final String uid,
			final AsyncCallback callback);
	
	/**
	 * prahalad adding 07/28/08
	 * @param email the email address to be verified.
	 * @return the VerifyEmailOutcome.
	 */
	public void verifyEmail(
			final String email,
			final AsyncCallback callback);

}
