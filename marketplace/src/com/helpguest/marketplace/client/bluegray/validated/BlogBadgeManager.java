package com.helpguest.marketplace.client.bluegray.validated;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.BlogBadgeService;
import com.helpguest.marketplace.client.BlogBadgeServiceAsync;
import com.helpguest.marketplace.client.bluegray.badge.BadgeMaker;
import com.helpguest.marketplace.client.bluegray.badge.CurrentBadgeSection;
import com.helpguest.marketplace.client.dto.BadgeDTO;
import com.helpguest.marketplace.client.event.CallbackSuccessHandler;
import com.helpguest.marketplace.client.event.CallbackSuccessEvent;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.SubCommandToken;
import com.helpguest.marketplace.client.util.DebugPanel;


public class BlogBadgeManager extends SimplePanel implements SubCommandToken, CallbackSuccessHandler<BadgeMaker> {

	protected static final String TOKEN = "BlogBadge";
	
	
	//Create the blog badge service proxy
	final BlogBadgeServiceAsync blogBadgeService = GWT.create(BlogBadgeService.class);
	
	//for setting the image of the currently selected icon
	SimplePanel badgeMakerPanel = new SimplePanel();
	SimplePanel webContentPanel = new SimplePanel();
	CurrentBadgeSection currentBadgeSection = new CurrentBadgeSection();

	
	public BlogBadgeManager() {

		//Specify the url where the blog badge service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.blogBadgeService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "BlogBadgeService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		init();
	}
	
	private void init() {
		
		currentBadgeSection.addCallbackSuccessHandler(this);
		
		VerticalPanel boundingBox = new VerticalPanel();
		boundingBox.add(currentBadgeSection);
		boundingBox.add(badgeMakerPanel);
		boundingBox.add(webContentPanel);
		boundingBox.addStyleName("blog_badge_boundingBox");

		add(boundingBox);
		addStyleName("blog_badge");
	}
	

	
	AsyncCallback<BadgeDTO> modifyBlogBadge = new AsyncCallback<BadgeDTO>() {
		public void onFailure(Throwable caught) {
			DebugPanel.setText(BlogBadgeManager.class, caught);
		}

		public void onSuccess(BadgeDTO result) {
		}
	
	};
	
	
	
	public void setParent(CommandToken parentCommand) {
		// TODO Auto-generated method stub
		
	}

	public String getBaseToken() {
		return TOKEN;
	}

	public void onEntry(Map<String, String> kvMap) {
		if (kvMap != null) {
			//no-op			
		}
	}

	public void onExit() {
		// TODO Auto-generated method stub
		
	}

	public void onCallbackSuccess(CallbackSuccessEvent<BadgeMaker> event) {
		if (event.getResult() == null) {
			badgeMakerPanel.clear();
			webContentPanel.clear();
		} else {
			badgeMakerPanel.setWidget(event.getResult());
			webContentPanel.setWidget(event.getResult().getWebContent());
		}
	}
}
