package com.helpguest.marketplace.client.bluegray.validated;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.brick.RightLeftBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.observer.ValidatedUser;
import com.helpguest.marketplace.client.observer.ValidatedUserListener;

public class ChangePassword extends SimplePanel implements ValidatedUserListener {

	private Label topLabel = new Label("Reset Password");  
	private ExpertDTO expertDTO;
	private static final String CURRENT_PASS_TEXT = "Current password:";
	private static final String NEW_PASS_TEXT = "New password:";
	private static final String CONFIRM_NEW_PASS_TEXT = "Confirm new password:";
	private final PasswordTextBox currentPass = new PasswordTextBox();
	private final RightLeftBrick currentPasswordBrick = new RightLeftBrick(CURRENT_PASS_TEXT, currentPass);
	private final PasswordTextBox newPass = new PasswordTextBox();
	private final RightLeftBrick newPasswordBrick = new RightLeftBrick(NEW_PASS_TEXT, newPass);
	private final PasswordTextBox confirmNewPass = new PasswordTextBox();
	private final RightLeftBrick confirmNewPassword = new RightLeftBrick(CONFIRM_NEW_PASS_TEXT, confirmNewPass);
	private final Button changeButton = new Button("Change Password");
	private final Button cancelButton = new Button("Cancel");
	private final RightLeftBrick modifyButton = new RightLeftBrick(changeButton, cancelButton);
	
	private SimplePanel parent;
	private Widget sibling;
	
	public ChangePassword() {
	}
	
	private void init() {
		VerticalPanel vp = new VerticalPanel();
		vp.add(topLabel);
		vp.add(currentPasswordBrick);
		vp.add(newPasswordBrick);
		vp.add(confirmNewPassword);
		vp.add(modifyButton);
		
		changeButton.addClickListener(new ClickListener() {

			public void onClick(Widget arg0) {
				Window.alert("This feature is not yet implemented.");
			}
			
		});
		
		cancelButton.addClickListener(new ClickListener() {

			public void onClick(Widget w) {
				parent.setWidget(sibling);   
			}
			
		});
		setWidget(vp);
	//Set Style Names
		topLabel.addStyleName("reset_password");
		vp.addStyleName("become_expert_request_password");
		currentPasswordBrick.setStyleName("current_password");
		newPasswordBrick.setStyleName("new_password");
		confirmNewPassword.setStyleName("confirm_password_1");
		modifyButton.addStyleName("buttons_submit");
				
	}
	
	public void onChange(ValidatedUser validatedUser) {
		expertDTO = validatedUser.getValidatedExpert();
		if (expertDTO != null) {
			init();
		}
	}

	public void swapWidget(final SimplePanel parent, final Widget sibling) {
		this.parent = parent;
		this.sibling = sibling;
		init();
	}
}
