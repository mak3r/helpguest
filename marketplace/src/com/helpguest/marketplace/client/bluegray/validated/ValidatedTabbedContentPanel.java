package com.helpguest.marketplace.client.bluegray.validated;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.SourcesTabEvents;
import com.google.gwt.user.client.ui.TabListener;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.bluegray.ui.FindExpert;
import com.helpguest.marketplace.client.bluegray.ui.TabbedContentPanel;
import com.helpguest.marketplace.client.history.DefaultToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.history.ProxySubCommandToken;
import com.helpguest.marketplace.client.history.SubCommandToken;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.observer.TabClickListener;
import com.helpguest.marketplace.client.observer.TabToken;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.widgets.TabFocusPanel;

public class ValidatedTabbedContentPanel extends SimplePanel implements AuthenticationListener, DefaultToken, TabListener, TabClickListener {



	private static ValidatedTabbedContentPanel tabbedContentPanel;
	private static Authenticator authenticator;
	
	private static final String TOKEN = "validated_tabs";
	
	//public static final String HOME_TOKEN = "tab_home";
	public static final String MY_ACCOUNT_TOKEN = ExpertProfile.TOKEN;
	public static final String SESSION_HISTORY_TOKEN = CompletedSessions.TOKEN;
	//public static final String VIEW_DEMO_TOKEN = "tab_view_demo";
	private static final String FIND_EXPERT_TOKEN = FindExpert.TOKEN;
	private static final String BLOG_BADGE_TOKEN = BlogBadgeManager.TOKEN;
	
	//public final Label home = new Label("HOME");
	public final Label myAccountLabel = new Label("MY ACCOUNT");
	public final Label sessionHistoryLabel = new Label("SESSION HISTORY");
	//public final Label view_demo = new Label("VIEW DEMO");
	private final Label findAnExpertLabel = new Label("FIND AN EXPERT");
	private final Label blogBadgeLabel = new Label("BLOG BADGE");
	
	//public final FocusPanel focusHome= new FocusPanel(home);
	public final FocusPanel focusMy_account= new FocusPanel(myAccountLabel);
	public final FocusPanel focusSession_history= new FocusPanel(sessionHistoryLabel);
	public final FocusPanel focusBlogBadge = new FocusPanel(blogBadgeLabel);
	//public final FocusPanel focusView_demo= new FocusPanel(view_demo);
	
	
	private int index;
	private TabPanel signInTabbedContent = new TabPanel();
	//private MyAccount myAccount = new MyAccount();
	private ExpertProfile expertProfile = new ExpertProfile();
	private CompletedSessions completedSessions = new CompletedSessions();
	private FindExpert findExpert = FindExpert.getEmptyInstance();
	private ProxySubCommandToken findExpertProxy = new ProxySubCommandToken();
	private BlogBadgeManager blogBadgeManager = new BlogBadgeManager();
	
	
	/**
	 * Define the default values for tab handling and entry routine.
	 */
	private static final String DEFAULT_PAGE_TOKEN = MY_ACCOUNT_TOKEN;
	private static int DEFAULT_TAB_INDEX;
	private Map<String, SubCommandToken> indexCommandMap = new HashMap<String, SubCommandToken>();
	private Map<String, String> subTokenIndexMap = new HashMap<String, String>();
	private Map<String, String> tabTokenMap = new HashMap<String, String>();
	private int currentTab = DEFAULT_TAB_INDEX;
	private int previousTab = DEFAULT_TAB_INDEX;
	
	private ValidatedTabbedContentPanel(Authenticator authWidget){
		try {
		authenticator = authWidget;
		authenticator.addAuthenticationListener(expertProfile);
		//and fire an on change event for myAccount now too
		expertProfile.onChange(authenticator);
		expertProfile.addValidatedUserListener(completedSessions);
		
		DebugPanel.setText("ValidatedTabbedContentPanel", "ctor(Authenticator) - about to init()");
		init(); 
		DebugPanel.setText("ValidatedTabbedContentPanel", "ctor(Authenticator) - done with init()");
		setStyleName("tabbed_content_panel");
		} catch (Throwable t) {
			GWT.getUncaughtExceptionHandler().onUncaughtException(t);
		}
		DebugPanel.setText("ValidatedTabbedContentPanel", "ctor(Authenticator) - leaving ctor");
	}
	
	private void init() {
		initTabs();
		signInTabbedContent.addTabListener(this);
		signInTabbedContent.selectTab(DEFAULT_TAB_INDEX);
		this.setWidget(signInTabbedContent);
		
	}
	
	//prahalad adding initTabs() 12/08/08
	private void initTabs() {
		//initialize generalTabbedContent 
		
		DEFAULT_TAB_INDEX = assignTab(expertProfile, myAccountLabel, MY_ACCOUNT_TOKEN);
		assignTab(completedSessions, sessionHistoryLabel, SESSION_HISTORY_TOKEN);
		findExpertProxy.setWidget(findExpert);
		assignTab(findExpertProxy, findAnExpertLabel, FIND_EXPERT_TOKEN);
		assignTab(blogBadgeManager, blogBadgeLabel, BLOG_BADGE_TOKEN);
		
		signInTabbedContent.setStyleName("tabbed_content_panel_signin_tabbed_content");
		signInTabbedContent.getTabBar().setStylePrimaryName("tabbed_content_panel-tabbed_signin_content-tabbed_bar");
		
				
		//Set stylename for each label
	    myAccountLabel.setStyleName("tabbed_content_panel_label_my_account");
		sessionHistoryLabel.setStyleName("tabbed_content_panel_label_session_history");
		findAnExpertLabel.setStyleName("tabbed_content_panel_label_find_an_expert");
		blogBadgeLabel.addStyleName("tabbed_content_panel_label_blog_badge");
		
	}
	
	/**
	 *
	 * @param command
	 * @param tabIndex
	 * @return return the tabIndex to which this command was set
	 */
	private int assignTab(final Widget widget, final Label tabLabel, final String tabToken) {
		//Create a focus panel to handle click events
		TabFocusPanel tcfp = new TabFocusPanel(tabLabel, tabToken);
		tcfp.addTabClickListener(this);
		//add the widget and the focus panel to the tab panel
		signInTabbedContent.add(widget, tcfp);
		
		//Populate mappings used to manage commands, and contents
		int widgetPlacmentIndex = signInTabbedContent.getWidgetCount() - 1;
		DebugPanel.setText("ValidatedTabbedContentPanel", "Added widget (" + tabLabel.getText() + ") at " + widgetPlacmentIndex);
		if (widget instanceof SubCommandToken) {
			setTabSelection((SubCommandToken)widget, widgetPlacmentIndex);
		} else {
			setTabSelection(tabToken, widgetPlacmentIndex);
		}
		
		//Return the index this widget was assigned in the tab panel
		return widgetPlacmentIndex;
	}
	
	private void setTabSelection(final SubCommandToken command, final int tabIndex) {	
		//associate an token with a command via a common index
		subTokenIndexMap.put(command.getBaseToken(), String.valueOf(tabIndex));
		indexCommandMap.put(String.valueOf(tabIndex), command);
		HistoryCommander.getInstance().register(command);
		setDefaultTabSelectionHistoryValue(command.getBaseToken());
	}
	
	/**
	 * Set the history token for a tab with the corresponding command.
	 * */
	private void setTabSelection(final String tabToken, final int tabIndex) {
		subTokenIndexMap.put(tabToken, String.valueOf(tabIndex));
		setDefaultTabSelectionHistoryValue(tabToken);
	}
	
	private void setDefaultTabSelectionHistoryValue(final String tabToken) {
		tabTokenMap.put(tabToken, HistoryCommander.buildToken(TOKEN, TabbedContentPanel.getTabKey(), tabToken));
	}
	
	
	/**
	 * Get the current instance.
	 * */
	public static ValidatedTabbedContentPanel getInstance(final Authenticator authWidget) {
		if (tabbedContentPanel == null) {
			tabbedContentPanel = new ValidatedTabbedContentPanel(authWidget);
		}
		return tabbedContentPanel;
	}
	
	/**
	 * Get the index of the tabbed panel.
	 * */
	public int getIndex(){
		return index;
		
	}
	
	/**
	 * Get the enclosed tab panel.
	 * */
	public TabPanel getTabPanel(){
		return signInTabbedContent;
	}
	

	public String getBaseToken() {
		return TOKEN;
	}

	public void onEntry(Map<String, String> kvMap) {
		// TODO Auto-generated method stub
		DebugPanel.setText("ValidatedTabbedContentPanel", "entering onEntry for command " + TOKEN);
		if (kvMap != null) {
			DebugPanel.setText("ValidatedTabbedContentPanel", "The map was not null");
			String tab = (String)kvMap.get(TabbedContentPanel.getTabKey());
			if (tab != null) {
				DebugPanel.setText("ValidatedTabbedContentPanel", "The tab was defined: " + tab);
				int tabIndex;
				try {
					tabIndex = Integer.parseInt(subTokenIndexMap.get(tab));
				} catch (Exception ex) {
					DebugPanel.setText("ValidatedTabbedContentPanel", "Unable to parse int value for key: " + tab);
					tabIndex = DEFAULT_TAB_INDEX;
				}
				
				//Only select the tab if it's not already selected.
				if (currentTab != tabIndex) {
					signInTabbedContent.selectTab(tabIndex);
				}
				
				//Now lets execute any entry methods necessary.
				String subCommandIndex = String.valueOf(tabIndex);
				if (kvMap != null && indexCommandMap.containsKey(subCommandIndex)) {
					DebugPanel.setText("ValidatedTabbedContentPanel", "Calling sub comand entry map");
					SubCommandToken subCommand = indexCommandMap.get(subCommandIndex);
					if (subCommand != null) {
						DebugPanel.setText("ValidatedTabbedContentPanel", "Sub command is not null");
						subCommand.onEntry(kvMap);
					}
					
					//Now we are going to update the click history cookie for this tab with the current kvMap entries
					tabTokenMap.put(tab, HistoryCommander.buildToken(TOKEN, kvMap));
					
					DebugPanel.setText("ValidatedTabbedContentPanel", "completed handling sub command in onEntry");
				} else {
					DebugPanel.setText("ValidatedTabbedContentPanel", "The kvMap is not null but there was no sub command to execute?");
				}
			} else {
				DebugPanel.setText("ValidatedTabbedContentPanel", "There was no tab value indicated, so using default tab");
				//we didnt get an index value.  use the default
				signInTabbedContent.selectTab(DEFAULT_TAB_INDEX);
			}
		} else {
			DebugPanel.setText("ValidatedTabbedContentPanel", "The map is null, so using default tab");
			//if no tab has been identified, then this is the default
			signInTabbedContent.selectTab(DEFAULT_TAB_INDEX);
		}
		
	}

	public void onExit() {
		// TODO Auto-generated method stub
		
	}

	private void memorizeTabSelection(final int tabIndex) {
		this.previousTab = currentTab;
		this.currentTab = tabIndex;
	}

	public static String getDefaultHistoryCommand() {		
		return HistoryCommander.buildToken(TOKEN, TabbedContentPanel.getTabKey(), String.valueOf(DEFAULT_PAGE_TOKEN));
	}
	
	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#getDefaultToken()
	 */
	public String getDefaultToken() {
		return getDefaultHistoryCommand();
	}

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#onBeforeTabSelected(com.google.gwt.user.client.ui.SourcesTabEvents, int)
	 */
	public boolean onBeforeTabSelected(SourcesTabEvents source, int tabIndex) {
		DebugPanel.setText("ValidatedTabbedContentPanel", "moving on to tab: " + tabIndex);
		//Return false to disable a tab
		return true;
	}

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#onTabSelected(com.google.gwt.user.client.ui.SourcesTabEvents, int)
	 */
	public void onTabSelected(SourcesTabEvents source, int tabIndex) {
		previousTab = currentTab;
		currentTab = tabIndex;
	}

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#onClick(com.helpguest.marketplace.client.observer.TabToken)
	 */
	public void onClick(TabToken tabCommand) {
		//Window.alert("ponder: " + CookieManager.ponder(tabCommand.getTabToken()));
		HistoryCommander.newItem(tabTokenMap.get(tabCommand.getTabToken()));
	}
	
	/**
	 * Call this method before making this object visible so it has an opportunity to reset itself.
	 * @return this instance of the ValidatedTabbedContentPanel
	 */
	public void onChange(final Authenticator authenticator) {
		if (authenticator.isLoggedIn()) {
			findExpert = FindExpert.getInstance();
		} else {
			findExpert = FindExpert.getEmptyInstance();
		}
		//now set the find expert into the correct widget
		findExpertProxy.setWidget(findExpert);
	}
	
}
