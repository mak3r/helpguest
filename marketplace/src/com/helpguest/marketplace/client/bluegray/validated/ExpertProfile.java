/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Sep 12, 2007
 */
package com.helpguest.marketplace.client.bluegray.validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.SessionService;
import com.helpguest.marketplace.client.SessionServiceAsync;
import com.helpguest.marketplace.client.brick.VerticalBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.SubCommandToken;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.observer.SourcesValidatedUser;
import com.helpguest.marketplace.client.observer.ValidatedUser;
import com.helpguest.marketplace.client.observer.ValidatedUserListener;
import com.helpguest.marketplace.client.validated.widgets.ExpertSessionBlock;
import com.helpguest.marketplace.client.validated.widgets.PersonalInfoBlock;
import com.helpguest.marketplace.client.validated.widgets.RateCalculatorBlock;
import com.helpguest.marketplace.client.widgets.ExpertTagsWidget;


/**
 * @author mabrams
 *
 */
public class ExpertProfile extends SimplePanel implements SubCommandToken, AuthenticationListener, ValidatedUser, SourcesValidatedUser {
	
	private Authenticator authSource;
	private List observerList = new ArrayList();
	private ExpertDTO expertDTO;
	private ExpertTagsWidget expertTagsWidget;
	protected static final String TOKEN = "expert_profile";
	
	private final ExpertSessionBlock expertSessionBlock = 
		new ExpertSessionBlock();
	private final RateCalculatorBlock rateCalculatorBlock =
		new RateCalculatorBlock();
	private final PersonalInfoBlock personalInfoBlock = 
		new PersonalInfoBlock();
	
	private VerticalPanel myAccountPanel;
	
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	//Create the session service proxy
	final SessionServiceAsync sessionService =
		(SessionServiceAsync) GWT.create(SessionService.class);
	
	
	
	public ExpertProfile() {
		super();
		
		//Initialize the ExpertService
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);
		
		//Initialize the SessionService
		ServiceDefTarget sessionEndpoint = (ServiceDefTarget) this.sessionService;
		String sessionModuleRelativeURL = GWT.getModuleBaseURL() + "SessionService";
		sessionEndpoint.setServiceEntryPoint(sessionModuleRelativeURL);
		
		

	}
	
	private void init() {
		
		FocusPanel resetPasswordFocus = new FocusPanel(new Label("Reset Password"));
		resetPasswordFocus.setStyleName("expert_profile_reset_password_focus");

		resetPasswordFocus.addClickListener(new ClickListener() {

			public void onClick(Widget w) {
				switchPanel(new ChangePassword());
			}
			
		});
		Hyperlink sessionMaintenance = new Hyperlink("Review Session History", "tab_session_history");
		sessionMaintenance.setStyleName("expert_profile_session_maintenance");
		HorizontalPanel maintenanceItems = new HorizontalPanel();
		maintenanceItems.add(resetPasswordFocus);
		maintenanceItems.add(sessionMaintenance);
		
		Label accountMaintenanceLabel = new Label("Account Maintenance");
		accountMaintenanceLabel.setStyleName("expert_profile_account_maintenance_label");
		VerticalBrick accountMaintenanceBrick = new VerticalBrick(accountMaintenanceLabel, maintenanceItems);	
		accountMaintenanceBrick.setStyleName("expert_profile_account_maintenance_brick");

		VerticalPanel ttBottom = new VerticalPanel();
		ttBottom.setStyleName("expert_profile_tt_bottom");
		Label tagsWidgetLabel = new Label("Update/Review Expertise");
		tagsWidgetLabel.setStyleName("expert_profile_tags_widget_label");
		ttBottom.add(tagsWidgetLabel);
		ttBottom.add(expertTagsWidget);

		myAccountPanel = new VerticalPanel();
		myAccountPanel.setStyleName("expert_profile_my_account_panel");
		myAccountPanel.add(expertSessionBlock);
		myAccountPanel.add(rateCalculatorBlock);
		personalInfoBlock.setStyleName("expert_profile_personal_info_block");
		myAccountPanel.add(personalInfoBlock);
		myAccountPanel.add(ttBottom);
		myAccountPanel.add(accountMaintenanceBrick);
		setWidget(myAccountPanel);
		setStyleName("expert_profile");
	}
	
	
	private void switchPanel(final ChangePassword cp) {
		cp.swapWidget(this, myAccountPanel);
		this.setWidget(cp);
	}
	
	AsyncCallback expertCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("MyAccount expertCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("MyAccount expertCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			updateDetails(expertDTO);
			notifyObservers();
		}		
	};
	
	public void updateDetails(final ExpertDTO expertDTO) {
		expertSessionBlock.setExpertDTO(expertDTO);
		rateCalculatorBlock.setExpertDTO(expertDTO);
		personalInfoBlock.setExpertDTO(expertDTO);
		expertTagsWidget = new ExpertTagsWidget(expertDTO);
		sessionService.getDemoSession(expertDTO, sessionCallback);	
		init();
	}
	
	
	AsyncCallback sessionCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			//no-op
		}

		public void onSuccess(Object result) {
			SessionDTO session = (SessionDTO) result;
			expertSessionBlock.setSessionDTO(session);
		}
		
	};
	
	AsyncCallback updateAccountCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("MyAccount updateAccountCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("MyAccount updateAccountCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			updateDetails(expertDTO);
			notifyObservers();
		}		
	};
	
	private final void notifyObservers() {
		for (int i = 0; i<observerList.size(); i++) {
			((ValidatedUserListener) observerList.get(i)).onChange(this);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String getBaseToken() {
		return TOKEN;
	}

	/**
	 * {@inheritDoc}
	 */
	public void onEntry(Map<String, String> kvMap) {
		//No need to check the kvMap for arguments
		if (authSource != null) {
			if (authSource.isLoggedIn() ) {
				//If the expert has not been initialized or
				// the expert does not match the currently authenticated party
				if (expertDTO == null || 
						!authSource.getAuthenticatedParty()
						.equals(expertDTO.getExpertAccount().getEmail())) {
					expertService.getExpert(authSource.getAuthenticatedParty(), expertCallback);
				} else { 
					//We need to init again in case the login had been expired
					updateDetails(expertDTO);
				}
			} else {
				expertDTO = null;
				setWidget(authSource.getExpiredWidget());
				notifyObservers();
				refreshTimer.cancel();
			}
		} else {
			setWidget(new Label("This page has expired."));
			refreshTimer.cancel();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void onExit() {
		// TODO Auto-generated method stub

	}

	public void onChange(Authenticator source) {
		this.authSource = source;
		onEntry(null);
	}

	public ExpertDTO getValidatedExpert() {
		return expertDTO;
	}

	public void addValidatedUserListener(
			ValidatedUserListener validatedUserListener) {
		observerList.add(validatedUserListener);		
	}

	public void removeValidatedUserListener(
			ValidatedUserListener validatedUserListener) {
		observerList.remove(validatedUserListener);		
	}
	
	//after 30 seconds, refresh the page.  we want to
	// clear the error and show connected icon
	Timer refreshTimer = new Timer() {

		public void run() {
			onEntry(null);
		}
		
	};



	public void setParent(CommandToken parentCommand) {
		//noop
	}

}
