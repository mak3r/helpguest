/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.marketplace.client.dto.BadgeDTO;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;

class BadgeListItem extends SimplePanel implements HasValueChangeHandlers<Boolean> {
	
	/**
	 * 
	 */
	private RadioButton badgeRadioButton = new RadioButton("badge_radio_button");
	private BadgeDTO badgeDTO;
	private HTML imageTag;
	
	private static MarketPlaceConstants marketPlaceConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String FULLY_QUALIFIED_DOMAIN_NAME = marketPlaceConstants.fullyQualifiedDomainName();
    private static String WEB_URI_PREFIX = marketPlaceConstants.webUriPrefix();
    String imageGeneratorURL = WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/hgservlets/DeployLaunchIcon?post_url=";

	public BadgeListItem(final BadgeDTO aBadge) {
		badgeDTO = aBadge;
		
		imageTag = new HTML("" +
				"<img " +
				"src=\"" + imageGeneratorURL + badgeDTO.getPostURL() + "&id=" + badgeDTO.getBaseImageRowId() + "\" " +
				"alt=\"HelpGuest\"/>");
		badgeRadioButton.setText(aBadge.getPostURL());
		badgeRadioButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				//false here because the click event has just been fired.
				setSelected(false);
			}
			
		});
		
		add(badgeRadioButton);
		addStyleName("badge_list_item");
	}
	
	public void updateText(final String text) {
		badgeRadioButton.setText(text);
	}
	
	protected void setSelected(boolean fireEvents) {
		badgeRadioButton.setValue(Boolean.TRUE, fireEvents);
		ValueChangeEvent.fire(this, true);
	}
	
	public BadgeDTO getBadgeDTO() {
		return badgeDTO;
	}
	
	public HTML getImageTag() {
		return imageTag;
	}

	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return addDomHandler(handler, ClickEvent.getType());
	}

	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Boolean> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

}