/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.dto.ExpertDTO;

class SubtextOption extends SimplePanel implements ValueChangeHandler<Boolean> {
	
	RadioButton subtextRadio = new RadioButton("subtext_option", "");
	SubtextOptionGroup sog;
	String text;
	String prefix;
	String suffix;
	ListBox expertiseList;
	SubtextType type;
	int groupId;
	
	enum SubtextType {
		PLAINTEXT,
		DROPDOWN,
		FILLIN;
	};
	
	enum StandardOption {
		
		HELP_NOW("Get Help Now!"),
		HELPGUEST_ME("HelpGuest Me."),
		ON_HELPGUEST("I'm on HelpGuest.");
		
		private String value;
		
		private StandardOption(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
	}
	
	public SubtextOption(final String prefix, final ExpertDTO expertDTO, final String Suffix, int groupId) {
		//TODO here we should implement a dropdown with the experts list of expertise
	}
	
	public SubtextOption(StandardOption option, int groupId) {
		text = option.getValue();
		this.groupId = groupId;
		//the type is PLAINTEXT
		type = SubtextType.PLAINTEXT;
		
		subtextRadio.addValueChangeHandler(this);
		subtextRadio.addValueChangeHandler(sog);
		
		subtextRadio.setText(text);
		
		add(subtextRadio);
		addStyleName("subtext");
	}
	
	public SubtextOption(int groupId) {
		this.groupId = groupId;
		//the type is PLAINTEXT
		type = SubtextType.FILLIN;
		
		subtextRadio.addValueChangeHandler(this);
		subtextRadio.addValueChangeHandler(sog);
		subtextRadio.setText("Write your own.");
		
		final TextBox subtextBox = new TextBox();
		subtextBox.setStyleName("subtext_box");
		subtextBox.addKeyPressHandler(new KeyPressHandler() {

			public void onKeyPress(KeyPressEvent event) {
				if (subtextBox.getText().length() > 32) {
					subtextRadio.setText(subtextBox.getText().substring(0, 32));
				} else if (event.getCharCode() == KeyCodes.KEY_BACKSPACE) {
					subtextRadio.setText(subtextBox.getText().substring(0, subtextBox.getText().length()-1));
				} else {
					subtextRadio.setText(subtextBox.getText() + event.getCharCode());
				}
				text = subtextBox.getText();
			}
			
		});
		
		subtextBox.addFocusHandler(new FocusHandler() {

			public void onFocus(FocusEvent event) {
				setValue(true);
			}
			
		});
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(subtextRadio);
		vp.add(subtextBox);
		
		add(vp);
		addStyleName("subtext");
	}
	
	
	public String getText() {
		return text;
	}
	
	public int getTypeId() {
		return type.ordinal();
	}
	
	public int getGroupId() {
		return groupId;
	}
	
	public void setEnabled(final boolean enabled) {
		subtextRadio.setEnabled(enabled);
	}

	public void onValueChange(ValueChangeEvent<Boolean> arg0) {
		sog.setSelected(groupId);
	}
	
	public void setValue(final boolean value) {
		//don't fire events
		subtextRadio.setValue(value, false);
	}

}