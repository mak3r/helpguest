/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

class CompellingWords extends SimplePanel {
	VerticalPanel vp = new VerticalPanel();
	private TagLineOption meOption = new TagLineOption("HelpGuest Me.");
	private TagLineOption onOption = new TagLineOption("I'm on HelpGuest");
	private TagLineOption nowOption = new TagLineOption("Get help now.");
	
	//FIXME
	//Each option must return the right value
	
	public CompellingWords() {
		vp.addStyleName("compelling_words_vp");
		vp.add(meOption);
		vp.add(onOption);
		vp.add(nowOption);
		
		add(vp);
		addStyleName("compelling_words");
	}
	
	public String getSelection() {
		return nowOption.getTagLine();
	}
	
	public void setEnabled(final boolean enabled) {
		meOption.setEnabled(enabled);
		onOption.setEnabled(enabled);
		nowOption.setEnabled(enabled);
	}
	
}