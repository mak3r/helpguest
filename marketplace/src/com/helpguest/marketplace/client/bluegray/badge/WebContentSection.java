/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.dto.BadgeDTO;
import com.helpguest.marketplace.client.widgets.SectionLabel;

class WebContentSection extends SimplePanel {
	VerticalPanel vp = new VerticalPanel();
	private static final String WEB_CONTENT_LABEL = "Web Content";
	private static final String WEB_CONTENT_DESC = "Copy the content below and paste it to your web page.";
	
	private TextArea contentArea = new TextArea();

	public WebContentSection(final BadgeDTO badgeDTO) {
		//FIXME  a new method is needed on badge dto for the real content
		contentArea.setText(badgeDTO.getRemoteURLContent());
		contentArea.setReadOnly(true);
		contentArea.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				contentArea.selectAll();
			}
			
		});
		
		vp.add(new SectionLabel(WEB_CONTENT_LABEL, WEB_CONTENT_DESC));
		vp.add(contentArea);
		vp.addStyleName("web_content_section_vp");
		
		add(vp);
		addStyleName("web_content_section");
	}
}