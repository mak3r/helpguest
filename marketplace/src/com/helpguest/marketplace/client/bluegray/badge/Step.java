/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

class Step extends SimplePanel {
	HorizontalPanel hp = new HorizontalPanel();
	SimplePanel stepImage = new SimplePanel();
	private String stepImageNum = "step_image_";
	private String completeStepImageNum = "completed_step_image_";
	private int stepNum;

	public Step(int stepNum, String instruction) {
		this.stepNum = stepNum;
		stepImage.addStyleName(stepImageNum + stepNum);
		stepImage.addStyleName("step_image");
		//add step image space
		hp.add(stepImage);
		//add the instruction
		Label instructionLabel = new Label(instruction);
		hp.add(instructionLabel);
		hp.setCellVerticalAlignment(instructionLabel, VerticalPanel.ALIGN_BOTTOM);
		hp.addStyleName("step_hp");
		
		add(hp);
		addStyleName("step");
	}
	
	public void completeStep() {
		stepImage.removeStyleName(stepImageNum + stepNum);
		stepImage.addStyleName(completeStepImageNum + stepNum);
	}
	
	public void reset() {
		stepImage.removeStyleName(completeStepImageNum + stepNum);			
		stepImage.addStyleName(stepImageNum + stepNum);
	}
}