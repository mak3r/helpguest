/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.bluegray.badge.SubtextOption.StandardOption;

class SubtextOptionGroup extends SimplePanel implements ValueChangeHandler<Boolean> {

	
	private SubtextOption one = new SubtextOption(StandardOption.HELP_NOW, 1);
	private SubtextOption two = new SubtextOption(StandardOption.HELPGUEST_ME, 2);
	private SubtextOption three = new SubtextOption(StandardOption.ON_HELPGUEST, 3);
	private SubtextOption four = new SubtextOption(4);
	
	private Step step;
	private int selectedOption;
	
	public SubtextOptionGroup(final Step step) {
		this.step = step;
		
		//start with the first item selected
		setSelected(1);
		
		VerticalPanel subtextOptions = new VerticalPanel();
		subtextOptions.add(one);
		subtextOptions.add(two);
		subtextOptions.add(three);
		subtextOptions.add(four);
		subtextOptions.addStyleName("section_content");
		
		add(subtextOptions);
	}
	
	public void setEnabled(final boolean enabled) {
		one.setEnabled(enabled);
		two.setEnabled(enabled);
		three.setEnabled(enabled);
		four.setEnabled(enabled);
	}
	
	public void setSelected(final int selected) {
		this.selectedOption = selected;
		switch (selectedOption) {
		case 1:
			one.setValue(true);
			break;
		case 2:
			two.setValue(true);
			break;
		case 3:
			three.setValue(true);
			break;
		case 4:
			four.setValue(true);
			break;
		default:
			one.setValue(true);
			break;
		}
	}
	
	public int getSelectedOption() {
		return selectedOption;
	}
	
	public String getSelectedText() {
		String text;
		switch(selectedOption) {
		case 1:
			text = one.getText();
			break;
		case 2:
			text = two.getText();
			break;
		case 3:
			text = three.getText();
			break;
		case 4:
			text = four.getText();
			break;
		default:
			text = "";
			break;
		}
		return text;
	}
	
	
	public void onValueChange(ValueChangeEvent<Boolean> arg0) {
		step.completeStep();
	}
	
}