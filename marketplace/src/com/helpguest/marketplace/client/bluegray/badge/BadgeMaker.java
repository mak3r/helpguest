/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.BlogBadgeService;
import com.helpguest.marketplace.client.BlogBadgeServiceAsync;
import com.helpguest.marketplace.client.bluegray.validated.BlogBadgeManager;
import com.helpguest.marketplace.client.dto.BadgeDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.event.CallbackSuccessEvent;
import com.helpguest.marketplace.client.event.CallbackSuccessHandler;
import com.helpguest.marketplace.client.event.HasCallbackSuccessHandlers;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.widgets.SectionLabel;

public class BadgeMaker extends Composite implements HasCallbackSuccessHandlers<BadgeDTO> {
		
	//Create the blog badge service proxy
	final BlogBadgeServiceAsync blogBadgeService = GWT.create(BlogBadgeService.class);

	private Step stepOne = new Step(1, "Choose the logo for your badge.");
	private Step stepTwo = new Step(2, "Add subtext. [max 32 chars]");
	private Step stepThree = new Step(3, "Tell us the URL where this badge will be located.");
	private Step stepFour = new Step(4, "Name your badge (alpha-numeric only, no spaces [max 16 chars]).");
	private Step stepFive = new Step(5, "Specify how much users will be charged. (optional).");
	
	VerticalPanel vp = new VerticalPanel();
	private static final String MAKE_IT_LABEL = "Blog Badge Editor";
	private static final String MAKE_IT_DESC = "Orange numbered steps are incomplete.  Blue numbered steps are complete.";
	private TextBox nameDetails = new TextBox();
	private TextBox feeDetails = new TextBox();
	private TextBox urlDetails = new TextBox();
	private BadgeImageOptionGroup badgeImageOptionGroup = new BadgeImageOptionGroup(stepOne);
	private SubtextOptionGroup subtextOptionGroup = new SubtextOptionGroup(stepTwo);
	private String allowableNameChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789";
	private final Button createIt = new Button("Create It");
	private WebContentSection webContent;
	private BadgeDTO badgeMakerBadge;
	
	public BadgeMaker(final BadgeDTO badgeDTO) {
		badgeMakerBadge = badgeDTO;
		webContent = new WebContentSection(badgeMakerBadge);
		
		//Specify the url where the blog badge service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.blogBadgeService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "BlogBadgeService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		init();
	}
	
	private void init() {
		update();
							
		createIt.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
//					if (isValid()) {
					updateBadgeDTO();
					//FIXME this should be a real expert dto
					blogBadgeService.createBadge(new ExpertDTO(), badgeMakerBadge, createBlogBadge);
					createIt.setEnabled(false);
//					}
			}
			
		});
		
		nameDetails.addKeyPressHandler(new KeyPressHandler() {

			public void onKeyPress(KeyPressEvent evt) {
				if (evt.getCharCode() == KeyCodes.KEY_TAB) {
					isValidNameDetails();
				}else if (allowableNameChars.indexOf(evt.getCharCode()) == -1) {
					nameDetails.setText(nameDetails.getText().substring(0, nameDetails.getText().length()));
				}
			}
			
		});
	
		
		createIt.addStyleName("badge_maker_section_create_it");
		urlDetails.addStyleName("badge_maker_section_url_details");
		nameDetails.addStyleName("badge_maker_section_name_details");
		feeDetails.addStyleName("badge_maker_section_fee_details");
		
		vp.add(new SectionLabel(MAKE_IT_LABEL, MAKE_IT_DESC));
		vp.add(stepOne);
		vp.add(badgeImageOptionGroup);
		vp.add(stepTwo);
		vp.add(subtextOptionGroup);
		vp.add(stepThree);
		vp.add(urlDetails);
		vp.add(stepFour);
		vp.add(nameDetails);
		vp.add(stepFive);
		vp.add(feeDetails);
		vp.add(createIt);
		vp.addStyleName("badge_maker_section_vp");
		
		initWidget(vp);
		addStyleName("badge_maker_section");
	}
	
	public WebContentSection getWebContent() {
		return webContent;
	}
	
	public void resetSteps() {
		stepOne.reset();
		stepTwo.reset();
		stepThree.reset();
		stepFour.reset();
		stepFive.reset();
	}

	public void completeAllSteps() {
		stepOne.completeStep();
		stepTwo.completeStep();
		stepThree.completeStep();
		stepFour.completeStep();
		stepFive.completeStep();
	}
	
	public void enable(final boolean enabled) {
		if (!enabled) {
			completeAllSteps();
		} else {
			resetSteps();
		}
		badgeImageOptionGroup.setEnabled(enabled);
		subtextOptionGroup.setEnabled(enabled);
		urlDetails.setEnabled(enabled);
		nameDetails.setEnabled(enabled);
		feeDetails.setEnabled(enabled);
		createIt.setEnabled(enabled);
	}
	
	private void update() {
		vp.clear();
		badgeImageOptionGroup.setSelectedImage(badgeMakerBadge.getBaseImageRowId());
		subtextOptionGroup.setSelected(badgeMakerBadge.getSubtextGroupId());
		urlDetails.setText(badgeMakerBadge.getPostURL());
		nameDetails.setText(badgeMakerBadge.getName());
		if (badgeMakerBadge.getAmount() > 0) {
			feeDetails.setText(String.valueOf(badgeMakerBadge.getAmount()));				
		}
	}
	
	private void updateBadgeDTO() {
		badgeMakerBadge.setAmount(Float.valueOf(feeDetails.getText()));
		badgeMakerBadge.setBaseImageRowId(badgeImageOptionGroup.getSelectedImage());
		badgeMakerBadge.setName(nameDetails.getText());
		badgeMakerBadge.setPostURL(urlDetails.getText());
		badgeMakerBadge.setSubText(subtextOptionGroup.getSelectedText());
	}
	
	private boolean isValidUrlDetails() {
		boolean outcome = true;
		if (!urlDetails.getText().trim().startsWith("http")) {
			stepThree.reset();
			outcome |= false;
		} else {
			stepThree.completeStep();
		}
		return outcome;
	}
	
	private boolean isValidNameDetails() {
		boolean outcome = true;
		if (nameDetails.getText().trim().length() > 16) {
			stepFour.reset();
			outcome |= false;
		} else {
			stepFour.completeStep();
		}
		return outcome;
	}
	
	private boolean isValid() {
		boolean outcome = true;
		outcome |= isValidUrlDetails();
		outcome |= isValidNameDetails();
		if (nameDetails.getText().trim().length() > 16) {
			stepFour.reset();
			outcome |= false;
		}
		if (feeDetails.getText() != null || !"".equals(feeDetails.getText().trim())) {
			try {
				Float.valueOf(feeDetails.getText());
			} catch (Exception ex) {
				stepFive.reset();
				outcome |= false;
			}
		}		
		
		return outcome;
	}
	
	AsyncCallback<BadgeDTO> createBlogBadge = new AsyncCallback<BadgeDTO>() {
		public void onFailure(Throwable caught) {
			DebugPanel.setText(BlogBadgeManager.class, caught);
		}

		public void onSuccess(BadgeDTO result) {
			fireEvent(result);
		}
	
	};

	   
	private void fireEvent(BadgeDTO result) {
		CallbackSuccessEvent.fire(this, result);
	}


	public HandlerRegistration addCallbackSuccessHandler(final CallbackSuccessHandler<BadgeDTO> handler) {
		return addHandler(handler, CallbackSuccessEvent.getType() );
	}

	
}