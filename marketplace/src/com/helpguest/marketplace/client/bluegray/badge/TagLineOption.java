/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;

class TagLineOption extends SimplePanel {
	HorizontalPanel hp = new HorizontalPanel();
	String words;
	private RadioButton radioButton;
	
	//TODO make this smarter to handle dynamic lists embeded in the words
	public TagLineOption(String words) {
		this.words = words;
		radioButton = new RadioButton("tag_line_option_radio", words);
		hp.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
		radioButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				//stepTwo.completeStep();
			}
			
		});

		hp.add(radioButton);
		hp.add(new Label(words));			
		hp.addStyleName("tag_line_option_hp");
					
		add(hp);
		addStyleName("section_content");
	}
	
	public String getTagLine() {
		return words;
	}
	
	public void setEnabled(final boolean enabled) {
		radioButton.setEnabled(enabled);
	}

}