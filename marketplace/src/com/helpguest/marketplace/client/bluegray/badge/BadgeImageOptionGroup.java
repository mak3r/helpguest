/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.marketplace.client.ui.VisualCuesImageBundle;

class BadgeImageOptionGroup extends SimplePanel implements ValueChangeHandler<Boolean> {

	private VisualCuesImageBundle visualCues = (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);
	private BadgeImageOption badgeBlue = new BadgeImageOption(visualCues.badgeBlue().createImage(), 1, this);
	private BadgeImageOption badgeDkGrey = new BadgeImageOption(visualCues.badgeDkGrey().createImage(), 2, this);
	private BadgeImageOption badgeDkGreyWOrange = new BadgeImageOption(visualCues.badgeDkGreyWOrange().createImage(), 3, this);
	private BadgeImageOption badgeLtGrey = new BadgeImageOption(visualCues.badgeLtGrey().createImage(), 4, this);
	private BadgeImageOption badgeOrange = new BadgeImageOption(visualCues.badgeOrange().createImage(), 5, this);
	private Step step;
	private int selectedImage;
	
	public BadgeImageOptionGroup(final Step step) {
		this.step = step;
		
		HorizontalPanel badgeImageOptions = new HorizontalPanel();
		badgeImageOptions.add(badgeDkGrey);
		badgeImageOptions.add(badgeBlue);
		badgeImageOptions.add(badgeLtGrey);
		badgeImageOptions.add(badgeDkGreyWOrange);
		badgeImageOptions.add(badgeOrange);
		badgeImageOptions.addStyleName("section_content");
		
		add(badgeImageOptions);
	}
	
	public void setEnabled(final boolean enabled) {
		badgeBlue.setEnabled(enabled);
		badgeDkGrey.setEnabled(enabled);
		badgeDkGreyWOrange.setEnabled(enabled);
		badgeLtGrey.setEnabled(enabled);
		badgeOrange.setEnabled(enabled);
	}
	
	public void setSelectedImage(final int selected) {
		this.selectedImage = selected;
		switch (selectedImage) {
		case 1:
			badgeBlue.setValue(true);
			break;
		case 2:
			badgeDkGrey.setValue(true);
			break;
		case 3:
			badgeDkGreyWOrange.setValue(true);
			break;
		case 4:
			badgeLtGrey.setValue(true);
			break;
		case 5:
			badgeOrange.setValue(true);
			break;
		default:
			break;
		}
	}
	
	public int getSelectedImage() {
		return selectedImage;
	}
	
	
	public void onValueChange(ValueChangeEvent<Boolean> arg0) {
		step.completeStep();
	}
	
}