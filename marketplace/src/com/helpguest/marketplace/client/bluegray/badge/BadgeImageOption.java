/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

class BadgeImageOption extends SimplePanel implements ValueChangeHandler<Boolean> {
	
	VerticalPanel vp = new VerticalPanel();
	RadioButton badgeOptionButton = new RadioButton("badge_image_option", "");
	Image image;
	BadgeImageOptionGroup biog;
	int badgeRowId;
	
	/**
	 * The badge row id is actually the row id in the databaase.  there has to be
	 * abetter way to do this.
	 * @param image
	 * @param badgeRowId
	 */
	public BadgeImageOption(final Image image, final int badgeRowId, final BadgeImageOptionGroup biog) {
		this.image = image;
		this.badgeRowId = badgeRowId;
		this.biog = biog;
		
		badgeOptionButton.addValueChangeHandler(this);
		badgeOptionButton.addValueChangeHandler(biog);
		
		image.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent arg0) {
				if (badgeOptionButton.isEnabled()) {
					badgeOptionButton.setValue(Boolean.TRUE, true);					
				}
			}				

		});
		
		vp.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		vp.add(image);
		vp.add(badgeOptionButton);
		
		vp.addStyleName("badge_option_vp");
		
		add(vp);
		addStyleName("badge_option");
	}
	
	public String getName() {
		if (image == null) {
			return "";
		}
		return image.getTitle();
	}
	
	public void setEnabled(final boolean enabled) {
		badgeOptionButton.setEnabled(enabled);
	}

	public void onValueChange(ValueChangeEvent<Boolean> arg0) {
		biog.setSelectedImage(badgeRowId);
	}
	
	public void setValue(final boolean value) {
		//don't fire events
		badgeOptionButton.setValue(value, false);
	}

}