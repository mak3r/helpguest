/**
 * 
 */
package com.helpguest.marketplace.client.bluegray.badge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.BlogBadgeService;
import com.helpguest.marketplace.client.BlogBadgeServiceAsync;
import com.helpguest.marketplace.client.bluegray.validated.BlogBadgeManager;
import com.helpguest.marketplace.client.dto.BadgeDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.event.CallbackSuccessEvent;
import com.helpguest.marketplace.client.event.CallbackSuccessHandler;
import com.helpguest.marketplace.client.event.HasCallbackSuccessHandlers;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.widgets.SectionLabel;

public class CurrentBadgeSection extends Composite implements HasValueChangeHandlers<Boolean>, 
	CallbackSuccessHandler<BadgeDTO>, HasCallbackSuccessHandlers<BadgeMaker> {
	
	//Create the blog badge service proxy
	final BlogBadgeServiceAsync blogBadgeService = GWT.create(BlogBadgeService.class);

	private static final String CURRENT_LABEL = "Current Badges";
	private static final String CURRENT_DESC = "Select the badge url to view or modify the details. " +
			"Click the new button to create a new badge.";
	
	Map<BadgeListItem, BadgeMaker> badgeMap = new HashMap<BadgeListItem, BadgeMaker>();
	
	HorizontalPanel hp = new HorizontalPanel();
	VerticalPanel vp = new VerticalPanel();
	Button newButton = new Button("New");
	Button deleteButton = new Button("Delete");
	private VerticalPanel badgeListPanel = new VerticalPanel();
	private BadgeListItem selectedBadgeListItem = null;
	SimplePanel currentBadgeIconPanel = new SimplePanel();
	
	public CurrentBadgeSection() {
		
		//Specify the url where the blog badge service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.blogBadgeService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "BlogBadgeService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		//initialize the badge list
		blogBadgeService.getAllBadges(new ExpertDTO(), getBadgeList);
		
		HorizontalPanel badges = new HorizontalPanel();			
		badges.addStyleName("current_badge_section_buttons");

		badgeListPanel.addStyleName("badge_list_panel");
		
		HorizontalPanel buttons = new HorizontalPanel();
		newButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				BadgeDTO currentBadge = new BadgeDTO();
				//FIXME there is a better way to do this than reseting the post url
				// consider parameterizing the BadgeListItem ctor.
				currentBadge.setPostURL("IN PROGRESS");
				BadgeListItem badgeListItem = new BadgeListItem(currentBadge);
				currentBadge.setPostURL("http://");
				badgeListPanel.add(badgeListItem);
				BadgeMaker badgeMaker = new BadgeMaker(currentBadge);
				fireBadgeMakerChangeEvent(badgeMaker);
				badgeMap.put(badgeListItem, badgeMaker);
				badgeListItem.setSelected(true);
				badgeMaker.enable(true);
			}
			
		});
		
		deleteButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				blogBadgeService.deleteBadge(
						new ExpertDTO(), 
						selectedBadgeListItem.getBadgeDTO(), 
						deleteBlogBadge);
			}
			
		});
		
		buttons.add(newButton);
		buttons.add(deleteButton);
		buttons.addStyleName("current_badge_section_buttons");
		
		VerticalPanel leftSide = new VerticalPanel();
		leftSide.add(badgeListPanel);
		leftSide.add(badges);
		leftSide.add(buttons);
		leftSide.addStyleName("current_badge_section_left_side");
		
		hp.add(leftSide);
		hp.add(currentBadgeIconPanel);
		hp.addStyleName("current_badge_section_hp");

		vp.add(new SectionLabel(CURRENT_LABEL, CURRENT_DESC));
		vp.add(hp);
		vp.addStyleName("current_badge_section_vp");
		
		initWidget(vp);
		addStyleName("current_badge_section");
	}

	
	public void add(BadgeListItem item) {
		badgeListPanel.add(item);
	}
	
	public void remove(BadgeListItem item) {
		int nextSelection = 0;
		if (item == selectedBadgeListItem) {
			int itemIndex = badgeListPanel.getWidgetIndex(item);
			if (itemIndex > 0) {
				//set the selected item as the next one in the list
				nextSelection = itemIndex-1;						
			}
		}
		//I don't believe this is necessary as it will automatically get removed when the new one is added in
		//badgeMakerPanel.remove(badgeMap.get(item));
		badgeMap.remove(item);
		badgeListPanel.remove(item);
		
		//Let's select a different item
		((BadgeListItem)badgeListPanel.getWidget(nextSelection)).setSelected(true);
	}
	
	public BadgeListItem getSelectedBadgeListItem() {
		return selectedBadgeListItem;
	}
	
	public BadgeMaker getSelectedBadgeMaker() {
		return badgeMap.get(selectedBadgeListItem);
	}
	
	private void clearBadgeList() {
		badgeListPanel.clear();
	}

	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Boolean> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	public void onCallbackSuccess(CallbackSuccessEvent<BadgeDTO> event) {
		remove(selectedBadgeListItem);
		BadgeListItem updatedItem = new BadgeListItem(event.getResult());
		BadgeMaker createdBadge = new BadgeMaker(event.getResult());
		fireBadgeMakerChangeEvent(createdBadge);
		badgeMap.put(updatedItem, createdBadge);
		add(updatedItem);
		updatedItem.setSelected(true);
	}
	
	AsyncCallback<BadgeDTO> deleteBlogBadge = new AsyncCallback<BadgeDTO>() {
		public void onFailure(Throwable caught) {
			DebugPanel.setText(BlogBadgeManager.class, caught);
		}

		public void onSuccess(BadgeDTO result) {
			remove(selectedBadgeListItem);
			fireDeleteEvent(result);
		}
	
	};
	
	   
	private void fireDeleteEvent(BadgeDTO result) {
		CallbackSuccessEvent.fire(this, null);
	}
	
	AsyncCallback<List<BadgeDTO>> getBadgeList = new AsyncCallback<List<BadgeDTO>>() {
		public void onFailure(Throwable caught) {
			DebugPanel.setText(BlogBadgeManager.class, caught);
		}

		public void onSuccess(List<BadgeDTO> result) {
			
			clearBadgeList();
			badgeMap.clear();
			for (BadgeDTO next : result) {
				final BadgeListItem anotherItem = new BadgeListItem(next);
				anotherItem.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
					public void onValueChange(ValueChangeEvent<Boolean> arg0) {
						selectedBadgeListItem = anotherItem;
						currentBadgeIconPanel.setWidget(anotherItem.getImageTag());
						BadgeMaker badgeMaker = badgeMap.get(anotherItem);
						badgeMaker.enable(false);
						fireBadgeMakerChangeEvent(badgeMaker);
					}
				});
				
				badgeMap.put(anotherItem, new BadgeMaker(next));
				if (badgeMap.size() == 1) {
					//fire events when programmatically selecting.
					anotherItem.setSelected(true);
				}
				add(anotherItem);				
			}
			fireBadgeMakerChangeEvent(badgeMap.get(selectedBadgeListItem));

		}
	
	};

	private void fireBadgeMakerChangeEvent(BadgeMaker badgeMaker) {
		CallbackSuccessEvent.fire(this, badgeMaker);
	}


	public HandlerRegistration addCallbackSuccessHandler(CallbackSuccessHandler<BadgeMaker> handler) {
		// TODO Auto-generated method stub
		return addHandler(handler, CallbackSuccessEvent.getType());
	}
	   

}