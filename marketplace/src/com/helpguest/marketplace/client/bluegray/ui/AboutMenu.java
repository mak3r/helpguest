package com.helpguest.marketplace.client.bluegray.ui;
/**
 * @author ying
 * */

//import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.marketplace.client.ui.HorizontalSpacer;


public class AboutMenu extends SimplePanel{
	public final static String BASICS = "basics";
	public final static String HISTORY = "history";
	public final static String TECHNOLOGY = "technology";
	public final static String TEAM = "team";
	
	public final static int BASICS_INDEX = 0;
	public final static int HISTORY_INDEX = 1;
	public final static int TECHNOLOGY_INDEX = 2;
	public final static int TEAM_INDEX = 3;
	
	
	/**
	 * Define hyperlinks
	 * */
    final Hyperlink basics = new Hyperlink(
    		"Basics", 
    		PublicTabPanel.getHistoryCommandForTab(AboutPanel.TOKEN, AboutPanel.CONTENT_KEY, BASICS));
	final Hyperlink history = new Hyperlink(
			"History",
			PublicTabPanel.getHistoryCommandForTab(AboutPanel.TOKEN, AboutPanel.CONTENT_KEY, HISTORY));
	final Hyperlink technology = new Hyperlink(
			"Technology", 
			PublicTabPanel.getHistoryCommandForTab(AboutPanel.TOKEN, AboutPanel.CONTENT_KEY, TECHNOLOGY));
	final Hyperlink team = new Hyperlink(
			"Team", 
			PublicTabPanel.getHistoryCommandForTab(AboutPanel.TOKEN, AboutPanel.CONTENT_KEY, TEAM));


	private static Map hyperlinkMap = new HashMap();
	
	SimplePanel dropPanel = null;
    TabbedContentPanel parent;
	public AboutMenu(final SimplePanel dropPanel){
		this.dropPanel = dropPanel;
		initmenu();
		addAvailableLinks();
		setStyleName("about_menu");
	}
	
	private void initmenu(){
		HorizontalPanel menu = new HorizontalPanel();
		menu.add(basics);
		menu.add(new HorizontalSpacer(30));
		menu.add(history);
		menu.add(new HorizontalSpacer(30));
		menu.add(technology);
		menu.add(new HorizontalSpacer(30));
		menu.add(team);
		this.add(menu);
		menu.setStyleName("about_menu_menu");
	}
	/**
	 * Map the tokens to the hyperlink
	 * */
	private void addAvailableLinks() {
		hyperlinkMap.put(BASICS, basics);
		hyperlinkMap.put(HISTORY, history);
		hyperlinkMap.put(TECHNOLOGY,technology);
		hyperlinkMap.put(TEAM,team);
	}
	
	/**
	 * Get the value from the token
	 * */
	public static Hyperlink getHyperlink(final String linkName) {
		return (Hyperlink) hyperlinkMap.get(linkName);
	}
}
	