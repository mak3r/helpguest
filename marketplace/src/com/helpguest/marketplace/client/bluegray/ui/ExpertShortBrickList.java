package com.helpguest.marketplace.client.bluegray.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.dto.ExpertDTO;

public class ExpertShortBrickList extends SimplePanel {
	private final Label label = new Label("Featured Experts");
	private final static int MAX = 4;
	private VerticalPanel baseVP = new VerticalPanel();
	private VerticalPanel expertList = new VerticalPanel();
	private int num;
	static final private String queryId = "0";
	public ExpertShortBrickList(){
		num = 0;
		baseVP.add(label);
		baseVP.add(expertList);
		setWidget(baseVP);
		//setup the style name
		label.setStyleName("expert_short_brick_list_label");
		baseVP.setStyleName("expert_short_brick_list_basevp");
		expertList.setStyleName("expert_short_brick_list_expert_list");
	}
	
	/**
	 * Add the expert dto according to the position in the expert uid list.
	 * */
	public void addExpertShortBrick(ExpertDTO expertDTO) {
		if(num < MAX) {
			expertList.add(new ExpertShortBrick(expertDTO, queryId, num));
			num++;
		}
	}
}
