package com.helpguest.marketplace.client.bluegray.ui;



import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.brick.InputBrick;
import com.helpguest.marketplace.client.widgets.ExpertTagsWidget;

public class SignUpPanel2 extends SimplePanel{
	private VerticalPanel vp = new VerticalPanel();
	private VerticalPanel top = new VerticalPanel();
	private VerticalPanel mid = new VerticalPanel();
	private VerticalPanel bottom = new VerticalPanel();
	private Label topLabel = new Label("EXPERT SIGNUP HERE");
	private Label midLabel = new Label("ACCOUNT INFORMATION");
	private Label bottomLabel = new Label("EXPERTISE CLASSFICATION");
	private Label bottomDescription = new Label("Check all categories below which apply to your abilities");
	private InputBrick email = new InputBrick("Your Email Address","15em");
	private InputBrick userName = new InputBrick("UserName");
	public InputBrick password = new InputBrick("Password","10em", InputBrick.PASSWORD_TYPE);
	public InputBrick cpassword = new InputBrick("Confirm Password","10em", InputBrick.PASSWORD_TYPE);
	private InputBrick zipcode = new InputBrick("Zip Code","5em");
    private HorizontalPanel source = new HorizontalPanel();
    private Label sourceLabel = new Label("Where did you find out about HelpGuest?");
    private ListBox sourceList = new ListBox();
    private ExpertTagsWidget expertTags = new ExpertTagsWidget(null);
	public SignUpPanel2(){
		HTML description1 = new HTML("<p>The experts are independent,professionals who have an advanced skill they have chosen to list on the HelpGuest marketplace.</p>");
	    HTML description2 = new HTML("<p>Own rates and work from their own computers on their own schedules. After a help session is completed, the user rates the expert.</p>");
		sourceList.addItem("Google");
	    sourceList.addItem("Friends");
	    top.add(topLabel);
	    top.add(description1);
	    top.add(description2);
	    mid.add(midLabel);
	    mid.add(email);
	    mid.add(userName);
	    mid.add(password);
	    mid.add(cpassword);
	    mid.add(zipcode);
	    source.add(sourceLabel);
	    source.setCellHorizontalAlignment(sourceLabel,HorizontalPanel.ALIGN_RIGHT );
	    source.add(sourceList);
	    source.setCellHorizontalAlignment(sourceList,HorizontalPanel.ALIGN_LEFT );
	    mid.add(source);
	    bottom.add(bottomLabel);
	    bottom.add(bottomDescription);
	    bottom.add(expertTags);
	    vp.add(top);
	    vp.add(mid);
	    vp.add(bottom);
	    setWidget(vp);
	    
	    //set up styleName
	    vp.setStyleName("sign_up_panel2_vp");
	    top.setStyleName("sign_up_panel2_top");
	    mid.setStyleName("sign_up_panel2_mid");
	    bottom.setStyleName("sign_up_panel2_bottom");
	    topLabel.setStyleName("sign_up_panel2_topLabel");
	    midLabel.setStyleName("sign_up_panel2_midLabel");
	    bottomLabel.setStyleName("sign_up_panel2_bottomLabel");
	    sourceLabel.setStyleName("sign_up_panel2_source_label");
	    sourceList.setStyleName("sign_up_panel2_source_list");
	    bottomDescription.setStyleName("sign_up_panel2_bottom_description");
	    description1.setStyleName("sign_up_panel2_description1");
	    description2.setStyleName("sign_up_panel2_description2");
	    email.setStyleName("sign_up_panel2_email");
	    userName.setStyleName("sign_up_panel2_username");
	    password.setStyleName("sign_up_panel2_password");
	    cpassword.setStyleName("sign_up_panel2_cpassword");
	    zipcode.setStyleName("sign_up_panel2_zip_code");
	    source.setStyleName("sign_up_panel2_source");
	}
	
}
