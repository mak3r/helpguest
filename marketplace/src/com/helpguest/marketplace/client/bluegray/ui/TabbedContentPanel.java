package com.helpguest.marketplace.client.bluegray.ui;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.marketplace.client.bluegray.validated.ValidatedTabbedContentPanel;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.DefaultToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.util.DebugPanel;

public class TabbedContentPanel extends SimplePanel implements AuthenticationListener, DefaultToken, CommandToken {


	//private static String TOKEN = "tab_panel";
	private static ValidatedTabbedContentPanel validatedTabPanel;
	private static PublicTabPanel publicTabPanel = PublicTabPanel.getInstance();
	private static DefaultToken defaultToken = publicTabPanel;
	private Authenticator authenticator;
	protected static String TAB_KEY = "tab";
	
	public TabbedContentPanel(Authenticator authenticator) {
		this.authenticator = authenticator;
		DebugPanel.setText("TabbedContentPanel", "ctor - registering publicTabPanel" );
		authenticator.addAuthenticationListener(publicTabPanel);
		HistoryCommander.getInstance().register(publicTabPanel);
		DebugPanel.setText("TabbedContentPanel", "ctor - creating validatedTabPanel" );
		validatedTabPanel = ValidatedTabbedContentPanel.getInstance(this.authenticator);
		authenticator.addAuthenticationListener(validatedTabPanel);
		DebugPanel.setText("TabbedContentPanel", "ctor - registering validatedTabPanel" );
		HistoryCommander.getInstance().register(validatedTabPanel);
		DebugPanel.setText("TabbedContentPanel", "ctor - deciding which panel to use" );
		if (authenticator.isLoggedIn()) {
			DebugPanel.setText("TabbedContentPanel", "using validated tab panel.");
			setWidget(validatedTabPanel);
		} else {
			DebugPanel.setText("TabbedContentPanel", "using public tab panel.");
			setWidget(publicTabPanel);
		}
	}
	
	public static String getHistoryCommandForTab(final String tabTokenValue, final String key, final String value) {
		Map kvMap = new HashMap();
		kvMap.put(key, value);
		return getHistoryCommandForTab(tabTokenValue, kvMap);
	}

	/**
	 * This is the same as calling {@link #getHistoryCommandForTab(String, Map, boolean)}
	 * @param tabTokenValue is the tab token value.
	 * @param kvMap is a map of the parameters to pass into the history command token.
	 * @return
	 */
	public static String getHistoryCommandForTab(final String tabTokenValue, final Map kvMap) {
		return getHistoryCommandForTab(tabTokenValue, kvMap, false);
	}
	
	public static String getHistoryCommandForTab(final String tabTokenValue, final Map kvMap, final boolean isExternal) {
		kvMap.put(TAB_KEY, tabTokenValue);
		if (isExternal) {
			return HistoryCommander.buildExternalToken(defaultToken.getBaseToken(), kvMap);
		} else {
			return HistoryCommander.buildToken(defaultToken.getBaseToken(), kvMap);
		}
	}
	
	public static String getDefaultHistoryCommand() {
		return HistoryCommander.buildToken(defaultToken.getBaseToken(), TAB_KEY, String.valueOf(defaultToken.getDefaultToken()));
	}
	
	
	public static String getTabKey() {
		return TAB_KEY;
	}
	
	public static String getValidatedDefaultCommand() {
		return ValidatedTabbedContentPanel.getDefaultHistoryCommand();
	}
	
	public void onChange(Authenticator source){
		if (source.isLoggedIn()) {
			setWidget(validatedTabPanel);
			defaultToken = validatedTabPanel;
		} else {
			setWidget(publicTabPanel);
			defaultToken = publicTabPanel;
		}
	}
	
	public String getDefaultToken() {
		return defaultToken.getDefaultToken();
	}

	public String getBaseToken() {
		return defaultToken.getBaseToken();
	}

	public void onEntry(Map<String, String> kvMap) {
		defaultToken.onEntry(kvMap);
		//This works because we already registered it in the 
		// constructor of TabbedContentPanel class
		//HistoryCommander.newItem(publicTabPanel.getDefaultToken());
	}

	public void onExit() {
		// TODO Auto-generated method stub
		defaultToken.onExit();
	}
	
}
