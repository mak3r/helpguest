package com.helpguest.marketplace.client.bluegray.ui;

import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.widgets.QuickLogin;

public class LoginHeader extends SimplePanel implements AuthenticationListener {
	
	//private QuickLogin login = new QuickLogin(ValidatedTabbedContentPanel.MY_ACCOUNT_TOKEN);
	private QuickLogin login = new QuickLogin(TabbedContentPanel.getValidatedDefaultCommand());
	LogoutWidget logout = new LogoutWidget(login);
	
	public LoginHeader(){
		login.addAuthenticationListener(this);
		//FIXME login and logout should be refactored to play nice while at the 
		// same time, not exposing methods like this.  This may be a security risk.
		// see bugzilla bug 121
		logout.addClickListener(login);
		logout.init();

		if (login.isLoggedIn()) {
			this.setWidget(logout);
		} else {
			this.setWidget(login);
		}
		this.setStyleName("log_header");
	}
	
	
	public void onChange(Authenticator authSource){
		if(authSource.isLoggedIn()){
			this.setWidget(logout);
		}else{
			this.setWidget(login);
		}
		
	}


	public Authenticator getAuthenticator() {
		return login;
	}
}
