package com.helpguest.marketplace.client.bluegray.ui;
/**
 * @author Ying
 * */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.bakery.CookieManager;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.history.SubCommandToken;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.ui.SearchPanel;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;


public class FindExpert extends SimplePanel implements ResultContainer, SubCommandToken {
	public static final String CONTENT_KEY = "content";
	public final static String CRITERIA = "criteria";
	public final static String TOKEN = "find_expert";
	public static final String DEMO = "demo";
	public static final String SEARCH = SearchResultsHandler.SEARCH_TOKEN;
	public static final String EXPERT_PREVIEW = ExpertPreviewsHandler.TOKEN;
	public static final String QUICK_TICKET = QuickTicket.HISTORY_PREFIX;
	public static final String SURVEY = SurveyViaDecisonDialog.HISTORY_PREFIX;
	public final static int FIND_EXPERT_INDEX = 0;
	public final static int SEARCH_INDEX = 1;
	private final Label center = new Label("EXPERT CENTER");
	private final Label tabTop = new Label("BROWSE FOR AN EXPERT BY CATEGORY");
	
	private static MarketPlaceConstants marketplaceConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String recentExperts = marketplaceConstants.recentExperts();
    
	
	final VerticalPanel outerContainerPanel = new VerticalPanel(); 
	final HorizontalPanel upperContent = new HorizontalPanel();
	final HorizontalPanel bottomContent = new HorizontalPanel();
	final VerticalPanel expertCenterLeft = new VerticalPanel();
	final SimplePanel leftContent = new SimplePanel();
	final DemoWidget demoPanel = new DemoWidget();	 
	final SimplePanel htmlContent = new SimplePanel() ;
	final HTML searchHtmlContent = new HTML("Welcome to HelpGuest search list of experts");
	final ExpertServiceAsync expertService = (ExpertServiceAsync) GWT.create(ExpertService.class);
	//private ExpertDTO expert;
	final CategoryContentTabbedPanel categoryPanel = new CategoryContentTabbedPanel(this);
	ExpertShortBrickList expertShortList = new ExpertShortBrickList();
	private final ContentEntry leftInfo = new ContentEntry("findAnExpertLeft.html");
	private static QuickTicket quickTicket;
	private static SurveyViaDecisonDialog surveyViaDecisonDialog;
	private static SearchWidget searchWidget;
	private CommandToken parentCommand;
	private List expertDTOList = new ArrayList();
	private boolean previewNow = false;
	private Map localKvMap;
	//initialize to true and set as false after the first time we have initialized the panel.
	private boolean doReInit = true;
	
	private static FindExpert findExpertPanel = null;
	
	private FindExpert() {

	}
	
	public static FindExpert getEmptyInstance() {
		return new FindExpert();
	}
	
	public static FindExpert getInstance(){
		if (findExpertPanel == null) {
			findExpertPanel = new FindExpert();
		
			searchWidget=SearchWidget.newInstance(new String("SEARCH FOR AN EXPERT BY KEYWORD OR NAME"), 
					new String(""), findExpertPanel, SearchPanel.one);
	
			quickTicket = new QuickTicket(findExpertPanel);
			QuickTicket.register(quickTicket);
			
			surveyViaDecisonDialog = new SurveyViaDecisonDialog(findExpertPanel);
			SurveyViaDecisonDialog.register(surveyViaDecisonDialog);
			
			findExpertPanel.init();
			//Initialize this with the find an expert search page
			findExpertPanel.setContent(FIND_EXPERT_INDEX);
		}
		return findExpertPanel;
	}
	
	public static FindExpert getInstance(final CommandToken parentCommand) {
		getInstance();
		findExpertPanel.parentCommand = parentCommand;
		return findExpertPanel;
	}
	

	
	private void init(){
		if (doReInit) {
			setCategory(CategoryContentTabbedPanel.design);
			htmlContent.setWidget(leftInfo.getContent());
			
			upperContent.add(htmlContent);
			upperContent.add(demoPanel);
	
			ServiceDefTarget endpoint = (ServiceDefTarget) this.expertService;
			String moduleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
			endpoint.setServiceEntryPoint(moduleRelativeURL);
			
			leftContent.setWidget(searchHtmlContent);
			outerContainerPanel.add(upperContent);
			outerContainerPanel.add(center);
			
			/**
			 * FIXME eventually we will get this list from a table
			 * in the database.  The table will be updated by
			 * a cron job periodically with recent experts.
			 * For now, we put the static list into the props file MarketPlaceConstants.properties
			 */
			String[] recentExpertList = recentExperts.split(",");
		    //Now add each recent expert to the panel
		    for(int i = 0; i < recentExpertList.length; i++) {
		    	expertService.getExpert(recentExpertList[i], loadExpertDTOCallback);
			}
		    ExpertPreviewsHandler.registerCommandToken(findExpertPanel, new ExpertCategoryDisplay(expertDTOList));
			expertCenterLeft.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
			expertCenterLeft.add(searchWidget);
			expertCenterLeft.add(tabTop);
			expertCenterLeft.add(categoryPanel);
			bottomContent.add(expertCenterLeft);
			bottomContent.add(expertShortList);
			outerContainerPanel.add(bottomContent);
			
		    
			htmlContent.setStyleName("find_expert_html_content");
			demoPanel.setStyleName("find_expert_demo_panel");
			upperContent.setStyleName("find_expert_upper_content");
			
			leftContent.setStyleName("find_expert_left_content");
			searchHtmlContent.setStyleName("find_expert_search_html_content");
			upperContent.setStyleName("find_expert_upper_content");
	
			tabTop.setStyleName("find_expert_tab_top");
		    leftContent.setStyleName("find_expert_left_content");
		    expertCenterLeft.setStyleName("find_expert_expert_center_left");
		    center.setStyleName("find_expert_center");
		    bottomContent.setStyleName("find_expert_bottom_content");
			setStyleName("find_expert");
			
			initCommand();
			doReInit = false;
		}
	}
	
	/**
	 * Initialize the basic commands.
	 * */
	private void initCommand() {
		//initialize the search command
		SearchResultsHandler.createCommandToken(null, null, searchWidget);
	}
	
	/**
	 * Set the content of the find expert panel.
	 * */
	public void setContent(int index){
		switch(index){
		
		case FIND_EXPERT_INDEX:
			findExpertPanel.setWidget(outerContainerPanel);
			break;
		case SEARCH_INDEX:
			//body.setWidget(expertCategoryDisplay);
			break;
		}
	}
	
	/**
	 * Set the content of the category panel.
	 * */
	public void setCategory(int index) {
		categoryPanel.setContent(index);
	}
	
	AsyncCallback loadExpertDTOCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			//FIXME: This should go to the oops page instead
			//Window.alert("I WANT TO BE AN OOPS PAGE \nfail to init and add expert brick.");
		}
		
		public void onSuccess(Object result) {
			ExpertDTO expert = (ExpertDTO)result;
			addExpert(expert);
		}
	};
	
	AsyncCallback previewNowCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			//FIXME: This should go to the oops page instead
			//Window.alert("I WANT TO BE AN OOPS PAGE \nfail to init and add expert brick.");
		}
		
		public void onSuccess(Object result) {
			ExpertDTO expert = (ExpertDTO)result;
			addExpert(expert);
			
			CommandToken expertPreviewCommand = ExpertPreviewsHandler.getCommandToken(expert.getExpertUid());
			expertPreviewCommand.onEntry(localKvMap);
		}
	};
	
	/**
	 * Add an expert to the short list panel.
	 * */
	public synchronized void addExpert(ExpertDTO expert) {
		//expertDTOList.add(expert);
		ExpertPreviewsHandler.registerPreview(findExpertPanel, expert);
		expertShortList.addExpertShortBrick(expert);
	}


	public String getBaseToken() {
		return TOKEN;
	}

	public void onEntry(Map<String, String> kvMap) {
		String content = (String)kvMap.get(FindExpert.CONTENT_KEY);
		//Window.alert("onEntry of FindExpert");
		if(content == null) {
			//set to default content
			setContent(FIND_EXPERT_INDEX);
			searchWidget.setQuery("");
		} else {
			if(content.equals(DEMO)){
				HistoryCommander.newItem(CookieManager.ponder(PublicTabPanel.DEMO_TOKEN));
				//generalTabbedContent.selectTab();
			} else if(content.equals(SEARCH)) {
				CommandToken searchCommand = SearchResultsHandler.getCommandToken();
				searchCommand.onEntry(kvMap);
			} else if(content.equals(EXPERT_PREVIEW)) {
				String uid = (String)kvMap.get(ExpertPreviewsHandler.UID);
				DebugPanel.setText("FindExpert", "onEntry: content " + content + ", uid: "+ uid);
				CommandToken expertPreviewCommand = ExpertPreviewsHandler.getCommandToken(uid);
				DebugPanel.setText("FindExpert", "onEntry: content " + content + ", expertPreviewCommand is " + (expertPreviewCommand==null?"null":"not null"));
				if (expertPreviewCommand != null) {
					expertPreviewCommand.onEntry(kvMap);					
				} else {
					previewNow = true;
					localKvMap = kvMap;
					expertService.getExpert((String)kvMap.get(ExpertPreviewsHandler.UID), previewNowCallback);
				}
			} else if(content.equals(QUICK_TICKET)) {
				CommandToken quickTicketCommand = QuickTicket.getCurrentCommandToken();
				quickTicketCommand.onEntry(kvMap);
			}else if(content.equals(SURVEY)) {
				CommandToken surveyCommand = SurveyViaDecisonDialog.getCurrentCommandToken();
				surveyCommand.onEntry(kvMap);
			}
		}
	}

	public void onExit() {
		// TODO Auto-generated method stub
		
	}

	public void reset() {
		// TODO Auto-generated method stub
		
	}

	public void setParent(CommandToken parentCommand) {
		this.parentCommand = parentCommand;
	}
	
	

}

