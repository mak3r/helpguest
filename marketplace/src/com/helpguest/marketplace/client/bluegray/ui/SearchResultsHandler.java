package com.helpguest.marketplace.client.bluegray.ui;

import java.util.HashMap;
import java.util.Map;

import com.helpguest.marketplace.client.history.CommandToken;


public class SearchResultsHandler {
	private static SearchResult currentSearchResult;
	private static Map searchResultsMap = new HashMap();
	//private static Map searchQueryMap = new HashMap();
	//private static String currentQuery;
	public final static String SEARCH_TOKEN = "ExpertSearch";
	public final static String CRITERIA = "criteria";
	public final static String DISPLAY = "displaycategory";
	public final static String PAGE = "page";
	public static CommandToken createCommandToken(final String query,
			final ExpertCategoryDisplay expertCategoryDisplay,
			final SearchWidget searchWidget) {
		//Maintain a reference to this search result list
		//searchResultsMap.put(query, expertLongList);
		//String query_id_str = String.valueOf(queryId++);
		//currentQuery = query;
		if(query != null) {
			searchResultsMap.put(query, expertCategoryDisplay);
		}
		//searchQueryMap.put(query_id_str, query);
		return currentSearchResult = new SearchResult(query, searchWidget);
	}
	
	public static CommandToken getCommandToken() {
		return currentSearchResult;
	}
	
	/**
	 * Get the search token.
	 * */
	public static String getSearchToken(final String token) {
		return getSearchToken(token, null, null);
	}
	
	/**
	 * Get the search token with query and category.
	 * */
	public static String getSearchToken(final String query, final String category) {
		return getSearchToken(query, category, null);
	}
	
	/**
	 * Get the search token with query, category and page.
	 * */
	public static String getSearchToken(final String query, final String category, final String page) {
		Map kvMap = new HashMap();
		kvMap.put(FindExpert.CONTENT_KEY, SEARCH_TOKEN);
		kvMap.put(CRITERIA, query);
		if(category != null) {
			kvMap.put(DISPLAY, category);
		}
		if(page != null) {
			kvMap.put(PAGE, page);
		}
		
		return TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, kvMap);
	}
	
	/**
	 * Get the current query.
	 * */
	/*
	public static String getQuery() {
		return currentQuery;
	}
	*/
	
	/**
	 * Private inner class implementing CommandToken.
	 * */
	private static class SearchResult implements CommandToken {
		//Create a command token to work with the HistoryCommander
		private String search;
		private SearchWidget searchWidget;
		//private List expertList;
		private Map map;
		
		//private ExpertCategoryDisplay expertCategoryDisplay;
		public SearchResult(String query, final SearchWidget searchWidget) {
			this.search = query;
			//this.expertCategoryDisplay = expertCategoryDisplay;
			this.searchWidget = searchWidget;
			//this.expertList = list;
			map = new HashMap();
			map.put(FindExpert.CONTENT_KEY, SEARCH_TOKEN);
			map.put(CRITERIA, query);
		}
		
		public String getBaseToken() {
			return TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, map);
		}
		
		public void onEntry(final Map<String, String> kvMap) {
			String query = "";
			if (kvMap.size() > 0) {
				query = (String)kvMap.get(CRITERIA);
			}
			
			//set the content of the results panel
			if (searchResultsMap.containsKey(query)) {
				String category = (String)kvMap.get(DISPLAY);
				ExpertCategoryDisplay expertCategoryDisplay = (ExpertCategoryDisplay)searchResultsMap.get(query);
				int categoryIndex = 0;
				
				if(category == null) {
					//Window.alert("no category");
				}else if(category.equals(ExpertCategoryDisplay.design)) {
					categoryIndex = ExpertCategoryDisplay.DESIGN;
				}else if(category.equals(ExpertCategoryDisplay.development)) {
					categoryIndex = ExpertCategoryDisplay.DEVELOPMENT;
				}else if(category.equals(ExpertCategoryDisplay.business)) {
					categoryIndex = ExpertCategoryDisplay.BUSINESS;
				}else if(category.equals(ExpertCategoryDisplay.systems)) {
					categoryIndex = ExpertCategoryDisplay.SYSTEMS;
				}else if(category.equals(ExpertCategoryDisplay.personal)) {
					categoryIndex = ExpertCategoryDisplay.PERSONAL;
				}
				
				expertCategoryDisplay.setExpertList(categoryIndex);
				expertCategoryDisplay.tab.selectTab(categoryIndex);
				
				//set the page
				String page = (String)kvMap.get(PAGE);
				int pageIndex = 0;
				if(page == null) {
					pageIndex = 1;
				} else {
					pageIndex = Integer.parseInt(page);
				}
				expertCategoryDisplay.setPagingLink(categoryIndex, pageIndex);
				
				//Window.alert("display page !");
				
				expertCategoryDisplay.getExpertLongBrickList(categoryIndex).displayPage(pageIndex);
				
				//Reset with existing values.
				expertCategoryDisplay.getSearchWidget().setQuery(query);
				
				searchWidget.setWidget(expertCategoryDisplay);
			} else {
				//this was a refresh action or the user entered a 
				// value in the address bar that was not previously known
				// Perform a new search
				//Window.alert("quoted search");
				searchWidget.quotedSearch(query);
			}
		}
		
		public void onExit() {
			//NOOP
		}
		
			
	}
}
