package com.helpguest.marketplace.client.bluegray.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.TreeListener;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.OfferingsConstraints;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.ExpertiseService;
import com.helpguest.marketplace.client.ExpertiseServiceAsync;
import com.helpguest.marketplace.client.SessionService;
import com.helpguest.marketplace.client.SessionServiceAsync;
import com.helpguest.marketplace.client.brick.HorizontalBrick;
import com.helpguest.marketplace.client.brick.VerticalBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.client.ui.StarRating;
import com.helpguest.marketplace.client.util.DebugPanel;

public class ClientViewOfExpert extends SimplePanel implements TreeListener {
	final ExpertServiceAsync expertService = (ExpertServiceAsync) GWT
			.create(ExpertService.class);
	private ExpertDTO expert = null;
	private VerticalPanel base = new VerticalPanel();
	private VerticalPanel info = new VerticalPanel();
	private SimplePanel infoWrapper = new SimplePanel();
	private VerticalPanel left = new VerticalPanel();
	private SimplePanel leftWrapper = new SimplePanel();
	private VerticalPanel right = new VerticalPanel();
	private SimplePanel rightWrapper = new SimplePanel();
	private HorizontalPanel hp = new HorizontalPanel();
	private HorizontalPanel top = new HorizontalPanel();
	private VerticalPanel treePanel = new VerticalPanel();
	private Tree tree = new Tree();
	private HorizontalPanel titleBar = new HorizontalPanel();
	private Hyperlink link1 = null;
	private Hyperlink link2 = null;
	private Hyperlink link3 = new Hyperlink("ADD TO MY GUESTLIST", "add");
	private Hyperlink link4 = null;
	private Hyperlink link5;
	private String nextExpertUID;
	private String previousExpertUID;
	private SessionDTO session;

	final ExpertiseServiceAsync expertiseService = (ExpertiseServiceAsync) GWT
			.create(ExpertiseService.class);
	final SessionServiceAsync sessionService =
		(SessionServiceAsync) GWT.create(SessionService.class);
	// private ExpertDTO expert;
	

	public ClientViewOfExpert(ExpertDTO expert, String previousExpertUID, String nextExpertUID) {
		DebugPanel.setText("ClientViewOfExpert", "entering ctor");
		this.nextExpertUID = nextExpertUID;
		this.previousExpertUID = previousExpertUID;
		this.expert = expert;
		ServiceDefTarget expertiseEndpoint = (ServiceDefTarget) this.expertiseService;
		String expertiseModuleRelativeURL = GWT.getModuleBaseURL()
				+ "ExpertiseService";
		expertiseEndpoint.setServiceEntryPoint(expertiseModuleRelativeURL);
		expertiseService.getCategories(getCatgsCallBack);
		
		ServiceDefTarget sessionEndpoint = (ServiceDefTarget) this.sessionService;
		String sessionModuleRelativeURL = GWT.getModuleBaseURL() + "SessionService";
		sessionEndpoint.setServiceEntryPoint(sessionModuleRelativeURL);

		if (expert != null) {
			sessionService.createSession(
					expert.getExpertAccount().getEmail(), 
					createSessionCallback);
		}
		DebugPanel.setText("ClientViewOfExpert", "exiting ctor");		
	}
	
	/**
	 * Initialize the panel after asynchronously getting the session dto.
	 * */
	private void init() {
		DebugPanel.setText("ClientViewOfExpert", "entering init()");		
		Label handle = new Label(expert.getHandle());
		DebugPanel.setText("ClientViewOfExpert", "init(): handle is:" + handle.getText());		
		CategoryIcon categoryIcon = new CategoryIcon(expert.getExpertUid());
		DebugPanel.setText("ClientViewOfExpert", "init(): got categoryIcon");		
		String historyItemNext = ExpertPreviewsHandler.getExpertPreviewToken(nextExpertUID);
		String historyItemPrevious = ExpertPreviewsHandler.getExpertPreviewToken(previousExpertUID);
		link1 = new Hyperlink("SEE PREVIOUS EXPERT", historyItemPrevious);
		link2 = new Hyperlink("SEE NEXT EXPERT", historyItemNext);

		if (nextExpertUID == null) {
			link2.setVisible(false);
		}
		
		if(previousExpertUID == null) {
			link1.setVisible(false);
		}

		top.add(link1);
		top.add(link2);
		// titlebar
		titleBar.add(handle);
		titleBar.add(categoryIcon);
		titleBar.add(link3);

		// the avatar part
		HorizontalPanel avatarPanel = new HorizontalPanel();
		Image image = new Image(expert.getAvatarURL());
		DebugPanel.setText("ClientViewOfExpert", "init(): got avatar");		
		String aboutMe = expert.getBio();
		DebugPanel.setText("ClientViewOfExpert", "init(): got bio: " + aboutMe);		
		HTML html1 = new HTML();
		html1.setText(aboutMe);
		avatarPanel.add(image);
		avatarPanel.add(html1);

		// connect part

		
		link5 = new Hyperlink("RECCOMMEND" + expert.getHandle(), "recommend");
		HorizontalPanel buttomLink = new HorizontalPanel();
		buttomLink.add(link3);
		
		String historyBack = TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, new HashMap());
		link4 = new Hyperlink("BACK TO EXPERT CENTER", historyBack);
		buttomLink.add(link4);

		// set up left panel
		left.add(avatarPanel);
		left.add(new ConnectPanel(expert, session));
		left.add(buttomLink);

		//wrap up the right side with an outer box for css styling.
		leftWrapper.setWidget(left);
		leftWrapper.setStyleName("client_view_of_expert_left_outer");

		// set up right panel
		Label rateLabel = new Label("Rate:");
		Label rateValue = new Label("$ "
				+ expert.getPaymentPlan().getRatePerMinute() + "/minute");
		DebugPanel.setText("ClientViewOfExpert", "init(): got rateValue: " + rateValue.getText());		
		HorizontalBrick rateBrick = new HorizontalBrick(rateLabel, rateValue);
		
		Label minDepositLabel = new Label("Minimum deposit:");
		Label minDepositValue = new Label("$ "
				+ expert.getPaymentPlan().getDepositAmount());
		HorizontalBrick minDepositBrick = new HorizontalBrick(minDepositLabel, minDepositValue);
		
		Label completedLabel = new Label("COMPLETED PROJECTS:");
		Label completeValue = new Label(expert.getStats().getNumberResolved());
		VerticalBrick completedBrick = new VerticalBrick(completedLabel, completeValue);
		completedBrick.setStyleName("client_view_of_expert_completed_brick");
		
		Label scoreLabel = new Label("USER SCORE:");
		StarRating starRating = new StarRating(expert.getStats()
				.getSatisfactionRating());
		VerticalBrick scoreRatingBrick = new VerticalBrick(scoreLabel, starRating);
		
		Label expertiseLabel = new Label("AREAS OF EXPERTISE");
		treePanel.add(tree);
		VerticalBrick expertiseBrick = new VerticalBrick(expertiseLabel, treePanel);
		expertiseBrick.setStyleName("client_view_of_expert_expertise_brick");

		right.add(rateBrick);
		right.add(minDepositBrick);
		right.add(completedBrick);
		right.add(scoreRatingBrick);
		right.add(expertiseBrick);

		//wrap up the right side with an outer box for css styling.
		rightWrapper.setWidget(right);
		rightWrapper.setStyleName("client_view_of_expert_right_outer");

		hp.add(leftWrapper);
		hp.add(rightWrapper);

		info.add(titleBar);
		info.add(hp);
		
		//wrap up the whole info block for css styling
		infoWrapper.setWidget(info);
		infoWrapper.setStyleName("client_view_of_expert_info_wrapper");
		
		base.add(top);
		base.add(infoWrapper);
		setWidget(base);

		// set styleName
		link1.setStyleName("client_view_of_expert_link1");
		link2.setStyleName("client_view_of_expert_link2");
		link3.setStyleName("client_view_of_expert_link3");
		link4.setStyleName("client_view_of_expert_link4");
		link5.setStyleName("client_view_of_expert_link5");
		handle.setStyleName("client_view_of_expert_handle");
		categoryIcon.setStyleName("client_view_of_expert_categoryIcon");
		avatarPanel.setStyleName("client_view_of_expert_avatarPanel");
		image.setStyleName("client_view_of_expert_image");
		html1.setStyleName("client_view_of_expert_html1");
		buttomLink.setStyleName("client_view_of_expert_buttom_link");
		titleBar.setStyleName("client_view_of_expert_title_bar");
		base.setStyleName("client_view_of_expert_base");
		info.setStyleName("client_view_of_expert_info");
		left.setStyleName("client_view_of_expert_left");
		right.setStyleName("client_view_of_expert_right");
		hp.setStyleName("client_view_of_expert_hp");
		top.setStyleName("client_view_of_expert_top");
		tree.addTreeListener(this);
		DebugPanel.setText("ClientViewOfExpert", "exiting init()");		
	}

	AsyncCallback getCatgsCallBack = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				DebugPanel.setText("ClientViewOfExpert", "getCatgsCallBack.onFailure(): " + sBuf.toString());
			} catch (Throwable th) {
				DebugPanel.setText("ClientViewOfExpert", "getCatgsCallBack.onFailure(): [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			DebugPanel.setText("ClientViewOfExpert", "getCatgsCallBack.onSuccess(): ");
			List categoryList = (List) result;
			for (int i = 0; i < categoryList.size(); i++) {
				final CategoryConstraints cc = new CategoryConstraints();
				cc.setCategoryName((String) categoryList.get(i));

				final TreeItem treeItem = new TreeItem((String) categoryList
						.get(i));
				tree.addItem(treeItem);
				tree.setStyleName("client_view_of_expert_tree");
				treeItem.setStyleName("client_view_of_expert_tree_item");
				expertiseService.getOfferings(cc, new AsyncCallback() {
					public void onFailure(Throwable caught) {
						try {
							throw caught;
						} catch (InvocationException iex) {
							StackTraceElement[] ste = iex.getStackTrace();
							StringBuffer sBuf = new StringBuffer(iex
									.getMessage());
							for (int i = 0; i < ste.length; i++) {
								sBuf.append("\n\t" + ste.toString());
							}
							Window
									.alert("ExpertTagsWidget getCatgsOffCallBack: "
											+ sBuf.toString());
						} catch (Throwable th) {
							Window
									.alert("ExpertTagsWidget getCatgsOffCallBack: [throwable]"
											+ th.getMessage());
						}
					}

					public void onSuccess(Object result) {
						List offerings = (List) result;
						for (int j = 0; j < offerings.size(); j++) {
							final TreeItem treeItemOff = new TreeItem(
									(String) offerings.get(j));
							treeItem.addItem(treeItemOff);

							OfferingsConstraints oc = new OfferingsConstraints();
							oc.setOfferingName((String) offerings.get(j));
							treeItemOff
									.setStyleName("client_view_of_expert_tree_item_offer");
							expertiseService.getTags(cc, oc, expert
									.getExpertUid(), new AsyncCallback() {
								public void onFailure(Throwable caught) {
									try {
										throw caught;
									} catch (InvocationException iex) {
										StackTraceElement[] ste = iex
												.getStackTrace();
										StringBuffer sBuf = new StringBuffer(
												iex.getMessage());
										for (int i = 0; i < ste.length; i++) {
											sBuf
													.append("\n\t"
															+ ste.toString());
										}
										DebugPanel.setText("ExpertTagsWidget", "getTagsCallBack: "
														+ sBuf.toString());
									} catch (Throwable th) {
										DebugPanel.setText("ExpertTagsWidget", "getTagsCallBack: [throwable]"
														+ th.getMessage());
									}
								}

								public void onSuccess(Object result) {
									List tags = (List) result;
									if (tags.isEmpty()) {
										treeItem.removeItem(treeItemOff);
									} else {
										List newTags = new ArrayList();
										for (int i = 0; i < tags.size(); i++) {
											String tag = (String) tags.get(i);
											String[] tags_array = tag
													.split("\\|");
											for (int j = 0; j < tags_array.length; j++) {
												newTags.add(tags_array[j]);
											}

										}

										for (int listIt = 0; listIt < newTags
												.size(); listIt++) {
											TreeItem treeItemTag = new TreeItem(
													(String) newTags
															.get(listIt));
											treeItemOff.addItem(treeItemTag);
										}
									}
								}
							});
						}
					}
				});
			}
		}
	};
	
	AsyncCallback createSessionCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			//go ahead and init with a null session
			init();
			DebugPanel.setText("ClientViewOfExpert", "createSessionCallback.failure(): " );
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				DebugPanel.setText("ClientViewOfExpert", "createSessionCallback.failure(): " + sBuf.toString());
			} catch (Throwable th) {
				DebugPanel.setText("ClientViewOfExpert", "createSessionCallback.failure(): [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			DebugPanel.setText("ClientViewOfExpert", "createSessionCallback.onSuccess(): ");
			session = (SessionDTO) result;
			init();
		}
		
	};

	/**
	 * Executes when a tree item is selected.
	 */
	public void onTreeItemSelected(TreeItem item) {

	}

	/**
	 * Executes when a tree item's state is changed.
	 */
	public void onTreeItemStateChanged(TreeItem item) {
		if (item.getState()) {
			// close all sibling items
			TreeItem parentItem = item.getParentItem();
			if (parentItem == null) {
				for (int i = 0; i < tree.getItemCount(); i++) {
					TreeItem childItem = tree.getItem(i);
					if (childItem == item) {
						;
					} else {
						childItem.setState(false);
					}
				}
			} else {
				for (int i = 0; i < parentItem.getChildCount(); i++) {
					TreeItem childItem = parentItem.getChild(i);
					if (childItem == item) {
						;
					} else {
						childItem.setState(false);
					}
				}
			}
		}
	}

	public static void main(String args[]) {
		String s = "java";
		String[] ss = s.split("\\|");
		System.out.println(ss.length);
	}
}
