package com.helpguest.marketplace.client.bluegray.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.*;


/**
 * @author Ying
 * */
public class DemoPanel extends SimplePanel{
	final ContentEntry contentDemo = new ContentEntry("demo.html");
	public DemoPanel(){
		setWidget(contentDemo);
		setStyleName("demo_panel");
	}
}
