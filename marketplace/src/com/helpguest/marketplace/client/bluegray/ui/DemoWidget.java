package com.helpguest.marketplace.client.bluegray.ui;
/**
 * @author Ying
 * */
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.VisualCuesImageBundle;

public class DemoWidget extends SimplePanel {
	private VerticalPanel base = new VerticalPanel();
	private HorizontalPanel upper = new HorizontalPanel();
	private VerticalPanel left = new VerticalPanel();
	private Hyperlink demo = new Hyperlink("Click To View Demo", HistoryCommander.buildToken("public_tabs",TabbedContentPanel.getTabKey(), PublicTabPanel.DEMO_TOKEN));
	private SimplePanel htmlContent = new SimplePanel();
	private SimplePanel lower = new SimplePanel();
	private VisualCuesImageBundle visualCues =  (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);	  

	private final ContentEntry topRightInfo = new ContentEntry("findAnExpertTopRight.html");
	private final ContentEntry bottomRightInfo = new ContentEntry("findAnExpertBottomRight.html");
	
	public DemoWidget(){
		//Set the content of the html part
		htmlContent.setWidget(topRightInfo.getContent());
		lower.setWidget(bottomRightInfo.getContent());
		left.add(htmlContent);
		left.add(demo);
		upper.add(left);
		upper.add(visualCues.secure_laptop().createImage());
		base.add(upper);
		base.add(lower);
		setWidget(base);
		
		//Add style name for components
		upper.setStyleName("demo_widget_upper_panel");
		lower.setStyleName("demo_widget_lower_panel");
		setStyleName("demo_widget");
	}
}
