/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client.bluegray.ui;

import java.util.HashMap;
import java.util.Map;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.IPNService;
import com.helpguest.marketplace.client.IPNServiceAsync;
import com.helpguest.marketplace.client.brick.ContentBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.IPNDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.GuestConnectInstructions;
import com.helpguest.marketplace.client.ui.HorizontalSpacer;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.ui.VerticalSpacer;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;
import com.helpguest.marketplace.client.widgets.PaddedPanel;

/**
 * @author mabrams
 * @ticket# CX11CE297170B
 */
public class QuickTicket extends SimplePanel implements CommandToken {
	public static CommandToken currentCommandToken;
	
	public final static String QUICK_TICKET_ID = "quickTicketId";
	public final static String PAYPAL_ID = "ticketId";
	public final static String HISTORY_PREFIX = "QuickTicket";
	private boolean usePaidTicket = false;
	private String ticketNumber;
	private static IPNDTO ipnDTO;
	private ExpertDTO expertDTO;
	//Create the ipn service proxy
	final IPNServiceAsync ipnService =
		(IPNServiceAsync) GWT.create(IPNService.class);
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	private ResultContainer resultContainer;
	private Button closeButton = new Button("Close ticket");
	private Hyperlink goToSurveyLnk = null;
	
	private SimplePanel ticketContainer = new SimplePanel();
	
	private static Label paypalRequired = new Label(
		"Thanks for using HelpGuest with PayPal.  " +
		"Your payment completed successfully.  " +
		"A receipt for your purchase has been emailed to you.  " +
		"Visit http://www.paypal.com to review your account.");
	
	private static HTML note = new HTML(
			"<sup>&dagger;</sup>" +
			"If your Expert has gone offline while " +
			"you went through the payment process, " +
			"please <a href=\"mailto:listen@helpguest.com\">" +
			"let us know</a>.  " +
			"You are entitled to a full refund or " +
			"you can re-use your TICKET# " +
			"when the expert comes back online.");
	static {
		paypalRequired.setStyleName("hg-PaymentCompletePaypalRequired");
		note.setStyleName("hg-PaymentCompleteNote");
	}
	//private static MarketPlaceConstants qtConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	/*private static String urlStr = 
		qtConstants.webUriPrefix() + 
    	"://" +
    	qtConstants.fullyQualifiedDomainName() + 
    	"/" +
    	qtConstants.gwtModule() + 
    	"/" + 
    	qtConstants.gwtModuleEntry() ;*/
	
	public QuickTicket(final ResultContainer resultContainer) {
		this.resultContainer = resultContainer;
		setStyleName("hg-PaymentComplete");
		//Specify the url where the ipn service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.ipnService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "IPNService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);

		//Specify the url where the expert service is running
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String moduleRelativeURLExpertService = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(moduleRelativeURLExpertService);

	}
	
	private void init() {
		String handle = ipnDTO.getSession().getExpertAccount().getHandle();
		VerticalPanel vp = new VerticalPanel();
		vp.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);

		if (usePaidTicket) {
			vp.add(paypalRequired);
		}
		
		Label ticket = new Label(ipnDTO.getSession().getSessionId()); 
		ticket.setStyleName("hg-PaymentCompleteTicket");
		Label ticketLabel = new Label("TICKET#:  ");
		ContentBrick ticketNumber = new ContentBrick(
				ticketLabel, ticket);				
		
		String onOff = (expertDTO.isLive()?
				"<font class=\"hg-online\">ONLINE</font>":
				"offline");
		HTML onOffLine = new HTML(
				handle + " is currently " + onOff + 
				"<sup>&dagger;</sup>" );
		
		Label stepsLabel = new Label(
				"The final steps to getting assistance from " + 
				handle + " are simple.");
		
		SimplePanel instructions =
			new GuestConnectInstructions(ipnDTO.getSession());

		//Add the receipt details and instructions to a distinct panel
		VerticalPanel receiptInfo = new VerticalPanel();
		receiptInfo.setSpacing(20);
		receiptInfo.add(onOffLine);
		receiptInfo.add(stepsLabel);

		//Add the ticket number, receipt details to the receipt
		VerticalPanel receiptPanel = new VerticalPanel();
		receiptPanel.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		receiptPanel.setStyleName("hg-PaymentCompleteReceiptPanel");
		receiptPanel.setBorderWidth(1);
		String surveyUrl = SurveyViaDecisonDialog.getQuickTicketToken(ipnDTO.getSession().getSessionId());
		goToSurveyLnk = new Hyperlink("Go to survey", surveyUrl);
        receiptPanel.add(goToSurveyLnk);
		receiptPanel.add(new PaddedPanel(ticketNumber, 10));
		receiptPanel.add(receiptInfo);
		receiptPanel.add(new PaddedPanel(instructions, 10));
		
		
		PaddedPanel receiptDock = new PaddedPanel(1, 20);
		receiptDock.setStyleName("hg-PaymentCompleteReceiptDock");
		receiptDock.add(receiptPanel);
		
		RoundedPanel receipt = new RoundedPanel(receiptDock, RoundedPanel.ALL, 4);
		receipt.setStyleName("hg-PaymentCompleteReceipt");
		receipt.setCornerStyleName("hg-PaymentCompleteReceipt-corners");
		
		HorizontalPanel outerPadding = new HorizontalPanel();
		outerPadding.add(new VerticalSpacer(75));
		outerPadding.add(receipt);
		outerPadding.add(new VerticalSpacer(75));
		
		vp.add(outerPadding);
		vp.add(new HorizontalSpacer(20));
		if (usePaidTicket) {
			vp.add(note);
		}
		vp.setSpacing(20);
		
		VerticalPanel content = new VerticalPanel();
		content.add(vp);
		content.add(closeButton);
		
		ticketContainer.setWidget(content);

		closeButton.addClickListener(new ClickListener() {

			public void onClick(Widget arg0) {
				String historyBack = TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, new HashMap());
				HistoryCommander.newItem(historyBack);
			}
			
		});
		
	
		
		setWidget(ticketContainer);
	}

	
	AsyncCallback ipnCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			Window.alert("We were unable to find ticket #" + ticketNumber + " at this time");
			History.back();
		}

		public void onSuccess(Object result) {
			if (result == null) {
				Window.alert("Ticket # " + ticketNumber + " is invalid.");
				History.back();
			}
			ipnDTO = (IPNDTO) result;
			expertService.getExpert(ipnDTO.getSession().getExpertAccount().getUid(), expertCallback);
		}
		
	};
	
	AsyncCallback expertCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				//Window.alert("QuickTicket expertCallback: " +  sBuf.toString());
			} catch (Throwable th) {
				//Window.alert("QuickTicket expertCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			init();
			showTicket();
		}		
	};

	public String getBaseToken() {
		return HISTORY_PREFIX;
	}

	public void onEntry(final Map<String, String> kvMap) {
		if (kvMap.size() > 0) {
			ticketNumber = (String) kvMap.get(QUICK_TICKET_ID);
			if (ticketNumber == null) {
				usePaidTicket = true;
				ticketNumber = (String) kvMap.get(PAYPAL_ID);
			}
			//TODO show locating your expert link message
			ipnService.getCompletionDetails(ticketNumber, ipnCallback);			
		}
	}

	public void onExit() {
		//NOOP
	}
	
	//Only set the widget if we have a positive result
	private void showTicket() {
		resultContainer.setWidget(this);		
	}

	
	public static void register(CommandToken commandToken) {
		currentCommandToken = commandToken;
	}
	
	public static CommandToken getCurrentCommandToken() {
		return currentCommandToken;
	}
	
	public static String getQuickTicketToken(String sessionID) {
		Map kvMap = new HashMap();
		kvMap.put(FindExpert.CONTENT_KEY, HISTORY_PREFIX);
		kvMap.put(QUICK_TICKET_ID, sessionID);
		return TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, kvMap);
	}	
	
	public static String getPaidTicketToken(String paidId) {
		Map kvMap = new HashMap();
		kvMap.put(FindExpert.CONTENT_KEY, HISTORY_PREFIX);
		kvMap.put(PAYPAL_ID, paidId);
		return TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, kvMap, true);
	}

}

