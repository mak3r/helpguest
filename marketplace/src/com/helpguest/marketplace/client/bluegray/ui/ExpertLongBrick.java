package com.helpguest.marketplace.client.bluegray.ui;
/**
 * @author ying
 */
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.ui.StarRating;
import com.helpguest.marketplace.client.ui.VerticalSpacer;
import com.helpguest.marketplace.client.ui.VisualCuesImageBundle;

public class ExpertLongBrick extends FocusPanel implements ClickListener{
	public static final int columnNo = 5;	
	private final int rowZero = 0;
	private final int colOne = 0;
	private final int colTwo = 1;
	private final int colThree = 2;
	private final int colFour = 3;
	private final int colFive = 4;
	private final Label expertLabel = new Label("expert");
	private final Label rateLabel = new Label("rate");
	private final Label historyLabel = new Label("history");
	private final Label ratingLabel = new Label("rating");
	private final Label connectedLabel = new Label("connected");
	
	private ExpertDTO expert;
	private Grid expertBrick = new Grid(1,5);
	private String queryId;
	private int position = 0;
	
	/*	
	 * Create a default ExpertLongBrick which represents the header of the table
	 * */
	public ExpertLongBrick(){
		
	    expertBrick.setWidget(rowZero, colOne, expertLabel);
		expertBrick.setWidget(rowZero, colTwo, rateLabel);
		expertBrick.setWidget(rowZero, colThree, historyLabel);
		expertBrick.setWidget(rowZero, colFour, ratingLabel);
		expertBrick.setWidget(rowZero, colFive, connectedLabel);		
		setWidget(expertBrick);
		expertLabel.setStyleName("expert_long_brick_title_expert");
		rateLabel.setStyleName("expert_long_brick_title_rate");
		historyLabel.setStyleName("expert_long_brick_title_history");
		ratingLabel.setStyleName("expert_long_brick_title_rating");
		connectedLabel.setStyleName("expert_long_brick_title_connected");
		setStyleName("expert_long_brick_title");
	}
	
	public ExpertLongBrick(String queryId, final ExpertDTO expert, ResultContainer resultContainer, int position){
		this.queryId = queryId;
		this.expert = expert;
		Rating rating = new Rating(expert);
		Expert expertComponent = new Expert(expert);
	    expertBrick.setWidget(rowZero,colOne,expertComponent);
	    expertBrick.setWidget(rowZero,colTwo,new Rate(expert));
	    expertBrick.setWidget(rowZero,colThree,new History(expert));
	    expertBrick.setWidget(rowZero,colFour,rating);
	    expertBrick.setWidget(rowZero,colFive,new Connect(expert));
	    expertBrick.setStyleName("expert_long_brick_grid");
	    setWidget(expertBrick);
	    if(position % 2 == 0){
	    	setStyleName("expert_long_brick_even");
	    	rating.setStyleName("expert_long_brick_rating_even");
	    	expertComponent.setStyleName("expert_long_brick_expert_even");
	    }
	    else{
	    	setStyleName("expert_long_brick_odd");
	    	rating.setStyleName("expert_long_brick_rating_odd");
	    	expertComponent.setStyleName("expert_long_brick_expert_odd");
	    }
	    this.addClickListener(this);
	    	
	}
	
	 public ExpertDTO getExpert(){
	    	return expert;
	 }
	 
	 public void onClick(Widget sender){
		 HistoryCommander.newItem(ExpertPreviewsHandler.getExpertPreviewToken(expert.getExpertUid()));
	 }
	 
	/**
	 * Set the Expert component
	 */
    public class Expert extends SimplePanel {
    	private HorizontalPanel hp = new HorizontalPanel();
    	private VerticalPanel vp = new VerticalPanel();
    	private Label handle = new Label();
    	private Image image;
    	private CategoryIcon category = new CategoryIcon(expert.getExpertUid());
    	
    	public Expert( final ExpertDTO expert){
    		/*The avatar part*/
    		image = new Image(expert.getAvatarURL());
    		image.setWidth("70px");
    		image.setHeight("78px");
    		if(position % 2==0){
    			image.setStyleName("expert_long_brick_expert_avatar_even");
    		}else{
    			image.setStyleName("expert_long_brick_expert_avatar_odd");
    		}
    		
    		
    		/*Set the Handler*/
    		handle.setText(expert.getHandle());
    		handle.setStyleName("expert_long_brick_expert_handle");
    		hp.add(image);
    		vp.add(handle);
    		vp.add(category);
    		hp.add(vp);
    		hp.setStyleName("expert_long_brick_expert_hp");
    		setWidget(hp);
    		
    		setStyleName("expert_long_brick_expert");
    	}
    }
    
    /**
     * Set the rate component
     */
    public class Rate extends SimplePanel{
    	private VerticalPanel vp = new VerticalPanel();
    	private Label rate = new Label();
    	private Label text = new Label("PER MINUTE");
    	public Rate(final ExpertDTO expert){
    		rate.setText ("$"+expert.getPaymentPlan().getRatePerMinute());
    		vp.add(rate);
    		vp.add(text);
    		setWidget(vp);
    		rate.setStyleName("expert_long_brick_rate_label_rate");
    		text.setStyleName("expert_long_brick_rate_label_text");
    		setStyleName("expert_long_brick_rate");
    	}
    }
    /**
     * Set History component
     */
    public class History extends SimplePanel{
    	private VerticalPanel vp = new VerticalPanel();
    	private Label projectNo = new Label();
    	private Label projectText = new Label("COMPLETED PROJECTS");
    	private Label avgTime = new Label();
    	private Label timeText = new Label("AVERAGE DURATION");
    	public History(final ExpertDTO expert){
    		projectNo.setText(expert.getStats().getNumberResolved());
    		avgTime.setText(expert.getStats().getAvgTimeToResolution());
    		vp.add(projectText);
    		vp.add(projectNo);
    		vp.add(new VerticalSpacer(20));
    		vp.add(timeText);
    		vp.add(avgTime);
    		setWidget(vp);
    		
    		projectText.setStyleName("expert_long_brick_history_project_text");
    		projectNo.setStyleName("expert_long_brick_history_project_No");
    		timeText.setStyleName("expert_long_brick_history_time_text");
    		avgTime.setStyleName("expert_long_brick_history_project_avg_time");
    		setStyleName("expert_long_brick_history");
    	}
    }
    /**
     * Set the rating component
     */
    public class  Rating extends SimplePanel{
    	private VerticalPanel vp = new VerticalPanel();
    	private Label label = new Label();
    	private StarRating star;
    	public Rating(final ExpertDTO expert){
    		star = new StarRating(expert.getStats().getSatisfactionRating());
    		label.setText("USER SCORE");
    		vp.add(label);
    		vp.add(star);
    		setWidget(vp);
    		label.setStyleName("expert_long_brick_rating_label");
    		setStyleName("expert_long_brick_rating");
    	}
    }
    
    public class Connect extends SimplePanel{
    	private VerticalPanel vp = new VerticalPanel();
    	private HorizontalPanel hp = new HorizontalPanel();
    	private HorizontalPanel imageText = new HorizontalPanel();
    	private Label label_upper = new Label();
    	private Label label_bottom = new Label();
    	private VisualCuesImageBundle visualCue = (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);
    	private Image image = new Image();
    	public Connect(final ExpertDTO expert){
    		//CHeck whether this expert is on line
    		if(expert.isLive()){
    			image = visualCue.connection().createImage();
    			imageText.add(image);
    			label_upper.setText("ONLINE");
    			imageText.add(label_upper);
    			label_bottom.setText("CONNECT");
    		}
    		else{
    			imageText.add(visualCue.offline().createImage());
    			label_upper.setText("OFFLINE");
    			imageText.add(label_upper);
    			label_bottom.setText("LEAVE MESSAGE");
    		}
    		hp.add(imageText);
    		vp.add(hp);
    		vp.add(label_bottom);
    		setWidget(vp);
    		if(position % 2 == 0){
    			image.setStyleName("expert_long_brick_connect_image_even");
    			label_upper.setStyleName("expert_long_brick_connect_label_upper_even");
        		label_bottom.setStyleName("expert_long_brick_connect_label_bottom_even");
        		imageText.setStyleName("expert_long_brick_connect_imageText_even");

    		}else{
    			image.setStyleName("expert_long_brick_connect_image_odd");
    			label_upper.setStyleName("expert_long_brick_connect_label_upper_odd");
        		label_bottom.setStyleName("expert_long_brick_connect_label_bottom_odd");
        		imageText.setStyleName("expert_long_brick_connect_imageText_odd");
    		}
    		    		
    		setStyleName("expert_long_brick_connect");
    	}
    	
    	
    }
    
    /*
    public static int getID(){
    	return position;
    }
    */
    
    /*
    public static int resetID(int num){
    	return position = num;
    }
    */
}
