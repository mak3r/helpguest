package com.helpguest.marketplace.client.bluegray.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.util.DebugPanel;

public class ExpertLongBrickList extends SimplePanel {
	public static final int NUMBER_PER_PAGE = 6;
	
	private String queryId;
	private VerticalPanel expertList = new VerticalPanel();
	ContentEntry noExpertsFound = new ContentEntry("noResultsFound.div");
	private ExpertLongBrick title = new ExpertLongBrick();
	private HorizontalPanel top = new HorizontalPanel();
	private Label resultNum = new Label("");
	private ExpertDTO expertDTOList[] = null;
	private List expertUIDList = new ArrayList();
	private Map expertPosition = new HashMap();
	private ResultContainer resultContainer;
	private int number = 1;
	private int pageNo = 0;
	private boolean headerAdded = false;
	private Label countLabel;
	
	public ExpertLongBrickList(ResultContainer resultContainer, String queryId){
		this.queryId = queryId;
		this.resultContainer = resultContainer;
		
		init();
		
		//By default, set no results found
		//replace when results are found.
		setNoResultsFound();
		headerAdded = false;
		
		setWidget(expertList);
		expertList.setStyleName("expert_long_brick_list_expertList");
		setStyleName("expert_long_brick_list");
	}
	
	private void init() {
		String token = TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN,
				new HashMap());
		Hyperlink back = new Hyperlink("<-- Return To Expert Center", token);
		top.add(resultNum);
		top.setCellHorizontalAlignment(resultNum, HorizontalPanel.ALIGN_LEFT);
		top.add(back);
		top.setCellHorizontalAlignment(back, HorizontalPanel.ALIGN_RIGHT);
		
		back.setStyleName("expert_long_brick_list_back");
		resultNum.setStyleName("expert_long_brick_list_result_num");
		top.setStyleName("expert_long_brick_list_top");
		noExpertsFound.getContent().setStyleName("expert_long_brick_list_no_results_found");
	}
	
	private void addExpertLongBrick(ExpertDTO expertDTO){
		if (!headerAdded) {
			//We only do these things the first time the header is added
			//remove the statement about no experts found
			expertList.remove(noExpertsFound);
			//add the experts found header
			setResultsFoundHeader();
			//reset the header added to true
			headerAdded = true;
		}

		try{
			ExpertLongBrick expertlongbrick = new ExpertLongBrick(queryId, expertDTO, resultContainer, number++);
			expertList.add(expertlongbrick);
			//expertDTOList.add(expertDTO);
			//ExpertPreviewsHandler.createCommandToken(resultContainer, expertDTO);
		} catch(Exception e) {
			StringBuffer sb = new StringBuffer();
			StackTraceElement[] ste = e.getStackTrace();
			for(int i = 0; i < ste.length; i++) {
				sb.append(ste[i].toString() + "\n");
			}
		}
	}

	private void setResultsFoundHeader() {
		//ExpertLongBrick.resetID(0);
		expertList.add(top);
		expertList.add(title);
	}
	
	private void setNoResultsFound() {
		expertList.add(noExpertsFound);
	}
	
	/**
	 * Add an expert dto to the ExpertLongBrickList.
	 * */
	public void addExpertDTO(ExpertDTO expert) {
		expertDTOList[((Integer)expertPosition.get(expert.getExpertUid())).intValue()] = expert;
		if (countLabel != null) {
			countLabel.setText(String.valueOf(expertDTOList.length));
		}
	}
	
	/**
	 * Get the list of expert dtos.
	 * */
	public List getExpertDTOList() {
		List list = new ArrayList();
		for(int i = 0; i < expertDTOList.length; i++) {
			list.add(expertDTOList[i]);
		}
		return list;
	}
	
	/**
	 * Get the list of expert uids.
	 * */
	public void setExpertUIDList(List expertUIDList) {
		this.expertUIDList = expertUIDList;
	}
	
	/**
	 * Display the experts in the specified page.
	 * */
	public void displayPage(int pageIndex) {
		//Window.alert("display page");
		if(pageIndex < 1 || pageIndex > pageNo) {
			pageIndex = 1;
		}
		
		//clear the ExpertLongBrickList
		expertList.clear();
		this.setNoResultsFound();
		headerAdded = false;
		
		//add experts
		String debugString = new String();
		
		int start = NUMBER_PER_PAGE * (pageIndex - 1);
		int end = NUMBER_PER_PAGE * pageIndex - 1;
		end = end < expertDTOList.length - 1 ? end : expertDTOList.length - 1;
		String startEnd =String.valueOf(start+1)+" - "+String.valueOf(end+1)+" of "+String.valueOf(expertDTOList.length);
		resultNum.setText("Viewing Results: " + startEnd);
		for(int i = start; i <= end; i++) {
			this.addExpertLongBrick((ExpertDTO)expertDTOList[i]);
			debugString += expertDTOList[i].getExpertUid() + " " + expertDTOList[i].getHandle() + "\n";
		}
		
		DebugPanel.setText("DesignCategoryDisplay PageIndex : " + pageIndex, "\n" + debugString);
	}
	
	/**
	 * Get the page number.
	 * */
	public int getPageNo() {
		return pageNo;
	}
	
	public void setCountLabel(final Label countLabel) {
		this.countLabel = countLabel;
	}

	
	/**
	 * Create the position map.
	 * */
	public void mapPosition(List expertUID){
		expertDTOList = new ExpertDTO[expertUID.size()];
		for(int i = 0 ;i < expertUID.size(); i++){
			expertPosition.put(expertUID.get(i), Integer.valueOf("" + i));
		}
		
		//update the page number
		this.pageNo = (int)Math.ceil((double)expertDTOList.length / NUMBER_PER_PAGE);
	}

}