package com.helpguest.marketplace.client.bluegray.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.widgets.QuickLogin;

public class LogoutWidget extends SimplePanel implements AuthenticationListener {
	private Authenticator authSource;
	private ExpertDTO expert;
	private Label label = new Label("You are signed in as:");
	private Label handleLabel = new Label();
	private Label availabilityLabel = new Label("availability:");
	private Hyperlink signOut = new Hyperlink("sign out",TabbedContentPanel.getDefaultHistoryCommand());
	private HorizontalPanel handle = new HorizontalPanel();
	private HorizontalPanel availability = new HorizontalPanel();
	private VerticalPanel vp = new VerticalPanel();
	
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	public LogoutWidget( final Authenticator authSource){
		this.authSource = authSource;
		this.authSource.addAuthenticationListener(this);
		
		//Initialize the ExpertService
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);
		handle.add(handleLabel);
		handle.add(signOut);
		availability.add(availabilityLabel);
		vp.add(label);
	    vp.add(handle);
	    vp.add(availability);
	    setWidget(vp);
		label.setStyleName("log_out_widget_label");
		handleLabel.setStyleName("log_out_widget_handle_label");
		availabilityLabel.setStyleName("log_out_widget_availability_label");
		signOut.setStyleName("log_out_widget_sign_out");
		handle.setStyleName("log_out_widget_handle");
		availability.setStyleName("log_out_widget_availability");
		vp.setStyleName("log_out_widget_vp");
		this.setStyleName("log_out_widget");
	}
	
	public void init(){
		if (authSource.isLoggedIn()) {
			expertService.getExpert(authSource.getAuthenticatedParty(), expertCallback);
		}
	}
	
	
		AsyncCallback expertCallback = new AsyncCallback(){
			public void onFailure(Throwable caught) {
				try {
					throw caught;
				} catch (InvocationException iex) {
					StackTraceElement[] ste = iex.getStackTrace();
					StringBuffer sBuf = new StringBuffer(iex.getMessage());
					for (int i = 0; i < ste.length; i++) {
						sBuf.append("\n\t" + ste.toString());
					}
					//Window.alert("LogoutWidget expertCallback: " +  sBuf.toString());
				} catch (Throwable th) {
					//Window.alert("LogoutWidget expertCallback: [throwable]" + th.getMessage());
				}
			}
			
			public void onSuccess(Object result){
				expert = (ExpertDTO)result;
			    handleLabel.setText(expert.getHandle());
			}
		};
	
	public void addClickListener(final QuickLogin login){
		signOut.addClickListener(new ClickListener(){
			public void onClick(Widget sender){
				login.logout();
			}
		});
	}

	public void onChange(Authenticator source) {
		init();
	}
	
	
}
