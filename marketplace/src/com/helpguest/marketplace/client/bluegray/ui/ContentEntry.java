package com.helpguest.marketplace.client.bluegray.ui;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.HTTPRequest;
import com.google.gwt.user.client.ResponseTextHandler;

 /**
  *  @author Ying
 * */


/**
 * This is used to add html file 
 * */
public class ContentEntry extends SimplePanel{

	private HTML content = new HTML();
	
	public ContentEntry(final String url) {
		super();		
		boolean b = HTTPRequest.asyncGet(url, new ResponseTextHandler(){
			public void onCompletion(String responseText){
				content.setHTML(responseText);
			}
		});
		setWidget(content);
		setStyleName("content_entry");
	}
	
	public HTML getContent() {
		return content;
	}
}
