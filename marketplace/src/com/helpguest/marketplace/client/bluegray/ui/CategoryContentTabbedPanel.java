package com.helpguest.marketplace.client.bluegray.ui;

import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.history.HistoryCommander;

public class CategoryContentTabbedPanel extends SimplePanel implements ClickListener {
	final public static int design = 0;
	final public static int development = 1;
	final public static int business = 2;
	final public static int systems = 3;
	final public static int personal = 4;
	
	final public static String CONTENT_KEY = "category";
	
	final public static String DESIGN = "DESIGN";
	final public static String DEVELOPMENT = "DEVELOPMENT";
	final public static String BUSINESS = "BUSINESS";
	final public static String SYSTEMS = "SYSTEMS";
	final public static String PERSONAL = "PERSONAL";
	
	final public static String DESIGN_TOKEN = HistoryCommander.buildToken(FindExpert.TOKEN, CONTENT_KEY, DESIGN);
	final public static String DEVELOPMENT_TOKEN = HistoryCommander.buildToken(FindExpert.TOKEN, CONTENT_KEY, DEVELOPMENT);
	final public static String BUSINESS_TOKEN = HistoryCommander.buildToken(FindExpert.TOKEN, CONTENT_KEY, BUSINESS);
	final public static String SYSTEMS_TOKEN = HistoryCommander.buildToken(FindExpert.TOKEN, CONTENT_KEY, SYSTEMS);
	final public static String PERSONAL_TOKEN = HistoryCommander.buildToken(FindExpert.TOKEN, CONTENT_KEY, PERSONAL);
	
	private OfferingDisplay designDisplay ;
	private OfferingDisplay developmentDisplay ;
	private OfferingDisplay businessDisplay ;
	private OfferingDisplay systemsDisplay ;
	private OfferingDisplay personalDisplay ;
	
	final TabWidget tabDesign = new TabWidget(DESIGN);
	final TabWidget tabDevelopment = new TabWidget(DEVELOPMENT);
	final TabWidget tabBusiness = new TabWidget(BUSINESS);
	final TabWidget tabSystems = new TabWidget(SYSTEMS);
	final TabWidget tabPersonal = new TabWidget(PERSONAL);
	private TabPanel categoryTab = new TabPanel();
	
	public CategoryContentTabbedPanel(){}
	public CategoryContentTabbedPanel (FindExpert findExpert){
		designDisplay = new OfferingDisplay(findExpert);
		developmentDisplay = new OfferingDisplay(findExpert);
		businessDisplay = new OfferingDisplay(findExpert);
		systemsDisplay = new OfferingDisplay(findExpert);
		personalDisplay = new OfferingDisplay(findExpert);
		categoryTab.add(designDisplay,tabDesign);
		categoryTab.add(developmentDisplay,tabDevelopment);
		categoryTab.add(businessDisplay, tabBusiness);
		categoryTab.add(systemsDisplay, tabSystems);
		categoryTab.add(personalDisplay, tabPersonal);
		
		categoryTab.getTabBar().setStyleName("category_content_tabbed_panel_tabbar");
		categoryTab.setStyleName("category_content_tabbed_panel_category_tab");
		
		/* Add clicklistener to each tab */
		tabDesign.addClickListener(this);
		tabDevelopment.addClickListener(this);
		tabBusiness.addClickListener(this);
		tabSystems.addClickListener(this);
		tabPersonal.addClickListener(this);
		
	    setWidget(categoryTab);
		setStyleName("category_content_tabbed_panel");
	}
	
	public void setContent(final int index) {
		categoryTab.selectTab(index);
		switch(index) {
		case design:
			designDisplay.setCategory(DESIGN);
			designDisplay.searchOffering();
			break;
		case development:
			developmentDisplay.setCategory(DEVELOPMENT);
			developmentDisplay.searchOffering();
			break;
		case business:
			businessDisplay.setCategory(BUSINESS);
			businessDisplay.searchOffering();
			break;
		case systems:
			systemsDisplay.setCategory(SYSTEMS);
			systemsDisplay.searchOffering();
			break;
		case personal:
			personalDisplay.setCategory(PERSONAL);
			personalDisplay.searchOffering();
			break;
		}
	}
	
   /* public void setTabSelection(final String token, final int index) {
    	CommandToken command = new CommandToken(){
    		public String getBaseToken(){
    			return FindExpert.TOKEN+HistoryCommander.BASE_SEPARATOR+CONTENT_KEY+HistoryCommander.VALUE_SEPARATOR+token;
    		}
    		
    		public void onEntry(final Map kvMap){
    			categoryTab.selectTab(index);
    			String content = (String)kvMap.get(CONTENT_KEY);
    			if(content == null){
    				designDisplay.setCategory(token);
    				designDisplay.searchOffering();
    			}else if(content == DESIGN){
    				designDisplay.setCategory(token);
    				designDisplay.searchOffering();
    			}else if(content == DEVELOPMENT){
    				developmentDisplay.setCategory(token);
    				developmentDisplay.searchOffering();
    			}else if(content == BUSINESS){
    				businessDisplay.setCategory(token);
    				businessDisplay.searchOffering();
    			}else if(content == SYSTEMS){
    				systemsDisplay.setCategory(token);
    				systemsDisplay.searchOffering();
    			}else if(content == PERSONAL){
    				personalDisplay.setCategory(token);
    			    personalDisplay.searchOffering();
    			}
    		}
    		
    		public void onExit(){
    			;
    		}
    	};
    	HistoryCommander.getInstance().register(command);
    }*/
	
	public void onClick(Widget sender){
		if (sender == tabDesign){
			setContent(design);
		}else if (sender == tabDevelopment ){
			setContent(development);
		}else if (sender == tabBusiness){
			setContent(business);
		}else if (sender == tabSystems){
			setContent(systems);
		}else if (sender == tabPersonal){
			setContent(personal);
		}
	}
}
