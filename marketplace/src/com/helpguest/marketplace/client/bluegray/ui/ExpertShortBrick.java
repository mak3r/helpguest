package com.helpguest.marketplace.client.bluegray.ui;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.RoundedImage;
import com.helpguest.marketplace.client.ui.StarRating;
import com.helpguest.marketplace.client.util.DebugPanel;

public class ExpertShortBrick extends FocusPanel implements ClickListener{
	final HorizontalPanel hp = new HorizontalPanel();
	final HorizontalPanel top = new HorizontalPanel();
	final VerticalPanel vp = new VerticalPanel();
	final HorizontalPanel rateBrick = new HorizontalPanel();
	final HorizontalPanel completed_project = new HorizontalPanel();
	final HorizontalPanel avgTime = new HorizontalPanel();
	private int number;
	private Label labelHandle = new Label();
	private Label rate = new Label();
	private Label labelProject = new Label();
	private Label projectNo = new Label();
	private Label labelTime = new Label();
	private Label time = new Label();
	private String queryID;
	private ExpertDTO dto;
	public ExpertShortBrick(final ExpertDTO dto,final String queryID, final int number){
		this.number = number;
		this.queryID = queryID;
		this.dto = dto;
		/**The avatar brick*/
		Image image = new Image(dto.getAvatarURL());
		image.setWidth("70px");
		image.setHeight("78px");
		addClickListener(this);
		RoundedImage roundedImage = new RoundedImage(image,"avatar_round_image",5);
		
		/** The rate brick */
	
		labelHandle.setText(dto.getHandle()+"-");
		rate.setText(dto.getPaymentPlan().getRatePerMinute() + "/minute");
		labelHandle.setStyleName("expert_short_brick_label_handle");
		rate.setStyleName("expert_short_brick_rate");
		rateBrick.add(labelHandle);
		rateBrick.add(rate);
		
		/** Rating star brick */
		StarRating star = new StarRating(dto.getStats().getSatisfactionRating());
		
		
		/** Completed Project brick */
		
		labelProject.setText("COMPLETED PROJECTS -");
		projectNo.setText(dto.getStats().getNumberResolved());
		labelProject.setStyleName("expert_short_brick_label_project");
		projectNo.setStyleName("expert_short_brick_projectNo");
		completed_project.add(labelProject);
		completed_project.add(projectNo);
		
		/** Average time brick */
	
		labelTime.setText("AVERAGE TIME - ");
		time.setText(dto.getStats().getAvgTimeToResolution()+"minutes");
		labelTime.setStyleName("expert_short_brick_label_time");
		time.setStyleName("expert_short_brick_time");
		avgTime.add(labelTime);
		avgTime.add(time);
		
		top.add(new CategoryIcon(dto.getExpertUid()));
		top.add(star);
		vp.add(top);
		vp.add(rateBrick);
		vp.add(completed_project);
		vp.add(avgTime);
		hp.add(image);
		hp.add(vp);
		setWidget(hp);
		image.setStyleName("expert_short_brick_image");
		rateBrick.setStyleName("expert_short_brick_rateBrick");
		star.setStyleName("expert_short_brick_star");
		completed_project.setStyleName("expert_short_brick_completed_project");
		avgTime.setStyleName("expert_short_brick_avgTime");
		if(number % 2 == 1){
			image.setStyleName("expert_short_brick_avatar_even");
			star.setStyleName("expert_short_brick_star_even");
			setStyleName("expert_short_brick_even");
			
		}
		else{
			image.setStyleName("expert_short_brick_avatar_odd");
			star.setStyleName("expert_short_brick_star_odd");
			setStyleName("expert_short_brick_odd");
		}
	}
	
	public void onClick(Widget sender){
		History.newItem(ExpertPreviewsHandler.getExpertPreviewToken(dto.getExpertUid()));
	}
}
