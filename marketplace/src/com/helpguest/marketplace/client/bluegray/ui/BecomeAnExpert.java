package com.helpguest.marketplace.client.bluegray.ui;



import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.protocol.SignUpOutcome;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.brick.InputBrick;
import com.helpguest.marketplace.client.brick.VerticalBrick;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.history.SubCommandToken;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.ui.VerticalSpacer;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.widgets.EmailValidator;
import com.helpguest.marketplace.client.widgets.ExpertTagsWidget;
import com.helpguest.marketplace.client.widgets.GetNewPswdToResetPswdPanel;
import com.helpguest.marketplace.client.widgets.GetUserEmailToResetPswd;
import com.helpguest.marketplace.client.widgets.HelpGuestInfoOK;
import com.helpguest.marketplace.client.widgets.OopsWidget;
import com.helpguest.marketplace.client.widgets.VisualTimer;

public class BecomeAnExpert extends SimplePanel implements SubCommandToken, ResultContainer {
	
	
	private VerticalPanel errors = new VerticalPanel();
	public static final String TOKEN = "become_an_expert";
	
	private VerticalPanel expertSignUpForm = new VerticalPanel();
	private VerticalPanel top = new VerticalPanel();
	private VerticalPanel mid = new VerticalPanel();
	private VerticalPanel bottom = new VerticalPanel();
	private Label topLabel = new Label("EXPERT SIGNUP HERE");
	private Label midLabel = new Label("ACCOUNT INFORMATION");
	private Label bottomLabel = new Label("EXPERTISE CLASSFICATION");
	private Label bottomDescription = new Label("Create tags to describe your expertise.  Select a category and offering.  Enter 1 tag per line.");
	private InputBrick email = new InputBrick("Your Email Address","15em");
	private InputBrick userName = new InputBrick("UserName");
	public InputBrick password = new InputBrick("Password","10em", InputBrick.PASSWORD_TYPE);
	public InputBrick confirmPassword = new InputBrick("Confirm Password","10em", InputBrick.PASSWORD_TYPE);
	private InputBrick zipCode = new InputBrick("Zip Code","5em");
	private InputBrick source = new InputBrick("Where did you find out about HelpGuest?", null, InputBrick.LIST_BOX_TYPE);
	private final static String defaultSource = "--select one--";
    private ContentEntry becomeAnExpertHeader = new ContentEntry("becomeAnExpertHeader.div");
    private HelpGuestInfoOK signUpThanks = new HelpGuestInfoOK(this);
    
	private Button submit = new Button("Sign Up Now");
	private static final String agreeStatement = 
        "<font style=\"font-size: smaller;\">I have read and agree with the&nbsp;" +
        "<a href=\"ExpertEULA.html\" target=\"_blank\">" +
        "terms of use" +
        "</a>" +
        "&nbsp;and the&nbsp;" +
        "<a href=\"RateCalculation.html\" target=\"_blank\">" +
        "payment plan" +
        "</a>" +
        "&nbsp;and I understand the&nbsp;" +
        "<a href=\"PrivacyPolicy.html\" target=\"_blank\">" +
        "privacy policy" +
        "</a>." + 
        "</font>";
	private CheckBox terms = new CheckBox(agreeStatement, true);

	private Label required = new Label("All fields below are required");
	private static final Label zipError =
		new Label("Your zip code must be exactly 5 digits.");
	//36 matches the field width in the db
	private static final int userNameLength = 36;
	private static final Label userNameErrorLength =
		new Label("Your userName cannot be more than " + userNameLength + " characters.");
	private static final Label userNameErrorContent =
		new Label("Your userName may only contain letters, numbers and underscores.");
	private static final VerticalBrick userNameErrors = new VerticalBrick(userNameErrorLength, userNameErrorContent);
	private static final Label passwordErrorLength =
		new Label("Password must be between 6 and 50 characters.");
	private static final Label passwordErrorContent =
		new Label("Password may contain letters, numbers, and (*$&#!_-^+).");
	private static final VerticalBrick passwordErrors = new VerticalBrick(passwordErrorLength, passwordErrorContent);
	private static final Label confirmPasswordError =
		new Label("Password and confirm password must match.");
	private static final Label emailError =
		new Label("Email address is invalid.");
	private static final Label primaryExpertiseError =
		new Label("Primary expertise may contain letters, numbers and spaces.");
	private static final int primaryExpertiseLength = 50;
	private static final Label primaryExpertiseErrorLength =
		new Label("Primary expertise cannot be more than " + primaryExpertiseLength + " characters.");
	private static final Label tagsListIsEmpty =
		new Label("At least one tag must be entered for any category and offering.");
	private static final Label sourceUnselected =
		new Label("Please indicate how you learned about HelpGuest.");
	
	private static final String ERROR_STYLE = "hg-error";
	private static final String HIDE_STYLE = "hide";
	private static final String SHOW_STYLE = "show";
	private VisualTimer creatingAccount = new VisualTimer("Creating your account ", 10);
	
	//prahalad adding 07/24/08
	ExpertTagsWidget expertTagsWidget = new ExpertTagsWidget(null);
	
	private CommandToken parentCommand;
	
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	public BecomeAnExpert(final CommandToken parentCommand) {
		this();
		this.parentCommand = parentCommand;
	}
	
	public BecomeAnExpert(){
		//Specify the url where the expert service is running
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String moduleRelativeURLExpertService = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(moduleRelativeURLExpertService);
		
		terms.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				submit.setEnabled(terms.isChecked());
			}
			
		});
		
		submit.addClickListener(new ClickListener(){

			public void onClick(Widget sender) {
				if (contentIsValidated()) {
					signUpThanks.resetInfo(getSignUpThanks());
					creatingAccount.start();
					errors.add(creatingAccount);
					expertService.createAccount(
							email.getInputText(), 
							password.getInputText(), 
							userName.getInputText(),
							expertTagsWidget.getCatOffgTagList(), 
							zipCode.getInputText(),
							source.getInputText(),
							expertCallback);
				}				
			}
			
		});
		
		init();
		initStyles();
	}
	
	private void init() {
		//FIXME this should be pulled from a fixed data source that it
		// can be validated against on submit.
		source.setInputText(defaultSource);
	    source.setInputText("From a friend, relative or acquaintance");
	    source.setInputText("A blog post");
	    source.setInputText("A story in an online or offline publication");
	    source.setInputText("Google or other search engine");
	    source.setInputText("At an event or workshop");
	    source.setInputText("On Twitter");
	    source.setInputText("Other");
	    		
	    top.add(topLabel);
	    top.add(becomeAnExpertHeader);
	    top.add(errors);
	    mid.add(midLabel);
	    mid.add(email);
	    mid.add(userName);
	    mid.add(password);
	    mid.add(confirmPassword);
	    mid.add(zipCode);
	    mid.add(source);
	    bottom.add(bottomLabel);
	    bottom.add(bottomDescription);
	    bottom.add(tagsListIsEmpty);
	    bottom.add(expertTagsWidget);

		submit.setEnabled(false);
		terms.setChecked(false);
		
		HorizontalPanel paddedSubmit = new HorizontalPanel();
		paddedSubmit.add(submit);
		paddedSubmit.add(new VerticalSpacer(30));
		
		VerticalPanel agreeAndSubmit = new VerticalPanel();
		agreeAndSubmit.add(terms);
		agreeAndSubmit.setCellHorizontalAlignment(terms, VerticalPanel.ALIGN_LEFT);
		agreeAndSubmit.add(paddedSubmit);
		agreeAndSubmit.setCellHorizontalAlignment(paddedSubmit, VerticalPanel.ALIGN_RIGHT);
		agreeAndSubmit.setSpacing(20);
		
	    HorizontalPanel secureAndButtons = new HorizontalPanel();
		//secureAndButtons.add(new SiteSeal());
		secureAndButtons.add(agreeAndSubmit);
		secureAndButtons.setCellHorizontalAlignment(secureAndButtons, HorizontalPanel.ALIGN_RIGHT);
		
		expertSignUpForm.add(top);
		expertSignUpForm.add(mid);
		expertSignUpForm.add(bottom);
		expertSignUpForm.add(secureAndButtons);
	    setWidget(expertSignUpForm);


	}
	
	private void initStyles() {
		required.addStyleName(ERROR_STYLE);
		zipError.addStyleName(ERROR_STYLE);
		userNameErrors.addStyleName(ERROR_STYLE);
		passwordErrorLength.addStyleName(ERROR_STYLE);
		passwordErrorContent.addStyleName(ERROR_STYLE);
		confirmPasswordError.addStyleName(ERROR_STYLE);
		emailError.addStyleName(ERROR_STYLE);
		primaryExpertiseError.addStyleName(ERROR_STYLE);
		primaryExpertiseErrorLength.addStyleName(ERROR_STYLE);
		tagsListIsEmpty.addStyleName(ERROR_STYLE);
		tagsListIsEmpty.setStylePrimaryName(HIDE_STYLE);
		sourceUnselected.addStyleName(ERROR_STYLE);
		
	    //set up styleName
	    expertSignUpForm.setStyleName("become_an_expert_vp");
	    top.setStyleName("become_an_expert_top");
	    mid.setStyleName("become_an_expert_mid");
	    bottom.setStyleName("become_an_expert_bottom");
	    topLabel.setStyleName("become_an_expert_topLabel");
	    midLabel.setStyleName("become_an_expert_midLabel");
	    bottomLabel.setStyleName("become_an_expert_bottomLabel");
	    bottomDescription.setStyleName("become_an_expert_bottom_description");
	    email.setStyleName("become_an_expert_email");
	    userName.setStyleName("become_an_expert_username");
	    password.setStyleName("become_an_expert_password");
	    confirmPassword.setStyleName("become_an_expert_confirm_password");
	    zipCode.setStyleName("become_an_expert_zip_code");
	    source.setStyleName("become_an_expert_source");
	}
	
	private boolean contentIsValidated() {
		errors.clear();
		return 
			generalCheck() 
			& emailCheck()
			& passwordCheck()
			& userNameCheck() 
			& zipCheck()
			& tagListEmptyCheck()
			& sourceSelectedCheck();
	}
	
	private boolean generalCheck() {
		if ("".equals(zipCode.getInputText().trim())
				|| "".equals(userName.getInputText().trim())
				|| "".equals(password.getInputText().trim())
				|| "".equals(confirmPassword.getInputText().trim())
				|| "".equals(email.getInputText().trim())) {
			errors.add(required);
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @return true if the zip code validates.  False
	 * if it does not validate
	 */
	private boolean zipCheck() {
		String zip = zipCode.getInputText();
		if (!zip.matches("[0-9]{5}")) {
			//errors.add(zipError);
			zipCode.setInputAccessory(zipError, InputBrick.RIGHT);
			zipError.addStyleName(SHOW_STYLE);
			zipError.removeStyleName(HIDE_STYLE);
			return false;
		} 
		zipError.addStyleName(HIDE_STYLE);
		zipError.removeStyleName(SHOW_STYLE);			

		return true;
	}
	
	private boolean userNameCheck() {
		String userNameValue = userName.getInputText();
		boolean ok = true;
		userName.setInputAccessory(userNameErrors, InputBrick.RIGHT);

		if (userNameValue.length() > userNameLength) {
			userNameErrorLength.addStyleName(SHOW_STYLE);
			userNameErrorLength.removeStyleName(HIDE_STYLE);
			ok = false;
		} else {
			userNameErrorLength.addStyleName(HIDE_STYLE);
			userNameErrorLength.removeStyleName(SHOW_STYLE);			
		}
		
		if (!userNameValue.matches("[\\w]*")) {
			//errors.add(userNameErrorContent);
			userNameErrorContent.addStyleName(SHOW_STYLE);
			userNameErrorContent.removeStyleName(HIDE_STYLE);
			ok = false;
		} else {
			userNameErrorContent.addStyleName(HIDE_STYLE);
			userNameErrorContent.removeStyleName(SHOW_STYLE);			
		} 
		
		return ok;
	}
	
	private boolean passwordCheck() {
		String passwordValue = password.getInputText();
		String confirmPasswordValue = confirmPassword.getInputText();
		boolean ok = true;
		password.setInputAccessory(passwordErrors, InputBrick.RIGHT);
		
		if (!passwordValue.matches("[\\w\\*\\&\\#\\!\\^\\+\\$-]*")) {
			passwordErrorContent.addStyleName(SHOW_STYLE);
			passwordErrorContent.removeStyleName(HIDE_STYLE);
			ok = false;
		} else {
			passwordErrorContent.addStyleName(HIDE_STYLE);
			passwordErrorContent.removeStyleName(SHOW_STYLE);			
		}

		if (passwordValue.length() < 6 || 
				passwordValue.length() > 50) {
			passwordErrorLength.addStyleName(SHOW_STYLE);
			passwordErrorLength.removeStyleName(HIDE_STYLE);
			ok = false;
		} else {
			passwordErrorLength.addStyleName(HIDE_STYLE);
			passwordErrorLength.removeStyleName(SHOW_STYLE);			
		}
		
		if (passwordValue != null && !passwordValue.equals(confirmPasswordValue)) {
			//errors.add(confirmPasswordError);
			confirmPassword.setInputAccessory(confirmPasswordError, InputBrick.RIGHT);
			confirmPasswordError.addStyleName(SHOW_STYLE);
			confirmPasswordError.removeStyleName(HIDE_STYLE);
			ok = false;
		} else {
			confirmPasswordError.addStyleName(HIDE_STYLE);
			confirmPasswordError.removeStyleName(SHOW_STYLE);
		}
		return ok;
	}
	
	private boolean emailCheck() {
		String emailValue = email.getInputText();
		if (!emailValue.matches("[\\w\\.\\+%-]+@[\\w\\.-]+\\.[\\w]{2,4}")) {
			email.setInputAccessory(emailError, InputBrick.RIGHT);
			emailError.addStyleName(SHOW_STYLE);
			emailError.removeStyleName(HIDE_STYLE);
			return false;
		}
		emailError.addStyleName(HIDE_STYLE);
		emailError.removeStyleName(SHOW_STYLE);
		return true;
	}
	
	//prahalad adding 07/24/08
	private boolean tagListEmptyCheck(){
		List tagsList = expertTagsWidget.getCatOffgTagList();
		int emptyCount = 0;
		boolean ok = true;
		for (int listIt = 0;listIt<tagsList.size();listIt++)
		{
			String catOffTag = tagsList.get(listIt).toString();
			String tag = catOffTag.substring(catOffTag.indexOf(':', catOffTag.indexOf(':') + 1) + 1).trim();
			if (!tag.equals("")){
				emptyCount++;
			}
		}
		
		if (emptyCount==0){
			ok = false;
			//errors.add(tagsListIsEmpty);
			tagsListIsEmpty.removeStyleName(HIDE_STYLE);
			tagsListIsEmpty.addStyleName(SHOW_STYLE);
		} else {
			tagsListIsEmpty.removeStyleName(SHOW_STYLE);
			tagsListIsEmpty.addStyleName(HIDE_STYLE);
 		}
		return ok;
		
	}
	
	private boolean sourceSelectedCheck() {
		boolean ok = false;
		if (!source.getInputText().equals(defaultSource)) {
			sourceUnselected.addStyleName(HIDE_STYLE);
			sourceUnselected.removeStyleName(SHOW_STYLE);
			ok = true;
		} else {
			source.setInputAccessory(sourceUnselected, InputBrick.RIGHT);
			sourceUnselected.addStyleName(SHOW_STYLE);
			sourceUnselected.removeStyleName(HIDE_STYLE);
		}
		
		return ok;
	}
	
	private void clearForm() {
		email.setInputText(null);
		password.setInputText(null);
		confirmPassword.setInputText(null);
		userName.setInputText(null);
		expertTagsWidget.resetTagsWidget();
		zipCode.setInputText(null);
		source.setInputText(null);
		terms.setChecked(false);		
		submit.setEnabled(false);
		errors.clear();
	}
	
	AsyncCallback expertCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			creatingAccount.stop();
			errors.clear();
			setWidget(new OopsWidget());
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				DebugPanel.setText("BecomeAnExpert", "SignUpPanel expertCallback: " +  sBuf.toString());
			} catch (Throwable th) {
				DebugPanel.setText("BecomeAnExpert", "SignUpPanel expertCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			DebugPanel.setText("BecomeAnExpert", "onSuccess outcome from the expertCallback()");
			creatingAccount.stop();
			errors.clear();
			SignUpOutcome outcome = (SignUpOutcome) result;
			DebugPanel.setText("BecomeAnExpert", "expertCallback() outcome: " + outcome.getMessage());
			if (outcome.equals(new SignUpOutcome.Success())) {
				setWidget(signUpThanks);
			} else {
				//TODO send the new user somewhere when the outcome is success
				Label finalOutcome = new Label(outcome.toString());
				finalOutcome.addStyleName(ERROR_STYLE);
				errors.add(finalOutcome);
			}
		}		
	};
	
	private HTML getSignUpThanks() {
		StringBuffer signUpContent = new  StringBuffer();
		signUpContent.append("<div class=\"sign_up_thanks\" >");
		signUpContent.append("<p>Thanks for signing up. </p>");
		signUpContent.append("<p>A verification email has been sent to&nbsp;");
		signUpContent.append(email.getInputText());
		signUpContent.append(".</p>");
		signUpContent.append("<p>After validating your email by using the link we sent you, you will be able to log in to the account you just created.</p>");
		signUpContent.append("</div>");
		return new HTML(signUpContent.toString());
	}

	public String getBaseToken() {
		return TOKEN;
	}

	public void onEntry(Map<String, String> kvMap) {
		//There are currently only two possibilities
		// one is the main page, the other is the validation page
		if (kvMap != null) {
			if (kvMap.containsKey(EmailValidator.EMAIL_ADDRESS_KEY)) {
				EmailValidator emailValidator = new EmailValidator(this);
				this.setWidget(emailValidator);
				emailValidator.onEntry(kvMap);
			} else if (kvMap.containsKey(GetUserEmailToResetPswd.TOKEN)) {
				//We should never have the email address key and forgot password key together on one command
				GetUserEmailToResetPswd resetPassword = new GetUserEmailToResetPswd(this);
				this.setWidget(resetPassword);
			} else if (kvMap.containsKey(GetNewPswdToResetPswdPanel.TOKEN)) {
				GetNewPswdToResetPswdPanel getNewPswdReset = new GetNewPswdToResetPswdPanel(this);
				this.setWidget(getNewPswdReset);
				getNewPswdReset.onEntry(kvMap);
			}

			
		}
	}

	public void onExit() {
		clearForm();
		creatingAccount.stop();
	}

	public String getDefaultToken() {
		return TOKEN;
	}

	public void reset() {
	    clearForm();
	    HistoryCommander.newItem(
	    		HistoryCommander.buildToken(
	    				parentCommand.getBaseToken(), 
	    				TabbedContentPanel.getTabKey(), 
	    				TOKEN));
		this.setWidget(expertSignUpForm);
	}

	public void setParent(CommandToken parentCommand) {
		this.parentCommand = parentCommand;
	}
}
