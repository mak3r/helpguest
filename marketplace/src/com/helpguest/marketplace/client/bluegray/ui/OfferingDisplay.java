package com.helpguest.marketplace.client.bluegray.ui;

/**
 * @author ying
 */
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.marketplace.client.SearchService;
import com.helpguest.marketplace.client.SearchServiceAsync;
import com.helpguest.marketplace.client.util.DebugPanel;

public class OfferingDisplay extends SimplePanel {
	private final int maxOffering = 20;
	private final String DESIGN = "design";
	private final String DEVELOPMENT = "development";
	private final String BUSINESS = "business";
	private final String SYSTEMS = "systems";
	private final String PERSONAL = "personal";
	private HorizontalPanel hp = new HorizontalPanel();
	private VerticalPanel vpOdd = new VerticalPanel();
	private VerticalPanel vpEven = new VerticalPanel();
	private FindExpert findExpert;
	private String offer;
	final SearchServiceAsync searchService = (SearchServiceAsync) GWT
			.create(SearchService.class);
	private List offerings = new ArrayList();
	private CategoryConstraints categoryConstraints = new CategoryConstraints();

	public OfferingDisplay(FindExpert findExpert) {
		this.findExpert = findExpert;
		// set up the url
		ServiceDefTarget endpoint = (ServiceDefTarget) this.searchService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "SearchService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		hp.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
		hp.add(vpEven);
		hp.add(vpOdd);
		// categoryConstraints.setCategoryName(category);
		// searchService.getOfferings(categoryConstraints, callback);
	}

	public void setCategory(String category) {
		categoryConstraints.setCategoryName(category);
	}

	public void searchOffering() {
		ServiceDefTarget endpoint = (ServiceDefTarget) this.searchService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "SearchService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		DebugPanel.setText("OfferingDisplay",
				"getting offerings with category constraints: "
						+ categoryConstraints.getCategoryName());
		searchService.getOfferings(categoryConstraints, callback);
		DebugPanel.setText("OfferingDisplay",
				"Done getting offerings with category constraints: "
						+ categoryConstraints.getCategoryName());
	}

	// display the offerings from the list
	public void display() {
		int offeringSize;
		Hyperlink offering;
		List display = new ArrayList();
		// Check whether offering size is larger than 20
		if (offerings.size() <= maxOffering) {
			offeringSize = offerings.size();
		} else {
			offeringSize = maxOffering;
		}

		vpEven.clear();
		vpOdd.clear();

		for (int i = 0; i < offeringSize; i++) {
			String query = (String)offerings.get(i);
			String historyToken = SearchResultsHandler.getSearchToken(query);
			offering = new Hyperlink(query, historyToken);
			// offering.setText((String)offerings.get(i));
			/*
			offering.addClickListener(new ClickListener() {
				public void onClick(Widget sender) {
					String query = ((Hyperlink) sender).getText();
					SearchWidget searchWidget = findExpert.getSearchWidget();
					searchWidget.setQuery(query);
					searchWidget.search();
				}
			});
			*/
			if (i % 2 == 0) {
				vpEven.add(offering);
			} else {
				vpOdd.add(offering);
			}
			offering.setStyleName("offering_display_offerings_hyperlink");
		}
		setWidget(hp);
		setStyleName("offering_display_display");
	}

	AsyncCallback callback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				// Window.alert("OfferingDisplay callback: " + sBuf.toString());
			} catch (Throwable th) {
				// Window.alert("OfferingDisplay callback [throwable]: " +
				// th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			offerings = (List) result;
			display();
		}

	};

}
