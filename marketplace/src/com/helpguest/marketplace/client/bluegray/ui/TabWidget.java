package com.helpguest.marketplace.client.bluegray.ui;
/**
 * @author ying
 */
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.ui.VisualCuesImageBundle;


/**
 * defined as a widget which will be displayed at the tab;
 */
public class TabWidget extends FocusPanel{
	private String DESIGN = "DESIGN";
	private String DEVELOPMENT = "DEVELOPMENT";
	private String BUSINESS= "BUSINESS";
	private String SYSTEMS = "SYSTEMS";
	private String PERSONAL = "PERSONAL";
	
	
	
	private VerticalPanel vp = new VerticalPanel();
	private VisualCuesImageBundle visualCue = (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);
	private SimplePanel pad = new SimplePanel();
	private Label label;
	private Label countLabel = new Label("0");
	private boolean isKeepingCount = false;
	
	/**
	 * Insert the result count on this TabWidget with title.
	 */
	public TabWidget(final String title, final boolean keepCount){
		vp.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);

		//add in an extra row to keep count of how many experts are in the group.
		if (keepCount) {
			isKeepingCount = keepCount;
			vp.add(countLabel);
			vp.setCellHorizontalAlignment(countLabel, HorizontalPanel.ALIGN_RIGHT);
			countLabel.addStyleName("category_content_tabbed_panel_tabwidget_count");
		} 
		
		if (title.equals(DESIGN)){
			Label design = new Label(title);
			label = design;
			pad.add(visualCue.design().createImage());
			vp.add(pad);
			vp.add(design);
			pad.setStylePrimaryName("category_content_tabbed_panel_tabwidget_pad");
			design.setStyleName("category_content_tabbed_panel_tabwidget_text");
			setWidget(vp);
		}else if (title.equals(DEVELOPMENT)){
			Label development = new Label(title);
			label = development;
			pad.add(visualCue.development().createImage());
			vp.add(pad);
			vp.add(development);
			pad.setStylePrimaryName("category_content_tabbed_panel_tabwidget_pad");
			development.setStyleName("category_content_tabbed_panel_tabwidget_text");
			setWidget(vp);
		}else if (title.equals(BUSINESS)){
			Label business = new Label(title);
			label = business;
			pad.add(visualCue.business().createImage());
			vp.add(pad);
			vp.add(business);
			pad.setStylePrimaryName("category_content_tabbed_panel_tabwidget_pad");
			business.setStyleName("category_content_tabbed_panel_tabwidget_text");
			setWidget(vp);
		}else if (title.equals(SYSTEMS)){
			Label systems = new Label(title);
			label = systems;
			pad.add(visualCue.system().createImage());
			vp.add(pad);
			vp.add(systems);
			pad.setStylePrimaryName("category_content_tabbed_panel_tabwidget_pad");
			systems.setStyleName("category_content_tabbed_panel_tabwidget_text");
			setWidget(vp);
		}else if (title.equals(PERSONAL)){
			Label personal = new Label(title);
			label = personal;
			pad.add(visualCue.personal().createImage());
			vp.add(pad);
			vp.add(personal);
			pad.setStylePrimaryName("category_content_tabbed_panel_tabwidget_pad");
			personal.setStyleName("category_content_tabbed_panel_tabwidget_text");
			setWidget(vp);
		}
		setStyleName("category_content_tabbed_panel_tabwidget");
		
	}
	
	/**
	 * Create a TabWidget wihout any result count value
	 * @param title
	 */
	public TabWidget(String title){
		this(title, false);
	}
	
	public Label getLabel(){
		return label;
	}
	
	/**
	 * if this TabWidget is keeping count then 
	 * set the count otherwise do nothing.
	 * @param count
	 */
	public Label getCountLabel() {
		if (isKeepingCount) {
			return countLabel;
		}
		return null;
	}
}

