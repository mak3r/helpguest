package com.helpguest.marketplace.client.bluegray.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.BodyPanel;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.ui.SearchPanel;

public class ExpertCategoryDisplay extends SimplePanel implements ClickListener {
	public static final String design = "DESIGN";
	public static final String development = "DEVELOPMENT";
	public static final String business = "BUSINESS";
	public static final String systems = "SYSTEMS";
	public static final String personal = "PERSONAL";
	private List currentExpertList = new ArrayList();
	final BodyPanel body = new BodyPanel();
	private String query;

	private VerticalPanel vp = new VerticalPanel();
	private HorizontalPanel hp = new HorizontalPanel();

	public final TabPanel tab = new TabPanel();
	public final static int DESIGN = 0;
	public final static int DEVELOPMENT = 1;
	public final static int BUSINESS = 2;
	public final static int SYSTEMS = 3;
	public final static int PERSONAL = 4;

	public String DESIGN_TOKEN;
	public String DEVELOPMENT_TOKEN;
	public String BUSINESS_TOKEN;
	public String SYSTEMS_TOKEN;
	public String PERSONAL_TOKEN;

	public VerticalPanel designTab = new VerticalPanel();
	public VerticalPanel developmentTab = new VerticalPanel();
	public VerticalPanel businessTab = new VerticalPanel();
	public VerticalPanel systemsTab = new VerticalPanel();
	public VerticalPanel personalTab = new VerticalPanel();

	private SimplePanel designLink = new SimplePanel();
	private SimplePanel developmentLink = new SimplePanel();
	private SimplePanel businessLink = new SimplePanel();
	private SimplePanel systemsLink = new SimplePanel();
	private SimplePanel personalLink = new SimplePanel();

	private TabWidget designWidget = new TabWidget("DESIGN", true);
	private TabWidget developmentWidget = new TabWidget("DEVELOPMENT", true);
	private TabWidget businessWidget = new TabWidget("BUSINESS", true);
	private TabWidget systemsWidget = new TabWidget("SYSTEMS", true);
	private TabWidget personalWidget = new TabWidget("PERSONAL", true);

	private ExpertLongBrickList designList;
	private ExpertLongBrickList developmentList;
	private ExpertLongBrickList businessList;
	private ExpertLongBrickList systemsList;
	private ExpertLongBrickList personalList;
	
	private Label label = new Label("FIND AN EXPERT");
	private HTML html = new HTML(
			"The expert you are searching for is listed below:");
	private SearchWidget searchWidget = null;

	final ExpertServiceAsync expertService = (ExpertServiceAsync) GWT
			.create(ExpertService.class);

	public int number = 0;

	public ExpertCategoryDisplay(String query, ResultContainer resultContainer) {
		this.query = query;
		ServiceDefTarget endpoint = (ServiceDefTarget) this.expertService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		designList = new ExpertLongBrickList(resultContainer, query);
		developmentList = new ExpertLongBrickList(resultContainer, query);
		businessList = new ExpertLongBrickList(resultContainer, query);
		systemsList = new ExpertLongBrickList(resultContainer, query);
		personalList = new ExpertLongBrickList(resultContainer, query);

		searchWidget = SearchWidget.newInstance(new String(""), new String(
				"YOUR CURRENT SEARCH"), resultContainer, SearchPanel.two);

		DESIGN_TOKEN = SearchResultsHandler.getSearchToken(query, design);
		DEVELOPMENT_TOKEN = SearchResultsHandler.getSearchToken(query,
				development);
		BUSINESS_TOKEN = SearchResultsHandler.getSearchToken(query, business);
		SYSTEMS_TOKEN = SearchResultsHandler.getSearchToken(query, systems);
		PERSONAL_TOKEN = SearchResultsHandler.getSearchToken(query, personal);

		designList.setCountLabel(designWidget.getCountLabel());
		developmentList.setCountLabel(developmentWidget.getCountLabel());
		businessList.setCountLabel(businessWidget.getCountLabel());
		systemsList.setCountLabel(systemsWidget.getCountLabel());
		personalList.setCountLabel(personalWidget.getCountLabel());
		
		// add long brick list
		designTab.add(designList);
		developmentTab.add(developmentList);
		businessTab.add(businessList);
		systemsTab.add(systemsList);
		personalTab.add(personalList);

		// add link
		designTab.add(designLink);
		developmentTab.add(developmentLink);
		businessTab.add(businessLink);
		systemsTab.add(systemsLink);
		personalTab.add(personalLink);

		tab.add(designTab, designWidget);
		tab.add(developmentTab, developmentWidget);
		tab.add(businessTab, businessWidget);
		tab.add(systemsTab, systemsWidget);
		tab.add(personalTab, personalWidget);

		// add clickListener to each tabitem
		designWidget.addClickListener(this);
		developmentWidget.addClickListener(this);
		businessWidget.addClickListener(this);
		systemsWidget.addClickListener(this);
		personalWidget.addClickListener(this);

		hp.add(tab);
		hp.add(searchWidget);
		vp.add(label);
		vp.add(html);
		vp.add(hp);

		String token = TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN,
				new HashMap());
		Hyperlink back = new Hyperlink("<-- Return to Expert Center", token);
		back.setStyleName("expert_category_display_back");

		vp.add(back);

		setWidget(vp);
		label.setStyleName("expert_long_brick_display_label");
		html.setStyleName("expert_long_brick_display_html");
		tab.setStyleName("expert_long_brick_display_tab");
		vp.setStyleName("expert_long_brick_display_vp");
		hp.setStyleName("expert_long_brick_display_hp");
		setStyleName("expert_long_brick_display");
	}

	public ExpertCategoryDisplay(List expertList) {
		query = "0";
		currentExpertList = expertList;
	}

	public void onClick(Widget sender) {
		if (sender == designWidget) {
			HistoryCommander.newItem(DESIGN_TOKEN);
		} else if (sender == developmentWidget) {
			HistoryCommander.newItem(DEVELOPMENT_TOKEN);
		} else if (sender == businessWidget) {
			HistoryCommander.newItem(BUSINESS_TOKEN);
		} else if (sender == systemsWidget) {
			HistoryCommander.newItem(SYSTEMS_TOKEN);
		} else if (sender == personalWidget) {
			HistoryCommander.newItem(PERSONAL_TOKEN);
		}
	}

	/**
	 * Get the expert long brick list by category.
	 * */
	public ExpertLongBrickList getExpertLongBrickList(int category) {
		ExpertLongBrickList elbl = null;
		switch (category) {
		case DESIGN:
			elbl = designList;
			break;
		case DEVELOPMENT:
			elbl = developmentList;
			break;
		case BUSINESS:
			elbl = businessList;
			break;
		case SYSTEMS:
			elbl = systemsList;
			break;
		case PERSONAL:
			elbl = personalList;
			break;
		}
		return elbl;
	}

	public SearchWidget getSearchWidget() {
		return searchWidget;
	}

	public List getExpertList() {
		return currentExpertList;
	}

	private void setExpertList(List expertList) {
		currentExpertList = expertList;
	}

	public void setExpertList(final int category) {
		switch (category) {
		case DESIGN:
			setExpertList(designList.getExpertDTOList());
			break;
		case DEVELOPMENT:
			setExpertList(developmentList.getExpertDTOList());
			break;
		case BUSINESS:
			setExpertList(businessList.getExpertDTOList());
			break;
		case SYSTEMS:
			setExpertList(systemsList.getExpertDTOList());
			break;
		case PERSONAL:
			setExpertList(personalList.getExpertDTOList());
			break;
		}
	}

	/**
	 * Get the related query.
	 * */
	public String getQuery() {
		return query;
	}

	/**
	 * Add the page link.
	 * */
	public void setPagingLink(int category, int page) {
		HorizontalPanel linkPanel = new HorizontalPanel();
		linkPanel.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);

		// get the page number
		int pageNo = 0;
		String cat = null;
		switch (category) {
		case DESIGN:
			pageNo = designList.getPageNo();
			cat = design;
			break;
		case DEVELOPMENT:
			pageNo = developmentList.getPageNo();
			cat = development;
			break;
		case BUSINESS:
			pageNo = businessList.getPageNo();
			cat = business;
			break;
		case SYSTEMS:
			pageNo = systemsList.getPageNo();
			cat = systems;
			break;
		case PERSONAL:
			pageNo = personalList.getPageNo();
			cat = personal;
			break;
		}
		
		if (pageNo <= 1) {
			//no need to add the paging component if there are
			// zero or one pages
			return;
		}
		
		//get the proper page index
		if(page < 1 || page > pageNo) {
			page = 1;
		}
		
		//add previous link
		String previous_history_token;
		Hyperlink previous = new Hyperlink();
		previous.setText("< Previous ");
		page--;
		if(page < 1) {
			//set a style name that we can use to hide this link
			previous.addStyleName("expert_category_display_link_panel_previous_hidden");
		} else {
			previous_history_token = SearchResultsHandler.getSearchToken(query, cat, Integer.toString(page));
			previous.setTargetHistoryToken(previous_history_token);
			previous.removeStyleName("expert_category_display_link_panel_previous_hidden");
			previous.addStyleName("expert_category_display_link_panel_previous");
		}
		linkPanel.add(previous);
		linkPanel.setCellHorizontalAlignment(previous, HorizontalPanel.ALIGN_LEFT);
		page++;
		
		Label goTo = new Label("goto page: ");
		final TextBox goToTextBox = new TextBox();
		final int pgNum = pageNo;
		final String cat1 = cat;
		goToTextBox.addKeyboardListener(new KeyboardListener(){
			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				// NOOP
			}
			
			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if(keyCode ==KeyboardListener.KEY_ENTER){
					if(Integer.parseInt(goToTextBox.getText())>0 && Integer.parseInt(goToTextBox.getText())<=pgNum){
						HistoryCommander.newItem(SearchResultsHandler.getSearchToken(query,cat1,goToTextBox.getText()));
					}else if(Integer.parseInt(goToTextBox.getText())<=0){
						HistoryCommander.newItem(SearchResultsHandler.getSearchToken(query,cat1,String.valueOf(1)));
					}else if(Integer.parseInt(goToTextBox.getText())>pgNum){
						HistoryCommander.newItem(SearchResultsHandler.getSearchToken(query,cat1,String.valueOf(pgNum)));
					}
				}
			}
			
			public void onKeyUp(Widget sender,char keyCode, int modifieirs){
				//NOOP
			}
		});
		Label total = new Label(" of "+pgNum);
		HorizontalPanel goToPanel = new HorizontalPanel();
		goToPanel.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
		goToPanel.add(goTo);
		goToPanel.add(goToTextBox);
		goToPanel.add(total);
		goToPanel.addStyleName("expert_category_display_link_panel_goto");
		linkPanel.add(goToPanel);
		linkPanel.setCellHorizontalAlignment(goToPanel, HorizontalPanel.ALIGN_CENTER);
		
		//add the next link
		String next_history_token;
		Hyperlink next = new Hyperlink();
		next.setText(" Next >");
		page++;
		if(page > pageNo) {
			//set a style name that we can use to hide this link
			next.addStyleName("expert_category_display_link_panel_next_hidden");
		} else {
			next_history_token = SearchResultsHandler.getSearchToken(query, cat, Integer.toString(page));
			next.setTargetHistoryToken(next_history_token);
			next.removeStyleName("expert_category_display_link_panel_next_hidden");
			next.addStyleName("expert_category_display_link_panel_next");
		}
		linkPanel.add(next);
		linkPanel.setCellHorizontalAlignment(next, HorizontalPanel.ALIGN_RIGHT);
		
		
		
		page--;

		switch (category) {
		case DESIGN:
			designLink.setWidget(linkPanel);
			break;
		case DEVELOPMENT:
			developmentLink.setWidget(linkPanel);
			break;
		case BUSINESS:
			businessLink.setWidget(linkPanel);
			break;
		case SYSTEMS:
			systemsLink.setWidget(linkPanel);
			break;
		case PERSONAL:
			personalLink.setWidget(linkPanel);
			break;
		}

		// hl.addStyleName("expert_category_display_hl");
		linkPanel.addStyleName("expert_category_display_link_panel");
	}
}
