/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client.bluegray.ui;

import java.util.Map;

import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.SubCommandToken;
import com.helpguest.marketplace.client.ui.BodyPanel;

/**
 * @author mabrams
 *
 */
public class AboutPanel extends SimplePanel implements SubCommandToken {
	
	protected final static String TOKEN = "tab_about";
	public static final String CONTENT_KEY = "content";
	final BodyPanel body = new BodyPanel();
	private CommandToken parentCommand;
	
	final ContentEntry basics = new ContentEntry("basics.html");
	final ContentEntry history = new ContentEntry("historyHelpGuest.html");
	final ContentEntry expert = new ContentEntry("technology.html");
	final ContentEntry about = new ContentEntry("team.html");

	
	/**
	 * Body content
	 * */
	final VerticalPanel aboutpanel = new VerticalPanel ();
	
	public AboutPanel(final CommandToken parentCommand) {
		this();
		this.parentCommand = parentCommand;
	}
	
	public AboutPanel() {
		super();
		AboutMenu menu = new AboutMenu(body);
		body.setWidget(basics);
		aboutpanel.add(menu);
		aboutpanel.add(body);
		
		basics.setStyleName("about_panel_content");
		history.setStyleName("about_panel_content");
		expert.setStyleName("about_panel_content");
		about.setStyleName("about_panel_content");
		
		setWidget(aboutpanel);
		setStyleName("about_panel");
	}
	
	/**
	 * Set the panel to display a specific content.
	 * */
	public void setContent(int index) {
		switch(index) {
		case AboutMenu.BASICS_INDEX:
			body.setWidget(basics);
			break;
		case AboutMenu.HISTORY_INDEX:
			body.setWidget(history);
			break;
		case AboutMenu.TECHNOLOGY_INDEX:
			body.setWidget(expert);
			break;
		case AboutMenu.TEAM_INDEX:
			body.setWidget(about);
			break;
		}
	}

	public String getBaseToken() {
		return TOKEN;
	}

	public void onEntry(Map<String, String> kvMap) {
		//read parameters
		String content = (String)kvMap.get(CONTENT_KEY);
		if (content == null) {
			//set to default content
			setContent(AboutMenu.BASICS_INDEX);
		} else {
			if(content.equals(AboutMenu.BASICS)) {
				setContent(AboutMenu.BASICS_INDEX);
			} else if(content.equals(AboutMenu.HISTORY)) {
				setContent(AboutMenu.HISTORY_INDEX);
			} else if(content.equals(AboutMenu.TECHNOLOGY)) {
				setContent(AboutMenu.TECHNOLOGY_INDEX);
			} else if(content.equals(AboutMenu.TEAM)) {
				setContent(AboutMenu.TEAM_INDEX);
			}
		}
	}

	public void onExit() {
		// TODO Auto-generated method stub
		
	}

	public void setParent(CommandToken parentCommand) {
		this.parentCommand = parentCommand;
	}
}
