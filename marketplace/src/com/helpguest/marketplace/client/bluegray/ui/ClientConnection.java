package com.helpguest.marketplace.client.bluegray.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.ui.VisualCuesImageBundle;

public class ClientConnection extends SimplePanel {
	final ExpertServiceAsync expertService = (ExpertServiceAsync) GWT.create(ExpertService.class);
	private VisualCuesImageBundle visualCue = (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);
	private VerticalPanel base = new VerticalPanel();
	private HorizontalPanel upper = new HorizontalPanel();
	private VerticalPanel lower = new VerticalPanel();
	private HorizontalPanel lower_top = new HorizontalPanel();
	private HorizontalPanel lower_bottom = new HorizontalPanel();
	private VerticalPanel lower_bottom_left = new VerticalPanel();
	private VerticalPanel lower_bottom_right = new VerticalPanel();
	
	public ClientConnection(ExpertDTO expertDTO) {
		upper.add(visualCue.ticket().createImage());
		upper.add(new Label("The quick ticket allows you to quickly establish a direct connection with one of the experts."));
		
		lower_top.add(new Label("Connecting to xxx"));
		ClientConnectionInstruction cci1 = new ClientConnectionInstruction("1.",
				"Click HERE or Quick Ticket Connect button on the right to start the HelpGuest application.", 
				"(If encounters error then please upgrade to latest version of java or contact us for help)");
		
		ClientConnectionInstruction cci2 = new ClientConnectionInstruction("2.",
				"After you are connected with the expert, click the \"see\" or \"do\" button on the HelpGuest application to allow expert to work on your problem. You can also chat directly with the expert with the application", 
				null);
		
		ClientConnectionInstruction cci3 = new ClientConnectionInstruction("3.",
				"Please answer the brief survey questions at the end of your session so that we can improve.", 
				null);
		
		lower_bottom_left.add(cci1);
		lower_bottom_left.add(cci2);
		lower_bottom_left.add(cci3);
		
		VerticalPanel lower_bottom_right_content = new VerticalPanel();
		Image image = new Image(expertDTO.getAvatarURL());
		
		HorizontalPanel online = new HorizontalPanel();
		Label statusLabel = new Label();
		if(expertDTO.isLive()){
			statusLabel.setText("ONLINE");
			online.add(visualCue.connection().createImage());
			online.add(statusLabel);
        } else {
        	statusLabel.setText("OFFLINE");
    		online.add(visualCue.offline().createImage());
    		online.add(statusLabel);
        }
		lower_bottom_right_content.add(image);
		lower_bottom_right_content.add(online);
		
		lower_bottom_right.add(lower_bottom_right_content);
		
		lower_bottom.add(lower_bottom_left);
		lower_bottom.add(lower_bottom_right);
		
		lower.add(lower_top);
		lower.add(lower_bottom);
		
		base.add(upper);
		base.add(lower);
		
		setWidget(base);
	}
}
