package com.helpguest.marketplace.client.bluegray.ui;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.SourcesTabEvents;
import com.google.gwt.user.client.ui.TabListener;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.bakery.CookieManager;
import com.helpguest.marketplace.client.history.DefaultToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.history.ProxySubCommandToken;
import com.helpguest.marketplace.client.history.SubCommandToken;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.observer.TabClickListener;
import com.helpguest.marketplace.client.observer.TabToken;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.widgets.TabFocusPanel;

public class PublicTabPanel extends SimplePanel implements AuthenticationListener, DefaultToken, TabListener, TabClickListener {
	private static PublicTabPanel tabbedContentPanel;
	private static Authenticator authenticator;
	private static String TOKEN = "public_tabs";
	
	private static final String ABOUT_TOKEN = AboutPanel.TOKEN;
	private static final String FIND_EXPERT_TOKEN = FindExpert.TOKEN;
	private static final String BECOME_EXPERT_TOKEN = BecomeAnExpert.TOKEN;
	private static final String SECURITY_TOKEN = "tab_security";
	private static final String FAQ_TOKEN = "tab_faq";
	public static final String DEMO_TOKEN = "tab_demo";
	
	private final Label aboutLabel = new Label("ABOUT");
	private final Label findAnExpertLabel = new Label("FIND AN EXPERT");
	private final Label becomeAnExpertLabel = new Label("BECOME AN EXPERT");
	private final Label securityLabel = new Label("SECURITY");
	private final Label faqLabel = new Label("FAQ");
	private final Label demoLabel = new Label("DEMO");
	
	
	private TabPanel generalTabbedContent = new TabPanel();
	private FindExpert findExpert = FindExpert.getInstance();
	private ProxySubCommandToken findExpertProxy = new ProxySubCommandToken();
	private BecomeAnExpert becomeAnExpert;
	private AboutPanel aboutPanel;
	private ContentEntry securityContent = new ContentEntry("security.html");
	private ContentEntry faqContent = new ContentEntry("faq.html");
			
	/**
	 * Define the default values for tab handling and entry routine.
	 */
	private static final String DEFAULT_PAGE_TOKEN = FIND_EXPERT_TOKEN;
	private static int DEFAULT_TAB_INDEX;
	
	private Map indexCommandMap = new HashMap();
	private Map subTokenIndexMap = new HashMap();
	
	private int currentTab = DEFAULT_TAB_INDEX;
	private int previousTab = DEFAULT_TAB_INDEX;
	
	private PublicTabPanel(){
		becomeAnExpert = new BecomeAnExpert(this);
		aboutPanel = new AboutPanel(this);
		securityContent.setStyleName("public_tab_panel_content_entry");
		securityContent.getContent().setStyleName("public_tab_panel_html_content");
		faqContent.setStyleName("public_tab_panel_content_entry");
		faqContent.getContent().setStyleName("public_tab_panel_html_content");
		
		init(); 
		setStyleName("tabbed_content_panel");
	}
	
	private void init() {
	    initTabs();
		generalTabbedContent.addTabListener(this);
		generalTabbedContent.selectTab(DEFAULT_TAB_INDEX);
		this.setWidget(generalTabbedContent);
	}
	
	private void initTabs() {
		//initialize generalTabbedContent 
		assignTab(aboutPanel, aboutLabel, ABOUT_TOKEN);
		findExpertProxy.setWidget(findExpert);
		DEFAULT_TAB_INDEX = assignTab(findExpertProxy, findAnExpertLabel, FIND_EXPERT_TOKEN);
		assignTab(becomeAnExpert, becomeAnExpertLabel, BECOME_EXPERT_TOKEN);
		assignTab(securityContent, securityLabel, SECURITY_TOKEN);
		assignTab(faqContent, faqLabel, FAQ_TOKEN);
		assignTab(new DemoPanel(), demoLabel, DEMO_TOKEN);
        
		generalTabbedContent.setStyleName("tabbed_content_panel-tabbed_content");
		generalTabbedContent.getTabBar().setStylePrimaryName("tabbed_content_panel-tabbed_content-tabbed_bar");
		//Set the width to 0 and let it be managed by the tabs and CSS
		generalTabbedContent.getTabBar().setWidth("0px");
				
		//Set stylename for each label
		aboutLabel.setStyleName("tabbed_content_panel_label_about");
		findAnExpertLabel.setStyleName("tabbed_content_panel_label_find_an_expert");
		becomeAnExpertLabel.setStyleName("tabbed_content_panel_label_become_an_expert");
		securityLabel.setStyleName("tabbed_content_panel_label_security");
		faqLabel.setStyleName("tabbed_content_panel_label_faq");
		demoLabel.setStyleName("tabbed_content_panel_label_demo");
	}
	
	
	/**
	 * Get the current instance.
	 * */
	public static PublicTabPanel getInstance() {
		if (tabbedContentPanel == null) {
			tabbedContentPanel = new PublicTabPanel();
		}
		return tabbedContentPanel;
	}
	
	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#getTabPanel()
	 */
	public TabPanel getTabPanel(){
		return generalTabbedContent;
	}
	
	/**
	 *
	 * @param command
	 * @param tabIndex
	 * @return return the tabIndex to which this command was set
	 */
	private int assignTab(final Widget widget, final Label tabLabel, final String tabToken) {
		//Create a focus panel to handle click events
		TabFocusPanel tcfp = new TabFocusPanel(tabLabel, tabToken);
		tcfp.addTabClickListener(this);
		//add the widget and the focus panel to the tab panel
		generalTabbedContent.add(widget, tcfp);

		//Populate mappings used to manage commands, and contents
		int widgetPlacmentIndex = generalTabbedContent.getWidgetCount() - 1;
		DebugPanel.setText("PublicTabPanel", "Added widget (" + tabLabel.getText() + ") at " + widgetPlacmentIndex);
		if (widget instanceof SubCommandToken) {
			setTabSelection((SubCommandToken)widget, widgetPlacmentIndex);
		} else {
			setTabSelection(tabToken, widgetPlacmentIndex);
		}
		
		//Return the index this widget was assigned in the tab panel
		return widgetPlacmentIndex;
	}
	

	private void setTabSelection(final SubCommandToken command, final int tabIndex) {	
		//associate an token with a command via a common index
		subTokenIndexMap.put(command.getBaseToken(), String.valueOf(tabIndex));
		indexCommandMap.put(String.valueOf(tabIndex), command);
		HistoryCommander.getInstance().register(command);
		setDefaultTabSelectionHistoryValue(command.getBaseToken());
	}
	
	/**
	 * Set the history token for a tab with the corresponding command.
	 * */
	private void setTabSelection(final String tabToken, final int tabIndex) {
		subTokenIndexMap.put(tabToken, String.valueOf(tabIndex));
		setDefaultTabSelectionHistoryValue(tabToken);
	}
	
	private void setDefaultTabSelectionHistoryValue(final String tabToken) {
		CookieManager.bake(
				tabToken, HistoryCommander.buildToken(TOKEN, TabbedContentPanel.getTabKey(), tabToken), false);		
	}

	public static String getHistoryCommandForTab(final String tabTokenValue, final String key, final String value) {
		Map kvMap = new HashMap();
		kvMap.put(key, value);
		return getHistoryCommandForTab(tabTokenValue, kvMap);
	}

	/**
	 * This is the same as calling {@link #getHistoryCommandForTab(String, Map, boolean)}
	 * @param tabTokenValue is the tab token value.
	 * @param kvMap is a map of the parameters to pass into the history command token.
	 * @return
	 */
	public static String getHistoryCommandForTab(final String tabTokenValue, final Map kvMap) {
		return getHistoryCommandForTab(tabTokenValue, kvMap, false);
	}
	
	public static String getHistoryCommandForTab(final String tabTokenValue, final Map kvMap, final boolean isExternal) {
		kvMap.put(TabbedContentPanel.TAB_KEY, tabTokenValue);
		if (isExternal) {
			return HistoryCommander.buildExternalToken(TOKEN, kvMap);
		} else {
			return HistoryCommander.buildToken(TOKEN, kvMap);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#getBaseToken()
	 */
	public String getBaseToken() {
		return TOKEN;
	}

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#onEntry(java.util.Map)
	 */
	public void onEntry(Map<String, String> kvMap) {
		DebugPanel.setText("PublicTabPanel", "entering onEntry for command " + TOKEN);
		if (kvMap != null) {
			DebugPanel.setText("PublicTabPanel", "The map was not null");
			String tab = (String)kvMap.get(TabbedContentPanel.getTabKey());
			if (tab != null) {
				DebugPanel.setText("PublicTabPanel", "The tab was defined: " + tab);
				int tabIndex;
				try {
					tabIndex = Integer.parseInt((String)subTokenIndexMap.get(tab));
				} catch (Exception ex) {
					DebugPanel.setText("PublicTabPanel", "Unable to parse int value for key: " + tab);
					tabIndex = DEFAULT_TAB_INDEX;
				}
				
				//Only select the tab if it's not already selected.
				if (currentTab != tabIndex) {
					generalTabbedContent.selectTab(tabIndex);
				}
				
				//Now lets execute any entry methods necessary.
				String subCommandIndex = String.valueOf(tabIndex);
				if (kvMap != null && indexCommandMap.containsKey(subCommandIndex)) {
					DebugPanel.setText("PublicTabPanel", "Calling sub comand entry map");
					SubCommandToken subCommand = ((SubCommandToken) indexCommandMap.get(subCommandIndex));
					if (subCommand != null) {
						DebugPanel.setText("PublicTabPanel", "Sub command is not null");
						//DebugPanel.setText("PublicTabPanel", "kvMap is " + (String)kvMap.get("content"));
						subCommand.onEntry(kvMap);
					}
					
					//Now we are going to update the click history cookie for this tab with the current kvMap entries
					//Window.alert("bake: " + HistoryCommander.buildToken(TOKEN, kvMap));
					CookieManager.bake(tab, HistoryCommander.buildToken(TOKEN, kvMap), false);
					
					DebugPanel.setText("PublicTabPanel", "completed handling sub command in onEntry");
				} else {
					DebugPanel.setText("PublicTabPanel", "The kvMap is not null but there was no sub command to execute?");
				}
			} else {
				DebugPanel.setText("PublicTabPanel", "There was no tab value indicated, so using default tab");
				//we didnt get an index value.  use the default
				generalTabbedContent.selectTab(DEFAULT_TAB_INDEX);
			}
		} else {
			DebugPanel.setText("PublicTabPanel", "The map is null, so using default tab");
			//if no tab has been identified, then this is the default
			generalTabbedContent.selectTab(DEFAULT_TAB_INDEX);
		}
	}
	
	private void memorizeTabSelection(final int tabIndex) {
		this.previousTab = currentTab;
		this.currentTab = tabIndex;
	}

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#onExit()
	 */
	public void onExit() {
		// TODO Auto-generated method stub
		
	}
	
	public static String getDefaultHistoryCommand() {
		return HistoryCommander.buildToken(TOKEN, TabbedContentPanel.getTabKey(), String.valueOf(DEFAULT_PAGE_TOKEN));
	}
	
	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#getDefaultToken()
	 */
	public String getDefaultToken() {
		return getDefaultHistoryCommand();
	}

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#onBeforeTabSelected(com.google.gwt.user.client.ui.SourcesTabEvents, int)
	 */
	public boolean onBeforeTabSelected(SourcesTabEvents source, int tabIndex) {
		DebugPanel.setText("PublicTabPanel", "moving on to tab: " + tabIndex);
		//Return false to disable a tab
		return true;
	}

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#onTabSelected(com.google.gwt.user.client.ui.SourcesTabEvents, int)
	 */
	public void onTabSelected(SourcesTabEvents source, int tabIndex) {
		previousTab = currentTab;
		currentTab = tabIndex;
	}

	/* (non-Javadoc)
	 * @see com.helpguest.marketplace.client.bluegray.ui.CommandTokenTabPanel#onClick(com.helpguest.marketplace.client.observer.TabToken)
	 */
	public void onClick(TabToken tabCommand) {
		History.newItem(CookieManager.ponder(tabCommand.getTabToken()));
	}
	
	/**
	 * Call this method before making this object visible so it has an opportunity to reset itself.
	 * @return this instance of the ValidatedTabbedContentPanel
	 */
	public void onChange(final Authenticator authenticator) {
		if (!authenticator.isLoggedIn()) {
			findExpert = FindExpert.getInstance();
		} else {
			findExpert = FindExpert.getEmptyInstance();
		}
		findExpertProxy.setWidget(findExpert);
	}
	
}
