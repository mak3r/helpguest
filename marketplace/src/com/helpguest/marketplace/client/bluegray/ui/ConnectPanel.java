package com.helpguest.marketplace.client.bluegray.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FormHandler;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormSubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormSubmitEvent;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.VisualCuesImageBundle;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;

public class ConnectPanel extends SimplePanel {
	
	//TODO: this should probably be a ContentEntry instead of a String
	private static final String agreeStatement = "<font style=\"font-size: smaller;\">I have read and agree with the&nbsp;"
			+ "<a href=\"ExpertEULA.html\" target=\"_blank\">"
			+ "terms of use"
			+ "</a>"
			+ "&nbsp;and the&nbsp;"
			+ "<a href=\"RateCalculation.html\" target=\"_blank\">"
			+ "payment plan"
			+ "</a>"
			+ "&nbsp;and I understand the&nbsp;"
			+ "<a href=\"PrivacyPolicy.html\" target=\"_blank\">"
			+ "privacy policy" + "</a>." + "</font>";
	
	private static MarketPlaceConstants marketPlaceConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String FULLY_QUALIFIED_DOMAIN_NAME = marketPlaceConstants.fullyQualifiedDomainName();
    private static String WEB_URI_PREFIX = marketPlaceConstants.webUriPrefix();
    private static String GWT_MODULE = marketPlaceConstants.gwtModule();
	private final static String ACTION = marketPlaceConstants.payPalAction();
	private final static String BUSINESS = marketPlaceConstants.payPalBusiness();
	private String formAction = ACTION;
	private String formHiddenBusiness = BUSINESS;
	
    
	public ConnectPanel(final ExpertDTO expert, final SessionDTO session) {
		//initial the form which will be submitted
		Hidden cmd = new Hidden("cmd", "_xclick");
		Hidden business = new Hidden("business", formHiddenBusiness);
        Hidden custom = new Hidden("custom", expert.getExpertUid());
        Hidden invoice = new Hidden("invoice", session.getSessionId());
        Hidden item_name = new Hidden("item_name", expert.getHandle());
        Hidden item_number = new Hidden("item_number", session.getItemNumber());
        Hidden currency_code = new Hidden("currency_code", "USD");
        Hidden amount = new Hidden("amount", expert.getPaymentPlan().getDepositAmount());
        Hidden image_url = new Hidden("image_url", WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/" + GWT_MODULE + "/images/brand.png");
        Hidden no_shipping = new Hidden("no_shipping", "1");
        Hidden no_note = new Hidden("no_note", "1");
        //TODO: oops this has to be parameterized phaseIII/pageBanner.html
        //FIXME: we need a buildSafariToken to send the encoded url to paypal as the return to
        Hidden returnTo = new Hidden("return", WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/" + 
        		"phaseIII/pageBanner.html#" + 
        		QuickTicket.getPaidTicketToken(session.getSessionId()));
        Hidden cancel_return = new Hidden("cancel_return", 
        		WEB_URI_PREFIX + "://" + FULLY_QUALIFIED_DOMAIN_NAME + "/" + 
        		"phaseIII/pageBanner.html#" + History.getToken());

        
        //TODO the action will be different for a 'canDemo' expert
        final FormPanel helpNowForm = new FormPanel("_parent");
        helpNowForm.setAction(formAction);
        helpNowForm.setMethod(FormPanel.METHOD_POST);
        helpNowForm.setEncoding(FormPanel.ENCODING_URLENCODED);
        //handler interface form submit events
        helpNowForm.addFormHandler(new FormHandler() {

			public void onSubmit(FormSubmitEvent event) {
				
			}

			public void onSubmitComplete(FormSubmitCompleteEvent event) {
				//Window.alert("HelpNowPanel onSubmitComplete(): " + event.getResults());				
			}
        	
        });
		//connect panel which consists of check box and button.
		VerticalPanel connectWidget = new VerticalPanel();
		HorizontalPanel connect = new HorizontalPanel();		
		Label labelupper = new Label();

		final PushButton button = new PushButton();
		button.setEnabled(false);
		ClickListener submitListener = new ClickListener() {
			public void onClick(Widget sender) {
				helpNowForm.submit();
			}
		};
		button.addClickListener(submitListener);
		
		final CheckBox terms = new CheckBox(agreeStatement, true);
		ClickListener enableButtonListener = new ClickListener() {
			public void onClick(Widget sender) {
				// TODO Auto-generated method stub
				if (terms.isChecked()&&expert.isLive()) {
					button.setEnabled(true);
					button.setText("CONNECT");
				} else {
					button.setEnabled(false);
				}
			}
        };
        terms.addClickListener(enableButtonListener);

		VisualCuesImageBundle visualCue = (VisualCuesImageBundle) GWT
				.create(VisualCuesImageBundle.class);
		HorizontalPanel labeledVisualCue = new HorizontalPanel();
		if (expert.isLive()) {
			labelupper.setText("ONLINE");
			button.setText("CONNECT");
			labeledVisualCue.add(visualCue.connection().createImage());
			labeledVisualCue.add(labelupper);
			connectWidget.add(labeledVisualCue);
			connectWidget.add(button);

		} else {
			labelupper.setText("OFFLINE");
			button.setText("LEAVE MESSAGE");
			labeledVisualCue.add(visualCue.offline().createImage());
			labeledVisualCue.add(labelupper);
			connectWidget.add(labeledVisualCue);
			connectWidget.add(button);
		}
		connect.add(connectWidget);
		connect.add(terms);
		terms.setChecked(false);
		connectWidget.setStyleName("client_view_of_expert_connect_widget");
		labeledVisualCue.setStyleName("connect_panel_labeled_visual_cue");
		connect.setStyleName("client_view_of_expert_connect");
		labelupper.setStyleName("client_view_of_expert_labelupper");
		button.setStyleName("client_view_of_expert_labelbuttom");
		terms.setStyleName("client_view_of_expert_terms");
		
		 VerticalPanel formContents = new VerticalPanel();
	        formContents.setStyleName("connect_panel_form_contents");
	        formContents.add(cmd);
			formContents.add(business);
	        formContents.add(custom);
	        formContents.add(invoice);
	        formContents.add(item_name);
	        formContents.add(item_number);
	        formContents.add(currency_code);
	        formContents.add(amount);
	        formContents.add(image_url);
	        formContents.add(no_shipping);
	        formContents.add(no_note);
	        formContents.add(returnTo);
	        formContents.add(cancel_return);
	        formContents.add(connect);
	        
	      //Add the form content
	        helpNowForm.setWidget(formContents);
	        helpNowForm.setStyleName("connect_panel_help_now_form");
	        
	        VerticalPanel vp = new VerticalPanel();
	        vp.add(helpNowForm);
	        vp.add(new ContentEntry("checkoutDetail.div"));
			vp.setStyleName("connect_panel_vp");

			setWidget(vp);
			setStyleName("connect_panel");
	}
	
	
}
