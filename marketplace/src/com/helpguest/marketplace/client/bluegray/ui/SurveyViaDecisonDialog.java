/**
 * HelpGuest Technologies, Inc.
 * 
 */
package com.helpguest.marketplace.client.bluegray.ui;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.IPNService;
import com.helpguest.marketplace.client.IPNServiceAsync;
import com.helpguest.marketplace.client.brick.ContentBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.IPNDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.GuestConnectInstructions;
import com.helpguest.marketplace.client.ui.HorizontalSpacer;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.ui.VerticalSpacer;
import com.helpguest.marketplace.client.widgets.PaddedPanel;
import com.helpguest.marketplace.client.util.DebugPanel;
/**
 * @author mabrams
 * @ticket# CX11CE297170B
 */
public class SurveyViaDecisonDialog extends SimplePanel implements CommandToken {
	public static CommandToken currentCommandToken;
	
	public final static String QUICK_TICKET_ID = "quickTicketId";
	public final static String PAYPAL_ID = "ticketId";
	public final static String HISTORY_PREFIX = "SurveyViaDecisonDialog";
	private boolean usePaidTicket = false;
	private String ticketNumber;
	private static IPNDTO ipnDTO;
	private ExpertDTO expertDTO;
	private ArrayList survey;
    
	//Create the ipn service proxy
	final IPNServiceAsync ipnService =
		(IPNServiceAsync) GWT.create(IPNService.class);
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	private ResultContainer resultContainer;
	private Button closeButton = new Button("Close ticket");
	private RadioButton rbSatsfctnXtremely;
	private RadioButton rbSatsfctnVery;
	private RadioButton rbSatsfctnYeahok;
	private RadioButton rbSatsfctnNotsogreat;
	private RadioButton rbSatsfctnThiswasawful;
	private RadioButton rbProblemSolvedYes;
	private RadioButton rbProblemSolvedNo;
	private RadioButton rbProblemSolvedDontknow;
	private RadioButton rbRecommendYes;
	private RadioButton rbRecommendNo;
	private RadioButton rbRepeatCustomerYes;
	private RadioButton rbRepeatCustomerNo;
	private RadioButton rbProfessionalMannerYes;
	private RadioButton rbProfessionalMannerNo;
	private Button submitSurveyButton = new Button("Submit Survey");
	private String strSatisfaction;
	private String strProblemSolved;
	private String strRecommend;
	private String strRepeatCustomer;
	private String strProfessionalManner;
	
	
	private SimplePanel ticketContainer = new SimplePanel();
	
	private static Label paypalRequired = new Label(
		"Thanks for using HelpGuest with PayPal.  " +
		"Your payment completed successfully.  " +
		"A receipt for your purchase has been emailed to you.  " +
		"Visit http://www.paypal.com to review your account.");
	
	private static HTML note = new HTML(
			"<sup>&dagger;</sup>" +
			"If your Expert has gone offline while " +
			"you went through the payment process, " +
			"please <a href=\"mailto:listen@helpguest.com\">" +
			"let us know</a>.  " +
			"You are entitled to a full refund or " +
			"you can re-use your TICKET# " +
			"when the expert comes back online.");
	static {
		paypalRequired.setStyleName("survey_via_decision_dialogPaypalRequired");
		note.setStyleName("survey_via_decision_dialogNote");
	}
	
	
	public SurveyViaDecisonDialog(final ResultContainer resultContainer) {
		this.resultContainer = resultContainer;
		setStyleName("survey_via_decision_dialog");
		//Specify the url where the ipn service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.ipnService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "IPNService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);

		//Specify the url where the expert service is running
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String moduleRelativeURLExpertService = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(moduleRelativeURLExpertService);
		
		
	}
	
	private void init() {
		
		String handle = ipnDTO.getSession().getExpertAccount().getHandle();
		VerticalPanel vp = new VerticalPanel();
		vp.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);

		if (usePaidTicket) {
			vp.add(paypalRequired);
		}
		
		Label ticket = new Label(ipnDTO.getSession().getSessionId()); 
		ticket.setStyleName("survey_via_decision_dialogTicket");
		Label ticketLabel = new Label("TICKET#:  ");
		ContentBrick ticketNumber = new ContentBrick(
				ticketLabel, ticket);				
		
		String onOff = (expertDTO.isLive()?
				"<font class=\"hg-online\">ONLINE</font>":
				"offline");
		HTML onOffLine = new HTML(
				handle + " is currently " + onOff + 
				"<sup>&dagger;</sup>" );
		
		Label stepsLabel = new Label(
				"The final steps to getting assistance from " + 
				handle + " are simple.");
		
		SimplePanel instructions =
			new GuestConnectInstructions(ipnDTO.getSession());

		//Add the receipt details and instructions to a distinct panel
		VerticalPanel receiptInfo = new VerticalPanel();
		receiptInfo.setSpacing(20);
		receiptInfo.add(onOffLine);
		receiptInfo.add(stepsLabel);

		//Add the ticket number, receipt details to the receipt
		VerticalPanel receiptPanel = new VerticalPanel();
		receiptPanel.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		receiptPanel.setStyleName("survey_via_decision_dialogReceiptPanel");
		receiptPanel.setBorderWidth(1);
		receiptPanel.add(new PaddedPanel(ticketNumber, 10));
		receiptPanel.add(receiptInfo);
		receiptPanel.add(new PaddedPanel(instructions, 10));
		
		
		PaddedPanel receiptDock = new PaddedPanel(1, 20);
		receiptDock.setStyleName("survey_via_decision_dialogReceiptDock");
		receiptDock.add(receiptPanel);
		
		RoundedPanel receipt = new RoundedPanel(receiptDock, RoundedPanel.ALL, 4);
		receipt.setStyleName("survey_via_decision_dialogReceipt");
		receipt.setCornerStyleName("survey_via_decision_dialogReceipt-corners");
		
		HorizontalPanel outerPadding = new HorizontalPanel();
		outerPadding.add(new VerticalSpacer(75));
		outerPadding.add(receipt);
		outerPadding.add(new VerticalSpacer(75));
		
		vp.add(outerPadding);
		vp.add(new HorizontalSpacer(20));
		if (usePaidTicket) {
			vp.add(note);
		}
		vp.setSpacing(20);
		
		VerticalPanel content = new VerticalPanel();
		content.add(vp);
		content.add(closeButton);
		
		ticketContainer.setWidget(content);

		closeButton.addClickListener(new ClickListener() {

			public void onClick(Widget arg0) {
				String historyBack = TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, new HashMap());
				HistoryCommander.newItem(historyBack);
			}
			
		});
		
		setWidget(ticketContainer);
	}
	
	private void showSurvey(){
		Label lblTitle = new Label("Guest Feedback");
		Label lblSurvey = new Label("Please evaluate your HelpGuest session.");
		Label lblSatisfaction = new Label("How satisfied were you with the overall HelpGuest experience?");
		Label lblProblemSolved = new Label("Did this Expert solve your problem?");
		Label lblRecommend = new Label("Would you recommend this expert to others with a similar problem?");
		Label lblRepeatCustomer = new Label("Have you worked with this Expert before (either with or without HelpGuest)?");
		Label lblProfessionalManner = new Label("Was the service provided in a professional manner?");
		Label lblSurveyComplete = new Label("The survey for this ticket number is complete.");
		DebugPanel.setText("SurveyViaDecisonDialog  ", "In start of showSurvey()");
		//satisfactionPanel
		rbSatsfctnXtremely = new RadioButton("satisfactionRadioGroup", "Extremely");
	    rbSatsfctnVery = new RadioButton("satisfactionRadioGroup", "Very");
	    rbSatsfctnYeahok = new RadioButton("satisfactionRadioGroup", "Yeah, O.K.");
	    rbSatsfctnNotsogreat = new RadioButton("satisfactionRadioGroup", "Not So Great");
	    rbSatsfctnThiswasawful = new RadioButton("satisfactionRadioGroup", "This Was Awful");

	    rbSatsfctnXtremely.setEnabled(true);
		rbSatsfctnVery.setEnabled(true);
		rbSatsfctnYeahok.setEnabled(true);
		rbSatsfctnNotsogreat.setEnabled(true);
		rbSatsfctnThiswasawful.setEnabled(true);
	    
		DebugPanel.setText("SurveyViaDecisonDialog  ", "satisfactionRadioGroup enabled");
		DebugPanel.setText("Ins show survey overall_satisfaction = ", (String) survey.get(0));
	    DebugPanel.setText("In show survey problem_solved = ", (String) survey.get(1));
	    DebugPanel.setText("In show survey recommend = ", (String) survey.get(2));
	    DebugPanel.setText("In show survey repeat_customer = ", (String) survey.get(3));
	    DebugPanel.setText("In show survey professional_service = ", (String) survey.get(4));
		
	    if((String) survey.get(0) != null){
		    	if (survey.get(0).equals("4")){
		    		rbSatsfctnXtremely.setChecked(true);
		    	}else if (survey.get(0).equals("3")){
		    		rbSatsfctnVery.setChecked(true);
		    	}else if (survey.get(0).equals("2")){
		    		rbSatsfctnYeahok.setChecked(true);
		    	}else if (survey.get(0).equals("1")){
		    		rbSatsfctnNotsogreat.setChecked(true);
		    	}else if (survey.get(0).equals("0")){
		    		rbSatsfctnThiswasawful.setChecked(true);
		    	}
		    }
		    	
		if (rbSatsfctnXtremely.isChecked() || rbSatsfctnVery.isChecked() || rbSatsfctnYeahok.isChecked()
	    			|| rbSatsfctnNotsogreat.isChecked() || rbSatsfctnThiswasawful.isChecked()){
	    		rbSatsfctnXtremely.setEnabled(false);
	    		rbSatsfctnVery.setEnabled(false);
	    		rbSatsfctnYeahok.setEnabled(false);
	    		rbSatsfctnNotsogreat.setEnabled(false);
	    		rbSatsfctnThiswasawful.setEnabled(false);
	    	}
	    

	    // Add them to the root panel.
	    HorizontalPanel satisfactionPanel = new HorizontalPanel();
	    satisfactionPanel.add(rbSatsfctnXtremely);
	    satisfactionPanel.add(rbSatsfctnVery);
	    satisfactionPanel.add(rbSatsfctnYeahok);
	    satisfactionPanel.add(rbSatsfctnNotsogreat);
	    satisfactionPanel.add(rbSatsfctnThiswasawful);
	    
	    DebugPanel.setText("SurveyViaDecisonDialog  ", "satisfactionPanel inited");
	    
	    //problemSolvedPanel
	    rbProblemSolvedYes = new RadioButton("problemSolvedRadioGroup", "Yes");
	    rbProblemSolvedNo = new RadioButton("problemSolvedRadioGroup", "No");
	    rbProblemSolvedDontknow = new RadioButton("problemSolvedRadioGroup", "I don't know");
	    
	    rbProblemSolvedYes.setEnabled(true);
		rbProblemSolvedNo.setEnabled(true);
		rbProblemSolvedDontknow.setEnabled(true);
	   
		if((String) survey.get(1) != null){
				if (survey.get(1).equals("1")){
		    		rbProblemSolvedYes.setChecked(true);
		    	}else if (survey.get(1).equals("0")){
		    		rbProblemSolvedNo.setChecked(true);
		    	}else if (survey.get(1).equals("2")){
		    		rbProblemSolvedDontknow.setChecked(true);
		    	}
			}	
	    	
		if (rbProblemSolvedYes.isChecked() || rbProblemSolvedNo.isChecked() || rbProblemSolvedDontknow.isChecked()
	    			){
	    		rbProblemSolvedYes.setEnabled(false);
	    		rbProblemSolvedNo.setEnabled(false);
	    		rbProblemSolvedDontknow.setEnabled(false);
	    		
	    	}

	    // Add them to the root panel.
	    HorizontalPanel problemSolvedPanel = new HorizontalPanel();
	    problemSolvedPanel.add(rbProblemSolvedYes);
	    problemSolvedPanel.add(rbProblemSolvedNo);
	    problemSolvedPanel.add(rbProblemSolvedDontknow);
	    
	    DebugPanel.setText("SurveyViaDecisonDialog  ", "problemSolvedPanel inited");
	    
	    //recommendPanel
	    rbRecommendYes = new RadioButton("recommendRadioGroup", "Yes");
	    rbRecommendNo = new RadioButton("recommendRadioGroup", "No");
	    
	    rbRecommendYes.setEnabled(true);
		rbRecommendNo.setEnabled(true);
	    
		if((String) survey.get(2) != null){
				if (survey.get(2).equals("1")){
		    		rbRecommendYes.setChecked(true);
		    	}else if (survey.get(2).equals("0")){
		    		rbRecommendNo.setChecked(true);
		    	}
		  }
	    
		if (rbRecommendYes.isChecked() || rbRecommendNo.isChecked()){
	    		rbRecommendYes.setEnabled(false);
	    		rbRecommendNo.setEnabled(false);
	    		}
	    	
	    

	    // Add them to the root panel.
	    HorizontalPanel recommendPanel = new HorizontalPanel();
	    recommendPanel.add(rbRecommendYes);
	    recommendPanel.add(rbRecommendNo);
	    
	    DebugPanel.setText("SurveyViaDecisonDialog  ", "recommendPanel inited");
	    
	   //repeatCustomerPanel
	    rbRepeatCustomerYes = new RadioButton("repeatCustomerRadioGroup", "Yes");
	    rbRepeatCustomerNo = new RadioButton("repeatCustomerRadioGroup", "No");
	   
	    rbRepeatCustomerYes.setEnabled(true);
		rbRepeatCustomerNo.setEnabled(true);
	    
		if((String) survey.get(3) != null){
			if (survey.get(3).equals("1")){
		    		rbRepeatCustomerYes.setChecked(true);
		    	}else if (survey.get(3).equals("0")){
		    		rbRepeatCustomerNo.setChecked(true);
		    	}
		}
	    	
		if (rbRepeatCustomerYes.isChecked() || rbRepeatCustomerNo.isChecked()){
	    		rbRepeatCustomerYes.setEnabled(false);
	    		rbRepeatCustomerNo.setEnabled(false);
	    		
	    	}
	    

	    // Add them to the root panel.
	    HorizontalPanel repeatCustomerPanel = new HorizontalPanel();
	    repeatCustomerPanel.add(rbRepeatCustomerYes);
	    repeatCustomerPanel.add(rbRepeatCustomerNo);
	    
	    DebugPanel.setText("SurveyViaDecisonDialog  ", "repeatCustomerPanel inited");
	    
	  //professionalMannerPanel
	    rbProfessionalMannerYes = new RadioButton("professionalMannerRadioGroup", "Yes");
	    rbProfessionalMannerNo = new RadioButton("professionalMannerRadioGroup", "No");
	    
	    rbProfessionalMannerYes.setEnabled(true);
		rbProfessionalMannerNo.setEnabled(true);
	    
		if((String) survey.get(4) != null){
		    	if (survey.get(4).equals("1")){
		    		rbProfessionalMannerYes.setChecked(true);
		    	}else if (survey.get(4).equals("0")){
		    		rbProfessionalMannerNo.setChecked(true);
		    	}
		}
	    	
		if (rbProfessionalMannerYes.isChecked() || rbProfessionalMannerNo.isChecked()){
	    		rbProfessionalMannerYes.setEnabled(false);
	    		rbProfessionalMannerNo.setEnabled(false);
	    		
	    	}
	    

	    // Add them to the root panel.
	    HorizontalPanel professionalMannerPanel = new HorizontalPanel();
	    professionalMannerPanel.add(rbProfessionalMannerYes);
	    professionalMannerPanel.add(rbProfessionalMannerNo);
	    
	    DebugPanel.setText("SurveyViaDecisonDialog  ", "professionalMannerPanel inited");
	    
	    VerticalPanel vpSurvey = new VerticalPanel();
	    vpSurvey.add(lblTitle);
	    vpSurvey.add(lblSurvey);
	    vpSurvey.add(lblSatisfaction);
	    vpSurvey.add(satisfactionPanel);
	    vpSurvey.add(lblProblemSolved);
	    vpSurvey.add(problemSolvedPanel);
	    vpSurvey.add(lblRecommend);
	    vpSurvey.add(recommendPanel);
	    vpSurvey.add(lblRepeatCustomer);
	    vpSurvey.add(repeatCustomerPanel);
	    vpSurvey.add(lblProfessionalManner);
	    vpSurvey.add(professionalMannerPanel);
	    vpSurvey.add(submitSurveyButton);
	    submitSurveyButton.setEnabled(true);
	    if (!rbSatsfctnXtremely.isEnabled() && !rbSatsfctnVery.isEnabled() && !rbSatsfctnYeahok.isEnabled()
	    		&& !rbSatsfctnNotsogreat.isEnabled() && !rbSatsfctnThiswasawful.isEnabled() && 
    			!rbProblemSolvedYes.isEnabled() && !rbProblemSolvedNo.isEnabled() && !rbProblemSolvedDontknow.isEnabled()
    			&& !rbRecommendYes.isEnabled() && !rbRecommendNo.isEnabled() && !rbRepeatCustomerYes.isEnabled() 
    			&& !rbRepeatCustomerNo.isEnabled() && !rbProfessionalMannerYes.isEnabled() && 
    			!rbProfessionalMannerNo.isEnabled() ) {
	    	vpSurvey.add(lblSurveyComplete);
	    	submitSurveyButton.setEnabled(false);
			Button goBackBtn = new Button("Go back");
			goBackBtn.addClickListener(new ClickListener() {

				public void onClick(Widget arg0) {
					HistoryCommander.newItem(QuickTicket
							.getQuickTicketToken(ticketNumber));
				}

			});
			vpSurvey.add(goBackBtn);
			submitSurveyButton.addStyleName("hide");
		}
	    

		submitSurveyButton.addClickListener(new ClickListener() {

			public void onClick(Widget arg0) {

				if (rbSatsfctnXtremely.isChecked()) {
					strSatisfaction = "4";
				} else if (rbSatsfctnVery.isChecked()) {
					strSatisfaction = "3";
				} else if (rbSatsfctnYeahok.isChecked()) {
					strSatisfaction = "2";
				} else if (rbSatsfctnNotsogreat.isChecked()) {
					strSatisfaction = "1";
				} else if (rbSatsfctnThiswasawful.isChecked()) {
					strSatisfaction = "0";
				} else {
					strSatisfaction = "-1";
				}

				if (rbProblemSolvedYes.isChecked()) {
					strProblemSolved = "1";
				} else if (rbProblemSolvedNo.isChecked()) {
					strProblemSolved = "0";
				} else if (rbProblemSolvedDontknow.isChecked()) {
					strProblemSolved = "2";
				} else {
					strProblemSolved = "-1";
				}

				if (rbRecommendYes.isChecked()) {
					strRecommend = "1";
				} else if (rbRecommendNo.isChecked()) {
					strRecommend = "0";
				} else {
					strRecommend = "-1";
				}

				if (rbRepeatCustomerYes.isChecked()) {
					strRepeatCustomer = "1";
				} else if (rbRepeatCustomerNo.isChecked()) {
					strRepeatCustomer = "0";
				} else {
					strRepeatCustomer = "-1";
				}

				if (rbProfessionalMannerYes.isChecked()) {
					strProfessionalManner = "1";
				} else if (rbProfessionalMannerNo.isChecked()) {
					strProfessionalManner = "0";
				} else {
					strProfessionalManner = "-1";
				}

				String surveyAction;
				if ((String) survey.get(0) == null
						&& (String) survey.get(1) == null
						&& (String) survey.get(2) == null
						&& (String) survey.get(3) == null
						&& (String) survey.get(4) == null) {

					surveyAction = "insert";
				} else {

					surveyAction = "update";
				}

				expertService.submitSurveyToDB(ipnDTO.getSession()
						.getExpertAccount().getUid(), ticketNumber,
						strSatisfaction, strProblemSolved, strRecommend,
						strRepeatCustomer, strProfessionalManner, surveyAction,
						submitSurveyCallback);
			}

		});
	    
	    ticketContainer.setWidget(vpSurvey);
	    
	    DebugPanel.setText("SurveyViaDecisonDialog  ", "In end of showSurvey()");

	}
	
	private void closeSurvey(){
		 /*VerticalPanel vpSurveyClose = new VerticalPanel();
		 Label lblCloseSurvey = new Label("Your feedback for this ticket has been submitted. Thank You.");
		 Button findAnotherExpertBtn = new Button("Find another expert");
		 vpSurveyClose.add(lblCloseSurvey);
		 vpSurveyClose.add(findAnotherExpertBtn);
		 findAnotherExpertBtn.addClickListener(new ClickListener() {

				public void onClick(Widget arg0) {
					String historyBack = TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, new HashMap());
					HistoryCommander.newItem(historyBack);
				}
				
			});
		 ticketContainer.setWidget(vpSurveyClose);*/
		 
		 HistoryCommander.newItem(QuickTicket.getQuickTicketToken(ticketNumber));	
	}
	
	AsyncCallback ipnCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			Window.alert("We were unable to find ticket #" + ticketNumber + " at this time");
			History.back();
		}

		public void onSuccess(Object result) {
			if (result == null) {
				Window.alert("Ticket # " + ticketNumber + " is invalid.");
				History.back();
			}
			ipnDTO = (IPNDTO) result;
			expertService.getExpert(ipnDTO.getSession().getExpertAccount().getUid(), expertCallback);
		}
		
	};
	
	AsyncCallback expertCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				//Window.alert("QuickTicket expertCallback: " +  sBuf.toString());
			} catch (Throwable th) {
				//Window.alert("QuickTicket expertCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			init();
			showTicket();
			DebugPanel.setText("SurveyViaDecisonDialog", "onSuccess: ticketNumber " + ticketNumber);
			expertService.getSurveyFromDB(ipnDTO.getSession().getExpertAccount().getUid(),ticketNumber, getSurveyCallBack);
			
		}		
	};
	
	AsyncCallback getSurveyCallBack = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			//showSurvey();
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				//Window.alert("QuickTicket expertCallback: " +  sBuf.toString());
				DebugPanel.setText("getSurveyCallBack ", "onFail: " +  sBuf.toString());
			} catch (Throwable th) {
				//Window.alert("QuickTicket expertCallback: [throwable]" + th.getMessage());
				DebugPanel.setText("getSurveyCallBack", "in catch " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			survey = (ArrayList) result;
			showSurvey();
		    DebugPanel.setText("getSurveyCallBack : overall_satisfaction = ", (String) survey.get(0));
		    DebugPanel.setText("getSurveyCallBack : problem_solved = ", (String) survey.get(1));
		    DebugPanel.setText("getSurveyCallBack : recommend = ", (String) survey.get(2));
		    DebugPanel.setText("getSurveyCallBack : repeat_customer = ", (String) survey.get(3));
		    DebugPanel.setText("getSurveyCallBack : professional_service = ", (String) survey.get(4));
		    
		    
			
		}		
	};
	
	AsyncCallback submitSurveyCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			//showSurvey();
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				//Window.alert("QuickTicket expertCallback: " +  sBuf.toString());
				DebugPanel.setText("submitSurveyCallback ", "onFail: " +  sBuf.toString());
			} catch (Throwable th) {
				//Window.alert("QuickTicket expertCallback: [throwable]" + th.getMessage());
				DebugPanel.setText("submitSurveyCallback", "in catch " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			closeSurvey();
		}		
	};

	public String getBaseToken() {
		return HISTORY_PREFIX;
	}

	public void onEntry(final Map<String, String> kvMap) {
		if (kvMap.size() > 0) {
			ticketNumber = (String) kvMap.get(QUICK_TICKET_ID);
			if (ticketNumber == null) {
				usePaidTicket = true;
				ticketNumber = (String) kvMap.get(PAYPAL_ID);
			}
			//TODO show locating your expert link message
			ipnService.getCompletionDetails(ticketNumber, ipnCallback);			
		}
	}

	public void onExit() {
		//NOOP
	}
	
	//Only set the widget if we have a positive result
	private void showTicket() {
		resultContainer.setWidget(this);		
	}

	
	public static void register(CommandToken commandToken) {
		currentCommandToken = commandToken;
	}
	
	public static CommandToken getCurrentCommandToken() {
		return currentCommandToken;
	}
	
	public static String getQuickTicketToken(String sessionID) {
		Map kvMap = new HashMap();
		kvMap.put(FindExpert.CONTENT_KEY, HISTORY_PREFIX);
		kvMap.put(QUICK_TICKET_ID, sessionID);
		return TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, kvMap);
	}	
	
	public static String getPaidTicketToken(String paidId) {
		Map kvMap = new HashMap();
		kvMap.put(FindExpert.CONTENT_KEY, HISTORY_PREFIX);
		kvMap.put(PAYPAL_ID, paidId);
		return TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, kvMap, true);
	}

}


