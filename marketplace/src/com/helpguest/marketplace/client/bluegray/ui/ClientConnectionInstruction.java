package com.helpguest.marketplace.client.bluegray.ui;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ClientConnectionInstruction extends SimplePanel {
	private HorizontalPanel base = new HorizontalPanel();
	private VerticalPanel left = new VerticalPanel();
	private VerticalPanel right = new VerticalPanel();
	private Label numberLabel = new Label();
	private HTML contentHTML = new HTML();
	private HTML extraHTML = new HTML();
	
	public ClientConnectionInstruction(String number, String content, String extra) {
		numberLabel.setText(number);
		contentHTML.setHTML(content);
		
		left.add(numberLabel);
		right.add(contentHTML);
		if(extra != null) {
			extraHTML.setHTML(extra);
			right.add(extraHTML);
		}
		numberLabel.setStyleName("client_connection_instruction_number");
		contentHTML.setStyleName("client_connection_instruction_content");
		extraHTML.setStylePrimaryName("client_connection_instruction_extra");
		
		base.add(left);
		base.add(right);
		
		setWidget(base);
	}
}
