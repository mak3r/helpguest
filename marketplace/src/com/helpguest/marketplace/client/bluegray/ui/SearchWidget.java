package com.helpguest.marketplace.client.bluegray.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.QueryConstraints;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.SearchService;
import com.helpguest.marketplace.client.SearchServiceAsync;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.ui.SearchPanel;
import com.helpguest.marketplace.client.ui.VerticalSpacer;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.widgets.FindingResultsMessagePanel;
import com.helpguest.marketplace.client.widgets.FutureFeaturePanel;

public class SearchWidget extends VerticalPanel implements ResultContainer {
	// There is two type of SearchWidget
	public static final int one = 1;
	public static final int two = 2;
	private String query = "";
	
	private Map categoryExpertMap = new HashMap(); 
	//private ExpertLongBrickList expertLongList = null;
    
	// TODO replace this with a SuggestBox
	private final TextBox searchBox = new TextBox();
	private List expertlist = new ArrayList();
	
	// Create the search service proxy
	
	final SearchServiceAsync searchService = (SearchServiceAsync)GWT.create(SearchService.class);
	final ExpertServiceAsync expertService = (ExpertServiceAsync)GWT.create(ExpertService.class);
	private ResultContainer resultContainer;
	private ExpertCategoryDisplay expertCategoryDisplay;
	public final static String SEARCH_COOKIE = "searchCookie";
	
	int searchListNumber = 0;
	boolean searchListEmpty[] = new boolean[5];

	private static SearchWidget searchWidget;
	//added by Ying on 11/05/08 
	private int listSize[] = new int[5];
	private boolean isFullList[] = new boolean[5];
	int searchListSize[] = new int[5];
	private boolean quoted = false;
	final FindingResultsMessagePanel findingResultsMessagePanel = new FindingResultsMessagePanel();


	private SearchWidget(String label, String searchbox,
			final ResultContainer locationOfResults, int type) {
		super();
		this.resultContainer = locationOfResults;

		// Specify the url where chatterbox is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.searchService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "SearchService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		ServiceDefTarget endpoint1 = (ServiceDefTarget) this.expertService;
		String moduleRelativeURL1 = GWT.getModuleBaseURL() + "ExpertService";
		endpoint1.setServiceEntryPoint(moduleRelativeURL1);
		init(label, searchbox, type);
	}

	public static SearchWidget newInstance(String label, String searchbox,
			final ResultContainer locationOfResults, int type) {
		searchWidget = new SearchWidget(label, searchbox, locationOfResults,
				type);
		return searchWidget;
	}

	public static SearchWidget getInstance() {
		return searchWidget;
	}

	public void init(String label, String searchbox, int type) {
		// Label searchLabel = new Label("QUICKSEARCH");
		Label searchLabel = new Label(label);
		searchLabel.setStyleName("search_label");
		// this.searchBox.setText("Enter your problem here");
	
		searchBox.addFocusListener(new FocusListener() {
			public void onFocus(Widget sender) {
				searchBox.selectAll();
				searchBox.setText(null);
			}

			public void onLostFocus(Widget sender) {
			}
		});
		
		this.searchBox.setText(searchbox);
		searchBox.setStyleName("quicksearch_input");
		KeyboardListener keyListener = new KeyboardListener() {
			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				// NOOP
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (KeyboardListener.KEY_ENTER == keyCode) {
					// Enter key pressed handle the search
					search();
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				// TODO this would be a good place to use the SuggestBox oracle
			}
		};
		this.searchBox.addKeyboardListener(keyListener);

		final PushButton findButton = new PushButton();
		findButton.setStyleName("quicksearch_button");
		ClickListener clickListener1 = new ClickListener() {
			public void onClick(Widget sender) {
				// Execute the search
				search();
			}
		};
		findButton.addClickListener(clickListener1);

		// Ying add these for type two use
		final PushButton findResult = new PushButton();
		findResult.setStyleName("findresult_button");
		ClickListener clickListener2 = new ClickListener() {
			public void onClick(Widget sender) {
				// Excute the search within results
				searchingResult();
			}
		};
		findResult.addClickListener(clickListener2);
		
		HorizontalPanel searchButton = new HorizontalPanel();
		HorizontalPanel searchInput = new HorizontalPanel();
		searchInput.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
		searchInput.add(this.searchBox);
		if (type == one) {
			findButton.setText("Find Experts");
			searchInput.add(new VerticalSpacer(5));
			searchInput.add(findButton);
		} else if (type == two) {
			findButton.setText("NEW SEARCH");
			findResult.setText("SEARCH WITHIN RESULTS");
			findResult.addStyleName("hide");
			searchButton.add(findResult);
			searchButton.add(findButton);
		}

		// Now make our widget
		setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
		add(searchLabel);
		add(searchInput);
		add(searchButton);
		setStyleName("search_panel");
	}

	public void search() {
		//expertLongList = new ExpertLongBrickList(resultContainer);
		// first display a 'finding experts message...'
		VerticalPanel someVp = new VerticalPanel();
		someVp.add(findingResultsMessagePanel.startTimer());
		Button stopSearchBtn = new Button("Stop search");
		someVp.add(stopSearchBtn);
		stopSearchBtn.addClickListener(new ClickListener() {

			public void onClick(Widget arg0) {
				expertCategoryDisplay = null;
				String historyBack = TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, new HashMap());
				HistoryCommander.newItem(historyBack);
				findingResultsMessagePanel.stopTimer();				
			}
			
		});
		resultContainer.setWidget(someVp);
		
		//resultContainer.setWidget(new FutureFeaturePanel("finding experts","finding experts","20"));
		query = getQuery();
		QueryConstraints qc = wrapQuery();
		
		//reset the search signals
		searchListNumber = 0;
		for(int i = 0; i < searchListEmpty.length; i++) {
			searchListEmpty[i] = false;
		}
		
		searchService.getExpertIdList(qc, searchIDCallback);
		// Add the latest search to a cookie
		Cookies.setCookie(SEARCH_COOKIE, searchBox.getText());
	}

	private void searchingResult() {
		searchService.getExpertIdList(wrapQuery(), expertlist, researchCallback);
	}

	/**
	 * @return the query
	 */
	public String getQuery() {
		return this.searchBox.getText();
	}

	/**
	 * Set query allows clients to set the initial query value that is
	 * displayed.
	 * 
	 * @param query
	 *            the query to set
	 */
	public void setQuery(final String query) {
		this.searchBox.setText(query);
	}

	public void quotedSearch(final String query) {
		quoted = true;
		searchBox.setText(query);
		search();
	}

	public QueryConstraints wrapQuery() {
		QueryConstraints queryConstraints = new QueryConstraints();
		queryConstraints.setSearchQuery(searchBox.getText());
		return queryConstraints;
	}
	
	public CategoryConstraints wrapCategory(String category){
		CategoryConstraints categoryConstraints = new CategoryConstraints();
		categoryConstraints.setCategoryName(category);
		return categoryConstraints;
	}
    
	AsyncCallback searchIDCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				//Window.alert("SearchWidget searchIDCallback: "
				//		+ sBuf.toString());
			} catch (Throwable th) {
				//Window.alert("SearchWidget searchIDCallback: [throwable]"
				//		+ th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			List expertId = (List) result;				
			expertlist = expertId;
			
			expertCategoryDisplay = new ExpertCategoryDisplay(getQuery(), resultContainer);
			
			//add history command token
			CommandToken searchResult = SearchResultsHandler.createCommandToken(getQuery(), expertCategoryDisplay, searchWidget);
			ExpertPreviewsHandler.registerCommandToken(resultContainer, expertCategoryDisplay);
			
			//search category lists				
			searchService.getExpertIdList(wrapCategory("design"), expertlist, designExpert);
			searchService.getExpertIdList(wrapCategory("development"), expertlist, developmentExpert);
			searchService.getExpertIdList(wrapCategory("business"), expertlist, businessExpert);
			searchService.getExpertIdList(wrapCategory("systems"), expertlist, systemsExpert);
			searchService.getExpertIdList(wrapCategory("personal"), expertlist, personalExpert);
			
			// Inform the HistoryCommander of this command token
			//HistoryCommander.getInstance().register(searchResult);
			DebugPanel.setText("SearchWidget", "SearchResult token currently is: " + searchResult.getBaseToken());
		}
	};
	
	AsyncCallback designExpert = new AsyncCallback(){
		public void onFailure(Throwable caught) {
			try {
				//failed, update
				increaseSearchedCategory(ExpertCategoryDisplay.DESIGN, true);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				 }
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				//Window.alert("design expertCallback: "
				//		+ sBuf.toString());
			} catch (Throwable th) {
				//Window.alert("design expertCallback: [throwable]"
				//		+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result){
			 List designExpertList = (List)result;
			 if (expertCategoryDisplay != null) {
			 expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.DESIGN).mapPosition(designExpertList);
			 if(designExpertList == null) {
				 //Window.alert("No Experts were found matching your search.");
			 }
			 //Window.alert("designlist no:"+designExpertList.size());
			 increaseSearchedCategory(ExpertCategoryDisplay.DESIGN, designExpertList.isEmpty());
			 
			 if(searchListNumber >= 5) {
				 newSearchHistoryItem();
			 }
			 
			 searchListSize[ExpertCategoryDisplay.DESIGN] = designExpertList.size();
			 
			 String debugString = new String();
			 
			 for (int i=0 ; i < designExpertList.size(); i++) {
				
				 expertService.getExpert((String)designExpertList.get(i), designExpertDTO);
				 debugString += (String)designExpertList.get(i) + "\n";
			 }
			 
			 DebugPanel.setText("DesignCategory", "\n" + debugString);
			 }
		}
	};
	
	AsyncCallback developmentExpert = new AsyncCallback(){
		public void onFailure(Throwable caught) {
			try {
				//failed, update
				increaseSearchedCategory(ExpertCategoryDisplay.DEVELOPMENT, true);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				 }
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				//Window.alert("developmentExpert expertCallback: "
				//		+ sBuf.toString());
			} catch (Throwable th) {
				//Window.alert("developmentExpert expertCallback: [throwable]"
				//		+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result){
			 List developmentExpertList = (List)result;
			 if (expertCategoryDisplay != null) {
				 expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.DEVELOPMENT).mapPosition(developmentExpertList);
				 if(developmentExpertList == null) {
					 //Window.alert("Your search returned no results");
				 }
				 
				 increaseSearchedCategory(ExpertCategoryDisplay.DEVELOPMENT, developmentExpertList.isEmpty());
				 
				 if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				 }
				 
				 searchListSize[1] = developmentExpertList.size();
				 for (int i=0 ; i<developmentExpertList.size(); i++){
					 expertService.getExpert((String)developmentExpertList.get(i), developmentExpertDTO);
				 }
			 }
		}
	};
	
	AsyncCallback businessExpert = new AsyncCallback(){
		public void onFailure(Throwable caught) {
			try {
				//failed, update
				increaseSearchedCategory(ExpertCategoryDisplay.BUSINESS, true);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				 }
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("businessExpert expertCallback: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("businessExpert expertCallback: [throwable]"
//						+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result){
			 List businessExpertList= (List)result;
			 if (expertCategoryDisplay != null) {
			 expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.BUSINESS).mapPosition(businessExpertList);
			 if(businessExpertList == null) {
//				 Window.alert("Your search returned no results.");
			 }
			 
			 increaseSearchedCategory(ExpertCategoryDisplay.BUSINESS, businessExpertList.isEmpty());
			 
			 if(searchListNumber >= 5) {
				 newSearchHistoryItem();
			 }
			 
			 searchListSize[2] = businessExpertList.size();
			 for (int i=0 ; i<businessExpertList.size(); i++){
				 expertService.getExpert((String)businessExpertList.get(i), businessExpertDTO);
				
			 	}
			 }
		}
	};
	
	AsyncCallback systemsExpert = new AsyncCallback(){
		public void onFailure(Throwable caught) {
			try {
				//failed, update
				increaseSearchedCategory(ExpertCategoryDisplay.SYSTEMS, true);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				 }
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("systemsExpert expertCallback: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("systemsExpert expertCallback: [throwable]"
//						+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result){
			 List systemsExpertList= (List)result;
			 if (expertCategoryDisplay != null) {
			 expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.SYSTEMS).mapPosition(systemsExpertList);
			 if(systemsExpertList == null) {
//				 Window.alert("Your search returned no results.");
			 }
			 
			 increaseSearchedCategory(ExpertCategoryDisplay.SYSTEMS, systemsExpertList.isEmpty());

			 if(searchListNumber >= 5) {
				 newSearchHistoryItem();
			 }
			 
			 searchListSize[3] = systemsExpertList.size();
			 for (int i=0 ; i<systemsExpertList.size(); i++){
				 expertService.getExpert((String)systemsExpertList.get(i), systemsExpertDTO);
				
			 	}
			 }
		}
	};
	
	AsyncCallback personalExpert = new AsyncCallback(){
		public void onFailure(Throwable caught) {
			try {
				//failed, update
				increaseSearchedCategory(ExpertCategoryDisplay.PERSONAL, true);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				 }
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("personalExpert expertCallback: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("personalExpert expertCallback: [throwable]"
//						+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result){
			 List personalExpertList= (List)result;
			 if (expertCategoryDisplay != null) {
			 expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.PERSONAL).mapPosition(personalExpertList);
			 if(personalExpertList == null) {
//				 Window.alert("Your search returned no results.");
			 }
			 
			 increaseSearchedCategory(ExpertCategoryDisplay.PERSONAL, personalExpertList.isEmpty());
			 
			 if(searchListNumber >= 5) {
				 newSearchHistoryItem();
			 }
			 
			 searchListSize[4] = personalExpertList.size();
			 for (int i=0 ; i<personalExpertList.size(); i++){
				 
				 expertService.getExpert((String)personalExpertList.get(i), personalExpertDTO);
			 	}
			 }
			 findingResultsMessagePanel.stopTimer();	
		}
	};

	AsyncCallback researchCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("SearchWidget researchCallback: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("SearchWidget researchCallback: [throwable]"
//						+ th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			List expert = (List) result;
			List expertDTOList = new ArrayList();
			expertlist = expert;
			//Category the search result list
			expertCategoryDisplay = new ExpertCategoryDisplay(getQuery(), resultContainer);
			
			searchService.getExpertIdList(wrapCategory("design"), expertlist, designExpert);
			searchService.getExpertIdList(wrapCategory("development"), expertlist, developmentExpert);
			searchService.getExpertIdList(wrapCategory("business"), expertlist, businessExpert);
			searchService.getExpertIdList(wrapCategory("systems"), expertlist, systemsExpert);
			searchService.getExpertIdList(wrapCategory("personal"), expertlist, personalExpert);
			
			CommandToken searchResult = SearchResultsHandler
					.createCommandToken(getQuery(), expertCategoryDisplay,
							searchWidget);
			// Inform the HistoryCommander of this command token
			HistoryCommander.getInstance().register(searchResult);

			// Indicate that a new history event has taken place
			HistoryCommander.newItem(searchResult.getBaseToken());

		}
	};
	
	AsyncCallback designExpertDTO = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				isFull(ExpertCategoryDisplay.DESIGN);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				}
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("SearchWidget designExpertDTO: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("SearchWidget designExpertDTO: [throwable]"
//						+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result) {
			ExpertDTO expert = (ExpertDTO)result;
			if (expertCategoryDisplay != null) {
			expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.DESIGN).addExpertDTO(expert);
			ExpertPreviewsHandler.registerPreview(resultContainer, expert);
			isFull(ExpertCategoryDisplay.DESIGN);
			if(searchListNumber >= 5) {
				 newSearchHistoryItem();
				}
			}
		}
	};
	
	AsyncCallback developmentExpertDTO = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				isFull(ExpertCategoryDisplay.DEVELOPMENT);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				}
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("SearchWidget developmentExpertDTO: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("SearchWidget developmentExpertDTO: [throwable]"
//						+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result) {
			ExpertDTO expert = (ExpertDTO)result;
			if (expertCategoryDisplay != null) {
			isFull(ExpertCategoryDisplay.DEVELOPMENT);
			expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.DEVELOPMENT).addExpertDTO(expert);
			ExpertPreviewsHandler.registerPreview(resultContainer, expert);
			if(searchListNumber >= 5) {
				 newSearchHistoryItem();
				}
			}
		}
	};
	
	AsyncCallback businessExpertDTO = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				isFull(ExpertCategoryDisplay.BUSINESS);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				}
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("SearchWidget businessExpertDTO: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("SearchWidget businessExpertDTO: [throwable]"
//						+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result) {
			ExpertDTO expert = (ExpertDTO)result;
			if (expertCategoryDisplay != null) {
			isFull(ExpertCategoryDisplay.BUSINESS);
			expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.BUSINESS).addExpertDTO(expert);
			ExpertPreviewsHandler.registerPreview(resultContainer, expert);
			if(searchListNumber >= 5) {
				 newSearchHistoryItem();
				}
			}
		}
	};
	
	AsyncCallback systemsExpertDTO = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				isFull(ExpertCategoryDisplay.SYSTEMS);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				}
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("SearchWidget systemsExpertDTO: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("SearchWidget systemsExpertDTO: [throwable]"
//						+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result) {
			ExpertDTO expert = (ExpertDTO)result;
			if (expertCategoryDisplay != null) {
			isFull(ExpertCategoryDisplay.SYSTEMS);
			expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.SYSTEMS).addExpertDTO(expert);
			ExpertPreviewsHandler.registerPreview(resultContainer, expert);
			if(searchListNumber >= 5) {
				 newSearchHistoryItem();
				}
			}
		}
	};
	
	AsyncCallback personalExpertDTO = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				isFull(ExpertCategoryDisplay.PERSONAL);
				if(searchListNumber >= 5) {
					 newSearchHistoryItem();
				}
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("SearchWidget personalExpertDTO: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("SearchWidget personalExpertDTO: [throwable]"
//						+ th.getMessage());
			}
		}
		
		public void onSuccess(Object result) {
			ExpertDTO expert = (ExpertDTO)result;
			if (expertCategoryDisplay != null) {
			isFull(ExpertCategoryDisplay.PERSONAL);
			expertCategoryDisplay.getExpertLongBrickList(ExpertCategoryDisplay.PERSONAL).addExpertDTO(expert);
			ExpertPreviewsHandler.registerPreview(resultContainer, expert);
			if(searchListNumber >= 5) {
				 newSearchHistoryItem();
				}
			}
		}
	};
	
	public static String getSearchCookie() {
		return Cookies.getCookie(SearchPanel.SEARCH_COOKIE);
	}
    
	public ResultContainer getresultContainer(){
		return resultContainer;
	}
	
	/*
	public ExpertLongBrickList getExpertLongBrickList(){
		return expertLongList;
	}
	*/
	public Map getMap(){
		return categoryExpertMap;
	}
	
	public void setExpertList(List expertList){
		this.expertlist = expertList;
	}
	
	public List getExpertList(){
		return expertlist;
	}
	/**
	 * {@inheritDoc}
	 */
	public void setWidget(Widget result) {
		resultContainer.setWidget(result);
	}

	public void reset() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Get the list for a new category.
	 * */
	private synchronized void increaseSearchedCategory(int category, boolean empty) {
		if(empty == true) {
			increaseSearchedNumber();
		}
		searchListEmpty[category] = empty;
	}
	
	/**
	 * Increase the searched list number.
	 * */
	private synchronized void increaseSearchedNumber() {
		searchListNumber++;
	}
	
	/**
	 * Increase the fetched experts for each category list.
	 * */
	private synchronized void isFull(int category){
		listSize[category]++;
		if(listSize[category] >= searchListSize[category]) {
			isFullList[category] = true;
			increaseSearchedNumber();
		} else {
			isFullList[category] = false;
		}
	}
	
	/**
	 * Add the new history item for search.
	 * */
	public void newSearchHistoryItem() {
		int i;
		for(i = 0; i < searchListEmpty.length; i++) {
			if(!searchListEmpty[i]) {
				break;
			}
		}
		
		i = i % 5;
		String category = null;
		switch(i) {
		case ExpertCategoryDisplay.DESIGN:
			category = ExpertCategoryDisplay.design;
			break;
		case ExpertCategoryDisplay.DEVELOPMENT:
			category = ExpertCategoryDisplay.development;
			break;
		case ExpertCategoryDisplay.BUSINESS:
			category = ExpertCategoryDisplay.business;
			break;
		case ExpertCategoryDisplay.SYSTEMS:
			category = ExpertCategoryDisplay.systems;
			break;
		case ExpertCategoryDisplay.PERSONAL:
			category = ExpertCategoryDisplay.personal;
			break;
		}
		
		searchListNumber = 0;
		for(i = 0; i < 5; i++) {
			searchListEmpty[i] = false;
			searchListSize[i] = 0;
			listSize[i] = 0;
			isFullList[i] = false;
		}
		
		if(quoted) {
			//if it is a quoted search, change the history with the current history token.
			//Window.alert("History Back!");
			//History.back();
			//History.back();
			String historyToken = SearchResultsHandler.getSearchToken(query, category);
			History.onHistoryChanged(historyToken);
		} else {
			HistoryCommander.newItem(SearchResultsHandler.getSearchToken(query, category));
		}
		
		quoted = false;
	}
}
	

