package com.helpguest.marketplace.client.bluegray.ui;
/**
 * @author ying
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.OfferingsConstraints;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.ExpertiseService;
import com.helpguest.marketplace.client.ExpertiseServiceAsync;
import com.helpguest.marketplace.client.ui.VisualCuesImageBundle;

public class CategoryIcon extends SimplePanel{
	private HorizontalPanel hp = new HorizontalPanel();
	private VisualCuesImageBundle visualCue = (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);
	private Map offeringMap = new HashMap();
	private String UID ;
	
	final private ExpertiseServiceAsync expertiseService = (ExpertiseServiceAsync) GWT
	.create(ExpertiseService.class);
	
	public CategoryIcon(){
		hp.add(visualCue.design().createImage());
		hp.add(visualCue.development().createImage());
		hp.add(visualCue.business().createImage());
		hp.add(visualCue.personal().createImage());
		hp.add(visualCue.system().createImage());
		
		setWidget(hp);
		setStyleName("category_icon");
	}
	
	public CategoryIcon(String UID){
		this.UID = UID;
		ServiceDefTarget expertiseEndpoint = (ServiceDefTarget) this.expertiseService;
		String expertiseModuleRelativeURL = GWT.getModuleBaseURL()
				+ "ExpertiseService";
		expertiseEndpoint.setServiceEntryPoint(expertiseModuleRelativeURL);
		
		expertiseService.getExpertCategories(UID, asynCallback);
		setWidget(hp);
		setStyleName("category_icon");
	}
	
	AsyncCallback asynCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				//Window.alert("CategoryIcon asyncCallback");
			} catch (Throwable th) {
				//Window.alert("CategoryIcon asyncCallback: [throwable]");
			}
		}
		
		public void onSuccess(Object result) {
			List categories = (List)result;
			//Window.alert("" + categories.size());
			for(int i = 0; i < categories.size(); i++) {
				String category = (String)categories.get(i);
				if(category.equals(CategoryContentTabbedPanel.DESIGN)) {
					hp.add(visualCue.design().createImage());
				} else if(category.equals(CategoryContentTabbedPanel.DEVELOPMENT)) {
					hp.add(visualCue.development().createImage());
				} else if(category.equals(CategoryContentTabbedPanel.BUSINESS)) {
					hp.add(visualCue.business().createImage());
				} else if(category.equals(CategoryContentTabbedPanel.SYSTEMS)) {
					hp.add(visualCue.system().createImage());
				} if(category.equals(CategoryContentTabbedPanel.PERSONAL)) {
					hp.add(visualCue.personal().createImage());
				};
			}
		}
	};
}
