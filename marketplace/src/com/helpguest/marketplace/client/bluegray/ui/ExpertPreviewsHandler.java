package com.helpguest.marketplace.client.bluegray.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.Window;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.util.DebugPanel;

public class ExpertPreviewsHandler {
	public static String TOKEN = "ExpertPreview";
	public static String QUERY = "query";
	public static String UID = "uid";
	
	private static Map<String,ExpertDTO> expertDTOs = new HashMap<String,ExpertDTO>();
	public static Map<String,ExpertPreview> expertCommandMap = new HashMap<String,ExpertPreview>();
	
	public static void registerCommandToken(ResultContainer resultContainer, ExpertCategoryDisplay expertCategoryDisplay) {
		expertCommandMap.put(expertCategoryDisplay.getQuery(), new ExpertPreview(resultContainer, expertCategoryDisplay));
	}
	
	public static void registerPreview(ResultContainer resultContainer, ExpertDTO dto) {
		expertCommandMap.put(dto.getExpertUid(), new ExpertPreview(resultContainer, dto));
	}
	
	public static CommandToken getCommandToken(String queryId) {
		return expertCommandMap.get(queryId);
	}
	
	public static String getExpertPreviewToken(String expertUID) {
		Map kvMap = new HashMap();
		kvMap.put(FindExpert.CONTENT_KEY, TOKEN);
		kvMap.put(UID, expertUID);
		return TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, kvMap);
	}

	public static ExpertDTO getExpertDTO(final String uid) {
		return (ExpertDTO)expertDTOs.get(uid);
	}
	
	private static class ExpertPreview implements CommandToken {
		List expertList = new ArrayList();
		
		final ExpertServiceAsync expertService = (ExpertServiceAsync) GWT.create(ExpertService.class);

		private ResultContainer resultContainer;
		private ExpertCategoryDisplay expertCategoryDisplay;
		private ExpertDTO expertDTO;
		private String prevExpert;
		private String nextExpert;
		
		private ExpertPreview() {
			ServiceDefTarget endpoint = (ServiceDefTarget) this.expertService;
			String moduleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
			endpoint.setServiceEntryPoint(moduleRelativeURL);
		}
		
		public ExpertPreview(ResultContainer resultContainer, ExpertCategoryDisplay expertCategoryDisplay) {
			this();
			this.resultContainer = resultContainer;
			this.expertCategoryDisplay = expertCategoryDisplay;
			for (int i=0; i<expertCategoryDisplay.getExpertList().size(); i++) {
				ExpertDTO dto = (ExpertDTO) expertCategoryDisplay.getExpertList().get(i);
				expertDTOs.put(dto.getExpertUid(), dto);
			}
			expertList.addAll(expertCategoryDisplay.getExpertList());
		}
		
		public ExpertPreview(ResultContainer resultContainer, ExpertDTO dto) {
			this();
			this.resultContainer = resultContainer;
			expertList.add(dto);
			expertDTOs.put(dto.getExpertUid(), dto);
		}
	
		public String getBaseToken() {
			Map kvMap = new HashMap();
			kvMap.put(FindExpert.CONTENT_KEY, TOKEN);
			return TabbedContentPanel.getHistoryCommandForTab(FindExpert.TOKEN, kvMap);
		}

		private void showPreview() {
			resultContainer.setWidget(new ClientViewOfExpert(expertDTO, prevExpert, nextExpert));
		}
		
		public void onEntry(Map<String, String> kvMap) {
			String expertUID = (String)kvMap.get(UID);
			DebugPanel.setText("ExpertPreviewsHandler.ExpertPreview", "********ExpertPreview onEntry expertUID is " + expertUID);
			DebugPanel.setText("ExpertPreviewsHandler.ExpertPreview", "size " + expertList.size());
			
			expertDTO = (ExpertDTO) expertDTOs.get(expertUID);
			DebugPanel.setText("ExpertPreviewsHandler.ExpertPreview", "dto is " + (expertDTO==null?"null":"not null"));
			DebugPanel.setText("ExpertPreviewsHandler.ExpertPreview", "dto is " + expertDTO.getExpertUid());
			if (expertDTO == null) {
				//If we don't have an expertDTO, lets go get one.
				expertService.getExpert(expertUID, callback);
			}else{
			showPreview();}
		}
		
		public void onExit() {
			
		}
		
		AsyncCallback callback = new AsyncCallback() {
			public void onFailure(Throwable caught) {
				//FIXME: This should go to the oops page instead
				//Window.alert("I WANT TO BE AN OOPS PAGE \nfail to init and add expert brick.");
				DebugPanel.setText("ExpertPreviewsHandler$ExpertPreview", "callback.onFailure()");
			}
			
			public void onSuccess(Object result) {
				DebugPanel.setText("ExpertPreviewsHandler$ExpertPreview", "callback.onSuccess()");
				expertDTO = (ExpertDTO) result;
				expertList.add(expertDTO);
				expertDTOs.put(expertDTO.getExpertUid(), expertDTO);
				showPreview();
			}
		};
	}
}
