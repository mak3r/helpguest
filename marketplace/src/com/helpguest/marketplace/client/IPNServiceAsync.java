/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author mabrams
 *
 */
public interface IPNServiceAsync {

	public void getCompletionDetails(final String sessionId, final AsyncCallback callback);

}
