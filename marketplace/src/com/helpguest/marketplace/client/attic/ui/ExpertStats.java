/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 27, 2007
 */
package com.helpguest.marketplace.client.attic.ui;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.brick.DetailBrick;
import com.helpguest.marketplace.client.dto.ExpertStatsDTO;

/**
 * @author mabrams
 *
 */
public class ExpertStats extends VerticalPanel {

	public ExpertStats(ExpertStatsDTO stats) {
		super();

		DetailBrick problemsHandled =
			new DetailBrick("Total problems handled:", 
					stats.getProblemsHandled());
		DetailBrick ttr =
			new DetailBrick("Average time to resolution:",
					stats.getAvgTimeToResolution());
		DetailBrick numSolved =
			new DetailBrick("Number of problems solved:",
					stats.getNumberResolved());
		DetailBrick repeats =
			new DetailBrick("How many repeat guests:",
					stats.getNumberRepeatGuests());
		add(problemsHandled);
		add(ttr);
		add(numSolved);
		add(repeats);
	}
}
