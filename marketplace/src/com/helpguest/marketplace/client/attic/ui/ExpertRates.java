/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 27, 2007
 */
package com.helpguest.marketplace.client.attic.ui;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.brick.DetailBrick;
import com.helpguest.marketplace.client.dto.PaymentPlanDTO;

/**
 * @author mabrams
 *
 */
public class ExpertRates extends VerticalPanel {

	public ExpertRates(PaymentPlanDTO paymentPlan) {
		super();
		DetailBrick depositAmt =
			new DetailBrick("You will need to deposit: ",
					"$" + paymentPlan.getDepositAmount());
		DetailBrick minFee =
			new DetailBrick("The minimum fee is:",
					"$" + paymentPlan.getMinimumFee());
		DetailBrick expertRate = 
			new DetailBrick("After " + paymentPlan.getMinTime()
					+ " minutes you will be charged at the Expert's rate of:",
					"$" + paymentPlan.getRatePerMinute());
		add(depositAmt);
		add(minFee);
		add(expertRate);
	}
}
