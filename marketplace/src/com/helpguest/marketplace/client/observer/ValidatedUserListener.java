package com.helpguest.marketplace.client.observer;

public interface ValidatedUserListener {

	void onChange(final ValidatedUser validatedUser);
}
