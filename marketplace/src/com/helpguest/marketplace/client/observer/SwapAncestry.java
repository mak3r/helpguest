package com.helpguest.marketplace.client.observer;

import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class SwapAncestry {

	private SimplePanel parent;
	private Widget sibling;
	
	public SwapAncestry(final SimplePanel parent, final Widget sibling) {
		this.parent = parent;
		this.sibling = sibling;
	}

	/**
	 * @return the parent
	 */
	public SimplePanel getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(SimplePanel parent) {
		this.parent = parent;
	}

	/**
	 * @return the sibling
	 */
	public Widget getSibling() {
		return sibling;
	}

	/**
	 * @param sibling the sibling to set
	 */
	public void setSibling(Widget sibling) {
		this.sibling = sibling;
	}
	
	
}
