package com.helpguest.marketplace.client.observer;


public interface TabToken {
	
	public String getTabToken();

}
