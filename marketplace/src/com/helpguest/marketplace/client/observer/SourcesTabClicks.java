package com.helpguest.marketplace.client.observer;

public interface SourcesTabClicks {
	void addTabClickListener(final TabClickListener tabClickListener);
	void removeTabClickListener(final TabClickListener tabClickListener);
}
