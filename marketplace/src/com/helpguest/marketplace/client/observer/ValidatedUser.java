package com.helpguest.marketplace.client.observer;

import com.helpguest.marketplace.client.dto.ExpertDTO;

public interface ValidatedUser {

	ExpertDTO getValidatedExpert();
}
