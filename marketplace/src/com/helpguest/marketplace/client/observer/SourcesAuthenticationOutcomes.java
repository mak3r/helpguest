/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 22, 2007
 */
package com.helpguest.marketplace.client.observer;

/**
 * @author mabrams
 *
 */
public interface SourcesAuthenticationOutcomes {
	
	void addAuthenticationListener(final AuthenticationListener authListener);
	void removeAuthenticationListener(final AuthenticationListener authListener);
	
}
