package com.helpguest.marketplace.client.observer;

public interface TabClickListener {
	void onClick(final TabToken tabToken);
}
