package com.helpguest.marketplace.client.observer;

public interface SourcesValidatedUser {

	void addValidatedUserListener(final ValidatedUserListener validatedUserListener);
	void removeValidatedUserListener(final ValidatedUserListener validatedUserListener);
}
