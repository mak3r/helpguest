/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 22, 2007
 */
package com.helpguest.marketplace.client.observer;

import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.protocol.AuthenticationOutcome;
import com.helpguest.gwt.protocol.LoginType;

/**
 * @author mabrams
 *
 */
public interface Authenticator extends SourcesAuthenticationOutcomes {

	AuthenticationOutcome getAuthenticationOutcome();
	
	/**
	 * It's not enough to have th AuthenticationOutcome,
	 * we also need to know if they are logged in.
	 * For example, one could login and then try to
	 * login in again unsuccessfully, but still be
	 * logged in with the old credentials.
	 * Also, it's possible to be logged in from a
	 * previous session, start the app and still be
	 * logged in.
	 */
	boolean isLoggedIn();
	
	String getAuthenticatedParty();
	
	LoginType getLoginType();
	
	Widget getExpiredWidget();
}
