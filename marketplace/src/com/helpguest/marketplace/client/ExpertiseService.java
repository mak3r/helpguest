/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.helpguest.gwt.protocol.VerifyEmailOutcome;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.OfferingsConstraints;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public interface ExpertiseService extends RemoteService {

	/*
	 * @param limit is the maximum number to return.
	 * @return a list of the most requested search terms.
	 */
	public List<java.lang.String> getMostRequested(final int limit);
	
	/*
	 * @param limit is the maximum number to return.
	 * @return a list of the most common expertise registered.
	 */
	public List<java.lang.String> getMostCommon(final int limit);
	
	/*
	 * @param dto the Expert whose expertise we are retrieving
	 * @return a list of this experts expertise
	 */
	public List<java.lang.String> getExpertise(final ExpertDTO dto);

	/*
	 * @param dto the Expert whose expertise we are retrieving
	 * @return a list of the most offered expertise
	 */
	public List<java.lang.String> getMostOffered(final int limit);
	
	/**
	 * prahalad 07/09/08 adding method to get a list of categories from DB
	 * ultimately this will enable expert to add tags under appropriate catgs. and offgs.
	 * @return the list of categories.
	 */
	public List<java.lang.String> getCategories();
	
	/**
	 * prahalad 07/22/08 adding method to get categories, offerings pairs from DB
	 * ultimately this will enable expert to add tags under appropriate catgs. and offgs.
	 * @return the list of categories.
	 */
	public List<java.lang.String> getCatOffgs();
	
	/**
	 * prahalad 10/31/08 adding method to get categories, offerings, tags tuples from DB
	 * ultimately this will enable expert to manage tags under appropriate catgs. and offgs.
	 * @return the list of categories.
	 */
	public List<java.lang.String> getCatOffgTags(String expert_id);
	/**
	 * 
	 * prahalad 07/09/08 adding method to get a list of offerings from DB for a category
	 * ultimately this will enable expert to add tags under appropriate catgs. and offgs.
	 * @param a CategoryConstraints to indicate which offerings to retrieve
	 * @return the list of offerings associated with the given CategoryContraints.
	 */
	public List<java.lang.String> getOfferings(final CategoryConstraints cc);
	
	/**
	 * 
	 * prahalad 07/14/08 adding method to get tags from DB for a category, offering and expertid
	 * ultimately this will enable expert to add tags under appropriate catgs. and offgs.
	 * @param a CategoryConstraints to indicate which offerings to retrieve
	 * @return the list of offerings associated with the given CategoryContraints.
	 */
    public List<java.lang.String> getTags(final CategoryConstraints cc,final OfferingsConstraints oc, final String expert_id);
    
    public void updateTags(final CategoryConstraints cc, final OfferingsConstraints oc, final String expert_id, final String tag_content);
    
    /**
     * ying 09/08/08
     * @param expert_id is the id of the expert
     * @return the list of categories
     * */
    public List<java.lang.String> getExpertCategories(final String expert_id);
}
