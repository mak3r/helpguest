/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Oct 4, 2007
 */
package com.helpguest.marketplace.client.ui;

import java.util.Map;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.widgets.PaddedPanel;

/**
 * @author mabrams
 *
 */
public class QuickTicketPanel extends SimplePanel  implements CommandToken {

	public final static String FAILED_TICKET_NUMBER = "failedTicketNumber";
	final static String HISTORY_PREFIX = HeaderMenu.TICKET;
	private String ticketNumber;

	private String TICKET_NOT_FOUND = "No ticket number matching your query was found.";
	private Label ticketVariant = new Label();
	private TextBox ticketBox = new TextBox();
	
	private String agreeStatement =
		"I have read and agree with" +
		"<br/>" +
		"&nbsp;&nbsp;&nbsp;&nbsp;the " +
		"<a href=\"GuestEULA.html\" " +
		"target=\"_blank\">terms and conditions</a>.";	
	private CheckBox enableAgreeButton = new CheckBox(agreeStatement, true);
	private Button goButton = new Button("GO");
	
	public QuickTicketPanel() {
		super();
		init();
	}
	
	private void init() {
		ticketBox.setWidth("9em");
		ticketBox.setHeight("65");
		ticketBox.setStyleName("hg-QuickTicket-textBox");
		
		goButton.setEnabled(false);
		goButton.setStyleName("hg-QuickTicket-button");

		HorizontalPanel goAgree = new HorizontalPanel();
		goAgree.add(enableAgreeButton);
		goAgree.add(new VerticalSpacer(10));
		goAgree.add(goButton);

		Grid goTicket = new Grid(6, 1);
		goTicket.setWidget(0, 0, ticketVariant);
		goTicket.setWidget(1, 0, new HorizontalSpacer(20));
		goTicket.setWidget(2, 0, ticketBox);
		goTicket.setWidget(3, 0, new HorizontalSpacer(10));
		goTicket.setWidget(4, 0, goAgree);
		goTicket.getCellFormatter().setAlignment(4, 0, HorizontalPanel.ALIGN_CENTER, VerticalPanel.ALIGN_MIDDLE);
		goTicket.setWidget(5, 0, new HorizontalSpacer(20));

		PaddedPanel goTicketPad = new PaddedPanel(goTicket, 30);
		
		TwoTonePanel ttp = new TwoTonePanel("100%");
		ttp.setHeading(new Label("Enter your Quick Ticket"));
		ttp.setBody(goTicketPad);
		
		setWidget(ttp);

		//Create some listeners
		enableAgreeButton.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				// TODO Auto-generated method stub
				if (enableAgreeButton.isChecked()) {
					goButton.setEnabled(true);
				} else {
					goButton.setEnabled(false);
				}
			}
        	
        });
		
		goButton.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				ticketVariant.setText(null);
				HistoryCommander.newItem(
						HistoryCommander.buildToken(
								"PaymentComplete", "sessionId", ticketBox.getText()));
				/*
						"PaymentComplete" +
						HistoryCommander.BASE_SEPARATOR + "sessionId" +
						HistoryCommander.VALUE_SEPARATOR + ticketBox.getText());
				*/
				ticketBox.setText(null);
			}
			
		});
		
		ticketBox.addKeyboardListener(new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (keyCode == KEY_ENTER) {
					goButton.click();
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				int cursorPos = ticketBox.getCursorPos();
				//Force all caps in the ticket box
				ticketBox.setText(ticketBox.getText().toUpperCase());				
				ticketBox.setCursorPos(cursorPos);
			}
			
		});
	}
	
	public String getBaseToken() {
		return HISTORY_PREFIX;
	}

	public void onEntry(final Map<String, String> kvMap) {
		ticketBox.setText(null);
		ticketVariant.setText(null);
		enableAgreeButton.setChecked(false);
		goButton.setEnabled(false);
		if (kvMap.size() > 0) {
			ticketNumber = (String) kvMap.get(FAILED_TICKET_NUMBER);
			if (ticketNumber != null && !ticketNumber.equals("")) {
				ticketVariant.setText(TICKET_NOT_FOUND);
				ticketVariant.setStyleName("hg-error");
				ticketBox.setText(ticketNumber);
			}
		}
	}

	public void onExit() {
	}
}
