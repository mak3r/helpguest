package com.helpguest.marketplace.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;

public class FooterMenu extends HorizontalPanel {
	//prahalad added begin 6/3/08
	private static MarketPlaceConstants fmConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String fmFqdn = fmConstants.fullyQualifiedDomainName();
    private static String fmWebUriPrefix = fmConstants.webUriPrefix();
	//prahalad added begin 6/3/08
	
	final HTML[] menuItems = {
			//new HTML("<a href=\"https://secure.helpguest.com/index.html\">[Home]</a>"),
			//new HTML("<a href=\"https://secure.helpguest.com/beta2/sign-in.html\">[Expert sign-in]</a>"),
			//new HTML("<a href=\"https://secure.helpguest.com/beta2/sign-up.html\">[Expert sign-up]</a>"),
			//prahalad changing static urls 6/3/08
			new HTML("<a href=\"" + fmWebUriPrefix + "://" + fmFqdn + "/index.html\">[Home]</a>"),
			new HTML("<a href=\"" + fmWebUriPrefix + "://" + fmFqdn + "/beta2/sign-in.html\">[Expert sign-in]</a>"),
			new HTML("<a href=\"" + fmWebUriPrefix + "://" + fmFqdn + "/beta2/sign-up.html\">[Expert sign-up]</a>"),
			new HTML("<a href=\"http://blog.helpguest.com/\">[Blog]</a>"),
			//new HTML("<a href=\"https://secure.helpguest.com/about.html\">[About]</a>"),
			new HTML("<a href=\"" + fmWebUriPrefix + "://" + fmFqdn + "/about.html\">[About]</a>"),
			new HTML("<a href=\"mailto:nobody@helpguest.com\">[Contact]</a>")
	};

	public FooterMenu() {
		super();
		init();
	}
	
	private void init() {
		setWidth("100%");
		setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
		setStyleName("footer_menu");
		
		HorizontalPanel menu = new HorizontalPanel();
		menu.setSpacing(5);
		for (int i=0; i<menuItems.length; i++) {
			menuItems[i].setStyleName("footer_menu_item");
			menu.add(menuItems[i]);
			menu.add(new VerticalSpacer(5));
		}		
		add(menu);
	}
	
}
