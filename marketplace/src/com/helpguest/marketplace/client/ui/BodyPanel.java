/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 22, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author mabrams
 *
 */
public class BodyPanel extends SimplePanel implements ResultContainer {

	public BodyPanel() {
		
	}
	
	public BodyPanel(String pageWidth, String styleName) {
		setWidth(pageWidth);
		setStyleName(styleName);
	}

	public void reset() {
		// TODO Auto-generated method stub
		
	}
}
