/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 22, 2007
 */
package com.helpguest.marketplace.client.ui;

import java.util.List;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.ExpertiseService;
import com.helpguest.marketplace.client.ExpertiseServiceAsync;

/**
 * @author mabrams
 * 
 */
public class TopThirty extends RoundedPanel {

	//Create the search service proxy
	final ExpertiseServiceAsync expertiseService =
		(ExpertiseServiceAsync) GWT.create(ExpertiseService.class);
	final FlexTable flexTable = new FlexTable();
	
	public TopThirty() {
		super(RoundedPanel.ALL, 2);
		//Specify the url where chatterbox is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.expertiseService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "ExpertiseService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		init();
	}

	public void init() {
		Label top30Label = new Label(" Top 30 Expert Offerings");
		top30Label.setStyleName("top_thirty");
		flexTable.setStyleName("top_thirty");
		flexTable.setCellSpacing(3);
		flexTable.setWidget(0, 1, top30Label);
		//Allow the label to cover all columns
		flexTable.getFlexCellFormatter().setColSpan(0, 1, 3);

		//for now just get the top 30 expert offerings
		expertiseService.getMostOffered(30, callback);
		
		HorizontalPanel gridShift = new HorizontalPanel();
		gridShift.add(new VerticalSpacer(10));
		gridShift.add(flexTable);
		gridShift.setStyleName("top_thirty");

		VerticalPanel top30 = new VerticalPanel();
		top30.setStyleName("top_thirty");
		top30.setSpacing(5);
		top30.add(top30Label);
		top30.add(gridShift);

		setWidget(top30);
		setCornerStyleName("top_thirty_corners");
		setStyleName("top_thirty_backdrop");
		setWidth("600px");
	}
	
	AsyncCallback callback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("TopThirty callback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("TopThirty callback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			List expertiseList = (List) result;
			int rows = 5;
			//Start at column 1
			int columns = expertiseList.size()/rows;
			//start at row 1
			int i=0;
			for (int row=1; row<=rows; row++) {				
				for (int col=1; col<=columns; col++) {
					final String expertiseValue = (String) expertiseList.get(i++);
					Hyperlink expertise = 
						new Hyperlink(expertiseValue,
								SearchResultHandler.getSearchToken(expertiseValue));
					ClickListener expertiseClick = new ClickListener() {

						public void onClick(Widget sender) {
							SearchPanel searchPanel = SearchPanel.getInstance();
							if (searchPanel != null) {
								searchPanel.quotedSearch(expertiseValue);
							}
						}
						
					};
					expertise.addClickListener(expertiseClick);
					
					flexTable.setWidget(row, col, expertise);
					if (i > expertiseList.size()) {
						break;
					}
				}
			}
		}
		
	};
}
