/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 25, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Set the styleName to the color of the background.
 * 
 * @author mabrams
 *
 */
public class RoundedImage extends RoundedPanel {

	public RoundedImage(Image toWrap, String backgroundStyle, int cellSpacing) {
		super(RoundedPanel.ALL, 2);
		VerticalPanel cell = new VerticalPanel();
		cell.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		cell.setVerticalAlignment(VerticalPanel.ALIGN_MIDDLE);
		cell.setSpacing(cellSpacing);
		cell.add(toWrap);
		cell.setStyleName("rounded_image_background");
		add(cell);
		setStyleName(backgroundStyle);
		setCornerStyleName("rounded_image_corners");
	}
}
