/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 21, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.ImageBundle;

/**
 * @author mabrams
 *
 */
public interface VisualCuesImageBundle extends ImageBundle {

	@Resource("com/helpguest/marketplace/public/images/online.png")
	AbstractImagePrototype onlineIcon();
	
	
	@Resource("com/helpguest/marketplace/public/images/offline.png")
	AbstractImagePrototype offlineIcon();
	
	
	@Resource("com/helpguest/marketplace/public/images/online12.png")
	AbstractImagePrototype online12Icon();
	
	
    @Resource("com/helpguest/marketplace/public/images/offline12.png")
	AbstractImagePrototype offline12Icon();
	
	
	@Resource("com/helpguest/marketplace/public/images/brand.png")
	AbstractImagePrototype brandIcon();
	
	
	@Resource("com/helpguest/marketplace/public/images/logo-final-horiz.png")
	AbstractImagePrototype wideLogo39Height();

	
	@Resource("com/helpguest/marketplace/public/images/starFull.png")
	AbstractImagePrototype starFullIcon();
	
	
	@Resource("com/helpguest/marketplace/public/images/starEmpty.png")
	AbstractImagePrototype starEmptyIcon();
	
	
	@Resource("com/helpguest/marketplace/public/images/starHalf.png")
	AbstractImagePrototype starHalfIcon();

	
	@Resource("com/helpguest/marketplace/public/images/thinkingAngel.png")
	AbstractImagePrototype thinkingAngelIcon();

	
	@Resource("com/helpguest/marketplace/public/images/guestInterface.png")
	AbstractImagePrototype guestInterfaceIcon();
	
	@Resource("com/helpguest/marketplace/public/images/helpguest_secure_laptop.png")
	AbstractImagePrototype secure_laptop();
	
	@Resource("com/helpguest/marketplace/public/images/zerostar.png")
	AbstractImagePrototype zeroStar();
	
	@Resource("com/helpguest/marketplace/public/images/halfstar.png")
	AbstractImagePrototype halfStar();
	
	@Resource("com/helpguest/marketplace/public/images/onestar.png")
	AbstractImagePrototype oneStar();
	
	@Resource("com/helpguest/marketplace/public/images/oneandhalfstars.png")
	AbstractImagePrototype oneandhalfStar();
	
	@Resource("com/helpguest/marketplace/public/images/twostars.png")
	AbstractImagePrototype twoStar();
	
	@Resource("com/helpguest/marketplace/public/images/twoandhalfstars.png")
	AbstractImagePrototype twoandhalfStar();
	
	@Resource("com/helpguest/marketplace/public/images/threestars.png")
	AbstractImagePrototype threeStar();

	@Resource("com/helpguest/marketplace/public/images/threeandhalfstars.png")
	AbstractImagePrototype threeandhalfStar();
	
	@Resource("com/helpguest/marketplace/public/images/fourstars.png")
	AbstractImagePrototype fourStar();
	
	@Resource("com/helpguest/marketplace/public/images/fourandhalfstars.png")
	AbstractImagePrototype fourandhalfStar();
	
	
	@Resource("com/helpguest/marketplace/public/images/fivestars.png")
	AbstractImagePrototype fiveStar();
	
	
	@Resource("com/helpguest/marketplace/public/images/businessicon.png")
	AbstractImagePrototype business();
	
	
	@Resource("com/helpguest/marketplace/public/images/designicon.png")
	AbstractImagePrototype design();
	
	
	@Resource("com/helpguest/marketplace/public/images/developmenticon.png")
	AbstractImagePrototype development(); 
	
	
	@Resource("com/helpguest/marketplace/public/images/personalicon.png")
	AbstractImagePrototype personal(); 
	
	
	@Resource("com/helpguest/marketplace/public/images/systemsicon.png")
	AbstractImagePrototype system(); 
	
	
	@Resource("com/helpguest/marketplace/public/images/GreenDotConnection.png")
	AbstractImagePrototype connection(); 
	
	
	@Resource("com/helpguest/marketplace/public/images/orangedotoffline.png")
	AbstractImagePrototype offline(); 

	
	@Resource("com/helpguest/marketplace/public/images/ticket.png")
	AbstractImagePrototype ticket();

	
	@Resource("com/helpguest/marketplace/public/images/disabledConnection.png")
	AbstractImagePrototype disabledConnection();

	
	@Resource("com/helpguest/marketplace/public/images/enabledConnection.png")
	AbstractImagePrototype enabledConnection();
	
	@Resource("com/helpguest/marketplace/public/images/badgeBlue.png")
	AbstractImagePrototype badgeBlue();
	
	@Resource("com/helpguest/marketplace/public/images/badgeDkGrey.png")
	AbstractImagePrototype badgeDkGrey();
	
	@Resource("com/helpguest/marketplace/public/images/badgeDkGreyWOrange.png")
	AbstractImagePrototype badgeDkGreyWOrange();
	
	@Resource("com/helpguest/marketplace/public/images/badgeLtGrey.png")
	AbstractImagePrototype badgeLtGrey();
	
	@Resource("com/helpguest/marketplace/public/images/badgeOrange.png")
	AbstractImagePrototype badgeOrange();

	
	
	
}
