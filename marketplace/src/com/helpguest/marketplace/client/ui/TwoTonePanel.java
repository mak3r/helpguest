/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 25, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author mabrams
 *
 */
public class TwoTonePanel extends SimplePanel {
	
	private RoundedPanel top;
	private RoundedPanel bottom;
	private HorizontalPanel paddedHeading = new HorizontalPanel();
	private Widget heading = new Label();
	private Widget body = new Label();
	private final String defaultTopStyle = "hg-TwoToneTop";
	private String currentTopStyle = defaultTopStyle;
	
	public TwoTonePanel() {
		paddedHeading.add(heading);
		paddedHeading.setSpacing(10);
		top = new RoundedPanel(paddedHeading, RoundedPanel.TOP, 3);
		bottom = new RoundedPanel(body, RoundedPanel.BOTTOM, 3);
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(top);
		vp.add(bottom);
		
		setWidget(vp);
	}

	public TwoTonePanel(final String brickWidth) {
		this();
		setWidth(brickWidth);
	}
	
	public TwoTonePanel(Widget head, Widget bd) {
		this();
		setHeading(head, currentTopStyle);
		setBody(bd);
	}

	public TwoTonePanel(Widget head, Widget body, final String brickWidth, final String topStyle) {
		this();
		currentTopStyle = topStyle;
		setHeading(head, currentTopStyle);
		setBody(body);
		setWidth(brickWidth);
	}
	
	public TwoTonePanel(Widget head, Widget body, final String brickWidth) {
		this(head, body);
		setWidth(brickWidth);
	}
	
	public void setHeading(final Widget head) {
		//use the default top style
		setHeading(head, currentTopStyle);
	}
	
	public void setHeading(final Widget head, final String topStyle) {
		int index = paddedHeading.getWidgetIndex(heading);
		paddedHeading.remove(index);
		this.heading = head;
		paddedHeading.insert(heading, index);
		
		paddedHeading.setStyleName(topStyle);
		top.setCornerStyleName(topStyle);
		top.setStyleName("hg-TwoToneBackground");

		top.setWidget(paddedHeading);
	}
	
	public void setBody(final Widget bd) {
		this.body = bd;
		
		body.setStyleName("hg-TwoToneBottom");
		bottom.setCornerStyleName("hg-TwoToneBottom");		
		bottom.setStyleName("hg-TwoToneBackground");

		bottom.setWidget(body);
	}
	
	public void update(final Widget head, final Widget body) {
		setHeading(head, currentTopStyle);
		setBody(body);
	}
	
	public void setWidth(final String width) {
		top.getWidget().setWidth(width);
		bottom.getWidget().setWidth(width);
	}
}
