/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 27, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public class ExpertDetailHeader extends HorizontalPanel {

	public ExpertDetailHeader(final ExpertDTO expert) {
		setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
		Label headerLabel = new Label("You have selected the Expert");
		add(headerLabel);
		headerLabel.setStyleName("expert_detail_header");
		add(new VerticalSpacer(20));
		Label expertHandle = new Label(expert.getHandle());
		expertHandle.setStyleName("expert_detail_header_handle");
		add(expertHandle);
		add(new VerticalSpacer(20));

		VisualCuesImageBundle visualCues = 
		    (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);	  
		Image onlineImage = 
			(expert.isLive()?
			visualCues.onlineIcon().createImage() :
			visualCues.offlineIcon().createImage());
		add(onlineImage);
	}
}
