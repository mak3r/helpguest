/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 23, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.client.util.Jaws;

/**
 * @author mabrams
 *
 */
public class GuestConnectInstructions extends SimplePanel {

	private SessionDTO session;
	private static final LoginType CLIENT = new LoginType.Client();
	
	/**
	 * FIXME we either need to check for online status here,
	 * or we need to attach that info to the session.
	 * 
	 * @param session
	 */
	public GuestConnectInstructions(final SessionDTO session) {
		super();
		this.session = session;
		init();
		setStyleName("hg-GuestConnectInstructions");
	}
	
	private void init() {
		String sessionId = session.getSessionId();
		String handle = session.getExpertAccount().getHandle();
		
		VisualCuesImageBundle visualCues = 
		    (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);	  
		Image onlineImage = visualCues.guestInterfaceIcon().createImage();
		
		String detectedMacintosh = 
			"<br/><font style=\"font-size: smaller; font-family: Arial, Helvetica, sans-serif; font-weight: normal;\">" +
			"If you get an error running the HelpGuest&reg; " +
			"application, please " + 
			Jaws.getUpgradeLaunchContent(sessionId, "upgrade", CLIENT) + 
			" to the current version of java on your mac." +
			"</font>";
		String required =
			"Prerequisite:&nbsp;" +
			Jaws.getUpgradeLaunchContent(sessionId, "Install", CLIENT) + 
			" the latest version of Java.<br/>Then:&nbsp;";
		StringBuffer stepOne = new StringBuffer(" the Client Application.");
		if (Jaws.installOnWinIERequired()) {
			stepOne.insert(
					0,
					Jaws.getWinIELaunchContent(sessionId, "Start", CLIENT));
		} else if (Jaws.isWindowsNotIE() && Jaws.installRequired()) {
			stepOne.insert(
					0, 
					Jaws.getLaunchContent(sessionId, "Start", CLIENT));
			stepOne.insert(0, required);
		} else if (Jaws.isMacintosh()) {
			//The correct version of jaws is installed and ready to go
			stepOne.insert(
					0,
					Jaws.getSimpleLaunchContent(sessionId, "Start", CLIENT));
			//or maybe not
			stepOne.append(detectedMacintosh);			
		} else {
			stepOne.insert(
					0, 
					Jaws.getLaunchContent(sessionId, "Start", CLIENT));
		}
		String stepTwo = 
			"<em>After</em> you are connected with " + handle + ", " +
			"click the see or do button on the Guest application " +
			"and allow Expert " + handle + " to work on your problem.<br/>" +
			onlineImage.toString();
		String stepThree =
			"Please answer the simple survey questions at the end of your " +
			"session so that we may improve the HelpGuest&reg; marketplace.";
		
		
		HTML instructions = new HTML(
				"<ol>" +				
				"<li>" +
				stepOne +
				"</li><br/>" +				
				"<li>" + 
				stepTwo +
				"</li><br/>" +				
				"<li>" + 
				stepThree +
				"</li>" + 
				"</ol>");
		setWidget(instructions);
	}
	
}
