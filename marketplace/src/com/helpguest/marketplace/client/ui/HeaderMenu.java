package com.helpguest.marketplace.client.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.SourcesClickEvents;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;

public class HeaderMenu extends SimplePanel implements AuthenticationListener, SourcesClickEvents {
	
	public static int GENERAL_MENU = 0;
	public static int EXPERT_MENU = 1;

	public final static String HOME = "home";
	public final static String SIGN_IN = "sign_in";
	public final static String SIGN_UP = "sign_up";
	public final static String BLOG = "blog";
	public final static String ABOUT = "about";
	public final static String CONTACT = "contact";
	public final static String SEARCH = "search";
	public final static String RATES = "rates";
	public final static String LOG_OUT = "log_out";
	public final static String HOW_TO = "how_to";
	public final static String MY_ACCOUNT = "my_account";
	public final static String UPDATE_EXPERTISE = "update_expertise";
	public final static String COMPLETED_SESSIONS = "completed_sessions";
	public final static String TICKET = "enter_ticket";

	/*
	final HTML blogURL =
		new HTML("<a href=\"http://blog.helpguest.com\">Blog</a>");
	*/
	final Hyperlink blog = new Hyperlink("Blog", BLOG);
	final Hyperlink home = new Hyperlink("Home", HOME);
	final Hyperlink signIn = new Hyperlink("Expert sign-in", SIGN_IN);
	final Hyperlink signUp = new Hyperlink("Expert sign-up", SIGN_UP);
	final Hyperlink about = new Hyperlink("About", ABOUT);
	final Hyperlink search = new Hyperlink("Search", SEARCH);
	final Hyperlink contact = new Hyperlink("Contact", CONTACT);
	final Hyperlink rateCalc = new Hyperlink("Rates", RATES);
	final Hyperlink logOut = new Hyperlink("Log out", LOG_OUT);
	final Hyperlink howTo = new Hyperlink("How-to", HOW_TO);
	final Hyperlink myAccount = new Hyperlink("My account", MY_ACCOUNT);
	final Hyperlink updateExpertise = new Hyperlink("Update expertise", UPDATE_EXPERTISE);
	final Hyperlink completedSessions = new Hyperlink("Completed sessions", COMPLETED_SESSIONS);
	final Hyperlink ticket = new Hyperlink("Quick Ticket", TICKET);

	private static Map hyperlinkMap = new HashMap();	
	private final SimplePanel generalMenu = new SimplePanel();
	private final SimplePanel expertMenu = new SimplePanel();
	
	private final List clickList = new ArrayList();
	
	SimplePanel dropPanel = null;

	/**
	 * On menu selections, if the associated item is a 
	 * widget, set it on the toUpdate panel.
	 * @param toUpdate SimplePanel
	 */
	public HeaderMenu(final SimplePanel dropPanel) {
		super();
		this.dropPanel = dropPanel;
		addAvailableLinks();
		init();
	}

	public void onChange(Authenticator source) {
		if (source.isLoggedIn()) {
			initExpertMenu();
			setWidget(expertMenu);
		} else {
			initGeneralMenu();
			setWidget(generalMenu);
		}
	}
	
	private void init() {
		//Currently logout is the only click-only link
		logOut.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				for (int i = 0; i < clickList.size(); i++) {
					((ClickListener) clickList.get(i)).onClick(sender);
				}
			}
			
		});		
		initGeneralMenu();
		//Default is to set the general menu
		setWidget(generalMenu);
	}
	
	
	private void initGeneralMenu() {
		
		generalMenu.setWidth("100%");
		//TODO change the color of the menu when logged in/ not logged in.
		generalMenu.setStyleName("hg-HeaderMenu");
		
		HorizontalPanel gMenu = new HorizontalPanel();
		gMenu.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
		gMenu.setSpacing(10);
		
		gMenu.add(home);
		gMenu.add(new VerticalSpacer(30));
		gMenu.add(signUp);
		gMenu.add(new VerticalSpacer(30));
		//gMenu.add(blogURL);
		gMenu.add(blog);
		gMenu.add(new VerticalSpacer(30));
		gMenu.add(ticket);
		gMenu.add(new VerticalSpacer(30));
		gMenu.add(signIn);			
		gMenu.add(new VerticalSpacer(30));

		generalMenu.setWidget(gMenu);
	}

	private void initExpertMenu() {
		
		expertMenu.setWidth("100%");
		expertMenu.setStyleName("hgx-HeaderMenu");
		
		HorizontalPanel eMenu = new HorizontalPanel();
		eMenu.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
		eMenu.setSpacing(10);
		
		eMenu.add(home);
		eMenu.add(new VerticalSpacer(30));
		eMenu.add(myAccount);
		eMenu.add(new VerticalSpacer(30));
		eMenu.add(updateExpertise);
		eMenu.add(new VerticalSpacer(30));
		eMenu.add(completedSessions);
		eMenu.add(new VerticalSpacer(30));
		//eMenu.add(blogURL);
		eMenu.add(blog);
		eMenu.add(new VerticalSpacer(30));
		eMenu.add(logOut);
		eMenu.add(new VerticalSpacer(30));

		expertMenu.setWidget(eMenu);
	}
	
	private void addAvailableLinks() {
		hyperlinkMap.put(BLOG, blog);
		hyperlinkMap.put(SIGN_UP, signUp);
		hyperlinkMap.put(HOME, home);
	}
	
	public static Hyperlink getHyperlink(final String linkName) {
		return (Hyperlink) hyperlinkMap.get(linkName);
	}
	
	public void setLink(final String token, final Widget toDisplay) {
		CommandToken displayWidget = null;
		if (toDisplay instanceof CommandToken) {
			displayWidget = new CommandToken() {
			
				public String getBaseToken() {
					return ((CommandToken) toDisplay).getBaseToken();
				}
	
				public void onEntry(final Map<String, String> kvMap) {
					((CommandToken) toDisplay).onEntry(kvMap);
					dropPanel.setWidget(toDisplay);
				}
				
				public void onExit() {
					((CommandToken) toDisplay).onExit();
				}
				
			};
		} else {
			displayWidget = new CommandToken() {			
	
				public String getBaseToken() {
					return token;
				}
	
				public void onEntry(final Map<String, String> kvMap) {
					dropPanel.setWidget(toDisplay);
				}
				
				public void onExit() {
					//NOOP
				}
				
			};
		}
		HistoryCommander.getInstance().register(displayWidget);
	}

	public void addClickListener(ClickListener listener) {
		clickList.add(listener);
	}

	public void removeClickListener(ClickListener listener) {
		clickList.remove(listener);
	}
	
}
