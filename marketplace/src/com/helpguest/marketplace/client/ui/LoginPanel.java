/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Aug 18, 2007
 */
package com.helpguest.marketplace.client.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.HistoryListener;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.protocol.AuthenticationOutcome;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.gwt.protocol.VerifyEmailOutcome;
import com.helpguest.gwt.protocol.AuthenticationOutcome.FailUserPass;
import com.helpguest.gwt.protocol.AuthenticationOutcome.Success;
import com.helpguest.marketplace.client.AuthenticationService;
import com.helpguest.marketplace.client.AuthenticationServiceAsync;
import com.helpguest.marketplace.client.SessionService;
import com.helpguest.marketplace.client.SessionServiceAsync;
import com.helpguest.marketplace.client.bakery.CookieManager;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.observer.SourcesAuthenticationOutcomes;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;
import com.helpguest.marketplace.client.widgets.PaddedPanel;
import com.helpguest.marketplace.client.widgets.SiteSeal;

/**
 * @author mabrams
 *
 */
public class LoginPanel extends SimplePanel 
		implements CommandToken, SourcesAuthenticationOutcomes, 
		Authenticator, HistoryListener, ClickListener {

	public static final LoginType EXPERT_LOGIN_TYPE =
		new LoginType.Expert();
	public static final LoginType ADMIN_LOGIN_TYPE =
		new LoginType.Admin();
	
	public static final AuthenticationOutcome SUCCESS_OUTCOME =
		new AuthenticationOutcome.Success();
	
	
	private Label error = new Label();
	private TextBox email = new TextBox();
	private PasswordTextBox password = new PasswordTextBox();
	private Button login = new Button("Login");	
	private static final String LOGIN_PANEL_TOKEN = HeaderMenu.SIGN_IN;
	
	private final List observerList = new ArrayList();
	
	//Create the authentication service proxy
	final AuthenticationServiceAsync authenticationService =
		(AuthenticationServiceAsync) GWT.create(AuthenticationService.class);
	//Create the session service proxy
	final SessionServiceAsync sessionService =
		(SessionServiceAsync) GWT.create(SessionService.class);
	private AuthenticationOutcome outcome = null;
	private VerifyEmailOutcome emailOutcome = null;
	private String verifyEmailAddress;
	private String verifyEmailUid;
	private final static String VERIFY_EMAIL_ADDRESS = "email";
	private final static String VERIFY_UID = "uid";
	
	private LoginType loginType;
	private String onLoginGoTo;
	private static TwoTonePanel ttExpired;
	
	//prahalad added begin 6/3/08
	private static MarketPlaceConstants lpConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String lpFqdn = lpConstants.fullyQualifiedDomainName();
    private static String lpWebUriPrefix = lpConstants.webUriPrefix();
	//prahalad added begin 6/3/08
	
	public LoginPanel(final LoginType loginType, final String gotoPageHref) {
		super();
		this.loginType = loginType;
		this.onLoginGoTo = gotoPageHref;
		
		//Initialize the AuthenticationService
		ServiceDefTarget endpoint = (ServiceDefTarget) this.authenticationService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "AuthenticationService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		//Initialize the SessionService
		ServiceDefTarget sessionEndpoint = (ServiceDefTarget) this.sessionService;
		String sessionModuleRelativeURL = GWT.getModuleBaseURL() + "SessionService";
		sessionEndpoint.setServiceEntryPoint(sessionModuleRelativeURL);
		
		error.setStyleName("hg-error");		
		init();
		History.addHistoryListener(this);
	}
	
	private void init() {
		
		login.addClickListener(this);
		
		password.addKeyboardListener(new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				// TODO Auto-generated method stub
				
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (keyCode == KeyboardListener.KEY_ENTER) {
					onClick(sender);
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				// TODO Auto-generated method stub
				
			}
			
		});
				
		
		HorizontalPanel emailInput = new HorizontalPanel();
		Label emailLabel = new Label("Email");
		emailInput.add(emailLabel);
		emailInput.setCellHorizontalAlignment(emailLabel, HorizontalPanel.ALIGN_RIGHT);
		emailInput.add(new VerticalSpacer(10));
		email.setWidth("200px");
		emailInput.add(email);
		emailInput.setCellHorizontalAlignment(email, HorizontalPanel.ALIGN_LEFT);
		
		HorizontalPanel passInput = new HorizontalPanel();
		Label passLabel = new Label("Password");
		passInput.add(passLabel);
		passInput.setCellHorizontalAlignment(passLabel, HorizontalPanel.ALIGN_RIGHT);
		passInput.add(new VerticalSpacer(10));
		password.setWidth("200px");
		passInput.add(password);
		passInput.setCellHorizontalAlignment(password, HorizontalPanel.ALIGN_LEFT);

		HTML altLogin = new HTML(
				"<label style=\"color: #000000; font-weight: bold;\">" +
				"<strong>" +
				"To access your fully functional Expert account " +
				"please login at the " +
				"<a href=\"" + lpWebUriPrefix + "://" + lpFqdn + "/hgservlets/Login?login_type=Expert\" style=\"color: #DD2200;\">" +
				"original site" +
				"</a>" +
				"</strong>" +
				"</label>"
		);
		altLogin.setStyleName("hg-altLogin");
		
		VerticalPanel vp = new VerticalPanel();
		vp.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		vp.add(emailInput);
		vp.add(new HorizontalSpacer(20));
		vp.add(passInput);
		vp.add(new HorizontalSpacer(20));
		vp.add(login);
		vp.add(new HorizontalSpacer(20));
		vp.add(altLogin);

		HorizontalPanel secureBlock = new HorizontalPanel();
		secureBlock.add(new SiteSeal());
		secureBlock.add(vp);
		secureBlock.setCellHorizontalAlignment(vp, HorizontalPanel.ALIGN_RIGHT);
		secureBlock.setWidth("400px");

		VerticalPanel loginWithError = new VerticalPanel();
		loginWithError.add(error);
		loginWithError.setCellHorizontalAlignment(error, VerticalPanel.ALIGN_CENTER);
		loginWithError.add(secureBlock);		
		loginWithError.setSpacing(20);
		loginWithError.setWidth("460px");		
		
		TwoTonePanel ttLogin = new TwoTonePanel("460px");
		ttLogin.setHeading(new Label("Login " + loginType.toString()));
		ttLogin.setBody(loginWithError);
		
		PaddedPanel paddedLogin = new PaddedPanel(ttLogin, 20, 0);
		
		setWidget(paddedLogin);
	}
	
	public Widget getExpiredWidget() {
		if (ttExpired == null) {
			Label expiredHeader = new Label("Page expired.");
			Label expiredInstruction = new Label(
					"You must login again to access the content on this page.");
			
			expiredInstruction.setStyleName("hg-instructional-verbage");
			
			PaddedPanel paddedInstruction = 
				new PaddedPanel(expiredInstruction, 20);
			paddedInstruction.setCellHorizontalAlignment(
					expiredInstruction, PaddedPanel.ALIGN_CENTER);
			paddedInstruction.setWidth("300px");
	
			ttExpired = new TwoTonePanel(
					expiredHeader, paddedInstruction, "300px");
		}
		return ttExpired;
	}
	
	AsyncCallback verifyEmailCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("LoginPanel verifyEmailCallback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("LoginPanel verifyEmailCallback [throwable]: " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			emailOutcome = (VerifyEmailOutcome) result;
			error.setText(emailOutcome.toString());
		}
		
	};
	
	AsyncCallback callback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("LoginPanel callback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("LoginPanel callback [throwable]: " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			outcome = (AuthenticationOutcome) result;
			if (outcome.equals(new Success())) {
				//preserve the email and password
				CookieManager.bake(
						CookieManager.EMAIL_COOKIE, email.getText(), false);
				CookieManager.bake(
						CookieManager.PASSWORD_COOKIE, password.getText(), true);
				
				//Create a demoSession (will only be created if expert.canDemo)
				sessionService.createDemoSession(
						email.getText(), createDemoSessionCallback);
				
				onExit();
				
				//go to the href page
				HistoryCommander.getInstance().onHistoryChanged(onLoginGoTo);				
			} else {
				error.setText(outcome.getMessage());
			}
			//notify observers
			notifyObservers();
		}
		
	};
	
	AsyncCallback createDemoSessionCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("LoginPanel createDemoSessionCallback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("LoginPanel createDemoSessionCallback [throwable]: " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			//no-op
		}
		
	};
	
	public void onClick(Widget sender) {
			
		if ( (email.getText() != null && !email.getText().equals(""))
				&& (password.getText() != null && !password.getText().equals("")) ) {
			authenticationService.authenticate(
					email.getText(),
					password.getText(),
					loginType,
					true,
					callback);
			/*
			callback.onSuccess(new AuthenticationOutcome.Success());
			*/
		} else {
			//TODO throw up an error indicating that they must have a username and password
			error.setText(new FailUserPass().getMessage());
		}
		//Now clear the password box
		password.setText(null);
	}

	/**
	 * Removes cookies, clears the outcome
	 *  and then notifies observers.
	 *
	 */
	public void logout() {
		outcome = null;
		CookieManager.eat(CookieManager.EMAIL_COOKIE);
		CookieManager.eat(CookieManager.PASSWORD_COOKIE);
		notifyObservers();
	}
	
	private final void notifyObservers() {
		for (int i = 0; i<observerList.size(); i++) {
			((AuthenticationListener) observerList.get(i)).onChange(this);
		}
	}

	public String getBaseToken() {
		return LOGIN_PANEL_TOKEN;
	}

	public void onEntry(Map<String, String> kvMap) {
		if (kvMap.size() > 0) {
			//TODO logout anyone that is currently logged in.
			verifyEmailAddress = (String) kvMap.get(VERIFY_EMAIL_ADDRESS);
			verifyEmailUid = (String) kvMap.get(VERIFY_UID);

			authenticationService.verifyEmail(
					verifyEmailAddress, 
					verifyEmailUid, 
					verifyEmailCallback);
		} else if (isLoggedIn()) {
			onExit();
			
			//go to the href page
			HistoryCommander.getInstance().onHistoryChanged(onLoginGoTo);
		}
	}

	public void onExit() {
		//clear any login details
		password.setText(null);
		email.setText(null);
		error.setText(null);
	}

	public void addAuthenticationListener(AuthenticationListener authListener) {
		observerList.add(authListener);
	}

	public void removeAuthenticationListener(AuthenticationListener authListener) {
		observerList.remove(authListener);
	}

	public AuthenticationOutcome getAuthenticationOutcome() {
		return outcome;
	}
	
	public boolean isLoggedIn() {
		//FIXME WE MUST SET AT LOGIN AND TEST HERE A UNIQUE READABLE PASSWORD KEY
		if (CookieManager.ponder(CookieManager.EMAIL_COOKIE) == null) {
			return false;
		}
		return true;
	}

	public String getAuthenticatedParty() {
		return CookieManager.ponder(CookieManager.EMAIL_COOKIE);
	}

	public LoginType getLoginType() {
		return loginType;
	}

	public String toString() {
		return "[" +
				"outcome: " + outcome + ", " +
				"loginType: " + loginType + ", " +
				"error: " + error + ", " +
				"isLoggedIn(): " + (isLoggedIn()?"true":"false") + "]";
	}

	public void onHistoryChanged(String historyToken) {
		this.notifyObservers();
	}
}
