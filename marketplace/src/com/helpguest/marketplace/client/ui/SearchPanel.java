/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 21, 2007
 */
package com.helpguest.marketplace.client.ui;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.SearchService;
import com.helpguest.marketplace.client.SearchServiceAsync;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.widgets.FindingResultsMessagePanel;

/**
 * @author mabrams
 *
 */
public class SearchPanel extends VerticalPanel implements ResultContainer {
    public static final int one = 1;
    public static final int two = 2;
    private static String query;
	//	TODO replace this with a SuggestBox
	private final TextBox searchBox = new TextBox();
	//Create the search service proxy
	final SearchServiceAsync searchService =
		(SearchServiceAsync) GWT.create(SearchService.class);
	private ResultContainer resultContainer;
	//private SearchResultsPanel searchResultsPanel;
	private SearchResultsPanel searchResultsPanel;
	public final static String SEARCH_COOKIE = "searchCookie";

	private static SearchPanel searchPanel;
	
	private SearchPanel(String label,String searchbox,final ResultContainer locationOfResults,int type) {
		super();
		this.resultContainer = locationOfResults;
		
		//Specify the url where chatterbox is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.searchService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "SearchService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		init(label,searchbox,type); 
	}
	
	public static SearchPanel newInstance(String label,String searchbox,final ResultContainer locationOfResults,int type) {
		searchPanel = new SearchPanel(label,searchbox,locationOfResults,type);
		return searchPanel;
	}
	
	public static SearchPanel getInstance() {
		return searchPanel;
	}
	
	public void init(String label,String searchbox,int type) {
        
		//Label searchLabel = new Label("QUICKSEARCH");
		Label searchLabel = new Label(label);
		searchLabel.setStyleName("search_label");
		//this.searchBox.setText("Enter your problem here");
		this.searchBox.setText(searchbox);
		searchBox.setStyleName("quicksearch_input");
		KeyboardListener keyListener = new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				//NOOP	
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (KeyboardListener.KEY_ENTER == keyCode) {
					//Enter key pressed handle the search
					search();
				}
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				// TODO this would be a good place to use the SuggestBox oracle
			}	
		};
		this.searchBox.addKeyboardListener(keyListener);
		
		
		final PushButton findButton = new PushButton();
		findButton.setStyleName("quicksearch_button");
		ClickListener clickListener = new ClickListener() {
			public void onClick(Widget sender) {
				//Execute the search
				search();
			}			
		};
		findButton.addClickListener(clickListener);
		
		
		//Ying add these for type two use
		final PushButton findResult = new PushButton();
		findResult.setStyleName("findresult_button");
		HorizontalPanel searchButton = new HorizontalPanel();
		
		HorizontalPanel searchInput = new HorizontalPanel();
		searchInput.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
		searchInput.add(this.searchBox);
		if (type == one){
			findButton.setText("Find Experts");
			searchInput.add(new VerticalSpacer(5));
			searchInput.add(findButton);
		}else if(type == two){
			findButton.setText("NEW SEARCH");
			findResult.setText("SEARCH WITHIN RESULTS");
			searchButton.add(findResult);
			searchButton.add(findButton);
		}
		
		//Now make our widget
		setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
		add(searchLabel);
		add(searchInput);
		add(searchButton);
		setStyleName("search_panel");
		
	}
	
	private void search() {
		//first display a 'finding experts message...'
		resultContainer.setWidget(new FindingResultsMessagePanel());
		//Add the latest search to a cookie
		Cookies.setCookie(SEARCH_COOKIE, searchBox.getText());
		searchService.findExperts(searchBox.getText(), searchCallback);
	}

	/**
	 * @return the query
	 */
	public String getQuery() {
		return this.searchBox.getText();
	}

	/**
	 * Set query allows clients to set the initial query value
	 * that is displayed.
	 * @param query the query to set
	 */
	public void setQuery(final String query) {
		this.searchBox.setText(query);
	}
	
	public void quotedSearch(final String query){
		searchBox.setText(query);
		search();
	}
	
	AsyncCallback searchCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("SearchPanel searchCallback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("SearchPanel searchCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			//our list is now ready
			List expertList = (List) result;
			
			searchResultsPanel =
				new SearchResultsPanel(expertList, resultContainer);
			
			CommandToken searchResult =
				SearchResultHandler.createCommandToken(
						getQuery(), searchResultsPanel, searchPanel);
			//Inform the HistoryCommander of this command token
			HistoryCommander.getInstance().register(searchResult);

			//Indicate that a new history event has taken place
			HistoryCommander.newItem(searchResult.getBaseToken());
		}

	};

	public static String getSearchCookie() {
		return Cookies.getCookie(SearchPanel.SEARCH_COOKIE);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setWidget(Widget result) {
		resultContainer.setWidget(result);
	}
	
	/**
	 * Set the text of search box.
	 * */
	public void setSearchText(){
		searchBox.setText(query);
	}

	public void reset() {
		// TODO Auto-generated method stub
		
	}
}
