/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client.ui;

import java.util.Map;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.IPNService;
import com.helpguest.marketplace.client.IPNServiceAsync;
import com.helpguest.marketplace.client.brick.ContentBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.IPNDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.widgets.PaddedPanel;

/**
 * @author mabrams
 *
 */
public class PaymentCompletePanel extends SimplePanel implements CommandToken {

	final static String SESSION_ID = "sessionId";
	final static String HISTORY_PREFIX = "PaymentComplete";
	private String sessionId;
	private IPNDTO ipnDTO;
	private ExpertDTO expertDTO;
	//Create the ipn service proxy
	final IPNServiceAsync ipnService =
		(IPNServiceAsync) GWT.create(IPNService.class);
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	private ResultContainer resultContainer;
	
	private static Label paypalRequired = new Label(
		"Thanks for using HelpGuest with PayPal.  " +
		"Your payment completed successfully.  " +
		"A receipt for your purchase has been emailed to you.  " +
		"Visit http://www.paypal.com to review your account.");
	
	private static HTML note = new HTML(
			"<sup>&dagger;</sup>" +
			"If your Expert has gone offline while " +
			"you went through the payment process, " +
			"please <a href=\"mailto:listen@helpguest.com\">" +
			"let us know</a>.  " +
			"You are entitled to a full refund or " +
			"you can re-use your TICKET# " +
			"when the expert comes back online.");
	static {
		paypalRequired.setStyleName("hg-PaymentCompletePaypalRequired");
		note.setStyleName("hg-PaymentCompleteNote");
	}
	
	
	public PaymentCompletePanel(final ResultContainer resultContainer) {
		this.resultContainer = resultContainer;
		setStyleName("hg-PaymentComplete");
		//Specify the url where the ipn service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.ipnService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "IPNService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);

		//Specify the url where the expert service is running
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String moduleRelativeURLExpertService = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(moduleRelativeURLExpertService);

	}
	
	private void init() {
		String handle = ipnDTO.getSession().getExpertAccount().getHandle();
		VerticalPanel vp = new VerticalPanel();
		vp.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);

		vp.add(paypalRequired);
		
		Label ticket = new Label(ipnDTO.getSession().getSessionId()); 
		ticket.setStyleName("hg-PaymentCompleteTicket");
		Label ticketLabel = new Label("TICKET#:&nbsp;&nbsp;");
		ContentBrick ticketNumber = new ContentBrick(
				ticketLabel, ticket);
				
		
		String onOff = (expertDTO.isLive()?
				"<font class=\"hg-online\">ONLINE</font>":
				"offline");
		HTML onOffLine = new HTML(
				handle +" is currently " + onOff + 
				"<sup>&dagger;</sup>" );
		
		Label stepsLabel = new Label(
				"The final steps to getting assistance from " + 
				handle + " are simple.");
		
		SimplePanel instructions =
			new GuestConnectInstructions(ipnDTO.getSession());

		//Add the receipt details and instructions to a distinct panel
		VerticalPanel receiptInfo = new VerticalPanel();
		receiptInfo.setSpacing(20);
		receiptInfo.add(onOffLine);
		receiptInfo.add(stepsLabel);

		//Add the ticket number, receipt details to the receipt
		VerticalPanel receiptPanel = new VerticalPanel();
		receiptPanel.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		receiptPanel.setStyleName("hg-PaymentCompleteReceiptPanel");
		receiptPanel.setBorderWidth(1);
		receiptPanel.add(new PaddedPanel(ticketNumber, 10));
		receiptPanel.add(receiptInfo);
		receiptPanel.add(new PaddedPanel(instructions, 10));
		
		
		PaddedPanel receiptDock = new PaddedPanel(1, 20);
		receiptDock.setStyleName("hg-PaymentCompleteReceiptDock");
		receiptDock.add(receiptPanel);
		
		RoundedPanel receipt = new RoundedPanel(receiptDock, RoundedPanel.ALL, 4);
		receipt.setStyleName("hg-PaymentCompleteReceipt");
		receipt.setCornerStyleName("hg-PaymentCompleteReceipt-corners");
		
		HorizontalPanel outerPadding = new HorizontalPanel();
		outerPadding.add(new VerticalSpacer(75));
		outerPadding.add(receipt);
		outerPadding.add(new VerticalSpacer(75));
		
		vp.add(outerPadding);
		vp.add(new HorizontalSpacer(20));
		vp.add(note);
		vp.setSpacing(20);
		setWidget(vp);
	}
	
	AsyncCallback ipnCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			showQuickTicket();
			/*
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
				Window.alert("PaymentCompletePanel ipnCallback: " +  sBuf.toString());
			} catch (Throwable th) {
				Window.alert("PaymentCompletePanel ipnCallback: [throwable]" + th.getMessage());
			}
			*/
		}

		public void onSuccess(Object result) {
			if (result == null) {
				showQuickTicket();
			}
			ipnDTO = (IPNDTO) result;
			expertService.getExpert(ipnDTO.getSession().getExpertAccount().getUid(), expertCallback);
		}
		
	};
	
	private void showQuickTicket() {
		HistoryCommander.newItem(
				HistoryCommander.buildToken(
						QuickTicketPanel.HISTORY_PREFIX, QuickTicketPanel.FAILED_TICKET_NUMBER, sessionId));
		/*
		HistoryCommander.newItem(QuickTicketPanel.HISTORY_PREFIX + 
				HistoryCommander.BASE_SEPARATOR + QuickTicketPanel.FAILED_TICKET_NUMBER +
				HistoryCommander.VALUE_SEPARATOR + sessionId);
		*/
	}
	
	AsyncCallback expertCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("PaymentCompletePanel expertCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("PaymentCompletePanel expertCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expertDTO = (ExpertDTO) result;
			init();
		}		
	};

	public String getBaseToken() {
		return HISTORY_PREFIX;
	}

	public void onEntry(final Map<String, String> kvMap) {
		if (kvMap.size() > 0) {
			sessionId = (String) kvMap.get(SESSION_ID);
			//TODO show locating your expert link message
			ipnService.getCompletionDetails(sessionId, ipnCallback);
			resultContainer.setWidget(this);
		}
	}

	public void onExit() {
		//NOOP
	}
	
	
}
