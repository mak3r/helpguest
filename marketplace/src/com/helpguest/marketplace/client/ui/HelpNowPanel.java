/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 27, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FormHandler;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormSubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormSubmitEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.SessionService;
import com.helpguest.marketplace.client.SessionServiceAsync;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;
import com.helpguest.marketplace.client.widgets.ChangingTextButton;
import com.helpguest.marketplace.client.widgets.SiteSeal;

/**
 * @author mabrams
 * 
 */
public class HelpNowPanel extends RoundedPanel {

	private ExpertDTO expert;
	private final static String ACTION = "https://www.paypal.com/cgi-bin/webscr";
	private final static String BUSINESS = "sales@helpguest.com";
	private String formAction = ACTION;
	private String formHiddenBusiness = BUSINESS;
	
	//prahalad added begin 6/3/08
	private static MarketPlaceConstants hnpConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
	private static String hnpFqdn = hnpConstants.fullyQualifiedDomainName();
    private static String hnpWebUriPrefix = hnpConstants.webUriPrefix();
	//prahalad added begin 6/3/08
	
	public HelpNowPanel(final ExpertDTO expert, final SessionDTO session) {
		super(RoundedPanel.ALL, 5);
		this.expert = expert;
		setWidth("190px");
		setCornerStyleName("marketplace_footer_corners");
		setStyleName("checkout_block");
		
		init(session);
	}
	
	public void init(final SessionDTO session) {
				
		Hidden cmd = new Hidden("cmd", "_xclick");
		Hidden business = new Hidden("business", formHiddenBusiness);
        Hidden custom = new Hidden("custom", expert.getExpertUid());
        Hidden invoice = new Hidden("invoice", session.getSessionId());
        Hidden item_name = new Hidden("item_name", expert.getHandle());
        Hidden item_number = new Hidden("item_number", session.getItemNumber());
        Hidden currency_code = new Hidden("currency_code", "USD");
        Hidden amount = new Hidden("amount", expert.getPaymentPlan().getDepositAmount());
        //prahalad changing beta 6/4/08
        //Hidden image_url = new Hidden("image_url", "http://beta.helpguest.com/images/brand-long.png");
        Hidden image_url = new Hidden("image_url", hnpWebUriPrefix + "://" + hnpFqdn + "/images/brand-long.png");
        Hidden no_shipping = new Hidden("no_shipping", "1");
        Hidden no_note = new Hidden("no_note", "1");
        //Hidden returnTo = new Hidden("return", "https://secure.helpguest.com/marketplace/Guest.html#PaymentComplete" +
        //prahalad changing static url 6/3/08
        /**
         * FIXME: HistoryCommander has changed DO NOT ACCESS separators directly
         */
        Hidden returnTo = new Hidden("return", hnpWebUriPrefix + "://" + hnpFqdn + "/marketplace/Guest.html#PaymentComplete" +
        "");
        /* FIXME
        		HistoryCommander.BASE_SEPARATOR + "sessionId" + 
        		HistoryCommander.VALUE_SEPARATOR + session.getSessionId());
        */
        //Hidden cancel_return = new Hidden("cancel_return", "https://secure.helpguest.com/");
      //prahalad changing static url 6/3/08
        Hidden cancel_return = new Hidden("cancel_return", hnpWebUriPrefix + "://" + hnpFqdn + "/");

        HTML oneMoreThing = new HTML(
    			"<div class=\"checkout_detail\">"
    			+ "<div id=\"onemorething\"> Following your payment:"
    			+ "<p> You will be directed to a page allowing you to <em>enable secure remote access</em> between you and the Expert.</p><p>With <em>remote access enabled</em>, this Expert will be able to solve your problem faster if you give them permission to see your problem, move your mouse, and type at your keyboard while helping you out.</p>"
    			+ "</div>"
    			+ "</div>");
        
        //TODO the action will be different for a 'canDemo' expert
        final FormPanel helpNowForm = new FormPanel("_parent");
        helpNowForm.setAction(formAction);
        helpNowForm.setMethod(FormPanel.METHOD_POST);
        helpNowForm.setEncoding(FormPanel.ENCODING_URLENCODED);
        helpNowForm.addFormHandler(new FormHandler() {

			public void onSubmit(FormSubmitEvent event) {
				
			}

			public void onSubmitComplete(FormSubmitCompleteEvent event) {
//				Window.alert("HelpNowPanel onSubmitComplete(): " + event.getResults());				
			}
        	
        });
        
        String agreeStatement =
        		"I have read and agree with the " +
        		"<a href=\"GuestEULA.html\" " +
        		"target=\"_blank\">terms and conditions</a>.";
        final ChangingTextButton agreeButton =
        	new ChangingTextButton("GET HELP NOW", "Click the agreement above to get help now");
        agreeButton.setEnabled(false);
        ClickListener submitListener = new ClickListener() {

			public void onClick(Widget sender) {
				helpNowForm.submit();
			}
        	
        };
        agreeButton.addClickListener(submitListener);
        
        final CheckBox enableAgreeButton = new CheckBox(agreeStatement, true);
        ClickListener enableAgreeButtonListener = new ClickListener() {

			public void onClick(Widget sender) {
				// TODO Auto-generated method stub
				if (enableAgreeButton.isChecked()) {
					agreeButton.setEnabled(true);
				} else {
					agreeButton.setEnabled(false);
				}
			}
        	
        };
        enableAgreeButton.addClickListener(enableAgreeButtonListener);
        
        VerticalPanel formContents = new VerticalPanel();
        formContents.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
        formContents.setStyleName("checkout_form");
        formContents.add(cmd);
		formContents.add(business);
        formContents.add(custom);
        formContents.add(invoice);
        formContents.add(item_name);
        formContents.add(item_number);
        formContents.add(currency_code);
        formContents.add(amount);
        formContents.add(image_url);
        formContents.add(no_shipping);
        formContents.add(no_note);
        formContents.add(returnTo);
        formContents.add(cancel_return);
        formContents.add(enableAgreeButton);
        formContents.add(new HorizontalSpacer(5));
        formContents.add(agreeButton);

        //Add the form content
        helpNowForm.setWidget(formContents);
        
        VerticalPanel vp = new VerticalPanel();
        vp.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
        vp.add(new HorizontalSpacer(10));
        vp.add(helpNowForm);
        vp.add(new HorizontalSpacer(30));
        vp.add(oneMoreThing);
        vp.add(new HorizontalSpacer(10));
        vp.add(new SiteSeal());
		vp.setStyleName("helpguest_HelpNowPanel");

		setWidget(vp);
	}

}
