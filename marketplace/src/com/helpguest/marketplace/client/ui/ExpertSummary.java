/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 24, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.brick.ContentBrick;
import com.helpguest.marketplace.client.brick.DetailBrick;
import com.helpguest.marketplace.client.brick.HeadingBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.history.HistoryCommander;

/**
 * This is a summary view of the Expert.
 * 
 * @author mabrams
 *
 */
public class ExpertSummary extends SimplePanel {

	//This will be changed to the experts handle
	private ExpertPreview expertPreview;
	private NumberFormat nf = NumberFormat.getFormat("###.00");

	public ExpertSummary(final ExpertDTO dto, final ResultContainer resultContainer) {
		/** The summary heading */
		HeadingBrick headingBrick = new HeadingBrick();
		headingBrick.setLeft(new Label(dto.getHandle()));
		VisualCuesImageBundle visualCues = 
		    (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);	  
		Image onlineImage = 
			(dto.isLive()?
			visualCues.online12Icon().createImage() :
			visualCues.offline12Icon().createImage());
		headingBrick.setRight(onlineImage);
		headingBrick.setWidth("100%");
				
		/** The avatar brick */
		Image avatarImage = new Image(dto.getAvatarURL());
		avatarImage.setWidth("70px");
		avatarImage.setHeight("78px");
		avatarImage.setStyleName("avatar");
		RoundedImage avatar =
			new RoundedImage(avatarImage, "avatar_rounded_image", 5);		

		/** The bio brick */
		//TODO only add '...' if the bio.len is > substr.len
		DetailBrick bioBrick =
			new DetailBrick(
				"Bio:",
				dto.getBio().substring(0, 65) + " ...");

		/** The rate brick */
		DetailBrick rateBrick =
			new DetailBrick(
				"Rate per minute:",
				"$ " + dto.getPaymentPlan().getRatePerMinute()
					+ " / minute");

		/** The rating brick */
		DetailBrick ratingBrick =
			new DetailBrick(
				"Rating:", 
				nf.format(dto.getStats().getSatisfactionRating()) + "%");
				
		VerticalPanel bodyPanel = new VerticalPanel();
		bodyPanel.add(ratingBrick);
		bodyPanel.add(rateBrick);
		bodyPanel.add(bioBrick);
		bodyPanel.setSpacing(10);
		
		/** add the details to the body brick */
		ContentBrick bodyBrick = new ContentBrick();
		bodyBrick.setLeft(avatar);
		bodyBrick.setRight(bodyPanel);
		bodyBrick.setSpacing(10);

		TwoTonePanel ttPanel = new TwoTonePanel(headingBrick, bodyBrick);
		ttPanel.setWidth("345px");
		FocusPanel focusPanel = new FocusPanel();
		focusPanel.add(ttPanel);
		
		
		ClickListener expertSummaryClicked =
			new ClickListener() {

				public void onClick(Widget sender) {
					//Move to Expert Preview
					expertPreview = new ExpertPreview(dto, resultContainer);
					HistoryCommander.getInstance().register(expertPreview);
					HistoryCommander.newItem(expertPreview.getBaseToken());			
				}
			
		};
		focusPanel.addClickListener(expertSummaryClicked);
		setWidget(focusPanel);
		setStyleName("helpguest_ExpertSummary");
	}

}
