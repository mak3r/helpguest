/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author mabrams
 * 
 */
public class RateCalculation extends SimplePanel {

	private static final String rateInfo = new String (
			"			<div id=\"content\">"
			+ "			<div style=\"text-align:left;\">"
			+ "		<small>Last Updated on January 5th, 2007</small>"

			+"			<h1>HelpGuest Fee Calculation</h1>"

			+"			  <h2>Overview:</h2>"
			+"			  HelpGuest provides tools and resources which allow Experts to charge a fee for services to be consumed by Guests.  The fees charged by Experts through the HelpGuest Marketplace may be bound by either minimum fee or minimum time requirements set by HelpGuest.  HelpGuest will take a percentage of the  fee charged as described in this document.  Since not all services are of equal value, Experts may adjust how much they charge Guests in order to maintain a reasonable rate for their services. At any time, HelpGuest may change the RPM, GMF, or other factors used to calculate Guest charges and transaction fees. We will make our best effort to notify registered Experts of changes to fee calculation at the email address  provided during registration. See below for an explanation of acronyms."
			+"			  <h2>Expert variables:</h2>"
			+"			  <ul>"
			+"			  <li><em><a name=\"rpm\" id=\"rpm\">Rate per minute</a></em> (RPM): This is the minimum amount that Experts charge Guests, per minute. This is a variable amount with a minimum of ninety-nine cents $0.99</li>"
			+"			  <li><em>Preferred session length</em> (PSL): This is the maximum amount of time that Experts require to solve a problem. </li>"
			+"			  </ul>"

			+"			  <h2>Factors considered in calculating charges:</h2>"
			+"			  <ul>"
			+"			    <li><em><a name=\"gmf\" id=\"gmf\">Minimum fee</a></em> (GMF): This is the minimum amount that Guests are required to pay. GMF = RPM x MSL </li>"
			+"			    <li><em>Minimum session length</em> (MSL): This is the minimum amount of time that Guests will be charged for.  MSL = GMF / RPM</li>"
			+"			  <li><em>Guest deposit</em> (GDP): Guests will be required to deposit an amount equal to the preferred session length times the rate per minute. GDP = PSL x RPM. </li>"
			+"			  </ul>"

			+"			  <h2>How Guest charges are calculated:</h2>"
			+"			  <em><a name=\"fch\" id=\"fch\">Final charge</a></em> (FCH): Prior to the session provided by the Expert, a deposit from the Guest will be required.  The deposit amount is the maximum amount a Guest will pay and is equivalent to GDP.  There are two ways to calculate FCH. "
			+"			  <ol>"
			+"			    <li>If the session is ended before or at the time of the MSL, then the Guest will pay an amount equal to the GMF.</li>  "
			+"			    <li>If the session ends after the MSL, then Guests will be charged the GMF plus the RPM for each additional minute for a total amount not to exceed the GDP.</li>"
			+"			  </ol>"

			+"			  <h2>Expert payments:</h2>"
			+"			  HelpGuest retains 17.9% of the FCH from each session in which a charge is made.  At any time following the close of a session, an Expert may elect to transfer the funds from one or more sessions into the paypal account of their choice.  HelpGuest will make every effort to submit payments  to the Experts PayPal account within 72 hours of the Experts request for payment.    "
			+"			  </div>"

			+"			</div>"
	);


	private final static HTMLPanel rateCalculation = new HTMLPanel(rateInfo);

	public RateCalculation() {
		super();
		rateCalculation.setStyleName("rate_calculation");
		VerticalPanel vp = new VerticalPanel();
		vp.add(rateCalculation);
		vp.setSpacing(20);
		vp.add(new HorizontalSpacer(10));
		setWidget(vp);
	}

}
