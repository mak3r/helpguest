/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 7, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.brick.DetailBrick;
import com.helpguest.marketplace.client.dto.EvaluationDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.widgets.AvatarImage;

/**
 * @author mabrams
 *
 */
public class CriticalSummary extends HorizontalPanel {

	private ExpertDTO expert;
	private EvaluationDTO evaluation;
	//Create the search service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);

	
	public CriticalSummary(final ExpertDTO dto) {
		super();
		this.expert = dto;
		
		//Specify the url where the expert service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.expertService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		expertService.getEvaluation(expert, callback);
		 
	}
	
	public void init() {		
		DetailBrick expertRate = 
			new DetailBrick("Expert's rate:",
				"$" + expert.getPaymentPlan().getRatePerMinute() + "/minute");
		DetailBrick maxFee = 
			new DetailBrick("Maximum fee:",
				"$" + expert.getPaymentPlan().getDepositAmount());

		DetailBrick starRating;
		if (evaluation.getSatisfactionCount() < 1) {
			starRating = new DetailBrick("Satisfaction rating:", 
					"not enough data"); 
		} else {
			float satisfaction = evaluation.getSatisfaction();
			HorizontalPanel numStars = new HorizontalPanel();
			numStars.add(new Label("(" + evaluation.getFormattedOverallSatisfaction() + "%)"));
			numStars.add(new VerticalSpacer(10));
			numStars.add(new StarRating(satisfaction));
			starRating = new DetailBrick("Satisfaction rating:", numStars);
		}
		
		VerticalPanel vp = new VerticalPanel();
		vp.add(expertRate);
		vp.add(maxFee);
		vp.add(starRating);
		vp.setSpacing(10);
		vp.setWidth("370px");
		
		AvatarImage avatarImage = new AvatarImage(expert);
		RoundedImage avatar =
			new RoundedImage(avatarImage, "avatar_rounded_image", 5);		
		
		add(avatar);
		setCellHorizontalAlignment(avatar, HorizontalPanel.ALIGN_LEFT);
		add(vp);
		setCellHorizontalAlignment(vp, HorizontalPanel.ALIGN_RIGHT);
	}
	
	AsyncCallback callback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("CriticalSummary callback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("CriticalSummary callback [throwable]: " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			evaluation = (EvaluationDTO) result;
			init();
		}
		
	};
}
