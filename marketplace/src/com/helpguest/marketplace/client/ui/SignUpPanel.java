/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Oct 14, 2007
 */
package com.helpguest.marketplace.client.ui;

import java.util.ArrayList;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.gwt.protocol.SignUpOutcome;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.ExpertServiceAsync;
import com.helpguest.marketplace.client.brick.InputBrick;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.widgets.ExpertTagsWidget;
import com.helpguest.marketplace.client.widgets.GetUserEmailToResetPswd;
import com.helpguest.marketplace.client.widgets.GetNewPswdToResetPswdPanel;
import com.helpguest.marketplace.client.widgets.PaddedPanel;
import com.helpguest.marketplace.client.widgets.SiteSeal;
import com.helpguest.marketplace.client.widgets.VisualTimer;

/**
 * @author mabrams
 *
 */
public class SignUpPanel extends SimplePanel implements CommandToken {

	private VerticalPanel errors = new VerticalPanel();
	private InputBrick emailAddress = 
		new InputBrick("Email Address: ", "15em");
	private InputBrick password = 
		new InputBrick("Password: ", "10em", InputBrick.PASSWORD_TYPE);
	private InputBrick confirmPassword =
		new InputBrick("Confirm Password: ", "10em", InputBrick.PASSWORD_TYPE);
	private InputBrick handle = 
		new InputBrick("Handle: ", "15em");
	private InputBrick primaryExpertise = 
		new InputBrick("Primary Expertise: ", "15em");
	private InputBrick zipCode =
		new InputBrick("Zip Code:", "5em");
	private Button submit = new Button("Sign Up Now");
	private static final String agreeStatement = 
        "<font style=\"font-size: smaller;\">I have read and agree with the&nbsp;" +
        "<a href=\"ExpertEULA.html\" target=\"_blank\">" +
        "terms of use" +
        "</a>" +
        "&nbsp;and the&nbsp;" +
        "<a href=\"RateCalculation.html\" target=\"_blank\">" +
        "payment plan" +
        "</a>" +
        "&nbsp;and I understand the&nbsp;" +
        "<a href=\"PrivacyPolicy.html\" target=\"_blank\">" +
        "privacy policy" +
        "</a>." + 
        "</font>";
	private CheckBox terms = new CheckBox(agreeStatement, true);

	
	private Label required = new Label("All fields are required");
	private static final Label zipError =
		new Label("Your zip code must be exactly 5 digits.");
	//36 matches the field width in the db
	private static final int handleLength = 36;
	private static final Label handleErrorLength =
		new Label("Your handle cannot be more than " + handleLength + " characters.");
	private static final Label handleErrorContent =
		new Label("Your handle may only contain letters, numbers and underscores.");
	private static final Label passwordErrorLength =
		new Label("Password must be between 6 and 50 characters.");
	private static final Label passwordErrorContent =
		new Label("Password may contain letters, numbers, and (*$&#!_-^+).");
	private static final Label confirmPasswordError =
		new Label("Password and confirm password must match.");
	private static final Label emailError =
		new Label("Email address is invalid.");
	private static final Label primaryExpertiseError =
		new Label("Primary expertise may contain letters, numbers and spaces.");
	private static final int primaryExpertiseLength = 50;
	private static final Label primaryExpertiseErrorLength =
		new Label("Primary expertise cannot be more than " + primaryExpertiseLength + " characters.");
	
	private static final Label tagsListIsEmpty =
		new Label("At least one tag must be entered for any category and offering.");
	
	private static final String ERROR_STYLE = "hg-error";
	private VisualTimer creatingAccount = new VisualTimer("Creating your account ", 10);
	
	//prahalad adding 07/24/08
	ExpertTagsWidget expertTagsWidget = new ExpertTagsWidget(null);
	
	//Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	public SignUpPanel() {
		super();

		//Specify the url where the expert service is running
		ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
		String moduleRelativeURLExpertService = GWT.getModuleBaseURL() + "ExpertService";
		expertEndpoint.setServiceEntryPoint(moduleRelativeURLExpertService);
		
		init();
	}
	
	private void init() {
		
		required.setStyleName(ERROR_STYLE);
		zipError.setStyleName(ERROR_STYLE);
		handleErrorContent.setStyleName(ERROR_STYLE);
		handleErrorLength.setStyleName(ERROR_STYLE);
		passwordErrorContent.setStyleName(ERROR_STYLE);
		passwordErrorLength.setStyleName(ERROR_STYLE);
		confirmPasswordError.setStyleName(ERROR_STYLE);
		emailError.setStyleName(ERROR_STYLE);
		primaryExpertiseError.setStyleName(ERROR_STYLE);
		primaryExpertiseErrorLength.setStyleName(ERROR_STYLE);
		tagsListIsEmpty.setStyleName(ERROR_STYLE);
		
		String leftWidth = "150px";
		emailAddress.setLeftWidth(leftWidth);
		password.setLeftWidth(leftWidth);
		confirmPassword.setLeftWidth(leftWidth);
		handle.setLeftWidth(leftWidth);
		primaryExpertise.setLeftWidth(leftWidth);
		zipCode.setLeftWidth(leftWidth);
		
		submit.setEnabled(false);
		terms.setChecked(false);
		
		terms.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				submit.setEnabled(terms.isChecked());
			}
			
		});
		
		submit.addClickListener(new ClickListener(){

			public void onClick(Widget sender) {
				if (contentIsValidated()) {
					creatingAccount.start();
					errors.add(creatingAccount);
					/*expertService.createAccount(
							emailAddress.getInputText(), 
							password.getInputText(), 
							handle.getInputText(), 
							primaryExpertise.getInputText(), 
							zipCode.getInputText(), 
							expertCallback);*/
					expertService.createAccount(
							emailAddress.getInputText(), 
							password.getInputText(), 
							handle.getInputText(), 
							expertTagsWidget.getCatOffgTagList(), 
							zipCode.getInputText(),
							"NO REFERRAL SOURCE LIST PROVIDED TO USER",
							expertCallback);
				}				
			}
			
		});
		
		HorizontalPanel paddedSubmit = new HorizontalPanel();
		paddedSubmit.add(submit);
		paddedSubmit.add(new VerticalSpacer(30));
		
		VerticalPanel agreeAndSubmit = new VerticalPanel();
		agreeAndSubmit.add(terms);
		agreeAndSubmit.setCellHorizontalAlignment(terms, VerticalPanel.ALIGN_LEFT);
		agreeAndSubmit.add(paddedSubmit);
		agreeAndSubmit.setCellHorizontalAlignment(paddedSubmit, VerticalPanel.ALIGN_RIGHT);
		agreeAndSubmit.setSpacing(20);
		
		HorizontalPanel secureAndButtons = new HorizontalPanel();
		secureAndButtons.add(new SiteSeal());
		secureAndButtons.add(agreeAndSubmit);
		secureAndButtons.setCellHorizontalAlignment(secureAndButtons, HorizontalPanel.ALIGN_RIGHT);
		
		
		VerticalPanel vp = new VerticalPanel();
		vp.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		vp.add(errors);
		vp.add(new HorizontalSpacer(20));
		vp.add(primaryExpertise);
		vp.add(emailAddress);
		vp.add(password);
		vp.add(confirmPassword);
		vp.add(handle);
		vp.add(zipCode);
		vp.add(expertTagsWidget);
		
		vp.add(new HorizontalSpacer(20));
		vp.add(secureAndButtons);
		vp.setCellHorizontalAlignment(secureAndButtons, VerticalPanel.ALIGN_CENTER);		
		vp.setWidth("450px");
		
		//padded total width is equal to the ttp width
		PaddedPanel padded = new PaddedPanel(vp, 20, 20);
		
		TwoTonePanel ttp = new TwoTonePanel("490px");
		ttp.setHeading(new Label("Sign Up"));
		ttp.setBody(padded);
		
		setWidget(ttp);
	}
	
	private boolean contentIsValidated() {
		errors.clear();
		return 
			generalCheck() 
			& expertiseCheck()
			& emailCheck()
			& passwordCheck()
			& handleCheck() 
			& zipCheck()
			& tagListEmptyCheck();
	}
	
	private boolean generalCheck() {
		if ("".equals(zipCode.getInputText().trim())
				|| "".equals(handle.getInputText().trim())
				|| "".equals(password.getInputText().trim())
				|| "".equals(confirmPassword.getInputText().trim())
				|| "".equals(emailAddress.getInputText().trim())
				|| "".equals(primaryExpertise.getInputText().trim())) {
			errors.add(required);
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @return true if the zip code validates.  False
	 * if it does not validate
	 */
	private boolean zipCheck() {
		String zip = zipCode.getInputText();
		if (!zip.matches("[0-9]{5}")) {
			errors.add(zipError);
			return false;
		}
		return true;
	}
	
	private boolean handleCheck() {
		String handleValue = handle.getInputText();
		boolean ok = true;
		if (handleValue.length() > handleLength) {
			errors.add(handleErrorLength);
			ok = false;
		}
		if (!handleValue.matches("[\\w]*")) {
			errors.add(handleErrorContent);
			ok = false;
		}
		return ok;
	}
	
	private boolean passwordCheck() {
		String passwordValue = password.getInputText();
		String confirmPasswordValue = confirmPassword.getInputText();
		boolean ok = true;
		if (!passwordValue.matches("[\\w\\*\\&\\#\\!\\^\\+\\$-]*")) {
			errors.add(passwordErrorContent);
			ok = false;
		}
		if (passwordValue.length() < 6 || 
				passwordValue.length() > 50) {
			errors.add(passwordErrorLength);
			ok = false;
		}
		if (passwordValue != null && !passwordValue.equals(confirmPasswordValue)) {
			errors.add(confirmPasswordError);
			ok = false;
		}
		return ok;
	}
	
	private boolean emailCheck() {
		String emailValue = emailAddress.getInputText();
		if (!emailValue.matches("[\\w\\.\\+%-]+@[\\w\\.-]+\\.[\\w]{2,4}")) {
			errors.add(emailError);
			return false;
		}
		return true;
	}
	
	private boolean expertiseCheck() {
		String expertiseValue = primaryExpertise.getInputText();
		boolean ok = true;
		if (!expertiseValue.matches("([\\w]+[\\s]*)+?")) {
			errors.add(primaryExpertiseError);
			ok = false;
		} 
		if (expertiseValue.length() > primaryExpertiseLength) {
			errors.add(primaryExpertiseErrorLength);
			ok = false;
		}
		
		return ok;
	}
	
	//prahalad adding 07/24/08
	private boolean tagListEmptyCheck(){
		ArrayList tagsList = expertTagsWidget.getCatOffgTagList();
		int emptyCount = 0;
		boolean ok = true;
		for (int listIt = 0;listIt<tagsList.size();listIt++)
		{
			String catOffTag = tagsList.get(listIt).toString();
			String tag = catOffTag.substring(catOffTag.indexOf(':', catOffTag.indexOf(':') + 1) + 1).trim();
			if (!tag.equals("")){
				emptyCount++;
			}
		}
		
		if (emptyCount==0){
			ok = false;
			errors.add(tagsListIsEmpty);	
		}
		
		return ok;
		
	}
	
	private void clearForm() {
		emailAddress.setInputText(null);
		password.setInputText(null);
		confirmPassword.setInputText(null);
		handle.setInputText(null);
		primaryExpertise.setInputText(null);
		expertTagsWidget.setTagTextArea(null);
		zipCode.setInputText(null);
		terms.setChecked(false);
		submit.setEnabled(false);
		errors.clear();
	}
	
	AsyncCallback expertCallback = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			creatingAccount.stop();
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("SignUpPanel expertCallback: " +  sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("SignUpPanel expertCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			SignUpOutcome outcome = (SignUpOutcome) result;
			if (outcome.equals(new SignUpOutcome.Success())) {
				setWidget(new SignUpSuccessfulPanel(
						emailAddress.getInputText(), 
						outcome));
			} else {
				//TODO send the new user somewhere when the outcome is success
				Label finalOutcome = new Label(outcome.toString());
				finalOutcome.setStyleName(ERROR_STYLE);
				creatingAccount.stop();
				errors.clear();
				errors.add(finalOutcome);
			}
		}		
	};

	public String getBaseToken() {
		return HeaderMenu.SIGN_UP;
	}

	public void onEntry(Map<String, String> kvMap) {
		//noop
	}

	public void onExit() {
		clearForm();
		creatingAccount.stop();
	}
}
