/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 24, 2007
 */
package com.helpguest.marketplace.client.ui;

import java.util.List;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.widgets.PaddedPanel;


/**
 * @author mabrams
 *
 */
public class SearchResultsPanel extends SimplePanel {

	ResultContainer resultContainer;
	final TopThirty topThirty = new TopThirty();
	
	public SearchResultsPanel(final List expertList, final ResultContainer resultContainer) {
		super();
		this.resultContainer = resultContainer;
		init(expertList);
	}
	
	private void init(final List expertList) {
		if (expertList == null || expertList.size() < 1) {
			//convert the hyperlink to use the hg-link class
			Hyperlink signUp = HeaderMenu.getHyperlink(HeaderMenu.SIGN_UP);
			HTML noResults = new HTML(
					"Your search terms did not return any results.<br/>" +
					"Please try again with different search terms or<br/>" +
					"select from our top 30 most popular expertise above.<br/>" +
					"<br/>" +
					"<font style=\"font-weight: bold;\">" +
					"Also, consider becoming the first HelpGuest<sup><small>&reg;</small></sup>" +
					"&nbsp;Expert for the terms of your search.&nbsp;&nbsp;Help others with " +
					"what you already know!" +
					"</font>&nbsp;&nbsp;" +
					"<a class=\"hg-link\" href=\"#" + signUp.getTargetHistoryToken() + "\">" +
					signUp.getText() + "</a>"
				);
			noResults.setStyleName("hg-generalInfo");
			PaddedPanel paddedNoResults = new PaddedPanel(noResults, 20);
			//Set the width to the same width as the top thirty panel
			paddedNoResults.setWidth("500px");

			TwoTonePanel ttp = new TwoTonePanel("500px");
			ttp.setHeading(new Label("No Experts found"));
			ttp.setBody(paddedNoResults);
			
			//With no results, just show the top 30
			VerticalPanel vp = new VerticalPanel();
			vp.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
			vp.add(topThirty);
			vp.add(ttp);
			vp.setSpacing(30);

			setWidget(vp);
		} else {
			
			VerticalPanel leftSide = new VerticalPanel();
			VerticalPanel rightSide = new VerticalPanel();
			
			for (int i=0; i<expertList.size(); i++) {
				ExpertDTO dto = (ExpertDTO) expertList.get(i);
				if (i%2!=0) {
					leftSide.add(new HorizontalSpacer(10));
					leftSide.add(new ExpertSummary(dto, resultContainer));
				}
				if (i%2==0) {
					rightSide.add(new HorizontalSpacer(10));
					rightSide.add(new ExpertSummary(dto, resultContainer));
				}
			}
			
			HorizontalPanel srPanel = new HorizontalPanel();
			srPanel.setSpacing(20);
			srPanel.add(leftSide);
			srPanel.add(rightSide);
			
			setWidget(srPanel);
		}
	}
	
	
}
