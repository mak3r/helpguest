/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 22, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.observer.AuthenticationListener;
import com.helpguest.marketplace.client.observer.Authenticator;
import com.helpguest.marketplace.client.widgets.BannerPanel;

/**
 * @author mabrams
 *
 */
public class HelpGuestHeader extends RoundedPanel implements AuthenticationListener {

	Widget subHeader = null;
	private Label loginIndicator = new Label("");
	
	public HelpGuestHeader() {
		this(null);
	}
	
	public HelpGuestHeader(Widget subHeader) {
		super(RoundedPanel.TOP, 5);
		this.subHeader = subHeader;
		init();
	}

	private void init() {
		
		VisualCuesImageBundle visualCues =
			(VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);
		Image logoImage = visualCues.brandIcon().createImage();
		logoImage.setStyleName("logo");
	
		Label brandName = new Label("HELPGUEST.com");
		brandName.setStyleName("brand_name");
		HTML tagLine = new HTML(
				"<label>Bringing the power of expertise " +
				"<em>directly</em> to your <em>PC</em></label>");
		tagLine.setStyleName("tag_line");
		
		Button seeHow = new Button("See how it works (flash)");
		seeHow.setStyleName("hg-HelpGuestHeader-seeHow");

		VerticalPanel headerDetail = new VerticalPanel();
		headerDetail.add(brandName);
		headerDetail.add(tagLine);
		if (subHeader != null) {
			headerDetail.add(new HorizontalSpacer(20));
			headerDetail.add(subHeader);
		}
		headerDetail.add(new HorizontalSpacer(20));
		HorizontalPanel addOns = new HorizontalPanel();
		addOns.add(loginIndicator);
		addOns.setCellHorizontalAlignment(loginIndicator, HorizontalPanel.ALIGN_LEFT);
		addOns.setCellVerticalAlignment(loginIndicator, HorizontalPanel.ALIGN_BOTTOM);
		addOns.add(seeHow);
		addOns.setCellHorizontalAlignment(seeHow, HorizontalPanel.ALIGN_RIGHT);
		addOns.setCellVerticalAlignment(seeHow, HorizontalPanel.ALIGN_BOTTOM);
		addOns.setWidth("100%");
		headerDetail.add(addOns);
		
		final HorizontalPanel headerPanel = new HorizontalPanel();
		headerPanel.add(logoImage);
		headerPanel.add(headerDetail);
		headerPanel.setSpacing(20);
		headerPanel.setStyleName("markteplace_header_body");
		headerPanel.setWidth("100%");
		
		final Timer timer = new Timer() {
			public void run() {
				setWidget(headerPanel);
			}					
		};

		seeHow.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				setWidget(new BannerPanel());
				timer.schedule(55000);				
			}
		});
		
		
		setWidget(headerPanel);
		//setWidget(new BannerPanel());
		setCornerStyleName("marketplace_header_corners");
		setStyleName("marketplace_header");
	}
	

	public void onChange(Authenticator source) {
		if (source.isLoggedIn()) {
			loginIndicator.setText("logged in as: " + source.getAuthenticatedParty());
		} else {
			loginIndicator.setText("");
		}
	}
}
