package com.helpguest.marketplace.client.ui;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;


public class VerticalSpacer extends VerticalPanel {

	public VerticalSpacer(final int pixelWidth) {
		super();
		setHeight("100%");
		setWidth(pixelWidth + "px");
		add(new HTML("&nbsp"));		
	}
	
	public VerticalSpacer(final int pixelWidth, final String styleName) {
		this(pixelWidth);
		setStyleName(styleName);		
	}
	
}
