/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 22, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.chatterbox.client.ui.ChatterClient;
import com.helpguest.chatterbox.client.ui.RequestChat;
import com.helpguest.marketplace.client.ProtocolResponseService;
import com.helpguest.marketplace.client.ProtocolResponseServiceAsync;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.ProtocolResponseDTO;

/**
 * @author mabrams
 *
 */
public class ChatterboxPanel extends TwoTonePanel {

	private String sessionId;
	private String brickWidth;
	private ProtocolResponseDTO response;
	
	//Create the protocol response service proxy
	final ProtocolResponseServiceAsync responseService =
		(ProtocolResponseServiceAsync) GWT.create(ProtocolResponseService.class);


	public ChatterboxPanel(
			final ExpertDTO expert, 
			final String chatSessionId, 
			final String styleName, 
			final String brickWidth) {
		super();
		this.sessionId = chatSessionId;
		this.brickWidth = brickWidth;
		
		//Specify the url where the protocol response service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.responseService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "ProtocolResponseService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		responseService.getProtocolResponse(
				expert.getExpertAccount().getGuestUid(), 
				ProtocolResponseDTO.WEB_CHAT,
				protocolResponseCallback);
	}
	
	private void init() {
		  Label chatContainerLabel = new Label("Chat with me. I'm online now.");
		  chatContainerLabel.setStyleName("chat_container_label");
		  VisualCuesImageBundle visualCues = 
			    (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);	  
		  Image onlineImage =
			  visualCues.onlineIcon().createImage();
		  VerticalPanel spacer = new VerticalPanel();
		  spacer.setWidth("10px");

		  HorizontalPanel chatContainerHeader = new HorizontalPanel();
		  chatContainerHeader.setSpacing(10);
		  chatContainerHeader.add(spacer);
		  chatContainerHeader.add(chatContainerLabel);
		  chatContainerHeader.add(onlineImage);
		  chatContainerHeader.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
		  chatContainerHeader.setStyleName("chat_container_header");
		  chatContainerHeader.setWidth("100%");  
		  
		  final RequestChat requestor = new RequestChat(sessionId, response.getPort());
		  requestor.setVisible(true);
		  final ChatterClient chatter = new ChatterClient(requestor);		
		  chatter.setVisible(false);
		  requestor.addClickListener(new ClickListener() {
			 public void onClick(Widget widget) {
				 //There is only one widget, the button
				 //Validate the expert can chat otherwise error
				 //We don't know the client name until after the
				 // requestor has completed.
				 requestor.setVisible(false);
				 chatter.setVisible(true);
			 }
		  });

		  VerticalPanel chatBody = new VerticalPanel();	  
		  chatBody.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		  chatBody.setSpacing(10);
		  chatBody.setWidth("100%");
		  chatBody.add(requestor);
		  chatBody.add(chatter);
		  
		  update(chatContainerHeader, chatBody);
		  setWidth(brickWidth);
	}
	
	
	AsyncCallback protocolResponseCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("ExpertPreview protocolResponseCallback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("ExpertPreview protocolResponseCallback [throwable]: " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			response = (ProtocolResponseDTO) result;
			init();
		}
		
	};
}
