/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 7, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author mabrams
 *
 */
public class StarRating extends SimplePanel {

	private int ones;
	private int tenths;
	private static final int maxStars = 5;
	
	public StarRating(final float rating) {
		super();
		//20 is max stars X 4 which is the number
		// of choices for satisfaction in survey_1.
		ones = (int) Math.floor(rating/20);
		tenths = Math.round((rating/20)-ones);
		init();
	}
	
	private void init() {
		VisualCuesImageBundle visualCues = 
		    (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);	  
		
		//HorizontalPanel hp = new HorizontalPanel();
		
		int i=0;
		/*while (i<ones) {
			hp.add(visualCues.starFullIcon().createImage());
			i++;
		}
		if (i<maxStars && tenths == 1) {			
			hp.add(visualCues.starHalfIcon().createImage());
			i++;
		}
		while (i<maxStars) {
			hp.add(visualCues.starEmptyIcon().createImage());
			i++;
		}
		add(hp);*/
		SimplePanel hp = new SimplePanel();
		if(ones == 0){
			if(tenths==0)
				hp.add(visualCues.zeroStar().createImage());
			 	else
			 		hp.add(visualCues.halfStar().createImage());
		}else if(ones == 1){
			if(tenths==0)
				hp.add(visualCues.oneStar().createImage());
				else
					hp.add(visualCues.oneandhalfStar().createImage());
		}else if(ones == 2){
			if(tenths==0)
				hp.add(visualCues.twoStar().createImage());
				else
					hp.add(visualCues.twoandhalfStar().createImage());
		}else if(ones == 3){
			if(tenths==0)
				hp.add(visualCues.threeStar().createImage());
				else
					hp.add(visualCues.threeandhalfStar().createImage());
		}else if(ones == 4){
			if(tenths==0)
				hp.add(visualCues.fourStar().createImage());
				else
					hp.add(visualCues.fourandhalfStar().createImage());
		}else if(ones == 5){
				hp.add(visualCues.fiveStar().createImage());
		}
		setWidget(hp);
		setStyleName("star_rating");
		}
	
	
	
}
