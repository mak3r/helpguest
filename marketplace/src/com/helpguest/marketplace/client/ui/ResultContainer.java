/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.user.client.ui.Widget;

/**
 * Implement this interface to indicate to give a secondary
 * Widget the opportunity to place results in the place
 * that this result container deems appropriate.
 * 
 * @author mabrams
 *
 */
public interface ResultContainer {

	/**
	 * Allows clients to set a widget in this ResultContainer
	 * @param result
	 */
	public void setWidget(final Widget result);

	/**
	 * reset is called by the result widget
	 * and provides the opportunity for this ResultContainer
	 * to reset it's state.
	 * When called, it is an indication that the Result 
	 * has been completed/resolved/viewed fully, by the user.
	 */
	public void reset();
}
