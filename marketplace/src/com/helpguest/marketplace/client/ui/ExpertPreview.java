/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 25, 2007
 */
package com.helpguest.marketplace.client.ui;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.SearchService;
import com.helpguest.marketplace.client.SearchServiceAsync;
import com.helpguest.marketplace.client.SessionService;
import com.helpguest.marketplace.client.SessionServiceAsync;
import com.helpguest.marketplace.client.attic.ui.ExpertStats;
import com.helpguest.marketplace.client.brick.DetailBrick;
import com.helpguest.marketplace.client.brick.HeadingBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.widgets.FutureFeaturePanel;

/**
 * @author mabrams
 *
 */
public class ExpertPreview extends SimplePanel implements CommandToken {

	private ExpertDTO expert;
	private SessionDTO session;
	private final Map viewedExperts  = new HashMap();
	private ResultContainer resultContainer;
	public static final String EXPERT_TOKEN = "Expert";
	private static final String EXPERT_COOKIE = "EXPERT_COOKIE";
	private static final String UID = "uid";
	
	//Create the search service proxy
	final SearchServiceAsync searchService =
		(SearchServiceAsync) GWT.create(SearchService.class);
	//Create the session service proxy
	final SessionServiceAsync sessionService =
		(SessionServiceAsync) GWT.create(SessionService.class);
	
	
	public ExpertPreview(final ExpertDTO expert, final ResultContainer resultContainer) {
		super();
		this.expert = expert;
		this.resultContainer = resultContainer;
		
		//Specify the url where the expert service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.searchService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "SearchService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		//Specify the url where the session service is running
		ServiceDefTarget sessionEndpoint = (ServiceDefTarget) this.sessionService;
		String sessionModuleRelativeURL = GWT.getModuleBaseURL() + "SessionService";
		sessionEndpoint.setServiceEntryPoint(sessionModuleRelativeURL);

		if (expert != null) {
			sessionService.createSession(
					expert.getExpertAccount().getEmail(), 
					createSessionCallback);
		}
	}
	
	private void init() {
		//The expert could be null if this was created as a placeholder
		if (expert != null) {
			
			String brickWidth = "500px";
			
			/** The stats panel */
			Label statsTop = new Label("Expert Stats");
			ExpertStats statsBottom = new ExpertStats(expert.getStats());
			statsBottom.setSpacing(10);
			TwoTonePanel stats = new TwoTonePanel(statsTop, statsBottom);
			stats.setWidth(brickWidth);
			
			/** The bio panel */
			Label bioTop = new Label("About Me:");
			DetailBrick bioBottom = new DetailBrick(expert.getBio(), "");
			bioBottom.setSpacing(10);
			TwoTonePanel bio = new TwoTonePanel(bioTop, bioBottom);
			bio.setWidth(brickWidth);
			
			HeadingBrick summaryTop = new HeadingBrick(
					new Label("Quick summary of " + expert.getHandle()),
					new Label(""));
			CriticalSummary summaryBottom = new CriticalSummary(expert);
			summaryBottom.setSpacing(10);
			TwoTonePanel summary = new TwoTonePanel(summaryTop, summaryBottom);
			summary.setWidth(brickWidth);

			VerticalPanel info = new VerticalPanel();
			info.add(summary);
			info.add(new HorizontalSpacer(20));
			String sessionId = null;
			if (session!=null && expert.isLive()) {
				sessionId = session.getSessionId();
				info.add(new ChatterboxPanel(expert, sessionId, "marketplace_body", brickWidth));
			} else {
				//Add the chatterbox alternative
				info.add(new FutureFeaturePanel(
						"Coming soon.", 
						"Get notified when this expert comes back online.",
						brickWidth));
			}
			info.add(new HorizontalSpacer(20));
			info.add(bio);
			info.add(new HorizontalSpacer(20));
			info.add(stats);
			
			HorizontalPanel middleDetail = new HorizontalPanel();
			middleDetail.add(info);
			middleDetail.add(new VerticalSpacer(20));
			middleDetail.add(new HelpNowPanel(expert, session));
	
			
			VerticalPanel vp = new VerticalPanel();
			vp.add(new HorizontalSpacer(20));
			vp.add(middleDetail);		
			vp.add(new HorizontalSpacer(30));
			
			//now add the whole thing to this panel
			setWidget(vp);

			viewedExperts.put(expert.getExpertUid(), this);
		}
		resultContainer.setWidget(this);
	}

	public String getBaseToken() {
		if (expert != null) {
			String[] paramList = {
				HistoryCommander.getParameterString(UID, expert.getExpertUid())
			};
			return HistoryCommander.buildToken(EXPERT_TOKEN, paramList);
		}
		return EXPERT_TOKEN;
	}

	public void onEntry(final Map<String, String> kvMap) {		
		String expertUid = null;
		if (kvMap.size() > 0) {
			expertUid = (String) kvMap.get(UID);
			if (viewedExperts.containsKey(expertUid)) {
				resultContainer.setWidget((ExpertPreview) viewedExperts.get(expertUid));			
			} else {
				searchService.getExpertByUid(expertUid, callback);
			}
		}
	}
	
	public static String getCookie() {
		return Cookies.getCookie(EXPERT_COOKIE);
	}

	AsyncCallback callback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("ExpertPreview callback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("ExpertPreview callback [throwable]: " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			expert = (ExpertDTO) result;
			new ExpertPreview(expert, resultContainer);
		}
		
	};
	
	AsyncCallback createSessionCallback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			//go ahead and init with a null session
			init();
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("HelpNowPanel createSessionCallback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("HelpNowPanel createSessionCallback: [throwable]" + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			session = (SessionDTO) result;
			init();
		}
		
	};



	public void onExit() {
		Cookies.setCookie(EXPERT_COOKIE, expert.getExpertUid());
	}

}
