/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Oct 25, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.helpguest.gwt.protocol.SignUpOutcome;
import com.helpguest.marketplace.client.widgets.PaddedPanel;

/**
 * @author mabrams
 *
 */
public class SignUpSuccessfulPanel extends SimplePanel {

	SignUpSuccessfulPanel(
			final String emailAddress,
			final SignUpOutcome outcome) {
		HTML message = new HTML(
			"<strong>Thank you for joining us as a HelpGuest&reg; Expert." +
			"</strong>" +
			"<br/><br />" +
			"A verification email will be sent to " + 
			emailAddress + "." + 
			"&nbsp;&nbsp;You must use the link provided in the verification email " +
			"in order to login to your new account." +
			"<br/><br/><br/>" +
			"<font style=\"font-size: smaller\">Note: Accounts that go unverified are periodicaly " +
			"removed from our system.</font>"
		);
		
		PaddedPanel paddedMessage = new PaddedPanel(message, 20);
		paddedMessage.setWidth("400px");
		
		TwoTonePanel ttp = new TwoTonePanel("400px");
		ttp.setHeading(new Label(outcome.getMessage()));
		ttp.setBody(paddedMessage);
		
		setWidget(ttp);
	}
}
