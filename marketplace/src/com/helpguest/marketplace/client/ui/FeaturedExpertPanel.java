/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.SearchService;
import com.helpguest.marketplace.client.SearchServiceAsync;
import com.helpguest.marketplace.client.brick.DetailBrick;
import com.helpguest.marketplace.client.brick.HeadingBrick;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.ExpertStatsDTO;
import com.helpguest.marketplace.client.dto.PaymentPlanDTO;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.util.MarketPlaceConstants;

/**
 * @author mabrams
 *
 */
public class FeaturedExpertPanel extends SimplePanel {

	//This will be changed to the experts handle
	private ExpertPreview expertPreview;
	private ExpertDTO expert;
	private ResultContainer resultContainer;

	//Create the search service proxy
	final SearchServiceAsync searchService =
		(SearchServiceAsync) GWT.create(SearchService.class);

	public FeaturedExpertPanel(final ResultContainer resultContainer) {
		super();
		this.resultContainer = resultContainer;
		
		//Specify the url where the expert service is running
		ServiceDefTarget endpoint = (ServiceDefTarget) this.searchService;
		String moduleRelativeURL = GWT.getModuleBaseURL() + "SearchService";
		endpoint.setServiceEntryPoint(moduleRelativeURL);
		
		//For now, the featured expert has to be manually changed
		// Later on, we'll have add a search service method
		// to back it and featured experts will be changed via
		// the datastore.
		//TODO: getExpertByHandle is currently unimplemented - this is
		// the preferred method.
		//searchService.getExpertByHandle("mabrams", callback);
		searchService.getExpertByUid("M11992EC30BDA", callback);
	}
	
	private void init() {
		/** The summary heading */
		HeadingBrick headingBrick = new HeadingBrick();
		headingBrick.setLeft(new Label("Featured Expert"));
		VisualCuesImageBundle visualCues = 
		    (VisualCuesImageBundle)GWT.create(VisualCuesImageBundle.class);	  
		Image onlineImage = 
			(expert.isLive()?
			visualCues.online12Icon().createImage() :
			visualCues.offline12Icon().createImage());
		headingBrick.setRight(onlineImage);
		headingBrick.setWidth("100%");
				
		/** The expert handle */
		Label expertLabel = new Label(expert.getHandle());
		
		/** The avatar brick */
		Image avatarImage = new Image(expert.getAvatarURL());
		avatarImage.setWidth("70px");
		avatarImage.setHeight("80px");
		avatarImage.setStyleName("avatar");
		RoundedImage avatar =
			new RoundedImage(avatarImage, "avatar_rounded_image", 5);
		HorizontalPanel avatarBrick = new HorizontalPanel();
		avatarBrick.add(avatar);
		avatarBrick.add(new VerticalSpacer(10));
		
		/** The expert bio */
		Label bioInfo = new Label("Bio: \n" + expert.getBio());
		
		/** The rate brick */
		DetailBrick rateBrick =
			new DetailBrick(
				"Rate per minute:",
				"$ " + expert.getPaymentPlan().getRatePerMinute()
					+ " / minute");

		/** The rating brick */
		DetailBrick ratingBrick =
			new DetailBrick(
				"Rating:", 
				expert.getStats().getSatisfactionRating());
				
		VerticalPanel bodyPanel = new VerticalPanel();
		bodyPanel.add(expertLabel);
		bodyPanel.add(avatarBrick);
		bodyPanel.add(bioInfo);
		bodyPanel.add(ratingBrick);
		bodyPanel.add(rateBrick);
		bodyPanel.setSpacing(10);
		
		TwoTonePanel ttPanel = new TwoTonePanel(headingBrick, bodyPanel);
		ttPanel.setWidth("240px");
		FocusPanel focusPanel = new FocusPanel();
		focusPanel.add(ttPanel);
		
		
		ClickListener expertSummaryClicked =
			new ClickListener() {

				public void onClick(Widget sender) {
					//Move to Expert Preview
					expertPreview = new ExpertPreview(expert, resultContainer);
					HistoryCommander.getInstance().register(expertPreview);
					HistoryCommander.newItem(expertPreview.getBaseToken());			
				}
			
		};
		focusPanel.addClickListener(expertSummaryClicked);
		setWidget(focusPanel);
		setStyleName("helpguest_ExpertSummary");
	}
	
	public static ExpertDTO getFeatured() {
		//prahalad added begin 6/3/08
		final MarketPlaceConstants fepConstants = (MarketPlaceConstants) GWT.create(MarketPlaceConstants.class);
		final String fepFqdn = fepConstants.fullyQualifiedDomainName();
		final String fepJawsAppDir = fepConstants.jawsAppDir();
		final String fepWebUriPrefix = fepConstants.webUriPrefix();
		//prahalad added end 6/3/08
		ExpertDTO expert = new ExpertDTO();
		PaymentPlanDTO plan = new PaymentPlanDTO();
		ExpertStatsDTO stats = new ExpertStatsDTO();
		
		plan.setRatePerMinute("1.29");
		stats.setSatisfactionRating(99.7f);
		//expert.setAvatarURL("https://secure.helpguest.com/jaws/apps/expert/avatars/78230ce47e7fea05.jpg");
		//prahalad changing static url 6/3/08
		expert.setAvatarURL(fepWebUriPrefix +  "://" +  fepFqdn + fepJawsAppDir + "/expert/avatars/78230ce47e7fea05.jpg");
		expert.setBio("Hey!  I'm a long-time programmer. (I actually have a degrees in both computer science and art!) and I now work in technology in the Albany area.  I'm a pretty good generalist, but Office is my specialty. It's a powerful tool, but because it's so feature-heavy unless you know the tricks for the individual applications most users seem to get stuck at a fairly low-level, which is a shame. \n\n -Mark ");
		expert.setExpertUid("78230ce47e7fea05");
		expert.setHandle("MySpaceWizard");
		expert.setLive(true);
		expert.setPaymentPlan(plan);
		expert.setStats(stats);
		
		return expert;
	}
	
	AsyncCallback callback = new AsyncCallback() {

		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("ExpertPreview callback: " + sBuf.toString());
			} catch (Throwable th) {
//				Window.alert("ExpertPreview callback [throwable]: " + th.getMessage());
			}
		}

		public void onSuccess(Object result) {
			if (result != null) {
				expert = (ExpertDTO) result;
			} else {
				expert = getFeatured();
			}
			init();
		}
		
	};
}
