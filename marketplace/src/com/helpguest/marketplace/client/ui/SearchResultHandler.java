/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 22, 2007
 */
package com.helpguest.marketplace.client.ui;

import java.util.HashMap;
import java.util.Map;

import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;

/**
 * @author mabrams
 *
 */
public class SearchResultHandler {

	private static Map searchResultsMap = new HashMap();
	public final static String SEARCH_TOKEN = "ExpertSearch";
	private final static String CRITERIA = "criteria";


	public static CommandToken createCommandToken(final String query,
			final SearchResultsPanel searchResultsPanel,
			final SearchPanel searchPanel) {
		//Maintain a reference to this search result list
		searchResultsMap.put(query, searchResultsPanel);
		return new SearchResult(query, searchPanel);
	}

	public static String getSearchToken(final String token) {
		String[] paramList = {
				HistoryCommander.getParameterString(CRITERIA, token)
		};
		return HistoryCommander.buildToken(SEARCH_TOKEN, paramList);
	}
	
	static class SearchResult implements CommandToken {
		
		//Create a command token to work with the HistoryCommander
		private String search;
		private SearchPanel searchPanel;

		public SearchResult(final String query, final SearchPanel searchPanel) {
			this.search = query;
			this.searchPanel = searchPanel;
		}
		
		public String getBaseToken() {
			String[] paramList = {
					HistoryCommander.getParameterString(CRITERIA, search)
			};
			return HistoryCommander.buildToken(SEARCH_TOKEN, paramList);
		}
		
		public void onEntry(final Map<String, String> kvMap) {
			String query = "";
			if (kvMap.size() > 0) {
				query = (String) kvMap.get(CRITERIA);
			}
			if (searchResultsMap.containsKey(query)) {
				//Reset with existing values.
				searchPanel.setQuery(query);
				searchPanel.setWidget(
					(SearchResultsPanel) searchResultsMap.get(query));
			} else {
				//this was a refresh action or the user entered a 
				// value in the address bar that was not previously known
				// Perform a new search
				searchPanel.quotedSearch(query);
			}
		}
		
		public void onExit() {
			//NOOP
		}
	}					
}
