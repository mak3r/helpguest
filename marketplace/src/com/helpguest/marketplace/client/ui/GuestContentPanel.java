/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 22, 2007
 */
package com.helpguest.marketplace.client.ui;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.helpguest.marketplace.client.widgets.AboutContent;

/**
 * @author mabrams
 *
 */
public class GuestContentPanel extends BodyPanel {

	private ResultContainer resultContainer;
	
	public GuestContentPanel(String pageWidth, String styleName, ResultContainer resultContainer) {
		super(pageWidth, styleName);
		this.resultContainer = resultContainer;
		init();
	}
	
	private void init() {
		VerticalPanel homePage = new VerticalPanel();
		FeaturedExpertPanel featuredExpert = new FeaturedExpertPanel(resultContainer);

		homePage.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
	    homePage.add(new HorizontalSpacer(10));
	    homePage.add(new TopThirty());
	    homePage.add(new HorizontalSpacer(10));

	    HorizontalPanel landingPanel = new HorizontalPanel();
	    AboutContent about = new AboutContent();
	    about.setWidth("450px");
	    featuredExpert.setWidth("240px");
	    landingPanel.setSpacing(20);
	    landingPanel.add(about);
	    landingPanel.add(featuredExpert);
		
	    homePage.add(landingPanel);
	    
	    setWidget(homePage);
	    setWidth("100%");
	}

}
