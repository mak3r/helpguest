package com.helpguest.marketplace.client.ui;

import com.bouwkamp.gwt.user.client.ui.RoundedPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;



public class Footer extends RoundedPanel {

	final static String FOOTER_BODY_STYLE = "marketplace_footer_body";
	final static String FOOTER_STYLE = "marketplace_footer";
	
	final HTML copyright =
		new HTML("&copy;&nbsp;2007&nbsp;HelpGuest Technologies, Inc.");
	
	public Footer() {
		super(RoundedPanel.BOTTOM, 5);
		init();
	}
	
	private void init() {

		VerticalPanel footerPanel = new VerticalPanel();
		footerPanel.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		footerPanel.setStyleName(FOOTER_BODY_STYLE);
		footerPanel.add(new FooterMenu());
		copyright.setStyleName(FOOTER_BODY_STYLE);
		footerPanel.add(copyright);
		footerPanel.setWidth("100%");
		setWidget(footerPanel);
		setWidth("100%");
		setCornerStyleName("marketplace_footer_corners");
		setStyleName(FOOTER_STYLE);
	}
}
