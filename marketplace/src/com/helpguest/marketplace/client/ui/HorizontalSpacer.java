package com.helpguest.marketplace.client.ui;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;


public class HorizontalSpacer extends HorizontalPanel {

	public HorizontalSpacer(int pixelHeight) {
		super();
		setWidth("100%");
		setHeight(pixelHeight + "px");
		add(new HTML("&nbsp"));
	}

	public HorizontalSpacer(final int pixelHeight, final String styleName) {
		this(pixelHeight);
		setStyleName(styleName);
	}
}
