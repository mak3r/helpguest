/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 22, 2007
 */
package com.helpguest.marketplace.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.helpguest.marketplace.client.bluegray.ui.AboutPanel;
import com.helpguest.marketplace.client.history.CommandToken;
import com.helpguest.marketplace.client.history.HistoryCommander;
import com.helpguest.marketplace.client.ui.BodyPanel;
import com.helpguest.marketplace.client.ui.ExpertPreview;
import com.helpguest.marketplace.client.ui.Footer;
import com.helpguest.marketplace.client.ui.GuestContentPanel;
import com.helpguest.marketplace.client.ui.HeaderMenu;
import com.helpguest.marketplace.client.ui.HelpGuestHeader;
import com.helpguest.marketplace.client.ui.HorizontalSpacer;
import com.helpguest.marketplace.client.ui.LoginPanel;
import com.helpguest.marketplace.client.ui.PaymentCompletePanel;
import com.helpguest.marketplace.client.ui.QuickTicketPanel;
import com.helpguest.marketplace.client.ui.RateCalculation;
import com.helpguest.marketplace.client.ui.ResultContainer;
import com.helpguest.marketplace.client.ui.SearchPanel;
import com.helpguest.marketplace.client.ui.SearchResultHandler;
import com.helpguest.marketplace.client.ui.SearchResultsPanel;
import com.helpguest.marketplace.client.ui.SignUpPanel;
import com.helpguest.marketplace.client.util.DebugPanel;
import com.helpguest.marketplace.client.validated.MyAccountPage;
import com.helpguest.marketplace.client.widgets.BlogPage;
import com.helpguest.marketplace.client.widgets.ComingSoonPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GuestAccess implements EntryPoint, ResultContainer {

	private final static boolean DEBUG = true;
	
	static {
		if (DEBUG) {
			RootPanel.get("debug-content").add(new DebugPanel(false));			
		}
	}
	
	final String pageWidth = "750px";
	final String bodyStyle = "marketplace_body";
	final BodyPanel body = new BodyPanel(pageWidth, bodyStyle);
	
	/**
	 * Body content
	 */
	final AboutPanel aboutPanel = new AboutPanel();
	final HorizontalPanel signInPanel = new HorizontalPanel();
	final RateCalculation rateCalc = new RateCalculation();
	final String search = "QUICKSEARCH"; 
    final String searchbox = "ENTER YOUR PROBLEM HERE";
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		final HeaderMenu headerMenu = new HeaderMenu(body);

		final SearchPanel searchPanel = SearchPanel.newInstance(search,searchbox,body,SearchPanel.one);
		//Use the search panel as our initial sub heading
		final HelpGuestHeader pageHeader = new HelpGuestHeader(searchPanel);

		//This is used on a refresh when the last HistoryCommander token was an expert preview
		final ExpertPreview expertStub = new ExpertPreview(null, body);
		final PaymentCompletePanel paymentCompleteStub =
			new PaymentCompletePanel(body);
		final SearchResultsPanel searchResultsPanel =
			new SearchResultsPanel(null, body);
		final CommandToken searchStub  =
			SearchResultHandler.createCommandToken("", searchResultsPanel, searchPanel);
		final GuestContentPanel guestContentPanel =
			new GuestContentPanel(pageWidth, bodyStyle, body);
		final LoginPanel expertLoginPanel = 
			new LoginPanel(LoginPanel.EXPERT_LOGIN_TYPE, HeaderMenu.MY_ACCOUNT);
		final MyAccountPage myAccountPage = 
			new MyAccountPage(headerMenu, expertLoginPanel);
		final QuickTicketPanel quickTicketPanel = 
			new QuickTicketPanel();
		final SignUpPanel signUpPanel = new SignUpPanel();
		
		//Setup widgets that need notfication of authentication
		expertLoginPanel.addAuthenticationListener(headerMenu);
		expertLoginPanel.addAuthenticationListener(pageHeader);
		
		/**
		 * Page Content
		 */
		VerticalPanel pagePanel = new VerticalPanel();
		pagePanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		pagePanel.setWidth(pageWidth);
		pagePanel.add(pageHeader);
		pagePanel.add(headerMenu);
		pagePanel.add(new HorizontalSpacer(10, bodyStyle));
		pagePanel.add(body);
		pagePanel.add(new HorizontalSpacer(20, bodyStyle));
		pagePanel.add(new Footer());
		RootPanel.get("page").add(pagePanel);
		RootPanel.get().setStyleName("body_style");
		

		//Initialize the widgets that can be in the history
		//These panels are attached to menu items
		headerMenu.setLink(HeaderMenu.HOME, guestContentPanel);
		headerMenu.setLink(HeaderMenu.BLOG, new BlogPage(pageWidth));
		headerMenu.setLink(HeaderMenu.SIGN_UP, new ComingSoonPanel("Sign-up page..."));
		headerMenu.setLink(HeaderMenu.ABOUT,aboutPanel);		
		headerMenu.setLink(HeaderMenu.SIGN_IN, expertLoginPanel);
		headerMenu.setLink(HeaderMenu.RATES, rateCalc);
		
		headerMenu.setLink(HeaderMenu.HOW_TO, new ComingSoonPanel("How to..."));
		headerMenu.setLink(HeaderMenu.MY_ACCOUNT, myAccountPage);
		//Logout brings us home for now
		headerMenu.setLink(HeaderMenu.LOG_OUT, guestContentPanel);
		headerMenu.setLink(HeaderMenu.TICKET, quickTicketPanel);
		headerMenu.setLink(HeaderMenu.SIGN_UP, signUpPanel);
		
		//We need to know when logout happens so we can clean up.
		headerMenu.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				//Currently, logout is the only clickable that notifys
				//TODO make this sender independent
				expertLoginPanel.logout();
			}
			
		});
		HistoryCommander.getInstance().register(expertStub);
		HistoryCommander.getInstance().register(paymentCompleteStub);
		HistoryCommander.getInstance().register(searchStub);
		
		//Initialize the app with the guestContentPanel
		String token = History.getToken();
		if (token.length()==0) {
			//This is the default location
			token = HeaderMenu.HOME;
		}
		HistoryCommander.newItem(token);
	}

	public void setWidget(Widget result) {
		body.setWidget(result);
	}

	public void reset() {
		// TODO Auto-generated method stub
		
	}

}
