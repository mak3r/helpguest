/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 15, 2007
 */
package com.helpguest.marketplace.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author mabrams
 *
 */
public interface ProtocolResponseServiceAsync {

	public void getProtocolResponse(final String guestUid, final String type, final AsyncCallback callback);

}
