/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 28, 2007
 */
package com.helpguest.marketplace.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.helpguest.marketplace.client.dto.ExpertAccountDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.ExpertStatsDTO;
import com.helpguest.marketplace.client.dto.PaymentPlanDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;

/**
 * @author mabrams
 *
 */
public class SearchServiceImplLocal {

	private static List expertList = null;
	
	public static List getExpertList() {
		if (expertList == null) {
			expertList = new ArrayList();
			for (int i=0; i<3; i++) {
				expertList.add(getExpert(new Random().nextInt()));
			}
		}
		return expertList;
	}

	private static ExpertDTO getExpert(final int expertId) {
		String expertUid = Integer.toHexString(expertId);
		String handle = expertUid + "handle";
		
		ExpertDTO dto = new ExpertDTO();
		ExpertStatsDTO stats = new ExpertStatsDTO();
		PaymentPlanDTO plan = new PaymentPlanDTO();
		SessionDTO session = new SessionDTO();
		
		session.setEnd(new Date().toString());
		session.setExpertId(expertId);
		session.setInsertDate(new Date().toString());
		session.setSessionId(handle);
		session.setItemName(handle + expertId);
		session.setItemNumber(handle);
		session.setPaymentRequested(new Date().toString());
		session.setPaymentSent(new Date().toString());
		session.setSessionGroupId(2*expertId);
		session.setStart(new Date().toString());
		
		plan.setDepositAmount("32.85");
		plan.setMinimumFee("12.99");
		plan.setMinTime(10*expertId);
		plan.setPreferredTime(5*expertId);
		plan.setRatePerMinute(".99");
		
		stats.setAvgTimeToResolution("8");
		stats.setNumberRepeatGuests("237");
		stats.setNumberResolved("901");
		stats.setProblemsHandled(953);
		stats.setSatisfactionRating(98.7f);
		
		ExpertAccountDTO ea = new ExpertAccountDTO();
		ea.setEmail("expert@helpguest.com");
		
		dto.setAvatarURL("images/thinkingAngel.png");
		dto.setBio("I'm an avid star trek fan.  Which does nothing for showing my ability to fix your communicator.  Yes, your mobile phone.  Gawd I love these things.");
		dto.setExpertUid(expertUid);
		dto.setHandle(handle);
		dto.setLive((expertId%2==0?true:false));
		dto.setPaymentPlan(plan);
		dto.setStats(stats);
		dto.setExpertAccount(ea);
		
		return dto;
	}
}
