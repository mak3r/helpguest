/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 23, 2007
 */
package com.helpguest.marketplace.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.data.Expert;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.HelpGuestSearchService;
import com.helpguest.gwt.search.QueryConstraints;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.OfferingsConstraints;
import com.helpguest.gwt.search.TagsConstraints;
import com.helpguest.marketplace.client.SearchService;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.server.converter.ExpertDTOConverter;

/**
 * @author mabrams
 *
 */
public class SearchServiceImpl extends RemoteServiceServlet implements
		SearchService {


	private static final Logger LOG =
		Logger.getLogger(SearchServiceImpl.class);
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 4941177411288853140L;

	public ExpertDTO getExpertByUid(final String expertUid) {
		ExpertAccount ea = ExpertAccount.getExpertAccount(expertUid);
		Expert expert = new Expert(ea.getGuestUid(), ea.getHandle());
		ExpertDTO dto = ExpertDTOConverter.convert(expert);
		
		return dto;			
	}

	public ExpertDTO getExpertByHandle(final String handle) {
		//FIXME This needs to get implmented in hgservice
		/**
		 * 
		 * Consider that
		 * 1) Guests should be able to search for a 'named' expert
		 * and the only name they know is the handle.
		 * 2) Duplicate handles would be confusing and create
		 * a potential hazard for privacy/safety
		 */
		//Expert expert = Expert.getInstance(handle);
		//prahalad adding 08/05/2008
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		
		String expertUid = hgSearchService.getExpertUid(handle);
		//ExpertAccount ea = ExpertAccount.getExpertAccount(expertUid);
		//Expert expert = new Expert(ea.getGuestUid(), ea.getHandle());
		//ExpertDTO dto = ExpertDTOConverter.convert(expert);
		
		
		return getExpertByUid(expertUid);	
	}

	/**
	 * Return a list of ExpertDTO's
	 * {@inheritDoc}
	 */
	public List findExperts(String query) {
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List expertList = hgSearchService.findExperts(query);
		List expertDTOList = new ArrayList();
		if (expertList != null) {
			//if this code doesnt execute, the list sent back is empty
			Iterator it = expertList.iterator();
			while (it.hasNext()) {
				expertDTOList.add(ExpertDTOConverter.convert((Expert) it.next()));
			}
		}
		return expertDTOList;
	}
	
	//prahalad added 06/26/08 to get list of offerings for a category
	public List getOfferings(CategoryConstraints cc) {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("about to search with Constraints: " + cc.getSearchQuery());
		}
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List offeringsList = hgSearchService.getOfferings(cc);
		
	
		return offeringsList;
	}
	
	//prahalad added 06/30/08 to get a list of experts for a find expert text box search
	public List getExpertIdList(QueryConstraints qc) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("about to search with QueryConstraints: " + qc.getSearchQuery());
		}

		try {
			HelpGuestSearchService hgSearchService =
				new HelpGuestSearchService();
			List expertIdList = hgSearchService.getExpertIdList(qc);
			return expertIdList;
		} catch (Exception ex) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Exception trying to getExpertIdList(): " + ex.getMessage(), ex);
			}
			
		}
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("leaving getExpertIdList(QueryConstraints): " + qc.getSearchQuery());
		}
		return null;
	}
	
	//prahalad 07/09/08 adding this method that will return expertid list to drill down
	//from existing expert list and textbox search
	public List getExpertIdList(QueryConstraints qc, List parentExpertIdList) {
		
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List expertIdList = hgSearchService.getExpertIdList(qc, parentExpertIdList);
	
		return expertIdList;
	}
	
	//prahalad added 06/30/08 to get a list of experts for a category
	public List getExpertIdList(CategoryConstraints cc) {
		
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List expertIdList = hgSearchService.getExpertIdList(cc);
	
		return expertIdList;
	}
	
	//prahalad added 06/30/08 to drill down into a list of experts for a category
	public List getExpertIdList(CategoryConstraints cc, List parentExpertIdList) {
		
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List expertIdList = hgSearchService.getExpertIdList(cc, parentExpertIdList);
	
		return expertIdList;
	}

	//prahalad added 10/30/08 to drill down into a list of experts for a category within a query search
	//for display under each corresponding category tab
	public List getExpertIdList(CategoryConstraints cc, QueryConstraints qc, List parentExpertIdList) {
		
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List expertIdList = hgSearchService.getExpertIdList(cc, qc, parentExpertIdList);
	
		return expertIdList;
	}
	
	//prahalad added 06/30/08 to get a list of experts for a offering
	public List getExpertIdList(OfferingsConstraints oc) {
		
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List expertIdList = hgSearchService.getExpertIdList(oc);
	
		return expertIdList;
	}
	
	//prahalad added 06/30/08 to drill down into a list of experts for a offering
	public List getExpertIdList(OfferingsConstraints oc, List parentExpertIdList) {
		
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List expertIdList = hgSearchService.getExpertIdList(oc, parentExpertIdList);
	
		return expertIdList;
	}
	
	//prahalad added 06/30/08 to get a list of experts for a tag
	public List getExpertIdList(TagsConstraints tc) {
		
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List expertIdList = hgSearchService.getExpertIdList(tc);
	
		return expertIdList;
	}
	
	//prahalad added 06/30/08 to drill down into a list of experts for a tag
	public List getExpertIdList(TagsConstraints tc, List parentExpertIdList) {
		
		HelpGuestSearchService hgSearchService =
			new HelpGuestSearchService();
		List expertIdList = hgSearchService.getExpertIdList(tc, parentExpertIdList);
	
		return expertIdList;
	}
	
	

}
