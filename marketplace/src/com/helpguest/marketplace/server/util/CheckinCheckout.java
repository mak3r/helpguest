/*
 * CheckinCheckout.java
 *
 * Created on September 13, 2005, 2:55 AM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 */

package com.helpguest.marketplace.server.util;

import com.helpguest.data.DataBase;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.FileSystem;
import com.helpguest.data.ResultHandler;
import com.helpguest.htmlgen.Generator;
import com.helpguest.protocol.Lexicon;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author mabrams
 */
public class CheckinCheckout extends HttpServlet implements ResultHandler {

    private int queryId;
    private int id;
    private static Set checkins = new HashSet();
    private static Set checkouts = new HashSet();
    private static String IN = String.valueOf(Lexicon.ExpertState.ACTIVE.ordinal());
    private static String OUT = String.valueOf(Lexicon.ExpertState.OFFLINE.ordinal());
    private static Timer timer = new Timer(true);
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.CheckinCheckout");

    //every so often update the checkins/outs
    static {
        TimerTask keepAlive = new TimerTask() {
            public void run() {
                try {
                    //checkins and checkouts should be sycronized?
                    //?should not even have a thread in a servlet?
                    if (checkouts.size() > 0 ) {
                        String outQuery = "update live_expert set is_live = " + Lexicon.ExpertState.OFFLINE.ordinal() + 
                                " where expert_id in (" + formatSet(checkouts) + ")";
                        DataBase.getInstance().submit(outQuery);
                        checkouts.clear();
                    }
                    if (checkins.size() > 0) {
                        String inQuery = "update live_expert set is_live = " + Lexicon.ExpertState.ACTIVE.ordinal() + 
                                " where expert_id in (" + formatSet(checkins) + ")";
                        DataBase.getInstance().submit(inQuery);
                        checkins.clear();
                    }
                } catch (Exception ex) {
                    log.error("oops.", ex);
                }
            }
        };
        int keepAlivePeriod = 15000; //about every 15 seconds
        timer.scheduleAtFixedRate(keepAlive,0, keepAlivePeriod);
    }

    public static String getState(int state) {
        if(Lexicon.ExpertState.ACTIVE.ordinal() == state) {
            return Lexicon.ExpertState.ACTIVE.toString();
        } else if (Lexicon.ExpertState.OFFLINE.ordinal() == state) {
            return Lexicon.ExpertState.OFFLINE.toString();
        } else {
            return "";
        }    
    }

    /** Creates a new instance of Authenticator */
    public CheckinCheckout() {
    }
    
    public void update(String available, String uid) {
        ExpertAccount ea = ExpertAccount.getExpertAccount(uid);
        if (ea != null && ea.isVerified()) {
            if (IN.equals(available)) {
                //sign in
                log.debug("Checking in: " + uid);
                checkins.add(String.valueOf(ea.getId()));
                checkouts.remove(String.valueOf(ea.getId()));
            } else if (OUT.equals(available)) {
                //sign out
                log.debug("Checking out: " + uid);
                checkouts.add(String.valueOf(ea.getId()));
                checkins.remove(String.valueOf(ea.getId()));
            } else {
                log.error("unknown command: " + available);
            }
        }//end if ea != null
    }
    
    private static String formatSet(Set ids) {
        if (ids.size() <= 0) {
            return "";
        }
        StringBuffer idSet = new StringBuffer("");
        Iterator it = ids.iterator();
        while (it.hasNext()) {
        	String id = (String) it.next();
            idSet.append(id + ",");
        }
        //remove the final comma
        idSet.deleteCharAt(idSet.length()-1);
        return idSet.toString();
    }

    public void capture(ResultSet rs, int queryId) {
    }
    
    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int id) {
        queryId = id;
    }

    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request,response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String qString = request.getQueryString();
        String cPath = request.getContextPath();
        String pInfo = request.getPathInfo();
        String rURI = request.getRequestURI();
        
        try {
            String available = URLDecoder.decode(request.getParameter("available"), "UTF-8");            
            String uid = URLDecoder.decode(request.getParameter("uid"), "UTF-8");
            if (log.isDebugEnabled()) {log.debug("available: '" + available + "', uid: '" + uid + "'");}
            update(available, uid);

            response.setContentType("text/plain");
            PrintWriter out;
            try {
                out = response.getWriter();
                String state = (Integer.parseInt(available) == Lexicon.ExpertState.ACTIVE.ordinal() ?
                                Lexicon.ExpertState.ACTIVE.toString() :
                                Lexicon.ExpertState.OFFLINE.toString());
                out.println(state);
            } catch (IOException iox) {
                log.error("Could not reply.", iox);
            }
        } catch (UnsupportedEncodingException uex) {
            log.error("Could not decode paramters.", uex);
        }
    }
    
    public void destroy() {
        timer.cancel();
        super.destroy();
    }
    
    private void debugServlet() {
        if (log.isDebugEnabled()) {
            javax.servlet.ServletContext servletContext = this.getServletContext();
            java.util.Date time = new java.util.Date(System.currentTimeMillis());
            log.debug("ServletContext " + (servletContext==null?"is ":"is not ") + 
                      "null at " + time);
        }
    }
}
