/*
 * Emailer.java
 *
 * Created on January 8, 2007, 9:13 PM
 *
 * Copyright 2006 - HelpGuest Technologies, Inc.
 * @author mabrams
 */

package com.helpguest.marketplace.server.util;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import org.apache.log4j.Logger;

public class Emailer {

    private static Properties props = System.getProperties();
    private static final String to = "scrtyadm@helpguest.com";
    private static final String from = "scrtyadm@helpguest.com";
    public static final String FROM_SUPPORT = "support@helpguest.com";
    
    private static final Logger log = Logger.getLogger("com.helpguest.servlet.util.Emailer");
    private static Message msg;
    
    /** Creates a new instance of Emailer */
    public Emailer() {
    }
    
    /**
    * Send an email requesting verification of the email address provided
    */
    public static void sendAlert(String subject, String message) {
        try {
            Session session = Session.getInstance(props, null);
            session.setDebug(true);
            msg = new VerifyMessage(session);
            try {
                msg.setFrom(new InternetAddress(from));
            } catch (AddressException aex) {
                log.error("from email address " + from + " is invalid.", aex);
            }
            try {
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to,false));
            } catch (AddressException aex) {
                log.warn("email address provided " + to + " is invalid", aex);
            }

            msg.setSubject(subject);
            
            try {
                msg.setDataHandler(new DataHandler(new ByteArrayDataSource(message, "text/plain")));
            } catch (IOException iox) {
                log.error("Could not add message content.", iox);
            }
            msg.setSentDate(new Date());
            Transport.send(msg);
            
        } catch (MessagingException mex) {
            log.error("Could not send email to " + to + ".", mex);
        } catch (Exception ex) {
            log.error("Message did not go out successfully.", ex);
        }
    }
    
    public static void sentMessage(String subject, String message, String from, String to) {
        try {
            Session session = Session.getInstance(props, null);
            session.setDebug(true);
            msg = new VerifyMessage(session);
            try {
                msg.setFrom(new InternetAddress(from));
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to,false));
                msg.setSubject(subject);
                msg.setDataHandler(new DataHandler(new ByteArrayDataSource(message, "text/plain")));
            } catch (AddressException aex) {
                log.warn("invalid address - from: " + from + ", to: " + to + ".", aex);        
            } catch (IOException iox) {
                log.error("Could not add message content.", iox);
            }
            msg.setSentDate(new Date());
            Transport.send(msg);
            
        } catch (MessagingException mex) {
            log.error("Could not send email to " + to + ".", mex);
        } catch (Exception ex) {
            log.error("Message did not go out successfully.", ex);
        }        
    }
    
    static class VerifyMessage extends MimeMessage {
        
        public VerifyMessage(Session session) {
            super(session);
        }
        
        protected void updateHeaders() throws MessagingException {
            super.updateHeaders();
            String header = Long.toHexString(System.currentTimeMillis()) + "servlet-emailer@secure.helpguest.com";
                    
            setHeader("Message-ID", header);
        }
    }
}
