/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 15, 2007
 */
package com.helpguest.marketplace.server;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.data.ProtocolResponse;
import com.helpguest.marketplace.client.ProtocolResponseService;
import com.helpguest.marketplace.client.dto.ProtocolResponseDTO;
import com.helpguest.marketplace.server.converter.ProtocolResponseDTOConverter;

/**
 * @author mabrams
 *
 */
public class ProtocolResponseServiceImpl extends RemoteServiceServlet implements
		ProtocolResponseService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6668282235834425100L;

	private static final Logger LOG = Logger.getLogger(ProtocolResponseServiceImpl.class);

	/**
	 * {@inheritDoc}
	 */
	public ProtocolResponseDTO getProtocolResponse(String guestUid, String type) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("getting response for " +
				"guestUid: " + guestUid + " and type: " + type);
		}
		ProtocolResponse response = ProtocolResponse.getProtocolResponse(guestUid, type);
		ProtocolResponseDTO dto = ProtocolResponseDTOConverter.convert(response);
		return dto;
	}

}
