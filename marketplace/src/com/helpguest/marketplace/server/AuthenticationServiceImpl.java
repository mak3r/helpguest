/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.data.Authenticator;
import com.helpguest.gwt.protocol.AuthenticationOutcome;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.gwt.protocol.VerifyEmailOutcome;
import com.helpguest.marketplace.client.AuthenticationService;
import com.helpguest.remote.service.VerifyEmail;

/**
 * @author mabrams
 *
 */
public class AuthenticationServiceImpl extends RemoteServiceServlet implements
		AuthenticationService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	public AuthenticationOutcome authenticate(
			final String email, final String password,
			final LoginType type, final boolean doGenerateJnlp) {
		Authenticator auth = new Authenticator();

		return auth.authenticate(email, password, type, doGenerateJnlp);
	}

	/**
	 * 
	 */
	public AuthenticationOutcome authenticate(
			final String email, 
			final String password,
			final LoginType type) {
		Authenticator auth = new Authenticator();

		return auth.authenticate(email, password, type);
	}

	/**
	 * @param email the email address to be verified.
	 * @param uid the uid associated with this email address.
	 * @return the VerifyEmailOutcome.
	 */
	public VerifyEmailOutcome verifyEmail(
			final String email,
			final String uid) {
		VerifyEmail verifyEmail = new VerifyEmail();
		return verifyEmail.verify(uid, email);
	}
	
	/**
	 * prahalad adding 07/28/08
	 * @param email the email address to be verified.
	 * @return the VerifyEmailOutcome.
	 */
	public VerifyEmailOutcome verifyEmail(
			final String email) {
		VerifyEmail verifyEmail = new VerifyEmail();
		return verifyEmail.verify(email);
	}
}
