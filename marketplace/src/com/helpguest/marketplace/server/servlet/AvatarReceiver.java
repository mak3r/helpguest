/**
 * VersionTwoDetails.java
 *
 * Copyright 2004-2006 - HelpGuest Technologies, Inc.
 * 
 * 
 * @Created on November 8, 2006, 7:28 PM
 * @author Mark Abrams
 */
package com.helpguest.marketplace.server.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.data.ExpertAccount;
import com.helpguest.marketplace.client.util.DebugPanel;

public class AvatarReceiver extends RemoteServiceServlet {

	/***/
	private static final long serialVersionUID = 1L;

	public static final String USER_KEY = "user";
	private static final String fileLocation = "/var/www/http/jaws/apps/expert/avatars/";
	public static final int maxFileSize = 102400;
	public static final String MULTIPART_FAILURE_MESSAGE = "The file upload failed.";
	public static final String FILE_SIZE_TOO_LARGE = "The file exceeded the maximum size";
	public static final String FILE_MISSING_EXTENSION = "Cannot determine file type. The file is missing a file extension";
	public static final String FILE_UPLOAD_SUCCESS = "SUCCESS";
	public static final String NO_FILE_FOUND = "No file was found to upload.";

	private static final Logger LOG = Logger.getLogger(AvatarReceiver.class);

	/*
	  //prahalad added begin 8/8/08
	  //Create the expert service proxy
	final ExpertServiceAsync expertService =
		(ExpertServiceAsync) GWT.create(ExpertService.class);
	
	//Initialize the ExpertService
	ServiceDefTarget expertEndpoint = (ServiceDefTarget) this.expertService;
	String expertModuleRelativeURL = GWT.getModuleBaseURL() + "ExpertService";
	expertEndpoint.setServiceEntryPoint(expertModuleRelativeURL);*/
/*
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {
			if (LOG.isDebugEnabled()) {LOG.debug("inside doGet()"); }
			/*response.getOutputStream().print("This servlet is alive.");
			 *
			//response.setContentType("text/plain");
			//doPost(request,response);
			response.getOutputStream().println("This servlet is alive 1");
			String callProcessUploadRequest = processUploadRequest(request,
					response);

			response.getOutputStream().print(callProcessUploadRequest);
			response.setContentType("text/plain");
			//Window.alert(callProcessUploadRequest);
			if (LOG.isDebugEnabled()) {
				LOG.debug("upload result in servlet: "
						+ callProcessUploadRequest);
			}
		} catch (Exception ex) {
			//Window.alert("Exception: " + ex.getMessage());
			if (LOG.isDebugEnabled()) {
				LOG.debug("Exception in servlet: " + ex.getMessage());
			}
		}
	}
*/
	private String processUploadedFile(FileItem item, ExpertAccount ea) {
		if (item != null) {
			long fileSize = item.getSize();
			if (fileSize <= maxFileSize) {
				String uploadedName = item.getName();
				if (LOG.isDebugEnabled()) {
					LOG.debug("uploadedName: " + uploadedName);
				}
				String fileType = null;
				int index = uploadedName.lastIndexOf(".");
				if (index > -1) {
					index++;
					fileType = uploadedName.substring(index);
					ea.setAvatarFileType(fileType);
					File avatarFile = new File(ea.getFullPathAvatarFileName());
					try {
						item.write(avatarFile);
					} catch (Exception ex) {
						LOG.error("unable to write avatar file to disk.", ex);
					}
					//expertService.uploadAvatarFile(ea.getUid(),ea.getFullPathAvatarFileName(),updateAvatarFileCallBack );
					return FILE_UPLOAD_SUCCESS;
				} else {
					//otherwise there is no suffix
					return FILE_MISSING_EXTENSION;
				}
			} else {
				//otherwise don't try to handle the file
				return FILE_SIZE_TOO_LARGE;
			}
		} else {
			return NO_FILE_FOUND;
		}
	}

	public String processCall(final String payload) {
		
			LOG.error("payload on processCall: " +payload);

		return payload;
	}
	
	public String processUploadRequest(
			/*final String payload*/
			HttpServletRequest request,
			HttpServletResponse response) {
		
		/*if (LOG.isDebugEnabled()) {
			LOG.debug("payload on processCall: " +payload);
		}*/
		//HttpServletResponse response = getThreadLocalResponse();
		try {
			response.getOutputStream().println(
					"This servlet is alive 1.5 just before request");
		} catch (IOException iox) {
			LOG.error("IOException.", iox);
		}

		//HttpServletRequest request = getThreadLocalRequest();

		String email = null;
		FileItem avatarFile = null;
		boolean isMultipartSuccess = true;

		try {
			response.getOutputStream().println(
					"This servlet is alive 2 before request " + "User = "
							+ request.getParameter(USER_KEY));
		} catch (IOException iox) {
			LOG.error("IOException.", iox);
		}

		// if (ServletFileUpload.isMultipartContent(new ServletRequestContext(request))) {

		FileItemFactory factory = new DiskFileItemFactory(maxFileSize,
				new File(fileLocation));
		ServletFileUpload upload = new ServletFileUpload(factory);
		List items = null;
		try {

			try {
				response.getOutputStream().println(
						"This servlet is alive 3 before parseRequest");
			} catch (IOException iox) {
				LOG.error("IOException.", iox);
			}

			if (LOG.isDebugEnabled()) {
				LOG.debug(" Before parseRequest");
			}

			items = upload.parseRequest(request);

			if (LOG.isDebugEnabled()) {
				LOG.debug(" After parseRequest");
			}

			try {
				response.getOutputStream().println(
						"This servlet is alive 3.5 after parseRequest");
			} catch (IOException iox) {
				LOG.error("IOException.", iox);
			}

			for (int i = 0; i < items.size(); i++) {
				FileItem next = (FileItem) items.get(i);
				if (next.isFormField()) {
					String name = next.getFieldName();
					String value = next.getString();
					//response.getOutputStream().print("name = " + name + "value = " + value);
					if (LOG.isDebugEnabled()) {
						LOG.debug("FieldName: " + name + " FieldValue: "
								+ value);
					}
					if (name != null) {
						if (name.equalsIgnoreCase(USER_KEY)) {
							email = value;
						}
					}
				} else {
					//process this after we get the expert account
					avatarFile = next;
				}
			}
		} catch (FileUploadException fuex) {
			LOG.error("Failed to parse request.", fuex);
			isMultipartSuccess = false;
			//TODO: we should let the user know it was a problem
			return fuex.getMessage();
		}
		//  } else {        
		email = request.getParameter(USER_KEY);

		try {
			response.getOutputStream().println(
					"This servlet is alive 4 not isMultipartContent");
		} catch (IOException iox) {
			LOG.error("IOException.", iox);
		}

		// }        

		if (LOG.isDebugEnabled()) {
			java.util.Enumeration en = request.getParameterNames();
			while (en.hasMoreElements()) {
				String next = (String) en.nextElement();
				LOG.debug("parameter: " + next + ", value: "
						+ request.getParameter(next));
			}
		}

		response.setContentType("text/plain");
		if (isMultipartSuccess) {

			//Not Requiring the password to get the expert account info
			// at this time.
			ExpertAccount ea = ExpertAccount.getExpertAccount(email);

			return processUploadedFile(avatarFile, ea);
		} else {
			//isMultipartSuccess is false
			return MULTIPART_FAILURE_MESSAGE;
		}

	}

	AsyncCallback updateAvatarFileCallBack = new AsyncCallback() {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (InvocationException iex) {
				StackTraceElement[] ste = iex.getStackTrace();
				StringBuffer sBuf = new StringBuffer(iex.getMessage());
				for (int i = 0; i < ste.length; i++) {
					sBuf.append("\n\t" + ste.toString());
				}
//				Window.alert("AvatarUploadPanel updateAvatarFileCallBack: "
//						+ sBuf.toString());
			} catch (Throwable th) {
//				Window
//						.alert("AvatarUploadPanel updateAvatarFileCallBack: [throwable]"
//								+ th.getMessage());
			}
		}

		public void onSuccess(Object result) {
//			Window.alert("File Saved in DB.");
		}
	};
}
