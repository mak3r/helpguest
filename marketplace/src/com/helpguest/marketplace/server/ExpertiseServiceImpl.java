/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.server;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.data.Expertise;
import com.helpguest.gwt.search.CategoryConstraints;
import com.helpguest.gwt.search.OfferingsConstraints;
import com.helpguest.marketplace.client.ExpertiseService;
import com.helpguest.marketplace.client.dto.ExpertDTO;

/**
 * @author mabrams
 *
 */
public class ExpertiseServiceImpl extends RemoteServiceServlet implements
		ExpertiseService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5547101542517283339L;
	
	/***/
	private static final Logger LOG = Logger.getLogger(ExpertiseServiceImpl.class);
	/**
	 * @gwt.tyepArgs <java.lang.String>
	 * {@inheritDoc}
	 */
	public List getExpertise(ExpertDTO dto) {
		// TODO This relies on a service offerings object
		return null;
	}

	/**
	 * @gwt.tyepArgs <java.lang.String>
	 * {@inheritDoc}
	 */
	public List getMostCommon(int limit) {
		Expertise expertise = new Expertise();
		return expertise.getCommonRequests(limit);
	}

	/**
	 * @gwt.tyepArgs <java.lang.String>
	 * {@inheritDoc}
	 */
	public List getMostRequested(int limit) {
		Expertise expertise = new Expertise();
		return expertise.getRecentRequests(limit);
	}

	/**
	 * @gwt.tyepArgs <java.lang.String>
	 * {@inheritDoc}
	 */
	public List getMostOffered(int limit) {
		/**/
		Expertise expertise = new Expertise();
		return expertise.getExpertOfferings(limit);
		/**
		List dummy = new java.util.ArrayList();
		for (int i=0; i<limit; i++) {
			dummy.add(Integer.toHexString(250*i));
		}
		return dummy;
		/**/
	}
	
	public List getCategories() {
		Expertise expertise = new Expertise();
		List categoryList = new ArrayList();
		categoryList = expertise.getExpertCategories();
		if (LOG.isDebugEnabled()) {
			LOG.debug("successfully retrieved the categories: ");
			LOG.debug("There are " + categoryList.size() + " element(s).");
		}
		
		return categoryList;
		
	}
	
	/**
	 * @author ying
	 * */
	public List getExpertCategories(String expert_id) {
		Expertise expertise = new Expertise();
		return expertise.getExpertCategories(expert_id);
	}
	
	public List getCatOffgs() {
		Expertise expertise = new Expertise();
		return expertise.getCatOffPairs();
	}
	
	public List getCatOffgTags(String expert_id) {
		Expertise expertise = new Expertise();
		return expertise.getCatOffTagTuples(expert_id);
	}
	
	public List getOfferings(CategoryConstraints cc) {
		String category = cc.getCategoryName();
		Expertise expertise = new Expertise();
		if(LOG.isDebugEnabled()) {
			LOG.debug("returning expertise for category:" + category);
		}
		//at least this way, we never return null
		List expertiseList = new ArrayList();
		try {
			expertiseList = expertise.getExpertOfferings(category);
			if (LOG.isDebugEnabled()) {
				LOG.debug("successfully retrieved the expertise list for category: " + category);
				LOG.debug("expertiseList has: " + expertiseList.size() + " element(s).");
			}
		} catch (Exception ex) {
			LOG.error("Failed to get the expertise list for category: " + category, ex);
		}
		return expertiseList;
	}
	
	public List getTags(CategoryConstraints cc, OfferingsConstraints oc, String expert_id){
		String category = cc.getCategoryName();
		String offering = oc.getOfferingName();
		Expertise expertise = new Expertise();
		return expertise.getExpertTags(category,offering,expert_id);
		
	}
	
	public void updateTags(CategoryConstraints cc, OfferingsConstraints oc, String expertId, String tagContent){
		String category = cc.getCategoryName();
		String offering = oc.getOfferingName();
		Expertise expertise = new Expertise();
	    List tagInDb = expertise.getExpertTags(category,offering,expertId);
		if (tagInDb.isEmpty())
		{
			if (!tagContent.trim().equals(""))
				{expertise.insertExpertTags(category, offering, expertId, tagContent);}
			}else{
					if(tagContent.trim().equals(""))
					{
						expertise.deleteExpertTags(category, offering, expertId);
					}else
					{expertise.updateExpertTags(category, offering, expertId, tagContent);}
			}
		
	}	
}
