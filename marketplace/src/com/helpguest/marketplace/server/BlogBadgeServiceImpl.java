/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.server;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.data.RemoteLaunchOption;
import com.helpguest.data.RemoteLaunchOptions;
import com.helpguest.marketplace.client.BlogBadgeService;
import com.helpguest.marketplace.client.dto.BadgeDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.server.converter.BadgeDTOConverter;

/**
 * @author mabrams
 *
 */
public class BlogBadgeServiceImpl extends RemoteServiceServlet implements
		BlogBadgeService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(BlogBadgeServiceImpl.class);

	public BadgeDTO createBadge(final ExpertDTO expertDTO, final BadgeDTO badgeDTO) {
		//FIXME set based on actual value not test value
		badgeDTO.setExpertId(5);
		//badgeDTO.setExpertId(expertDTO.getExpertAccount().getId());
		
		RemoteLaunchOptions rlOptions = new RemoteLaunchOptions();
		RemoteLaunchOption rlo = BadgeDTOConverter.convert(badgeDTO);
		rlo = rlOptions.createLaunchOption(rlo);
		BadgeDTO dto = BadgeDTOConverter.convert(rlo);
		
		return dto;
	}

	public BadgeDTO updateBadge(final ExpertDTO expertDTO, final BadgeDTO badgeDTO) {
		return null;
	}
	
	public BadgeDTO deleteBadge(final ExpertDTO expertDTO, final BadgeDTO badgeDTO) {
		RemoteLaunchOptions rlOptions = new RemoteLaunchOptions();
		RemoteLaunchOption rlo = BadgeDTOConverter.convert(badgeDTO);
		LOG.debug("rlo id: " + rlo.getRowId());
		rlo = rlOptions.deleteLaunchOption(rlo);
		return BadgeDTOConverter.convert(rlo);
	}
	
	public List<BadgeDTO> getAllBadges(final ExpertDTO expertDTO) {
		RemoteLaunchOptions rlOptions = new RemoteLaunchOptions();
		//FIXME set based on actual value not test value
		List<RemoteLaunchOption> optionList = rlOptions.getLaunchOptions(5);
			//rlOptions.getLaunchOptions(expertDTO.getExpertAccount().getId());
		
		List<BadgeDTO> badges = new ArrayList<BadgeDTO>();
		for (RemoteLaunchOption next : optionList) {
			BadgeDTO badge = new BadgeDTO();
			badge = BadgeDTOConverter.convert(next);
			badges.add(badge);
		}
		
		return badges;
	}

}
