/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.data.DemoSession;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.HGRates;
import com.helpguest.data.HGSession;
import com.helpguest.data.Rates;
import com.helpguest.data.Session;
import com.helpguest.data.SessionGroup;
import com.helpguest.gwt.protocol.LoginType;
import com.helpguest.marketplace.client.SessionService;
import com.helpguest.marketplace.client.dto.ExpertAccountDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.server.converter.ExpertAccountDTOConverter;
import com.helpguest.marketplace.server.converter.SessionDTOConverter;
import com.helpguest.service.JnlpGen;

/**
 * @author mabrams
 *
 */
public class SessionServiceImpl extends RemoteServiceServlet implements
		SessionService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8006107191899336178L;

    private static final Logger LOG =
		Logger.getLogger("com.helpguest.marketplace.server.SessionServiceImpl");
	
	/**
	 * {@inheritDoc}
	 */
	public SessionDTO createSession(final String expertEmail) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("creating session for '" + expertEmail + "'");
		}
		ExpertAccount expertAccount = ExpertAccount.getExpertAccount(expertEmail);
		Rates rates = HGRates.getCurrentInstance(expertAccount);
		Session session = HGSession.newInstance(expertAccount, rates);
		if (LOG.isDebugEnabled()) {
			LOG.debug("expertAccountId : " + expertAccount.getId());
			LOG.debug("ratesId : " + rates.getId());
			LOG.debug("sessionId : " + session.getSessionId());
		}		
		
		SessionDTO dto = SessionDTOConverter.convert(session);		
		return dto;
	}

	public SessionDTO createDemoSession(final String expertEmail) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Creating demo session");
		}
		SessionDTO dto = new SessionDTO();
		//If the expert can demo, create a new demo session.
		ExpertAccount ea = ExpertAccount.getExpertAccount(expertEmail);
		if (ea.getCanDemo() > 0) {
			Session session = DemoSession.createDemoSession(ea);
			dto = SessionDTOConverter.convert(session);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Demo session has session id: " + dto.getSessionId());
		}

		return dto;
	}
	
	public SessionDTO getDemoSession(final ExpertDTO expert) {
		ExpertAccount ea = ExpertAccount.getExpertAccount(expert.getExpertUid());
		Session session = HGSession.getDemoInstance(ea);
		
		//Generate the jnlp files for the client
		if (expert.isCanDemo()) {
			JnlpGen.generateHtml(session.getSessionId(), new LoginType.Client());
			JnlpGen.generateJnlp(session.getSessionId(), new LoginType.Client());
		}
		//Convert to the usable session dto
		SessionDTO dto = SessionDTOConverter.convert(session);
				
		return dto;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public SessionDTO getSession(String sessionId) {
		Session session = HGSession.getInstance(sessionId);
		ExpertAccount ea = ExpertAccount.getExpertAccount(session);
		
		SessionDTO dto = SessionDTOConverter.convert(session);
		ExpertAccountDTO eaDTO = ExpertAccountDTOConverter.convert(ea);
		dto.setExpertAccount(eaDTO);
		        
		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	public List getAllSessions(ExpertAccountDTO dto) {
		LOG.debug("SessionServiceImpl: Entering getAllSessions()");
		ExpertAccount ea = ExpertAccount.getExpertAccount(dto.getUid());
		SessionGroup allSessions = new SessionGroup(ea, true);
		Collection sessionGroupList = allSessions.getSessions();
		List sessionDTOList = new ArrayList();
		Iterator it = sessionGroupList.iterator();
		while(it.hasNext()) {
			SessionDTO sessionDTO = 
				SessionDTOConverter.convert((Session) it.next());
			sessionDTOList.add(sessionDTO);
		}
		LOG.debug("SessionServiceImpl: Exiting getAllSessions()");
		return sessionDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	public List getUnpaidSessions(ExpertAccountDTO dto) {
		ExpertAccount ea = ExpertAccount.getExpertAccount(dto.getUid());
		SessionGroup unpaidSessions = new SessionGroup(ea, false);
		Collection sessionGroupList = unpaidSessions.getSessions();
		List sessionDTOList = new ArrayList();
		Iterator it = sessionGroupList.iterator();
		while(it.hasNext()) {
			SessionDTO sessionDTO = 
				SessionDTOConverter.convert((Session) it.next());
			sessionDTOList.add(sessionDTO);
		}
		return sessionDTOList;
	}

}
