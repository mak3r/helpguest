/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 25, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.RemoteLaunchOption;
import com.helpguest.marketplace.client.dto.BadgeDTO;

/**
 * @author mabrams
 *
 */
public class BadgeDTOConverter {

	public static BadgeDTO convert(RemoteLaunchOption rlo) {
		if (rlo == null) {
			return null;
		}
		
		BadgeDTO dto = new BadgeDTO();

		dto.setAmount(rlo.getAmount());
		dto.setBadgeRowId(rlo.getRowId());
		dto.setBaseImageRowId(rlo.getLaunchIconId());
		dto.setExpertId(rlo.getExpertId());
		dto.setName(rlo.getRefName());
		dto.setSubText(rlo.getSubText());
		dto.setSubtextGroupId(rlo.getSubtextGroupId());
		dto.setPostURL(rlo.getPostURL());
		dto.setRemoteURLContent(rlo.getRemoteURLContent());
		
		return dto;
	}
	
	public static RemoteLaunchOption convert(BadgeDTO dto) {
		RemoteLaunchOption rlo = new RemoteLaunchOption();
		
		rlo.setAmount(dto.getAmount());
		if (dto.getAmount() > 0) {
			rlo.setPayLink(true);
		}
		rlo.setRowId(dto.getBadgeRowId());
		rlo.setLaunchIconId(dto.getBaseImageRowId());
		rlo.setExpertId(dto.getExpertId());
		rlo.setRefName(dto.getName());
		rlo.setSubText(dto.getSubText());
		rlo.setSubtextGroupId(dto.getSubtextGroupId());
		rlo.setPostURL(dto.getPostURL());
		rlo.setRemoteURLContent(dto.getRemoteURLContent());
		
		return rlo;
	}
}
