/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 15, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.ProtocolResponse;
import com.helpguest.marketplace.client.dto.ProtocolResponseDTO;

/**
 * @author mabrams
 *
 */
public class ProtocolResponseDTOConverter {
	
	
	public static ProtocolResponseDTO convert(final ProtocolResponse response) {
		ProtocolResponseDTO dto = null;

		if (response != null) {
			dto = new ProtocolResponseDTO();
			dto.setId(response.getId());
			dto.setInetAddress(response.getInetAddress());
			dto.setKeyId(response.getKeyId());
			dto.setPort(response.getPort());
			dto.setType(response.getType());
			dto.setUid(response.getUid());
			dto.setResponseTS(response.getResponseTS().getTime());
		}
		
		return dto;
	}
}
