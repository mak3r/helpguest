/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.ExpertAccount;
import com.helpguest.marketplace.client.dto.ExpertAccountDTO;

/**
 * @author mabrams
 *
 */
public class ExpertAccountDTOConverter {

	public static ExpertAccountDTO convert(final ExpertAccount ea) {
		
		ExpertAccountDTO dto = new ExpertAccountDTO();
		dto.setEmail(ea.getEmail());
		dto.setEndDate(ea.getEndDate());
		dto.setExpertiseList(ea.getExpertise());
		dto.setGuestsWaiting(ea.getGuestsWaiting());
		dto.setGuestUid(ea.getGuestUid());
		dto.setHandle(ea.getHandle());
		dto.setId(ea.getId());
		dto.setPaypalEmail(ea.getPaypalEmail());
		dto.setUid(ea.getUid());
		
		return dto;
	}
}
