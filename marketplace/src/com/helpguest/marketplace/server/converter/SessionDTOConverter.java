/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 25, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.Session;
import com.helpguest.marketplace.client.dto.SessionDTO;

/**
 * @author mabrams
 *
 */
public class SessionDTOConverter {

	public static SessionDTO convert(Session session) {
		SessionDTO dto = new SessionDTO();

		if (session.getEnd() != null) {
			dto.setEnd(session.getEnd().toString());
		}
		dto.setExpertId(session.getExpertId());
		if (session.getInsertDate() != null) {
			dto.setInsertDate(session.getInsertDate().toString());
		}
		dto.setSessionId(session.getSessionId());
		dto.setItemName(session.getItemName());
		dto.setItemNumber(session.getItemNumber());
		if (session.getPaymentRequestDate() != null) {
			dto.setPaymentRequested(session.getPaymentRequestDate().toString());
		}
		if (session.getPaymentSentDate() != null) {
			dto.setPaymentSent(session.getPaymentSentDate().toString());
		}
		dto.setSessionGroupId(session.getSessionGroupId());
		if (session.getStart() != null) {
			dto.setStart(session.getStart().toString());
		}
		if (session.getRates() != null) {
			dto.setRates(RatesDTOConverter.convert(session.getRates()));
		}
		dto.setClosed(session.isClosed());
		if (session.getEnd() != null && session.getStart() != null) {
			dto.setDuration(session.getEnd().getTime() - session.getStart().getTime());
		}
		
		return dto;
	}
}
