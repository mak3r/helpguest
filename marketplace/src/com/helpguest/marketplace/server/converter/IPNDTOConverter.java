/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.IPNData;
import com.helpguest.marketplace.client.dto.IPNDTO;

/**
 * @author mabrams
 *
 */
public class IPNDTOConverter {

	public static IPNDTO convert(final IPNData ipnData) {
		
		IPNDTO dto = new IPNDTO();
		dto.setInvoice(ipnData.getInvoice());
		dto.setItemName(ipnData.getItemName());
		dto.setItemNumber(ipnData.getItemNumber());
		dto.setPayerEmail(ipnData.getPayerEmail());
		dto.setPaymentStatus(ipnData.getPaymentStatus());
		dto.setVerifySign(ipnData.getVerifySign());
		
		return dto;
	}
}
