/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 25, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.Rates;
import com.helpguest.marketplace.client.dto.RatesDTO;

/**
 * @author mabrams
 *
 */
public class RatesDTOConverter {

	public static RatesDTO convert(Rates rates) {
		RatesDTO dto = new RatesDTO();

		dto.setDepositAmount(rates.getDepositAmount());
		dto.setExpertId(rates.getExpertId());
		dto.setFormattedDepositAmount(rates.getFormattedDepositAmount());
		dto.setFormattedMinFee(rates.getFormattedMinFee());
		dto.setFormattedMinRate(rates.getFormattedMinRate());
		dto.setId(rates.getId());
		dto.setMinFee(rates.getMinFee());
		dto.setMinRate(rates.getMinRate());
		dto.setMinTime(rates.getMinTime());
		dto.setPrefTime(rates.getPreferredTime());
		
		return dto;
	}
}
