/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Oct 25, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.Evaluation;
import com.helpguest.marketplace.client.dto.EvaluationDTO;

/**
 * @author mabrams
 *
 */
public class EvaluationDTOConverter {

	public static EvaluationDTO convert(
			final Evaluation eval) {
		
		EvaluationDTO dto = new EvaluationDTO();
		dto.setFormattedOverallSatisfaction(eval.getFormattedOverallSatisfaction());
		dto.setFormattedPointPercent(eval.getFormattedPointPercent());
		dto.setFormattedProblemsSolved(eval.getFormattedProblemsSolved());
		dto.setFormattedRepeatCustomers(eval.getFormattedRepeatCustomers());
		dto.setPointCount(eval.getPointCount());
		dto.setPossiblePoints(eval.getPossiblePointCount());
		dto.setProblemsSolved(eval.getProblemsSolved());
		dto.setProblemsSolvedCount(eval.getProblemsSolvedCount());
		dto.setRepeatCustomers(eval.getRepeatCustomers());
		dto.setRepeatCustomersCount(eval.getRepeatCustomersCount());
		dto.setSatisfaction(eval.getOverallSatisfaction());
		dto.setSatisfactionCount(eval.getSatisfactionCount());

		return dto;
	}
}
