/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.helpguest.data.ServiceOffering;
import com.helpguest.marketplace.client.dto.ServiceOfferingDTO;

/**
 * @author mabrams
 *
 */
public class ServiceOfferingDTOConverter implements IsSerializable {

	public static ServiceOfferingDTO convert(final ServiceOffering offering) {
		
		ServiceOfferingDTO dto = new ServiceOfferingDTO();
		dto.setExpertise(offering.getExpertise());
		
		return dto;
	}
}
