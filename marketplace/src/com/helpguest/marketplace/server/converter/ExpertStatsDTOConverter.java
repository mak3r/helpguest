/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 25, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.Evaluation;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.SessionGroup;
import com.helpguest.marketplace.client.dto.ExpertStatsDTO;

/**
 * @author mabrams
 *
 */
public class ExpertStatsDTOConverter {

	public static ExpertStatsDTO convert(final ExpertAccount ea) {
		ExpertStatsDTO dto = new ExpertStatsDTO();
		
		SessionGroup evalGroup = new SessionGroup(ea, true);
        Evaluation eval = new Evaluation(evalGroup);
		
        dto.setAvgTimeToResolution(evalGroup.getFormattedAverageSessionTime());
		dto.setNumberRepeatGuests(eval.getFormattedRepeatCustomers());
		dto.setNumberResolved(eval.getFormattedProblemsSolved());
		dto.setProblemsHandled(evalGroup.getSessions().size());
		dto.setSatisfactionRating(eval.getOverallSatisfaction());
		
		return dto;
	}
}
