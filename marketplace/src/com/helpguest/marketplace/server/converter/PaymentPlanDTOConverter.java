/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 24, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.Rates;
import com.helpguest.marketplace.client.dto.PaymentPlanDTO;

/**
 * @author mabrams
 *
 */
public class PaymentPlanDTOConverter {

	public static PaymentPlanDTO convert(Rates rates) {
		PaymentPlanDTO dto = new PaymentPlanDTO();
		dto.setDepositAmount(rates.getFormattedDepositAmount());
		dto.setMinimumFee(rates.getFormattedMinFee());
		dto.setRatePerMinute(rates.getFormattedMinRate());
		dto.setMinTime(rates.getMinTime());
		dto.setPreferredTime(rates.getPreferredTime());
		return dto;
	}
}
