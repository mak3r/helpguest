/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jun 24, 2007
 */
package com.helpguest.marketplace.server.converter;

import com.helpguest.data.Expert;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.HGRates;
import com.helpguest.data.Rates;
import com.helpguest.marketplace.client.dto.ExpertAccountDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.ExpertStatsDTO;
import com.helpguest.marketplace.client.dto.PaymentPlanDTO;

/**
 * @author mabrams
 *
 */
public class ExpertDTOConverter {

	public static ExpertDTO convert(final Expert expert) {

		ExpertAccount ea = expert.getExpertAccount();
    	Rates rates = HGRates.getCurrentInstance(ea);
        
    	PaymentPlanDTO paymentPlan =
    		PaymentPlanDTOConverter.convert(rates);
    	
    	ExpertStatsDTO expertStats =
    		ExpertStatsDTOConverter.convert(ea);
    	
    	ExpertAccountDTO eaDTO = ExpertAccountDTOConverter.convert(ea);

    	ExpertDTO dto = new ExpertDTO();
    	dto.setPaymentPlan(paymentPlan);
    	dto.setStats(expertStats);
    	
    	dto.setHandle(expert.getHandle());
    	dto.setBio(ea.getSelfDescription());
    	dto.setLive(ea.isLive());
    	dto.setExpertUid(ea.getUid());
    	dto.setAvatarURL(ea.getAvatarURL().toExternalForm());
    	dto.setExpertAccount(eaDTO);
    	dto.setCanDemo((ea.getCanDemo()!=0?true:false));
    	
		return dto;
	}
}
