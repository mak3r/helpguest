/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 1, 2007
 */
package com.helpguest.marketplace.server;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.HGSession;
import com.helpguest.data.IPNData;
import com.helpguest.data.Session;
import com.helpguest.marketplace.client.IPNService;
import com.helpguest.marketplace.client.dto.ExpertAccountDTO;
import com.helpguest.marketplace.client.dto.IPNDTO;
import com.helpguest.marketplace.client.dto.SessionDTO;
import com.helpguest.marketplace.server.converter.ExpertAccountDTOConverter;
import com.helpguest.marketplace.server.converter.IPNDTOConverter;
import com.helpguest.marketplace.server.converter.SessionDTOConverter;

/**
 * @author mabrams
 *
 */
public class IPNServiceImpl extends RemoteServiceServlet implements
		IPNService {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7520135631559127507L;

	private static final Logger LOG = Logger.getLogger(IPNServiceImpl.class);

	/**
	 * {@inheritDoc}
	 */
	public IPNDTO getCompletionDetails(String sessionId) {
        if (LOG.isDebugEnabled()) {
        	LOG.debug("inside getCompletionDetails()");
        }
        Session session = HGSession.getInstance(sessionId);
        SessionDTO sessionDTO = SessionDTOConverter.convert(session);

		ExpertAccount ea = ExpertAccount.getExpertAccount(session);
		if (ea == null) {
			return null;
		}
		ExpertAccountDTO eaDTO = ExpertAccountDTOConverter.convert(ea);
		sessionDTO.setExpertAccount(eaDTO);

		//See if we've got a completed ipn 
        IPNData ipn = IPNData.getInstance(session, IPNData.COMPLETED);

        if (ipn == null) {
        	return null;
        }
        IPNDTO ipnDTO = IPNDTOConverter.convert(ipn);   
        ipnDTO.setSession(sessionDTO);
        
        return ipnDTO;
	}

}
