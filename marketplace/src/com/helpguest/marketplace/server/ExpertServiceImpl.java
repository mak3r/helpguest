/**
 * HelpGuest Technologies, Inc.
 * Copyright 2007
 *
 * Created on Jul 2, 2007
 */
package com.helpguest.marketplace.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.helpguest.data.Evaluation;
import com.helpguest.data.Expert;
import com.helpguest.data.ExpertAccount;
import com.helpguest.data.ExpertDetails;
import com.helpguest.data.HGRates;
import com.helpguest.data.ResetCode;
import com.helpguest.data.SessionGroup;
import com.helpguest.gwt.protocol.SignUpOutcome;
import com.helpguest.marketplace.client.ExpertService;
import com.helpguest.marketplace.client.dto.EvaluationDTO;
import com.helpguest.marketplace.client.dto.ExpertAccountDTO;
import com.helpguest.marketplace.client.dto.ExpertDTO;
import com.helpguest.marketplace.client.dto.PaymentPlanDTO;
import com.helpguest.marketplace.server.converter.EvaluationDTOConverter;
import com.helpguest.marketplace.server.converter.ExpertDTOConverter;
import com.helpguest.remote.service.SignUp;
//import com.helpguest.remote.service.SignUp.SMTPAuthenticator;
//import com.helpguest.remote.service.SignUp.VerifyMessage;
import com.google.gwt.user.client.Window;

/**
 * @author mabrams
 *
 */
public class ExpertServiceImpl extends RemoteServiceServlet implements
		ExpertService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 507583790400905539L;

	/**
	 * These should be objects of some type.
	 */
	public static final String REMOVE = "remove";
    public static final String ADD = "add";
    public static final String PRIMARY = "primary";

	private static final Logger LOG =
		Logger.getLogger("com.helpguest.marketplace.server.ExpertServiceImpl");
	
	/**
	 * {@inheritDoc}
	 */
	public ExpertDTO getExpert(String expertUid) {
		/*if (LOG.isDebugEnabled()) {
			LOG.debug("Getting expert by expert uid : " + expertUid);
		}*/
		ExpertAccount ea = ExpertAccount.getExpertAccount(expertUid);
		Expert expert = new Expert(ea.getGuestUid(), ea.getHandle());
		ExpertDTO dto = ExpertDTOConverter.convert(expert);
		
		return dto;		
	}
	
	/**
	 * {@inheritDoc}
	 */
	
	
	/**
	 * {@inheritDoc}
	 */
	public void makePayments(final ExpertAccountDTO dto) {
		ExpertAccount ea = ExpertAccount.getExpertAccount(dto.getUid());
		//if (LOG.isDebugEnabled()) {LOG.debug("makePayments dto.getPaypalEmail() " + dto.getPaypalEmail());}
		if (dto.getPaypalEmail() != null && !"".equals(dto.getPaypalEmail())) {
			ea.updatePaypalEmail(dto.getPaypalEmail());

			SessionGroup unpaidSessions = new SessionGroup(ea, false);
			unpaidSessions.requestPaymentOnGroup();			
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public ExpertDTO modifyExpertise(
			final ExpertDTO dto, 
			final String updateList,
			final String mode) {
		ExpertDTO updatedExpertDTO = dto;
		ExpertAccount ea = ExpertAccount.getExpertAccount(dto.getExpertUid());
		boolean updated = false;
		
        //Don't try anything if updateList is null or empty
        if (updateList != null && updateList.length() > 0) {
            if (PRIMARY.equalsIgnoreCase(mode)) {
                String primary = (String) getExpertiseList(updateList).get(0);
                ea.updatePrimaryExpertise(primary);
                updated = true;
            } else if (REMOVE.equalsIgnoreCase(mode)) {
                ea.removeExpertise(getExpertiseList(updateList));
                updated = true;
            } else if (ADD.equalsIgnoreCase(mode)) {
                ea.addExpertise(getExpertiseList(updateList));
                updated = true;
            }
            //If the expertise was updated, we will refresh the expert
            if (updated) {
            	updatedExpertDTO = getExpert(dto.getExpertUid());
            }
        }
		return updatedExpertDTO;
	}
	
    //Turn a comma separated list into an array list
    private ArrayList getExpertiseList(String commaList) {
        ArrayList expertiseList = new ArrayList();
        String[] stringList = commaList.split(",");        
        for (int i=0; i<stringList.length; i++) {
            if (stringList[i].trim().length()>0) {
                expertiseList.add(stringList[i].trim());
            }
        }        
        return expertiseList;
    }
    
	/**
	 * 
	 * @param email The new email addres String
	 * @param pass the new password String
	 * @param handle the new handle String
	 * @param offering the new expertise String
	 * @param zip the new zipcode String
	 * @param callback the AsyncCallback
	 */
	public SignUpOutcome createAccount(
			final String email, 
    		final String pass,
    		final String handle, 
    		final String offering, 
    		final String zip) {

		SignUp signUp = new SignUp();
		String ipAddress = this.getThreadLocalRequest().getRemoteAddr();
		return signUp.createAccount(email, pass, handle, offering, zip, ipAddress);
	}
	
	/**
	 * prahalad adding 7/23/08 alternate createAccount method to handle ArrayList of catOffgTags
	 * @param email The new email addres String
	 * @param pass the new password String
	 * @param handle the new handle String
	 * @param zip the new zipcode String
	 * @param callback the AsyncCallback
	 */
	public SignUpOutcome createAccount(
			final String email, 
    		final String pass,
    		final String handle, 
    		final List<String> catOffgTagList, 
    		final String zip,
    		final String referralSource){
		
		SignUp signUp = new SignUp();
		String ipAddress = this.getThreadLocalRequest().getRemoteAddr();
		return signUp.createAccount(email, pass, handle, catOffgTagList, zip, ipAddress, referralSource);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public EvaluationDTO getEvaluation(final ExpertDTO expert) {
		ExpertAccount ea = ExpertAccount.getExpertAccount(expert.getExpertUid());
        SessionGroup evalGroup = new SessionGroup(ea, true);
        Evaluation eval = new Evaluation(evalGroup);

        EvaluationDTO dto = EvaluationDTOConverter.convert(eval);
        
		return dto;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public ExpertDTO updateAccount(final ExpertDTO expert) {
		ExpertAccount ea = ExpertAccount.getExpertAccount(expert.getExpertUid());
		//ea.updateBio(expert.getBio());
		//prahalad 07-01-08 adding
		ExpertDetails ed = new ExpertDetails(ea);
		ed.setSelfDescription(expert.getBio());

		try {
			PaymentPlanDTO ppDTO = expert.getPaymentPlan();
			HGRates.newInstance(ea, Float.parseFloat(ppDTO.getRatePerMinute()), ppDTO.getPreferredTime(), false);
		} catch (NumberFormatException nfx) {
			//do nothing
		}
		
		return getExpert(expert.getExpertUid());
	}
	
	//prahalad adding updatePaswd, expireResetCode, getResetCode, sendEmail 07/30/08 
	//for password reset app
	public void updatePaswd(final ExpertDTO expert, final String newPaswd) {
		ExpertAccount ea = ExpertAccount.getExpertAccount(expert.getExpertUid());
		ea.updatePassword(newPaswd);
	}
	
	public void expireResetCode(final String currResetCode, final String expertEmail) {
		ResetCode resetCode = ResetCode.getInstance(currResetCode, expertEmail);
		ResetCode.expire(resetCode);
	}
	
	public void getResetCode(final String expertEmail) {
		ResetCode resetCode = ResetCode.getInstance(expertEmail);
		resetCode.sendEmail(expertEmail);
		//resetCode = resetCode.newInstance(expertEmail);
		//resetCode = ResetCode.getInstance(expertEmail);
		//return resetCode.getCode();
	}
	
	public void uploadAvatarFile(final String uid, final String avatarFile) {
		ExpertAccount ea = ExpertAccount.getExpertAccount(uid);
		//ea.uploadAvatarFile(avatarFile);
		//Window.alert("File is: " + avatarFile );
	}
	
	public ArrayList<String> getSurveyFromDB(final String uid, final String sessionId) {
		ExpertAccount ea = ExpertAccount.getExpertAccount(uid);
		ea.getSurvey(sessionId);
		//ea.uploadAvatarFile(avatarFile);
		//Window.alert("File is: " + avatarFile );
		
		//Expert expert = new Expert(ea.getGuestUid(), ea.getHandle());
		//ExpertDTO dto = ExpertDTOConverter.convert(expert);
		ArrayList<String> survey = new ArrayList<String>();
		survey.add(ea.getOverallSatisfaction());
		survey.add(ea.getProblemSolved());
		survey.add(ea.getRecommend());
		survey.add(ea.getRepeatCustomer());
		survey.add(ea.getProfessionalService());
		
		return survey;
	}
	
	public void submitSurveyToDB(final String uid, final String sessionId, 
			final String strSatisfaction, final String strProblemSolved, final String strRecommend, 
			final String strRepeatCustomer, final String strProfessionalManner, final String surveyAction){
		ExpertAccount ea = ExpertAccount.getExpertAccount(uid);
		ea.setOverallSatisfaction(strSatisfaction);
		ea.setProblemSolved(strProblemSolved);
	    ea.setRecommend(strRecommend);
	    ea.setRepeatCustomer(strRepeatCustomer);
		ea.setProfessionalService(strProfessionalManner);
		ea.submitSurvey(sessionId, surveyAction);
	}
	

}
