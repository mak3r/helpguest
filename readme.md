# Building from source on linux

## Setting up the database 
1. Adding service users to the DB

    * `CREATE USER 'hlpguest'@'localhost' IDENTIFIED BY 'password';`
    * `CREATE USER 'hgservice'@'localhost' IDENTIFIED BY 'password';`
    * `GRANT SELECT ON helpguest.* to 'hlpguest'@'localhost';`
    * `GRANT SELECT,INSERT ON helpguest.* to 'hgservice'@'localhost';`
    * You will also need an admin user to create the `helpguest` database
    

2. create the database `hgdb/create_db.sql`
3. create the tables in this order

    * create_tables.sql 
    * create_admin_tables.sql 
    * create_user_tables.sql 
    * signup_tables.sql 
    * search_tables.sql 
    * ipn_tables.sql 
    * load_data.sql
    
    It's possible to do this in one single command
    
```    
        cat create_tables.sql \
            create_admin_tables.sql \
            create_user_tables.sql \
            signup_tables.sql \
            search_tables.sql \
            ipn_tables.sql \
            load_data.sql | mysql -u root -p helpguest
```


## Compiling the code
1. Copy the *.properties.template files into resources/{project}/directory

    * /ChatterBox/build.properties.template
    * /hgservice/src/hg-general-service.properties.template
    * /hgservice/src/hg-tech-service.properties.template
    * /hgservice/src/hg-user-service.properties.template
    * /hgservice/src/hgservice.properties.template
    * /hgservlets/build.properties.template
    * /insight/proguard/expert.conf.template
    * /insight/proguard/guest.conf.template
    * /insight/src/com/helpguest/ssh/KeyManager.properties.template
    * /marketplace/build.properties.template
    * /marketplace/src/com/helpguest/marketplace/client/util/MarketPlaceConstants.properties.template

2. Remove the .template extension (they should all end in .properties now)
3. Update the *.properties to have the correct paths, content and metadata (for example, the tomcat and gwt locations)

    * for marketplace build.properties, encrypt the username and password
    * `java -classpath hgservice/classes com.helpguest.data.HGCipher <value>`
    
4. Download gwt 1.7.1 and install
5. `$> ant build`

## Deploying the project
1. For a default appache2 installation, deploy to /var/www/html

`ant -v deploy -Ddeploy.dir=<web component deploy directory>`
